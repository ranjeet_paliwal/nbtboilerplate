const path = require("path");
const fs = require("fs");
const dotenv = require("dotenv");
const colors = require("colors/safe");

if (!process.env.NODE_ENV) {
  throw new Error(colors.red(`Please specify NODE_ENV (development|production)`));
}

const site = process.env.SITE ? process.env.SITE : "nbt";
const envi = process.env.SITE ? process.env.NODE_ENV : "production";
const platform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";

const DOT_ENV_PATH = path.join(
  __dirname,
  `env/${site}/${
    platform == "desktop" && envi == "production" ? `${platform}/` : "" // for production env search in desktop folder for web
  }${envi}.env`,
);
const UNDER_ENV_PATH = path.join(__dirname, "env/" + "_env");

console.log(`--------------${DOT_ENV_PATH}`);
// The absence of a NODE_ENV suggests we are running locally
// and not in a deployed environment so we load the local .env file.
try {
  fs.statSync(DOT_ENV_PATH);
  dotenv.load({
    path: DOT_ENV_PATH,
  });
  // Require 'debug' after env has been setup to ensure it takes into
  // account `process.env.DEBUG`.
  require("debug")("app")(colors.yellow(`Using environment variables from ${process.env.NODE_ENV}.env`));
} catch (e) {
  throw new Error(
    colors.red(
      `${DOT_ENV_PATH} does not exist.
          Try renaming the '_env' file.`,
    ),
  );
}

// `./_env` is considered a definitive list of required environment variables
const missingVars = Object.keys(dotenv.parse(fs.readFileSync(UNDER_ENV_PATH))).filter(key => !process.env[key]);

if (missingVars.length) {
  throw new Error(colors.red(`Missing required environment variable(s): ${missingVars.join(", ")}`));
}
