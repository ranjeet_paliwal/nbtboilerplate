/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
export const retry_strategy = options => {
  if (options.error && options.error.code === "ECONNREFUSED") {
    // if connection unavailable , set chaching supported false
    configfile.redis.isRedisCachingSupported = false;
    console.log("Something went wrong ", options.error.code);

    // End reconnecting on a specific error and flush all commands with
    // a individual error
    return new Error("The server refused the connection");
  }
  if (options.total_retry_time > 1000 * 60 * 60) {
    // End reconnecting after a specific timeout and flush all commands
    // with a individual error
    return new Error("Retry time exhausted");
  }
  if (options.attempt > 10) {
    // End reconnecting with built in error
    return undefined;
  }
  // reconnect after
  return Math.min(options.attempt * 100, 3000);
};

export const flush_strategy = options => {
  if (options.attempt >= 3) {
    // flush all pending commands with this error
    return new Error("Redis unavailable");
  }
  // let the connection come up again on its own
  return null;
};

const redis = require("redis");
const configfile = require(`./../common/${process.env.SITE}`);
const client = redis.createClient({
  host: configfile.redis.host,
  port: configfile.redis.port,
  db: configfile.redis.db,
  // this strategy handles the reconnect of a failing redis
  retry_strategy,
  // this strategy handles the pending redis commands when the connection goes down
  flush_strategy,
});

client.on("connect", () => {
  // if connection available , set chaching supported false
  configfile.redis.isRedisCachingSupported = true;
  console.log("connected ");
});

client.on("error", err => {
  configfile.redis.isRedisCachingSupported = false;
  console.log("Something went wrong ", err);
});

const redisClient = {
  get: (key, callback) => {
    try {
      client.get(key, (err, reply) => {
        if (err) {
          console.log(`page not found!${err.message}`);
          typeof callback === "function" ? callback() : null;
          return false;
        }
        if (reply == null) {
          console.log("page not found!!!");
          typeof callback === "function" ? callback() : null;
        } else {
          typeof callback === "function" ? callback(err, reply) : null;
          return true;
        }
      });
    } catch (err) {
      console.log(`Redis error${err.message}`);
      typeof callback === "function" ? callback(err) : null;
    }
  },

  set: (key, html, expires, callback) => {
    try {
      client.set(key, html, "EX", expires || 60 * 60, (err, reply) => {
        if (err) {
          console.log(`page not created!${err.message}`);
          typeof callback === "function" ? callback() : null;
          return false;
        }
        typeof callback === "function" ? callback() : null;
        return reply;
      });
    } catch (err) {
      console.log(`Redis error${err.message}`);
      typeof callback === "function" ? callback(err) : null;
    }
  },

  delete: key => {
    try {
      if (del instanceof Array) key = key.join(",");
      client.del(key);
    } catch (err) {
      console.log(`Redis error${err.message}`);
      typeof callback === "function" ? callback(err) : null;
    }
  },
};

export default redisClient;
