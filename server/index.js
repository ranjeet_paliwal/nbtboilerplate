const path = require("path");
const fs = require("fs");
const fse = require("fs-extra");
const dotenv = require("dotenv");
const colors = require("colors/safe");

if (!process.env.NODE_ENV) {
  throw new Error(colors.red(`Please specify NODE_ENV (development|production)`));
}

const site = process.env.SITE ? process.env.SITE : "nbt";
const envi = process.env.SITE ? process.env.NODE_ENV : "production";
const platform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";

const DOT_ENV_PATH = path.join(
  __dirname,
  `env/${site}/${
    platform == "desktop" && envi == "production" ? `${platform}/` : "" // for production env search in desktop folder for web
  }${envi}.env`,
);
const UNDER_ENV_PATH = path.join(__dirname, "env/" + "_env");

console.log(`--------------${DOT_ENV_PATH}`);

// The absence of a NODE_ENV suggests we are running locally
// and not in a deployed environment so we load the local .env file.
try {
  fs.statSync(DOT_ENV_PATH);
  dotenv.load({
    path: DOT_ENV_PATH,
  });
  // Require 'debug' after env has been setup to ensure it takes into
  // account `process.env.DEBUG`.
  require("debug")("app")(colors.yellow(`Using environment variables from ${process.env.NODE_ENV}.env`));

  if (envi == "development") {
    // Copy common/ files to common_prod/
    // fse.copy('common' , 'common_prod').then(()=>console.log('config files copied for dev')).catch(err=>console.error(err))

    /**
     * Copies config files from 'common' to 'common_prod'
     */
    const configSrcPath = process.env.PLATFORM == "desktop" ? "common/desktop" : "common";
    const SITE_NAME = process.env.SITE;

    function copyConfigFile(srcDir, destDir, msg) {
      msg = msg || "config copy success for DEV!";
      fse
        .copy(path.join(srcDir, `${SITE_NAME}.js`), path.join(destDir, `${SITE_NAME}.js`))
        .then(() => console.log(msg))
        .catch(err => console.error(err));
    }

    copyConfigFile(configSrcPath, "common_prod");
    copyConfigFile(`common/footer`, "common_prod/footer");
    copyConfigFile(`common/css`, "common_prod/css");

    fse
      .copy(`src/router/${platform}/routes.js`, "src/router/master/routes.js")
      .then(() => console.log(`routes file copied on dev for platform ${platform}`))
      .catch(err => console.error(err));
  }
} catch (e) {
  throw new Error(
    colors.red(
      `${DOT_ENV_PATH} does not exist.
          Try renaming the '_env' file.`,
    ),
  );
}

// `./_env` is considered a definitive list of required environment variables
const missingVars = Object.keys(dotenv.parse(fs.readFileSync(UNDER_ENV_PATH))).filter(key => !process.env[key]);

if (missingVars.length) {
  throw new Error(colors.red(`Missing required environment variable(s): ${missingVars.join(", ")}`));
}

if (process.env.NODE_ENV === "production") {
  require("newrelic");
}

// const fs= require('fs');
const express = require("express");
const compression = require("compression");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const multer = require("multer");
const debug = require("debug")("app");
const responseTime = require("response-time");
// const colors = require('colors/safe');
const api = require("./routes/api");
const cms = require("./routes/cms");
const sitemap = require("./routes/sitemap");
const staticpageapi = require("./routes/staticpageapi");
const monitor = require('./routes/monitor');
const app = express();
const morgan = require("morgan");

if (process.env.NODE_ENV !== "development") {
  const accessLogStream = fs.createWriteStream("/var/log/node/lang-access.log", {
    flags: "a",
  });
  app.use(
    morgan(
      `:remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":response-time"`,
      { stream: accessLogStream },
    ),
  );

  process.on("uncaughtException", err => {
    if (err) {
      fs.appendFileSync("/var/log/node/lang-error.log", `caughtException but no error msg ${err.stack}`);
      // process.exit(1);
    }
  });
  process.on("unhandledRejection", err => {
    if (err) {
      fs.appendFileSync("/var/log/node/lang-error.log", `caughtException but no error msg ${err.stack}`);
     
      // process.exit(1);
    }
  });
}

// Don't set tags
app.set("etag", false);

if (process.env.PORT) {
  app.set("port", process.env.PORT);
} else {
  app.set("port", 3003);
}

app.enable("trust proxy");

// const SRC_DIR = path.join(__dirname, '../../../src/public');

app.use((req, res, next) => {
  const seconds = 6 * 30 * 24 * 60 * 60; // for 6 months now
  if (req.url.match(/^\/(img|icons|fonts|css)\/.+/)) {
    res.setHeader("cache-control", `public, max-age=${seconds}`);
    res.setHeader("expires", new Date(Date.now() + seconds * 1000).toUTCString());
  }
  next();
});

app.use(compression());
// app.use(methodOverride());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(multer());
app.use(responseTime()); // use to get response time

/* app.use(function (req, res, next) {
    //if (req.url.match(/^\/(api_)\/.+/)) {
        //res.setHeader('Cache-Control', 'public, max-age=360');
    //}
    next();
}); */

app.use("/api", api);

app.use("/cms", cms);

app.use("/staticpage", staticpageapi);

app.use('/monitoring', monitor);

// app.get('/healthcheckok', (req, res) => {
app.get("/healthcheck", (req, res) => {
  // res.write("OK");
  res.end("ok");
});

app.use("/testdr4", (req, res) => {
  res.status(404).send("testing DR  with 404");
});

app.use("/testdr5", (req, res) => {
  res.status(500).send("testing DR  with 500");
});

// app.get("/tech/compare-mobile-phones", (req, res) => {
//   // res.write("OK");
//   res.end("ok");
// });

if (
  process.env.NODE_ENV != "production" &&
  process.env.NODE_ENV != "stg1" &&
  process.env.NODE_ENV != "stg2" &&
  process.env.NODE_ENV != "stg3"
) {
  app.use(require("./routes/webpack"));
} else {
  app.use(require("./routes/static"));
}

if (process.env.NODE_ENV === "development") {
  const errorhandler = require("errorhandler");
  errorhandler.title = "¯\\_(ツ)_/¯";
  app.use(errorhandler());
}
const server = app.listen(app.get("port"), err => {
  debug(colors.white(`Server ${process.env.NODE_ENV} started: http://localhost:${process.env.PORT}`));
  console.log(colors.white(`Server ${process.env.NODE_ENV} started: http://localhost:${process.env.PORT}`));
  debug(colors.grey("Press 'ctrl + c' to terminate server"));
});
