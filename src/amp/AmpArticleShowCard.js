import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import AmpSocialShare from "./AmpSocialShare";
class AmpArticleShowCard extends Component {
  render() {
    let { value } = this.props;
    return (
      <div>
        {typeof value.img != "undefined" && value.img.hasOwnProperty("dsrc") ? (
          <div className="mainImage">
            <amp-img
              height="240"
              width="320"
              layout="responsive"
              src={
                value.img.dsrc
                  ? value.img.dsrc
                  : config.IMAGE_URL + value.img.src
              }
              alt={value.title ? value.title : ""}
            ></amp-img>
          </div>
        ) : null}

        <main className="box font-english">
          <h1 className="heading1">{value.title ? value.title : null}</h1>
          <div className="news-source">
            <a
              target="_blank"
              className="italic"
              rel="author"
              href={value.author.href ? value.author.href : "#"}
            >
              {value.author.name ? value.author.name : null}
              <span className="seperator">|</span>
            </a>
            {value.pubdate ? value.pubdate : null}
          </div>
          <AmpSocialShare
            su={value.lurl}
            title={value.title ? value.title : ""}
          />
          <article>
            <div
              data-plugin="newsshow"
              className="story"
              dangerouslySetInnerHTML={{
                __html: value.story
              }}
            />
          </article>
          <AmpSocialShare
            su={value.su}
            title={value.title ? value.title : ""}
          />
        </main>
      </div>
    );
  }
}

AmpArticleShowCard.propTypes = {
  value: PropTypes.object
};

export default AmpArticleShowCard;
