import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
class Footer extends Component {
  render() {
    return (
      <div>
        <footer data-node="footer" className="clearfix" id="footer">
          <a className="backtotop" href="#top">
            <span className="btpText">BACK TO TOP</span>
            <span className="circle mobile-sprite"></span>
          </a>
          <ul className="footer-links clearfix">
            <li className=" ">
              <a
                className="track"
                href="/aboutus.cms?lang=english"
                data-src="About Us"
                data-track="npfooter_About Us"
                data-gtm-event="npfooter_About Us"
              >
                About Us
              </a>
            </li>
            <span className="seperator">|</span>
            <li className=" ">
              <a
                className="track"
                href="/termsofpolicy.cms?lang=english"
                data-src="Terms of Use"
                data-track="npfooter_Terms of Use"
                data-gtm-event="npfooter_Terms of Use"
              >
                Terms of Use
              </a>
            </li>
            <span className="seperator">|</span>
            <li className=" ">
              <a
                className="track"
                href="mailto:npappfeedback@timesinternet.in?subject=Feedback on NewsPoint - ArticleShow"
                data-src="Feedback"
                data-track="npfooter_Feedback"
                data-gtm-event="npfooter_Feedback"
              >
                Feedback
              </a>
            </li>
            <span className="seperator">|</span>
            <li className=" ">
              <a
                className="track"
                href="/privacypolicy.cms?lang=english"
                data-src="Privacy Policy"
                data-track="npfooter_Privacy Policy"
                data-gtm-event="npfooter_Privacy Policy"
              >
                Privacy Policy
              </a>
            </li>
            <span className="seperator">|</span>
          </ul>
          <span className="copyright">
            Copyright © Bennett, Coleman &amp; Co. Ltd. All rights reserved.
          </span>
          <span className="reprint">
            For reprint rights:{" "}
            <a
              data-track="nbtfooter_tss"
              className="track"
              href="https://timescontent.com/"
            >
              Times Syndication Service
            </a>
          </span>
        </footer>
      </div>
    );
  }
}

export default Footer;
