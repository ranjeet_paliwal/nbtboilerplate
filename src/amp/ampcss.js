import { _getStaticConfig, getCssPath } from "../utils/util";

const config_css = _getStaticConfig("cssConfig");

let cssVariables = {
  "$color-white": "#fff",
  "$home-section-first-bg": "#fed20a",
  "$home-section-second-bg": "#fff",
  "$home-section-txt-color": "#000",
  "$home-section-more-color": "#000",
  "$home-section-more-bg": "#fed20a",
  "$home-section-more-border": "#fed20a",
  "$font-family-name": "Noto Sans",
  "$image-sitename": "nbt",
  "$font-family-regular": "NotoSansDevanagari-Regular",
  "$story-partition-border-color": "#fed20a",
};

const assignCssVariables = key => {
  try {
    return config_css.css.split(`${key}:`)[1].split(";")[0];
  } catch (ex) {
    console.log("exception caught: ", ex);
  }
};

export const mapCssVariables = _cssVariables => {
  if (typeof _cssVariables === "object") cssVariables = _cssVariables;
  for (const key in cssVariables) {
    cssVariables[key] = assignCssVariables(key);
  }

  return cssVariables;
};

mapCssVariables();

export const ampCss = (portalName, pagetype, reqURL) => {
  // const spriteAstro = `https://static.${portalName}.indiatimes.com/img/pwa_astrology.svg?v=17072020`; // Astro sprite path
  const spriteAstro = getCssPath(portalName, "pwa_astrology"); // Astro sprite path
  const spritesizeAstro = "268px 171px"; // Astro sprite size

  const headerCss = `
        html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%}

        @media (max-width: 319px) {
        html {font-size: 56.25%;}
        }

        @media (min-width: 320px) and (max-width: 359px) {
        html {font-size: 62.5%;}
        }

        @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ),  (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
        html {font-size: 62.5%;}
        }

        @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ),  (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
        html {font-size: 68.75%;}
        }
        .svgiconcount {display:inline-flex; width:100%; height:100%;}
        .svgiconcount svg {margin:auto;}

         q:before, q:after {display:none;}
        .header {position: relative; background:#fff}
        .header:after{content: ''; display: block; clear: both;}
        
        .header .logo {float: left; margin: 0 0 0 20px; position: relative; height: 50px;}
        .header .logo h1, .header .logo h2{position: absolute; top: 50%; transform: translateY(-50%); left: 0; margin:0; padding:0}
        .header .logo a {display: block; font-size: initial;}
        .header .logo amp-img {vertical-align:bottom;}
        .header .logo.logo-gadget amp-img{width:116px; height:30px}
        .header .logo .txt-seo {font-size: 1px;display: block;text-indent: -100000px;height: 1px;color: #fff;}

        .header .app_download{float: right; margin: 13px 12px 0 0}
        .header .app_download svg{width: 42px; height: 23px;}
        .header .app_download svg.ios, .header .app_download svg.android {width: 23px; height: 23px;}
        .appicotext {font-size: 14px; transform: translate(10px, 9px);}
        .appicotext.ta, .appicotext.bn, .appicotext.kn, .appicotext.te {font-size:12px; transform: translate(6px, 7px);}
        .appicotext.ml {font-size:10px; transform: translate(7px, 6px);}
        .home_icon {width: 24px; height: 24px; display: inline-block; vertical-align: top;}
        .hrnavigation.top_fixed{position: fixed; left: 0; top: 0; width: 100%; z-index: 99; display: block;}
        .hrnavigation {overflow: hidden; position: relative; background: #fff; border-top: 1px solid #d5d5d5; box-shadow: 0 2px 5px #b8b8b8;}
        
        .hrnavigation h3{display: block; margin:0; padding:0}
        .hrnavigation h3 a{position: absolute; width: 45px; height: 35px; text-align: center; padding-top: 10px; opacity: 0.5;}
        .hrnavigation .nav_scroll {display: block; white-space: nowrap; margin: 0; padding: 0; width: calc(100% - 40px); overflow-y: scroll; height: 45px; margin-left: 40px;}
        
        .hrnavigation .nav_scroll:empty{display: none;}
        .hrnavigation ul {margin: 0; padding: 0; list-style: none; position: relative;}
        .hrnavigation ul li {position:relative; overflow:hidden; transform:translate3d(0,0,0); display: inline-block; margin: 0 5px; padding: 0; text-align: center; vertical-align: middle; width: auto; min-height: 43px; border-bottom: solid 2px transparent;}
        .hrnavigation ul li.active{border-bottom: solid 2px #000;}
        .hrnavigation ul li.active a{font-weight: bold;}
        .hrnavigation ul li a.nav_link {display: block; font-size: 1.6rem; line-height: 2.6rem; margin: 0; padding: 12px 10px 0; color:#000;}
        .hrnavigation ul li.hrnavparent a{position: relative;}
        .hrnavigation ul li.hrnavparent a:after{content:'❯'; font-size: 1.3rem; position: absolute; right: -8px; top: 12px;}
        .hrnavigation ul li.hide {display:none;}
        .hrnavigation ul li.special_new:not(.isicon) {position: relative; padding-right: 16px;}
        .hrnavigation ul li.special_new:not(.isicon):before {content: 'NEW';position: absolute;right: 0;top: 3px;padding: 2px 3px 1px;background-color: #ff0000;color: #ffffff;display: block;font-size: 8px;border-radius: 8px;font-weight: 600;}
        .hrnavigation ul li.special_dot:not(.isicon) {position: relative;}
        .hrnavigation ul li.special_dot:not(.isicon):before {content: '';position: absolute;right: 0;top: 9px;background-color: #ff0000;display: block;width: 5px;height: 5px;border-radius: 50%;}
        .hrnavigation ul li.special_color:not(.isicon) a.nav_link {color: #ff0000;font-weight: 600;}
        .hrnavigation ul li svg {width:28px; height:28px;}
        
        .hrnavigation ul li:after{content:"";display:block;position:absolute;width:100%;height:100%;top:0;left:0;pointer-events:none;background-image:radial-gradient(circle,#000 10%,transparent 10.01%);background-repeat:no-repeat;background-position:50%;transform:scale(10,10);opacity:0;transition:transform .5s,opacity 1s}
        .hrnavigation ul li:active:after{transform:scale(0,0);opacity:.2;transition:0s}
        .header .goldnav {float: right;margin: 13px 12px 0 0;}
        .header .goldnav a {width: 23px;height: 23px;display: block;}
        .header .goldnav a svg {width: 23px;height: 23px;}
        .header .hamburger{display:none}
        .atf-wrapper {height: 64px; overflow-y:auto;}
    `;

  const footerCss = `
        .bottom_nav{padding: 15px 3%;}
        .bottom_nav h3{margin:0; font-size: 1.6rem; line-height: 1.9rem;}
        .bottom_nav ul{margin-top:20px;}
        .bottom_nav ul li{display:inline-block; width:49%; box-sizing:border-box; margin-bottom:15px; font-size: 1.4rem;}
        .bottom_nav ul li a{position:relative; padding:0 0 0 15px}
        .bottom_nav ul li a:before{content: ''; height: 6px; width: 6px; border-radius: 50%;position: absolute; left: 0; top: 5px;}
        
        .footer {font-family: arial; padding: 20px 3% 60px; background:#f2f1f1}
        .footer h4 {font-size: 1.3rem; margin: 0 0 10px; padding: 0 10px 8px 0; text-transform: uppercase;}
        .footer .head {display: inline;	font-size: 1.3rem; font-weight:600;	text-transform: uppercase;}
        .footer .footlinks-wrapper {margin-bottom: 15px;}
        .footer .footlinks-wrapper a:after {content: "|";	margin: 0 5px;}
        .footer .footlinks-wrapper a:last-child:after {content: ""; margin: 0}
        .footer .footlinks-wrapper a {display: inline-block; font-size: 1.3rem;	line-height: 2.2rem;color: #555;}
        .footer .trending-topics {margin-bottom: 15px;}
        .footer .trending-topics .head{display: block;}
        .footer .trending-topics a {padding: 3px 10px;	border-radius: 20px;	margin: 0 8px 8px 0; display: inline-block;	font-size: 1.3rem;}
        .footer .footer-links{text-align:center}
        .footer .footer-links a{font-size: 1.2rem;}
        .footer .copy-right {display: block; font-size: 1.2rem; line-height: 1.8rem; text-align: center; color: #555;}
        .footer .copy-right a, .footer .copy-right a:visited {text-decoration: none; color: #555;}
        .footer .sociallink, .footer .app-wrapper {width: 50%; padding-left: 5%; box-sizing: border-box; text-align: center; display: inline-block; vertical-align: top;}
        .footer .sociallink .heading, .footer .app-wrapper span {margin-bottom: 10px; display: block; font-size: 1.2rem; font-weight: 600; font-family: arial; color: #333; text-transform: uppercase;}
        .footer .sociallink .icons a, .footer .app-wrapper a{width:30px; height:30px; display:inline-block; vertical-align: top; border-radius: 50%;background:#000; margin-right: 7px;}
        .footer .sociallink a:last-child, .footer .app-wrapper a:last-child{margin: 0}
        .footer .sociallink .icons a svg, .footer .app-wrapper a svg {margin: auto; width: 16px; color: #fff;}
        .footer .sociallink .icons a.facebook{background-color: #3A5897;}
        .footer .sociallink .icons a.google-plus{background-color: #DA4835;}
        .footer .sociallink .icons a.twitter{background-color: #5EA8DC;}
        .footer .sociallink .icons a.youtube {background-color: #E10000;}
        .footer .sociallink .icons a.telegram {background-color: #27A7E8;}

        .footer .app-wrapper{ padding-right: 5%; border-right: 2px solid #e1e1e1;}
        .footer .app-wrapper span{white-space: nowrap;}
        .footer .app-n-follow {background-color: #fff; border: 1px solid #cbcbcb; margin: 10px 0; padding: 12px 3% 8px;}
        #onetap-box {height: 50px;width: 100%;bottom: auto;top: calc(100vh - 130px);left: 0;right: 0;margin: auto;z-index: 100;position: fixed;}
        #onetap-box iframe[class*="onetap-google-ui-bottom_sheet"]{top: calc(100vh - 280px); bottom: auto;}
    `;

  const commonCss = `
        ${
          !(portalName == "nbt" || portalName == "mt")
            ? `
            @font-face {
              font-family: ${cssVariables["$font-family-name"]};
              src: url('https://static.${cssVariables["$image-sitename"]}.indiatimes.com/fonts/${cssVariables["$font-family-regular"]}.eot');
              src: url('https://static.${cssVariables["$image-sitename"]}.indiatimes.com/fonts/${cssVariables["$font-family-regular"]}.eot?#iefix') format('embedded-opentype'),
              url('https://static.${cssVariables["$image-sitename"]}.indiatimes.com/fonts/${cssVariables["$font-family-regular"]}.woff2') format('woff2'),
              url('https://static.${cssVariables["$image-sitename"]}.indiatimes.com/fonts/${cssVariables["$font-family-regular"]}.ttf') format('truetype');
              font-weight: normal;
              font-style: normal;
              font-display: optional;
            }
          `
            : ``
        }

        body {margin: 0; padding: 0; font-family: 'Noto Sans' , Arial,Helvetica,sans-serif; font-size: 1.6rem; background: #e4e4e4; color: #000; max-width: 768px; margin: auto;}
        body.webview .navbar, body.webview .footer, body.webview .breadcrumb , body.webview .landscape-mode , body.webview .ad1, body.webview [data-plugin=ctn] {display: none;} 
        img {border: 0; vertical-align: bottom;}
        a{color:#000; text-decoration: none;}
        a:focus, a:hover, a:active {outline:none; color:#000; cursor: pointer; text-decoration: none;}
        ul{margin:0; padding:0; list-style:none}
        form, input, select {margin: 0; padding: 0; outline: none; font-family: 'Noto Sans', Arial,Helvetica,sans-serif;}
        .tbl, .table {display: Table; width: 100%;}
        .tbl .tbl_row, .table .table_row {display: table-row; vertical-align: middle;}
        .tbl .tbl_col, .table .table_col, .table .tbl_column {display: table-cell; vertical-align: middle;}

        .ad1 {text-align: center; position: relative; z-index: 0;}
        .ad2 {min-height:1px;}
        .ad1.mrec, .ad1.mrec1, .ad1.mrec2{ margin: auto;}
        .ad1.atf{padding:7px 2%; height: 50px}
        .ad1.atf amp-ad[type=doubleclick]{vertical-align:top}
        .topatf {text-align: center; position: relative; }
        .nbt-list [data-plugin="ctn"] {margin: 0 3% 4%;border-bottom: 1px solid #dfdfdf;text-align: center; background: transparent;}
        amp-iframe[width="1"][title="User Sync"]{display:block}

        .row{display:flex;flex-wrap:wrap;align-content:flex-start;margin-right:-15px;margin-left:-15px;}
        .row .col{margin:0;}
        .row .col1, .row .col2, .row .col3, .row .col4, .row .col5, .row .col6, .row .col7, .row .col8, .row .col9, .row .col10, .row .col11, .row .col12{display:flex;flex-wrap:wrap;align-content:flex-start;padding-right:15px;padding-left:15px;box-sizing:border-box;}
        .row .col12{flex:0 0 100%;max-width:100%;}
        .row .col11{flex:0 0 91.66%;max-width:91.66%;}
        .row .col10{flex:0 0 83.33%;max-width:83.33%;}
        .row .col9{flex:0 0 75%;max-width:75%;}
        .row .col8{flex:0 0 66.66%;max-width:66.66%;}
        .row .col7{flex:0 0 58.33%;max-width:58.33%;}
        .row .col6{flex:0 0 50%;max-width:50%;}
        .row .col5{flex:0 0 41.66%;max-width:41.66%;}
        .row .col4{flex:0 0 33.33%;max-width:33.33%;}
        .row .col3{flex:0 0 25%;max-width:25%;}
        .row .col2{flex:0 0 16.66%;max-width:16.66%;}
        .row .col1{flex:0 0 8.33%;max-width:8.33%;}
        .row .mr0, .row.mr0{margin:0;}
        .row .pd0, .row.pd0{padding:0;}

        .news-card.video .img_wrap a {display:block;}

        .nbt-list [data-plugin=ctn],[data-plugin="ctn"]{min-height:50px; width:100%}
        .top-news-content, .box-content {margin: 0 0 4%; background: #fff; border-bottom: 2px solid #c6c6c6; padding: 0 0 3%;}
        .box-item{margin-bottom: 4%; background: #fff; padding-bottom:3%;}
        .box-item.data-after-ads {padding-top: 4%;}

        .section{width:100%; padding: 0 15px; box-sizing: border-box;}  
        .section h3, .section h2, .section h1{font-size: 1.8rem; font-weight: bold; display: block; height: 40px; position: relative; border-bottom: 2px solid #dfdfdf; margin:0}
        .section h3 span, .section h2 span, .section h1 span {height: 40px; line-height: 40px; padding: 0 10px 0 0; display: inline-block; position: relative; font-weight: bold;}
        .section h3 span:before, .section h2 span:before, .section h1 span:before {content: ''; position: absolute; bottom: -2px; left: 0; right: 0; border-bottom: 2px solid #000;}
        .section .top_section{position: relative; margin:0 3% 4%;}
        .section .top_section .read_more, .section .top_section .read_more{position: absolute; right: 0; bottom: 10px; font-size: 13px; margin: 0 15px 0 0; color: #000;}
        .section .top_section .read_more:after, .section .top_section .read_more:after {content: ""; display: block; height: 5px; width: 5px; margin: auto; position: absolute; top: 0; bottom: 0; transform: rotate(45deg); right: -10px; border-top: 1px solid #000; border-right: 1px solid #000; }


        .box-content h2, .sectionHeading h1{font-size: 1.8rem; margin:5% 0; padding:0 0 0 42px; font-weight: bold; line-height: 2.4rem; position:relative; box-sizing: border-box; display: inline-block; width: 100%;}
        .box-content h2:before, .sectionHeading h1:before {height:18px; top: 3px; left: 0; width:42px; z-index: 1; position: absolute; content: ""; background: linear-gradient(45deg,${
          cssVariables["$home-section-first-bg"]
        } 71%, ${cssVariables["$home-section-second-bg"]} 0);}
        .box-content h2:after,.more-btn, .sectionHeading h1:after{background: ${
          cssVariables["$home-section-first-bg"]
        };}
        
        .more-btn{display: block; margin: 20px auto; width:70%; text-align: center; padding: 7px 10px 5px;  font-family: 'Noto Sans' , Arial,Helvetica,sans-serif; font-weight: normal; font-size:1.6rem; font-weight: 600}
        .more-btn, .more-btn:active, .more-btn:focus, .more-btn:hover {color: ${
          cssVariables["$home-section-more-color"]
        };}
        
        .box-content h2 span, .sectionHeading h1 span {display: inline-block; padding: 0 10px; position: relative; z-index: 1; box-sizing: border-box;}
        .box-content h2:after, .sectionHeading h1:after{width: 100%; height: 2px; left: 0; top: 50%; margin-top: -1px; position: absolute; content: "";}
        
        .sectionHeading {background: ${cssVariables["$color-white"]};}
        .sectionHeading h1 {color:${cssVariables["$home-section-txt-color"]};}
        .box-content h2 span:after, .sectionHeading h1 span:after {background: ${
          cssVariables["$home-section-first-bg"]
        }}
        .sectionHeading h1 span, .box-content h2 span {background: ${cssVariables["$home-section-second-bg"]};}


        .section ul.sub-list{position: relative;position: relative; white-space: nowrap;} 
        .section ul.sub-list li{display: inline-block; vertical-align: top; margin: 0 25px 5px 0;}
        .section ul.sub-list li a{font-size: 1.3rem; color: #000; line-height: 2.1rem; display: block;}
        .section ul.sub-list li:last-child{margin-right: 0;}

        .section .sub-section{position: relative; margin: 0 3% 15px;overflow: hidden;overflow-x: auto;}
        .section .sub-section span.more-sublist {position: absolute; top:-3px; right: 0; height: 22px; width: 33px; text-align: center; cursor: pointer;}
        .section .sub-section span.more-sublist b{height:5px; width: 5px; display: inline-block; border-radius: 50%; background: #000; margin: 0 10px; position: relative;}
        .section .sub-section span.more-sublist b:before, .section .sub-section span.more-sublist b:after{content: ''; position: absolute; left:-10px; height:5px; width: 5px; display: inline-block; border-radius: 50%; background: #000;}
        .section .sub-section span.more-sublist b:after{left:auto; right: -10px;}

        .breadcrumb{padding:10px 4% 5px; background:#fff}
        .breadcrumb ul {line-height: 1.8rem;}
        .breadcrumb ul li{list-style-type: none;display: inline;}
        .breadcrumb ul li a, .breadcrumb ul li a span{text-decoration:none; font-size: 1.0rem; vertical-align:top; line-height: 1.6rem; color: #a0a0a0;}
        .breadcrumb ul li span{font-size: 1.0rem; vertical-align:top; line-height: 1.6rem; color: #a0a0a0; text-transform: capitalize;}
        .breadcrumb ul li:after{content:"\\BB"; padding: 0 1% 0 2%; font-size: 1.4rem; vertical-align: top; line-height: 1.6rem; color: #a0a0a0;}
        .breadcrumb ul li:last-child:after{content:""; padding:0;}

        amp-consent {background: none;}
        .popupOverlay {padding: 10% 0}
        .popupOverlay .consentPopup{background:#fff; border-radius:13px; margin:0 auto; padding:4% 5% 5%; font-size:1.2rem; line-height:1.8rem; width:80%; color:#000; box-shadow: 0 0px 20px #000;}
        .popupOverlay .dismiss-button{text-align: right; font-size: 1.6rem; padding:5px 5px 10px; display:none}
        .popupOverlay .consentPopup h2,.popupOverlay .consentPopup .h2{margin:0px 0px 10px; padding:0px 0px 10px 0; font-size:1.5rem; line-height:1.8rem; color:#B03726; text-transform:uppercase; border-bottom:1px solid #D1D1D1; text-align:center}
        .popupOverlay .consentPopup p.m1{margin:0 0 15px}
        .popupOverlay .consentPopup a{font-weight: bold; text-decoration: underline;}
        .popupOverlay .consentPopup .btn{margin: 10px 0 0; width: auto; min-width: auto; border: 0; display: block;}
        .popupOverlay .consentPopup .btn {text-align:center}
        .popupOverlay .consentPopup .btn button{border-radius:20px; text-transform:uppercase; font-size:1.2rem; line-height:1.6rem; display:inline-block; padding:5px 0;  border:1px solid #000; cursor:pointer; font-family:arial; width:35%; text-align:center;}
        .popupOverlay .consentPopup .btn button.success{margin:0 10px 0 0; background:#000; color:#fff;}
        .popupOverlay .consentPopup .btn button.reject{background:#fff; color:#000}
        .btn_consent {display:none;background: #000; border: 1px solid #eee; width: 65px; border-radius: 100%; height: 65px; font-size: 10px; font-weight: bold; color:#fff; margin-left: -7px; margin-bottom: 70px;}
        .amp-geo-group-eu .btn_consent{display:inline-block;}
        @media screen and (min-width:500px) {
            .popupOverlay .consentPopup{padding:4%}
        }

        .button{border:1px solid #ee111c; font-size: 1.3rem; height:22px; line-height: 23px; padding: 0 5%; border-radius:3px;  font-family: 'Noto Sans' , Arial,Helvetica,sans-serif; color:  #ee111c; background: transparent;}

        .scroll_content {width: 100%; overflow: hidden; overflow-x: auto; height: auto;}

        .text_ellipsis{overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2;}
        .white-bg {background: #fff;}
        
        .type_icon {position: absolute; bottom: 0; right: 0; top: 5px; left: 5px; display: inline-block; z-index:10;}
        .back_icon, .video_icon {background-repeat: no-repeat;}
        .video_icon {width: 18px; height: 18px; border-radius: 50%; background:red;}
        .video_icon:after {content: ''; border-top: 5px solid transparent;border-bottom: 5px solid transparent;border-left: 5px solid white;position: absolute;left: 50%;top: 50%; border-radius:2px; transform: translate(-50%, -50%);}
        .photo_icon {width: 25px; height: 21px; background: url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzUgMjYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgIDxwYXRoIHN0cm9rZT0iI2ZmZmZmZiIgc3Ryb2tlLXdpZHRoPSIxLjcyIiBkPSJNMSA5LjQ3aDI0LjExdjE1LjVIMXoiLz4KICAgIDxwYXRoIGZpbGw9IiNmZmZmZmYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTI1Ljk3IDIyLjM5di0xLjczaDIuNThWNi4wM0g2LjE3VjguNkg0LjQ0di00LjNoMjUuODN2MTguMDhoLTEuNzJ6Ii8+CiAgICA8cGF0aCBmaWxsPSIjZmZmZmZmIiBmaWxsLXJ1bGU9Im5vbnplcm8iIGQ9Ik0zMC4yNyAxOC4wOHYtMS43MmgyLjU5VjEuNzJIMTAuNDdWNC4zSDguNzVWMGgyNS44M3YxOC4wOGgtMS43MnoiLz4KICA8L2c+Cjwvc3ZnPg==") no-repeat;}
        
        .yContainer {display:none}

        .news-card.lead{width:100%;margin-bottom:4%;}
        .news-card.lead .img_wrap{display:block; width:100%}
        .news-card.lead .img_wrap img, .news-card.lead .img_wrap amp-img{width:100%;}
        .news-card.lead .con_wrap{display:block;margin:10px 3% 0;}
        .news-card.lead .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
        .news-card.lead .con_wrap .text_ellipsis{font-size:1.5rem;line-height:2.5rem;max-height:7.5rem;-webkit-line-clamp:3;}
        .news-card.horizontal{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;width:100%;}
        .news-card.horizontal .img_wrap{margin:0 10px 0 0;position:relative;}
        .news-card.horizontal .img_wrap img, .news-card.horizontal .img_wrap amp-img{width:80px;}
        .news-card.horizontal .con_wrap .section_name{color:#1f81db;font-size:1.3rem;display:block;margin-bottom:5px;}
        .news-card.horizontal .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;-webkit-line-clamp:2;}
        .news-card.horizontal-lead{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;flex-wrap:nowrap;width:100%;}
        .news-card.horizontal-lead .img_wrap{margin:0 10px 10px 0;position:relative;}
        .news-card.horizontal-lead .img_wrap img, .news-card.horizontal-lead .img_wrap amp-img{width:140px;}
        .news-card.horizontal-lead .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
        .news-card.horizontal-lead .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3;}
        .news-card.vertical{margin-bottom:20px;}
        .news-card.vertical .img_wrap{margin:0 0 10px 0;position:relative;display:block;}
        .news-card.vertical .img_wrap img, .news-card.vertical .img_wrap amp-img{width:154px;}
        .news-card.vertical .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
        .news-card.vertical .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3;overflow:hidden;}
        .news-card.only-info{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;width:100%;}
        .news-card.only-info img, .news-card.only-info amp-img{display:none;}
        .news-card.only-info .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
        .news-card.only-info .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3;}
        .news-card.photo .img_wrap{position:relative; display:block;}
        .news-card.photo .img_wrap a{position:relative}
        .news-card.photo .img_wrap a:before{content:'';position:absolute;top:10px;left:10px;width:25px;height:21px; background: url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzUgMjYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgIDxwYXRoIHN0cm9rZT0iI2ZmZmZmZiIgc3Ryb2tlLXdpZHRoPSIxLjcyIiBkPSJNMSA5LjQ3aDI0LjExdjE1LjVIMXoiLz4KICAgIDxwYXRoIGZpbGw9IiNmZmZmZmYiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTI1Ljk3IDIyLjM5di0xLjczaDIuNThWNi4wM0g2LjE3VjguNkg0LjQ0di00LjNoMjUuODN2MTguMDhoLTEuNzJ6Ii8+CiAgICA8cGF0aCBmaWxsPSIjZmZmZmZmIiBmaWxsLXJ1bGU9Im5vbnplcm8iIGQ9Ik0zMC4yNyAxOC4wOHYtMS43MmgyLjU5VjEuNzJIMTAuNDdWNC4zSDguNzVWMGgyNS44M3YxOC4wOGgtMS43MnoiLz4KICA8L2c+Cjwvc3ZnPg==") no-repeat; z-index:1;}
        .news-card.video .img_wrap a{position:relative; display:block;}
        .news-card.video .img_wrap[data-tag]:after{font-family:arial;font-size:1.1rem;position:absolute;bottom:10px;right:10px;padding:2% 3%;line-height:1.2rem;background:rgba(0, 0, 0, 0.6);content:attr(data-tag);text-align:center;color:#fff;z-index:10;}
        .news-card.video .img_wrap{position:relative; display:block;}
        .news-card.video .img_wrap a:before{content:'';position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);width:25px;height:25px;background: red; border-radius: 50%;box-shadow: 0 1px 8px rgba(0, 0, 0, 0.7); z-index:1;}
        .news-card.video .img_wrap a:after {content: '';border-top: 5px solid transparent;border-bottom: 5px solid transparent;border-left: 7px solid #fff;position: absolute;top:50%;left:50%;transform:translate(-50%, -50%); z-index:2; border-radius:2px;}
        .news-card.video.lead .img_wrap a:before{width:40px;height:40px;}
        .news-card.video.lead .img_wrap a:after {border-top: 7px solid rgba(0,0,0,0); border-bottom: 7px solid rgba(0,0,0,0); border-left: 9px solid #fff;}
        .fixedHeightContent .news-card.lead{margin-bottom:15px; padding:0;}
        .fixedHeightContent .news-card.horizontal{margin-bottom:15px; padding-bottom:10px; height:72px}
        .fixedHeightContent .news-card.lead .img_wrap img, .fixedHeightContent .news-card.lead .img_wrap amp-img{max-height:170px}
        .webtitle{margin:20px 4% 0;font-size:1.0rem;line-height:1.8rem;color:#a0a0a0;}
        .webtitle b{font-weight:600;}
        .webtitle a{color:#1a75ff;}
        .keywords_wrap {padding: 15px 0; position: relative; margin: 20px 4% 0; background: #fff; border-bottom: 1px solid #d0d0d0;}
        .keywords_wrap h3 {font-size: 1.6rem; text-align: center; margin: 0 0 15px; color: #000;}
        .keywords_wrap .nowarp_content {white-space: nowrap;}
        .keywords_wrap a {margin: 2px 5px 5px 2px; font-size: 1.1rem; border-radius: 20px; padding: 0 20px; height: 31px; line-height: 33px; display: inline-block; white-space: nowrap; color: #716f6f; border: 1px solid #cbcbcb; background: #fff;}
        .wdt_amz, .wdt_twogud{margin-bottom:4%}
        .wdt_amz iframe, .wdt_twogud iframe{width:100%}
        .result-widget .graph-container, .result-widget .graph-container .result-tbl, .result-widget .table-container, .result-widget .table-container .result-tbl {margin-top:0; padding: 0;}
        .box-item:empty {display:none;}
        .youtube-iframe {position: relative; width: 180px; height: 25px; display: flex; align-items: center;}
        .youtube-iframe .txt {font-family: arial; font-size: 13px; color: #000; text-transform: capitalize;}
        .youtube-iframe amp-iframe {position: absolute; width: 130px; height: 25px; top: 0; left: 65px; z-index: 100;} 
        .youtube-iframe iframe{overflow:hidden}
        .liveblink_newscard {display: inline-block; position: relative; font-size: 12px; text-transform: uppercase; padding-left: 13px; color: red; font-family:sans-serif;}
        .news-card.lead .liveblink_newscard {display:block;}
        .liveblink_newscard::before{box-sizing:border-box;content:" ";position:absolute;top:50%;left:2px;width:6px;height:6px;border-radius:50%;background-color:red;transform:translateY(-50%); margin-top:-1px;}
        .liveblink_newscard::after{opacity:0;box-sizing:border-box;content:" ";position:absolute;top:50%;left:0;width:10px;height:10px;border:1px solid red;border-radius:50%;background-color:transparent;transform:translateY(-50%);z-index:1;animation:liveblink 2s infinite 1s; margin-top:-1px;}
        @keyframes liveblink{0%{opacity:0;}
        50%{opacity:1;}
        to{opacity:0;}
        }
    `;

  const listCss = `
        ul {margin: 0; padding: 0; list-style-type: none;}
        li.nbt-listview {margin: 0 3% 4%; padding-bottom: 4%; width:94%; position: relative; border-bottom: 1px solid #efefef;}
        li.nbt-listview .img_wrap{position:relative; width:32%; vertical-align:top;display: table-cell;}
        li.nbt-listview .img_wrap img{vertical-align:bottom; width: 100%; height: auto;}
        li.nbt-listview .con_wrap {text-align: left; word-break: break-word; padding: 0 0 20px 10px; vertical-align: top; position: relative; font-weight: 600; color: #333;}
        li.nbt-listview .text_ellipsis{font-size: 1.6rem; line-height: 2.4rem;}
        li.nbt-listview .time-caption {font-size: 1.1rem; line-height: 1.2rem; display: block; position: absolute; bottom: 0; font-weight: normal;color:#bababa;}

        .nbt-list li.nbt-listview.lead-post {border-bottom: 0; padding-bottom: 0; position:relative; margin:0 0 4%;}
        .nbt-list li.nbt-listview.lead-post .img_wrap{min-height: 178px}
        .nbt-list li.nbt-listview.lead-post .con_wrap {position: absolute; margin: auto; bottom: 0; left: 0; right: 0; padding: 15px 3% 10px; background: linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.9)); color: #fff; z-index:1;}
        .nbt-list li.nbt-listview.lead-post .text_ellipsis{font-size: 2.0rem; line-height: 3.0rem; max-height: 6.0rem; color:#fff;}
        .nbt-list li.nbt-listview.lead-post, .nbt-list li.nbt-listview.lead-post .table_row, .nbt-list li.nbt-listview.lead-post .table_col{display:block; width:auto; float:none}
        .nbt-list li.nbt-listview.lead-post .video_icon {width:40px; height:40px; top:50%;left:50%;transform:translate(-50%, -50%); border-radius:50%; background:red;}
        .nbt-list li.nbt-listview.lead-post .video_icon:after {border-top: 12px solid transparent;border-bottom: 12px solid transparent;border-left: 14px solid #fff;}
        .nbt-list li.nbt-listview.lead-post .photo_icon {width:39px; height: 33px; top:6px; left:6px}
        .nbt-list li.nbt-listview.lead-post .photo_icon{background-position: -9px -278px; background-size: 79px 1077px;}
        .nbt-list li.nbt-listview.lead-post .time-caption {position:static;}
        .nbt-list li.nbt-listview.lead-post[data-list-type]:after{content: ''; display:none}
        .nbt-list li.nbt-listview .img_wrap.photolead_img_warpper{background-color: #000}
        .nbt-list li.nbt-listview .img_wrap.photolead_img_warpper img{height: 270px; object-fit: contain}

        .nbt-list li.nbt-listview[data-list-type]:after {content: attr(data-list-type); position: absolute; right:0px; bottom:14px; font-size: 1.2rem; border: 1px solid #9a9a9a; color: #747474; background: #f0f0f0; height:28px; line-height: 30px; border-radius: 20px; width:25%; text-align: center; font-weight:normal; white-space: nowrap;}
        .nbt-list li.nbt-listview[data-list-type=""]:after {display:none}
        .nbt-list li.nbt-listview:last-child[data-list-type]:after{bottom:0}

        .nbt-list li.nbt-listview.section-name .news_section{padding:0 0 0 10px; left:32%; position: absolute; top:0; z-index:10;}
        .nbt-list li.nbt-listview.section-name .news_section a{font-size: 1.4rem; line-height: 2.2rem; font-weight: bold; color:#0e76bc}
        .nbt-list li.nbt-listview.section-name .con_wrap{padding-top: 20px}

        .nbt-list li.nbt-listview.lead-post.section-name .news_section{padding:0 0 5px 3%; left:0; position: absolute; bottom:0; z-index:10; top:auto;}
        .nbt-list li.nbt-listview.lead-post.section-name .news_section a{font-size: 1.4rem; line-height: 2.2rem; color:#fff}
        .nbt-list li.nbt-listview.lead-post.section-name .con_wrap{padding-bottom: 35px}

        li.nbt-horizontalView span.article-text {vertical-align: top; text-align: left; font-size: 1.5rem; line-height: 2.0rem; margin-bottom:1%; display:inline-block; font-weight: 600;}
        li.nbt-horizontalView {margin: 0 3% 4%; padding-bottom: 4%; width:auto; position: relative;}
        li.nbt-horizontalView.video .nbt-list li.nbt-listview{width:133px; margin: 0 10px 0 0;}
        li.nbt-horizontalView.photo .nbt-list li.nbt-listview{width:133px; margin: 0 10px 0 0;}
        li.nbt-horizontalView .nbt-list li.nbt-listview:last-child{margin: 0}

        li.nbt-horizontalView .nbt-list li.nbt-listview {width:120px; vertical-align: top; margin: 0 2px 0 0; padding-bottom: 0; padding-top:0; box-sizing: border-box; display: inline-block; position: static; border:0;}
        li.nbt-horizontalView .nbt-list li.nbt-listview .img_wrap, li.nbt-horizontalView .nbt-list li.nbt-listview .tbl_column:first-child{width:100%; float:none; display: block;}
        li.nbt-horizontalView .nbt-list li.nbt-listview .con_wrap{display: block; padding:10px 0 0 0; position: static;}
        li.nbt-horizontalView .nbt-list li.nbt-listview .text_ellipsis{font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem}

        .nbt-list li.nbt-listview .img_wrap[data-tag]:after{font-family: arial; font-size: 1.1rem; position: absolute; bottom: 0; right: 0; padding: 2% 3%; line-height: 1.2rem; background: rgba(0,0,0,.6); content: attr(data-tag); text-align: center; color: #fff; z-index: 10;}

        li.moviereview-listview {border-bottom: 1px solid #ebebeb;}
        li.moviereview-listview .con_wrap {color: #333;}
        li.moviereview-listview .con_wrap .title {color: #000;}
        li.moviereview-listview .con_wrap .filled-stars.critic {color: #ff001f;}
        li.moviereview-listview .des {color: #7f7f7f;}
        li.moviereview-listview .time-caption {color: #7f7f7f;}
        li.moviereview-listview.lead-post .con_wrap {color: #afafaf;}
        li.moviereview-listview.lead-post .con_wrap .caption {color: #ffdc00;}
        li.moviereview-listview.lead-post .con_wrap .title {color: #fff;}
        li.moviereview-listview.lead-post .con_wrap .des {color: #afafaf;}
        li.moviereview-listview.lead-post .con_wrap .filled-stars.critic {color: #ff001f;}
        li.moviereview-listview {margin: 0 3% 4%; padding-bottom: 4%; width:94%; position: relative;}
        li.moviereview-listview .img_wrap{position:relative; width:35%; vertical-align:top;}
        li.moviereview-listview .img_wrap img{vertical-align:bottom; width: 100%; height: auto;}
        li.moviereview-listview .con_wrap {text-align: left; word-break: break-word; padding: 0 0 0 10px; vertical-align: top; position: relative;}
        li.moviereview-listview .con_wrap .title{font-size: 1.6rem; line-height: 2.4rem; font-weight: 600; margin: 0 0 5px; display: block;}
        li.moviereview-listview .con_wrap .cast{margin: 0 0 15px;}
        li.moviereview-listview .text_ellipsis{font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem;}
        li.moviereview-listview .des{font-size: 1.4rem; line-height: 2.2rem; display: block;}
        li.moviereview-listview .time-caption {display: block; font-size: 1.1rem; line-height: 1.2rem;}
        li.moviereview-listview .rating{margin: 0 0 5px; display: block;}
        li.moviereview-listview .rating b{display: inline-block; vertical-align: top; font-size: 1.4rem; line-height: 2.2rem; font-weight: normal; margin: 0 10px 0 0}

        li.moviereview-listview.lead-post{margin:0 0 4%; width: 100%;}
        li.moviereview-listview.lead-post .table_row .poster-bg{position: absolute; object-fit: cover; width: 100%; height:174px;}
        li.moviereview-listview.lead-post .img_wrap{width: 35%; text-align: center; display:block}
        li.moviereview-listview.lead-post .img_wrap amp-img{width:140px; height:187px; max-width: 140px; max-height: 187px; margin: 15px 15px 0; box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.1); border: 5px solid #fff;}
        li.moviereview-listview.lead-post .con_wrap{padding:15px 10px 0 0; box-sizing: border-box; width: 100%}
        li.moviereview-listview.lead-post .caption{font-size: 1.6rem; line-height: 2.0rem; margin: 0 0 5px; display: block;}
        li.moviereview-listview.lead-post .text_ellipsis{font-size: 2.0rem; line-height: 2.8rem; font-weight: normal; max-height: 5.6rem; display: -webkit-box;}
        li.moviereview-listview.lead-post .lang b{font-weight: normal; display: inline-block; font-size: 1.4rem; line-height: 2.2rem;}
        li.moviereview-listview.lead-post .lang b:after {content: "|"; margin: 0 3px;}
        li.moviereview-listview.lead-post .lang b:last-child:after{content: ''; margin: 0}

        li.moviereview-listview.lead-post, li.moviereview-listview .rating-stars{vertical-align: middle;}
        .w0{width:0%} .w5{width:5%} .w10{width:10%} .w15{width:15%} .w20{width:20%} .w25{width:25%} .w30{width:30%} .w35{width:35%} .w40{width:40%}
        .w45{width:45%} .w50{width:50%} .w55{width:55%} .w60{width:60%} .w65{width:65%} .w70{width:70%} .w75{width:75%} .w80{width:80%} .w85{width:85%}
        .w90{width:90%} .w95{width:95%} .w100{width:100%}
        .rating-stars {position: relative;vertical-align: baseline; display: inline-block;color:#afafaf;overflow: hidden;height:20px;line-height:20px;}
        .empty-stars {height: 100%; vertical-align: top;}
        .filled-stars:before, .empty-stars:before {content: "\\2605\\2605\\2605\\2605\\2605"; font-size:18px; letter-spacing: 3px;line-height: 18px;display: inline-block;word-break: normal;}
        .filled-stars {position: absolute;left: 0;top: 0;height: 100%;overflow: hidden;}

        li.nbt-horizontalView.app-exclusive{background: #121212; margin: 0 0 4%; padding: 0 3% 7%; width:100%; box-sizing: border-box;}
        li.nbt-horizontalView.app-exclusive ul.nbt-list {white-space: nowrap;}
        li.nbt-horizontalView.app-exclusive .app-txt{font-size: 1.2rem; display: inline-block; width: 25%; background: #fed20a; color: #000; margin: auto; right: 0; left: 0; position: absolute; bottom: 14px; height: 27px; line-height: 31px; border-radius: 20px; text-align: center; padding: 0 10px; white-space: nowrap;}
        li.nbt-horizontalView.app-exclusive span.article-text {font-size: 1.4rem; line-height: 2.2rem; margin: 3px 0 0; color:#fff;}

        .trending_keywords{background:#ebebeb;margin:5% 0 4%;padding:6% 3% 2%;position:relative;width:100%;}
        .trending_keywords h3{background:#e5821e;color:#fff;border-radius:20px;padding:0 20px;height:28px;line-height:30px;display:inline-block;position:absolute;top:-15px;left:0;right:0;margin:auto;max-width:70%;text-align:center;font-size:1.4rem; overflow:hidden}
        .trending_keywords .nowarp_content{white-space:nowrap;}
        .trending_keywords .nowarp_content a{margin:2px 5px 5px 2px;font-size:1.1rem;border-radius:20px;padding:0 20px;height:28px;line-height:30px;display:inline-block;color:#716f6f;border:1px solid #cbcbcb;background:#fff;}
        .mobile_body .view-horizontal{flex-wrap: nowrap; overflow-x: auto;}
        .mobile_body .view-horizontal li.news-card.vertical{margin-right:15px}
        .mobile_body .view-horizontal li.news-card.vertical .img_wrap img, .mobile_body .view-horizontal li.news-card.vertical .img_wrap amp-img{width:200px: height:111px}
        .section-wrapper .top-news-content{padding-top:4%;}
        .section-wrapper .list-slider{margin:0 3%;width:100%;}
        .section-wrapper .list-slider .view-horizontal{padding:0;}
        .section-wrapper .box-item .view-horizontal{margin:0 3%;}
        .section-wrapper .top-news-content li:nth-child(3) {border-top: 1px solid #dfdfdf; padding-top: 4%;}
        .section-wrapper .top-news-content li.list-slider ul li:nth-child(3){border-top: 0; padding-top: 0;}
        .section-wrapper .top-news-content .news-card.lead.col6 .img_wrap a:before {width: 32px; height: 32px;}
        .section-wrapper .btn-subscribe {width: 100%; margin: 0 3%;}
        .section-wrapper .btn-subscribe .youtube-iframe {margin-left: auto;}

        .wdt_top_section{background:#eee;}
        .wdt_next_sections{background:#fff}
        .wdt_top_section, .wdt_next_sections{padding:2% 0 4%;margin:0 0 4%;}
        .wdt_top_section .top_section, .wdt_next_sections .top_section{margin:0}
        .wdt_top_section .section, .wdt_next_sections .section{background:transparent;}
        .wdt_top_section .section h2, .wdt_next_sections .section h2{border-bottom:0;margin:0;}
        .wdt_top_section .section h2 span, .wdt_next_sections .section h2 span{background:transparent;padding:0;color:#000;}
        .wdt_top_section .section h2 span:before, .wdt_next_sections .section h2 span:before{border:0;}
        .wdt_top_section .section h1, .wdt_next_sections .section h1{border-bottom:0;margin:0;}
        .wdt_top_section .section h1 span{background:transparent;padding:0;color:#000;}
        .wdt_next_sections .section h1 span{background:transparent;padding:0;color:#000;}
        .wdt_top_section .section h1 span:before, .wdt_next_sections .section h1 span:before{border:0;}
        .wdt_top_section .section .read_more, .wdt_next_sections .section .read_more{display:none;}
        .wdt_top_section ul li.lead .con_wrap, .wdt_next_sections ul li.lead .con_wrap{margin:10px 0 0;}
        .wdt_top_section ul li.vertical, .wdt_next_sections ul li.vertical{margin:0 3% 0 0;}
        .wdt_top_section ul li.vertical .img_wrap img, .wdt_next_sections ul li.vertical .img_wrap img{width:127px;}
        .wdt_top_section.video, .wdt_next_sections.video{background:#282e34;}
        .wdt_top_section.video .section h2 a, .wdt_next_sections.video .section h2 a, .wdt_top_section.video .section h1 a, .wdt_next_sections.video .section h1 a, .wdt_top_section.video .section ul.sub-list li a, .wdt_next_sections.video .section ul.sub-list li a{color:#fff;}
        .wdt_top_section.video .con_wrap .text_ellipsis, .wdt_next_sections.video .con_wrap .text_ellipsis{color:#fff;}

        .mobile_body .wdt_sections {margin-bottom: 4%; padding: 0 3%;}
        .mobile_body .wdt_sections ul li {padding: 0 5px; margin-bottom:20px}
        .mobile_body .wdt_sections .section .top_section h1 {height: auto;}
        .mobile_body .wdt_sections .section .top_section h1 span {height: auto; line-height: 2.8rem; padding: 10px 10px 5px 0;}

        .photo_video_section{background:#282e34;padding:2% 0 4%;margin:0 0 4%; display: flex; width: 100%;}
        .photo_video_section .section{background:transparent;}
        .photo_video_section .section h2{border-bottom:0;margin:0;}
        .photo_video_section .section h2 span{background:transparent;padding:0;color:#fff;}
        .photo_video_section .section h2 span a{color:#fff;}
        .photo_video_section .section h2 span:before{border:0;}
        .photo_video_section .section .top_section .read_more{display:none;}
        .photo_video_section .section .top_section{margin:0}
        .photo_video_section .section .sub-section{margin:0 0 15px;}
        .photo_video_section .section .sub-section .sub-list li a{color:#fff;}
        .photo_video_section ul li.news-card.vertical{margin-right:4%;}
        .photo_video_section ul li.news-card.vertical .img_wrap img, .photo_video_section ul li.news-card.vertical .img_wrap amp-img{width:250px;}
        .photo_video_section ul li.news-card.vertical .con_wrap .text_ellipsis{color:#fff;}
        .photo_video_section ul li.news-card.vertical .con_wrap .section_name{display: none;}
        .photo_video_section.video ul li.news-card.vertical{margin-right:4%;}
        .photo_video_section.video ul li.news-card.vertical .img_wrap img{width:200px;}
        .photo_video_section.video ul li.lead .con_wrap{margin:10px 0 0 0;}
        .photo_video_section.video ul li.lead .con_wrap .text_ellipsis{color:#fff;}

        .movielisting .news-card.horizontal{border-bottom:1px solid #dfdfdf;padding-bottom:4%;margin-bottom:4%;}
        .movielisting .news-card.horizontal .img_wrap img, .movielisting .news-card.horizontal .img_wrap amp-img{width:120px;}
        .movielisting .news-card.horizontal .con_wrap {width:100%;}
        .movielisting .news-card.horizontal .con_wrap span{display:block;margin-bottom:5px;word-break:break-word;}
        .movielisting .news-card.horizontal .con_wrap .title{font-size: 1.6rem; line-height: 2.4rem; max-height: 4.8rem;-webkit-line-clamp:2;font-weight:bold;}
        .movielisting .news-card.horizontal .con_wrap .cast{font-size: 1.4rem; line-height: 2.2rem; max-height: 6.6rem; display: -webkit-box;}
        .movielisting .news-card.horizontal .con_wrap .cast small{display:inline-block;vertical-align:top;font-weight:bold;margin-right:5px;font-size:1.4rem;}
        .movielisting .news-card.horizontal .con_wrap .rating{display: block; margin-bottom: 5px; overflow: hidden;}
        .movielisting .news-card.horizontal .con_wrap .rating b{font-size: 1.2rem; line-height: 2.0rem; font-weight: normal; float: left; width: 45%; margin-right: 3%;}
        .movielisting .news-card.horizontal .con_wrap .rating .rating-stars{display:inline-block;vertical-align:top;margin-right:5px;}
        .movielisting .news-card.horizontal .con_wrap .rating .rating-stars .filled-stars.critic{color:#ff001f;}
        .movielisting .news-card.horizontal .con_wrap .rating .rating-stars .filled-stars.user{color:#00b3ff}
        .movielisting .news-card.horizontal .con_wrap .rating .rate_val {display: none;}
        .movielisting .news-card.horizontal .con_wrap .desc{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;margin-bottom:10px;}
        .movielisting .news-card.horizontal .con_wrap .des{font-size:1.4rem;line-height:2.2rem;color:#7f7f7f;}
        .movielisting .news-card.horizontal .con_wrap .time-caption{font-size:1.2rem;line-height:2rem;}

        .al-trending{background: #fff; padding-bottom: 4%; margin-top: -4%;}
        .al-trending .trending_keywords{margin: 0;}
        .mobile_body .wdt_sections.video ul {padding: 0;}
        .news-card.horizontal.app-exclusive {background: #efefef; margin: 0 0 4%; padding: 3% 3% 35px; border-radius: 0; box-shadow: 0 1px 3px 0 rgba(0,0,0,.5); position: relative; border:0}
        .news-card.horizontal.app-exclusive .con_wrap .text_ellipsis {max-height: 4.2rem; -webkit-line-clamp: 2;}
        .news-card.horizontal.app-exclusive .app-txt {font-size: 1.2rem; display: inline-block; width: 25%; background: #fed20a; color: #000; right: 2%; position: absolute; bottom: 7px; height: 25px; line-height: 25px; border-radius: 20px; text-align: center; padding: 0 5px; white-space: nowrap; font-family: arial;}

        .mobile_body .trending-bullet{text-align:left;border:0;margin:0 3% -5px;padding-top:0;padding-bottom:0;max-width:94%;}
        .trending-bullet h3, .list-trending-now h3{font-size:1.8rem;margin:0 0 10px;padding-top:3px;font-weight:bold;display:block;height:40px;position:relative;border-bottom:2px solid #dfdfdf;width:100%;text-align:left;}
        .trending-bullet h3 span, .list-trending-now h3 span{height:40px;line-height:40px;padding:0 10px 0 0;display:inline-block;position:relative;font-weight:bold;color:#000;}
        .trending-bullet h3 span:before, .list-trending-now h3 span:before{content:'';position:absolute;bottom:-2px;left:0;right:0;border-bottom:2px solid #000;}
        .trending-bullet a{font-size:1.1rem;line-height:1.9rem;color:#000;margin:0 0 5px 0;display:inline;vertical-align:top;font-weight:bold;}
        .trending-bullet a:after{margin:0 5px;content:'|';font-size:.9rem;vertical-align:top;}
        .trending-bullet a:last-child:after {margin: 0; content: '';}

        .list-trending-now{margin:0 3% -5px;max-width:94%;}
        .list-trending-now a{font-size:1.1rem;line-height:1.9rem;padding:5px 15px 3px;color:#000;display:inline-block;vertical-align:top;border-radius:15px;margin:0 10px 10px 0;background:#eeeeee;}
        .news-card.horizontal.banner-image a {display: block; width: 100%;}
        .news-card.horizontal.banner-image img, .news-card.horizontal.banner-image amp-img{max-width: 100%; height: auto; max-height: 120px;}
        .news-card.horizontal.wdt_data_drawer{margin:0; padding:0; border:0}
        .news-card.horizontal.webstories.midthumb .img_wrap a {display: block; max-height: 60px; overflow: hidden;}
        .slidercomp .slider {white-space:nowrap; overflow:auto; width:auto; padding:0 10px;}
        .slidercomp .slider .slider_content li {display:inline-block; white-space:normal;vertical-align:top;}
        .slidercomp {width: 100%;box-sizing: border-box;background-color: #282e34;margin-bottom: 10px;padding-bottom: 10px;}
        .slidercomp .section {padding:0;}
        .slidercomp .section h2 { border-bottom: none;}
        .slidercomp .section h2 a {color: #fff;}
        .slidercomp .section h2 span {padding-top:3px;}
        .slidercomp .section h2 span:before {display: none;}
        .slidercomp .section .top_section .read_more {color: #fff;}
        .slidercomp .section .top_section .read_more:after {border-top-color: #fff; border-right-color: #fff;}
        .slidercomp .slider .text_ellipsis {color: #fff; font-size: 1.2rem;line-height: 2rem;max-height: 6rem;-webkit-line-clamp: 3;}
        .slidercomp .slider .con_wrap {margin: 10px 0 0; display:block;}
        .articlelist .top_atf {margin: 0 3% 4%; padding-bottom: 2%; border-bottom: 1px solid #dfdfdf;}
        .articlelist .top_atf .caption {font-size: 1.4rem; line-height: 2.2rem; -webkit-line-clamp: initial;}
        .articlelist .btf_con{font-size: 1.4rem; line-height: 2.4rem; margin: 0 0 4%; padding: 4% 3% 4%; background: #FFF;}

        .btn_openinapp, .btn_openinapp:visited{z-index:2;color:#fff;padding:0 15px 0 13px;text-decoration:none; font-size:14px;border-radius:0 25px 25px 0;background:#AD0000;position:fixed;bottom:210px;margin:auto;left:0;font-weight:normal;transition:all 350ms ease-in-out;height:30px;line-height:32px;white-space:nowrap;}
        .btn_openinapp.float-amz {background: #fff; box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.2); height: 45px; line-height: 47px; padding: 0 8px 0 10px;}
        .btn_openinapp.float-amz span{display:inline-block;vertical-align:top;}
        .btn_openinapp.float-amz span.txt{text-transform:uppercase;color:rgba(255, 166, 0, 0.932);font-size:14px;position:relative;width:55px;font-weight:bold;text-align:left;padding:8px 0 0 2px;box-sizing:border-box;height:45px; font-family:arial}
        .btn_openinapp.float-amz span.txt:after{content:'';position:absolute;top:10px;left:0;width:50px;height:12px;background:url(https://navbharattimes.indiatimes.com/photo/58606011.cms) no-repeat;background-size:contain;}
        .btn_openinapp.float-amz span.img{margin:4px 0 0 0;}
        .btn_openinapp.float-amz span.img img, .btn_openinapp.float-amz span.img amp-img{width:36px; height:36px; vertical-align:top}
    `;
  const videolistCss = `
        .nbt-list li.nbt-listview.videolistview.lead-post{margin:0 0 4%; padding: 0}
        .videolist-container .nbt-list li.nbt-listview.lead-post .img_wrap{min-height: 178px}
        li.nbt-listview.videolistview .time-caption {position: static;}
        li.nbt-listview.videolistview {display: inline-block; width:45.9%; min-height: 100px; vertical-align: top; margin: 0 0 4% 0; box-sizing: border-box; padding:0; border:0}
        li.nbt-listview.videolistview a.table_row{display:block}
        li.nbt-listview.videolistview .img_wrap{display:block; width: 100%}
        li.nbt-listview.videolistview .con_wrap{display:block; padding: 10px 0 0;}
        
        @media screen and (max-width:450px) {
        li.nbt-listview.videolistview:nth-child(2n+2) {padding-right: 4px; margin-left: 4%}
        li.nbt-listview.videolistview:nth-child(2n+3) {padding-left: 4px; margin-right: 4%}
        }
        
        @media screen and (min-width:451px) {
        li.nbt-listview.videolistview  {width: 31%; margin: 0 0 4% 0}
        li.nbt-listview.videolistview:nth-child(3n+2) {padding-right: 4px; margin-left: 3%}
        li.nbt-listview.videolistview:nth-child(3n+3) {padding: 0 2px}
        li.nbt-listview.videolistview:nth-child(3n+4) {padding-left: 4px; margin-right: 3%}
        }

        li.videoshowview .default-outer-player{display:table; width:100%}
        .videolist .con_social{margin-right: 3%;}
        .videolist .con_social .youtube-iframe {margin-left: auto; padding: 0 0 3% 0;}
    `;

  const posterContentCss = `
        .poster-content{margin: 0 0 4%;background: #fff;}
        .pos_section{width:100%; height:200px; position: relative;}
        .pos_section amp-img{position:initial}
        .pos_section .poster-bg{position: absolute; object-fit: cover; width: 100%; height:200px; z-index: 0}
        .pos_section .pos_title{position: absolute; top:10px; left:10px; font-size: 1.6rem; line-height:2.4rem; z-index: 1; color: #fff;}
        .pos_section .pos_text{position: absolute; top: 60%; transform: translateY(-60%); left: 0; right: 0; margin: 0 2%; z-index: 1; text-align: center}
        .pos_section h3{font-size: 2.5rem; font-weight: normal; text-align:center;color: #fff;}
        .pos_section .pos_more{font-size: 2.0rem; border-radius:10px; margin: 10px auto 0; width: auto; padding: 7px 20px 1px; display: inline-block; background: #fed20a; border: 1px solid #fed20a; color: #000;}
    `;
  const serviceDrawerCss = `
        .service_drawer{white-space: nowrap; margin:0 0 4%;}
        .item_drawer{padding: 4%; margin: 0 0 0 4%; position: relative; background: #fff; text-align: right; display: inline-block; min-width: 70%; vertical-align: top; min-height: 100px}
        .item_drawer:last-child{margin-right:4%}
        .item_drawer:only-child{display: block; width: auto;}
        .item_drawer .label {font-size: 1.8rem; line-height: 2.6rem; margin: 0 0 10px; white-space: normal;}
        .item_drawer .label h4{font-size: 2.0rem; line-height: 2.8rem; margin: 0 0 5px}
        .item_drawer .label img{width: 50px; display: inline; vertical-align: baseline; margin: 0 2px 0;}
        .con_chromealert.item_drawer .label amp-img{width: 50px; height: 16px; display: inline-block; top: 3px;}
        .con_chromealert.item_drawer .label img{min-height: initial; min-width: initial; height: auto; z-index: 1;}
        .item_drawer .text_email{margin: 0 0 10px}
        .item_drawer .text_email input{font-size: 1.2rem; padding: 5px 5px 3px; width:55%}
        .item_drawer .bg_circle{height: 65px; width:65px; border-radius: 50%; display: inline-block; position: absolute; left:15px; bottom: 15px; border: 1px solid #c7c7c7; background: #f4f4f4; text-align:center;}
        .con_facebooklike.item_drawer .bg_circle amp-img{position:absolute;top:0; bottom:0; left:0; right:0; margin:auto;}
        .con_facebooklike.item_drawer .bg_circle img{width:50px; height:auto; min-width:initial; min-height:initial;}
        .item_drawer .phone_icon{width: 22px; height: 41px; display: inline-block;}
        .item_drawer img.portal_icon{position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto; width: 51px;}
        .item_drawer .btn-draweralert{border-radius: 4px; font-size: 1.6rem; height: 28px; line-height: 32px; padding: 0 4%; display:inline-block; border:1px solid #717171; background:#fff; color:#333;}
        .item_drawer.con_newsletter .label{margin: 0 0 5px}
    `;
  const resultHome = `
        .wdt_election_result{padding:5% 3%; position:relative;}
        .wdt_election_result h1 , .wdt_election_result h2{font-size: 1.6rem;line-height: 2.4rem; text-transform: uppercase; margin: 0 0 15px; color:#fff;}
        .wdt_election_result h2 a{font-size: 1.6rem;line-height: 2.4rem; text-transform: uppercase; margin: 0 0 15px; color:#fff;}
        .tabs_widget{position:relative; min-height:30px}
        .tabs_widget.tabs_big{display: block}
        .tabs_widget.tabs_big ul li{display: inline-block; vertical-align: top; border: 1px solid #e2e2e2; border-radius: 2px; width:130px; text-align: center;}
        .tabs_widget.tabs_big ul li a{font-size: 1.2rem; text-transform: uppercase; color: #fff; display:block; height: 28px; line-height: 28px; font-family: arial; padding: 0 15px; white-space: nowrap;}
        .tabs_widget.tabs_big ul li.active{border: 1px solid #e2e2e2; margin: -5px -1px 0;}
        .tabs_widget.tabs_big ul li.active a{background: #e2e2e2; color: #000; font-weight: bold; height: 38px; line-height: 38px;}
        
        .tabs_widget.tabs_normal {margin-top: 10px;display: block;}
        .tabs_widget.tabs_normal ul {width: 100%; overflow: hidden; overflow-x: auto; height: auto; white-space: nowrap;}
        .tabs_widget.tabs_normal ul li{display: inline-block; vertical-align: top; text-align: center; margin: 0 20px 0 0;}
        .tabs_widget.tabs_normal ul li:last-child{margin-right: 0}
        .tabs_widget.tabs_normal ul li a{font-size: 1.2rem; text-transform: uppercase; color: #fff; display:block; height: 28px; line-height: 28px; font-family: arial; background:transparent; white-space: nowrap;}
        .tabs_widget.tabs_normal ul li.active a{color: #fff; font-weight: bold; border-bottom: 2px solid #fff;}
        
        .tabs_widget.tabs_normal ul.tabs_select_box {width: 200px; white-space: initial; height: 0;border: 1px black solid;position: absolute; padding: 25px 10px 0; top: 0; z-index: 9; padding-right: 35px; background:#fff; left:0;}
        .tabs_widget.tabs_normal ul.tabs_select_box li.active {position: absolute; top: 0; left: 0; right: 0; text-align: left; padding: 0; margin:0}
        .tabs_widget.tabs_normal ul.tabs_select_box li {display: block; text-align:left; margin:0}
        .tabs_widget.tabs_normal ul.tabs_select_box li.active a{padding:0 0 0 10px}
        .tabs_widget.tabs_normal ul.tabs_select_box li a{height:25px}
        .wdt_election_result .tabs_widget.tabs_normal ul.tabs_select_box li.active a{border-bottom:1px solid #000}
        .tabs_widget.tabs_normal input.toggleCheckBox {display:none;}
        .tabs_widget.tabs_normal input.toggleCheckBox:checked ~ .tabs_select_box { height: auto}
        
        .chart_container{margin:100px 0 0; display: block; position: relative;}
        .chart_container span.liveTicker{color:#c33;font-weight:600;text-transform:uppercase;font-size:1.4rem;line-height:1;position:absolute;margin:2px 0 0; top:-5px; left: 4%;}
        .svg-chart-data {color: #fff; font-size: 1.8rem; text-align: center; margin: -80px auto 35px; font-family: arial;}
        .svg-chart-data h3 {color:#fff; font-weight: 700; border-bottom: 1px solid #fff; display: inline-block; margin:0; padding:0}
        .svg-chart-data h4 {color:#fff; font-weight: 500;; margin:0; padding:0}
        .svg-chart-data h5 {color:#fff; font-size: 1.0rem; color: #ccc; font-weight: 200; text-transform: uppercase; margin:0; padding:0}
        .chart_container.exit-poll{overflow:hidden}
        .chart_container.exit-poll svg{width:100%; height:100%;}
        .highcharts-contextbutton{display:none}
        .chart_container.exit-poll .chart-detail {font-size: 12px; position: absolute; white-space: nowrap; visibility: visible; margin-left: 0px; margin-top: 0px; left: calc(50% - 58px); top: calc(50% - 36px);}
        .chart_container.exit-poll .svg-chart-data{text-align: center; box-shadow: 1px 1px 18px #777; width: 120px; height: 120px; border-radius: 50%; background: #fff; font-size: 18px; line-height: 1; margin: 0 auto;}
        .chart_container.exit-poll .svg-chart-data h3{font-size: 1.8rem; padding-top: 36px; padding-bottom: 2px;}
        .chart_container.exit-poll .svg-chart-data h4{font-size: 1.8rem; font-weight: bold; padding: 5px 0 0 0;}
        .wdt_majority{font-size:1.0rem; color:#999; position:relative; margin:10px 0 10px 0; text-transform: uppercase;}
        .wdt_majority strong{font-weight:bold; font-family:arial}
        .wdt_majority span:last-child{position:absolute;right:0}

        .result_table_widget{display:block; background: #f5f5f5; margin-bottom: 4%}
        .result_table_widget table{table-layout: fixed; width: 100%; border-collapse: collapse; margin-bottom: 4%;}
        .result_table_widget table tr td{font-size: 1.0rem; color: #000; text-transform: uppercase; background: #fff; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; padding: 10px; border: 2px solid #fff; vertical-align: middle;}
        .result_table_widget table tr td:first-child{text-align: left; width: 40%; border-radius: 8px 0 0 8px;}
        .result_table_widget table tr:not(:first-child) td:nth-child(2){font-weight: bold; font-size: 1.8rem;}
        .result_table_widget table tr td:nth-child(2){color: #fff; position: relative; z-index: 1;}
        .result_table_widget table tr td:last-child{border-radius: 0 8px 8px 0;}
        .result_table_widget table tr.dataCell td{font-size: 1.4rem; color:#fff; font-weight: normal;}
        .result_table_widget table tr.total td{color:#666666; padding-left: 0}
        .result_table_widget table tr.total td span{font-weight: normal;}
        .result_table_widget table tr.total td:last-child{text-align: right; padding-right: 0;}
        .result_table_widget .wdt_btn{margin-bottom: 0; text-align: center}
        .result_table_widget .wdt_btn .btn_result{width: 46%; text-transform: uppercase; text-align: center; font-size: 1.2rem; font-family: Arial, Helvetica, sans-serif; color: #000; border-radius: 2px; height: 32px; line-height: 34px; margin-right: 4%; display: inline-block; border: 1px solid #000; background: #fff; outline: none; font-weight: bold;}
        .result_table_widget .wdt_btn .btn_result:last-child{margin: 0;}
        .result_table_widget .wdt_note{font-size: 1.0rem; color: #999; margin:10px 0;}

        .wdt_election_result .result_table_widget {padding: 4%; border-radius: 4px;}
        h2.election_h2, .inner_box_widget h2 {font-size: 1.6rem; text-transform: uppercase; color: #000;}
        .wdt_btn{margin-bottom: 4%; text-align: center}
        .wdt_btn .btn_result{width: 46%; text-transform: uppercase; text-align: center; font-size: 1.2rem; font-family: Arial, Helvetica, sans-serif; color: #000; border-radius: 2px; height: 32px; line-height: 34px; margin-right: 4%; display: inline-block; border: 1px solid #000; background: #fff; outline: none; font-weight: bold; white-space: nowrap;}
        .wdt_btn .btn_result:last-child{margin: 0;}
        .wdt_note{font-size: 1.0rem; color: #999; margin:10px 0;}
        .state_wise_widget .scroll_content{white-space: nowrap; margin: 3% 0 0;}
        ul.state-column{display: inline-block; width: 20%; margin: 20px 3% 0 0; vertical-align: top; position: relative;}
        ul.state-column:empty {display: none;}
        ul.state-column:last-child{margin-right:0}
        ul.state-column li{display: block; font-size: 1.2rem; color: #fff; text-align: center; padding: 4px 5px; margin:0 0 5px; border-radius: 3px; font-family: arial; white-space: normal}
        ul.state-column li:nth-last-child(1){margin:0}
        ul.state-column li.stateName{color: #000; padding: 4px 0; position: absolute; top: -22px; left: 0; right: 0; margin: 0;}
        ul.state-column li.stateName span:first-child, ul.state-column li.stateName span:last-child{display:inline; padding:0; font-size: 1.2rem; line-height: 1.2rem;}
        ul.state-column li span{display: block;}
        ul.state-column li span:first-child{border-bottom: 1px solid #efefef; margin: 0 0 3px; padding: 0 0 3px;}
        ul.state-column li span:nth-child(2){font-size: 14px; line-height: 14px;}
        .inner_box_widget {display: block; background: #f5f5f5; border-radius: 4px; padding: 4%; margin: 0 0 4%;}
        
        .state_wise_widget.power_state_wise {background:#eee;}
        .state_wise_widget.power_state_wise .scroll_content{overflow:hidden; margin:0}    
        .state_wise_widget.power_state_wise ul.state-column{width: auto; margin: 25px 0 25px; display: block; padding: 0 0 25px; border-bottom: 1px solid #dadada;}
        .state_wise_widget.power_state_wise ul.state-column:last-child{border:0; padding:0; margin-bottom:0}
        .state_wise_widget.power_state_wise ul.state-column li{display: inline-block; vertical-align: top; margin: 0 5% 0 0; width: 21%; padding:0; overflow:hidden; border-radius: 0;}
        .state_wise_widget.power_state_wise ul.state-column li span:first-child{border:0; margin:0}
        .state_wise_widget.power_state_wise ul.state-column li span:last-child{background:#fff; color:#000; margin:0 1px 1px}
        .state_wise_widget.power_state_wise ul.state-column li span{padding:6px 5px}
        .state_wise_widget.power_state_wise ul.state-column li:last-child{margin-right:0}
        .state_wise_widget.power_state_wise ul.state-column li.stateName{position:absolute; top:-16px; right:3px; position: absolute; top: -20px; right: 3px; left: 0; text-align: right; width: auto; font-size:1.4rem}
        .state_wise_widget.power_state_wise ul.state-column li.stateName span{background: transparent; margin: 0; padding:0;  font-size:1.4rem; line-height:1.4rem;}
        .state_wise_widget.power_state_wise ul.state-column li.stateName span:first-child{position:absolute; left:0; font-weight:bold;}    

        .result_table_widget table tr td:nth-child(2):after{background: rgba(0, 0, 0, 0.7); content: ''; top:-2px; bottom: 0; left: 0; right: 0; position: absolute; z-index: -1;}
        .right-open-in-app, .right-open-in-app:hover, .right-open-in-app:visited{color: #fff; font-size: 10px; text-transform: uppercase; display: inline-block; width: 42px; text-align: center; background: #E55E40; border-radius: 2px; padding: 4px 0 3px; font-family: arial; position: absolute; right: 3%; top: 57px; outline:none}
        .right-open-in-app span{display: block; margin-bottom: 1px;}
        .svg-chart-data {color: #fff; font-size: 1.8rem; text-align: center; margin: -80px auto 35px; font-family: arial;}
        .svg-chart-data h3 {color:#fff; font-weight: 700; border-bottom: 1px solid #fff; display: inline-block; margin:0; padding:0}
        .svg-chart-data h4 {color:#fff; font-weight: 500;; margin:0; padding:0}
        .svg-chart-data h5 {color:#fff; font-size: 1.0rem; color: #ccc; font-weight: 200; text-transform: uppercase; margin:0; padding:0}
        .white-bg .tabs_widget.tabs_big ul li a{color:#000}
        .white-bg h1 , .white-bg h2{color:#000;} 
        .white-bg .svg-chart-data h4{color:#000}
        .white-bg .svg-chart-data h3{border-bottom:1px solid #000; color:#000;}
        .white-bg .svg-chart-data h5{color:#333}
        .white-bg .tabs_widget.tabs_normal ul li a{color: #000;}
        .white-bg .tabs_widget.tabs_normal ul li.active a{color:#000; border-bottom:2px solid #000}

        .result_inner {position: relative;text-align: center;width: 100%;margin: 0 auto;background: #fff;}
        .result_inner h2 {color: #1a1a1a; padding-top: 10px; text-transform:uppercase; font-size:15px}
        .elm_container{padding: 2.5%; background:#fff}
        .el_oops.hide{display:none}
        .refresh_btn {width: 150px;line-height: 25px;font-size: 16px;background: #be2819;padding: 10px 5px;text-align: center;margin: 20px auto 10px auto;}
        .refresh_btn a {color: #fff;}
        .refresh_btn .ex_res {width: 25px;vertical-align: text-bottom;margin-right: 10px; display:inline-block}
        .ctyLoaderCont{text-align:center;}
        .ctyLoaderCont img{margin: 25% auto; width: 30px;}
        .result_inner .btn_group {text-align: center; padding: 15px 0 5px;}

        .wdt_election_result{background: #222; padding:5% 3%; position:relative;}
        .wdt_election_result h1 , .wdt_election_result h2{font-size: 1.6rem;line-height: 2.4rem; text-transform: uppercase; margin: 0 0 15px; color:#fff;}
        .horizontal-table{text-align:center}
        .horizontal-table ul.state-column{width: auto; margin: 0 0 20px; display: block;}
        .horizontal-table ul.state-column li{display: inline-block; vertical-align: top; margin: 0 3% 3% 0; width: 15%;}
        .horizontal-table ul.state-column li:last-child{margin-right:0}
        .horizontal-table ul.state-column li:first-child:nth-last-child(5), .horizontal-table ul.state-column li:first-child:nth-last-child(5) ~ li {width: 14%;}

        .chart-horizontal{white-space: nowrap; overflow-y: scroll;}
        .chart-horizontal table {
            font-size: 12px;
            text-align: left;
            font-weight: bold;
            text-transform: uppercase;
            width: 96%;
            margin: 2%;
            box-shadow: 0px 1px 9px rgba(0, 0, 0, 0.2);}
        .chart-horizontal table tr {border-top: none;}
        .chart-horizontal table td.party .seats {color: #cc3333;}
        .chart-horizontal table td.state .seats {color: #868686; font-size: 1.1rem;}
        .election_head{padding: 3% 2% 0; display:block; overflow:hidden}
            .election_head h2 {float: left; font-size: 1.6rem; line-height: 1.4rem; text-transform: uppercase; margin: 0 5px 0 0; width:110px; word-break: break-all; white-space: normal;}
            .election_head h2 a {color: #000;}
            .election_head h2 b {color: #000; font-size: 1.6rem; line-height:2.4rem; display: block;}
            .election_head h2 span {color: #333; font-size: 10px; display: inline-block; width: 100%; box-sizing: border-box; text-align: left; border-radius: 2px; padding: 0px;
            line-height: 17px;}
            .election_head h2 a i {font-style: inherit; line-height: 1.6rem; display: block; color: #000; font-size: 1.4rem; margin-top: 2px;}
        .election_con table td {border-bottom: 1px solid #dcdcdc; padding: 10px 2%;}
    `;

  const electionnewcss = `
        .elm_container, .result-widget {background:#fff;}
        .graph-container, .table-chart, .table-container {width:100%}
        .result-tbl .tbl_heading {font-weight: normal;display: table-row;background-color: #000;text-align: left;line-height: 1.4rem;font-size: 1.3rem;color: #fff;text-transform: uppercase;}
        .result-tbl .tbl_row {display: table-row;text-align: left;color: #333;}
        .result-tbl .tbl_row:nth-child(even) {background-color: #e6ecee;}
        .result-tbl .tbl_row:last-child {background: transparent; font-weight: bold;}
        .result-tbl .party_logo amp-img {margin-right: 6px;}
        .result-tbl .party_logo amp-img, .result-tbl .party_logo img {width: 32px;float: left;}
        .result-tbl {margin-bottom: 0; display: table; width: 100%; border-collapse: collapse; margin: 20px auto 10px; font-size: 1rem; background: #fff;}
        .result-tbl .tbl_heading .tbl_col {line-height: 14px; font-size: 12px; text-align: center; display: table-cell; vertical-align: middle; text-transform: uppercase;}
        .result-tbl .tbl_heading .tbl_col.first { text-align: left;padding-left: 4%;}
        .result-tbl .tbl_col {text-align: center; line-height: 22px; font-size: 13px; display: table-cell; padding: 5px 2%; border-bottom: 1px solid #dcdcdc;}
        .result-tbl .tbl_col.party_logo {text-align: left;width: 45%;text-transform: uppercase; font-weight: bold;}
        .exitpoll-widget {border: 1px solid #d4d4d4;margin: 0 0 20px;width: auto;}
        .exitpoll-widget .exitpoll_table_container {padding:0;}
        .exitpoll-widget .section {width: 100%;padding: 0;}
        .exitpoll-widget .section .top_section {margin: 0;}
        .exitpoll-widget .section .top_section h2, .exitpoll-widget .section .top_section h1 {background: #1a1a1a; color: #fff; padding: 0px 10px; font-size: 20px; border: none; box-sizing: border-box;}
        .exitpoll-widget .section .top_section h2 a, .exitpoll-widget .section .top_section h1 a, .exitpoll-widget .section .top_section h2 span, .exitpoll-widget .section .top_section h1 span {color: #fff;}
        .exitpoll-widget .section .top_section h2 a:before, .exitpoll-widget .section .top_section h1 a:before, .exitpoll-widget .section .top_section h2 span:before, .exitpoll-widget .section .top_section h1 span:before {display: none;}
        .exitpoll-widget .table-chart {display:inline-block; width: 300px; margin-right: 10px; vertical-align:top;}
        .powerbycont { font-size:13px; margin-bottom: 10px; color:#333; text-transform:uppercase;text-align:center;}
        .chart-horizontal{white-space: nowrap; overflow: auto;}
        .combinedchartbox, .tablecount {width:100%;}
        .result-widget .read_more, .exitpoll-widget .read_more, .election-as-widget .read_more {display: none;}
        .tablecomp, tablecomp.hide {width:100%; display:block;}
        .mobile_body .result_inner .tbl_txt, .results_container .tbl_txt {background:#e6ecee; padding:10px 0; text-align:center;font-size:1.2rem; margin:0;}
        .nodatawidget {padding: 40px 0; text-align: center; font-size: 18px; font-weight: bold;color: #666666; box-sizing:border-box; width:100%;}
        .election-as-widget .table.container, .election-as-widget .section, .election-as-widget .top_section {padding:0; margin:0;}
        .election-as-widget .top_section a {color:#000;}
        .result_inner .section h1, .result_inner .section h2, .result_inner .section h3 {height: auto; padding: 7px 0 0;}
        .result_inner .section h1 a, .result_inner .section h2 a, .result_inner .section h3 a, .result_inner .section h1 span, .result_inner .section h2 span, .result_inner .section h3 span {height: auto; line-height: normal;}
  `;

  const electionshpwidget = `
        .graph-container, .table-chart, .table-container, .tablecomp, .tablecount, .combinedchartbox {width:100%}
        .result-tbl .tbl_heading {font-weight: normal;display: table-row;text-align: left;line-height: 1.4rem;font-size: 1.3rem;color: #000;text-transform: uppercase;}
        .result-tbl .tbl_row {display: table-row;text-align: left;color: #333;}
        .result-tbl .tbl_row:last-child {font-weight: bold;}
        .result-tbl .party_logo amp-img {margin-right: 6px;}
        .result-tbl .party_logo amp-img, .result-tbl .party_logo img {width: 32px;float: left;}
        .result-tbl {margin-bottom: 0; display: table; width: 100%; border-collapse: collapse; margin: 20px auto 10px; font-size: 1rem; background: #fff;}
        .result-tbl .tbl_heading .tbl_col {line-height: 14px; font-size: 12px; text-align: center; display: table-cell; vertical-align: middle; text-transform: uppercase;}
        .result-tbl .tbl_heading .tbl_col.first { text-align: left;padding-left: 4%;}
        .result-tbl .tbl_col {text-align: center; line-height: 22px; font-size: 13px; display: table-cell; padding: 5px 2%; border-bottom: 1px solid #dcdcdc;}
        .result-tbl .tbl_col.party_logo {text-align: left;width: 45%;text-transform: uppercase; font-weight: bold;}
        .election-hpwidget {background:#fff;}
        .election-hpwidget .election-data-header {margin-bottom:10px;}
        .election-hpwidget .election-header-wrapper .assembly-election-header{display:inline-block;background-color:#352f2f;border-radius:4px 0px 0px 4px;position:relative;font-size:1.2rem;line-height:2.2rem;color:#fff;padding-left:10px;margin:10px 10px 0;}
        .election-hpwidget .election-header-wrapper .assembly-election-header span{display:inline-block;background-color:#fc363b;margin-left:7px;padding:0 7px;}
        .election-hpwidget .election-header-wrapper .assembly-election-header span::after{content:"";width:0;height:0;border-top:0px solid transparent;border-bottom:22px solid transparent;border-left:16px solid #fc363b;top:0;position:absolute;right:-16px;}
        .election-hpwidget .election-header-wrapper .statestoggle{display:block; margin:5px 0 0; white-space:nowrap; overflow-x:auto; overflow-y:visible;padding:10px 0;}
        .election-hpwidget .election-header-wrapper .statestoggle .states {height:20px;}
        .election-hpwidget .election-header-wrapper .statestoggle a{border-top:1px solid #c0c2c4;border-bottom:1px solid #c0c2c4;font-size:0.9rem;height:20px;line-height:19px;color:#333;position:relative;padding:0 15px;display:inline-block; vertical-align:top;}
        .election-hpwidget .election-header-wrapper .statestoggle a::after{content:"";display:block;position:absolute;right:0;top:4px;height:12px;width:1px;background-color:#c0c2c4;}
        .election-hpwidget .election-header-wrapper .statestoggle a:first-child{color:#fff; margin-left:3px;}
        .election-hpwidget .election-header-wrapper .statestoggle a:first-child::before{content:"";display:block;position:absolute;background-color:#000;top:-2px;left:-3px;right:0;bottom:0;border-radius:3px;height:24px;width:calc(100% + 6px);z-index:1;box-shadow: 0 0 8px 1px rgba(0, 0, 0, .34);}
        .election-hpwidget .election-header-wrapper .statestoggle a b{font-weight:normal;position:relative;z-index:2;}
        .election-hpwidget .election-header-wrapper .statestoggle a:last-child::after{display:none;}
        .election-hpwidget .election-header-wrapper .read_more{display:none;}
        .election-hpwidget .result_inner .election-data-header .section .top_section{margin:0; text-align:center;}
        .election-hpwidget .result_inner .election-data-header .section .top_section .read_more {display:none;}
        .election-hpwidget .result_inner .election-data-header .section .top_section h2{background:transparent;font-size:14px;padding:0; border:none; text-transform:uppercase;}
        .election-hpwidget .result_inner .election-data-header .section .top_section span:before {display:none;}
        .election-hpwidget .result_inner .election-data-header .section .top_section h2 a{color:#000;}
        .election-hpwidget .result_inner .election-data-header .electionsnav{background:#ebeef1;}
        .election-hpwidget .result_inner .election-data-header .electionsnav a{font-size:12px;color:#000;padding:0 10px; display:inline-block; vertical-align:top; height:22px; line-height:22px;}
        .election-hpwidget .result_inner .star-candidates-wrapper{margin-top:10px;}
        .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col{border-left:none; border-right:none; font-weight:bold;}
        .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col:first-child {padding-left:25px;}
        .election-hpwidget .result_inner .result-tbl .tbl_col:first-child{border-right:1px solid #dcdcdc;}
        .election-hpwidget .result_inner .result-tbl .tbl_row:last-child .tbl_col{border:none;font-size:11px;}
        .election-hpwidget .result-tbl .tbl_col .partyclr {display: inline-block;border-radius: 2px;padding: 1px 7px;line-height: normal;color: #fff;font-size: 11px;margin-left: 20px;font-weight: normal;}
        .chart-horizontal{white-space: nowrap; overflow: auto; width:100%; margin-bottom:10px;}
        .chart-horizontal .result-tbl {border: 1px solid #dcdcdc;}
        .election-hpwidget .table-chart {display:inline-block; width: 300px; margin-right: 10px; vertical-align:top;}
        .election-hpwidget .el_powerdby{font-size: 11px;color: #555;text-transform: uppercase;line-height: 22px;text-align: center;font-weight: normal;}
        .widget_disclaimer {color: #333;background: #fff;font-size: 10px;line-height: 1.2;padding: 0 15px 5px;}
        .widget_disclaimer ul li {padding: 3px 0;}
  `;

  const articleshowCss = `
        .article-section{background:#fff;}   
        .article-section .text_ellipsis{overflow:visible; -webkit-line-clamp: unset;}
        .story-article {color: #000; word-break: break-word;}
        .story-article .story-content a {color: #1a75ff;}
        .story-article .story-content p{margin:0}
        .story-article article div {margin: 5px auto;}
        .story-article .story-content .youtube-iframe{margin: 8px 0 8px auto;}
        .story-article .story-content .top-img-ad{min-height:250px}
        .news_card {margin: 0 0 4%;}
        .news_card h1 {font-size: 1.8rem; line-height: 2.8rem; padding: 15px 3% 5px; color: #000; margin:0;}
        .news_card h1 amp-img{width: 28px; height: 28px; display: inline-block; vertical-align: sub; margin-left: 8px;}
        .news_card .txtdisclaim {padding: 0 3% 5px; font-style: italic; font-size: 1.25rem; color: #c5c5c5;}
        .news_card .news_card_source {margin: 0 3% 10px; display: block; font-size: 1.2rem; color: #a0a0a0;}
        .news_card .news_card_source a {color: #1f81db;}
        .adsForThirdparty {margin:5px 0 0 0}
        .adsForThirdparty img, .adsForThirdparty amp-img{width: auto; min-width: initial; margin: initial; max-height:25px}
        .backToBrandwire {padding: 10px 3% 0;}
        .backToBrandwire a {color: #999; font: normal 1.1rem/1.4rem arial,sans-serif; text-transform: uppercase; position: relative; display: block; margin-left: 12px;}
        .backToBrandwire a:before {content: ""; display: block; height: 6px; width: 6px; position: absolute; top: 4px; left: -10px; transform: rotate(-135deg); transition: all .35s ease-in-out; border-top: 1px solid #a7a7a7; border-right: 1px solid #a7a7a7;}
        .story-article .brandwire_disclaimer {font-style: italic; font-size: 1.4rem; color: #000; line-height: 2.2rem; margin: 0 0 15px; clear: both;}
        .story-article .brandwire_disclaimer a {color: #555; text-decoration: underline;}
        .news_card .enable-read-more {padding: 0 3% 10px; display: block;}
        .enable-read-more .first_col {width: 100%; display: inline-block; vertical-align: top;}
        .enable-read-more .caption {font-size: 1.4rem; line-height: 2.2rem; transition: max-height .4s ease-out; font-weight: normal; color: #7d7d7d; margin:0}  
        .ads-between-story {margin: 4% 0;}
        .top-article {padding: 3%; margin: 0 0 4%;}
        .top-article.tophighlight{background: #ebebeb;}
        .top-article h2 {font-size: 16px; margin: 0 0 5px; color: #333333;}
        .top-article li {position: relative; padding: 0 0 0px 15px; font-size: 1.6rem; line-height: 2.4rem; margin: 0 0 10px; color: #6e6e6e;}
        .top-article li:before {content: ""; height: 6px; width: 6px; border-radius: 50%; position: absolute; left: 0; top: 7px; z-index: 2; border: 1px solid #6e6e6e; background: #ebebeb;}
        .top-article li:after {content: ""; position: absolute; left: 3px; top: 10px; bottom: -20px; width: 1px; z-index: 1; background: #c9c9c9;}
        .top-article li:last-child {margin: 0;}
        .top-article li:last-child:after {width: 0;}
        .story-article .tophighlight .btn_open_in_app {font-size: 1.4rem; display: block; text-align: center; height: 36px; line-height: 40px; outline: none; margin: 10px 0 0; border-radius: 40px; color: #fff; background-image: linear-gradient(94deg, #ff0303, #c90000);}
        .story-article .story-content .nic {position: relative; padding: 3px 5px 0 5px; margin-right: 5px; cursor: pointer; font-weight: bold; display: inline-block; background: rgba(220, 140, 141, 0.2); transition: .25s ease; line-height: 25px; font-size: 1.5rem;}
        .story-article .video_icon {width: 41px; height: 41px; top:50%;left:50%;transform:translate(-50%, -50%); animation: ripple-circle 1s linear infinite;background:red; border-radius:50%}
        .story-article .video_icon:after {border-top: 12px solid transparent;border-bottom: 12px solid transparent;border-left: 14px solid #fff;}

        .story-article .news-card.banner-image {width: auto; margin-top: 4%; border: 0; padding: 0;}
        .story-article .news-card.banner-image a{display:block; width:100%}
        .story-article .news-card.banner-image img, .story-article .news-card.banner-image amp-img{max-height:120px}
        .inlineshare span.inline-txt{background: #e9e9e9; padding:4px 0 2px}
        .inlineshare .share_icon{position: static; display: inline-block; padding: 0; background: #d0021b; line-height: 12px; margin: 0 0 0 5px; box-shadow: none; height: 25px; width: 25px}
        .inlineshare .share_icon span{padding:6px; cursor: pointer;}
        .inlineshare .share_icon svg{width:11px;}

        .bulletContent h2{display:none;}
        .bulletContent li{color:#333;}
        .bulletContent li:before{border:1px solid #000;background:#000;}
        .bulletContent li:after{display:none;}

        .story-article .img_wrap {margin: 0 0 2%; position: relative;}
        .story-article img, .story-article amp-img {max-width: 100%; height: auto;}
        .story-article .img_wrap .img_caption {margin:0; padding: 10px 0 5px; color: #8d8d8d; font-size: 1.2rem; line-height: 1.6rem;}
        .story-article .story-content {padding: 0 4% 20px 4%; font-size: 1.5rem; line-height: 2.8rem; word-wrap: break-word;}
        .story-article .story-content h2{font-size: 1.8rem; line-height: 2.6rem; margin: 20px 0 5px;}
        .story-article .story-content h2+img{margin-bottom:5px}
        .story-article .story-content section {margin: 5px 0px 20px; box-shadow: 0px 5px 3px -2px rgba(143, 143, 143, 0.29); padding-bottom: 15px;}
        .story-article .story-content section h2 {margin: 15px 0 5px;}
        .story-article .story-content section p{display: block; margin:0 0 15px;}
        .story-article .story-content section p:last-child{margin-bottom:0;}
        .story-article .story-content .amz_btn {display: inline-block; margin: 0 5px;}
        .story-article .story-content .amz_btn a {padding: 0 10px; font-size: 12px; color: #000; font-weight: bold; height: 25px; border-radius: 3px; background: #fed20a; line-height: 26px; font-family: arial; display: block;}     
        .default-outer-player {position: relative;}
        .story-article .embedarticle {border-left: 5px solid #000; padding: 0 0 0 10px; position: relative; clear: both; display: block; margin: 10px 0;}  
        .story-article .embedarticle a {display: flex;}
        .story-article .embedarticle a img, .story-article .embedarticle a amp-img {width: 68px; height: 51px;}
        .story-article .embedarticle a strong {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2; width:75%;  margin-left: 15px;}
        .default-outer-player .inlinevideo {position: absolute; margin: auto; bottom: 0; left: 0; right: 0; padding: 10px 3% 5px; color: #fff; background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.9));}
        .default-outer-player .inlinevideo .text_ellipsis {font-size: 1.2rem; line-height: 1.6rem; max-height: 3.2rem;}
        .default-outer-player .video_icon{display:none}
        .comment_wrap .btn_comment {display: block; margin: 20px auto; width: 80%; text-align: center; height: 35px; line-height: 40px; font-family: 'Noto Sans' , Arial,Helvetica,sans-serif; font-size: 1.6rem; background: #ee111c; color: #fff;}
        .con_fbapplinks{padding:0 0 15px;text-align:center;margin:20px 2% 0;background:#fff;border-bottom:1px solid #d0d0d0;}
        .con_fbapplinks h3{font-size:1.4rem; line-height: 2.4rem; margin:0}
        .con_fbapplinks .wdt_story_social {width: 100%; overflow: hidden; overflow-x: auto;}
        .con_fbapplinks .wdt_story_social ul{display: flex; justify-content: space-between; flex-wrap: nowrap;}
        .con_fbapplinks .wdt_story_social ul li{margin:15px 0 0;}
        .con_fbapplinks .wdt_story_social ul li a{width:64px;min-height:80px;position:relative;display:block;}
        .con_fbapplinks .wdt_story_social ul li a label{color:#8d8d8d;font-size:12px;font-family:Arial, Helvetica, sans-serif;display:block;margin:auto;position:absolute;bottom:0;left:0;right:0;}
        .con_fbapplinks .wdt_story_social ul li a label b{display:block;margin-bottom:3px;color:#000;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle{width:40px;height:40px;border-radius:50%;margin:0 auto;display:flex;align-items:center;justify-content:center;background:#f0f0f0;box-sizing:border-box;overflow:hidden;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle svg{width:24px;height:24px;color:#fff;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.app-install{background:#f0f0f0;border:1px solid #dbdbdb;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.app-install svg.mobile{color:#000; width:20px; height:20px}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.fb-like{background:#3a5897;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.twitter{background:#5ea8dc;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.youtube{background:#fa1100;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.telegram{background:#08c;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.koo{background:#f9ce45;}
        .con_fbapplinks .wdt_story_social ul li a .iconcircle.koo svg{width:35px;height:35px;}
        .next_article{display: table; width: 100%; margin:20px 0 20px}
        .next_article a{display: table-row;}
        .next_article .nxttitle{padding: 5px 4%; width: 72%; display: table-cell; vertical-align: middle; background: #de4949;}
        .next_article .nxttitle .text_ellipsis{font-size: 1.5rem; line-height: 2.4rem; color:#fff; max-height: 4.8rem; -webkit-line-clamp: 2; overflow:hidden}
        .next_article .nxtlink{font-size: 1.5rem; line-height: 2.4rem; color:#fff; padding: 10px; width: 28%; display: table-cell; vertical-align: middle; position: relative; text-align:center; background: #000;}

        .btn_openinapp, .btn_openinapp:visited{z-index:2;color:#fff;padding:0 15px 0 13px;text-decoration:none; font-size:14px;border-radius:0 25px 25px 0;background:#AD0000;position:fixed;bottom:210px;margin:auto;left:0;font-weight:normal;transition:all 350ms ease-in-out;height:30px;line-height:32px;white-space:nowrap;}
        .btn_openinapp.float-amz {background: #fff; box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.2); height: 45px; line-height: 47px; padding: 0 8px 0 10px;}
        .btn_openinapp.float-amz span{display:inline-block;vertical-align:top;}
        .btn_openinapp.float-amz span.txt{text-transform:uppercase;color:rgba(255, 166, 0, 0.932);font-size:14px;position:relative;width:55px;font-weight:bold;text-align:left;padding:8px 0 0 2px;box-sizing:border-box;height:45px; font-family:arial}
        .btn_openinapp.float-amz span.txt:after{content:'';position:absolute;top:10px;left:0;width:50px;height:12px;background:url(https://navbharattimes.indiatimes.com/photo/58606011.cms) no-repeat;background-size:contain;}
        .btn_openinapp.float-amz span.img{margin:4px 0 0 0;}
        .btn_openinapp.float-amz span.img img, .btn_openinapp.float-amz span.img amp-img{width:36px; height:36px; vertical-align:top}

        .story_partition {font-size: 1.8rem; margin: 20px 0 10px; font-weight: 600; line-height: 2.4rem; display: inline-block; width: 100%; text-align: center; position: relative; box-sizing: border-box; color: #000;}
        .story_partition span {padding: 0 10px; z-index: 1; background: #fff; position: relative; box-sizing: border-box;}
        .story_partition:after {width: 100%; height: 4px; left: 0; top: 44%; margin-top: -1px; position: absolute; content: ""; background: ${cssVariables["$story-partition-border-color"]};}
        amp-next-page ad1{background-color:#fff}

        .quote{position: relative; margin:15px 0}
        .quote .start-quote, .quote .end-quote {display: block; overflow: hidden; margin:0 0 10px 0}
        .quote .start-quote:before, .quote .end-quote:before {width: 100%; height: 1px; left: 0; top: 15px; margin-top: -1px; position: absolute; content: ""; background: #d3d3d3;}
        .quote .start-quote b, .quote .end-quote b{position: relative; height: 30px; width:25px; display: block; background: #fff; left:5%; float: left;}
        .quote .start-quote b:before, .quote .start-quote b:after, .quote .end-quote b:before, .quote .end-quote b:after {height: 30px; top:0; left:0; width: 11px; z-index: 1; position: absolute; content: ""; background: linear-gradient(135deg, #000000 75%, #ffffff 25%);}
        .quote .start-quote b:after {left:13px;}
        .quote .end-quote{margin:10px 0 0 0}
        .quote .end-quote:before {top: auto; bottom: 15px;}
        .quote .end-quote b{left: auto; right:5%; float: right;}
        .quote .end-quote b:before {left: auto; right:0;}
        .quote .end-quote b:after {left: auto; right:13px;}
        .quote .quote_txt{font-size: 2.0rem; line-height: 3.0rem; margin:0 0 10px; font-weight: 600;}
        .quote .quote_author{font-size: 1.6rem; line-height: 2.0rem; color: #c8c8c8; text-align: right;}

        table.tableContainer, .tableContainer table{width:100%; border-collapse:collapse; margin:15px auto; border-spacing: 0px; border:0;}
        table.tableContainer tr td, table.tableContainer tr th, .tableContainer table tr td{text-align:left; font-size:1.4rem; padding: 10px 10px 8px; line-height: 2.2rem; border: solid 1px #d5d5d5; color: #000;}
        
        .tableContainer{margin: 15px 0; overflow-x: scroll; overflow-y: hidden;}
        .tableContainer h3{margin:0 0 10PX; font-size:1.8rem; text-transform:uppercase;}
        .tableContainer table{margin:0 auto;}
        .tableContainer table tr td{background: #fff;}
        .tableContainer table tr:first-child td, .tableContainer table tr th{font-weight: 600;background: #e3e3e3;}
        .story-article .tableContainer table td a{color:#000; border-bottom:1px dotted #000}

        .horizontal{white-space:nowrap;width:100%;overflow:hidden;overflow-x:auto;margin:0 0 15px;background:#000;}
        .horizontal li.photo_card{display:inline-block;vertical-align:top;border-right:5px solid #fff;width:80%;min-height:245px;background:transparent;border-bottom:0;margin-bottom:-200px;padding-bottom:200px;}
        .horizontal li.photo_card:last-child{border-right:0;}
        .horizontal li.photo_card .pagination{display:none;}
        .horizontal li.photo_card .caption{white-space:initial;}
        .horizontal li.photo_card .place_holder{height:180px;background:#fff;border:2px solid #d7d7d7;border-bottom:0;}
        .horizontal li.photo_card .place_holder img{display:block;position:relative;left:50%;top:50%;transform:translate(-50%, -50%);max-width:100%;max-height:100%;}
        .horizontal li.photo_card h4{position:absolute;top:2px;left:2px;right:2px;padding:10px;white-space:normal;background:linear-gradient(rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0));margin:0;}
        .horizontal li.photo_card h4 .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;}
        .horizontal li.photo_card .enable-read-more{color:#fff;padding:10px 0 0 10px;}
        .horizontal li.photo_card .enable-read-more .first_col{width:85%;}
        .horizontal li.photo_card .enable-read-more .second_col{width:15%;}
        .horizontal li.photo_card .enable-read-more .readmore:after{top:15px;border-width:0 2px 2px 0;}

        li.nbt-horizontalView.relarticle{margin:0 0 4%; border-bottom: 1px solid #ebebeb; padding-bottom:3%}
        li.nbt-horizontalView.relarticle span.article-text{font-size:1.8rem;position:relative; margin-bottom:10px; font-weight:bold}
        li.nbt-horizontalView.relarticle .nbt-list{white-space:nowrap;}
        li.nbt-horizontalView.relarticle .nbt-list li.nbt-listview{width:230px;margin:0 10px 0 0;border:0; display: inline-block; vertical-align: top;}
        li.nbt-horizontalView.relarticle .nbt-list li.nbt-listview .tbl_column:first-child{width:105px;float:left;}
        li.nbt-horizontalView.relarticle .nbt-list li.nbt-listview .tbl_column:last-child{padding:0 0 0 10px;display:table-cell;white-space:normal;}
        li.nbt-horizontalView.relarticle .nbt-list li.nbt-listview .text_ellipsis{font-size:1.6rem;line-height:2.4rem;max-height:7.2rem;-webkit-line-clamp:3; overflow:hidden; color:#000;}
        
        .recipeArticle .time_calor{font-size: 12px; background-color: #f2f2f2; padding: 10px 0; overflow:hidden; margin:0 0 20px}
        .recipeArticle .time_calor ul{margin:0; padding:0; white-space:nowrap;}
        .recipeArticle .time_calor ul li{display:inline-block; vertical-align:top; width:33%; text-align:center;}
        .recipeArticle .time_calor .txt_innner {display: inline-block; vertical-align: middle; text-align: left; color:#333;}
        .recipeArticle .time_calor ul li i{display:inline-block; vertical-align:top;}
        .recipeArticle .time_calor ul li span {font: bold 14px/12px 'Arial'; display:block; margin-top: 3px;}
        .recipeArticle .img_caption{display: none}
        .recipeArticle h1, .recipeArticle .news_card_source{text-align: left}
        .recipeArticle .img_wrap{margin:0}
        .recipe_ingriedient{border-bottom:1px solid #d8d8d8; padding: 0 4% 20px 4%; margin-bottom: 15px}
        .recipe_ingriedient h4{color: #525252; font-size: 2.0rem; margin: 5px 0 20px; text-transform: uppercase;}
        .recipe_ingriedient .ingredients_lilsting{margin:0; padding:0; list-style:none}
        .recipe_ingriedient .ingredients_lilsting li{font-size:1.6rem; line-height:2.4rem;margin: 0 0 0 15px; position: relative;}
        .recipe_ingriedient .ingredients_lilsting li:before {content: ''; position: absolute; height: 2px; width: 5px; background: #333; left: -15px; top: 8px;}
        .recipe_ingriedient .specialingrs{font-size:1.7rem;display:block;color:#000; margin:15px 0; text-transform: capitalize;}
        
        .recipeArticle h1{position: relative;}
        .recipeArticle .recipeStepsTitle{margin: -20px -4.2% 20px; background: #f2f2f2; padding: 10px 4% 5px;}
        .vegnonveg{border:1px solid #ebebeb;padding:2px;display:inline-block;margin-left:10px;text-indent:-9999px;position:absolute;right:6px;top:6px;}
        .veg, .Veg{background-color:#60cb55;width:8px;height:8px;display:block;border-radius:8px;}
        .nonveg, .nonVeg, .non-veg, .Non-Veg{background:#f13838;width:8px;height:8px;display:block;border-radius:8px;}

        .recipeArticle .time_calor ul li svg{ width:28px; height:28px; display: inline-block; vertical-align: middle; margin-right: 8px; color:#333;}

        .story-article article .jobschema h2, .story-article article .jobschema strong{font-size: 16px; line-height: 24px;margin: 20px 0 5px;}
        .story-article article .jobschema table{border:solid 1px #d3d3d3; margin: 0 0 15px 0; table-layout: fixed; width:100%; border-spacing: 0;}
        .story-article article .jobschema table tr td{font-size: 1.4rem; line-height: 2.2rem; color:#333; font-weight: normal;word-break: break-word; width: 50%; border:0; padding: 5px;}
        .story-article article .jobschema table tr td img, .story-article article .jobschema table tr td amp-img{max-width: 140px; min-height: auto;}
        .story-article article .jobschema table tr:nth-child(2n+1) td{background: #f5f5f5;}

        .wdt_superhit{margin: 20px 3%;}
        .wdt_superhit .section{padding:0}
        .wdt_superhit .section h3 {margin: 0 0 5px; text-align: center; position: relative; width:100%; border-bottom: 0;}
        .wdt_superhit .section h3 span{font-size: 16px; color: #000; position: relative; z-index: 2; background: #fff; padding: 0 10px; display: inline-block;}
        .wdt_superhit .section h3 span:before{display: none;}
        .wdt_superhit .section h3:before {content: ''; background: #d7d7d7; height: 1px; top: 0; bottom: 0; left: 0; right: 0; position: absolute; margin: auto; z-index: 1;}
        .wdt_superhit .rest-topics{padding: 0; flex-wrap:nowrap; overflow-x:auto;}
        .wdt_superhit .news-card.horizontal{box-shadow: 0 0 8px 6px rgba(223, 223, 223, 0.5); padding: 15px; background: #fff; margin: 10px 15px 10px 10px; border: 0; white-space:normal; flex-direction: row-reverse; overflow:visible;}
        .wdt_superhit .news-card.horizontal .img_wrap{margin:0 0 0 10px}
        .wdt_superhit .news-card.horizontal .img_wrap img, .wdt_superhit .news-card.horizontal .img_wrap amp-img{width:90px}
        .wdt_superhit .news-card.horizontal .con_wrap {width: 100px;}
        .wdt_superhit .news-card.horizontal .con_wrap .text_ellipsis{font-size: 1.4rem; line-height: 2.4rem; max-height: 7.2rem; display: -webkit-box; color: #000; -webkit-line-clamp: 3; overflow:hidden}

        #docx{margin-left:25px; position:relative}
        #docx:before{content:''; background:url('https://navbharattimes.indiatimes.com/photo/70661346.cms') -6px -63px no-repeat; width:20px; height:25px; position:absolute; left:-25px; top:-5px}
        #pdf{margin-left:30px; position:relative}
        #pdf:before{content:''; background:url('https://navbharattimes.indiatimes.com/photo/70661346.cms') -4px -5px no-repeat; width:22px; height:25px; position:absolute; left:-30px; top:-5px}
        #xls{margin-left:30px; position:relative}
        #xls:before{content:''; background:url('https://navbharattimes.indiatimes.com/photo/70661346.cms') -4px -34px no-repeat; width:22px; height:25px; position:absolute; left:-30px; top:-5px}
        #ppt{margin-left:30px; position:relative}
        #ppt:before{content:''; background:url('https://navbharattimes.indiatimes.com/photo/70661346.cms') -4px -91px no-repeat; width:22px; height:25px; position:absolute; left:-30px; top:-5px}
        .amp-social-share-telegram {background-image:
          url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8cGF0aCBkPSJNMjAuMjQgMjUuODYzbC0uMzQ3IDUuMDI2Yy40OTcgMCAuNzEyLS4yMi45Ny0uNDg0bDIuMzMtMi4yOSA0LjgyOSAzLjYzN2MuODg1LjUwNyAxLjUxLjI0IDEuNzQ4LS44MzhsMy4xNy0xNS4yNzVjLjI4MS0xLjM0Ny0uNDczLTEuODc0LTEuMzM2LTEuNTQzbC0xOC42MyA3LjMzNmMtMS4yNy41MDctMS4yNTEgMS4yMzYtLjIxNSAxLjU2N2w0Ljc2MyAxLjUyMyAxMS4wNjItNy4xMmMuNTIxLS4zNTQuOTk0LS4xNTguNjA1LjE5N2wtOC45NDkgOC4yNjR6IiBmaWxsPSIjRkZGIi8+Cjwvc3ZnPg=="); background-color:#27A7E8; background-size: 42px 42px;}
        .amp-next-page-links {padding: 0 15px; background: #fff;}
        .wdt_popular_videos .view-horizontal{flex-wrap: nowrap; overflow-x: auto;}
        .wdt_popular_videos .view-horizontal li.news-card.vertical{margin-right:15px}
        .wdt_popular_videos .view-horizontal li.news-card.vertical .img_wrap img, .mobile_body .view-horizontal li.news-card.vertical .img_wrap amp-img{width:200px; height:111px;}
        .wdt_popular_videos .list-slider{padding: 0 0 0 30px; width: calc(100% - 60px);}
        .wdt_popular_videos .list-slider .view-horizontal{padding:0;}
        .mobile_body .wdt_amz.pwa-deals {margin-top: 4%; padding-top: 4%; background: #fff;}
    `;

  const removedASCss = `
        .pollContainer{box-shadow: 0 1px 1px 1px rgba(0,0,0,0.1); padding:15px 4% 10px; margin:15px 0; background: #fff; border: 1px solid #c6c6c6;}
        .pollContainer.wdt{margin: 0 3%}
        .pollContainer h4{font-size:2.2rem; line-height: 2.8rem; margin:0 0 15px; color:#000;}
        .pollContainer ul li label{font-size: 18px; padding:0 30px 0 10px; display: block; margin: 0 0 10px; line-height: 33px; position: relative; background: #e4e4e4; color: #333;}
        .pollContainer .answer-bar {height: 100%;line-height: 33px; transition: width 1.5s ease-in-out; text-align: center; position: absolute; right: 0; font-size: 1.4rem; font-weight: 600;}
        .pollContainer ul li label input[type="radio"]{position: absolute; right: 10px; top: 0; bottom: 0; margin: auto; left:initial; opacity: 1;}
        .pollContainer .captcha-box{position: relative; padding:10px 0 0 0; height: 55px;}
        .pollContainer .captcha-box .btnsubmit{display:inline-block; text-align: center; padding:5px 15px 1px; font-size:1.6rem; font-weight: 600; position: absolute; right: 0; top: 20px; -webkit-appearance: none; border: 1px solid ${cssVariables["$home-section-more-border"]}; background:${cssVariables["$home-section-more-bg"]}; color: ${cssVariables["$home-section-more-color"]};}
        .pollContainer .captcha-box .g-recaptcha{-webkit-transform:scale(0.70); transform:scale(0.70); -webkit-transform-origin:0 0; transform-origin:0 0;}
        .pollContainer .errorMsg{text-align: left; margin:5px 0 0 10px; font-size: 1.4rem; line-height: 1.6rem;}
        .pollContainer .txt{margin:0 0 10px; font-size: 1.6rem; line-height: 1.8rem}

        .tableContainer table.elec_inl_wdt tr td{text-align: center}
        .tableContainer table.elec_inl_wdt tr td:first-child{color:#fff;}
        .tableContainer table.elec_inl_wdt tr td.NDA{background: linear-gradient(to right, #FFA460, #F06005);}
        .tableContainer table.elec_inl_wdt tr td.UPA{background: linear-gradient(to right, #5FB4E5, #0076CC);}
        .tableContainer table.elec_inl_wdt tr td.MGB{background: linear-gradient(to right, #00CE7D, #008440);}
        .tableContainer table.elec_inl_wdt tr td.OTH{background: linear-gradient(to right, #97A3AA, #5C6569);}
        .tableContainer table.elec_inl_wdt tr td.BJP{background:#FF7D1D}
        .tableContainer table.elec_inl_wdt tr td.JJP{background:#F7DC10}
        .tableContainer table.elec_inl_wdt tr td.INLD{background:#198de2}
        .tableContainer table.elec_inl_wdt tr td.OTH{background:#84898B}
        .tableContainer table.elec_inl_wdt tr td.INC{background:#0076CC}
        .tableContainer table.elec_inl_wdt tr td.SS{background:#FF6634}
        .tableContainer table.elec_inl_wdt tr td.NCP{background:#07B2B2}
        .tableContainer table.elec_inl_wdt tr:last-child td{font-weight: bold; color:#000;}

        .wdt_amazon._paytmsponsored h3{font-size:1.8rem;margin-bottom:5px;}
        .wdt_amazon._paytmsponsored h3 amp-img{margin-top:2px;width:80px;height:20px;float:right}
        .wdt_amazon._paytmsponsored ul{white-space:nowrap;overflow-x:auto;padding-right:5px;}
        .wdt_amazon._paytmsponsored ._paytm_card{display:inline-block;vertical-align:top;margin-right:10px;box-shadow:0 0 5px #b8b8b8;width:133px;white-space:normal;text-align:center;margin:5px;padding:5px;min-height:210px;word-break:break-word;}
        .wdt_amazon._paytmsponsored ._paytm_card:last-child{margin-right:0;}
        .wdt_amazon._paytmsponsored .tbl .tbl_col{display:block;}
        .wdt_amazon._paytmsponsored ._paytm_card .prod_img{margin:auto;}
        .wdt_amazon._paytmsponsored ._paytm_card .des .heading .text_ellipsis{font-size:1.4rem;line-height:2rem;color:#000;font-weight:bold;overflow:hidden;-webkit-line-clamp:2;}
        .wdt_amazon._paytmsponsored ._paytm_card .items .tbl_col:first-child{text-align:center;}
        .wdt_amazon._paytmsponsored ._paytm_card .prod_img{width:auto;height:110px;}
        .wdt_amazon._paytmsponsored ._paytm_card .items .tbl_col:first-child img{max-width:initial;max-height:100px;min-width:initial;width:auto;}
        .wdt_amazon._paytmsponsored ._paytm_card ._paytm_price{margin-top:5px;}
        .wdt_amazon._paytmsponsored ._paytm_card ._paytm_price .main-price{color:#df2027;font-family:arial;font-size:1.6rem;}
        .wdt_amazon._paytmsponsored ._paytm_card ._paytm_price .btn-buy{padding:0 10px 0px;font-size:1.4rem;float:none;color:#000;font-weight:bold;height:28px;border-radius:3px;background:#fed20a;line-height:32px;display:inline-block;margin-top:5px;}

        .header .hamburger {cursor: pointer; width: 18px; height: 15px; float: left; margin: 16px 0 16px 15px; background: transparent; border: 0; padding: 0; vertical-align: top; outline: none;}
        .header .hamburger i {border-radius: 2px; content: ''; display: block; width: 18px; height: 2px; margin-top: 3px; background-color: #000;}
        .amp-close-image {position: absolute; right: 10px; top: 10px; width: 23px; height: 23px; border: 0; min-width: initial; padding: 0; text-indent: -9000px; margin: 0; vertical-align: top; cursor: pointer; background: transparent; border:0; outline:none}
        .amp-close-image:before, .amp-close-image:after {position: absolute; left: 10px; content: ' '; height: 25px; width: 2px; background-color: #c9c9c9; top: 0;}
        .amp-close-image:before {transform: rotate(45deg);}
        .amp-close-image:after {transform: rotate(-45deg);}
        .sidenav{padding:40px 10px; background-color:#fff; width:260px}
        .sidenav li{display:inline-block; vertical-align:top; width:45%; margin:0 2.5%}
        .sidenav a {padding: 15px 0; word-break: break-word; text-decoration: none; font-size: 1.3rem; line-height:2.1rem; display: block; transition: .3s; border-bottom: 1px solid #f5f5f5; color: #333;}
        amp-sidebar{background:#fff}       
  `;

  const ampwebpush = `.align-center-button{display:flex;align-items: center;justify-content:center;flex-direction: row;width:100%; margin: 15px 0 0;}.notifyBtn .subscribe{display: flex;flex-direction: row;align-items: center;border-radius:2px;border:1px solid #007ae2;margin:0;padding:8px 15px;cursor:pointer;outline:0;font-size:16px;font-weight:400;background:#0e82e5;color:#fff;-webkit-tap-highlight-color:transparent}.notifyBtn .subscribe .subscribe-icon{margin-right:10px}.notifyBtn .subscribe:active{transform:scale(.99)}`;

  const videoshowCss = `
      .videoshow-container {background: #fff; overflow: hidden;}
      .videoshow-container .top_wdt_video .row .col8 {flex: 0 0 100%; max-width: 100%;}
      .videoshow-container .top_wdt_video {background: #282e34; padding: 20px 0 0 0;}
      .videoshow-container .top_wdt_video .row {background: transparent;}
      .videoshow-container .top_wdt_video .section {background: transparent; margin: 10px 3%; width: 100%; padding:0}
      .videoshow-container .top_wdt_video .section h1 {border-bottom: 0; margin: 0; font-size: 1.6rem; height: auto;}
      .videoshow-container .top_wdt_video .section h1 span {color: #fff; height: auto; line-height: 2.6rem;}
      .videoshow-container .top_wdt_video .section h1 span:before {border: 0;}
      .videoshow-container .top_wdt_video .row .col8 .con_social {display: none}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap {margin: 10px 3% 0;}
      .videoshow-container .top_wdt_video .row .col8 .image-container {width:540px;height:0px;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis {font-size: 1.2rem; line-height: 2.0rem; color: #bbb; max-height: initial;-webkit-line-clamp: initial; font-weight:normal}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis a{color:#ededed;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis ul, .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis ol{padding: 0 0 10px 20px;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis ul li{list-style: disc;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .text_ellipsis ol li{list-style: decimal;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .more_items {margin-top: 10px;}
      .videoshow-container .top_wdt_video .row .col8 .con_wrap .more_items span {display: inline-block; vertical-align: top; color: #868686; font-size: 1.2rem;}
      .videoshow-container .top_wdt_video .default-outer-player{width:100%}
      .videoshow-container .section_videos{padding:4% 4% 0;position:relative;background:#efefef;margin-bottom:20px;}
      .videoshow-container .nbt-list{display:flex;flex-wrap:wrap;}
      .videoshow-container .nbt-list li{background:transparent;width:48%;min-height:100px;margin:0 0 4% 0;padding:0;display:inline-block;vertical-align:top;position:relative;box-sizing:border-box;}
      .videoshow-container .nbt-list li .img_wrap{width:auto;display:block;position:relative;}
      .videoshow-container .nbt-list li .img_wrap[data-tag]:after{font-family:arial;font-size:1.1rem;position:absolute;bottom:10px;right:10px;padding:2% 3%;line-height:1.2rem;background:rgba(0, 0, 0, 0.6);content:attr(data-tag);text-align:center;color:#fff;z-index:10;}
      .videoshow-container .nbt-list li .con_wrap{margin:10px 0 0 0;padding:0;display:block;}
      .videoshow-container .nbt-list li .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;}
      .videoshow-container .nbt-list li:nth-child(2n+2) {margin-left: 4%;}  
      .videoshow-container .section_most_viewed {margin: 0 4% 4%; width: 100%; overflow: hidden; overflow-x: auto; height: auto;}
      .videoshow-container .section_most_viewed ul {white-space: nowrap; display: block;}
      .videoshow-container .section_most_viewed ul li {white-space: normal; width: 225px; margin-right: 4%;}
      .videoshow-container .section_most_viewed ul li:nth-child(2n+2) {margin-left: 0; margin-right: 4%;}
      .videoshow-container .section_most_viewed ul li:last-child {margin-right: 0;}
      .videoshow-container .section h3 {margin: 0 0 4%;}
      .videoshow-container .other_section_video {overflow: hidden; padding: 15px 4%; position: relative; background: #efefef;}
      .videoshow-container .other_section_video h2 {font-size: 2.0rem; margin: 0 0 15px;}
      .videoshow-container .other_section_video .slider .hidden_layer {overflow: hidden; overflow-x: auto; white-space: nowrap;}
      .videoshow-container .slider ul li .con_wrap {margin: 10px 0 0; padding: 0; display: block; white-space: normal;}
      .videoshow-container .slider .slider_content li .slide,.videoshow-container .slider .slider_content li {display: inline-block;vertical-align: top;}
      .videoshow-container .slider .slider_content li .slide .img_wrap{display:block}
      .videoshow-container .other_section_video .slider .con_wrap .text_ellipsis {color: #000; font-size: 1.3rem; line-height: 2.1rem; max-height: 6.3rem; -webkit-line-clamp: 3;}
      .videoshow-container .other_section_video .slider .con_wrap .section_name {font-size: 1.3rem; color: #1f81db; display: block; margin-bottom: 5px;}
      .videoshow-container .section_videos .more_icon{display:none}
      .videoshow-container .top_wdt_video .row .col8 .con_social{display:block; width:100%; text-align:center; margin: 10px 3% 0;}
      .videoshow-container .top_wdt_video .row .col8 .con_social .youtube-iframe{margin-left:auto; margin-bottom:4%}
      .videoshow-container .section .top_section {margin: 0 0 4%;}
      .videoshow-container .wdt_popular_videos .section .top_section {margin: 0 3% 4%;}
      .videoshow-container .wdt_popular_videos .list-slider {padding: 0 0 0 30px; width: calc(100% - 60px);}
      .videoshow-container .wdt_popular_videos .list-slider .view-horizontal {padding: 0;}
      .videoshow-container.hypervideo .top_wdt_video {padding: 0; background: #fff;}
      .videoshow-container.hypervideo .top_wdt_video .section h1 span{color:#000;}
      .videoshow-container.hypervideo .top_wdt_video .row .col8 .con_wrap .text_ellipsis{color:#333;}
      .videoshow-container.hypervideo .top_wdt_video .row .col8 .con_wrap .text_ellipsis a{color:#1f81db;}
      .videoshow-container.hypervideo .top_wdt_video .row .col8 .con_wrap .more_items{margin:0;}
      .videoshow-container.hypervideo .top_wdt_video .row .col8 .enable-read-more .second_col .readmore:after{border-color:#000;}
      .videoshow-container.hypervideo .keywords_wrap{margin-top:0;}
      .btn_openinapp, .btn_openinapp:visited{z-index:2;color:#fff;padding:0 15px 0 13px;text-decoration:none; font-size:14px;border-radius:0 25px 25px 0;background:#AD0000;position:fixed;bottom:210px;margin:auto;left:0;font-weight:normal;transition:all 350ms ease-in-out;height:30px;line-height:32px;white-space:nowrap;}
      .btn_openinapp.float-amz {background: #fff; box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.2); height: 45px; line-height: 47px; padding: 0 8px 0 10px;}
      .btn_openinapp.float-amz span{display:inline-block;vertical-align:top;}
      .btn_openinapp.float-amz span.txt{text-transform:uppercase;color:rgba(255, 166, 0, 0.932);font-size:14px;position:relative;width:55px;font-weight:bold;text-align:left;padding:8px 0 0 2px;box-sizing:border-box;height:45px; font-family:arial}
      .btn_openinapp.float-amz span.txt:after{content:'';position:absolute;top:10px;left:0;width:50px;height:12px;background:url(https://navbharattimes.indiatimes.com/photo/58606011.cms) no-repeat;background-size:contain;}
      .btn_openinapp.float-amz span.img{margin:4px 0 0 0;}
      .btn_openinapp.float-amz span.img img, .btn_openinapp.float-amz span.img amp-img{width:36px; height:36px; vertical-align:top}

      .amp-social-share-telegram {background-image:
        url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8cGF0aCBkPSJNMjAuMjQgMjUuODYzbC0uMzQ3IDUuMDI2Yy40OTcgMCAuNzEyLS4yMi45Ny0uNDg0bDIuMzMtMi4yOSA0LjgyOSAzLjYzN2MuODg1LjUwNyAxLjUxLjI0IDEuNzQ4LS44MzhsMy4xNy0xNS4yNzVjLjI4MS0xLjM0Ny0uNDczLTEuODc0LTEuMzM2LTEuNTQzbC0xOC42MyA3LjMzNmMtMS4yNy41MDctMS4yNTEgMS4yMzYtLjIxNSAxLjU2N2w0Ljc2MyAxLjUyMyAxMS4wNjItNy4xMmMuNTIxLS4zNTQuOTk0LS4xNTguNjA1LjE5N2wtOC45NDkgOC4yNjR6IiBmaWxsPSIjRkZGIi8+Cjwvc3ZnPg=="); background-color:#27A7E8; background-size: 42px 42px;}
      .amp-next-page-links {padding: 0 15px; background: #fff;}
    `;

  const photoshowCss = `
        .photomazzashow-content {background: #fff;}
        .photomazzashow-content h1 {background: #282e34; color: #fff; font-weight: normal; text-align: left; font-size: 2.0rem; line-height: 3.0rem; padding: 15px 3%; margin:0}
        .photomazzashow-content .photo_story li.photo_card {min-height: 200px; position: relative; width: 100%; background: #282e34;}
        .photomazzashow-content .photo_story li.photo_card .photo_des {padding: 20px 3%; color: #9e9e9e;}
        .photomazzashow-content .photo_story li.photo_card h2 {font-size: 2.0rem; line-height: 30px; font-weight: normal; margin: 0 0 15px; color:#fff;}
        .photomazzashow-content .photo_story li.photo_card p{margin:0}
        .photomazzashow-content .photo_story li.photo_card .pagination {position: absolute; left: 3%; top: 3%; display: block;}
        .photomazzashow-content .photo_story li.photo_card .pagination span {padding: 0 10px; font-size: 1.4rem; height: 30px; line-height: 30px; font-family: arial; display: block; color: #ffffff; background: #000;}
        .photomazzashow-content .photo_story li.photo_card .caption {font-size: 1.6rem; line-height: 2.4rem; -webkit-line-clamp: initial; word-break: break-word;}
        .photomazzashow-content .photo_story li.photo_card .photo_des .table_col:first-child {width: 100%;}
        .photomazzashow-content .photo_story li.photo_card img, .photomazzashow-content .photo_story li.photo_card amp-img {width:100%; height:auto; min-width:auto; min-height:auto;}
        .view-from-start, .view-from-start:hover, .view-from-start:visited{font-size: 1.6rem; text-align: center; display: inline-block; margin-bottom: 10px; padding: 8px 5% 3px 4%; border-radius: 0 20px 20px 0; font-weight: bold; line-height: 2.0rem; background:#1f81db; color:#fff;}
        .socialsharewrap{margin: 4px; text-align: center; line-height: 0; min-height:34px;}
        .next_article{display: table; width: 100%; margin:20px 0 20px}
        .next_article a{display: table-row;}
        .next_article .nxttitle{padding: 5px 4%; width: 60%; display: table-cell; vertical-align: middle; background: #de4949;}
        .next_article .nxttitle .text_ellipsis{font-size: 1.5rem; line-height: 2.4rem; color:#fff; max-height: 7.2rem; -webkit-line-clamp: 3; overflow:hidden}
        .next_article .nxtlink{font-size: 1.5rem; line-height: 2.4rem; color:#fff; padding: 20px 50px 20px 10px; width: 40%; display: table-cell; vertical-align: middle; position: relative; text-align:center; background: #000;}
        .next_article .nxtlink:before {content: ""; position: absolute; height: 30px; width: 25px; background: #de4949; border-radius: 0 0 5px 5px; right: 15px; top: 0px; bottom: 0; margin: auto;}
        .next_article .nxtlink:after {content: ""; display: block; height: 8px; width: 8px; margin: auto; position: absolute; top: -4px; right: 22px; bottom: 0; -webkit-transform: rotate(135deg); transform: rotate(135deg); border-top: 2px solid #fff; border-right: 2px solid #fff;}    
        .photomazzashow-content .list-vertical a{width: 100%;}
        .webtitle{margin:20px 4% 0;font-size:1.0rem;line-height:1.8rem;color:#a0a0a0;}
        .webtitle b{font-weight:600;}
        .webtitle a{color:#1a75ff;}
        .keywords_wrap {padding: 15px 0; position: relative; margin: 20px 4% 0; background: #fff; border-bottom: 1px solid #d0d0d0;}
        .keywords_wrap h3 {font-size: 1.6rem; text-align: center; margin: 0 0 15px; color:#000;}
        .keywords_wrap .nowarp_content {white-space: nowrap;}
        .keywords_wrap a {margin: 2px 5px 5px 2px; font-size: 1.1rem; border-radius: 20px; padding: 0 20px; height: 31px; line-height: 33px; display: inline-block; white-space: nowrap; color: #716f6f; border: 1px solid #cbcbcb; background: #fff;}
        .wdt_list_vertical {padding: 2% 0 0; margin: 0 0 4%; background: #eee;}
        .wdt_list_vertical .section .top_section {margin: 0;}
        .wdt_list_vertical .section .top_section h2 {border-bottom: 0; margin: 0;}
        .wdt_list_vertical .section .top_section h2 span {background: transparent; padding: 0; color: #000;}
        .wdt_list_vertical .section .top_section h2 span:before {border: 0;}
        .mobile_body .wdt_list_vertical ul.view-horizontal li.vertical .img_wrap img, .mobile_body .wdt_list_vertical ul.view-horizontal li.vertical .img_wrap amp-img {width: 140px; max-height:105px;}
        .inline_app_download{background: #49eac6; padding: 15px 3%}
        .inline_app_download .con_wrap{font-size: 1.6rem; font-weight: 600; display: block; height: 31px; line-height: 35px;}
        .inline_app_download .btn_wrap{text-align: right;}
        .btn_appdownload{background: #6d0ec1; color: #fff; font-size: 1.4rem; padding:0 5px; border-radius:10px; height: 31px; line-height: 33px; display: block; text-align: center; text-transform: uppercase;}
        .btn_openinapp, .btn_openinapp:visited{z-index:2;color:#fff;padding:0 15px 0 13px;text-decoration:none; font-size:14px;border-radius:0 25px 25px 0;background:#AD0000;position:fixed;bottom:210px;margin:auto;left:0;font-weight:normal;transition:all 350ms ease-in-out;height:30px;line-height:32px;white-space:nowrap;}
        .btn_openinapp.float-amz {background: #fff; box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.2); height: 45px; line-height: 47px; padding: 0 8px 0 10px;}
        .btn_openinapp.float-amz span{display:inline-block;vertical-align:top;}
        .btn_openinapp.float-amz span.txt{text-transform:uppercase;color:rgba(255, 166, 0, 0.932);font-size:14px;position:relative;width:55px;font-weight:bold;text-align:left;padding:8px 0 0 2px;box-sizing:border-box;height:45px; font-family:arial}
        .btn_openinapp.float-amz span.txt:after{content:'';position:absolute;top:10px;left:0;width:50px;height:12px;background:url(https://navbharattimes.indiatimes.com/photo/58606011.cms) no-repeat;background-size:contain;}
        .btn_openinapp.float-amz span.img{margin:4px 0 0 0;}
        .btn_openinapp.float-amz span.img img, .btn_openinapp.float-amz span.img amp-img{width:36px; height:36px; vertical-align:top}
        .amp-social-share-telegram {background-image:
          url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8cGF0aCBkPSJNMjAuMjQgMjUuODYzbC0uMzQ3IDUuMDI2Yy40OTcgMCAuNzEyLS4yMi45Ny0uNDg0bDIuMzMtMi4yOSA0LjgyOSAzLjYzN2MuODg1LjUwNyAxLjUxLjI0IDEuNzQ4LS44MzhsMy4xNy0xNS4yNzVjLjI4MS0xLjM0Ny0uNDczLTEuODc0LTEuMzM2LTEuNTQzbC0xOC42MyA3LjMzNmMtMS4yNy41MDctMS4yNTEgMS4yMzYtLjIxNSAxLjU2N2w0Ljc2MyAxLjUyMyAxMS4wNjItNy4xMmMuNTIxLS4zNTQuOTk0LS4xNTguNjA1LjE5N2wtOC45NDkgOC4yNjR6IiBmaWxsPSIjRkZGIi8+Cjwvc3ZnPg=="); background-color:#27A7E8; background-size: 42px 42px;}
        .amp-next-page-links {padding: 0 15px; background: #fff;}
    `;

  const moviereviewshowCss = `
        .moviereview-summaryCard .poster_card{position:relative; height:300px; background: #333;}
        .moviereview-summaryCard h1 {font-size: 1.8rem; line-height: 2.8rem; padding: 15px 0 5px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; color: #ffdc00;}
        .moviereview-summaryCard .tbl_column:first-child {width: 120px; vertical-align: top; text-align: left; position: relative;}
        .moviereview-summaryCard .tbl_column:first-child amp-img, .moviereview-summaryCard .tbl_column:first-child img{border:4px solid #fff; max-width: 120px; max-height: 160px; width:auto;}
        .moviereview-summaryCard .tbl_column:first-child .video_icon {width: 40px; height: 40px;top:50%;left:50%;transform:translate(-50%, -50%); margin: auto; animation:initial; background:red; border-radius:50%;}
        .moviereview-summaryCard .tbl_column:first-child .video_icon:after {border-top: 12px solid transparent;border-bottom: 12px solid transparent;border-left: 14px solid #fff;}
        .moviereview-summaryCard .tbl_column:last-child {text-align: left; word-break: break-word; padding: 0 0 0 10px; vertical-align: top; position: relative; width:auto}
        .moviereview-summaryCard .tbl_column:last-child .cast{font-size: 1.6rem; line-height: 2.4rem; max-height: 7.2rem; -webkit-line-clamp: 3; color:#fff; overflow:hidden; }
        .moviereview-summaryCard .tbl_column:last-child .des{font-size: 1.4rem; line-height: 2.0rem; display: block; position: absolute; bottom: 0; color: #999;}
        .moviereview-summaryCard .tbl_column:last-child .des span{display: block;font-size: 1.2rem; line-height: 1.6rem; font-family: arial;}
        .moviereview-summaryCard .rating{margin: 10px 0 0 0}
        .moviereview-summaryCard .rating .tbl_column{text-align:center; width: 50%}
        .moviereview-summaryCard .rating .txt{font-size: 1.4rem; display:inline-block;}
        .moviereview-summaryCard .rating .count{font-size: 1.6rem; display:block; font-family: arial; color:#fff}
        .moviereview-summaryCard .rating .count b{font-size: 2.6rem; font-weight: normal;}
        .moviereview-summaryCard .rating .critic, .moviereview-summaryCard .rating .users{position: relative; color:#fff;}
        .moviereview-summaryCard .rating .critic:before{content: '\\2605'; font-size: 20px; line-height: 30px; position: absolute; top: -4px; left: -25px; color: #ff001f;}
        .moviereview-summaryCard .rating .users:before{content: '\\2605'; font-size: 20px; line-height: 30px; position: absolute; top: -4px; left: -25px;color: #00b3ff; }
        .moviereview-summaryCard .con_poster{position: absolute; z-index: 2; background-image: linear-gradient(to bottom, #1a0c01 30%, #995420 167%); left: 0; right: 0; top: 0; bottom: 0; padding: 0 3%; color:#fff}
        .moviereview-summaryCard .bg_poster{dsplay:none;height: 288px}
        
        .listen-on{height: 50px; line-height: 55px; padding: 0 3%; font-size: 1.6rem; text-align: center; display: block;}
        .listen-on a{font-weight: 600;}
        .listen-on img{display: inline; vertical-align: middle; width: 105px;}
        .moviereview-sourceCard{margin: 15px 0 5px 0}
        .moviereview-sourceCard .source{display: block; font-size: 1.2rem; line-height: 2rem; color: #a0a0a0;}

        .con_review_rate{display:none}
        .amp-social-share-telegram {background-image:
          url("data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDggNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8cGF0aCBkPSJNMjAuMjQgMjUuODYzbC0uMzQ3IDUuMDI2Yy40OTcgMCAuNzEyLS4yMi45Ny0uNDg0bDIuMzMtMi4yOSA0LjgyOSAzLjYzN2MuODg1LjUwNyAxLjUxLjI0IDEuNzQ4LS44MzhsMy4xNy0xNS4yNzVjLjI4MS0xLjM0Ny0uNDczLTEuODc0LTEuMzM2LTEuNTQzbC0xOC42MyA3LjMzNmMtMS4yNy41MDctMS4yNTEgMS4yMzYtLjIxNSAxLjU2N2w0Ljc2MyAxLjUyMyAxMS4wNjItNy4xMmMuNTIxLS4zNTQuOTk0LS4xNTguNjA1LjE5N2wtOC45NDkgOC4yNjR6IiBmaWxsPSIjRkZGIi8+Cjwvc3ZnPg=="); background-color:#27A7E8; background-size: 42px 42px;}
    `;
  const liveblogCss = `
        .lb_container .lb_summaryCard .lb_top_summary{padding:15px; background-image:radial-gradient(#73a8d4, #4d93ce, #0072d2); position:relative}
        .lb_container .lb_summaryCard .lb_top_summary amp-img{float: left; width:100px; border:2px solid #fff; margin: 0 15px 10px 0}
        .lb_container .lb_summaryCard h1{font-size: 1.6rem; line-height: 2.6rem; padding:0; color:#fff; margin-top:-7px;}
        .lb_container .lb_summaryCard h2{font-size: 1.4rem; line-height: 2.2rem; padding:0; display: block; clear: both; color:#fff}
        .lb_container .lb_summaryCard .source {margin: 0 0 10px; display: block; font-size: 1.1rem; text-align: left; color:#fff}
        .lb_container .lb_summaryCard .caption{font-size: 1.3rem; display: block; padding: 5px 0 0; clear: both; line-height: 2.0rem; color:#fff;}
        .lb_container .lbhighlight h2 {color: #000}
        .lb_container .lbhighlight a{color:#6e6e6e}
        
        .lb_container .livepost li.livelist {margin: 0 0 4%; padding: 3%; font-size: 1.6rem; line-height: 2.4rem; background:#fff;}
        .lb_container .livepost li.list_over_end{margin: -4% 0 0; background: #fff; padding: 3%}
        .lb_container .livepost li .over_end{background: #e3f2ff; border:1px solid #c6daeb; border-radius: 29px; font-size: 1.4rem; line-height: 2.4rem; text-align: center; margin: 0 auto; padding:6px 0 3px}
        .lb_container .livepost li .over_end span{display: block; font-weight: bold; margin:0;}
        .lb_container .livepost li .blog_timestamp{display: table;border-radius: 14px; padding: 4px 10px 3px; border: 1px solid #3592e1;}
        .lb_container .livepost li .blog_time{font-size: 1.3rem; font-family: arial; display: table-cell; vertical-align: middle; line-height: normal;color: #3291e0;}
        .lb_container .livepost li .blog_date{font-size: 1.3rem; font-family: arial; display: table-cell; padding:0 0 0 4px; vertical-align: middle; line-height: normal; color: #3291e0;}
        .lb_container .livepost li .blog_txt{padding: 12px 0 12px 10px; margin: 0 0 0 10px; position: relative;border-left: 1px solid #dadada;}
        .lb_container .livepost li .blog_txt a{color: #3291e0;}

        .lb_container .livepost li .blog_txt:after{position: absolute; border-radius: 50%; height:9px; width:9px; content: ''; left: -6px; bottom: 0;border: 1px solid #3291e0; background: #fff;}
        .lb_container .livepost li .score-with-url{border-radius: 14px; font-size: 1.4rem; font-weight: bold; line-height: normal; display: inline-block; padding: 6px 10px 3px 20px; margin: 5px 0 10px 0; position: relative; border: 1px solid #cccccc;}
        .lb_container .livepost li .score-with-url b{font-size:1.2rem; font-family: arial; margin:0 0 0 10px}
        .lb_container .livepost li .score-with-url span{width: 24px; height: 14px; background: #000; color: #fff; font-family: arial; font-size: 1.2rem; line-height: normal; position: absolute; top: -10px; left: -5px; border-radius: 50%; text-align: center; padding: 5px 0 5px; font-weight: normal;}
        .lb_container .livepost li .score-with-url span.cls_W{background: #ff0000; color:#fff;}
        .lb_container .livepost li .score-with-url span.cls_NB, .lb_container .livepost li .score-with-url span.cls_LB{background: #f5a623; color:#fff;}
        .lb_container .livepost li .score-with-url span.cls_WD{background: #7400da; color: #fff}
        .lb_container .livepost li .score-with-url span.cls_6{background: #66c300; color:#fff}
        .lb_container .livepost li .score-with-url span.cls_4{background: #3291e0; color:#fff}
        .lb_container .btn_newupdates{background: #ff0d00; color: #fff; height: 35px; line-height: 37px; font-size: 1.4rem; padding: 0 20px; border-radius: 20px;}
        .pos_fixed {position: fixed; bottom: 80px; left: 0; right: 0; text-align: center;}
        .hide {display: none;}
        .lb_summaryCard {width:100%;}
        .lb_container .lb_summaryCard .width_scorecard{margin-bottom: 65px;}
        .lb_container .lb_summaryCard .liveblog-scorecard img{border:0; width:auto;}
        .liveblog-scorecard{border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.24); background: #fff; font-family: arial; padding: 10px 10px 8px; margin: 10px 0 -65px;}
        .liveblog-scorecard .teama{width:40%;}
        .liveblog-scorecard .teamb{width:40%;}
        .liveblog-scorecard .versus{width:10%;}
        .liveblog-scorecard .teama img, .liveblog-scorecard .teama amp-img{float: left; margin:0 5px 0 0; width:60px;height:60px;}
        .liveblog-scorecard .teamb img, .liveblog-scorecard .teamb amp-img{float: right; margin:0 0 0 5px; width:60px;height:60px;}
        .liveblog-scorecard .teamb .country{text-align: right; margin: 0 5px 3px 0}
        .liveblog-scorecard .teamb{text-align:right}
        .liveblog-scorecard .country{display: block; font-size: 14px; font-weight: bold; line-height: normal; color: #333333; text-transform: uppercase; text-align: left; margin:0 0 3px 5px}
        .liveblog-scorecard .score{font-size: 18px; font-weight: bold; color:#000; display: block; vertical-align: top; margin:10px 0 0}
        .liveblog-scorecard .over{font-size: 12px; color:#787878; display: block; vertical-align: top;}
        .liveblog-scorecard .versus span{height: 22px; width: 30px; border-radius: 50%; border: 1px solid #cccccc; background: #f5f5f5; display: inline-block; color: #4c4c4c; font-size: 12px;  text-align: center;  font-weight: bold;  padding: 8px 0 0 0;}
        .liveblog_tabs{padding:0 3%;margin: 0 0 30px; text-align: center; width:100%; box-sizing:border-box;}
        .liveblog_tabs ul li{display: inline-block; vertical-align: top; border-radius: 20px; margin: 0 15px 0 0; border:1px solid #000; width:140px;}
        .liveblog_tabs ul li.current{background: #3291e0; border:1px solid #3291e0}
        .liveblog_tabs ul li.current b{color:#fff}
        .liveblog_tabs ul li:last-child{margin:0}
        .liveblog_tabs ul li a, .liveblog_tabs ul li b{display: block; height:35px; line-height: 35px; text-align: center; color:#000; font-weight: normal; font-size:1.4rem}
    `;

  const astroWidgetCss = `  
    .astro_widget {border-bottom: 1px solid #dfdfdf; margin: 0 3% 4%; padding-bottom: 4%;}
    .astro_widget .news-card.horizontal {display:flex; margin: 0 0 5px; padding: 0; border: 0; width: auto;}
    .astro_widget .news-card.horizontal .text_ellipsis {color: #000; font-weight: bold; font-size: 1.5rem; line-height: 2.3rem; max-height: 4.6rem;}
    .astro_widget .astro_text {margin: 10px 0 4%;}
    .astro_widget .astro_text .text_ellipsis {color: #727272; font-size: 1.4rem; line-height: 2.2rem; max-height: 6.6rem; -webkit-line-clamp: 3;}
    .astro_widget .astro_slider{background: #ececec; padding: 10px 0;}
    .astro_widget .astro_slider .slider {max-width: 320px; margin: 0 auto;}
    .astro_widget .slider .hidden_layer {overflow: hidden; overflow-x: auto; white-space: nowrap;}
    .astro_widget .slider .slider_content li, .astro_widget .slider .slider_content li .slide {display: inline-block; vertical-align: top; text-align:center}
    .astro_widget .slider li .slide .txt {font-size: 1.0rem;}
    .astro_widget .astro-sign {height: 40px; width: 40px; display: inline-block; text-indent: 10000px; cursor: pointer; background: url(${spriteAstro}) no-repeat; background-size: ${spritesizeAstro};}
    .astro_widget .astro-sign.sign-aries {background-position: -114px -64px;}
    .astro_widget .astro-sign.sign-taurus {background-position: -17px -64px;}
    .astro_widget .astro-sign.sign-gemini {background-position: -16px -15px;}
    .astro_widget .astro-sign.sign-cancer {background-position: -212px -15px;}
    .astro_widget .astro-sign.sign-leo {background-position: -65px -64px;}
    .astro_widget .astro-sign.sign-virgo {background-position: -16px -113px;}
    .astro_widget .astro-sign.sign-libra {background-position: -211px -64px;}
    .astro_widget .astro-sign.sign-scorpio {background-position: -114px -15px;}
    .astro_widget .astro-sign.sign-sagittarius {background-position: -64px -113px;}
    .astro_widget .astro-sign.sign-capricorn {background-position: -163px -15px;}
    .astro_widget .astro-sign.sign-aquarius {background-position: -162px -64px;}
    .astro_widget .astro-sign.sign-pisces {background-position: -65px -15px;}
`;

  const gnCommonCss = `
        .m-scene.tech .thumb_img_reviews {margin: 0 0 20px 0;position: relative;}
        .m-scene.tech .thumb_img_reviews .image_caption {position: absolute; bottom: 0; left: 0; right: 0; margin: auto; padding: 15px 3% 10px; background: linear-gradient(transparent, rgba(0, 0, 0, 0.9)); color: #fff;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .tbl_column {text-align: center; width: 50%;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .tbl_column:only-child{display: block;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .critic, .m-scene.tech .thumb_img_reviews .image_caption .rating .users {position: relative; font-size: 1.4rem; display: inline-block;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .critic:before {color: #ff001f; content: "\\2605"; font-size: 20px; line-height: 30px; position: absolute; top: -4px; left: -25px;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .count {font-size: 1.6rem; display: block; font-family: arial;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .count b {font-size: 2.6rem; font-weight: normal;}
        .m-scene.tech .thumb_img_reviews .image_caption .rating .users:before {color: #00b3ff; content: "\\2605"; font-size: 20px; line-height: 30px; position: absolute; top: -4px; left: -25px;}
        .m-scene.tech .wdt_varients {margin: 0 0 4%; border-bottom: 2px solid #c6c6c6;padding: 0 0 3%;}
        .m-scene.tech .wdt_varients ul {margin: 20px 0 0 0;}
        .m-scene.tech .wdt_varients ul li {display: block; margin: 0 0 10px; position: relative; line-height: normal; vertical-align: top;}
        .m-scene.tech .wdt_varients ul li a, .m-scene.tech .wdt_varients ul li span {height: 16px; line-height: 24px; padding-left: 25px;}
        .m-scene.tech .wdt_varients ul li a:before, .m-scene.tech .wdt_varients ul li span:before {content: ''; border: 1px solid #818181; border-radius: 50%; height: 16px; width: 16px; position: absolute; left: 0; top: 0;}
        .m-scene.tech .wdt_varients ul li.active a:after, .m-scene.tech .wdt_varients ul li.active span:after {content: ''; background: #135394; border-radius: 50%; height: 12px; width: 12px; position: absolute; left: 3px; top: 3px;}
        .m-scene.tech .tbl_articleshow .top-head {background: #e9e9e9; font-size: 1.6rem; line-height: 2.4rem; font-weight: bold; padding: 10px 10px 4px; margin-bottom: -15px;}
        .m-scene.tech .tbl_articleshow .top-head a{color:#000}
        .m-scene.tech .tbl_articleshow .gn_table_layout table tr td{font-size:1.4rem}
        .m-scene.tech .tbl_articleshow .gn_table_layout label.spec {display: none;}
        .m-scene.tech .tbl_articleshow .bottom-btn {background: #005499; color: #fff; font-size: 1.6rem; height: 36px; line-height: 38px; text-align: center; display: block;}
        .m-scene.tech .tbl_articleshow .gn_table_layout table tr td:first-child {color: #696969;width: 35%;border-right: 0;}
    `;

  const gnlistCss = `
        .m-scene.tech .rating_txt {display: block; color: #505050; font-size: 1.4rem; margin-top: 5px;}
        .m-scene.tech .rating_txt b {font-family: arial;font-weight: normal;}
        .m-scene.tech .rating_txt b small {color: #000;font-size: 1.4rem;font-weight: bold;} 
        .m-scene.tech .more_brands {display: block; margin-top: 5px; line-height: 1.8rem;}
        .m-scene.tech .more_brands a {color: #1f81db; font-size: 1.2rem; line-height: 1.8rem; position: relative; display: inline-block; vertical-align: top;}
        .m-scene.tech .more_brands a:after {content: '|'; margin: 0 5px;}
        .m-scene.tech .more_brands a:last-child:after {content: '';margin: 0;}
        .m-scene.tech .con_gadgets {margin-bottom: 4%; background: #fff;}
        .m-scene.tech .con_gadgets ul.scroll, .m-scene.tech .con_gadgets ul.gn-tabs{display:none}
        .m-scene.tech .top-caption {display: none; height: 25px; line-height: 28px; background: #fed20a; position: absolute; top: 0; left: 0; z-index: 10; padding: 0 5px 0 10px; font-size: 1.1rem; text-transform: capitalize;}
        .m-scene.tech .top-caption:before{background: linear-gradient(-235deg, #fed20a 50%, #ffffff 50%); height: 25px; top: 0; right: -20px; width: 20px; z-index: 1; position: absolute; content: "";}
        .m-scene.tech .symbol_rupees {margin: 0 0 0 10px; position: relative; }
        .m-scene.tech .symbol_rupees:before {content: '\\20B9'; position: absolute; left: -12px; top: 0; font-weight: normal; }
        .m-scene.tech .upcoming .top-caption:after{content: attr(data-attr)}
        .m-scene.tech .rumoured .top-caption:after{content: attr(data-attr)}
        .wdt-inline-gadgets .inline-gadgets-content{width:100%}
        .wdt-inline-gadgets .inline-gadgets-content ul li{display:block; border: 1px solid #e3e3e3; color: #000; margin: 0 3% 4%; position: relative;}
        .wdt-inline-gadgets .slider .slider_content li .slide{box-shadow: none; min-height: auto; margin: 0; padding: 15px 3% 10px; position: static; display: flex; flex-wrap: wrap; font-family: Arial, Helvetica, sans-serif;}
        .wdt-inline-gadgets .inline-gadgets-content ul li .slide.upcoming, .wdt-inline-gadgets .inline-gadgets-content ul li .slide.rumoured{padding-top: 35px}
        .wdt-inline-gadgets .inline-gadgets-content ul li .slide .prod_img{width:25%; text-align: left; height:auto; padding: 0;}
        .wdt-inline-gadgets .inline-gadgets-content ul li .slide .prod_img img, .wdt-inline-gadgets .inline-gadgets-content ul li .slide .prod_img amp-img{max-width:60px; height: auto; min-height: auto; max-height:100px; width: auto; min-width: auto;}
        .wdt-inline-gadgets .inline-gadgets-content ul li .slide .gadget_detail{width:75%; text-align: left;}
        .wdt-inline-gadgets .slider.trendingslider .slide span{display:block; cursor: pointer;}
        .wdt-inline-gadgets .slider.trendingslider .slide.upcoming .top-caption, .wdt-inline-gadgets .slider.trendingslider .slide.rumoured .top-caption{display:block;}
        .wdt-inline-gadgets .slider.trendingslider .title .text_ellipsis{font-size:1.4rem;line-height:2.2rem; max-height:4.4rem;font-weight:bold; display: -webkit-box;}
        .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage{display: flex; flex-wrap: wrap; justify-content: space-between;}
        .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage li{font-size:1.1rem;line-height:1.7rem;color:#666666;padding:0;margin:0 0 5px 10px;position:relative; border: 0;}
        .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage li:before{content:'';height:6px;width:6px;background:#666;border-radius:50%;position:absolute;top:6px;left:-10px;}
        .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage li label{font-weight:bold;text-transform:capitalize;}
        .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage li b{font-family:arial; font-weight: normal;}
        .wdt-inline-gadgets .slider.trendingslider .rating span{display:inline-block;vertical-align:top;position:relative;font-size:1.1rem; margin-right: 10px; color:#777}
        .wdt-inline-gadgets .slider.trendingslider .rating span:last-child{margin-right: 0;}
        .wdt-inline-gadgets .slider.trendingslider .rating span a{color:#1a75ff}
        .wdt-inline-gadgets .slider.trendingslider .rating b{font-family:arial;}
        .wdt-inline-gadgets .slider.trendingslider .rating b small{color:#f5a623;font-size:1rem;}
        .wdt-inline-gadgets .slider.trendingslider .rating span:first-child{margin:0 10px 0 0;}
        .wdt-inline-gadgets .slider.trendingslider .price{margin-top:5px;font-family:arial;min-height:21px;}
        .wdt-inline-gadgets .slider.trendingslider .price b{font-size:1.6rem; font-weight:bold;}
        .wdt-inline-gadgets .slider.trendingslider .btn_wrap{margin-top:5px;}
        .wdt-inline-gadgets .slider.trendingslider .btn_wrap button{text-align: center; color: #000; padding: 0 8px; font-family: 'NotoSans Regular', Arial,Helvetica,sans-serif; font-size: 1.2rem; border: 0; height: 28px; line-height: 32px; cursor: pointer; background: #fed20a; border-radius: 3px; outline: none; font-weight: bold;}
        .wdt-inline-gadgets .slider.trendingslider .gd_launch{font-size: 1.0rem; line-height: 1.8rem; display: block; color: #505050; margin-top: 5px;}
        .m-scene.tech .top-news-content .news-card.horizontal .wdt_amz{display:block; width:100%; margin:0}
    `;
  const gnextendedCommmon = `
        .m-scene.tech .btn-buy {padding: 0 10px 0px; font-size: 12px; color: #000; font-weight: bold; height: 28px; border-radius: 3px; background: #fed20a; line-height: 32px; display: block; margin-top: 5px; cursor: pointer; margin: auto; border: 0; outline: none; box-sizing: border-box;} 
        .stars-in-rating{position:relative;vertical-align:baseline;display:inline-block;color:#afafaf;overflow:hidden;height:20px;line-height:20px;}
        .stars-in-rating .filled-stars:before,.stars-in-rating .empty-stars:before{font-size:22px;letter-spacing:0px;line-height:22px;}
        .stars-in-rating .filled-stars.critic{color:#ff001f;}
        .empty-stars {height: 100%; vertical-align: top;}
        .filled-stars:before, .empty-stars:before {content: "\\2605\\2605\\2605\\2605\\2605"; font-size:18px; letter-spacing: 3px;line-height: 18px;display: inline-block;word-break: normal;}
        .filled-stars {position: absolute;left: 0;top: 0;height: 100%;overflow: hidden;}  
        .ui_slider .slider .slider_content li .slide span{display:block;}
        .ui_slider .slider .slider_content li .slide{width:165px;display:inline-block;vertical-align:top;box-shadow:0 0 5px #b8b8b8;white-space:normal;text-align:center;margin:5px 15px 5px 5px;padding:5px;min-height:210px;background:#fff;position:relative;}
        .ui_slider .slider .slider_content li .slide .prod_img{width:auto;height:80px;padding-top:10px;}
        .ui_slider .slider .slider_content li .slide img, .ui_slider .slider .slider_content li .slide amp-img{max-height:80px;width:auto;height:auto;max-width:100%; max-width: 100%; min-width: auto; min-height: auto;}
        .ui_slider .slider .slider_content li .slide .title{display:-webkit-box;}
        .ui_slider .slider .slider_content li .slide .title{font-size:1.2rem;line-height:2.0rem;max-height:4.0rem;font-weight:bold; margin-top:10px}
        .ui_slider .slider .slider_content li .slide .price_tag {font-size: 1.2rem; color: #d70000; font-weight: bold; font-family: arial; margin-top: 5px;}
        .ui_slider .slider .slider_content li .slide .price_tag b {color: #000; text-decoration: line-through; font-weight: bold; margin-left: 5px;}
        .ui_slider .slider .slider_content li .slide .price{margin-top:5px; height:21px}
        .ui_slider .slider .slider_content li .slide .price b{font-size:1.2rem; font-family:arial; font-weight:bold; vertical-align: top;}
        .symbol_rupees {position: relative;}
        .symbol_rupees:before {content: "\\20B9"; position: absolute;left: -13px;top: 0;}
        .ui_slider .slider .slider_content li .slide .btn_wrap {margin-top: 5px;}
        .ui_slider .slider .slider_content li .slide .vs{height:30px;width:30px;background:#b1b1b1;border-radius:50%;position:absolute;right:-35px;top:45px;color:#fff;font-size:11px;font-family:Arial, Helvetica, sans-serif;text-transform:uppercase;line-height:31px;}
        .ui_slider .slider .slider_content li:last-child .slide:last-child{margin:5px 15px 5px 5px}
        .ui_slider .slider .slider_content li .slide:last-child .vs{display:none;}
        .ui_slider .slider .btnPrev{top:0;bottom:0; left:5px}
        .ui_slider .slider .btnNext{top:0;bottom:0; right:5px}
        ul.scroll {list-style-type: none; white-space: nowrap; overflow-x: auto; overflow-y: hidden;}
        .top-caption {display: none; height: 25px; line-height: 25px; background: #fed20a; position: absolute; top: 0; left: 0; z-index: 10; padding: 0 5px 0 10px; font-size: 1.1rem; text-transform: capitalize;}
        .top-caption:before {background: linear-gradient(-235deg, #fed20a 50%, #ffffff 50%); height: 25px; top: 0; right: -20px; width: 20px; z-index: 1; position: absolute; content: "";}
        .top-caption:after{content: attr(data-attr)}
        .rd_more a, .rd_more span {font-size: 16px; color: #1a75ff; font-weight: bold; margin-right: 15px; display: block; position: relative; cursor: pointer;}
        .rd_more a:after, .rd_more span:after {content: ''; display: block; height: 6px; width: 6px; margin: auto; position: absolute; top: -3px; bottom: 0; transform: rotate(45deg); right: -13px; border-top: 2px solid #1a75ff; border-right: 2px solid #1a75ff;}
        .m-scene.tech .slider .hidden_layer {overflow: hidden; overflow-x: auto; white-space: nowrap;}
        .m-scene.tech .slider .slider_content li {display: inline-block;vertical-align: top;}
        .more-list{width:100%}
        .m-scene.tech .full-heading .section .top_section h2 {height: auto;font-size: 1.6rem;}
        .m-scene.tech .full-heading .section .top_section h2 span {height: auto; line-height: 25px; max-width:initial}
        .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{height:auto;font-size:1.6rem}
        .m-scene.tech .section h3 span, .m-scene.tech .section h2 span, .m-scene.tech .section h1 span{height:auto; line-height:25px; max-width:65%; padding:10px 10px 5px 0px}
  `;
  const gncompare = `
        .container-compare-show h1 {font-size: 1.8rem; line-height: 2.6rem; padding: 4% 3% 2%; margin: 0; background: #fff;}
        .container-compare-show .wdt_compare-gadgets.details {background: #fff; padding: 5px; width: auto;}
        .container-compare-show .wdt_compare-gadgets.details ul{display: flex; justify-content: space-between;}
        .container-compare-show .wdt_compare-gadgets.details ul li{flex: 1 1 0px; border-radius: 0; border: 1px solid #e3e3e3; margin: 0 5px 0 0; padding: 15px 5px 5px; box-sizing: border-box; position:relative;min-height:220px;white-space:normal;}
        .container-compare-show .wdt_compare-gadgets.details ul li:last-child{margin-right:0;}
        .container-compare-show .wdt_compare-gadgets.details ul li span {display: block; text-align: center; margin: 0 0 5px;}
        .container-compare-show .wdt_compare-gadgets.details ul li .img_wrap{margin-top:10px;}
        .container-compare-show .wdt_compare-gadgets.details ul li .img_wrap img, .container-compare-show .wdt_compare-gadgets.details ul li .img_wrap amp-img{width: auto; max-height: 80px; margin: 0 auto; min-width: auto; height: auto; min-height: auto;}
        .container-compare-show .wdt_compare-gadgets.details ul li .con_wrap h4{height:44px;font-weight:bold; font-size: 14px; line-height: 22px; color: #000; margin:0}
        .container-compare-show .wdt_compare-gadgets.details ul li .con_wrap .price_tag{font-size:1.2rem; color:#d70000; line-height: 20px; font-weight: bold; font-family: arial;}
        .container-compare-show .wdt_compare-gadgets.details ul li .btn_wrap{margin:5px 0 0 0;}
        .container-compare-show .wdt_compare-gadgets.details ul li .btn_wrap .btn-buy{width: 100%; font-size: 12px; height: 25px; line-height: 28px}
        .detailed-spec-show{margin-bottom:4%; background: #fff;}
        .detailed-spec-show .gn_table_layout{margin:0 0 20px;}
        .detailed-spec-show .gn_table_layout table tr td:first-child {width: 50%;}
        .detailed-spec-show .gn_table_layout:last-child{margin:0;}
        .detailed-spec-show .gn_table_layout table{margin:2%;width:96%; border-collapse: collapse;border-spacing: 0; table-layout:fixed;}
        .detailed-spec-show .gn_table_layout table td{font-size:14px;padding:12px 15px 7px;border:0;border-right:1px solid #d6d6d6; text-align:center; word-break: break-word;}
        .detailed-spec-show .gn_table_layout table td a{color: #044e97;}
        .detailed-spec-show .gn_table_layout table tr td:last-child{border:0;}
        .detailed-spec-show .gn_table_layout table tr th{font-size:15px;border:1px solid #d6d6d6;background:#eeeeee;color:#000;font-weight:bold;text-align:center;padding: 10px 5px;}
        .detailed-spec-show .gn_table_layout table tr:last-child{border-bottom:1px solid #d6d6d6;}
        .detailed-spec-show .gn_table_layout .spec{background:#a5b6c4;font-size:18px;color:#fff;text-align:center;display:block;padding:12px 15px 9px;position:relative;cursor:pointer;font-weight:bold;margin-bottom:2%;}
        .detailed-spec-show .gn_table_layout .spec + table{display:table;}
        .detailed-spec-show .gn_table_layout .charging-no{position:relative;height:28px;width:20px;color:#fff;font-size:1px;display:inline-block;vertical-align:top;}
        .detailed-spec-show .gn_table_layout .charging-yes{position:relative;height:28px;width:20px;color:#fff;font-size:1px;display:inline-block;vertical-align:top;}
        .detailed-spec-show .gn_table_layout table td .stars-in-value{display:inline-block;vertical-align:top;margin:2px 10px 0 0;}
        .detailed-spec-show .gn_table_layout table td .stars-in-value b{font-weight:bold;}
        .charging-yes {position: relative; height: 28px; width: 20px; color: #fff; font-size: 1px; display: inline-block; vertical-align: top;}
        .charging-yes:after {content: '\\2714'; font-size: 25px; font-weight: bold; position: absolute; top: 0; left: 0; height: 20px; line-height: 26px; color: #75da00;}
        .wdt_popular_slider{margin-bottom:4%;background:#fff;padding-bottom:3%;overflow:hidden;}
        .wdt_popular_slider .section{padding:0;}
        .wdt_popular_slider .slider .slider_content li .slide {box-shadow: none;padding: 0;margin: 0 35px 0 0; min-height: auto; background: transparent; width: 120px;}
        .wdt_popular_slider .slider .slider_content li .slide:last-child, .wdt_popular_slider .slider .slider_content li:last-child .slide:last-child {margin: 0 0 0 5px;}
        .wdt_popular_slider .slider .slider_content{white-space:nowrap;overflow:hidden;overflow-x:auto;width:97%;margin-left:3%;}
        .wdt_popular_slider .slider .slider_content ul li{flex-wrap:nowrap;display:inline-flex; box-shadow: 0 0 5px #b8b8b8; white-space: normal; text-align: center; margin: 5px 15px 5px 5px; padding: 5px; background: #fff; position: relative; flex-shrink:0;width:auto;min-height:135px;}
        .wdt_popular_slider .slider .slider_content ul li a{display: flex;}
  `;
  const gngadgetlist = `
        .gl_top_bar{background:#fff;margin:0;padding:15px 3% 0;overflow:hidden;clear:both;width:100%;}
        .gl_top_bar .section{padding:0}
        .gl_top_bar .top_section{margin:0 0 4%}
        .gl_top_bar p{font-size:1.2rem;line-height:2rem;margin-bottom:20px;}
        .gl_top_bar ul.gl_suggested_keywords li{display:inline-block;vertical-align:top;background:#e6e6e6;border:1px solid #dadada;white-space:nowrap;border-radius:18px;margin:0 15px 15px 0;}
        .gl_top_bar ul.gl_suggested_keywords li a{display:block;font-size:1.1rem;color:#000;height:34px;line-height:34px;padding:0 20px;}
        .gl_top_bar ul.gl_suggested_keywords li:last-child{margin-right:0;}
        .gadgets-in-list > ul > li{margin:0 3% 4%;border:1px solid #e3e3e3;padding:20px 3% 4%;position:relative;background:#fff;}
        .gl_list_mobiles{background:#fff;padding-bottom:4%;margin-bottom:4%;}
        .gadgets-in-list ul li.upcoming .top-caption, .gadgets-in-list ul li.rumoured .top-caption {display: block;}
        .gadgets-in-list ul li.upcoming, .gadgets-in-list ul li.rumoured {padding-top: 35px;}
        .gadgets-in-list ul li .top_spec {display: flex;flex-wrap: wrap;}
        .gadgets-in-list ul li .top_spec .gadget_img {width: 25%; display: inline-block; vertical-align: top; text-align: center;}
        .gadgets-in-list ul li .top_spec .gadget_img img, .gadgets-in-list ul li .top_spec .gadget_img amp-img {max-width: 60px; max-height: 100px; width: auto; min-width: auto; min-height: auto; height: auto;}
        .gadgets-in-list ul li .top_spec .gadget_detail {width: 75%; display: inline-block; vertical-align: top;}
        .gadgets-in-list .top_spec .gadget_detail span {display: block; color: #777; margin: 0 0 3px;}
        .gadgets-in-list .top_spec .gd_name a{font-size:1.4rem;line-height:2.4rem;max-height:4.8rem;font-weight:bold;}
        .gadgets-in-list .top_spec .gadget_detail .rating span{display:inline-block;vertical-align:top;position:relative;font-size:1.1rem;margin-right:10px;}
        .gadgets-in-list .top_spec .gadget_detail .rating span:first-child{margin:0 10px 0 0;}
        .gadgets-in-list .top_spec .gadget_detail .rating span:first-child:after{content:'|';position:absolute;right:-8px;}
        .gadgets-in-list .top_spec .gadget_detail .rating b{font-family:arial;}
        .gadgets-in-list .top_spec .gadget_detail .rating b small{color:#f5a623;font-size:1rem;}
        .gadgets-in-list .top_spec .gadget_detail .rating span a{color:#1a75ff;}
        .gadgets-in-list .top_spec .gd_price{font-size:1.6rem;font-family:arial;font-weight:bold;}
        .gadgets-in-list .top_spec .gadget_detail .gd_launch{font-size:1.1rem;line-height:2rem;}
        .gadgets-in-list ul li .top_spec .gadget_specs{margin-top:10px;}
        .gadgets-in-list ul li .top_spec .gadget_specs ul{display:flex;flex-wrap:wrap;justify-content:space-between;}
        .gadgets-in-list .top_spec .gadget_specs ul li{font-size:1.2rem;line-height:1.8rem;color:#666666;padding:0;margin:0 0 5px 15px;position:relative;width:44%;}
        .gadgets-in-list .top_spec .gadget_specs ul li:before{content:'';height:6px;width:6px;background:#666;border-radius:50%;position:absolute;top:9px;left:-15px;}
        .gadgets-in-list .top_spec .gadget_specs ul li label{font-weight:bold;}
        .gadgets-in-list .top_spec .gadget_specs ul li b{font-family:arial;font-weight:normal;}
        .gadgets-in-list .rd_more{display:block;text-align:right;margin-top:5px;width:100%;}
        .gadgets-in-list ul li .top_spec .gd_head{display:block;font-size:1.4rem;font-weight:bold;}
        .gadgets-in-list ul li .top_spec .gadget_buy{width:100%;margin-top:10px;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li{display:flex;flex-wrap:wrap;margin:0 0 5px;border:1px dashed #777;padding:10px 15px;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .txt{font-size:1.2rem;line-height:1.8rem;color:#525252;margin:0 0 10px;white-space:normal;max-height:3.6rem; width:100%}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .gd_price{font-size:1.4rem;font-weight:bold;color:#000;margin-top:5px;font-family:arial;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .gd_price b{text-decoration:line-through;color:#9b9b9b;margin:0 0 0 10px;font-size:1.1rem;font-weight:bold;line-height:1.8rem;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .btm{margin:0 0 0 auto;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .btm .amazon{margin:8px 10px 0 0;display:inline-block;vertical-align:top;}
        .gadgets-in-list .top_spec .dotted-box li .btm .btn-buy{text-align:center;float:right;width:auto;}
        .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .btm img, .gadgets-in-list ul li .top_spec .gadget_buy .dotted-box li .btm amp-img{width:65px;height:17px}
        .gl_latestGadgets.pwa {padding: 0 0 4%; margin-bottom: 4%; background: #fff;}
        .gl_latestGadgets.pwa .section h2, .gl_latestGadgets.pwa .section h2 span {height: auto;}
        .gl_latestGadgets.pwa .gn_table_layout {margin: 0 3%;max-width: 94%;}
        .gn_table_layout table {width: 100%; border-collapse: collapse; margin: 0 auto; border-spacing: 0; border: 0; table-layout: fixed;}
        .gl_latestGadgets .gn_table_layout table tr th {background: #e9e9e9; font-size: 1.3rem; color: #000; font-weight: bold; text-transform: capitalize; padding: 10px 5px; border:1px solid #d8d8d8}
        .gl_latestGadgets .gn_table_layout table tr td {font-size: 1.1rem; border: 1px solid #d8d8d8; text-align:center; padding: 10px 5px;}      
        .seo-btf-content.pwa {margin: 0 0 4%; background: #fff; padding: 4% 3%;}
        .seo-btf-content p {font-size: 1.2rem; line-height: 2rem; margin-bottom: 10px;}
        .seo-btf-content p strong {font-weight: bold; font-size: 1.1rem; line-height: 1.7rem;}
        .gadgetlist_body .wdt-inline-gadgets{margin-bottom:20px}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li.active a {border-bottom: 2px solid #000;}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li{display: inline-block; vertical-align: top;}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li a{min-width: 55px; width: auto; text-align: center; padding: 10px 5px 5px; border-bottom: 2px solid #e5e5e5; display:block; cursor: pointer; height: 20px;}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li.active label {font-weight: bold;color: #000;}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li label {font-size: 1.2rem; line-height: 1.6rem; display: block; cursor: pointer;}
        .gadgetlist_body .wdt-inline-gadgets ul.scroll li .svg-icons {display:none}
  `;
  const gngadgetshow = `
        .productdetails_body.pwa h1 {font-size: 1.8rem; line-height: 2.6rem; padding: 4% 3% 2%; margin: 0; background: #fff;}  
        .productdetails_body.pwa .section {padding: 0;}
        .productdetails_body.pwa .box-item{overflow:hidden; width:100%}
        .productdetails_body.pwa .product_summary {background: #fff; margin-bottom: 4%; padding: 0 3% 4%; overflow:hidden;}
        .productdetails_body.pwa .product_summary .product_slider {width: 100%;margin: 0 0 4%;}
        .productdetails_body.pwa .product_summary .product_slider ul.slider-tabs {display: block; overflow-x: auto; white-space: nowrap;}
        .productdetails_body.pwa .product_summary .product_slider ul.slider-tabs li {width: 210px; min-height: 200px; display: inline-block; vertical-align: top; margin: 0 10px 0 0; border: 1px solid #d1d1d1; height: 100px; text-align: center; position: relative;}
        .productdetails_body.pwa .product_summary .product_slider ul.slider-tabs li img, .productdetails_body.pwa .product_summary .product_slider ul.slider-tabs li amp-img {max-height: 150px; margin: auto; width: auto; height: auto; max-width: 91%; position: absolute; top: 0; bottom: 0; left: 0px; right: 0; min-width: auto; min-height: auto;}
        .productdetails_body.pwa .tabs_circle {padding: 0 0 15px; text-align: center; background: #fff;}
        .productdetails_body.pwa .tabs_circle .tabs_circle_list li {display: inline-block; border-radius: 20px; height: 35px; line-height: 36px; margin: 0 10px 0 0; white-space: nowrap; cursor: pointer; width: 45%; vertical-align: top; text-align: center; border: 1px solid #333; background: #fff; color: #333; font-size: 1.2rem;font-weight: 700;}
        .productdetails_body.pwa .product_summary .enable-read-more .caption {font-size: 1.2rem; line-height: 2.2rem; max-height: initial; -webkit-line-clamp: initial;}
        .productdetails_body.pwa .pd_suggested_slider .slider .slider_content {margin: 0 0 0 3%; width: 97%;}
        .productdetails_body.pwa .pd_suggested_slider{overflow:hidden}
        .pd_suggested_slider.ui_slider .slider .slider_content li .slide {min-height: 170px;}
        .pd_suggested_slider.ui_slider .slider .slider_content li .slide .title {height: 4rem;}
        .usrreviewswidget{width:100%}
        .usrreviewswidget .wdt_review_rate .rating-values {margin: 10px 3% 0;}
        .wdt_review_rate .user_review .caption {font-size: 1.2rem; color: #808080; text-align:center;}
        .wdt_review_rate .user_review span {color: #f90; font-size: 30px; margin-top:10px; text-align:center; font-family: arial; position: relative; font-weight: 700; display: block;}
        .wdt_review_rate .user_review small {font-family: arial; color: #000; font-size: 20px; font-weight: normal;}
        .wdt_review_rate .user_review .first_review{display:block; text-align:center; margin:10px 0 0 0}
        .productdetails_body.pwa .pd-accordion .pd-spec-show {margin: 0 3%;}
        .pd-spec-show .gn_table_layout .spec {font-size: 1.4rem; color: #000; text-align: left; display: block; padding: 9px 15px 12px 3px; position: relative; cursor: pointer; font-weight: bold;}
        .gn_table_layout table {border: 1px solid #d6d6d6; border-bottom: none; margin-bottom: 20px; width: 100%; border-collapse: collapse; border-spacing: 0; table-layout: fixed;}
        .gn_table_layout table td {font-size: 1.2rem; padding: 9px 15px; border: 0; border-bottom: 1px solid #d6d6d6; text-align: left; color: #333;}
        .gn_table_layout table tr td:first-child {font-weight: bold; width: 30%;}
        .price-widget.gn_table_layout table {width: 94%;margin: 0 3%;}
        .price-widget table tr:nth-child(odd) {background-color: #fafafa;}
        .gadgetslider li .proditem .prod_dec {margin-top: 5px; display: block;}
        .gadgetslider li .proditem {width: 150px; box-shadow: 0 0 5px #b8b8b8; white-space: normal; text-align: center; margin: 5px 0 0 0; padding: 5px 10px 10px; background: #fff; position: relative; height: 100%; box-sizing: border-box;}
        .gadgetslider li .proditem .prod_img{height:120px; padding:10px 0 0; display:block}
        .gadgetslider li .proditem h4{margin:0}
        .gadgetslider li .proditem h4 .text_ellipsis{max-height: 4.4rem; height:4.4rem;   -webkit-line-clamp: 2;}
        .gadgetslider li .proditem .prod_img img, .gadgetslider li .proditem .prod_img amp-img {max-width: 100%; max-height: 100px; width: auto; height: auto; min-height: auto; min-width: auto;}
        .gadgetslider li .proditem .prod_dec .price_tag {font-size: 1.4rem; font-weight: bold; margin-bottom: 5px; display: inline-block;}  
        .m-scene.tech .view-horizontal {flex-wrap: nowrap;overflow-x: auto;}
        .m-scene.tech .view-horizontal li.news-card.vertical {padding-right: 15px;}
        .wdt_popular_slider {margin-bottom: 4%; background: #fff; padding-bottom: 3%; overflow: hidden;}
        .wdt_popular_slider .slider .slider_content li .slide {box-shadow: none;padding: 0;margin: 0 35px 0 0; min-height: auto; background: transparent; width: 120px;}
        .wdt_popular_slider .slider .slider_content li .slide:last-child, .wdt_popular_slider .slider .slider_content li:last-child .slide:last-child {margin: 0 0 0 5px;}
        .wdt_popular_slider .slider .slider_content{white-space:nowrap;overflow:hidden;overflow-x:auto;width:97%;margin-left:3%;}
        .wdt_popular_slider .slider .slider_content ul li{flex-wrap:nowrap;display:inline-flex; box-shadow: 0 0 5px #b8b8b8; white-space: normal; text-align: center; margin: 5px 15px 5px 5px; padding: 5px; background: #fff; position: relative; flex-shrink:0;width:auto;min-height:135px;}
        .wdt_popular_slider .slider .slider_content ul li a{display: flex;}
        .productdetails_body.pwa .wdt_varients {background-color: #fff; border-bottom: none; width: 100%;}
        .productdetails_body.pwa .wdt_varients ul {margin: 10px 3% 0;}
        .productdetails_body.pwa .wdt_varients ul li {display: block;margin: 0 0 10px;font-size: 1.2rem;line-height: 2.0rem;}
        .productdetails_body.pwa .wdt_varients ul li a{color:#1f81db}
  `;
  const gngadgethome = `
        .photo_video_section{background:#282e34;padding:2% 0 4%;margin:0 0 4%; display: flex; width: 100%;}
        .photo_video_section .section{background:transparent;}
        .photo_video_section .section h2{border-bottom:0;margin:0;}
        .photo_video_section .section h2 span{background:transparent;padding:0;color:#fff;}
        .photo_video_section .section h2 span a{color:#fff;}
        .photo_video_section .section h2 span:before{border:0;}
        .photo_video_section .section .top_section .read_more{display:none;}
        .photo_video_section .section .top_section{margin:0}
        .photo_video_section .section .sub-section{margin:0 0 15px;}
        .photo_video_section .section .sub-section .sub-list li a{color:#fff;}
        .photo_video_section ul li.news-card.vertical{margin-right:4%;}
        .photo_video_section ul li.news-card.vertical .img_wrap img, .photo_video_section ul li.news-card.vertical .img_wrap amp-img{width:250px;}
        .photo_video_section ul li.news-card.vertical .con_wrap .text_ellipsis{color:#fff;}
        .photo_video_section ul li.news-card.vertical .con_wrap .section_name{display: none;}
        .photo_video_section.video ul li.news-card.vertical{margin-right:4%; margin-bottom:0}
        .photo_video_section.video ul li.news-card.vertical .img_wrap img, .photo_video_section.video ul li.news-card.vertical .img_wrap amp-img{width:200px;}
        .photo_video_section.video ul li.lead .con_wrap{margin:10px 0 0 0;}
        .photo_video_section.video ul li.lead .con_wrap .text_ellipsis{color:#fff;}
        .photo_video_section .view-horizontal {flex-wrap: nowrap; overflow-x: auto;}
        .mobile_body .trending-bullet{text-align:left;border:0;margin:0 3%;padding-top:0;padding-bottom:0;max-width:94%;}
        .trending-bullet h3, .list-trending-now h3{font-size:1.8rem;margin:0 0 10px;padding-top:3px;font-weight:bold;display:block;height:40px;position:relative;border-bottom:2px solid #dfdfdf;width:100%;text-align:left;}
        .trending-bullet h3 span, .list-trending-now h3 span{height:40px;line-height:40px;padding:0 10px 0 0;display:inline-block;position:relative;font-weight:bold;color:#000;}
        .trending-bullet h3 span:before, .list-trending-now h3 span:before{content:'';position:absolute;bottom:-2px;left:0;right:0;border-bottom:2px solid #000;}
        .trending-bullet a{font-size:1.1rem;line-height:1.9rem;color:#000;margin:0 0 5px 0;display:inline;vertical-align:top;font-weight:bold;}
        .trending-bullet a:after{margin:0 5px;content:'|';font-size:.9rem;vertical-align:top;}
        .trending-bullet a:last-child:after {margin: 0; content: '';}
  `;
  const topicsCss = `
        .section-topics{background: #fff; overflow:hidden}
        .section-topics h1 {font-size: 1.6rem; line-height: 2.4rem; margin:0}  
        .section-topics .tabs-circle{margin: 4% 3%; text-align: center; position: relative;}
        .section-topics .tabs-circle:after{content: ''; position: absolute; top: 0; bottom: 0; margin: auto; height: 1px; background: #b9b9b9; left: 0; right: 0; z-index: -1;}
        .section-topics .tabs-circle ul{display: flex;}
        .section-topics .tabs-circle ul li.active{background: #3f3f41; border-color: #3f3f41; color: #fff;font-size: 1.2rem;}
        .section-topics .tabs-circle ul li:first-child{text-transform: uppercase;}
        .section-topics .tabs-circle ul li:last-child{margin-right: 0;}
        .section-topics .tabs-circle ul li{margin-right: 1%; width:24%; height: 30px; line-height: 33px; background: #f0f0f1; text-align: center;}
        .section-topics .tabs-circle ul li a{font-size: 1.2rem; display: block; padding: 0 3%;}
        .section-topics .box-item.ampvideos .news-card.lead .con_wrap {margin-left: 0; margin-right: 0;}
        .section-topics .rest-topics .news-card.horizontal {padding-left: 0;padding-right: 0;flex-wrap: nowrap;flex: auto;}
        .section-topics .box-item.ampphotos {margin-bottom: 4%; padding: 0 3%;}
        .section-topics .box-item.ampphotos .news-card.vertical {padding: 0 5px;}
        .section-topics .news-card.lead .con_wrap .text_ellipsis{max-height: initial; -webkit-line-clamp: initial;}
        .section-topics .date {font-size: 1.1rem; color: #7d7d7d; margin: 0 3% 0; max-width: 94%;}
        .section-topics .player-stats .con_stats{width:100%; background:#fff; margin-bottom:4%}
        .section-topics .player-stats .profile-date {font-size: 12px; line-height:18px; color: #a0a0a0; margin: 5px 0 0; font-family: Arial, Helvetica, sans-serif;}
        .section-topics .player-stats .lead-topics {margin: 0 0 20px; width: 100%;}
        .section-topics .player-stats .lead-topics .news-card.lead .con_wrap {margin: 0;}
        .section-topics .player-stats .lead-topics .news-card.lead {margin: 0; box-sizing: border-box; padding: 10px; box-shadow: 0 1px 2px 1px rgba(0,0,0,0.1); overflow:hidden}
        .section-topics .player-stats .lead-topics .news-card.lead .img_wrap {float: left; margin: 0 10px 0 0; width:auto;}
        .section-topics .player-stats .lead-topics .news-card.lead .img_wrap img, .section-topics .player-stats .lead-topics .news-card.lead .img_wrap amp-img {width: 120px;}
        .section-topics .player-stats .txt-wrap .only-txt, .section-topics .player-stats .profileDesc {font-size: 1.45rem; line-height: 2.7rem; margin: 0 3%; display: block;}
        .dynamicpage {padding: 3%;box-sizing: border-box;}
        .section-topics.dynamicpage .player-stats .profileDesc {margin: 0; width: 100%;}
        .section-topics.dynamicpage .player-stats .profileDesc a {color:#1f81db;}
        table.tableContainer, .tableContainer table{width:100%; border-collapse:collapse; margin:15px auto; border-spacing: 0px; border:0;}
        table.tableContainer tr td, table.tableContainer tr th, .tableContainer table tr td{text-align:left; font-size:1.5rem; padding: 10px 10px 8px; line-height: 1.8rem; border: solid 1px #d3d3d3; color: #000;}
        table.tableContainer tr th{font-weight: 600;}
        .bulletContent {list-style-type: disc; padding-left:25px;}
        .dynamicpage .wdt_next_sections {background:#e6ecee;padding:0 15px 10px 15px;margin:10px 0;}
        .dynamicpage .section, .dynamicpage .view-horizontal {padding:0;}
        .dynamicpage .accordion-container {font-size: 1.2rem;line-height: 1.8rem;}
        .dynamicpage .accordion-container > ol {padding:0;}
        .dynamicpage .accordion-container > ol > li {list-style-type: decimal-leading-zero;font-weight: bold;cursor: pointer;list-style-position: inside;border-bottom: 1px dashed rgba(0, 0, 0, 0.3);padding: 10px 0;position: relative;}
        .dynamicpage .accordion-container .description {margin-top: 10px;font-weight: normal;cursor: initial;}
        .dynamicpage .sourcedate {margin: 0; display: block; font-size: 1.2rem; color: #a0a0a0;}
        .dynamicpage .sourcedate a {color: #1f81db;}
        .nodata_txt_icon {text-align:center; line-height:60px;}
        .source_sharing {padding: 0 0 10px;margin: 0 0 10px;border-bottom: 1px solid #c3c3c3;width: 100%;overflow: hidden;}
        .section-topics.glossary-list{padding:0;}
        .section-topics.glossary-list .tabs-circle{margin-bottom:0;}
        .section-topics.glossary-list .tabs-circle ul{flex-wrap:nowrap;overflow-x:auto;}
        .section-topics.glossary-list .tabs-circle ul li{margin:0 20px 10px 0;border-radius:50%;border:solid 1px #575960;background:#fff;}
        .section-topics.glossary-list .tabs-circle ul li, .section-topics.glossary-list .tabs-circle ul li span{height:42px;width:42px;align-items:center;justify-content:center;text-transform:uppercase;color:#1f81db;font-size:16px;padding:0;display:flex;line-height:normal;flex-shrink:0;overflow:hidden;}
        .section-topics.glossary-list .tabs-circle ul li.active, .section-topics.glossary-list .tabs-circle ul li span.active{background:#000;color:#fff;}
        .section-topics.glossary-list .tabs-circle:after{display:none;}
        .section-topics.glossary-list .wdt_glossary{overflow:hidden;clear:both;}
        .section-topics.glossary-list .wdt_glossary .section{padding:0;}
        .section-topics.glossary-list .wdt_glossary .box-item{padding-bottom:0;}
        .section-topics.glossary-list .wdt_glossary .box-item .news-card{margin-bottom:5px;border:0;padding-bottom:5px;}
        .section-topics.glossary-list .wdt_glossary .box-item .news-card .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;-webkit-line-clamp:2;word-break:break-word;}

  `;

  const stickyWP = `
        .share_container .share_whatsapp{position:fixed;right:0;bottom:223px;width:44px;height:44px;background:#2bae1c;border-radius:15px 0px 0px 15px;z-index:98;display:block;overflow:hidden;box-shadow:-1px -1px 3px 0px rgba(0, 0, 0, 0.5);}
        .share_container .share_whatsapp a{display:block;padding:12px 12px 10px 12px;}
        .share_container amp-social-share[type="whatsapp"]{position: absolute; top: -3px; left: 0; right: -3px; bottom: 0; margin: auto; background-color: #2bae1c;}
        .socialsharewrap{margin: 4px; text-align: center; line-height: 0; min-height:34px;}
        .socialsharewrap [class*="amp-social-share"]{vertical-align:top}
        .share_container .share_icon{background:#f2f2f2;display:block;position:fixed;border-radius:0 0 0 15px;right:0;width:44px;height:44px;box-shadow:-1px 1px 3px 0px rgba(0, 0, 0, 0.5);bottom:180px;z-index:98;}
        .share_container .share_icon a{padding:13px 10px 11px 14px;display:block;}
        .share_container .share_icon svg{fill:#000;width:15px;vertical-align:top;}
        amp-social-share[type="system"]{width: 25px; height: 25px; display: inline-block; margin: 0 10px; vertical-align: middle;}
    `;
  const webstories = `
      .mobile_body .news-card.vertical.webstories .img_wrap a {position: relative; display: block;}
      .mobile_body .news-card.vertical.webstories .img_wrap a img, .mobile_body .news-card.vertical.webstories .img_wrap a amp-img {border-radius: 8px; overflow: hidden;}
      .mobile_body .news-card.vertical.webstories .img_wrap a:before {content: ""; background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzOCAzMyI+CiAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJub256ZXJvIiBzdHJva2U9IiNmZmZmZmYiIHN0cm9rZS13aWR0aD0iMS41NiI+CiAgICA8cmVjdCB4PSI4LjU4IiB5PSIuNzgiIHdpZHRoPSIxOC43MiIgaGVpZ2h0PSIyOS42NCIgcng9IjMuMTIiLz4KICAgIDxwYXRoIGQ9Ik0zMC40MiA1LjQ2aDIuODA4YzEuMDM0IDAgMS44NzIgMS4yMSAxLjg3MiAyLjcwM3YxNC44NzRjMCAxLjQ5Mi0uODM4IDIuNzAzLTEuODcyIDIuNzAzSDMwLjQyTTUuNDYgNS40NkgyLjY1MkMxLjYxOCA1LjQ2Ljc4IDYuNjcuNzggOC4xNjN2MTQuODc0YzAgMS40OTIuODM4IDIuNzAzIDEuODcyIDIuNzAzSDUuNDYiLz4KICA8L2c+Cjwvc3ZnPg==") no-repeat; position: absolute; top: auto; left: 0; right: 0; bottom: 30px; margin: auto; width: 25px; height: 22px; z-index:1;}
      .mobile_body .news-card.vertical.webstories .img_wrap a:after {content: ""; border-top: 3px dashed #fff; height: 20px; left: 10px; right: 10px; margin: auto; position: absolute; bottom: 0;}
      .mobile_body .view-horizontal li.vertical.webstories .img_wrap img, .mobile_body .view-horizontal li.vertical.webstories .img_wrap amp-img {width: 140px;}
  `;
  const petrolWidget = `
      .pdwidget {display: flex; margin: 0 3% 2%; padding: 0 15px 5px; width: 100%; white-space: nowrap;}  
      .pdwidget > a {margin: auto;}
      .pdwidget .pd, .pdwidget .pp {display: inline-block; vertical-align: top; padding: 0 5px; background: #fff; color: #333; height: 28px; line-height: 29px; border: 1px solid #d6d6d6; border-right: 0;}
      .pdwidget .pd svg, .pdwidget .pp svg {display: inline-block; vertical-align: top; margin: 6px 5px 0 0;}
      .pdwidget .pd h3, .pdwidget .pp h3 {display: inline-block; margin: 0 10px 0 0; font-size: 1.1rem; font-weight: bold; vertical-align: top; line-height: 31px; color:#000;}
      .pdwidget .pd span, .pdwidget .pp span {font-weight: bold; font-size: 1.1rem; font-family: arial; color: #000; vertical-align: top;}
      .pdwidget .checplist {display: inline-block; vertical-align: top; text-transform: uppercase; font-family: arial; font-size: 0.85rem; text-align: right; background: #000; color: #fff; padding: 4px 5px 2px; height: 30px; box-sizing: border-box;}
      .pdwidget .checplist .tdate {margin: 0 0 0 4px;}
      .pdwidget .checplist .cp {padding-top: 2px; font-weight: bold;}
  `;
  const sports_css = `
      .mini_schedule{white-space: nowrap; margin: 0 0 4%; background: #fff; padding: 3% 0 1%;}
      .mini_schedule .wdt_schedule .match_final_status .match_venue{height: 1.5rem;}
      .mini_schedule .wdt_schedule .match_final_status .status_live{position: absolute; left: 0; right: 0; bottom: -1px; width: 30px; margin: auto;}
      .mini_schedule .wdt_schedule{position:relative; overflow:hidden}
      .mini_schedule .view-horizontal {flex-wrap: nowrap; overflow-x: auto;}
      .wdt_schedule{border-radius:8px; font-family:arial;padding:5px 10px 5px;margin:5px 0 5px 4%;display:inline-block;width:70%;vertical-align:top;min-height:100px; box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.24); background: #fff;}
      .wdt_schedule:last-child{margin-right:4%;}
      .wdt_schedule .liveScore{margin-top:5px;}
      .wdt_schedule .match_date_venue{font-size:1.0rem;line-height:1.5rem; text-align:center;white-space:normal;text-transform:uppercase;}
      .wdt_schedule .match_date_venue span{display:block; font-weight:bold;line-height:2rem;}
      .wdt_schedule .teama, .wdt_schedule .teamb{width:40%;}
      .wdt_schedule .teama .teaminfo, .wdt_schedule .teamb .teaminfo{overflow:hidden;clear:both;display:block; margin-top:2px}
      .wdt_schedule .teama amp-img{float:left;margin:2px 5px 0 0;width:35px;}
      .wdt_schedule .teama img{margin:0;width:35px;}
      .wdt_schedule .teama .con_wrap{display:inline-block;vertical-align:top;}
      .wdt_schedule .teamb{text-align:right;}
      .wdt_schedule .teamb .con_wrap{display:inline-block;vertical-align:top;}
      .wdt_schedule .teamb .teaminfo amp-img{float:right;margin:2px 0 0 5px;width:35px; border-radius: initial;}
      .wdt_schedule .teamb .teaminfo img{margin:0;width:35px;}
      .wdt_schedule .teamb .country{text-align: right; display: block; width: auto;}
      .wdt_schedule .country{display:block;font-size:1.2rem;font-weight:bold;line-height:normal;text-transform:uppercase;text-align:left;width:50px;}
      .wdt_schedule .country.btm {width: 50px; clear: both; text-align: center; padding-top: 10px;}
      .wdt_schedule .teama .country.btm {float: left;}
      .wdt_schedule .teamb .country.btm {float: right;}
      .wdt_schedule .score{font-size:1.4rem;font-weight:bold;display:block;margin:0 0 1px;}
      .wdt_schedule .over{font-size:1.1rem; display:block;}
      .wdt_schedule .match_final_status{font-size:1rem;line-height:1.4rem; text-align:center;white-space:normal;margin-top:10px;text-transform:uppercase; color: #000;}
      .wdt_schedule .match_final_status .time{margin-right:5px; color:#000;}
      .wdt_schedule .match_final_status .status_live{font-size:10px;display:inline-block;padding:0 7px 0 15px;height:17px;line-height:17px;text-transform:uppercase;border-radius:3px;position:relative;}
      .wdt_schedule .match_final_status .match_venue{font-size:1rem;line-height:1.5rem; overflow: hidden; display: block; color: #999;}
      .wdt_schedule .versus{width:10%; text-align:center}
      .wdt_schedule .versus span{height: 16px; width: 23px; border-radius: 50%; display: inline-block; font-size: 1rem; text-align: center; padding: 6px 0 0 0; font-weight: bold; margin-top:5px; background: #8e8e8e; color: #fff;}   
      .wdt_schedule .teama .team_color {float: left; margin: 2px 5px 0 0; width: 18px; height: 18px; border-radius: 50%;}
      .wdt_schedule .teamb .team_color {float: right; margin: 2px 0 0 5px; width: 18px; height: 18px; border-radius: 50%;}
      .wdt_schedule .teaminfo .rcb{background: #bd243f;}
      .wdt_schedule .teaminfo .csk{background: #fdd103;}
      .wdt_schedule .teaminfo .sh{background: #ffa603;}
      .wdt_schedule .teaminfo .dc{background: #324b9e;}
      .wdt_schedule .teaminfo .kxip{background: #ea4354;}
      .wdt_schedule .teaminfo .kkr{background: #4b2d83;}
      .wdt_schedule .teaminfo .mi{background: #027dc8;}
      .wdt_schedule .teaminfo .rr{background: #084ca2;}
      .wdt_schedule .teaminfo .default-color{background: #e44650;}
      .wdt_schedule .calendar {width: 20%; text-align: center; font-family: Arial, Helvetica, sans-serif;}
      .wdt_schedule .calendar .mt-mumber {font-size: 10px; color: #fff; background: #e44650; display: block; border-radius: 10px 10px 0 0; padding: 10px 0 4px; position: relative;}
      .wdt_schedule .calendar .mt-date {font-size: 11px; color: #000; background: #eaebc2; display: block; font-weight: bold; border-radius: 0 0 10px 10px; padding: 12px 0;}
      .schedule_intro{background: #fff;}
      .schedule_intro .section {padding: 0;}
      .matchNvenue {background: #fff; padding: 0 3% 0; text-align: right; min-height: 7px;}
      .matchNvenue select {width: 35%; padding: 5px 5px 3px; font-size: 12px; margin-right: 5%; outline: none;border: solid 1px #bababa; color: #808080; background: #fff;}
      .matchNvenue select:last-child {margin: 0;}
      .schedule_container{margin: 0 0 4%; background: #fff; padding: 4% 0;}
      .schedule_container .wdt_schedule {display: block; width: auto; margin: 0 3% 4%;}
      .hide {display: none;}
      .playerOftheDay {padding-top: 15px; background: #fff; margin: 15px 0;}
      .playerOftheDay a {display: flex; position: relative; width: 100%; padding-bottom: 15px;}
      .playerOftheDay a img, .playerOftheDay a amp-img {width: 108px; height: 81px; min-width: auto; min-height: auto; max-width: initial; max-height: initial;}
      .playerOftheDay a amp-img{margin: 0 10px 0 15px;}
      .playerOftheDay a .rgt {display: flex; flex-wrap: wrap; align-content: space-between; width:60%}
      .playerOftheDay .rgt h4 {font-size: 1.6rem; margin: 0 0 10px 0;}
      .playerOftheDay a .rgt .player_name .text_ellipsis {font-size: 1.4rem; line-height: 2rem; max-height: 2rem; -webkit-line-clamp: 1; font-weight: bold;}
  `;

  const apnaBazaar_css = `
      .wdt_apnaBazaar {display: block; margin:10px 0; background: #fff;}
      .wdt_apnaBazaar h3.abz-head{margin:0}
      .wdt_apnaBazaar h3.abz-head a.text_ellipsis {font-size: 1.4rem; line-height: 2.2rem; max-height: 6.6rem; -webkit-line-clamp: 3; overflow:hidden}
      .wdt_apnaBazaar .abz-slider {display:flex; flex-wrap: wrap; flex-direction: column-reverse; margin: 10px 0;}
      .wdt_apnaBazaar .abz-slider-content {border: 1px solid #d1d1d1; position: relative; background: #fff; width: 100%; margin:0}
      .wdt_apnaBazaar .abz-slider-content .imgwrap{height:226px;padding:50px 10px 20px;position:relative;box-sizing:border-box;}
      .wdt_apnaBazaar .abz-slider-content .imgwrap img, .wdt_apnaBazaar .abz-slider-content .imgwrap amp-img{max-height:220px;width:auto;height:auto;max-width:90%;position:absolute;top:0;bottom:0;left:0;right:0;margin:auto; min-width: auto; min-height: auto;}
      .wdt_apnaBazaar .abz-slider-content .imgwrap .discount {background: #d13d28; position: absolute; z-index: 1; font-size: 12px; height: 40px; width: 40px; border-radius: 50%; color: #fff; text-align: center; line-height: 14px; font-family: arial; font-weight: bold; top: 5px; left: 5px; display: flex; align-items: center; padding-top: 2px; box-sizing: border-box;}
      .wdt_apnaBazaar .abz-description{display:flex;flex:1;flex-direction:column;justify-content:space-between; margin:0}
      .wdt_apnaBazaar .abz-description .price-box{width:100%; margin:0}
      .wdt_apnaBazaar .abz-description .price_tag{margin:0 0 5px;}
      .wdt_apnaBazaar .abz-description .price_tag a{font-family:arial; color:#d13d28;font-size:18px; font-weight:bold}
      .wdt_apnaBazaar .abz-description .price_tag b{color:#333;margin-left:10px;font-size:14px;display:inline-block;vertical-align:top;font-weight:normal;}
      .wdt_apnaBazaar .abz-description .price_tag b span{text-decoration:line-through;}
      .wdt_apnaBazaar .abz-description .abz-btn{display:flex;justify-content:space-between; margin:0; align-items:center; flex-direction: row-reverse;}
      .wdt_apnaBazaar .abz-description .abz-btn a.btn{padding:0 10px;color:#000;font-weight:bold;background:#fed20a;font-family:arial;display:inline-block;text-transform:uppercase;font-size:14px;height:40px;line-height:42px; text-align:center; width:60%}
      .wdt_apnaBazaar .abz-description .abz-btn .abz-icon img, .wdt_apnaBazaar .abz-description .abz-btn .abz-icon amp-img{width:80px; height:21px}
  `;

  const movieShowCss = `
            .movieshow .movieshowlft {width:100%;}
            .movieshow {background:#fff;}
            .movieshow .moviewshowcard {background:#282e34;}
            .movieshow .moviewshowcard .mb10 {margin-bottom:10px;}
            .movieshow .moviewshowcard .movieinfo {padding:15px; color:#fff;}
            .movieshow .moviewshowcard .moviemeta {font-size:1.2rem; line-height:1.2rem;}
            .movieshow .moviewshowcard svg {margin-right:5px; vertical-align:middle;}
            .movieshow .moviewshowcard h1 {font-size:1.8rem; line-height:2.4rem; font-weight:bold; margin-bottom:5px;}
            .movieshow .moviewshowcard .ms_button {border:1px solid #fff; border-radius:15px; height:30px; padding:0 15px; width:auto; margin:5px 0 15px; text-align:center; font-size:1.2rem; background:transparent; color:#fff;}
            .movieshow .moviewshowcard .ms_poster {margin-top:10px; overflow:hidden;}
            .movieshow .moviewshowcard .ms_poster .ms_posterimg {width: 135px; float:left;}
            .movieshow .moviewshowcard .ms_poster .ms_posterinfo { padding-left:150px; box-sizing:border-box;}
            .movieshow .moviewshowcard .ms_share {margin: 10px 0; padding:10px 0 5px; border-top: solid 1px #4d4d4d; border-bottom: solid 1px #4d4d4d;max-height:45px; overflow:hidden;}
            .movieshow .moviewshowcard .ms_share .sharetxt {display:inline-block; font-size:1.2rem; vertical-align:text-top;}
            .movieshow .moviewshowcard .ms_share .wdt_social_share {display:inline-block; text-align:left; float:none; width:auto;}
            .movieshow .moviewshowcard .ms_synopsis h3 {font-size:1.4rem; font-weight:bold; margin:0 0 10px;}
            .movieshow .moviewshowcard .ms_trailers {padding-bottom: 15px;}
            .movieshow .moviewshowcard .ms_trailers .section {padding: 0 15px; box-sizing:border-box;}
            .movieshow .moviewshowcard .ms_trailers .section h2 span {font-size: 1.4rem; color:#fff; font-weight:normal;}
            .movieshow .moviewshowcard .ms_trailers .slider ul li .con_wrap .text_ellipsis {color:#fff;}
            .moviewidgets {padding: 10px 0;background-color: #f6f6f6;}
            .moviestry {padding: 15px;font-size: 1.2rem;line-height: 2rem;color: #000;}
            .section .top_section {margin:0;}
            .section.noborder h1, .section.noborder h2, .section.noborder h3 {border-bottom: none;font-size: 1.6rem;}
            .section.noborder h1 a:before, .section.noborder h1 span:before, .section.noborder h2 a:before, .section.noborder h2 span:before, .section.noborder h3 a:before, .section.noborder h3 span:before {border-bottom: none}
            .movieshow .adcards {display: flex; justify-content:space-between; margin:10px 0;}
            .movieshow .adcards .box-item {width: 49%;}
            .movieshow .star-candidates {width:100%; box-sizing:border-box;}
            .movieshow .star_candidates_widget {background:#e6ecee; padding:0 10px 20px; box-sizing:border-box; width:100%;}
            .movieshow .star_candidates_widget .h2WithViewMore {margin: 0 0 10px 10px;}
            .movieshow .star_candidates_widget h2 {font-size:22px;}
            .movieshow .slider {white-space:nowrap; overflow:auto; max-width:100%; padding:0 10px; box-sizing:border-box;}
            .movieshow .slider .slider_content li, .movieshow .castprofile .slider .slider_content li .slide {display:inline-block; white-space:normal;vertical-align:top;}
            .movieshow .castprofile .slider .slider_content li {display:inline-block; white-space:nowrap;}
            .movieshow .candidate_box {height:100%; box-sizing:border-box; font-weight:bold; border-radius: 5px; box-shadow: 0 0 4px 0 #c8d5d9; background-color: #ffffff; padding: 10px; text-align:center; margin: 2px;}
            .movieshow .candidate_box .candidate_imgwrap {display:inline-block; position:relative; margin-bottom:10px}
            .movieshow .candidate_box .candidate_imgwrap img, .movieshow .candidate_box .candidate_imgwrap amp-img {width:96px; height:96px; border-radius:50%; border:1px solid #e6ecee; padding:8px; box-sizing:border-box; object-fit: cover;}
            .movieshow .candidate_box .candidate_name {font-size: 1.4rem; display:block; text-align: center; max-width: 100%; overflow:hidden; text-overflow:ellipsis; margin: 0 auto; white-space: nowrap;}
            .movieshow .candidate_box .constituency {font-size: 1.2rem; color:#8c8c8c; display:block; margin: 5px 0 10px;}
            .movieshow .metaitems {display:inline-block; margin-right: 10px;line-height:2rem;}
            .movieshow .metafirst {display:inline-block; border-right: solid 1px #575757; padding-right:10px; margin-right:10px;}
            .movieshow .metafirst svg {vertical-align:middle;}
            .movieshow .dotlist {display:inline-block; padding-left:13px; margin-left:7px;position: relative;}
            .dotlist::before {content:"";width:6px; height:6px; background:#7a7a7a; display:inline-block; border-radius: 50%;position: absolute;left: 0;top: 7px;}
            .dotlist:nth-child(2) {padding: 0;margin: 0;}
            .dotlist:nth-child(2)::before {display: none;}
            .movieshow .short-text {font-size:1.4rem; line-height:2rem;}
            .moviestry {padding: 15px; font-size: 1.4rem; line-height: 2.4rem; color: #000;}
            .movieshow .rating {width: 100%; padding: 0; text-align: left; margin:15px 0;}
            .movieshow .rating .tbl_column {text-align: center; width: 50%; position: relative;}
            .movieshow .rating .txt {font-size: 1.1rem;display:block; color:#fff;margin-top:5px;}
            .movieshow .rating .count {font-size: 1.4rem; display: inline-block; font-family: arial; color:#fff;position: relative; padding-left:25px;}
            .movieshow .rating .count b {font-size: 2.0rem; font-weight: 400;}
            .movieshow .empty-stars {color:#d8d8d8;}
            .movieshow .rating .critic:before {color:#ff001f;}
            .movieshow .rating .ratestar:before {color:#fc0;}
            .movieshow .rating .users:before {color: #00b3ff;}
            .movieshow .rating .critic:before, .movieshow .rating .users:before {content: "\\2605"; font-size: 20px; line-height: 30px;position: absolute;top: -4px;left: 0;}
            .movieshow .rating .tbl_column::after {content: ""; position: absolute; right: 0; top: 4px; width: 1px; height: 40px; background-color: #888888;}
            .movieshow .rating .tbl_column:last-child::after {display: none;}
            .msnews .news-card.horizontal {margin: 0 0 4%; padding: 0 3% 4%; box-sizing: border-box; background:transparent;}
            .msnews .news-card.horizontal .con_wrap .text_ellipsis {overflow:visible; -webkit-line-clamp: unset; white-space:normal;}
            .share_container .share_whatsapp {position: fixed;right: 0;bottom: 223px;width: 44px;height: 44px;background: #2bae1c;border-radius: 15px 0 0 15px;z-index: 98;display: block;overflow: hidden;box-shadow: -1px -1px 3px 0 rgb(0 0 0 / 50%);}
            .share_container amp-social-share[type=whatsapp] {position: absolute;top: -3px;left: 0;right: -3px;bottom: 0;margin: auto;background-color: #2bae1c;}
            .news_card_source {font-size: 1.1rem; line-height:1.8rem; color: #9d9d9d; margin:-10px 15px 10px;}
            .socialsharewrap{margin: 4px; text-align: center; line-height: 0; min-height:34px;}
            .gaanaiframe {margin: 2%;}
            .movieshow .moviereview .section.noborder{padding: 0; margin-top: 0;}
            .movieshow .moviereview .section.noborder .top_section {margin: 0;}
            .movieshow .moviereview .section.noborder .top_section h2, .movieshow .moviereview .section.noborder .top_section span {height: auto; line-height: normal;}
            .movieshow .moviereview .news_card_source { margin: 0 0 10px;}
            .movieshow .moviereview .rating .rating-stars {vertical-align: middle; margin: 0 15px; position:relative;}
            .movieshow .moviereview .rating .critic:before, .movieshow .moviereview .rating .users:before {font-size:15px; top:-10px; left:-7px;}
            .movieshow .readfullreview {padding: 20px 0; text-align: center;}
            .movieshow .readfullreview a {position:relative; display: inline-block; box-shadow: 0 0 4px 2px rgba(194, 194, 194, 0.5); background-color: #ffffff; color: #ff0000; font-size: 1.4rem; padding: 8px 30px 8px 23px; border-radius: 30px;text-align: center;font-weight: 400; margin-bottom:10px;}
            .movieshow .readfullreview a::after {content: "";display: block;height: 5px;width: 5px;margin: auto;position: absolute;top: 0;bottom: 0;transform: rotate(45deg);right: 18px;border-top: 1px solid #ff0000;border-right: 1px solid #ff0000;}
  `;

  let commonStyle = commonCss + headerCss + footerCss;

  if (portalName == "nbt") {
    commonStyle +=
      ".header .logo amp-img {width: 81px; height:27px}.header .logo .etLogo amp-img{width:57px; height:34px}.header .logo.logo-gadget amp-img{height:31px}";
  } else if (portalName == "eis") {
    commonStyle +=
      ".header .logo amp-img {width: 75px; height:33px}.navbar .header .app_download .appdownload_icon{width:50px;}.sidenav a[href*=eisamaygold] {color: #f5ae2d;}";
  } else if (portalName == "mt") {
    commonStyle +=
      ".header .logo amp-img {width: 120px; height:19px}.navbar .header .app_download .appdownload_icon{width:50px;}";
  } else if (portalName == "vk") {
    commonStyle +=
      ".header .logo amp-img {width: 109px; height:23px}.navbar .header .app_download .appdownload_icon{width:50px;}.vk-mini .header .logo img, body.enable-video-show .vk-mini .header .logo img{display:none;}.vk-mini .header .logo a{width:86px; height:38px; background: url(https://vijaykarnataka.com/photo/77152342.cms) 0 0 no-repeat; background-size: 86px 38px; margin-top:3px}";
  } else if (portalName == "tlg") {
    commonStyle +=
      ".header .logo amp-img {width: 93px; height:31px}.header .logo.logo-gadget amp-img{height:29px}.navbar .header .app_download .appdownload_icon{width:50px;}";
  } else if (portalName == "tml") {
    commonStyle +=
      ".header .logo amp-img {width: 88px; height:30px}.header .logo.logo-gadget amp-img{height:29px}.navbar .header .app_download .appdownload_icon{width:50px;}";
  } else if (portalName == "mly") {
    commonStyle +=
      ".header .logo amp-img {width: 100px; height:29px}.navbar .header .app_download .appdownload_icon{width:50px;}";
  } else if (portalName == "iag") {
    commonStyle +=
      ".header .logo amp-img {width: 106px; height:35px}.navbar .header .app_download .appdownload_icon{width:50px;}";
  }

  if (pagetype.indexOf("list") > -1) {
    commonStyle += listCss;
  }

  console.log("called @ ampcss", pagetype);
  
  if (pagetype == "home") {
    commonStyle += serviceDrawerCss + posterContentCss + listCss + electionshpwidget;
  }
  // else if (pagetype == "articlelist") {
  //   commonStyle += listCss + gnCommonCss + gnlistCss + astroWidgetCss + petrolWidget + sports_css;
  // }
  else if (pagetype == "articlelist" && (reqURL.includes("/tech/") || reqURL.includes("/gadget-news/"))) {
    commonStyle += listCss + gnCommonCss + gnlistCss;
  } else if (pagetype == "articlelist" && reqURL.indexOf("/sports/") > -1) {
    commonStyle += listCss + sports_css;
  } else if (pagetype == "articlelist") {
    commonStyle += listCss + astroWidgetCss + petrolWidget;
  } else if (pagetype == "reviews") {
    commonStyle += listCss + gnlistCss;
  } else if (pagetype == "videolist") {
    commonStyle += videolistCss;
  } else if (pagetype == "articleshow" && reqURL.indexOf("/tech/") > -1) {
    commonStyle += articleshowCss + stickyWP + gnCommonCss + ampwebpush + apnaBazaar_css;
  } else if (pagetype == "articleshow") {
    commonStyle += articleshowCss + stickyWP + ampwebpush + apnaBazaar_css;
  } else if (pagetype == "videoshow") {
    commonStyle += videoshowCss + listCss + videolistCss;
  } else if (pagetype == "moviereview") {
    commonStyle += articleshowCss + moviereviewshowCss + stickyWP + ampwebpush;
  } else if (pagetype == "liveblog") {
    commonStyle += liveblogCss + articleshowCss + stickyWP;
  } else if (pagetype == "topics") {
    commonStyle += listCss + topicsCss;
  } else if (pagetype == "photoshow") {
    commonStyle += listCss + photoshowCss;
  } else if (pagetype == "photolist") {
    commonStyle += webstories;
  } else if (pagetype == "compareshow") {
    commonStyle += gnextendedCommmon + gncompare;
  } else if (pagetype == "gadgetslist") {
    commonStyle += gnextendedCommmon + gngadgetlist;
  } else if (pagetype == "gadgetshow") {
    commonStyle += gnextendedCommmon + gngadgetshow;
  } else if (pagetype == "gadgethome") {
    commonStyle += gnlistCss + gngadgethome;
  } else if (pagetype == "elections") {
    commonStyle += electionnewcss;
  } else if (pagetype == "movieshow") {
    commonStyle += articleshowCss + movieShowCss;
  } else if (pagetype == "others") {
    commonStyle += sports_css;
  }

  return commonStyle;
};

export default ampCss;
