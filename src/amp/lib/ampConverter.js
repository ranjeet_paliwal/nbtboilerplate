/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable indent */
/* eslint-disable eqeqeq */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable func-names */
import cheerio from "cheerio";
import { _getStaticConfig, filterAlaskaData, isProdEnv, _filterRecursiveALJSON, checkTopAtf } from "../../utils/util";

import htmlConfig from "../../utils/htmlConfig";
import ampCss from "../ampcss";
import { isFeatureURL, getSplatsVars, wapadsGenerator } from "../../components/lib/ads/lib/utils";
import ampAdConfig from "./ampAdConfig";
const siteConfig = _getStaticConfig();
const SITE_PATH = process.env.SITE_PATH || "";
const SITE_NAME = process.env.SITE || "";

const ampAudianceUrl = `https://ade.clmbtech.com/cde/aeamp/${siteConfig.ccaudAmp}`;

const ampscripts = {
  ampiframe:
    '<script id="ampiframe" async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>',
  amptwitter:
    '<script id="amptwitter" async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>',
  ampinsta:
    '<script id="ampinsta" async custom-element="amp-instagram" src="https://cdn.ampproject.org/v0/amp-instagram-0.1.js"></script>',
  ampvideo:
    '<script id="ampvideo" async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>',
  ampform:
    '<script id="ampform" async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>',
  ampmustache:
    '<script id="ampmustache" async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>',
  ampfblike:
    '<script id="ampfblike" async custom-element="amp-facebook-like" src="https://cdn.ampproject.org/v0/amp-facebook-like-0.1.js"></script>',
  ampfb:
    '<script id="ampfb" async custom-element="amp-facebook" src="https://cdn.ampproject.org/v0/amp-facebook-0.1.js"></script>',
  ampshare:
    '<script id="ampshare" async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>',
  ampnext:
    '<script id="ampnext" async custom-element="amp-next-page" src="https://cdn.ampproject.org/v0/amp-next-page-1.0.js"></script>',
  ampcarousel:
    '<script id="ampcarousel" async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>',
  ampflyingCarpet: `<script async custom-element="amp-fx-flying-carpet" src="https://cdn.ampproject.org/v0/amp-fx-flying-carpet-0.1.js"></script>`,
  // ampwebpush:
  //   '<script id="ampwebpush" async custom-element="amp-web-push" src="https://cdn.ampproject.org/v0/amp-web-push-0.1.js"></script>',
  ampscript:
    '<script id="ampscript" async custom-element="amp-script" src="https://cdn.ampproject.org/v0/amp-script-0.1.js"></script>',
  //   ampgrx: ` <amp-analytics config="https://static.growthrx.in/js/v2/amp-sdk.json">
  //   <script type="application/json" >
  //   {
  //       "vars": {
  //           "projectCode": ${isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id}
  //       },
  //       "triggers": {
  //           "trackPageview": {
  //               "on": "visible",
  //               "request": "pageview",
  //               "vars" :{
  //                   "event_name" :"page_view"
  //               }
  //           }
  //       }
  //   }
  // </script>
  // </amp-analytics>`,

  // onetapgoogle:
  //  '<script async id="onetapgoogle" custom-element="amp-onetap-google" src="https://cdn.ampproject.org/v0/amp-onetap-google-0.1.js"></script>',
};

let cust_params = "";
const ampInuxu = `<amp-iframe width="auto" height="66" layout="fixed-height" sandbox="allow-scripts allow-same-origin allow-popups"
frameborder="0" src=${siteConfig.ads.ampInuxuAd} style="margin-top:-57px;">
</amp-iframe>`;

function getAds({ pagetype, data = {} }) {
  // let configObj = siteConfig.ads.dfpads.amp;
  const adsObj = ampAdConfig[process.env.SITE].adsconfig.dfpads;
  let configObj = adsObj.amp;
  const profileID = siteConfig.ads.profileid;

  if (data && data.pwa_meta) {
    configObj = wapadsGenerator({ secname: data.pwa_meta.adsec, defaultWapAds: configObj, pagetype });
  }

  return {
    bottom: `<aside class="box"><div class="bottom_widget_amp"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-loading-strategy="prefer-viewability-over-views"  height="800" width="300" type="colombia" layout="responsive" data-clmb_slot="${configObj.bottomctnamp}" data-clmb_position="1" data-clmb_section="0" data-clmb_divid="c" data-block-on-consent data-npa-on-unknown-consent=true data-amp-slot-index="3" heights="(min-width:320px) 920px, 920px"></amp-ad></div></aside>`,
    mrec1: `<div class="ad1 mrec"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}}, "timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-enable-refresh="30"  data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="336" height="280" type="doubleclick" data-slot="${configObj.mrec1}" data-multi-size="300x250" data-multi-size-validation="false"></amp-ad>`,
    mrec2: `<div class="ad1 mrec"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" :${profileID},"PUB_ID" : "23105"}}, "timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-enable-refresh="45" data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="336" height="280" type="doubleclick" data-slot="${configObj.mrec2}" data-multi-size="300x250" data-multi-size-validation="false"></amp-ad>`,
    mrecinf: `<div class="ad1 mrec"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}}, "timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-enable-refresh="45" data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="336" height="280" type="doubleclick" data-slot="${configObj.mrecinf}" data-multi-size="300x250" data-multi-size-validation="false"></amp-ad>`,
    atf: `<amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${configObj.atf}" data-multi-size="300x50" data-multi-size-validation="false"></amp-ad>`,
    fbn: `<amp-sticky-ad layout="nodisplay"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-enable-refresh="30"  data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${configObj.fbn}"></amp-ad></amp-sticky-ad>`,
    parallax: `
      <amp-fx-flying-carpet height="300px">
        <amp-ad  rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' width="300" height="600" data-loading-strategy="1.25" layout="fixed" type="doubleclick" data-slot="${configObj.mrec2}"></amp-ad>
      </amp-fx-flying-carpet>
      `,
    ampcarousalad: `<div class="ad1 carousal"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" :${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}'  data-loading-strategy="prefer-viewability-over-views" data-npa-on-unknown-consent="" data-clmb_slot=${configObj.ampcarousalad} heights="(min-width:1907px) 250px, (min-width:1200px) 250px, (min-width:780px) 250px, (min-width:480px) 250px, (min-width:460px) 250px, 250px" data-clmb_divid="c" data-clmb_section="0" data-clmb_position="1" data-block-on-consent data-npa-on-unknown-consent=true layout="responsive" type="colombia" width="300" height="250"  data-amp-slot-index="0"></amp-ad></div>`,
    slug1: `<div class="ad1 mrec"><amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-npa-on-unknown-consent="" width="320" height="100" type="doubleclick" data-slot="${configObj.slug1}" data-multi-size="300x100" data-multi-size-validation="false"></amp-ad></div>`,
    topatf: `<amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" : ${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}' data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${siteConfig.ads.dfpads.topatf}" data-multi-size="300x50" data-multi-size-validation="false"></amp-ad>`,
    ctnmidartad: `<amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"vendors": {"openwrap": {"PROFILE_ID" :${profileID},"PUB_ID" : "23105"}},"timeoutMillis": 1000}' json='{"targeting":{ "scn":"" }}'  data-loading-strategy="prefer-viewability-over-views" data-npa-on-unknown-consent="" data-clmb_slot="321530" heights="(min-width:1907px) 250px, (min-width:1200px) 250px, (min-width:780px) 250px, (min-width:480px) 250px, (min-width:460px) 250px, 250px" data-clmb_divid="c" data-clmb_section="0" data-clmb_position="1" data-block-on-consent data-npa-on-unknown-consent=true layout="responsive" type="colombia" width="300" height="250"  data-amp-slot-index="0"></amp-ad>`,
  };
  // ad structure on page article show
  // mrec1 - mrec
  // ctn (caraousal)
  // parallax - flying carpet
  // mrec2 - mrec1
  // mrecinf - mrec2
}

export function replaceHTMLContent(data, key, _options) {
  data = data.replace(htmlConfig[key].from, htmlConfig[key].with);
  return data;
}

function generateCSPHash(script) {
  const data = hash.update(script, "utf-8");
  return `sha384-${data
    .digest("base64")
    .replace(/=/g, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_")}`;
}

export function ampConverter(html, pagetype, reqUrl, version) {
  // version = clientStats.hash (for sevrice worker file)
  if (!html) return "";
  html = html.replace(/!important/g, "");
  // const amp_html = "";
  const $ = cheerio.load(html);
  let data = {};
  let msid = "";
  let relatedVideoURL = "";
  let superHitWidgetURL = "";
  const isFeatureURLFlag = isFeatureURL(reqUrl);
  let dfpAdConfig = {};

  let initialState = $("#initialState")
    .html()
    .split("window.__INITIAL_STATE__ = ")[1];
  initialState = JSON.parse(initialState);

  let svgsprite = "";
  if (["development", "stg1", "stg2"].includes(process.env.DEV_ENV)) {
    svgsprite = "/img/svgicons.svg";
  } else if (initialState.header && initialState.header.alaskaData) {
    const alaskasprite = filterAlaskaData(initialState.header.alaskaData, ["pwaconfig", "sprite"], "label");
    svgsprite = alaskasprite && alaskasprite._svgamppath ? alaskasprite._svgamppath : "/img/svgicons.svg";

    dfpAdConfig = _filterRecursiveALJSON(initialState.header, ["pwaconfig", "dfpAdConfig"], true);
  }

  $("#childrenContainer").removeAttr("style");
  $("#footer-wrapper").removeAttr("style");
  $("#footerContainer").removeAttr("style");

  // const svgsprite = ["development", "stg1", "stg2"].includes(process.env.DEV_ENV)
  //   ? "/img/svgicons.svg"
  //   : "/photo/msid-78486461.cms";
  // Add grx script for pageview
  //  addAmpScript($, "ampgrx");

  if ((pagetype == "articleshow" || pagetype == "moviereview") && initialState != undefined) {
    if (initialState && initialState.articleshow && initialState.articleshow.items[0]) {
      data = initialState.articleshow.items[0];
      if (initialState.articleshow.items[0].id) {
        msid = initialState.articleshow.items[0].id;
      }
    }

    //  Add No Index No Follow for AMP pages created before 24 Feb 2016
    const ampHtml = data && data.pwa_meta && data.pwa_meta.amphtml;
    if (ampHtml == undefined || ampHtml == "") {
      $("head").append(`<meta content="noindex, nofollow" name="robots">`);
    }
    // Advertorial Perpectual
    // let filteradvertorial;
    // let advertorialData = initialState && initialState.app && initialState.app.rhsData;
    // if (advertorialData && Array.isArray(advertorialData) && advertorialData.length > 0) {
    //   let checkPlatform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";
    //   filteradvertorial = advertorialData
    //     ? advertorialData.filter(fdata => {
    //         return (
    //           (checkPlatform === "mobile" ? fdata.ctype == "adv_wap" : fdata.ctype == "adv") &&
    //           fdata.id != msid &&
    //           fdata.wu &&
    //           fdata.wu.includes(process.env.WEBSITE_URL)
    //         );
    //       })
    //     : [];
    // }

    // related Video Url
    const pwaMeta = (data && data.pwa_meta) || "";
    const relatedvideomapid =
      pwaMeta && pwaMeta.parentid ? pwaMeta.parentid : pwaMeta.sectionid ? pwaMeta.sectionid : pwaMeta.navsecid;
    if (relatedvideomapid) {
      relatedVideoURL =
        relatedvideomapid && relatedvideomapid != ""
          ? `${
              process.env.API_BASEPOINT
            }/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=6&feedtype=sjson${
              pwaMeta.pagetype === "videoshow" ? "&pagetype=videoshow" : ""
            }`
          : null;
    }

    // superHit Widget Url
    if (data && data.pwa_meta && data.pwa_meta && data.pwa_meta.parentid) {
      const superHitWidgetId = data.pwa_meta.parentid;
      superHitWidgetURL =
        superHitWidgetId && superHitWidgetId != ""
          ? `${process.env.API_BASEPOINT}/sc_superhitwidget.cms?msid=${superHitWidgetId}&tag=ibeatmostread&type=articleshow&feedtype=sjson`
          : null;
    }
  }

  if (pagetype == "movieshow" && initialState != undefined) {
    if (initialState && initialState.movieshow && initialState.movieshow.items[0]) {
      data = initialState.movieshow.items[0];
      if (initialState.movieshow.items[0].id) {
        msid = initialState.movieshow.items[0].id;
      }
    }
  }

  if (pagetype == "photoshow" && initialState != undefined) {
    if (
      initialState &&
      initialState.photomazzashow &&
      initialState.photomazzashow.value[0] &&
      initialState.photomazzashow.value[0][0]
    ) {
      data = initialState.photomazzashow.value[0][0];
      if (data.pwa_meta) {
        msid = data.pwa_meta.msid ? data.pwa_meta.msid : null;
      }
    }
  }

  if (pagetype == "videoshow" && initialState != undefined) {
    // Add Required Scripts
    addAmpScript($, "ampscript");
    if (initialState && initialState.videoshow && initialState.videoshow.value) {
      data = initialState.videoshow.value;
      if (data.pwa_meta) {
        msid = data.pwa_meta.msid ? data.pwa_meta.msid : null;
        // related Video Url
        let relatedVideoURL = "";
        const pwaMeta = data && data.pwa_meta;
        const relatedvideomapid = pwaMeta.parentid
          ? pwaMeta.parentid
          : pwaMeta.sectionid
          ? pwaMeta.sectionid
          : pwaMeta.navsecid;
        if (relatedvideomapid) {
          //const relatedvideomapid = data.pwa_meta.relatedvideomapid;
          relatedVideoURL =
            relatedvideomapid && relatedvideomapid != ""
              ? `${
                  process.env.API_BASEPOINT
                }/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=6&feedtype=sjson${
                  pwaMeta.pagetype === "videoshow" ? "&pagetype=videoshow" : ""
                }`
              : null;
        }
        // amp-script component allows you to run custom JavaScript
        // script for related videos Widget
        if (relatedVideoURL && relatedVideoURL != null) {
          const relatedVideoScript = `<amp-script layout="responsive" width="200" height="135" src="${`${process.env.WEBSITE_URL}ampjs/relatedVideosWorker.js?v=${version}`}">
      <span id="relatedVideos" data-sectionURL="${relatedVideoURL}"></span>
      </amp-script>`;

          if ($(".related-links") != undefined && $(".related-links") != null && $(".related-links") != "") {
            $(".related-links").before(`${relatedVideoScript}`);
          }
        }
      }
    }
  }

  if ((pagetype == "articlelist" || pagetype == "videolist" || pagetype == "photolist") && initialState != undefined) {
    if (initialState && initialState.articlelist && initialState.articlelist.value[0]) {
      data = initialState.articlelist.value[0];
      if (data.pwa_meta) {
        msid = data.pwa_meta.msid ? data.pwa_meta.msid : null;
      }
    }
  }

  let homePageWidgets = [];
  const widgetsForHomeCsr = [];
  let budgetWidget = [];
  if (pagetype === "home") {
    data = { pwa_meta: { adsec: "home" } };
    homePageWidgets =
      initialState && initialState.home && initialState.home.value && initialState.home.value[1]
        ? initialState.home.value[1]
        : [];
    if (initialState && initialState.app && initialState.app.budget && initialState.app.budget) {
      budgetWidget = initialState.app.budget[process.env.SITE] && initialState.app.budget[process.env.SITE].lb;
    }
    // console.log('initialState.home.value[1] in amp', initialState.home.value[1]);
    homePageWidgets.forEach(widget => {
      if (widget.label === "ipl") {
        widgetsForHomeCsr.push(widget);
      }
    });
    if (budgetWidget) {
      //if (budgetWidget.label == "budget") {
      //widgetsForHomeCsr.push(budgetWidget);
      //}
    }
  }

  // We are set all sectional data for keyword taregting of ads
  const splatVars = getSplatsVars(reqUrl);
  const sectionData = splatVars.splats;
  cust_params = `
    "scn":"${sectionData[0] || ""}" ${sectionData[1] ? `,"subscn" : "${sectionData[1]}"` : ""} 
    ${sectionData[2] ? `,"lastsubscn" : "${sectionData[2]}"` : ""} 
    ${msid && msid != "" ? `,"storyID" : "${msid}"` : ""} ${pagetype ? `,"templatetype" : "${pagetype}"` : ""} 
    ${data && data.pwa_meta && data.pwa_meta.blacklist === "true" ? ',"BL" : "1"' : ',"BL" : "0"'} 
  `;

  const changePerpectualAds = reqUrl.indexOf("ampnext=") > -1 ? reqUrl.split("ampnext=")[1].split(/[\/.]/)[0] : null;
  let ampAds;
  const { adsconfig } = ampAdConfig[process.env.SITE];
  if (changePerpectualAds == "true" || changePerpectualAds == true) {
    // ampAds = Ads;
    ampAds = getAds({ pagetype, data });
  } else {
    // ampAds = Ads;
    ampAds = getAds({ pagetype, data });
  }

  // To be pass in ctn ad call
  let activeSection = $('[name="active:section"]').attr("content");
  // console.log(html)
  // remove all style and script
  $('style , script, [type="esi"],ad,amazonwidget').each(function(i, elem) {
    if ($(elem).attr("data-type") != "ampElement") {
      $(this).replaceWith("");
    }
  });

  // remove instagram/FB  and slideshow node and paste innerHtml in a DIV
  $("instagram,facebook,slideshow").each(function(i, elem) {
    const socialDiv = `<div>${$(elem).html()}</div>`;
    $(this).replaceWith(socialDiv);
  });
  // remove font tag
  $("font").each(function(i, elem) {
    const fontTagHtml = $(elem).html();
    const fonttag = fontTagHtml;
    $(this).replaceWith(fontTagHtml);
  });
  $(".svg-icons").each(function(elem) {
    const svgurl = $(this)
      .find("use")
      .attr("href");
    $(this)
      .find("use")
      .attr("href", svgsprite + svgurl);
  });

  // remove instagram/FB node and paste innerHtml in a DIV
  $(".table_row span.table_col").each(function(i, elem) {
    const socialDiv = `<span class="tbl_column">${$(elem).html()}</span>`;
    $(this).replaceWith(socialDiv);
  });

  // if (pagetype && pagetype == "articleshow") {
  //   if ($(".leadImage img")) {
  //     const leadImageSrc = $(".leadImage")
  //       .children("img")
  //       .attr("src");
  //     $("head").append(`<link rel="preload" href="${leadImageSrc}" as="image">`);
  //   }
  // }

  $("img").each(function(i, elem) {
    $(this).replaceWith(
      ampImgConverter(
        {
          alt: elem.attribs.alt || elem.attribs.title,
          src: elem.attribs.src,
          datasrc: elem.attribs["data-src"],
          class: elem.attribs.class,
          id: elem.attribs.id,
        },
        $(elem).html(),
        pagetype,
      ),
    );
  });

  // Remove all tag having attribute javascript:
  $("[href]").each((i, elem) => {
    if (
      $(elem)
        .attr("href")
        .indexOf("javascript") > -1
    ) {
      $(elem).removeAttr("href");
    }
  });
  // removing url for substory
  $("section").each((i, elem) => {
    $(elem).removeAttr("sub_url");
  });
  // removing istrending from hyperlink, dicussed with vipul , no need to serve attrbute in future
  $("a").each((i, elem) => {
    // Handling href link convert "/articleshow/" --> "/amp_articleshow/"
    let hrefUrl = $(elem).attr("href");

    let target = $(elem).attr("target");
    // hrefUrl = hrefUrl && hrefUrl.indexOf('/articleshow/') ? hrefUrl.replace('/articleshow/', '/amp_articleshow/') : hrefUrl;
    if (hrefUrl && hrefUrl.includes("/articleshow/")) {
      hrefUrl = hrefUrl.replace("/articleshow/", "/amp_articleshow/");
    } else if (hrefUrl && hrefUrl.includes("/videoshow/")) {
      hrefUrl = hrefUrl.replace("/videoshow/", "/amp_videoshow/");
    } else {
      hrefUrl = hrefUrl;
    }
    $(elem).attr("href", hrefUrl);

    $(elem).removeAttr("istrending");

    if (!target) {
      $(elem).removeAttr("target");
    }
  });

  // Set DFP and CTN Ads
  // Switch off ads for Featured URLs.
  if (isFeatureURLFlag) {
    $(".ad1").each((i, elem) => {
      $(elem).remove();
    });
  } else {
    $(".ad1").each((i, elem) => {
      $(elem).removeAttr("style");
      // TopATF added in amp for CPD campaign
      // made it config based

      // const dfpAdConfig = {
      //   topatf: "sports:",
      // };

      if ($(elem).hasClass("topatf") && checkTopAtf(dfpAdConfig, reqUrl)) {
        $(elem).html(insertAdHtml($, ampAds.topatf));
      } else if ($(elem).hasClass("atf")) {
        $(elem).html(insertAdHtml($, ampAds.atf));
      } else if ($(elem).hasClass("mrec") || $(elem).hasClass("mrec1")) {
        // need to remove ads after from around the web ad and between two articles in case of articleshow and moviereview
        if (!(pagetype === "articleshow" || pagetype === "moviereview")) {
          $(elem).html(insertAdHtml($, ampAds.mrec1));
        }
      } else if ($(elem).hasClass("mrec2")) {
        // mrec2 new ad code
        if (!(pagetype === "articleshow" || pagetype === "moviereview")) {
          $(elem).html(insertAdHtml($, ampAds.mrec2));
        }
      } else if ($(elem).hasClass("parallaxDiv")) {
        // previously it was the parallax ad position which is now mrec
        $(elem).html(insertAdHtml($, ampAds.mrec1));
      } else if ($(elem).hasClass("slug1")) {
        // it is the slug1 ad position which is used for city special AL pages
        $(elem).html(insertAdHtml($, ampAds.slug1));
      } else if ($(elem).attr("ctn-style") && adsconfig.ctnads[`amp${$(elem).attr("ctn-style")}`] != undefined) {
        let inlinead =
          pagetype == "home" ? adsconfig.ctnads.ampinlinehome : adsconfig.ctnads[`amp${$(elem).attr("ctn-style")}`];
        if ($(elem).attr("data-slotname") == "ctnhometop") {
          inlinead = adsconfig.ctnads.ampctnhometop;
        }
        activeSection = pagetype === "topics" ? "0" : activeSection;
        // prefer-viewability-over-views changed to 1.25
        // default value of prefer-viewability-over-views is 1.25 so there is no impact
        $(elem).html(
          insertAdHtml(
            $,
            `<amp-ad rtc-config='{"urls":["${ampAudianceUrl}"],"timeoutMillis": 1000}' data-loading-strategy="1.25" data-npa-on-unknown-consent="" data-amp-slot-index="2" data-clmb_divid="c" data-clmb_section="${activeSection}" data-clmb_position="${i}" data-clmb_slot="${inlinead.id}" layout="responsive" data-block-on-consent data-npa-on-unknown-consent=true type="colombia" width="${inlinead.width}" height="${inlinead.height}" heights="(min-width:780px) ${inlinead.minHeights1}, (min-width:480px) ${inlinead.minHeights2}, (min-width:460px) ${inlinead.minHeights3}, ${inlinead.minHeights4}">
            <div placeholder>Loading ...</div>
          </amp-ad>`,
          ),
        );
        $(elem).removeAttr("ctn-style");
      } else {
        $(elem).remove();
      }
    });
    // Add FBN sticky add
    $("body").append(insertAdHtml($, `${ampAds.fbn}`));
  }

  if (pagetype == "videoshow" || pagetype == "photoshow") {
    // Insert Mrec in Video show
    $(".videoshow-container > ul > li:nth-child(1)").append(insertAdHtml($, ampAds.mrec1));

    // Bottom Ad
    // Below code commented to fix duplicate CTN Ad issue (From Around the WEB below Footer)
    // $("#parentContainer").after(insertAdHtml($, `<div class="box-content">${ampAds.bottom}</div>`));

    // let brdcrm = $('.breadcrumb').remove();
    // brdcrm.insertBefore('.videoshow-container > ul');
  }

  // Insert MREC and More from Web ad in ase of article show and Movie Review

  if (
    pagetype == "articleshow" ||
    pagetype == "moviereview" ||
    pagetype == "movieshow" ||
    pagetype == "photoshow" ||
    pagetype == "videoshow"
  ) {
    // Switch off ads for Featured URLs.
    if (!isFeatureURLFlag && !(pagetype == "videoshow")) {
      // Bottom Ad
      $('[data-attr="next_article"]').after(insertAdHtml($, ampAds.bottom));
      // mrec before the footer ad
      if (pagetype == "articleshow" || pagetype == "moviereview" || pagetype == "movieshow") {
        $("article").after(insertAdHtml($, ampAds.mrecinf));
      } else {
        $('[data-attr="next_article"]').after(insertAdHtml($, ampAds.mrec1));
      }
      // console.log("length is", $("article > br").length);
      const length = $("article > br , article > section").length;
      // For ads in between article
      $("article > br , article > section").each(function(i, elem) {
        // in case of sections, ad after every section, in case of br ad after every 4 br
        // eslint-disable-next-line no-nested-ternary
        let iterator = elem.name === "section" ? i + 1 : i % 4 === 0 ? i / 4 : undefined;
        // handlling case of the two ads at the end of article arising due to mrec1 and eoa
        if (elem.name === "section") {
          if (length === iterator) {
            iterator = undefined;
          }
        } else if (Math.floor(length / 4) === iterator) {
          iterator = undefined;
        }

        // ad sequence :
        // mrec1 - mrec
        // parallax - flying carpet
        // carousal
        // mrec2 - mrec1 - this will go on till last
        // mrecinf - mrec2 - at the end of the article

        if (iterator) {
          if (iterator === 0) {
            // console.log(already taken care);
          } else if (iterator === 1) {
            //  To Add required AMP Script
            addAmpScript($, "ampflyingCarpet");
            $(this).after(insertAdHtml($, ampAds.parallax));
          } else if (iterator === 2) {
            $(this).after(insertAdHtml($, ampAds.ampcarousalad));
          } else if (SITE_NAME == "nbt" && reqUrl.indexOf("62994700.cms") > -1 && iterator === 3) {
            $(this).after(insertAdHtml($, ampAds.ctnmidartad));
          } else {
            $(this).after(insertAdHtml($, ampAds.mrecinf));
          }
        }
      });
    }

    if (data.pwa_meta.ampnext) {
      // To required script in head
      addAmpScript($, "ampnext");

      if (msid != "") {
        $("body").append(`
        <amp-next-page src="${
          siteConfig.channelCode == "nbt" ? process.env.NODE_API_BASEPOINT : process.env.API_BASEPOINT
        }/sc_ampnextitems.cms?msid=${msid}&feedtype=sjson"></amp-next-page>
        `);
      }

      // For amp-next-page move footer to amp-next-element
      const footer = $("#footerContainer").clone();
      $(footer).attr("footer", "");
      $(footer).attr("next-page-hide", "");
      $("#footerContainer").remove();
      $("amp-next-page").append(footer);

      $("#headerContainer").attr("next-page-hide", "");
      $(".con_mfw").attr("next-page-hide", "");

      // Removing photshow fake loader
      $(".fake-photoview").remove();
    }
  }

  if (
    pagetype == "articleshow" ||
    pagetype == "photoshow" ||
    pagetype == "videoshow" ||
    pagetype == "moviereview" ||
    pagetype == "movieshow" ||
    pagetype == "liveblog"
  ) {
    let shareUrl = "";
    if ($(".article-section") && $(".article-section").attr("data-shorurl")) {
      shareUrl = $(".article-section").attr("data-shorurl");
    } else {
      shareUrl = $('[property="og:url"]').attr("content");
    }
    // To Add required AMP Script
    addAmpScript($, "ampshare");

    // To add Social Share
    if ($('[data-attr="news_card"]').length > 0) {
      $('[data-attr="news_card"]').append(ampShare("", "", $("h1").text(), shareUrl));
    }
    if ($(".photo_card .place_holder").length > 0) {
      $(".photo_card .place_holder").append(ampShare("", "", $("h1").text(), shareUrl));
    }

    // Added sharing icons for liveblog
    if ($(".lb_top_summary").length > 0) {
      $(".lb_top_summary").append(ampShare("", "", $("h1").text(), shareUrl));
    }

    if ($(".con_social").length > 0) {
      $(".con_social").append(ampShare("", "", $("h1").text(), shareUrl));
    }

    if ($(".default-outer-player").length > 0) {
      $(".lead-post .default-outer-player").append(ampShare("", "", $("h1").text(), shareUrl));
    }

    // Added sharing icons for movieshow
    if ($(".reviewbuttons").length > 0) {
      $(".reviewbuttons").append(ampShare("", "", $("h1").text(), shareUrl));
    }

    // TO Add Inline Share Icon
    $(".inlineshare").each((i, elem) => {
      $(elem).append(ampShare("", "inline", $("h1").text(), shareUrl));
    });
  }

  // Runs custom JavaScript in a Web Worker
  if (pagetype && (pagetype == "articleshow" || pagetype == "moviereview")) {
    // Add Required Scripts
    addAmpScript($, "ampscript");

    // amp-script component allows you to run custom JavaScript
    // script for related videos Widget
    if (relatedVideoURL && relatedVideoURL != null) {
      const relatedVideoScript = `<amp-script layout="responsive" width="200" height="135" src="${`${process.env.WEBSITE_URL}ampjs/relatedVideosWorker.js?v=${version}`}">
      <span id="relatedVideos" data-sectionURL="${relatedVideoURL}"></span>
      </amp-script>`;
      if ($(".adCANContainer") != undefined && $(".adCANContainer") != null && $(".adCANContainer") != "") {
        $(".adCANContainer").after(`${relatedVideoScript}`);
      } else {
        $(".keywords_wrap").before(`${relatedVideoScript}`);
      }
    }

    // script for superHit Widget
    if (superHitWidgetURL && superHitWidgetURL != null) {
      const superHitWidgetScript = `<amp-script layout="responsive" width="200" height="110" src="${`${process.env.WEBSITE_URL}ampjs/superHitWidgetWorker.js?v=${version}`}">
      <span id="superHitWidget" data-sectionURL="${superHitWidgetURL}"></span>
      </amp-script>`;
      if ($(".amp_superwidget") != undefined && $(".amp_superwidget") != null && $(".amp_superwidget") != "") {
        $(".amp_superwidget").before(`${superHitWidgetScript}`);
      } else {
        $(".keywords_wrap").before(`${superHitWidgetScript}`);
      }
    }
  }

  if (pagetype && pagetype == "home" && widgetsForHomeCsr.length > 0) {
    // Add Required Scripts

    addAmpScript($, "ampscript");
    // amp-script component allows you to run custom JavaScript
    // script for homepage Widgets
    widgetsForHomeCsr.forEach(widget => {
      const listApiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${widget.msid ||
        widget._sec_id}&type=${widget._type || "list"}&feedtype=sjson&count=5`;

      let widgetHtml = "";
      if (listApiUrl) {
        widgetHtml += `<amp-script class="fixedHeightContent" layout="fixed-height" height="720" src="${`${process.env.WEBSITE_URL}ampjs/verticalListViewWorker.js?v=${version}`}">
          <span id="listApiUrl" data-sectionURL="${listApiUrl}"></span>
          </amp-script>`;
      }

      if (widget._rlvideoid && widget._rlvideoid != "") {
        const videoApiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${widget._rlvideoid}&type=video&feedtype=sjson&count=15`;
        widgetHtml += `<amp-script layout="fixed-height" height="290" src="${`${process.env.WEBSITE_URL}ampjs/relatedVideosWorker.js?v=${version}`}">
          <span id="relatedVideos" data-sectionURL="${videoApiUrl}"></span>
          <span id="pagetype" data-type="home"></span>
          </amp-script>`;
      }
      if (widget.label === "budget") {
        $(".top-news-content").before(`${widgetHtml}`);
      } else {
        $(".top-news-content").after(`${widgetHtml}`);
      }
    });
  }
  //inuxu ad for amp pages
  if (pagetype == "articleshow" && $(".img_wrap amp-img")) {
    $(".img_wrap amp-img").after(ampInuxu);
  }
  // Remove gatracked from div.
  $("[gatracked]").each((i, elem) => {
    $(elem).removeAttr("gatracked");
  });

  // Remove embed node from story for AMP, json parser is not supported for div or span
  $("embed").each((i, elem) => {
    $(elem).remove();
  });

  // for grx Push notification button added end of the article
  if (pagetype == "articleshow" || pagetype == "moviereview" || pagetype == "movieshow") {
    // addAmpScript($, "ampwebpush");
    $(".con_fbapplinks").append(
      `<div class="align-center-button notifyBtn"><a href=${`${siteConfig.mweburl}/pwafeeds/amp_notification.cms?wapCode=${siteConfig.wapCode}`}
    target="_blank" class="subscribe">
          <amp-img class="subscribe-icon" width="18" height="18" layout="fixed"
          src="data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTYgMjU2Ij48dGl0bGU+UHVzaEFsZXJ0PC90aXRsZT48ZyBpZD0iRm9ybWFfMSIgZGF0YS1uYW1lPSJGb3JtYSAxIj48ZyBpZD0iRm9ybWFfMS0yIiBkYXRhLW5hbWU9IkZvcm1hIDEtMiI+PHBhdGggZD0iTTEzMi4wNywyNTBjMTguNjIsMCwzMy43MS0xMS41MywzMy43MS0yMUg5OC4zNkM5OC4zNiwyMzguNDIsMTEzLjQ2LDI1MCwxMzIuMDcsMjUwWk0yMTksMjAwLjUydjBhNTcuNDIsNTcuNDIsMCwwLDEtMTguNTQtNDIuMzFWMTE0LjcyYTY4LjM2LDY4LjM2LDAsMCwwLTQzLjI0LTYzLjU1VjM1LjlhMjUuMTYsMjUuMTYsMCwxLDAtNTAuMzIsMFY1MS4xN2E2OC4zNiw2OC4zNiwwLDAsMC00My4yMyw2My41NXY0My40NmE1Ny40Miw1Ny40MiwwLDAsMS0xOC41NCw0Mi4zMXYwYTEwLjQ5LDEwLjQ5LDAsMCwwLDYuNTcsMTguNjdIMjEyLjQzQTEwLjQ5LDEwLjQ5LDAsMCwwLDIxOSwyMDAuNTJaTTEzMi4wNyw0NS40MmExMS4zMywxMS4zMywwLDEsMSwxMS4zNi0xMS4zM0ExMS4zMywxMS4zMywwLDAsMSwxMzIuMDcsNDUuNDJabTczLjg3LTE3LjY3LTYuNDUsOS43OGE4My40Niw4My40NiwwLDAsMSwzNi4xNSw1NC43N2wxMS41My0yLjA2YTk1LjIzLDk1LjIzLDAsMCwwLTQxLjIzLTYyLjVoMFpNNjQuNDYsMzcuNTJMNTgsMjcuNzVhOTUuMjMsOTUuMjMsMCwwLDAtNDEuMjMsNjIuNWwxMS41MywyLjA2QTgzLjQ2LDgzLjQ2LDAsMCwxLDY0LjQ1LDM3LjU0aDBaIiBmaWxsPSIjZmZmIi8+PC9nPjwvZz48L3N2Zz4=">
          </amp-img>
          Subscribe to Notifications
    </a></div>
  `,
    );
  }

  // Remove elements having data-exclude=amp
  $('[data-exclude="amp"]').each(function(i, elem) {
    $(this).remove();
  });

  // To Add Navigation as AMP-Sidebar
  const sidebar = $("#mySidenav");
  const hamburger = $(".hamburger");
  $(hamburger).removeAttr("class");
  $("#mySidenav").remove();
  $("body").prepend(
    `<amp-sidebar id="sidebar" layout="nodisplay" side="left"><button class="amp-close-image" on="tap:sidebar.close"></button>${sidebar}</amp-sidebar>`,
  );
  $("header .hamburger").remove();
  $("header").prepend(`<button on="tap:sidebar.toggle" data-attr="btnnav" class="hamburger">${hamburger}</button>`);

  // To Dynamically injects ads into an AMP page
  // Insert Auto Ads code script at ../../utils/htmlConfig at head of amp page
  // Insert <amp-auto-ads> tag at body
  $("body").prepend(`<amp-auto-ads type="adsense" data-ad-client="ca-pub-1902173858658913"> </amp-auto-ads>`);

  if (pagetype == "home") {
    $(".item_drawer").css({ display: "inline-block" });
    $(".con_facebooklike").css({ display: "none" });
  }

  // Will pass css string to criticalCSS component
  // let spritepath = "";
  // if (["development"].includes(process.env.DEV_ENV)) {
  //   spritepath = "/img/pwa_sprite.svg?v=12082020";
  // } else if (initialState.header && initialState.header.alaskaData) {
  //   const alaskasprite = filterAlaskaData(initialState.header.alaskaData, ["pwaconfig", "sprite"], "label");
  //   spritepath = alaskasprite && alaskasprite._svgpath ? alaskasprite._svgpath : "";
  // }

  // To add custom styling on page
  const ampcss = ampCss(process.env.SITE === "eisamay" ? "eis" : process.env.SITE, pagetype, reqUrl).replace(
    /!important/g,
    "",
  );

  const eventCss =
    initialState.articlelist &&
    initialState.articlelist.value[0] &&
    initialState.articlelist.value[0].theme &&
    initialState.articlelist.value[0].theme.style
      ? initialState.articlelist.value[0].theme.style
      : "";

  // $("head").append(`<style amp-custom>${ampcss + eventCss}</style>`);
  $("head").append(`<style amp-custom>${ampcss}</style>`);

  // for elections only
  $(
    ".tabs_widget, .wdgt_majoritymark, .right-open-in-app, #svgElectionWidgetContainer, .share_icon, .actionbtn",
  ).remove();

  // Remove all lable attribute from div tag
  $("[label], [tab]").each((i, elem) => {
    $(elem).removeAttr("label");
    $(elem).removeAttr("tab");
  });
  // Remove all gn custom attribute from div tag tech section attribute
  $("[itemname], [seo], [isequal], [parent-index]").each((i, elem) => {
    $(elem).removeAttr("itemname");
    $(elem).removeAttr("seo");
    $(elem).removeAttr("isequal");
    $(elem).removeAttr("parent-index");
  });

  // Handling FB like Button
  if ($("#btnfblike").length > 0) {
    // To required script in head
    addAmpScript($, "ampfblike");

    $("#btnfblike").each(function(i, elem) {
      $(this).replaceWith(ampFbLike(elem));
    });
  }

  // Iframe Handling
  if ($("iframe").length > 0) {
    // To required script in head
    addAmpScript($, "ampiframe");

    $("iframe").each(function(i, elem) {
      if ($(this).attr("id") != "btnfblike") {
        $(this).replaceWith(ampIfrmae(this));
      }
    });
  }

  // Handling Video Tag for slike video create frame
  // Create Iframe
  // Required child container having css class '.default-player' to append the amp iframe
  if ($("[data-attr-slk]").length > 0) {
    // console.log("data-attr-slk", $("[data-attr-slk]").length);
    // To required script in head
    addAmpScript($, "ampiframe");

    $("[data-attr-slk]").each(function(i, elem) {
      $(this)
        .find("amp-img")
        .remove();
      $(this).prepend(ampIfrmae(this, $(this).attr("data-attr-slk"), $(this).attr("data-video-seo"), reqUrl));
      // $(this).find('.default-player').append(ampIfrmae(this, $(this).attr('data-attr-slk')))
    });
  }
  // handing FB
  if ($(".fb-post").length > 0) {
    addAmpScript($, "ampfb");
    $(".fb-post").each(function(i, elem) {
      $(this).replaceWith(ampFb(this));
    });
  }
  // handing FB video
  if ($(".fb-video").length > 0) {
    addAmpScript($, "ampfb");
    $(".fb-video").each(function(i, elem) {
      $(this).replaceWith(ampFb(this));
    });
  }

  // Handling video tag/silke video
  // Create Video Tag
  if ($("[data-attr-sl]").length > 0) {
    // To required script in head
    addAmpScript($, "ampvideo");

    $("[data-attr-sl]").each(function(i, elem) {
      $(this).replaceWith(
        ampVideo({
          url: $(this).attr("data-attr-sl"),
          poster: $(this)
            .find("amp-img, img")
            .attr("src"),
        }),
      );
    });
  }

  // Handline Twitter Widget
  if ($(".twitter_widget").length > 0) {
    // To required script in head
    addAmpScript($, "amptwitter");

    $(".twitter_widget").each(function(i, elem) {
      $(this).replaceWith(ampTwitter(this));
    });
  }

  // Handline Twitter script
  if ($("twitter").length > 0) {
    // To required script in head
    addAmpScript($, "amptwitter");

    $("twitter").each(function(i, elem) {
      $(this).replaceWith(ampTwitterem(this));
    });
  }

  // Handling Instagram Widget
  if ($('blockquote[class="instagram-media"]').length > 0) {
    // To required script in head
    addAmpScript($, "ampinsta");
    $('blockquote[class="instagram-media"]').each(function(i, elem) {
      $(this).replaceWith(ampInstagram(this));
    });
  }

  // Handling Floating WhatsApp Icon
  if ($('[data-attr="share_whatsapp"]').length > 0) {
    // To Add required AMP Script
    addAmpScript($, "ampshare");

    $('[data-attr="share_whatsapp"]').each(function(i, elem) {
      $(this).replaceWith(ampShare(this, "wp", $("h1").text(), $('[property="og:url"]').attr("content")));
    });
  }

  if ('[data-attr="polldiv"]') {
    // To Add required amp script
    addAmpScript($, "ampform");
    addAmpScript($, "ampmustache");
    $('[data-attr="polldiv"] form').each(function(i, elem) {
      $(this).attr("action-xhr", `${siteConfig.mweburl}/poll_reult.cms?`);
      $(this).attr("target", "_top");
      $(this).attr("method", "post");
      $(this)
        .find('input[type="submit"]')
        .attr("on", `click:${$(this).attr("id")}.submit`);
      $(this)
        .find("input")
        .removeAttr("valign");
      $(this)
        .parent()
        .find(".login-before-result")
        .remove();
      $(this).append(
        `<input name="clientId" type="hidden" value="CLIENT_ID(POLL_USER_ID)" data-amp-replace="CLIENT_ID">`,
      );
      $(this).append(`
                <div submit-success class="ml1">
                    <template type="amp-mustache">
                        <div class="ampstart-card mt1 mr1 p1">
                            <span class="fixed-size-cell"></span>
                            <span></span>
                                {{#PollEntryResults}}
                            <div class="mt1 relative">
                                <span class="fixed-size-cell pollans">{{Answer}} </span>
                                <span class="fixed-size-cell">{{Percentage}}% </span>
                            </div>{{/PollEntryResults}}
                        </div>
                    </template>
                </div>
                <div submit-error>
                    <template type="amp-mustache">
                        <p>Error! Looks like something went wrong with your vote, please try to submit it again. {{error}}</p>
                    </template>
                </div>
            `);
    });
  }

  const oneTapFrame = `<amp-onetap-google    
    layout="nodisplay"
    data-src=${`${process.env.WEBSITE_URL}pwafeeds/onetap_iframe.cms`}>
  </amp-onetap-google>`;
  $("#onetap-box").append(`${oneTapFrame}`);

  return $.html();
}

function addAmpScript($, type) {
  if ($(`#${type}`).length == 0) {
    $("head").append(`${ampscripts[type]}`);
  }
}

// Handling AMP Social Share
function ampShare(obj, icon, titletext, url) {
  const wapCode = siteConfig.wapCode;
  const title = titletext.replace(/"|'/g, "");
  const appDownloadLink = `${siteConfig.locale.social_download.extrasharetxt} ${siteConfig.applinks.android.social}`;

  if (icon == "wp") {
    return `<amp-social-share type="whatsapp" data-share-endpoint="whatsapp://send" height="24" width="24" data-param-text="${title} - ${url}?utm_source=Whatsapp_Wap_stickyAS&utm_campaign=${wapCode}mobile&utm_medium=referral \n${appDownloadLink}"></amp-social-share>`;
  }
  if (icon == "inline") {
    return `<amp-social-share type="system" width="25" height="25"></amp-social-share>`;
  }
  return `<div class="socialsharewrap">
                <amp-social-share type="whatsapp" data-share-endpoint="whatsapp://send" height="34" data-param-text="${title} - ${url}?utm_source=whatsapp&utm_campaign=${wapCode}mobile&utm_medium=referral \n${appDownloadLink}"></amp-social-share>
                
                <amp-social-share type="facebook" height="34" data-param-quote="${title}" data-param-href="${url}?utm_source=fb&utm_campaign=${wapCode}mobile&utm_medium=referral" data-param-app_id="${
    siteConfig.fbappid
  }"></amp-social-share>
                    
  <amp-social-share type="twitter" height="34" data-param-url="${appDownloadLink}" data-param-text="${title} ${url}?utm_source=fb&utm_campaign=${wapCode}mobile&utm_medium=referral"></amp-social-share>
  
  <amp-social-share type="email" height="34" data-param-body="${title} - ${url}?utm_source=email&utm_campaign=${wapCode}mobile&utm_medium=referral ${appDownloadLink}"></amp-social-share>

  <amp-social-share type="telegram" data-param-text="${title}" data-share-endpoint="https://telegram.me/share/url?url=${url}" height="34" data-param-url="${url}?${encodeURIComponent(
    "utm_source=telegram_native_sharing_amp&utm_campaign=telegram&utm_medium=referral",
  )}"></amp-social-share>
            </div>`;
}

// Handling FB Like Button
function ampFbLike(obj) {
  return `<amp-facebook-like width=${obj.attribs.width} height=${obj.attribs.height} layout="fixed" data-layout="button_count" data-href="${siteConfig.fb}" />`;
}

function ampImgConverter(attributes, html, pagetype) {
  // console.log(pagetype)
  attributes.src = attributes.datasrc != undefined && attributes.datasrc != "" ? attributes.datasrc : attributes.src;

  attributes.class = attributes.class != undefined ? attributes.class : "amp-image";

  if (!attributes.src) {
    attributes.src = siteConfig.imageconfig.thumbid;
  }

  let attrWidth = "540";
  let attrHeight = "405";
  if (attributes.src && attributes.src.indexOf("width") > -1) {
    const splitWidth = attributes.src.split("width-");
    if (splitWidth && splitWidth[1] && typeof splitWidth[1] == "string") {
      attrWidth = splitWidth[1].split(",")[0];
    }
  }

  if (attributes.src && attributes.src.indexOf("height") > -1) {
    const splitHeight = attributes.src.split("height-");
    if (splitHeight && splitHeight[1] && typeof splitHeight[1] == "string") {
      attrHeight = splitHeight[1].split(",")[0];
      if (attrHeight && attrHeight.includes("/")) {
        // if height containes slash('/')
        attrHeight = attrHeight.split("/").shift();
      }
    }
  }

  attributes.width = attrWidth;
  attributes.height = attrHeight;

  // attributes.width = attributes.src.indexOf("width") > -1 ? attributes.src.split("width-")[1].split(",")[0] : "540";
  // attributes.height = attributes.src.indexOf("height") > -1 ? attributes.src.split("height-")[1].split(",")[0] : "405";

  // This is for photoshow template only to maintain image rationa and quality
  if (pagetype == "photoshow" && attributes.id != "logo-img") {
    attributes.width = "480";
    attributes.height = "640";
    attributes.src = attributes.src.replace("resizemode-4", "resizemode-75");
    attributes.src =
      attributes.src.indexOf("width-") > -1
        ? attributes.src.replace(`width-${attributes.width}`, "width-480")
        : attributes.src.replace("thumb/", "thumb/width-480,");
    attributes.src =
      attributes.src.indexOf("height-") > -1
        ? attributes.src.replace(`height-${attributes.width}`, "height-640")
        : attributes.src.replace("thumb/", "thumb/height-640,");
  }

  // convert  double quotes into single quote
  if (attributes && attributes.alt && attributes.alt != "") {
    attributes.alt = attributes.alt.replace(/"/g, "'");
  }

  return `<amp-img 
      ${attributes.class.indexOf("asleadimg") > -1 ? "data-hero" : ""}
			alt= "${attributes.alt}" 
			src= "${
        attributes.src
          ? attributes.src
          : "https://static.langimg.com/thumb/msid-${siteConfig.imageconfig.thumbid},width-540,height-405,resizemode-4/${siteConfig.wapsitename}.jpg"
      }"  
			width= "${attributes.width}"
            height=  "${attributes.height}"
            class = "${attributes.class}"
			layout= "responsive"
        >
            
            <amp-img fallback layout="fill" src="https://static.langimg.com/thumb/msid-${
              siteConfig.imageconfig.thumbid
            },width-540,height-405,resizemode-4/${siteConfig.wapsitename}.jpg" width="${attributes.width}" height="${
    attributes.height
  }" alt="${attributes.alt}" />
		</amp-img>`;
}

function ampIfrmae(obj, slike, seopath, reqUrl) {
  // default image src
  const defaultImage = `https://static.langimg.com/thumb/msid-${siteConfig.imageconfig.thumbid},width-540,height-405,resizemode-4/${siteConfig.wapsitename}.jpg`;
  // slike = need to paas video id
  let slikeURL = "";
  let frameSrc = "";
  // FIXME: Revert to this before going live
  const domain =
    siteConfig.channelCode == "nbt"
      ? "https://photogallery.navbharattimes.indiatimes.com"
      : siteConfig.channelCode == "mt"
      ? "https://navbharattimes.indiatimes.com"
      : "https://maharashtratimes.com";
  // these links shoud be called from config
  const changeddomain =
    siteConfig.channelCode == "nbt"
      ? "https://photogallery.navbharattimes.indiatimes.com"
      : siteConfig.channelCode == "mt"
      ? "https://photogallery.maharashtratimes.indiatimes.com"
      : siteConfig.channelCode == "tlg"
      ? "https://photogallery.telugu.samayam.com"
      : "https://photogallery.tamil.samayam.com";
  if (slike != undefined && slike != "") {
    slikeURL = `${isProdEnv() ? domain : "https://langdev.indiatimes.com"}/${seopath}/videoplayer.cms?wapCode=${
      siteConfig.wapCode
    }&msid=${slike}&label=amp_videoshow${reqUrl ? `&requesturl=${reqUrl}` : ""}#amp=1`;
    // defaultImage = slike;
  }

  if (slikeURL) {
    frameSrc = slikeURL;
  } else if (obj.attribs.src) {
    frameSrc = obj.attribs.src;
    if (frameSrc && frameSrc.includes(process.env.WEBSITE_URL)) {
      const oldUrl = frameSrc;
      const [_, urlPath] = oldUrl.split(process.env.WEBSITE_URL);
      const newURL = `${changeddomain}/${urlPath}`;
      frameSrc = newURL;
      //console.log("changeddomain", newURL, frameSrc);
    }
  } else if (obj.attribs["data-src"]) {
    frameSrc = obj.attribs["data-src"];
  }

  return `<amp-iframe height="${
    obj.attribs.height && slike == undefined
      ? obj.attribs.height
      : obj && obj.attribs && obj.attribs.class && obj.attribs.class.indexOf("__sast") > -1
      ? "95"
      : "250"
  }"
            sandbox="allow-scripts allow-same-origin allow-popups allow-top-navigation"
            src="${frameSrc}">
            <amp-img layout="fill" placeholder="placeholder" src="${defaultImage}"></amp-img>
        </amp-iframe>`;
}

function ampVideo(obj) {
  return `<amp-video width="600" height="450" layout="responsive" controls poster="${obj.poster}" title="">
            <source type="video/mp4" src="${obj.url}" />
            <div fallback><p>Your browser doesn’t support HTML5 video</p></div>
        </amp-video>`;
}

// FB handling
function ampFb(obj) {
  return ` <amp-facebook width="552" height="310" layout="responsive" data-href=${decodeURIComponent(
    obj.attribs["data-href"],
  )}>
         </amp-facebook>`;
}
function ampInstagram(obj) {
  return `<amp-instagram data-captioned="rmattval" width="400" height="400" layout="responsive" 
        data-shortcode=${
          obj.attribs["data-instgrm-permalink"]
            ? obj.attribs["data-instgrm-permalink"]
                .split("/p/")
                .pop()
                .split("/")[0]
            : null
        }>
        </amp-instagram>`;
}

function ampTwitter(obj) {
  return `<amp-twitter width="350"
            height="472"
            layout="responsive"
            data-tweetid="${obj.attribs.id ? obj.attribs.id.split("container_")[1] : null}">
        </amp-twitter>`;
}
// hadling embedscript
function ampTwitterem(obj) {
  return `<amp-twitter width="350"
            height="472"
            layout="responsive"
            data-tweetid="${obj.attribs.id ? obj.attribs.id : null}">
        </amp-twitter>`;
}

function ampAnchorConverter(attributes, html) {
  return `<a 
			href = "${attributes.href}"
			target =  "_blank"
		>
			${html}
		</a>`;
}

function insertAdHtml($, obj) {
  const adFlag = $('meta[name="adsserving"]').attr("content");
  if (adFlag == undefined || $(adFlag).attr("content") == "true") {
    return obj ? obj.replace(' "scn":"" ', cust_params == "" ? ' "scn":"" ' : `${cust_params}`) : null;
  }
  $(".ad1")
    .removeClass("ad1")
    .addClass("ad2");
}

// old code for the adconfig which is now redundant
// const Ads = {
//   bottom: `<aside class="box"><div class="bottom_widget_amp"><amp-ad json='{"targeting":{ "scn":"" }}' data-loading-strategy="prefer-viewability-over-views"  height="1250" width="360" type="colombia" layout="responsive" data-clmb_slot="${siteConfig.ads.dfpads.amp.bottomctnamp}" data-clmb_position="1" data-clmb_section="0" data-clmb_divid="c" data-npa-on-unknown-consent="" data-amp-slot-index="3" heights="(min-width:980px) 2999px,(min-width:780px) 2469px,(min-width:680px) 2386px,(min-width:650px) 2191px,(min-width:570px) 2102px,(min-width:550px) 1895px,(min-width:520px) 1855px,(min-width:488px) 1752px, (min-width:468px) 1687px, (min-width:440px) 1591px,(min-width:400px) 1538px,(min-width:370px)1467px, (min-width:360px)1367px, 1220px"></amp-ad></div></aside>`,
//   mrec: `<div class="ad1 mrec"><amp-ad json='{"targeting":{ "scn":"" }}' data-enable-refresh="30" data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="300" height="250" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.mrec}"></amp-ad>`,
//   mrec1: `<div class="ad1 mrec"><amp-ad json='{"targeting":{ "scn":"" }}' data-enable-refresh="45" data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="300" height="250" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.mrec1}"></amp-ad>`,
//   atf: `<amp-ad json='{"targeting":{ "scn":"" }}' layout="responsive" data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.atf}" data-multi-size="320x50" data-multi-size-validation="false"></amp-ad>`,
//   fbn: `<amp-sticky-ad layout="nodisplay"><amp-ad json='{"targeting":{ "scn":"" }}' data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.fbn}"></amp-ad></amp-sticky-ad>`,
//   parallax: `
//     <amp-fx-flying-carpet height="300px">
//       <amp-ad  width="300" height="600" layout="fixed" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.mrec1}"></amp-ad>
//     </amp-fx-flying-carpet>
//     `,
// };

// const perpampAds = {
//   bottom: `<aside class="box"><div class="bottom_widget_amp"><amp-ad json='{"targeting":{ "scn":"" }}' data-loading-strategy="prefer-viewability-over-views"  height="1250" width="360" type="colombia" layout="responsive" data-clmb_slot="${siteConfig.ads.dfpads.amp.bottomctnamp}" data-clmb_position="1" data-clmb_section="0" data-clmb_divid="c" data-npa-on-unknown-consent="" data-amp-slot-index="3" heights="(min-width:980px) 2999px,(min-width:780px) 2469px,(min-width:680px) 2386px,(min-width:650px) 2191px,(min-width:570px) 2102px,(min-width:550px) 1895px,(min-width:520px) 1855px,(min-width:488px) 1752px, (min-width:468px) 1687px, (min-width:440px) 1591px,(min-width:400px) 1538px,(min-width:370px)1467px, (min-width:360px)1367px, 1220px"></amp-ad></div></aside>`,
//   mrec: `<div class="ad1 mrec"><amp-ad json='{"targeting":{ "scn":"" }}' data-enable-refresh="30"  data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="300" height="250" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.perp_Mrec}"></amp-ad>`,
//   mrec1: `<div class="ad1 mrec"><amp-ad json='{"targeting":{ "scn":"" }}' data-enable-refresh="45"  data-loading-strategy="1.25"  data-npa-on-unknown-consent="" width="300" height="250" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.perp_Mrec1}"></amp-ad>`,
//   atf: `<amp-ad json='{"targeting":{ "scn":"" }}' layout="responsive" data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.perp_Atf}" data-multi-size="320x50" data-multi-size-validation="false"></amp-ad>`,
//   fbn: `<amp-sticky-ad layout="nodisplay"><amp-ad json='{"targeting":{ "scn":"" }}' data-npa-on-unknown-consent="" width="320" height="50" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.perp_Fbn}"></amp-ad></amp-sticky-ad>`,
//   parallax: `
//     <amp-fx-flying-carpet height="300px">
//       <amp-ad  width="300" height="600" layout="fixed" type="doubleclick" data-slot="${siteConfig.ads.dfpads.amp.mrec1}"></amp-ad>
//     </amp-fx-flying-carpet>
//     `,
// };
