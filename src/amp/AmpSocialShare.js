import React, { Component } from "react";
import { Link } from "react-router";
import config from "./../../config";
class AmpSocialShare extends Component {
  render() {
    let style = { width: "30px", height: "30px" };
    let { su, title } = this.props;
    return (
      <div className="socialsharewrap">
        <amp-social-share
          width="30"
          height="30"
          data-share-endpoint="whatsapp://send"
          type="whatsapp"
          data-param-text={
            title +
            " - " +
            config.SITE_TITLE +
            " - " +
            su +
            "?utm_source=whtsapp&amp;utm_campaign=ampmobile&amp;utm_medium=referral&amp;fb_ref=Default"
          }
        ></amp-social-share>
        <amp-social-share
          width="30"
          height="30"
          type="facebook"
          data-param-href={
            su +
            "&utm_source=fb&amp;utm_campaign=ampmobile&amp;utm_medium=referral&amp;fb_ref=Default"
          }
          data-param-app_id="972612469457383"
          data-param-text={title + " - " + config.SITE_TITLE}
        ></amp-social-share>
        <amp-social-share
          width="30"
          height="30"
          type="gplus"
          data-param-url={
            su +
            "&utm_source=gplus&amp;utm_campaign=ampmobile&amp;utm_medium=referral&amp;fb_ref=Default"
          }
          data-param-text={title + " - " + config.SITE_TITLE}
        ></amp-social-share>
        <amp-social-share
          width="30"
          height="30"
          type="twitter"
          data-param-url={
            su +
            "&utm_source=twt&amp;utm_campaign=ampmobile&amp;utm_medium=referral&amp;fb_ref=Default"
          }
          data-param-text={title + " - " + config.SITE_TITLE}
        ></amp-social-share>
        <amp-social-share
          width="30"
          height="30"
          type="email"
          data-param-body={
            su +
            "&utm_source=email&amp;utm_campaign=ampmobile&amp;utm_medium=referral&amp;fb_ref=Default"
          }
          data-param-subject={title + " - " + config.SITE_TITLE}
        ></amp-social-share>
      </div>
    );
  }
}

export default AmpSocialShare;
