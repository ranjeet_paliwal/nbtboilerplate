/* eslint-disable react/no-danger */
import React from "react";
import PropTypes from "prop-types";
import serialize from "serialize-javascript";
import { _getStaticConfig, getPWAmetaByPagetype, isProdEnv } from "../utils/util";
import { SeoSchema } from "../components/common/PageMeta";
import { setIbeatConfigurations } from "../components/lib/analytics/src/iBeat";
const siteConfig = _getStaticConfig();
const SITE_PATH = process.env.SITE_PATH || "";

function Amp({ js, css, html, head, initialState, version, pagetype, requrl }) {
  const pwa_meta = getPWAmetaByPagetype(initialState, pagetype);

  // GRX section identifier string
  let sectionIdentifierString = "";

  if (
    pagetype === "articleshow" ||
    pagetype === "photoshow" ||
    pagetype === "videoshow" ||
    pagetype === "moviereview"
  ) {
    // Split pathname to get location and msid of article
    // Eg ["/metro/mumbai/other-news/bmc-has-started-preparati…izens-extra-bed-in-hospitals-during-corona-crisis", "81757096.cms"]
    const [pageLocation, _] = requrl.split(`/amp_${pagetype}/`);

    // Remove seolocation by splitting against last "/"
    // Eg "/metro/mumbai/other-news"
    const pageSections = pageLocation.slice(0, pageLocation.lastIndexOf("/"));

    // Split levels again and only keep truthy values (non empty strings)
    //  Eg ["", "metro", "mumbai", "other-news"] -> ["metro", "mumbai", "other-news"]
    const pageSectionLevels = pageSections.split("/").filter(level => level);

    // Set all these section levels dynamically for grx calls
    pageSectionLevels.forEach((level, index) => {
      sectionIdentifierString += `,"section_l${index + 1}":"${level}"`;
    });
  }
  // For Ibeat
  const ibeatStr =
    "https://ibeat.indiatimes.com/iBeat/pageTrendlogAmp.html?h=${h}&d=${d}&url=${url}&k=${k}&ch=${ch}&at=${at}&aid=${aid}&loc=${loc}&ct=${ct}&cat=${cat}&scat=${scat}&ac=${ac}&tg=${tg}&ctids=${ctids}&pts=${pts}&pubT=${pubT}&auth=${auth}&pos=${pos}&utmvsi=${utmvsi}&utmcsr=${utmcsr}&utmccn=${utmccn}&utmcmd=${utmcmd}&utma=${utma}&iBeatField=${iBeatField}&ref=${documentReferrer}&ts=${pageDownloadTime}&clientId=CLIENT_ID(_iibeat_session)";

  //   const grxAmpObject = {
  //     __html: `{
  // "vars": {
  //     "projectCode": "${isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id}"
  // },
  // "triggers": {
  //     "trackPageview": {
  //         "on": "visible",
  //         "request": "pageview",
  //         "vars" :{
  //             "event_name" :"page_view"
  //         }
  //     }
  // }
  // }`,
  //   };
  //   const testObject = {
  //     vars: {
  //       projectCode: `"${isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id}"`,
  //     },
  //     triggers: {
  //       trackPageview: {
  //         on: "visible",
  //         request: "pageview",
  //         vars: {
  //           event_name: "page_view",
  //         },
  //       },
  //     },
  //   };
  return (
    <html lang={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge,chrome=1" />
        <meta httpEquiv="content-language" content={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"} />
        {head.title.toComponent()}
        {head.meta.toComponent()}
        {head.link.toComponent()}

        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta content="yes" name="apple-touch-fullscreen" />
        <meta name="msapplication-tap-highlight" content="no" />

        {/* <link rel="icon" type="image/png" sizes="16x16" href={siteConfig.icon} /> */}
        <link rel="shortcut icon" href={siteConfig.icon} />

        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png" />
        <meta name="theme-color" content="#ffffff" />
        <meta content={siteConfig.google_site_verification} name="google-site-verification" />
        <meta content="width=device-width,minimum-scale=1,initial-scale=1" name="viewport" />
        {process.env.SITE == "nbt" || process.env.SITE == "mt" ? (
          <link
            href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;1,700&display=optional"
            rel="stylesheet"
          />
        ) : (
          ""
        )}

        {SeoSchema().commonSchema()}

        <amp-install-serviceworker
          src={`${siteConfig.mweburl}/${SITE_PATH}service-worker.js?v=${version}`}
          data-iframe-src={`${siteConfig.mweburl}/off-url-forward/sw.cms?v=${version}`}
          layout="nodisplay"
        />
      </head>
      <body>
        {/* Google Tag Manager */}
        {/* process.env.SITE == "off" ? (
          <amp-analytics
            config={"https://www.googletagmanager.com/amp.json?id=" + siteConfig.ga.ampgtm + "&gtm.url=SOURCE_URL"}
            data-credentials="include"
          />
        ) : null */}
        <script
          id="initialState"
          dangerouslySetInnerHTML={{
            __html: `window.__INITIAL_STATE__ = ${serialize(initialState)}`,
          }}
        />

        <amp-analytics data-block-on-consent="" type="googleanalytics" id="analytics1">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
                "vars": {
                    "account": "${siteConfig.ga.gatrackid}"
                },
                ${
                  pwa_meta && pwa_meta.cd && pwa_meta.cd != ""
                    ? `"extraUrlParams": {
                    "${process.env.SITE == "nbt" ? "cd4" : "cd2"}": "${pwa_meta.cd.toLowerCase().replace(/ /g, "-")}"
                  },`
                    : ""
                }
                "triggers": {
                    "trackPageview": {
                      "on": "visible",
                      "request": "pageview"
                    }
                }
            }`,
            }}
          />
        </amp-analytics>
        {/* GRX script for triggering page views */}
        <amp-analytics config="https://static.growthrx.in/js/v2/amp-sdk.json">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
            "vars": {
                "projectCode": "${isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id}"
            },
            "triggers": {
                "trackPageview": {
                    "on": "visible",
                    "request": "pageview",
                    "vars" :{
                        "event_name" :"page_view"
                    },
                    "extraUrlParams":{
                      "properties": {
                          "screen_type":"${pagetype}",
                          "url":"${requrl}"
                          ${sectionIdentifierString}

                      }
                  }
                }
            }
          }`,
            }}
          />
        </amp-analytics>

        {/* For Comscore */}
        <amp-analytics type="comscore">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
                "vars": ${JSON.stringify(Object.assign(siteConfig.comscore, { c4: siteConfig.weburl + requrl }))},
                "extraUrlParams": {"comscorekw": "amp"}
                }`,
            }}
          />
        </amp-analytics>

        <amp-analytics type="colanalytics" data-block-on-consent="">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
                "vars": {
                    "id": "${siteConfig.ccaudIdAmp}"
                    }
                }`,
            }}
          />
        </amp-analytics>

        <amp-pixel
          data-block-on-consent=""
          src={`https://www.facebook.com/tr?id=${siteConfig.fbpixelid}&ev=PageView&noscript=1`}
          layout="nodisplay"
        />
        <amp-analytics type="ibeatanalytics" id="ibeatanalytics">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
              "vars":{
                "d": "${siteConfig.iBeat.host}"
              }
            }`,
            }}
          />
        </amp-analytics>

        {/* GDPR user connsent popup and script for AMP pages */}
        <amp-geo layout="nodisplay">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
              "ISOCountryGroups": {
                "eu": ["al", "ad", "am", "at", "by", "be", "ba", "bg", "ch", "cy", "cz", "de", "dk", "ee", "es", "fo", "fi", "fr", "gb", "ge", "gi", "gr", "hu", "hr", "ie", "is", "it", "lt", "lu", "lv", "mc", "mk", "mt", "no", "nl", "po", "pt", "ro", "ru", "se", "si", "sk", "sm", "tr", "ua", "uk", "va"],
                "usca": [ "preset-us-ca" ]
              }
            }`,
            }}
          />
        </amp-geo>

        <amp-consent layout="nodisplay" id="myUserConsent">
          <script
            type="application/json"
            data-type="ampElement"
            dangerouslySetInnerHTML={{
              __html: `{
                "consentInstanceId": "world-wide-consent",
                "consentRequired": false,
                "geoOverride": {
                  "eu": {
                    "consentRequired": true,
                    "promptIfUnknownForGeoGroup": "eu",
                    "promptUI": "myConsentFlow",
                    "promptIfUnknown": false
                  },
                  "usca": {
                    "consentRequired": true,
                    "promptIfUnknownForGeoGroup": "usca",
                    "promptUI": "myConsentFlow",
                    "promptIfUnknown": false
                  }
                },
                "postPromptUI": "post-consent-ui"
            }`,
            }}
          />
          <div className="popupOverlay" id="myConsentFlow">
            <div className="consentPopup">
              <div on="tap:myUserConsent.dismiss" tabIndex="0" role="button" className="dismiss-button">
                X
              </div>
              <div className="h2 m1">Cookies on the {siteConfig.wapsitename} website</div>
              <p className="m1">
                We use cookies and other tracking technologies to provide services in line with the preferences you
                reveal while browsing the Website to show personalize content and targeted ads, analyze site traffic,
                and understand where our audience is coming from in order to improve your browsing experience on our
                Website. By continuing to browse this Website, you consent to the use of these cookies. If you wish to
                object such processing, please read the instructions described in our{" "}
                <a href={`${siteConfig.mweburl}/privacypolicyeu.cms`}>privacy policy</a>/
                <a href={`${siteConfig.mweburl}/cookiepolicy.cms`}>cookie policy</a>.
              </p>
              <div className="btn">
                <button className="ampstart-btn ampstart-btn-secondary caps mx1 success" on="tap:myUserConsent.accept">
                  Accept
                </button>
                <button className="ampstart-btn ampstart-btn-secondary caps reject" on="tap:myUserConsent.reject">
                  Reject
                </button>
              </div>
            </div>
          </div>
          <div id="post-consent-ui">
            <button className="ampstart-btn caps m1 btn_consent" on="tap:myUserConsent.prompt()">
              Update Consent
            </button>
          </div>
        </amp-consent>
        {/* Pubmatic opengraph ad js */}
        <amp-iframe
          title="User Sync"
          width="1"
          height="1"
          sandbox="allow-same-origin allow-scripts"
          frameborder="0"
          src={`https://ads.pubmatic.com/AdServer/js/pwtSync/load-cookie.html?pubid=23105&profid=${siteConfig.ads.profileid}&bidders=appnexus,pubmatic,ix,rubicon`}
        >
          <amp-img
            layout="fill"
            src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
            placeholder
          ></amp-img>
        </amp-iframe>

        <div
          id="root"
          className="overflowHidden"
          dangerouslySetInnerHTML={{
            __html: html,
          }}
        />
      </body>
    </html>
  );
}

Amp.propTypes = {
  js: PropTypes.array.isRequired,
  css: PropTypes.array.isRequired,
  html: PropTypes.string,
  head: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
};

export default Amp;
