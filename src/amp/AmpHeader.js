import React, { Component } from "react";
import { Link, browserHistory } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import config from "./../../config";

class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <a className="app-title" id="logo" href="/">
            <div className="logo-img"></div>
            <span>NewsPoint</span>
          </a>
        </header>
      </div>
    );
  }
}

export default Header;
