const config = {
  gadgetMapping: {
    "mobile-phones": "mobile",
    laptops: "laptop",
    tablets: "tablet",
    cameras: "camera",
    television: "tv",
    ac: "ac",
    washingmachine: "washingmachine",
    refrigerator: "refrigerator",
    powerbank: "powerbank",
    smartwatch: "smartwatch",
    fitnessband: "fitnessband",
  },

  gadgetCategories: {
    mobile: "mobile-phones",
    laptop: "laptops",
    tablet: "tablets",
    camera: "cameras",
    tv: "television",
    ac: "ac",
    smartwatch: "smartwatch",
    fitnessband: "fitnessband",
    washingmachine: "washingmachine",
    refrigerator: "refrigerator",
    powerbank: "powerbank",
  },

  gadgetsDisplay: {
    "mobile-phones": "MOBILES",
    laptops: "LAPTOPS",
    tablets: "TABLETS",
    cameras: "CAMERAS",
    television: "TELEVISIONS",
    ac: "AIR CONDITIONERS",
    washingmachine: "WASHING MACHINES",
    refrigerator: "REFRIGERATORS",
    powerbank: "POWER BANKS",
    smartwatch: "SMART WATCHES",
    fitnessband: "FITNESS BANDS",
  },

  hostid: "53",
  techPrefix: "/tech",
  gnMetaVal: "gn",
  affiliateTags: {
    // nbt_web_comparisondetail_sponsoredwidget-21
    CD: "comparisondetail_sponsoredwidget",
    CL: "comparisonlanding_suggestionwidget",
    CDSM: "comparisondetail_seelctedmodels", // Selected Models
    HP: "home_latesttrendingwidget",
    GL: "gadgetslist_alsoseewidget",
    GS: "pdp_buyatwidget",
    GS_SEEALSO: "pdp_seealsowidget",
    GS_RHS_SPONS: "pdp_rhssponsoredwidget-21",
    GS_BRAND: "pdp_rhsbrandwidget",
    AS: "articleshow_sponsoredwidgetlhs",
    RS: "reviewshow_sponsoredwidgetlhs",
  },
  languages: [
    {
      name: "Hindi",
      regName: "हिन्दी",
      link: "https://hindi.gadgetsnow.com",
    },
    {
      name: "Kannada",
      regName: "ಕನ್ನಡ",
      link: "https://kannada.gadgetsnow.com",
    },
    {
      name: "Tamil",
      regName: "தமிழ்",
      link: "https://tamil.gadgetsnow.com",
    },
    {
      name: "Malayalam",
      regName: "മലയാളം",
      link: "https://malayalam.gadgetsnow.com",
    },
    {
      name: "Telugu",
      regName: "తెలుగు",
      link: "https://telugu.gadgetsnow.com",
    },
    {
      name: "Marathi",
      regName: "मराठी",
      link: "https://marathi.gadgetsnow.com",
    },
    {
      name: "Bangla",
      regName: "বাংলা",
      link: "https://bengali.gadgetsnow.com",
    },
    {
      name: "Gujarati",
      regName: "ગુજરાતી",
      link: "https://gujarati.gadgetsnow.com",
    },
    {
      name: "English",
      link: "https://gadgetsnow.com",
    },
  ],

  brands: [
    "sony-ericsson",
    "hi-tech",
    "m-tech",
    "colors-mobile",
    "i-kall",
    "i-smart",
    "i-mobile",
    "vox-mobile",
    "t-series",
    "mu-phone",
    "t-mobile",
    "aqua-mobile",
    "black-bear",
    "good-one",
    "vk-mobile",
    "vell-com",
    "gee-pee",
    "i-mate-mobile",
    "red-cherry",
    "v3-mobile",
    "i-tel-mobiles",
    "m-horse",
    "i-life", // laptop
    "michael-kors", // Smart Watch
    "mont-blanc",
    "tag-heuer",
    "world-tech", // TV
    "noble-skiodo",
    "t-series",
    "jack-martin",
    "mr-light",
    "blue-star", // AC
    "o-general",
    "hi-tech", // Tablet
    "i-mobile",
    "sky-mobile",
    "t-mobile",
    "voltas-beko", // WM
    "super-general",
    "tp-link", // PB
  ],

  pathParams: function() {
    let params = "";
    this.brands.forEach(item => {
      params += `(${item})`;
    });
    return params;
  },
};

export default config;
