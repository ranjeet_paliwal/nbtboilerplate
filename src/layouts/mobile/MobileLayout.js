import React, { Component } from "react";
import { connect } from "react-redux";

import SearchHeader from "./../../components/common/SearchHeader";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import { fetchHeaderDataIfNeeded } from "./../../actions/header/header";
import { fetchMiniTvDataIfNeeded, fetchBnewsDataIfNeeded, fetchTrendingDataIfNeeded } from "./../../actions/app/app";
import BreakingNewsCard from "../../components/common/BreakingNewsCard";
import ElectionsHPWidget from "../../campaign/election/components/ElectionsHPWidget";

import {
  _getStaticConfig,
  _sessionStorage,
  getPageType,
  _isFrmApp,
  _isOPPO,
  _isVivo,
  _isSamsung,
  LoadingComponent,
  _isCSR,
  _filterRecursiveALJSON,
  checkWidgetToRender,
  isMobilePlatform,
  checkIsAmpPage,
  alterTransformEffect,
} from "../../utils/util";

import AdCard from "../../components/common/AdCard";

import Loadable from "react-loadable";

const Header = Loadable({
  loader: () => import("./../../components/common/Header"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("./../../components/common/Header"),
});

const ResultHPWidget = Loadable({
  loader: () => import("../../campaign/election/components/ResultHPWidget"),
  LoadingComponent,
});

const ExitPollHPWidget = Loadable({
  loader: () => import("../../campaign/election/components/ExitPollHPWidget"),
  LoadingComponent,
});

const MiniTVCard = Loadable({
  loader: () => import("../../components/common/MiniTVCard"),
  LoadingComponent,
});
const Footer = Loadable({
  loader: () => import("./../../components/common/Footer"),
  LoadingComponent,
});

const siteConfig = _getStaticConfig();

class MobileLayout extends Component {
  constructor(props) {
    super(props);

    (this.state = {
      makePlayerVisible: false,
      makeVideoHeaderVisible: false,
      dockHeader: false,
      dockVideo: false,
      fbnCloseOpen: false,
      resetFbnBtn: false,
      actualUrl: "",
      hasMounted: false,
    }),
      (this.config = {
        isFrmapp: false,
        isOPPO: false,
        isVivo: false,
        isSamsung: false,
        pollingInterval: 30000,
      });
  }
  componentDidMount() {
    if (_isCSR()) {
      window.forceOffTransform = () => alterTransformEffect("remove");
      window.forceOnTransform = () => alterTransformEffect("add");
      window.isTransforming = true;
    }
    const { dispatch, config } = this.props;
    this.setState({ hasMounted: true });
    let _this = this;
    //check session storage for minitv & bnews
    if (!_sessionStorage().get("bnews")) dispatch(fetchBnewsDataIfNeeded());
    if (!_sessionStorage().get("minitv")) dispatch(fetchMiniTvDataIfNeeded());

    // Trending data of home can be called directly ( empty subsecid)
    // For other pages config.subsec1 must exist ( which sometimes is preloaded in SSR)
    if (config.pagetype == "home" || config.subsec1) {
      dispatch(fetchTrendingDataIfNeeded({ subsec1: (config && config.subsec1) || "" }));
    }

    dispatch(fetchHeaderDataIfNeeded());
    // if (_isCSR() && !_isFrmApp(_this.props.router)) {
    if (_isCSR()) {
      _this.reset_Fbn_ad();
      window.addEventListener("scroll", _this.handle_Fbn_add);
    }
    let actualUrl = `${window.location.origin}${this.props && this.props.location && this.props.location.pathname}`;
    this.setState({ actualUrl: actualUrl });

    // this snippet is just to ensure that if the resizeobserver is not initialized, this will try to initialize it
    if (!window.observeAtfSizeChange) {
      if (observeResizeChange) {
        const newElement = document.getElementById("general-atf-wrapper");
        if (newElement) {
          window.observeAtfSizeChange = true;
          observeResizeChange(newElement);
        }
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { config, dispatch } = this.props;

    // If location and subsec1 values differ for pages, fetch trending data again ( on CSR)
    if (prevProps.config.subsec1 !== config.subsec1) {
      dispatch(fetchTrendingDataIfNeeded({ subsec1: (config && config.subsec1) || "" }));
    }
  }

  reset_Fbn_ad = () => {
    let eleWrapper = document.querySelector(".fbnad");
    if (eleWrapper) {
      let adopenclose = eleWrapper.querySelector(".fbnad .ads-open-close");
      // let ele = eleWrapper.querySelector(".fbn");
      // eleWrapper.style.display = "none";
      if (adopenclose) {
        adopenclose.style.display = "none";
      }
    }
  };

  handle_Fbn_add = () => {
    if (
      this.state &&
      !this.state.fbnCloseOpen &&
      this.state.actualUrl.indexOf(window && window.location.href.split("?story")[0]) <= -1
    ) {
      this.reset_Fbn_ad();
      let actualUrl = window && window.location.href.split("?story")[0];
      this.setState({ fbnCloseOpen: true, actualUrl: window.location.href });
    }

    let eleWrapper = document.querySelector(".fbnad");
    let adopenclose = eleWrapper && eleWrapper.querySelector(".fbnad .ads-open-close");
    let ele = eleWrapper && eleWrapper.querySelector(".fbn");
    // if (window.scrollY == 0) {
    //   // ele.style.display = "none";
    //   eleWrapper.style.display = "none";
    //   adopenclose.style.display = "none";
    //   let actualUrl = `${window.location.origin}${this.props && this.props.location && this.props.location.pathname}`;
    //   this.setState({ resetFbnBtn: true, actualUrl: actualUrl });
    // } else {
    //ele.style.display = "block";
    if (eleWrapper) {
      eleWrapper.style.display = "block";
    }
    if (adopenclose) {
      adopenclose.style.display = "block";
    }
    if (
      (eleWrapper && eleWrapper.style.display == "block" && this.state.fbnCloseOpen) ||
      (ele && ele.style.visibility == "" && this.state.resetFbnBtn)
    ) {
      eleWrapper.style.height = "85px";
      if (ele) {
        ele.style.visibility = "visible";
      }

      adopenclose.classList.remove("fbnad", "ads-open-close", "up");
      adopenclose.classList.add("fbnad", "ads-open-close");
    }
    this.setState({ resetFbnBtn: false });
    // }

    // ele ?
    //   window.addEventListener('scroll_up', function () {
    //     ele.style.bottom == '-60px' ? ele.style.bottom = '0' : ''
    //   })
    //   : '';
  };

  fbnCloseOpen = () => {
    let eleWrapper = document.querySelector(".fbnad");
    let adopenclose = eleWrapper.querySelector(".fbnad .ads-open-close");
    let ele = eleWrapper.querySelector(".fbn");
    this.setState({ fbnCloseOpen: !this.state.fbnCloseOpen });
    if (this.state.fbnCloseOpen) {
      eleWrapper.style.height = "28px";
      //adopenclose.style.display = "block";
      ele.style.visibility = "hidden";
      adopenclose.classList.add("fbnad", "ads-open-close", "up");
    } else {
      eleWrapper.style.height = "85px";
      ele.style.visibility = "visible";
      adopenclose.classList.remove("fbnad", "ads-open-close", "up");
      adopenclose.classList.add("fbnad", "ads-open-close");
      adopenclose.style.display = "block";
    }
  };

  render() {
    //election is used for widgetswiching
    const { header, bnews, config, router, budget, minitv, trending, election, params, location } = this.props;
    const msid = params && params.msid ? params.msid : undefined;
    let { pathname } = router.location;
    let pagetype = getPageType(pathname);
    let { query } = this.props.location;
    let isFrmApp = _isFrmApp(this.props.router);
    let isOPPO = _isOPPO(this.props.router);
    let isVivo = _isVivo(this.props.router);
    let isSamsung = _isSamsung(this.props.router);

    //Liveblog Widget config
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    let showLiveblogWidget =
      budgetLbConfig &&
      budgetLbConfig.status == "true" &&
      budgetLbConfig.msid != "" &&
      (config.pagetype == "home" || this.props.router.location.pathname.indexOf("/budget/articlelist/") > -1)
        ? true
        : false;

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      this.config.isFrmapp = true;
    }

    //viral adda logo header
    let viralBranding =
      this.props.router.location.pathname.indexOf("/viral-adda/") > -1 ||
      this.props.router.location.pathname.indexOf("/viral-corner/") > -1
        ? true
        : false;

    const isTimesPointPage = this.props.router.location.pathname.indexOf("timespoints.cms") > -1;

    const widgetToRender = checkWidgetToRender({ election, pagetype, msid });
    const isAMPPage = checkIsAmpPage(pathname);

    return (
      <div className="mobile_body">
        {!this.config.isFrmapp ? (
          <ErrorBoundary>
            {config.searchHeader && !isTimesPointPage ? (
              <SearchHeader />
            ) : (
              <Header
                navigations={header && header.alaskaData}
                viralBranding={viralBranding}
                pagetype={config.pagetype}
                {...this.state}
                {...this.props}
                query={query}
                navData={header && header.navData}
                isTimesPointPage={isTimesPointPage}
              />
            )}
          </ErrorBoundary>
        ) : null}

        {/* Breaking News */}
        <div data-exclude="amp">
          {!isTimesPointPage &&
          config.pagetype != "newsbrief" &&
          config.pagetype != "videolist" &&
          !isFrmApp &&
          config.pagetype != "webviewpage" ? (
            <ErrorBoundary>
              <BreakingNewsCard bnews={bnews} trending={trending} />
            </ErrorBoundary>
          ) : null}
        </div>
        {/* pre render dfp */}
        {/* {isFrmApp ? null : ( */}
        {!isTimesPointPage ? (
          <div>
            {/* {router.location.pathname.indexOf("/newsbrief") == -1 ? ( */}
            <div className="atf-wrapper">
              <div id="general-atf-wrapper">
                <AdCard
                  mstype={isOPPO ? "newsPointatf" : isVivo ? "newsPointatf" : isSamsung ? "newsPointatf" : "atf"}
                  adtype="dfp"
                  rendertype="prerender"
                  className="esi"
                  // pagetype={config.pagetype}
                />
                {/* MINITV */}
                {config.pagetype !== "home" &&
                !isFrmApp &&
                minitv &&
                minitv.hl &&
                //router.location.pathname.indexOf("/tech") < 0 &&
                config.pagetype != "newsbrief" &&
                config.pagetype != "videolist" &&
                config.pagetype != "videoshow" &&
                config.pagetype != "webviewpage" ? (
                  <ErrorBoundary>
                    <MiniTVCard minitv={minitv} location={location} />
                  </ErrorBoundary>
                ) : null}
              </div>
            </div>
            {/* ) : null} */}
            <div className="fbnad" data-exclude="amp">
              <div className="ads-open-close" onClick={this.fbnCloseOpen}>
                <a href="javascript:void(0);">
                  <i />
                </a>
              </div>
              <AdCard
                mstype={isOPPO ? "newsPointfbn" : isVivo ? "newsPointfbn" : isSamsung ? "newsPointfbn" : "fbn"}
                adtype="dfp"
                rendertype="prerender"
                // pagetype={config.pagetype}
              />
            </div>
          </div>
        ) : null}
        {/* )} */}
        {/* MINITV */}
        {/* {config.pagetype !== "home" &&
        !isFrmApp &&
        minitv &&
        minitv.hl &&
        //router.location.pathname.indexOf("/tech") < 0 &&
        config.pagetype != "newsbrief" &&
        config.pagetype != "videolist" &&
        config.pagetype != "videoshow" &&
        config.pagetype != "webviewpage" ? (
          <ErrorBoundary>
            <MiniTVCard minitv={minitv} location={location} />
          </ErrorBoundary>
        ) : null} */}

        {/*showLiveblogWidget ? (
          <LiveBlogStrip lbconfig={budgetLbConfig} msid={budgetLbConfig.msid} widgetType={"multipleposts"} />
        ) : null*/}

        {/* <div id="electionwidget" className="transform" style={{ transform: "translateY(0px)" }}>
          {widgetToRender && (
            <ErrorBoundary>
              <ElectionsHPWidget
                widgetType={widgetToRender}
                sequence={election.result.sequence}
                url={this.props.router.location.pathname}
              />
            </ErrorBoundary>
          )}
        </div> */}

        <div id="sitesync-wrapper" data-exclude="amp"></div>

        <div
          id="childrenContainer"
          className="con_content transform"
          style={{ transform: "translateY(0px)", minHeight: "100vh" }}
        >
          {this.props.children}
        </div>
        <div id="onetap-box"></div>
        <div id="share-container-wrapper" data-exclude="amp" />
        <div id="position-fixed-floater" data-exclude="amp" />

        {!this.config.isFrmapp && !isTimesPointPage ? (
          <ErrorBoundary>
            {/*As we are using _FilterAlJson inside Footer to get alskaData */}
            <div id="footer-wrapper" className="transform" style={{ transform: "translateY(0px)" }}>
              {this.state.hasMounted || isAMPPage ? (
                <Footer
                  header={header}
                  pagetype={config && config.pagetype}
                  initialState={
                    this.props.initialState
                  } /*As we are using _FilterAlJson inside Footer to get alskaData */
                  offfoot={1}
                />
              ) : null}
            </div>
          </ErrorBoundary>
        ) : null}
        <div id="interstitial-ad-wrapper" />
        {!this.config.isFrmapp ? (
          <div className="landscape-mode" data-exclude="amp">
            <div className="center-align">
              <span className="img_landscape" />
              <div className="txt">
                <h3>{siteConfig.locale.orientation_heading}</h3>
                {siteConfig.locale.orientation_text}
              </div>
            </div>
          </div>
        ) : null}

        {isMobilePlatform() && _isCSR() ? (
          <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: "" }} />
        ) : (
          <div>
            {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="canvasAd"
                className="ad1 canvasAd prerender emptyAdBox"
                id="div-gpt-ad-1558437895757-0-canvasAd"
                data-path={siteConfig.ads.dfpads.canvasAd}
                data-size="[[1, 1]]"
              />
            ) : null}
          </div>
        )}

        {_isCSR() ? (
          <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: "" }} />
        ) : (
          <div>
            {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="andbeyond"
                className="ad1 andbeyond prerender emptyAdBox"
                id="div-gpt-ad-1558437895757-0"
                data-path={siteConfig.ads.dfpads.andbeyond}
                data-size="[[1, 1]]"
              />
            ) : null}
            {process.env.SITE == "nbt" && queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="networkPGT"
                className="ad1 oop prerender emptyAdBox"
                id="div-gpt-ad-1558437895757-0-networkPGT"
                data-path={siteConfig.ads.dfpads.networkPGT}
                data-size="[[1, 1]]"
              />
            ) : null}
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.app,
    initialState: state,
    header: state.header,
    config: state.config,
    routing: state.routing,
    scoreboard: state.scoreboard,
    home: state.home,
  };
}

export default connect(mapStateToProps)(MobileLayout);
