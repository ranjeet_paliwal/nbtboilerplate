import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchHeaderDataIfNeeded } from "./../../actions/header/header";
import { fetchBnewsDataIfNeeded } from "./../../actions/app/app";
import { fetchMiniTvDataIfNeeded, fetchTrendingDataIfNeeded } from "./../../actions/app/app";

import { fetchExitPollIfNeeded } from "../../campaign/election/actions/exitpoll/exitpoll";
import { fetchResultsIfNeeded } from "../../campaign/election/actions/results/results";
import { fetchElectionConfigSuccess } from "../../campaign/election/actions/electoralmap/electoralmap";

import DesktopHeader from "../../components/desktop/DesktopHeader";
import Footer from "./../../components/common/Footer";
import AdCard from "./../../components/common/AdCard";
import BreakingNewsCard from "../../components/common/BreakingNewsCard";
import AnchorLink from "../../components/common/AnchorLink";
import {
  _getStaticConfig,
  _sessionStorage,
  _isFrmApp,
  _isCSR,
  LoadingComponent,
  getPageType,
  getElectionConfigFromData,
  checkWidgetToRender,
} from "../../utils/util";

import "../../components/common/css/Desktop.scss";
import ErrorBoundary from "../../components/lib/errorboundery/ErrorBoundary";
import { fetchWidgetSequenceDataIfNeeded } from "../../actions/home/home";
import ElectionsHPWidget from "../../campaign/election/components/ElectionsHPWidget";

import Loadable from "react-loadable";
const siteConfig = _getStaticConfig();

const MiniTVCard = Loadable({
  loader: () => import("../../components/common/MiniTVCard"),
  LoadingComponent,
});

const ExitPollHPWidget = Loadable({
  loader: () => import("../../campaign/election/components/ExitPollHPWidget"),
  LoadingComponent,
});

const ResultHPWidget = Loadable({
  loader: () => import("../../campaign/election/components/ResultHPWidget"),
  LoadingComponent,
});

// const ElectionsHPWidget = Loadable({
//   loader: () => import("../../campaign/election/components/ElectionsHPWidget"),
//   LoadingComponent,
// });
class DesktopLayout extends Component {
  constructor(props) {
    super(props);
    (this.state = { scroll: false, scrollbound: true, showAfterAds: false }),
      (this.config = {
        isFrmapp: false,
        scroll: false,
        showAfterAds: false,
        hasMounted: false,
      });

    this.scrollRef = this.handleScroll.bind(this);
  }

  componentDidMount() {
    const { dispatch, router, minitv, config } = this.props;
    this.setState({ hasMounted: true });

    let _this = this;
    DesktopLayout.fetchData({ dispatch, router });

    if (!_sessionStorage().get("bnews")) {
      dispatch(fetchBnewsDataIfNeeded());
    }
    // if (!_sessionStorage().get("minitv")) {
    //   dispatch(fetchMiniTvDataIfNeeded());
    // }

    // Trending data of home can be called directly ( empty subsecid)
    // For other pages config.subsec1 must exist ( which sometimes is preloaded in SSR)
    if (config.pagetype == "home" || config.subsec1) {
      dispatch(fetchTrendingDataIfNeeded({ subsec1: (config && config.subsec1) || "" }));
    }

    if (_isCSR()) {
      this.adjustMiniTvPosition();
    }

    // this snippet is just to ensure that if the resizeobserver is not initialized, this will try to initialize it
    if (!window.observeAtfSizeChange) {
      if (observeResizeChange) {
        const newElement = document.getElementById("general-atf-wrapper");
        if (newElement) {
          window.observeAtfSizeChange = true;
          observeResizeChange(newElement);
        }
      }
    }

    window.addEventListener("scroll", this.scrollRef);
    // AdsModule.render({});
    // add an appropriate event listener
  }

  componentDidUpdate(prevProps) {
    const { config, dispatch, router } = this.props;
    // If location and subsec1 values differ for pages, fetch trending data again ( on CSR)
    if (prevProps.config.subsec1 !== config.subsec1 && config.subsec1 != "0") {
      dispatch(fetchTrendingDataIfNeeded({ subsec1: (config && config.subsec1) || "" }));
    }
    // CAUTION : Following code is for again activating the scroll eventlistener for the minitv over ad issue but it will open up plethora of bugs in itself
    // const { pathname } = router.location;
    // const pagetype = getPageType(pathname);
    // if (pagetype && (pagetype === "home" || pagetype === "articleshow" || pagetype === "articlelist")) {
    //   console.log(this.state.scrollbound);
    //   if (!this.state.scrollbound) {
    //     console.log("addingeventlistner to the page on scrolling");
    //     window.addEventListener("scroll", this.scrollRef);
    //   }
    // }
  }

  adjustMiniTvPosition() {
    // Find Mini tv and content divs, move minitv position to right, if exists.
    const contentDiv = document.getElementById("childrenContainer");
    const miniTvDiv = document.getElementById("outer_minitv_container");
    const dockPlayerDiv = document.getElementById("dock-root-container");

    if (contentDiv && miniTvDiv) {
      const distanceFromRight = contentDiv.getBoundingClientRect().left;
      miniTvDiv.style.right = `${distanceFromRight}px`;
    }
    if (contentDiv && dockPlayerDiv) {
      const distanceFromRight = contentDiv.getBoundingClientRect().left;
      dockPlayerDiv.style.right = `${distanceFromRight}px`;
    }
  }

  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return item.override ? item.override : url;
  }

  handleScroll() {
    const { router } = this.props;
    const { pathname } = router.location;
    const pagetype = getPageType(pathname);
    const rhsTopAdDiv = document.getElementsByClassName("ad-top");
    if (
      rhsTopAdDiv &&
      rhsTopAdDiv[0] &&
      (pagetype === "home" || pagetype === "articleshow" || pagetype === "articlelist")
    ) {
      const bottom = rhsTopAdDiv[0].getBoundingClientRect().bottom;
      if (bottom <= 30) {
        this.config.scroll = true;
        this.setState({ showAfterAds: true });
      } else {
        if (!this.state.scroll) {
          this.setState({ scroll: true });
        }
        this.setState({ showAfterAds: false });
      }
    } else {
      this.config.scroll = true;
      this.setState({ scroll: true, showAfterAds: true, scrollbound: false }, this.unbindScroll);
    }
  }

  unbindScroll() {
    window.removeEventListener("scroll", this.scrollRef);
  }

  render() {
    const { header, router, config, bnews, minitv, trending, home, location, election, params } = this.props;
    const msid = params && params.msid ? params.msid : undefined;
    let { pathname } = router.location;
    let pagetype = getPageType(pathname);
    const subsec1 = config && config.subsec1;
    const parentId = config && config.parentId;
    const secId = config && config.secId;
    const hierarchylevel = config && config.hierarchylevel;
    let isFrmApp = _isFrmApp(this.props.router);
    let topVideos = "";
    let topVideoURL = "";
    if (
      home &&
      home.value &&
      home.value[0] &&
      home.value[0].recommended &&
      home.value[0].recommended.video &&
      home.value[0].recommended.video[0] &&
      home.value[0].recommended.video[0].items &&
      Array.isArray(home.value[0].recommended.video[0].items)
    ) {
      topVideoURL = home.value[0].recommended.video[0].override || "";
      const videos = home.value[0].recommended.video[0].items;
      const filteredVideos = videos.filter(item => item.tn != "ad");
      topVideos = [...filteredVideos.slice(0, 5)];
    }

    const widgetToRender = checkWidgetToRender({ election, pagetype, msid });

    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";

    // alaskaSections = getAlaskaSections(objSectionInfo);
    return (
      <div className="container">
        {/* <ErrorBoundary>
          <AdCard mstype="skinlhs" adtype="dfp" pagetype={config && config.pagetype ? config.pagetype : ""} />
        </ErrorBoundary> */}
        <ErrorBoundary>
          <DesktopHeader
            subsec1={subsec1}
            parentId={parentId}
            secId={secId}
            hierarchylevel={hierarchylevel}
            header={header}
            pagetype={config && config.pagetype}
            otherSitesInfo={siteConfig && siteConfig.otherSitesInfo}
            topVideos={topVideos}
            topVideoURL={topVideoURL}
            location={location}
            router={router}
            config={config}
            region={config && config.region}
          />
        </ErrorBoundary>
        <ErrorBoundary>
          <AdCard
            className="emptyAdBox"
            mstype="innove"
            adtype="dfp"
            pagetype={config && config.pagetype ? config.pagetype : ""}
          />
        </ErrorBoundary>

        {/* CTN ppd ad should be called on all pages as discussed by Sandeep */}
        {/* CTN ppd ad should be on pages except homepage corrected by Vasunaik */}
        {config && config.pagetype != "home" ? (
          <ErrorBoundary>
            <AdCard className="emptyAdBox" mstype="ctnppdad" adtype="ctn" />
          </ErrorBoundary>
        ) : null}

        {/* <div id="electionwidget" className="transform" style={{ transform: "translateY(0px)" }}>
          {widgetToRender && (
            <ErrorBoundary>
              <ElectionsHPWidget
                widgetType={widgetToRender}
                sequence={election.result.sequence}
                url={this.props.router.location.pathname}
              />
            </ErrorBoundary>
          )}
        </div> */}

        {/* Render Page Content */}
        <div
          id="childrenContainer"
          className="con_content transform"
          style={{ transform: "translateY(0px)", minHeight: "100vh" }}
        >
          <TrendingMinitvWidget
            trending={trending}
            minitv={minitv}
            showAfterAds={this.state.showAfterAds}
            bnews={bnews}
            location={location}
            scroll={this.state.scroll}
          />
          {this.props.children}
        </div>
        <div id="position-fixed-floater" />
        {/* <DesktopFooter /> */}
        {!this.config.isFrmapp /*&& _isCSR() ?*/ ? (
          <ErrorBoundary>
            <div id="footer-wrapper" className="transform" style={{ transform: "translateY(0px)" }}>
              {this.state.hasMounted ? (
                <Footer
                  header={header}
                  pagetype={config && config.pagetype}
                  initialState={
                    this.props.initialState
                  } /*As we are using _FilterAlJson inside Footer to get alskaData */
                  offfoot={1}
                />
              ) : null}
            </div>
          </ErrorBoundary>
        ) : null}

        <div className="wdt_skin_lhsrhs">
          <div className="rhs">
            <ErrorBoundary>
              <div
                data-adtype="skinrhs"
                className="ad1 skinrhs prerender"
                id="div-gpt-ad-1558437895757-skinrhs"
                data-path={siteConfig.ads.dfpads.skinrhs}
                data-size="[[125, 600],[120, 600],[160, 600]]"
              />
            </ErrorBoundary>
          </div>
        </div>

        {_isCSR() ? (
          <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: "" }} />
        ) : (
          <div>
            {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="andbeyond"
                className="ad1 andbeyond prerender emptyAdBox"
                id="div-gpt-ad-1558437895757-0"
                data-path={siteConfig.ads.dfpads.andbeyond}
                data-size="[[1, 1]]"
              />
            ) : null}
            {process.env.SITE == "nbt" && queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="networkPGT"
                className="ad1 oop prerender emptyAdBox"
                id="div-gpt-ad-1558437895757-0-networkPGT"
                data-path={siteConfig.ads.dfpads.networkPGT}
                data-size="[[1, 1]]"
              />
            ) : null}
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.app,
    initialState: state,
    header: state.header,
    config: state.config,
    routing: state.routing,
    home: state.home,
  };
}

DesktopLayout.fetchData = function({ dispatch, params, query, history, router, state }) {
  let { pathname } = router.location;
  let pagetype = getPageType(pathname);
  const electionConfig =
    state && state.app && state.app.election
      ? getElectionConfigFromData({ electionConfig: state.app.election, pathname, pagetype, params })
      : {};

  if (electionConfig.showWidget) {
    if (electionConfig.shouldSetConfig) {
      const config = {};
      if (electionConfig.widgetType === "exitpoll") {
        config.exitpollurl = electionConfig.widgetUrl;
      }
      if (electionConfig.widgetType === "result") {
        config.resulturl = electionConfig.widgetUrl;
      }
      config.state = electionConfig.stateName;
      const electionConfigArr = [];
      electionConfigArr.push(config);
      dispatch(fetchElectionConfigSuccess(electionConfigArr));
    }
    dispatch(fetchMiniTvDataIfNeeded());
    if (electionConfig.widgetType === "exitpoll") {
      return dispatch(
        fetchExitPollIfNeeded(params, query, { url: electionConfig.widgetUrl, state: electionConfig.stateName }),
      )
        .then(() => {
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        })
        .catch(err => {
          console.log(`error--${err}`);
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        });
    } else if (electionConfig.widgetType === "result") {
      return dispatch(
        fetchResultsIfNeeded(params, query, { url: electionConfig.widgetUrl, state: electionConfig.stateName }),
      )
        .then(() => {
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        })
        .catch(err => {
          console.log(`error--${err}`);
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        });
    }
  }
  // let pagetype = getPageType(pathname)

  // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
  /*
  if (pagetype == 'home' || pagetype == 'liveblog') {
    //dispatch(fetchExitPollIfNeeded());
    //dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
    params && (params.value = _electionConfig.feedlanguage);
    dispatch(fetchELECTIONRESULT_IfNeeded());
    // dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "result"));
  }
  */

  // Mintv is called here to update the CTN Video Ad params Value
  // based on mintv feed response

  dispatch(fetchMiniTvDataIfNeeded());
  return dispatch(fetchHeaderDataIfNeeded()).then(() => {
    dispatch(fetchWidgetSequenceDataIfNeeded());
  });
  // return Promise.resolve([]);
};

const TrendingMinitvWidget = props => {
  const { trending, minitv, showAfterAds, bnews, scroll, location } = props;
  return (
    <div
      id="trending-minitv"
      className="wdt-trending-minitv-breaking transform"
      // style={{ transform: "translateY(0px)" }}
    >
      <div className="header-trending">
        {trending && trending.items && (
          <React.Fragment>
            <b>{siteConfig.locale.trending}</b>
            <ul>
              {Array.isArray(trending.items) &&
                trending.items.map(item => {
                  let itemURL = item && item.override ? item.override : item.wu;
                  if (itemURL && !itemURL.includes("utm_source")) {
                    itemURL = `${itemURL}?utm_source=edittrending&utm_medium=toptrendingwdt&utm_campaign=desktop`;
                  }
                  return (
                    <li key={item.id}>
                      <AnchorLink href={itemURL}>{item.hl}</AnchorLink>
                    </li>
                  );
                })}
            </ul>
          </React.Fragment>
        )}
      </div>

      {/* MINITV */}
      {showAfterAds && minitv && minitv.hl && _isCSR() ? (
        <ErrorBoundary>
          <MiniTVCard minitv={minitv} location={location} />
        </ErrorBoundary>
      ) : null}
      {/* Breaking News */}
      {scroll && bnews ? (
        <ErrorBoundary>
          <BreakingNewsCard bnews={bnews} />
        </ErrorBoundary>
      ) : null}
    </div>
  );
};

export default connect(mapStateToProps)(DesktopLayout);
