import {
  FETCH_DESKTOP_HEADER_REQUEST,
  FETCH_DESKTOP_HEADER_SUCCESS,
  FETCH_DESKTOP_HEADER_FAILURE
} from "../../actions/header/header";

function headerDesktop(
  state = {
    isFetching: false,
    //isFetchingMore: false,
    error: false,
    desktopHeader: null
  },
  action
) {
  switch (action.type) {
    case FETCH_DESKTOP_HEADER_REQUEST:
      return { ...state, isFetching: true, error: false };

    case FETCH_DESKTOP_HEADER_SUCCESS:
      return {
        ...state,
        desktopHeader: action.payload,
        isFetching: false,
        error: false
      };

    case FETCH_DESKTOP_HEADER_FAILURE:
      return { ...state, isFetching: false, error: true };

    default:
      return state;
  }
}

export default headerDesktop;
