import {
  FETCH_HEADER_REQUEST,
  FETCH_HEADER_SUCCESS,
  FETCH_HEADER_FAILURE,
  FETCH_TECH_HEADER_REQUEST,
  FETCH_TECH_HEADER_SUCCESS,
  FETCH_TECH_HEADER_FAILURE,
  FETCH_NAV_DATA_SUCCESS,
} from "actions/header/header";
import {
  FETCH_HOVER_DATA_REQUEST,
  FETCH_HOVER_DATA_SUCCESS,
  FETCH_HOVER_DATA_FAILURE,
} from "../../actions/header/header";

function header(
  state = {
    isFetching: false,
    //isFetchingMore: false,
    navHoverData: {},
    error: false,
  },
  action,
) {
  switch (action.type) {
    case FETCH_HEADER_REQUEST:
      return { ...state, isFetching: true, error: false };
    case FETCH_HEADER_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_HEADER_SUCCESS:
      const alaskaData = action.payload; // data comes from Alaska panel via feed
      return {
        isFetching: false,
        error: false,
        alaskaData: alaskaData,
      };
    case FETCH_NAV_DATA_SUCCESS:
      const navData = action.payload;
      return {
        ...state,
        isFetching: false,
        navData: navData,
      };

    case FETCH_TECH_HEADER_REQUEST:
      state.Gadgets = [];
      return { ...state, isFetching: true, error: false };
    case FETCH_TECH_HEADER_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_TECH_HEADER_SUCCESS:
      state.Gadgets = action.payload;
      return { ...state, isFetching: false, error: false };
    /*  When hover data will be fetched, check if the msid is already present in the data , if present dont dispatch action*/
    case FETCH_HOVER_DATA_REQUEST:
      console.log();
      return {
        ...state,
        navHoverData: {
          ...state.navHoverData,
          [action.payload.msid]: {
            isFetching: "true",
            isError: false,
            data: null,
          },
        },
      };

    case FETCH_HOVER_DATA_SUCCESS:
      return {
        ...state,
        navHoverData: {
          ...state.navHoverData,
          [action.payload.msid]: {
            isFetching: "done",
            isError: false,
            data: action.payload.data,
          },
        },
      };

    case FETCH_HOVER_DATA_FAILURE:
      return {
        ...state,
        navHoverData: {
          ...state.navHoverData,
          [action.payload.msid]: {
            isFetching: false,
            isError: true,
            error: action.payload.error,
          },
        },
      };

    default:
      return state;
  }
}

export default header;
