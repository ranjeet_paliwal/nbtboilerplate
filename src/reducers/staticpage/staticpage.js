import {
  FETCH_STATICPAGE_SUCCESS,
  FETCH_STATICPAGE_FAILURE
} from "./../../actions/staticpage/staticpage";

function STATICPAGE(state = {}, action) {
  switch (action.type) {
    case FETCH_STATICPAGE_SUCCESS:
      state = action.payload;
      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_STATICPAGE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default STATICPAGE;
