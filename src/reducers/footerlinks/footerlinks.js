import {
  FETCH_FOOTER_LINKS_REQUEST,
  FETCH_FOOTER_LINKS_SUCCESS,
  FETCH_FOOTER_LINKS_FAILURE,
} from "actions/footerlinks/footerlinks";

function footerlinks(
  state = {
    isFetching: false,
    error: false,
    msid: "",
    footer_links: {},
  },
  action,
) {
  switch (action.type) {
    case FETCH_FOOTER_LINKS_REQUEST:
      state.footer_links = {};
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_FOOTER_LINKS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_FOOTER_LINKS_SUCCESS:
      state.msid = action.payload.msid ? action.payload.msid : "";
      state.footer_links = action.payload.footer_links ? action.payload.footer_links : {};
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    default:
      return state;
  }
}

export default footerlinks;
