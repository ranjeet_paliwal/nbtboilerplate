import {
  CONFIG_PAGETYPE,
  CONFIG_SEARCH_HEADER,
  CONFIG_PARENTID,
  CONFIG_DATETIME,
  CONFIG_REGION,
  CONFIG_RESET_REGION,
} from "../../actions/config/config";

function config(
  state = {
    pagetype: "",
    searchHeader: false,
    searchTerm: "",
    parentId: "",
    subsec1: "",
    resultLbl: "",
    siteName: "",
    region: "",
  },
  action,
) {
  switch (action.type) {
    case CONFIG_SEARCH_HEADER:
      return {
        ...state,
        searchHeader: action.payload,
        searchTerm: action.term,
      };
    case CONFIG_PAGETYPE:
      return {
        ...state,
        pagetype: action.payload,
        siteName: action.siteName,
      };
    case CONFIG_PARENTID:
      return {
        ...state,
        parentId: action.payload.parentId,
        subsec1: action.payload.subsec1,
        secId: action.payload.secId,
        hierarchylevel: action.payload.hierarchylevel,
      };
    case CONFIG_DATETIME:
      return {
        ...state,
        datetime: action.payload,
      };

    case CONFIG_REGION:
      return {
        ...state,
        region: action.region,
      };

    case CONFIG_RESET_REGION:
      return {
        ...state,
        region: action.region,
      };

    default:
      return state;
  }
}

export default config;
