import { FETCH_SCHEDULE_REQUEST, FETCH_SCHEDULE_SUCCESS, FETCH_SCHEDULE_FAILURE } from "actions/schedule/schedule";

function custom_sort(a, b) {
  return new Date(b.matchdate_ist).getTime() - new Date(a.matchdate_ist).getTime();
}

function schedule(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    seriesid: "",
    msid: "", //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_SCHEDULE_REQUEST:
      state.msid = action.payload;
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_SCHEDULE_SUCCESS:
      if (
        action.payload &&
        action.payload[1].data &&
        action.payload[1].data.matches &&
        action.payload[1].data.matches.length > 0 &&
        action.payload[0].id == state.msid
      ) {
        let matches = [];
        if (action.seriesid && action.seriesid !== "") {
          matches = action.payload[1].data.matches.filter(data => {
            if (action.isResultPage) {
              return data.series_Id == action.seriesid && data.matchstatus == "Match Ended";
            }
            return data.series_Id == action.seriesid;
          });
        } else {
          matches = action.payload[1].data.matches.filter(data => {
            if (action.isResultPage) {
              // return data.matchstatus_Id == 115 && data.matchstatus == "Match Ended";
              return data.matchstatus === "Match Ended";
            }
            return data.matchstatus_Id == 115 || data.matchstatus_Id == 117;
          });
        }
        if (action.isResultPage && matches && matches.length > 0) {
          matches.sort(custom_sort);
        }
        action.payload[1].data.matches = matches;
        state.seriesid = action.seriesid;

        state.value = action.payload;
      }
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_SCHEDULE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default schedule;
