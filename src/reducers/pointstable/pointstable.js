import {
  FETCH_POINTSTABLE_REQUEST,
  FETCH_POINTSTABLE_SUCCESS,
  FETCH_POINTSTABLE_FAILURE
} from "actions/pointstable/pointstable";

function pointstable(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    seriesid: "",
    msid: "" //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action
) {
  switch (action.type) {
    case FETCH_POINTSTABLE_REQUEST:
      state.msid = action.payload;
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_POINTSTABLE_SUCCESS:
      if (
        action.payload &&
        action.payload.length > 0 &&
        action.payload[0].id == state.msid
      ) {
        state.value = action.payload;
      }
      return {
        ...state,
        isFetching: false
      };

    case FETCH_POINTSTABLE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default pointstable;
