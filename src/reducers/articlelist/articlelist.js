import {
  FETCH_LIST_REQUEST,
  FETCH_LIST_SUCCESS,
  FETCH_LIST_FAILURE,
  FETCH_NEXT_LIST_REQUEST,
  FETCH_NEXT_LIST_SUCCESS,
  FETCH_BRAND_REQUEST,
  FETCH_BRAND_SUCCESS,
  FETCH_BRAND_FAILURE,
} from "actions/articlelist/articlelist";

function articlelist(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    msid: "", //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_LIST_REQUEST:
      state.msid = action.payload; // Usage explained above
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_LIST_SUCCESS:
      //console.log("A11", action.payload);
      if (action.payload && action.payload.length > 0 && action.payload[0].id == state.msid) {
        state.value = action.payload && action.payload[0] ? [action.payload[0]] : [];
        state.astroListingWidget = action.payload && action.payload[1] ? action.payload[1] : [];
        state.gadgetsData = action.payload && action.payload[2] ? action.payload[2] : [];
      }

      return {
        ...state,
        isFetching: false,
      };

    case FETCH_LIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_NEXT_LIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_NEXT_LIST_SUCCESS:
      if (
        typeof state.value[0] != "undefined" &&
        state.value[0].hasOwnProperty("items") &&
        action.payload.items &&
        action.payload.items.length > 0
      ) {
        // state.value[0].items = state.value[0].items.concat(
        //   action.payload.items
        // );
        state.value.push({ items: action.payload.items });
        state.value[0].pg.cp = action.payload.pg.cp;
      }
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_BRAND_REQUEST:
      //state.msid = action.payload;
      return {
        ...state,
        isFetchingBrand: true,
        error: false,
      };
    case FETCH_BRAND_SUCCESS:
      let brandlist;
      //get index number of brand section
      if (action.payload && action.payload[0].brand && action.payload[0].brand.length > 0) {
        state.brandList = action.payload;
      }
      // state.value[0].sections.forEach(function(x, index) {
      //   if (x.type && x.type === "brandlist") {
      //     brandlist = index;
      //   }
      // });
      // //assign barnd content
      // if (
      //   action.payload &&
      //   action.payload[0].brand &&
      //   action.payload[0].brand.length > 0
      // ) {
      //   state.value[0].sections[brandlist].brandlisting.brand =
      //     action.payload[0].brand;
      //   state.value[0].sections[brandlist].brandlisting.category =
      //     action.payload[0].category;
      //   state.value[0].sections[brandlist].brandlisting.categoryseo =
      //     action.payload[0].categoryseo;
      // }
      return {
        ...state,
        isFetchingBrand: false,
      };

    case FETCH_BRAND_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default articlelist;
