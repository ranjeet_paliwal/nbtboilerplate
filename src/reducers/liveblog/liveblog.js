import {
  FETCH_LIVEBLOG_REQUEST,
  FETCH_LIVEBLOG_SUCCESS,
  FETCH_LIVEBLOG_FAILURE,
} from "./../../actions/liveblog/liveblog";
import { _isCSR } from "../../utils/util";

function LIVEBLOG(
  state = {
    isFetching: false,
    error: false,
  },
  action,
) {
  switch (action.type) {
    case FETCH_LIVEBLOG_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_LIVEBLOG_SUCCESS:
      // state = action.payload;
      let data = {};
      // data.value = (state[0] && state[0].lbcontent) ? _isCSR() ? state[0] : state[0].lbcontent.slice(0, 20) : state[0];
      data.value = action.payload && action.payload[0];
      data.meta = action.payload && action.payload[1];

      return {
        ...data,
        isFetching: false,
        error: false,
      };

    case FETCH_LIVEBLOG_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default LIVEBLOG;
