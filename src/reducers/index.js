import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import app from "reducers/app/app";
import home from "reducers/home/home";
import header from "reducers/header/header";
import headerDesktop from "reducers/header/headerDesktop";
import photo from "../modules/photos/reducers/photo";
import liveblogstrip from "../modules/liveblogstrip/reducers/liveblogstrip";
import articleshow from "reducers/articleshow/articleshow";
import liveblog from "reducers/liveblog/liveblog";
import staticpage from "reducers/staticpage/staticpage";
import articlelist from "reducers/articlelist/articlelist";
import photomazzashow from "reducers/photomazzashow/photomazzashow";
import config from "reducers/config/config";
import search from "reducers/search/search";
import videoshow from "reducers/videoshow/videoshow";
import videoplayer from "reducers/videoplayer/videoplayer";
import footer from "reducers/footer/footer";
import footerlinks from "reducers/footerlinks/footerlinks";

// import paytmwidget from './../modules/paytmwidget/reducers/paytmwidget';
// import amazonwidget from "../modules/amazonwidget/reducers/amazonwidget";
import exitpoll from "../campaign/election/reducers/exitpoll/exitpoll";
import results from "../campaign/election/reducers/results/results";
import electoralmap from "../campaign/election/reducers/electoralmap/electoralmap";
import candidatelist from "../campaign/election/reducers/candidatelisting/candidatelisting";
import scoreboard from "../campaign/cricket/scoreboard/reducer";
import budgetwidget from "../campaign/budget/budgetwidget/reducer";
import loksabhaelectionresult from "../campaign/election/loksabhaelectionresult/reducer";
import loksabhaexitpoll from "../campaign/election/loksabhaexitpoll/reducer";

import schedule from "reducers/schedule/schedule";
import minischedule from "../modules/minischedule/reducers/minischedule";
import minischedule_cricplay from "../modules/minischedule_cricplay/reducers/minischedule_cricplay";
import pointstablecard from "../modules/pointstablecard/reducers/pointstablecard";

import pointstable from "../campaign/cricket/reducers/pointstable/pointstable";
import IPL from "../campaign/cricket/reducers/IPL/IPL";
import scorecard from "../modules/scorecard/reducers/scorecard";
import newsbrief from "reducers/newsbrief/newsbrief";

import homerecommended from "reducers/homerecommended/homerecommended";
import byelection from "reducers/byelection/byelection";
import topics from "reducers/topics/topics";
import timespoints from "./timespoints/timespoints";
import authentication from "./authentication/authentication";
import movieshow from "./movieshow/movieshow";
import comparisionList from "../reducers/gn/comparision/List/index";
import compareDetail from "../reducers/gn/comparision/Detail/index";
import gadgetshow from "../reducers/gn/Gadgetshow/index";
import gadgetlist from "../reducers/gn/gadgetlist/gadgetlist";
import gadgetfiltertool from "../reducers/gn/gadgetlist/gadgetfiltertool";
import trendsComparision from "../reducers/gn/comparision/Trends";
import gadgetsNowHome from "../reducers/gn/home/home";

const rootReducer = combineReducers({
  app,
  home,
  header,
  headerDesktop,
  photo,
  liveblogstrip,
  articleshow,
  liveblog,
  staticpage,
  articlelist,
  photomazzashow,
  config,
  search,
  videoshow,
  videoplayer,
  IPL,
  minischedule,
  minischedule_cricplay,
  pointstablecard,
  schedule,
  pointstable,
  newsbrief,
  footer,
  footerlinks,
  // paytmwidget,
  // amazonwidget,
  exitpoll,
  electoralmap,
  results,
  candidatelist,
  scoreboard,
  budgetwidget,
  routing: routerReducer,
  loksabhaelectionresult,
  loksabhaexitpoll,
  scorecard,
  byelection,
  topics,

  //gadgets now reducers start
  comparisionList,
  compareDetail,
  gadgetshow,

  gadgetlist,
  gadgetfiltertool,
  trendsComparision,
  gadgetsNowHome, //gadgets now reducers end

  homerecommended,
  timespoints,
  authentication,
  movieshow,
});

export default rootReducer;
