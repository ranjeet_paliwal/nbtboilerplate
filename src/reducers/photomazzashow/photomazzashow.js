/* eslint-disable no-param-reassign */
import {
  FETCH_PHOTOMAZZASHOW_REQUEST,
  FETCH_PHOTOMAZZASHOW_SUCCESS,
  FETCH_PHOTOMAZZASHOW_FAILURE,
  FETCH_NEXT_PHOTOMAZZASHOW_REQUEST,
  FETCH_NEXT_PHOTOMAZZASHOW_SUCCESS,
} from "actions/photomazzashow/photomazzashow";

function photomazzashow(
  state = {
    isFetching: false,
    error: false,
    value: {},
    head: {},
  },
  action,
) {
  switch (action.type) {
    case FETCH_PHOTOMAZZASHOW_REQUEST:
      state.value = {};
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_PHOTOMAZZASHOW_SUCCESS:
      // console.log(action.payload[0]);
      state.value[0] = [action.payload];
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_PHOTOMAZZASHOW_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        errorMessage: action.payload,
      };

    case FETCH_NEXT_PHOTOMAZZASHOW_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_NEXT_PHOTOMAZZASHOW_SUCCESS:
      if (!action.nextGallery) {
        state.value[0][action.galIndex].items = state.value[0][action.galIndex].items.concat(action.payload.items);
        state.value[0][action.galIndex].pg.cp = action.payload.pg.cp;
        if (action.payload.nextset !== undefined) {
          state.value[0][action.galIndex].nextset = action.payload.nextset;
        } else {
          state.value[0][action.galIndex].nextset = "";
        }
      } else if (typeof state.value[0] !== "undefined" && state.value[0].length > 0) {
        state.value[0] = state.value[0].concat(action.payload);
      } else {
        state.value[0] = [action.payload];
      }

      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}

export default photomazzashow;
