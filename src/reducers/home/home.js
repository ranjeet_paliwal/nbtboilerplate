/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import {
  FETCH_WIDGETSEQUENCE_REQUEST,
  FETCH_WIDGETSEQUENCE_SUCCESS,
  FETCH_WIDGETSEQUENCE_FAILURE,
  FETCH_TOPLIST_REQUEST,
  FETCH_TOPLIST_SUCCESS,
  FETCH_TOPLIST_FAILURE,
  // FETCH_ARTICLELIST_SUCCESS,
  // FETCH_ARTICLELIST_FAILURE,
  FETCH_WIDGETDATA_SUCCESS_DESKTOP,
  FETCH_WIDGETDATA_SUCCESS,
  FETCH_WIDGETDATA_FAILURE,
  FETCH_RELATEDVIDEO_REQUEST,
  FETCH_RELATEDVIDEO_SUCCESS,
  FETCH_RELATEDVIDEO_FAILURE,
  FETCH_POLL_REQUEST,
  FETCH_POLL_SUCCESS,
  FETCH_POLL_FAILURE,
  SET_REFRESH_HOMEFEED,
  REMOVE_REFRESH_HOMEFEED,
  MAKE_DATA_CHANGED_FALSE,
} from "actions/home/home";
// import { designConfigs } from "../../containers/desktop/home/designConfigs";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
let feedsection;

function home(
  state = {
    isFetching: false,
    error: false,
    value: [],
    weather: {},
    head: {},
    refreshFeedInterval: "",
    dataChanged: false,
    sections: {},
  },
  action,
) {
  switch (action.type) {
    case MAKE_DATA_CHANGED_FALSE:
      return {
        ...state,
        dataChanged: false,
      };

    case SET_REFRESH_HOMEFEED:
      return {
        ...state,
        refreshFeedInterval: action.payload,
      };

    case REMOVE_REFRESH_HOMEFEED:
      return {
        ...state,
        refreshFeedInterval: "",
      };

    case FETCH_WIDGETSEQUENCE_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_WIDGETSEQUENCE_SUCCESS:
      state.value[1] = action.payload;
      // state.value[1] = siteConfig.homeWidgetsList;
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_WIDGETSEQUENCE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_TOPLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_TOPLIST_SUCCESS: {
      let dataChanged;
      if (state.value[0] && state.value[0].items && action.isFromRefreshFeed) {
        const currentItems = JSON.stringify(action.payload.items);
        const prevItems = JSON.stringify(state.value[0].items);
        if (currentItems === prevItems) {
          dataChanged = false;
        } else {
          dataChanged = true;
        }
      }

      state.value[0] = action.payload;
      return {
        ...state,
        isFetching: false,
        dataChanged,
      };
    }

    case FETCH_TOPLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
      // case FETCH_ARTICLELIST_REQUEST:
      //   return {
      //     ...state,
      //     isFetching: true,
      //     error: false
      //   };
      // case FETCH_ARTICLELIST_SUCCESS:
      //   state.value[1] = state.value[1].map((section)=>{
      //     if(section.msid && action.payload.id && (section.msid  == action.payload.id)){
      //       return action.payload;
      //     }
      //     else return section;
      //   });

      //   return {
      //     ...state,
      //     isFetching: false,
      //   };

      // case FETCH_ARTICLELIST_FAILURE:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     error: true
      //   };

    case FETCH_WIDGETDATA_SUCCESS_DESKTOP:
      if (typeof state.value[2] !== "object") {
        state.value[2] = {};
      }
      state.value[2][action.label.toLowerCase()] = action.payload;
      return {
        ...state,
        sections: { ...state.sections, [action.label.toLowerCase()]: action.payload },
        isFetching: false,
      };

    case FETCH_WIDGETDATA_SUCCESS:
      state.value[1] = state.value[1].map(section => {
        if (
          (section._sec_id && action.payload.id && section._sec_id == action.payload.id) ||
          section._type === action.payload.tn
        ) {
          return action.payload;
        }
        return section;
      });

      return {
        ...state,
        isFetching: false,
      };

    case FETCH_WIDGETDATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_RELATEDVIDEO_REQUEST:
      if (process.env.PLATFORM == "desktop") {
        if (typeof state.value[1] === "undefined") state.value[1] = {};
      } else {
        state.value[1] = state.value[1].map(section => {
          if (section.id && action.payload.id && section._rlvideoid == action.payload.id) {
            section.isfetchingrlvideo = true;
            return section;
          }
          return section;
        });
      }
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_RELATEDVIDEO_SUCCESS:
      if (process.env.PLATFORM === "desktop" && state.value[2] && state.value[2][action.label]) {
        state.value[2][action.label].rlvideo = action.payload;
      } else {
        state.value[1] = state.value[1].map(section => {
          if (section.id && action.payload.id && section._rlvideoid === action.payload.id) {
            section.rlvideo = action.payload;
            section.isfetchingrlvideo = false;
            return section;
          }
          return section;
        });
      }

      return {
        ...state,
        isFetching: false,
      };

    case FETCH_RELATEDVIDEO_FAILURE:
      // state.value[1] = state.value[1].map((section) => {
      //   if (section.id && action.payload.id && (section.rlvideoid == action.payload.id)) {
      //     section.isfetchingrlvideo = false;
      //     return section;
      //   }
      //   else return section;
      // });
      // console.log(action.payload);
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_POLL_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_POLL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_POLL_SUCCESS:
      const pollData = action.payload;
      state = {
        ...state,
        poll: pollData,
      };
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    default:
      return state;
  }
}

export default home;
