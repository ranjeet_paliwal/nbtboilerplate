import {
  FETCH_TOPICS_REQUEST,
  FETCH_TOPICS_SUCCESS,
  FETCH_TOPICS_FAILURE,
  FETCH_NEXT_TOPICS_REQUEST,
  FETCH_NEXT_TOPICS_SUCCESS,
  FETCH_TOPICS_DATA,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAILURE,
} from "actions/topics/topics";

function topics(
  state = {
    isFetching: false,
    error: false,
    value: [],
    topicsData: {},
  },
  action,
) {
  let value = [];
  let msid;
  let profile;

  switch (action.type) {
    case FETCH_TOPICS_REQUEST:
      msid = action.payload; // Usage explained above
      return {
        ...state,
        isFetching: true,
        error: false,
        msid,
      };

    case FETCH_TOPICS_SUCCESS:
      value = action.payload;
      return {
        ...state,
        isFetching: false,
        value,
      };

    case FETCH_TOPICS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_PROFILE_SUCCESS:
      if (action.payload && action.businessType) {
        switch (action.businessType) {
          case "cricketer":
            ({ profile } = action.payload);
            profile.businessType = "cricketer";
            break;
          case "politician":
            profile = action.payload;
            profile.businessType = "politician";
            break;
          default:
        }
      }
      return {
        ...state,
        isFetching: false,
        profile,
      };

    case FETCH_PROFILE_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_NEXT_TOPICS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_TOPICS_DATA:
      const data = { [action.topic]: action.payload };
      let topicsData = { ...state.topicsData, ...data };
      return {
        ...state,
        topicsData,
        isFetching: false,
        error: false,
      };

    case FETCH_NEXT_TOPICS_SUCCESS:
      if (
        typeof state.value != "undefined" &&
        state.value.hasOwnProperty("items") &&
        action.payload.items &&
        action.payload.items.length > 0
      ) {
        state.value.items = state.value.items.concat(action.payload.items);
        state.value.pg.cp = action.payload.pg.cp;
      }
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}

export default topics;
