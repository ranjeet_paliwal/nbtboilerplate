import { FETCH_BRIEF_REQUEST, FETCH_BRIEF_SUCCESS, FETCH_BRIEF_FAILURE } from "actions/newsbrief/newsbrief";

function newsbrief(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    msid: "", //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_BRIEF_REQUEST:
      return {
        ...state,
        isFetching: true,
        msid: action.payload,
        error: false,
      };
    case FETCH_BRIEF_SUCCESS:
      let newValue = [];
      if (action.payload && action.payload.length > 0) {
        newValue = action.payload;
      }
      return {
        ...state,
        value: [].concat(newValue),
        isFetching: false,
      };

    case FETCH_BRIEF_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default newsbrief;
