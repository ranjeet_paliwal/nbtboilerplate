import { UPDATE_USER_DATA } from "../../actions/authentication/authentication";

function authentication(
  state = {
    loggedIn: false,
    userData: null,
  },
  action,
) {
  switch (action.type) {
    case UPDATE_USER_DATA:
      return { loggedIn: Boolean(action.payload), userData: action.payload, userAction: action.userAction };
    default:
      return state;
  }
}

export default authentication;
