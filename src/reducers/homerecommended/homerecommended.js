import {
  FETCH_RECOMMENDED_TOPLIST_REQUEST,
  FETCH_RECOMMENDED_TOPLIST_SUCCESS,
  FETCH_RECOMMENDED_TOPLIST_FAILURE,
  FETCH_NEXT_RECOMMENDED_TOPLIST_REQUEST,
  FETCH_NEXT_RECOMMENDED_TOPLIST_SUCCESS
} from "actions/homerecommended/homerecommended";

function home(
  state = {
    isFetching: false,
    error: false,
    value: []
  },
  action
) {
  switch (action.type) {
    case FETCH_RECOMMENDED_TOPLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_RECOMMENDED_TOPLIST_SUCCESS:
      state.value = action.payload;
      return {
        ...state,
        isFetching: false
      };

    case FETCH_RECOMMENDED_TOPLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_NEXT_RECOMMENDED_TOPLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_NEXT_RECOMMENDED_TOPLIST_SUCCESS:
      if (
        typeof state.value != "undefined" &&
        state.value.length > 0 &&
        action.payload &&
        action.payload.value &&
        action.payload.value.length > 0
      ) {
        state.value = state.value.concat(action.payload.value);
        state.pn = action.payload.pn;
        //state.value[0].pg.cp = action.payload.pg.cp;
      }
      return {
        ...state,
        isFetching: false
      };

    default:
      return state;
  }
}

export default home;
