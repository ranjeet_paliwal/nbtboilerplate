//import {parseFeedSectionTitle} from './../../utils/util';
import * as actionTypes from "actions/videoshow/videoshow";

function videoshow(
  state = {
    isFetching: false,
    error: false,
    isLoadingNextVideo: false,
    value: {},
    head: {}
  },
  action
) {
  switch (action.type) {
    case actionTypes.FETCH_SHOW_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case actionTypes.FETCH_SHOW_SUCCESS:
      state.value = action.payload;
      return {
        ...state,
        isFetching: false
      };

    case actionTypes.FETCH_SHOW_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case actionTypes.UPDATE_VIDEOSHOW_REQUEST:
      return {
        ...state,
        isLoadingNextVideo: true
      };

    case actionTypes.UPDATE_VIDEOSHOW_SUCCESS:
      state.value = action.payload;
      return {
        ...state,
        isLoadingNextVideo: "loaded"
        // data: { ...state.data, videoData: action.payload }
      };

    case actionTypes.UPDATE_VIDEOSHOW_FAILURE:
      state.value = action.payload;
      return {
        ...state,
        isLoadingNextVideo: false
        // data: { ...state.data, videoData: action.payload }
      };
    default:
      return state;
  }
}

export default videoshow;
