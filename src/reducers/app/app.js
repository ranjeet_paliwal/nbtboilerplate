import {
  FETCH_APP_REQUEST,
  FETCH_APP_SUCCESS,
  FETCH_APP_FAILURE,
  FETCH_BNEWS_REQUEST,
  FETCH_BNEWS_SUCCESS,
  FETCH_BNEWS_FAILURE,
  FETCH_MINITV_REQUEST,
  FETCH_MINITV_SUCCESS,
  FETCH_MINITV_FAILURE,
  FETCH_TRENDING_REQUEST,
  FETCH_TRENDING_SUCCESS,
  FETCH_TRENDING_FAILURE,
  FETCH_MINISCORECARD_REQUEST,
  FETCH_MINISCORECARD_FAILURE,
  FETCH_MINISCORECARD_SUCCESS,
  FETCH_WEATHER_REQUEST,
  FETCH_WEATHER_SUCCESS,
  FETCH_WEATHER_FAILURE,
  FETCH_FUEL_REQUEST,
  FETCH_FUEL_SUCCESS,
  FETCH_FUEL_FAILURE,
  FETCH_IPLCAP_REQUEST,
  FETCH_IPLCAP_SUCCESS,
  FETCH_IPLCAP_FAILURE,
} from "actions/app/app";
import { FETCH_RHS_DATA_REQUEST, FETCH_RHS_DATA_SUCCESS, FETCH_RHS_DATA_FAILURE } from "../../actions/app/app";

function app(
  state = {
    bnews: "",
    isFetching: false,
    error: false,
    minitv: {},
    rhsData: null,
  },
  action,
) {
  switch (action.type) {
    case FETCH_APP_REQUEST:
      return { ...state, isFetching: true, error: false };
    case FETCH_APP_SUCCESS:
      return { ...state, isFetching: false, value: action.payload[0] };
    case FETCH_APP_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_BNEWS_REQUEST:
      if (state && typeof state == "object") {
        //fixed Cannot create property 'bnews' on string
        state.bnews = "";
      }

      return { ...state, isFetching: true, error: false };
    case FETCH_BNEWS_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_BNEWS_SUCCESS:
      state = { ...state, bnews: action.payload.bnews };
      return { ...state, isFetching: false, error: false };
    case FETCH_MINITV_REQUEST:
      if (state && typeof state == "object") {
        state.minitv = {};
      }
      return { ...state, isFetching: true, error: false };
    case FETCH_MINITV_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_MINITV_SUCCESS:
      state = { ...state, minitv: action.payload ? action.payload : {} };
      return { ...state, isFetching: false, error: false };

    case FETCH_TRENDING_REQUEST: {
      return { ...state, isFetching: true, error: false };
    }
    case FETCH_TRENDING_FAILURE: {
      return { ...state, isFetching: false, error: true };
    }
    case FETCH_TRENDING_SUCCESS: {
      state = { ...state, trending: action.payload ? action.payload : {} };
      return { ...state, isFetching: false, error: false };
    }

    case FETCH_MINISCORECARD_REQUEST:
      state.mini_scorecard = state.mini_scorecard && state.mini_scorecard.Calendar ? { ...state.mini_scorecard } : {};
      return { ...state, isFetching: true, error: false };
    case FETCH_MINISCORECARD_FAILURE:
      return { ...state, isFetching: false, error: true };
    case FETCH_MINISCORECARD_SUCCESS:
      const scorecard_data = action.payload;
      if (
        scorecard_data &&
        scorecard_data.Calendar &&
        scorecard_data.Calendar.length &&
        scorecard_data.Calendar.length > 0
      ) {
        scorecard_data.Calendar = scorecard_data.Calendar.filter(
          match =>
            (match.live == 1 || match.live == 2 || match.live == 3 || match.live == 4) &&
            (match.teama_short == "IND" ||
              match.teamb_short == "IND" ||
              match.seriesname == "ICC Champions Trophy" ||
              match.seriesname == "Indian Premier League" ||
              match.seriesname == "ICC Cricket World Cup"),
        ); // (match.live == 0 || match.live == 1 || match.live == 3 || match.live == 4)
        if (scorecard_data.Calendar.length > 0) {
          state = { ...state, mini_scorecard: scorecard_data };
        }
      }

      return { ...state, isFetching: false, error: false };

    case FETCH_RHS_DATA_REQUEST:
      return { ...state, isFetching: true, error: false, rhsData: null };

    case FETCH_RHS_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: false,
        rhsData: action.payload,
      };

    case FETCH_RHS_DATA_FAILURE:
      return { ...state, isFetching: false, error: true, rhsData: null };
    case FETCH_WEATHER_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_WEATHER_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_WEATHER_SUCCESS:
      const weatherData = action.payload;
      state = {
        ...state,
        weather: weatherData,
      };
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    case FETCH_FUEL_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_FUEL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_FUEL_SUCCESS:
      const fuelData = action.payload;
      return {
        ...state,
        fuel: fuelData,
        isFetching: false,
        error: false,
      };
    case FETCH_IPLCAP_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_IPLCAP_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_IPLCAP_SUCCESS:
      const capData = action.payload;
      return {
        ...state,
        capData,
        isFetching: false,
        error: false,
      };
    default:
      return state;
  }
}

export default app;
