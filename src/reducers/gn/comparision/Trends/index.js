/* eslint-disable no-use-before-define */
import { actionTypes } from "../../../../actions/gn/comparision/Trends";

const initialState = {
  data: null,
  isFetching: false,
};

function trendsComparision(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGET_TRENDS_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GADGET_TRENDS_SUCCESS: {
      // console.log('AL Action, ', action);

      return { ...state, data: action.payload, isFetching: false };
    }

    // case actionTypes.UPDATE_GADGET_TRENDS_SUCCESS: {
    //   const updatedData = { ...state };
    //   updatedData.data.listData.compareData = action.payload;
    //   return { ...state, updatedData, isFetching: false };
    // }

    default:
      return state;
  }
}

export default trendsComparision;
