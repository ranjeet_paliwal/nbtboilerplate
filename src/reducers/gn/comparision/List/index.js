/* eslint-disable no-use-before-define */
import { actionTypes } from "../../../../actions/gn/comparision/List/index";

const initialState = {
  data: null,
  isFetching: false,
};

function comparisionList(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGET_LIST_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GADGET_LIST_SUCCESS: {
      // console.log('AL Action, ', action);
      const data = action.payload;
      // state = {
      //   ...state,
      //   listData : action.payload
      // }
      const allData = {
        listData: data,
        // metaData,
      };
      return { ...state, data: allData, isFetching: false };
    }

    case actionTypes.UPDATE_GADGET_LIST_SUCCESS: {
      const updatedData = { ...state };
      updatedData.data.listData.compareData = action.payload;

      return {
        ...state,
        updatedData,
        isFetching: false,
      };
    }

    case actionTypes.FETCH_GN_VIDEO_REQUEST: {
      return {
        ...state,
        //data: null,
        isFetching: true,
      };
    }

    case actionTypes.FETCH_GN_VIDEO_SUCCESS: {
      const data = action.payload;
      state = {
        ...state,
        videoData: data,
      };
      return {
        ...state,
        isFetching: false,
      };
    }

    case actionTypes.FETCH_GN_PHOTO_REQUEST: {
      return {
        ...state,
        // data: null,
        isFetching: true,
      };
    }

    case actionTypes.FETCH_GN_PHOTO_SUCCESS: {
      const data = action.payload;
      state = {
        ...state,
        photoData: data,
      };
      return {
        ...state,
        isFetching: false,
      };
    }

    default:
      return state;
  }
}

export default comparisionList;
