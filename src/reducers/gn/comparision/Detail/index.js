import { actionTypes } from "../../../../actions/gn/comparision/Detail/index";

const initialState = {
  data: null,
  isFetching: false,
};

function comparisionDetail(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGET_DETAIL_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GADGET_DETAIL_SUCCESS: {
      // console.log('AL Action, ', action);
      const data = { ...action.payload };

      return { ...state, data, isFetching: false };
    }

    case actionTypes.ADD_GADGET_DETAIL_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.compareData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }

    case actionTypes.REMOVE_GADGET_DETAIL_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.compareData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }

    default:
      return state;
  }
}

export default comparisionDetail;
