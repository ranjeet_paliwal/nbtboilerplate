import { actionTypes } from "../../../actions/gn/gadgetshow/gadgetshow";

function gadgetshow(
  state = {
    data: null,
    isFetching: false,
    isFetchingRelatedArticles: false,
    relatedGadgetsToFetch: [],
    relatedGadgetsData: [],
    relatedArticlesFetchError: false,
    relatedArticlesDataFetched: false,
  },
  action,
) {
  switch (action.type) {
    case actionTypes.FETCH_GADGETSHOW_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
        isFetchingRelatedArticles: false,
        relatedGadgetsToFetch: [],
        relatedGadgetsData: [],
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_GADGETSHOW_SUCCESS: {
      const relatedGadgetsToFetch =
        action.payload &&
        action.payload.gadgetData &&
        action.payload.gadgetData.relatedgadgets &&
        action.payload.gadgetData.relatedgadgets.gadget
          ? [].concat(action.payload.gadgetData.relatedgadgets.gadget).map(rData => rData.uname)
          : [];

      return {
        ...state,
        data: action.payload,
        relatedGadgetsToFetch,
        isFetching: false,
      };
    }

    case actionTypes.FETCH_GADGETSHOW_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        errorMessage: action.payload,
      };

    case actionTypes.FETCH_RELATED_GADGETS_REQUEST:
      return {
        ...state,
        isFetchingRelatedArticles: true,
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_RELATED_GADGETS_SUCCESS:
      const relatedGadgetsToFetch = state.relatedGadgetsToFetch.slice(1);
      return {
        ...state,
        relatedGadgetsData: [...state.relatedGadgetsData, action.payload.gadgetData],
        relatedGadgetsToFetch,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: true,
        relatedArticlesFetchError: false,
      };

    case actionTypes.FETCH_RELATED_GADGETS_FAILURE:
      return {
        ...state,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: false,
        relatedArticlesFetchError: true,
      };

    default:
      return state;
  }
}

export default gadgetshow;
