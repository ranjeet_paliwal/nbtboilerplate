import { actionTypes } from "../../../actions/gn/home/home";

const initialState = {
  data: null,
  isFetching: false,
};

function gadgetsNowHome(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GN_HOME_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GN_HOME_SUCCESS: {
      return { ...state, data: action.payload, isFetching: false };
    }

    default:
      return state;
  }
}

export default gadgetsNowHome;
