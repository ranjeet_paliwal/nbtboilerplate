import {
  FETCH_FILTERLIST_REQUEST,
  FETCH_FILTERLIST_SUCCESS,
  FETCH_FILTERLIST_FAILURE,
} from "../../../actions/gn/gadgetlist/gadgetfiltertool";

const initialState = {
  isFetching: false,
  error: false,
  techgadgetfilter: [],
  category: "",
};

function gadgetfiltertool(state = initialState, action) {
  switch (action.type) {
    case FETCH_FILTERLIST_REQUEST:
      state.category = action.category ? action.category : "";
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_FILTERLIST_SUCCESS:
      if (
        action.payload &&
        (state.category != action.category ||
          (action.payload && action.payload.facets && action.payload.facets.length > 0))
      ) {
        state.techgadgetfilter = action.payload;
      }
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_FILTERLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default gadgetfiltertool;
