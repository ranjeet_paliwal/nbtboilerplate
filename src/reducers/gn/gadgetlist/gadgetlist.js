import { actionTypes } from "../../../actions/gn/gadgetlist/gadgetlist";

const initialState = {
  isFetching: false,
  error: false,
  gadgetsData: null,
  urlParams: null,
};

function gadgetlist(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGETLIST_REQUEST:
      return {
        ...state,
        gadgetsData: null,
        urlParams: null,
        isFetching: true,
        error: false,
      };
    case actionTypes.FETCH_GADGETLIST_SUCCESS:
      return {
        ...state,
        gadgetsData: action.payload.gadgetListData,
        urlParams: action.payload.params,
        isFetching: "loaded",
        error: false,
      };

    case actionTypes.FETCH_GADGETLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case actionTypes.FETCH_NEXT_GADGETLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case actionTypes.FETCH_NEXT_GADGETLIST_SUCCESS:
      const newGadgetsPayload = JSON.parse(JSON.stringify(action.payload));
      const newGadgets = newGadgetsPayload.gadgets;
      const existingGadgets = JSON.parse(JSON.stringify(state.gadgetsData.gadgets));
      const mergedGadgets = [...existingGadgets, ...newGadgets];
      state.gadgetsData.gadgets = mergedGadgets;
      state.gadgetsData.pg.cp = newGadgetsPayload.pg.cp;
      return {
        ...state,
        // gadgetsData: action.payload,
        // urlParams: action.payload.params,
        isFetching: false,
      };

    // if (typeof state.item[0] !== "undefined") {
    //   state.item[action.curpg - 1] = action.payload;
    // }
    // return {
    //   ...state,
    //   isFetching: false,
    // };

    default:
      return state;
  }
}

export default gadgetlist;
