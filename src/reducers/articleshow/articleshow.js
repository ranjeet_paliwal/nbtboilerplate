import {
  FETCH_ARTICLESHOW_SUCCESS,
  FETCH_ARTICLESHOW_FAILURE,
  FETCH_ARTICLESHOW_REQUEST,
  FETCH_NEXT_ARTICLESHOW_SUCCESS,
  FETCH_NIC_ARTICLESHOW_SUCCESS,
  RESET_ON_UNMOUNT,
} from "./../../actions/articleshow/articleshow";

function ARTICLESHOW(
  state = {
    isFetching: false,
    isFetchingMore: false,
    error: false,
    items: [],
  },
  action,
) {
  switch (action.type) {
    case FETCH_ARTICLESHOW_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_ARTICLESHOW_SUCCESS:
      state.items = []; // empty storylist items
      state.items = state.items.concat(action.payload);
      state.advertorialPerpectual = true;
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_NEXT_ARTICLESHOW_SUCCESS:
      let items = action.payload;
      state.items = state.items.concat(items);
      state.advertorialPerpectual = false;

      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_NIC_ARTICLESHOW_SUCCESS:
      state.itemNIC = action.payload;
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_ARTICLESHOW_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        errorMessage: action.payload,
      };

    case RESET_ON_UNMOUNT:
      state.items = []; // empty storylist items
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    default:
      return state;
  }
}

export default ARTICLESHOW;
