import {
  FETCH_QUICK_LINKS_REQUEST,
  FETCH_QUICK_LINKS_SUCCESS,
  FETCH_QUICK_LINKS_FAILURE
} from "actions/footer/footer";

function footer(
  state = {
    isFetching: false,
    error: false,
    msid: "",
    quick_links: {}
  },
  action
) {
  switch (action.type) {
    case FETCH_QUICK_LINKS_REQUEST:
      state.quick_links = {};
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_QUICK_LINKS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_QUICK_LINKS_SUCCESS:
      state.msid = action.payload.msid ? action.payload.msid : "";
      state.quick_links = action.payload.quick_links
        ? action.payload.quick_links
        : {};
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default footer;
