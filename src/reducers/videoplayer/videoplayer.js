/* eslint-disable no-param-reassign */
import {
  ADD_VIDEO,
  REMOVE_VIDEO,
  SHOW_VIDEOPOPUP,
  HIDE_VIDEOPOPUP,
  ADD_VIDEO_CONFIG,
  REMOVE_VIDEO_CONFIG,
  ADD_SLIDER_DATA,
  REMOVE_SLIDER_DATA,
  SET_VIDEO_HEADER,
  ADD_PREV_URL_POPUP,
  SET_VIDEO_TYPE,
  SET_VIDEO_MSID,
  SET_MINITV_PLAYING,
  SET_MINITV_VISIBILITY,
  SET_MINITV_WIDGET_CLICK,
  UPDATE_VIDEO_CONFIG,
  UPDATE_AD_STATE,
} from "../../actions/videoplayer/videoplayer";

function videoplayer(
  state = {
    showPopup: false,
    isMinitvPlaying: false,
    showMinitv: true,
    videoConfig: {},
    videoHeader: "",
    videoUrl: "",
    msid: "",
    prevUrlPopup: "",
    sliderData: [],
    videoType: "",
    videoItem: {},
    isMinitvClicked: false,
    isAdPlaying: true,
  },
  action,
) {
  switch (action.type) {
    case SET_MINITV_VISIBILITY:
      state.showMinitv = action.payload;
      return {
        ...state,
      };

    case SET_MINITV_WIDGET_CLICK:
      state.isMinitvClicked = action.payload;
      return {
        ...state,
      };

    case SET_MINITV_PLAYING:
      state.isMinitvPlaying = action.payload;
      return {
        ...state,
      };

    case UPDATE_AD_STATE:
      state.isAdPlaying = action.payload;
      return {
        ...state,
      };

    case SHOW_VIDEOPOPUP:
      state.showPopup = true;
      return {
        ...state,
      };

    case SET_VIDEO_TYPE:
      state.videoType = action.payload;
      return {
        ...state,
      };

    case SET_VIDEO_MSID:
      state.videoItem = action.payload.video;
      state.msid = action.payload.msid;
      return {
        ...state,
      };

    case ADD_PREV_URL_POPUP:
      state.prevUrlPopup = action.payload;
      return {
        ...state,
      };

    case HIDE_VIDEOPOPUP:
      state.showPopup = false;
      return {
        ...state,
      };

    case SET_VIDEO_HEADER: {
      state.videoHeader = action.payload.title;
      state.videoUrl = action.payload.url;
      return {
        ...state,
      };
    }

    case ADD_VIDEO_CONFIG: {
      state.videoConfig = action.payload;
      state.msid = action.payload.video.msid;
      return {
        ...state,
      };
    }

    case UPDATE_VIDEO_CONFIG: {
      state.videoConfig.video = action.payload;
      state.msid = action.payload.msid;
      return {
        ...state,
      };
    }

    case REMOVE_VIDEO_CONFIG: {
      state.videoConfig = {};
      state.msid = "";
      return {
        ...state,
      };
    }

    case ADD_SLIDER_DATA: {
      state.sliderData = action.payload;
      return {
        ...state,
      };
    }

    case REMOVE_SLIDER_DATA: {
      state.sliderData = [];
      return {
        ...state,
      };
    }

    case ADD_VIDEO:
      state.videoData = action.payload;
      return {
        ...state,
      };

    case REMOVE_VIDEO:
      state.videoData = {};
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default videoplayer;
