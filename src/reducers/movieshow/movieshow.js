import {
  FETCH_MOVIESHOW_SUCCESS,
  FETCH_MOVIESHOW_FAILURE,
  FETCH_MOVIESHOW_REQUEST,
  FETCH_NEXT_MOVIESHOW_SUCCESS,
  RESET_ON_UNMOUNT,
} from "../../actions/movieshow/moviewshow";

const INITIAL_STATE = {
  isFetching: false,
  isFetchingMore: false,
  error: false,
  items: [],
};
const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_MOVIESHOW_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_MOVIESHOW_SUCCESS:
      return {
        ...state,
        items: [action.payload],
        isFetching: false,
        error: false,
      };

    case FETCH_NEXT_MOVIESHOW_SUCCESS:
      return {
        ...state,
        items: state.items.concat(action.payload),
        isFetching: false,
        error: false,
      };

    case FETCH_MOVIESHOW_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        errorMessage: action.payload,
      };

    case RESET_ON_UNMOUNT:
      return INITIAL_STATE;

    default:
      return state;
  }
}

export default reducer;
