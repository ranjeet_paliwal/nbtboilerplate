import {
  FETCH_LOGIN_POINTS_SUCCESS,
  HEADER_CROWN_REWARD_POINTS,
  POINTS_RECEIVED_ERROR,
  RECEIVE_ALL_ACTIVITIES,
  INITIALIZE_TP_FLAG,
  RECEIVE_FAQ,
} from "../../actions/timespoints/timespoints";

import {
  RECEIVE_USER_REWARDS,
  RECEIVE_USER_PROFILE,
  RECEIVE_DAILY_CHECKIN_DETAILS,
  RECEIVE_DAILY_ACTIVITY,
} from "../../actions/timespoints/sdkactions";

function timespoints(state = {}, action) {
  switch (action.type) {
    case FETCH_LOGIN_POINTS_SUCCESS:
      const response = (action.payload && action.payload.data && action.payload.data.response) || "";
      const totalPoints = response && response.points;
      return {
        ...state,
        points: totalPoints,
        userData: response,
      };
    case INITIALIZE_TP_FLAG:
      return {
        ...state,
        tpinitialized: true,
      };
    case HEADER_CROWN_REWARD_POINTS:
      return {
        ...state,
        headerCrownPoints: action.payload,
      };

    case RECEIVE_USER_REWARDS:
      return {
        ...state,
        userRewards: action.payload,
      };

    case RECEIVE_FAQ:
      return {
        ...state,
        faq: action.payload,
      };
    case RECEIVE_USER_PROFILE:
      return {
        ...state,
        userProfile: action.payload,
      };
    case RECEIVE_DAILY_CHECKIN_DETAILS:
      return {
        ...state,
        dailyCheckInDetails: action.payload,
      };
    case RECEIVE_ALL_ACTIVITIES:
      return {
        ...state,
        ...action.payload,
        // allActivity: action.payload.allActivity,
        //activitiesMapList: action.payload.activitiesMapList,
      };
    case RECEIVE_DAILY_ACTIVITY:
      return {
        ...state,
        dailyActivity: action.payload,
      };

    default:
      return state;
  }
}

export default timespoints;
