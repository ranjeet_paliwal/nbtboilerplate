import {
  FETCH_SEARCH_REQUEST,
  FETCH_SEARCH_SUCCESS,
  FETCH_SEARCH_FAILURE,
  FETCH_NEXT_SEARCH_REQUEST,
  FETCH_NEXT_SEARCH_SUCCESS,
  CLEAR_SEARCH_RESULT
} from "actions/search/search";

let matched = false;

function search(
  state = {
    isFetching: false,
    error: false,
    value: {}
  },
  action
) {
  switch (action.type) {
    case FETCH_SEARCH_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_NEXT_SEARCH_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_SEARCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        value: action.payload
      };
    case FETCH_NEXT_SEARCH_SUCCESS:
      let value = action.payload;
      if (state.value != "" && state.value.hasOwnProperty("items")) {
        state.value.items = state.value.items.concat(value.items);
        state.value.pg.cp = value.pg.cp;
      } else {
        state.value = value;
      }
      return {
        ...state,
        isFetching: false
      };
    case FETCH_SEARCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case CLEAR_SEARCH_RESULT:
      return {
        isFetching: false,
        error: false,
        value: {}
      };
    default:
      return state;
  }
}

export default search;
