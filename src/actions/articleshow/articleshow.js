import fetch from "utils/fetch/fetch";
import { _apiBasepointUpdate, _getStaticConfig } from "../../utils/util";
export const FETCH_ARTICLESHOW_SUCCESS = "FETCH_ARTICLESHOW_SUCCESS";
export const FETCH_NIC_ARTICLESHOW_SUCCESS = "FETCH_NIC_ARTICLESHOW_SUCCESS";
export const FETCH_ARTICLESHOW_FAILURE = "FETCH_ARTICLESHOW_FAILURE";
export const FETCH_ARTICLESHOW_REQUEST = "FETCH_ARTICLESHOW_REQUEST";
export const FETCH_NEXT_ARTICLESHOW_SUCCESS = "FETCH_NEXT_ARTICLESHOW_SUCCESS";
export const RESET_ON_UNMOUNT = "RESET_ON_UNMOUNT";
import { FETCH_RHS_DATA_SUCCESS } from "../../actions/app/app";
import { isMobilePlatform } from "../../utils/util";
const siteConfig = _getStaticConfig();

function fetchArticleShowDataFailure(error) {
  return {
    type: FETCH_ARTICLESHOW_FAILURE,
    payload: error && error.message,
  };
}

function fetchArticleShowDataSuccess(data) {
  return {
    type: FETCH_ARTICLESHOW_SUCCESS,
    payload: data,
  };
}

function fetchNICArticleShowDataSuccess(data) {
  return {
    type: FETCH_NIC_ARTICLESHOW_SUCCESS,
    payload: data,
  };
}

function fetchNextArticleShowDataSuccess(data) {
  return {
    type: FETCH_NEXT_ARTICLESHOW_SUCCESS,
    payload: data,
  };
}

function superHitWidgetWithAdvertorial(advertorialData, item) {
  //Advertorial story
  let checkPlatform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";
  let filteradvertorial =
    advertorialData && Array.isArray(advertorialData) && advertorialData.length > 0
      ? advertorialData.filter(fdata => {
          return (
            (checkPlatform === "mobile" ? fdata.ctype == "adv_wap" : fdata.ctype == "adv") &&
            fdata.id != item.id &&
            fdata.wu &&
            fdata.wu.includes(process.env.WEBSITE_URL)
          );
        })
      : [];

  if (filteradvertorial && filteradvertorial.length > 1) {
    // suffle Advertorial
    for (let i = filteradvertorial.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      let temp = filteradvertorial[i];
      filteradvertorial[i] = filteradvertorial[j];
      filteradvertorial[j] = temp;
    }
    filteradvertorial.splice(1);
  }

  let superhitwidgetData;
  let superhitwidgetArray;
  let superhitwidgetFilterData;
  let superhitwidgetArraywithId = [];
  if (
    item &&
    item.recommended &&
    item.recommended.superhitwidget &&
    item.recommended.superhitwidget[0] &&
    item.recommended.superhitwidget[0].items
  ) {
    superhitwidgetData = [].concat(filteradvertorial, item.recommended.superhitwidget[0].items);
  }

  superhitwidgetFilterData =
    superhitwidgetData && Array.isArray(superhitwidgetData) && superhitwidgetData.length > 0
      ? superhitwidgetData.filter(fdata => {
          return (
            fdata.id != item.id &&
            !superhitwidgetArraywithId.includes(fdata.id) &&
            superhitwidgetArraywithId.push(fdata.id)
          );
        })
      : [];
  superhitwidgetFilterData && superhitwidgetFilterData.length > 5
    ? superhitwidgetFilterData.splice(5)
    : superhitwidgetFilterData;

  //UTM at SuperHitWidget
  if (superhitwidgetFilterData && superhitwidgetFilterData.length > 1) {
    superhitwidgetArray = superhitwidgetFilterData.map((data, index) => {
      let widgetObj = data;
      //UTM at superhit widget
      if (widgetObj) {
        widgetObj.override = `${widgetObj.override ||
          widgetObj.wu}?utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article${index + 1}`;
      }
      return widgetObj;
    });
  }
  return superhitwidgetArray;
}

function fetchArticleShowData(state, params, query, router) {
  const tagparams = "";
  // const apiUrl=`https://langdev8352.indiatimes.com/pwafeeds/sc_articleshow.cms?feedtype=sjson&version=v9&msid=74356122&upcache=2`
  const apiUrl =
    /*  FIXME: Change feed path to pwa_articleshow */
    `${_apiBasepointUpdate(router, params.splat)}/sc_articleshow.cms?feedtype=sjson&version=v9&msid=${
      params.msid
    }${tagparams}`;
  // router && router.location && (router.location.pathname.indexOf('63531454') || router.location.pathname.indexOf('63531177')) > -1 ?
  // process.env.API_BASEPOINT+`/pwa_articleshow_test.cms?feedtype=sjson&version=v9&msid=${params.msid}`
  // :

  let rhsApiUrl = "";
  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    rhsApiUrl = `${process.env.API_BASEPOINT}/pwa_home_new.cms?feedtype=sjson`;
  } else {
    rhsApiUrl = `${siteConfig.langnetstorageBasePath}/${siteConfig.channelCode}headline.htm`;
  }

  return dispatch => {
    let Promise1 = fetch(apiUrl, { credentials: "same-origin" });
    let Promise2 = fetch(rhsApiUrl);
    return (
      Promise.all([Promise1, Promise2])
        // return fetch(apiUrl, { credentials: "same-origin" })
        .then(
          data => {
            let articleData = (data && data[0]) || [];
            let advertorialData = (data && data[1] && data[1].items) || [];
            if (advertorialData && advertorialData.length > 1) {
              if (
                articleData &&
                articleData.recommended &&
                articleData.recommended.superhitwidget &&
                articleData.recommended.superhitwidget[0]
              ) {
                articleData.recommended.superhitwidget[0].items = superHitWidgetWithAdvertorial(
                  advertorialData,
                  articleData,
                );
              }
            }

            if (typeof data === "object") {
              if (query && query.type == "nic" && isMobilePlatform()) {
                dispatch({ type: FETCH_RHS_DATA_SUCCESS, payload: advertorialData });
                return dispatch(fetchNICArticleShowDataSuccess(articleData));
              }
              dispatch({ type: FETCH_RHS_DATA_SUCCESS, payload: advertorialData });
              return dispatch(fetchArticleShowDataSuccess(articleData));
            }
            return dispatch(fetchArticleShowDataFailure(articleData));
          },
          error => dispatch(fetchArticleShowDataFailure(error)),
        )
        .catch(error => {
          dispatch(fetchArticleShowDataFailure(error));
        })
    );
  };
}

function shouldFetchArticleShowData(state, params, query) {
  // check if NIC articleshow item , and already fetched or not
  if (
    query &&
    query.type == "nic" &&
    state.articleshow.itemNIC &&
    isMobilePlatform() &&
    state.articleshow.itemNIC.id == params.msid
  ) {
    return false;
  }
  if (query && query.type == "nic" && isMobilePlatform() && !state.articleshow.itemNIC) {
    return true;
  }
  if (
    state.articleshow.items.length > 0 &&
    state.articleshow.items[0] &&
    state.articleshow.items[0].id == params.msid
  ) {
    return false;
  }
  return true;
}

export function fetchArticleShowIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchArticleShowData(getState(), params, query)) {
      return dispatch(fetchArticleShowData(getState(), params, query, router));
    }
    return Promise.resolve([]);
  };
}

export function fetchNextArticleShowData(nextmsid, dispatch, params, query, router, advertorialData) {
  window ? (window.isesi = 0) : null;
  window ? (window.colab = true) : null;

  const tagparams = "";
  const apiUrl =
    // router && router.location && router.location.pathname && (router.location.pathname.indexOf('68737608') || router.location.pathname.indexOf('68700324') || router.location.pathname.indexOf('68686852')) > -1 ?
    // process.env.API_BASEPOINT+`/pwa_articleshow_test.cms?feedtype=sjson&version=v9&msid=${nextmsid}`
    // :
    `${_apiBasepointUpdate(
      router,
      params.splat,
    )}/sc_articleshow.cms?feedtype=sjson&version=v9&msid=${nextmsid}${tagparams}`;

  dispatch({
    type: FETCH_ARTICLESHOW_REQUEST,
  });

  return fetch(apiUrl)
    .then(
      data => {
        if (advertorialData && advertorialData.length > 1) {
          if (data && data.recommended && data.recommended.superhitwidget && data.recommended.superhitwidget[0]) {
            data.recommended.superhitwidget[0].items = superHitWidgetWithAdvertorial(advertorialData, data);
          }
        }
        return dispatch(fetchNextArticleShowDataSuccess(data));
      },
      error => dispatch(fetchArticleShowDataFailure(error)),
    )
    .catch(error => {
      dispatch(fetchArticleShowDataFailure(error));
    });
}

export function resetOnUnmount(dispatch) {
  dispatch({
    type: RESET_ON_UNMOUNT,
  });
}
