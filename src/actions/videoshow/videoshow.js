import fetch from "utils/fetch/fetch";
export const FETCH_SHOW_REQUEST = "FETCH_SHOW_REQUEST";
export const FETCH_SHOW_SUCCESS = "FETCH_SHOW_SUCCESS";
export const FETCH_SHOW_FAILURE = "FETCH_SHOW_FAILURE";

export const UPDATE_VIDEOSHOW_REQUEST = "UPDATE_VIDEOSHOW_REQUEST";
export const UPDATE_VIDEOSHOW_SUCCESS = "UPDATE_VIDEOSHOW_SUCCESS";
export const UPDATE_VIDEOSHOW_FAILURE = "UPDATE_VIDEOSHOW_FAILURE";

function fetchShowDataFailure(error) {
  return {
    type: FETCH_SHOW_FAILURE,
    payload: error.message
  };
}
function fetchShowDataSuccess(data) {
  return {
    type: FETCH_SHOW_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function updateVideoShowDataSuccess(videoData) {
  return {
    type: UPDATE_VIDEOSHOW_SUCCESS,
    payload: videoData
  };
}

function updateVideoShowDataFailure(error) {
  return {
    type: UPDATE_VIDEOSHOW_FAILURE,
    payload: error
  };
}

export function updateVideoShow(videoId) {
  return dispatch => {
    dispatch({
      type: UPDATE_VIDEOSHOW_REQUEST
    });

    fetch(
      `${process.env.API_BASEPOINT}/sc_videoshow/${videoId}.cms?feedtype=json`
    ).then(
      videoData => dispatch(updateVideoShowDataSuccess(videoData)),
      error => dispatch(updateVideoShowDataFailure(error))
    );
  };
}

function fetchShowData(state, params) {
  let apiUrl =
    process.env.API_BASEPOINT +
    "/sc_videoshow/" +
    params.msid +
    ".cms?feedtype=sjson";
  // console.log("apiUrl", apiUrl);
  return dispatch => {
    dispatch({
      type: FETCH_SHOW_REQUEST
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchShowDataSuccess(data)),
        error => dispatch(fetchShowDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchShowDataFailure(error));
      });
  };
}

function shouldFetchShowData(state, params) {
  const { videoshow } = state;
  if (
    (videoshow && videoshow.value && !videoshow.value.items) ||
    (videoshow.value.items[0] && videoshow.value.items[0].id != params.msid)
  ) {
    return true;
  }

  return false;
}

export function fetchShowDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchShowData(getState(), params)) {
      return dispatch(fetchShowData(getState(), params));
    } else {
      return Promise.resolve([]);
    }
  };
}
