import fetch from "utils/fetch/fetch";
import { isMobilePlatform } from "../../utils/util";
export const FETCH_PHOTOMAZZASHOW_REQUEST = "FETCH_PHOTOMAZZASHOW_REQUEST";
export const FETCH_PHOTOMAZZASHOW_SUCCESS = "FETCH_PHOTOMAZZASHOW_SUCCESS";
export const FETCH_PHOTOMAZZASHOW_FAILURE = "FETCH_PHOTOMAZZASHOW_FAILURE";
export const FETCH_NEXT_PHOTOMAZZASHOW_REQUEST = "FETCH_NEXT_PHOTOMAZZASHOW_REQUEST";
export const FETCH_NEXT_PHOTOMAZZASHOW_SUCCESS = "FETCH_NEXT_PHOTOMAZZASHOW_SUCCESS";

function fetchPhotoMazzaShowDataFailure(error) {
  return {
    type: FETCH_PHOTOMAZZASHOW_FAILURE,
    payload: error && error.message,
  };
}
function fetchPhotoMazzaShowDataSuccess(data) {
  // Get recommended items from data
  const recommendedItems = data[1];
  // Add to first node of array
  const dataWithRecommendedItems = data[0] ? { ...data[0], recommendedItems } : [];
  return {
    type: FETCH_PHOTOMAZZASHOW_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: dataWithRecommendedItems,
  };
}

function fetchNextPhotoMazzaShowDataSuccess(data, nextGallery, galIndex) {
  // Get recommended items from data
  const recommendedItems = data[1];
  // Add to first node of array
  const dataWithRecommendedItems = data[0] ? { ...data[0], recommendedItems } : data[0];
  return {
    type: FETCH_NEXT_PHOTOMAZZASHOW_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: dataWithRecommendedItems,
    nextGallery,
    galIndex,
  };
}

function getMSID(msid) {
  if (msid.indexOf("msid-") > -1 && msid.indexOf("picid-") > -1) {
    const msidData = msid.match(/picid-[0-9]+/);
    return msidData && msidData[0] && msidData[0].split("-")[1];
  }
  return msid;
}

function fetchPhotoMazzaShowData(params) {
  const parsedMSID = getMSID(params.msid);
  const apiUrl = `${process.env.API_BASEPOINT}/sc_photoshow.cms?feedtype=sjson&msid=${parsedMSID}`;
  // Api url for recommended widget
  const recommendedWidgetUrl = `${process.env.API_BASEPOINT}/sc_related_photos/${parsedMSID}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({
      type: FETCH_PHOTOMAZZASHOW_REQUEST,
    });
    const galleryDataPromise = fetch(apiUrl);
    // Recommended widget called on SSR for mobile, AMP pages
    const recommendedWidgetPromise = isMobilePlatform()
      ? fetch(recommendedWidgetUrl).catch(e => null)
      : Promise.resolve(null);

    return Promise.all([galleryDataPromise, recommendedWidgetPromise])
      .then(
        data => dispatch(fetchPhotoMazzaShowDataSuccess(data)),
        error => dispatch(fetchPhotoMazzaShowDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchPhotoMazzaShowDataFailure(error));
      });
  };
}

function fetchNextPhotoMazzaShowData(state, params, query, nextgalid, galIndex) {
  let curpg = 1;
  // eslint-disable-next-line no-unused-expressions
  window ? (window.isesi = 0) : null;
  // eslint-disable-next-line no-unused-expressions
  window ? (window.colab = true) : null;

  let apiUrl = "";
  let fetchNextGallery = false;
  if (
    typeof state.photomazzashow.value[0] !== "undefined" &&
    state.photomazzashow.value[0].length > 0 &&
    state.photomazzashow.value[0][galIndex] &&
    state.photomazzashow.value[0][galIndex].pg &&
    typeof state.photomazzashow.value[0][galIndex].pg.cnt !== "undefined"
  ) {
    // eslint-disable-next-line radix
    curpg = parseInt(state.photomazzashow.value[0][galIndex].pg.cp) + 1;
    if (
      state.photomazzashow.value[0][galIndex].pg.tp !== "" &&
      curpg <= state.photomazzashow.value[0][galIndex].pg.tp &&
      typeof state.photomazzashow.value[0][galIndex].nextset !== "undefined" &&
      state.photomazzashow.value[0][galIndex].nextset !== ""
    ) {
      apiUrl = `${process.env.API_BASEPOINT}${state.photomazzashow.value[0][galIndex].nextset}`;
    } else {
      fetchNextGallery = true;
      apiUrl = `${process.env.API_BASEPOINT}/sc_photoshow.cms?feedtype=sjson&msid=${nextgalid || params.msid}`;
    }
  } else {
    fetchNextGallery = true;
    apiUrl = `${process.env.API_BASEPOINT}/sc_photoshow.cms?feedtype=sjson&msid=${nextgalid || params.msid}`;
  }
  // Api url for recommended widget
  const recommendedWidgetUrl = `${process.env.API_BASEPOINT}/sc_related_photos/${nextgalid ||
    params.msid}.cms?feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_NEXT_PHOTOMAZZASHOW_REQUEST,
    });
    //FIXME: Currently for mobile, this request goes from here, for desktop , directly through widget.
    //If fetching next gallery, fetch its recommended content here ( ONLY FOR MOBILE)
    const recommendedWidgetPromise =
      isMobilePlatform() && fetchNextGallery ? fetch(recommendedWidgetUrl) : Promise.resolve(null);

    const nextGalleryPromise = fetch(apiUrl);
    return Promise.all([nextGalleryPromise, recommendedWidgetPromise])
      .then(
        data => dispatch(fetchNextPhotoMazzaShowDataSuccess(data, fetchNextGallery, galIndex)),
        error => dispatch(fetchPhotoMazzaShowDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchPhotoMazzaShowDataFailure(error));
      });
  };
}

function shouldFetchPhotoMazzaShowData(state, params, query) {
  if (
    typeof state.photomazzashow.value[0] === "undefined" ||
    typeof state.photomazzashow.value[0][0].it === "undefined" ||
    state.photomazzashow.value[0][0].it.id != params.msid ||
    typeof state.photomazzashow.value[0][0].pwa_meta.first !== "undefined"
  ) {
    return true;
  }
  return false;
}

export function fetchPhotoMazzaShowDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchPhotoMazzaShowData(getState(), params, query)) {
      return dispatch(fetchPhotoMazzaShowData(params));
    }
    return Promise.resolve([]);
  };
}

export function fetchNextPhotoMazzaShowDataIfNeeded(params, query, nextgalid, galIndex) {
  return (dispatch, getState) => dispatch(fetchNextPhotoMazzaShowData(getState(), params, query, nextgalid, galIndex));
}
