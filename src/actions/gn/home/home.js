/* eslint-disable radix */
import fetch from "../../../utils/fetch/fetch";
import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

export const actionTypes = {
  FETCH_GN_HOME_REQUEST: "FETCH_GN_HOME_REQUEST",
  FETCH_GN_HOME_SUCCESS: "FETCH_GN_HOME_SUCCESS",
  FETCH_GN_HOME_FAILURE: "FETCH_GN_HOME_FAILURE",
};

function fetchListDataFailure(error) {
  return {
    type: actionTypes.FETCH_GN_HOME_FAILURE,
    payload: error.message,
  };
}
function fetchListDataSuccess(data) {
  return {
    type: actionTypes.FETCH_GN_HOME_SUCCESS,
    payload: {
      newsData: data[0],
      gadgetsData: data[1],
    },
  };
}

function getAPIPath() {
  let apipath = `${process.env.API_BASEPOINT}/sc_articlelist/${siteConfig.pages.gadgethome}.cms?feedtype=sjson&version=v9`;
  return apipath;
}

function fetchListData() {
  const apiUrlNews = getAPIPath();
  console.log("apiUrlNews", apiUrlNews);
  // const apiUrlMeta = `${process.env.API_BASEPOINT}/pwa_metalist/${siteConfig.pages.gadgethome}.cms?feedtype=sjson&version=v9`;

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GN_HOME_REQUEST,
    });
    const PromiseNews = fetch(apiUrlNews).catch(() => null);
    const PromiseGadgetsData = fetch(
      `${process.env.API_BASEPOINT}/sc_gadgetlist.cms?category=mobile&feedtype=sjson&pagetype=widget&perpage=10&sort=latest`,
    ).catch(() => null);
    //   const PromiseMeta = fetch(apiUrlMeta).catch(() => null);

    const promiseArray = [PromiseNews, PromiseGadgetsData];

    return Promise.all(promiseArray)
      .then(
        data => dispatch(fetchListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function shouldFetchListData(state, params, query, location) {
  const { gadgetsNowHome } = state;
  if (!gadgetsNowHome || (gadgetsNowHome && !gadgetsNowHome.data)) {
    return true;
  }
  return false;
}

export function fetchListDataIfNeeded(params, query, router, pagetype) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), params, query, router && router.location)) {
      return dispatch(fetchListData(getState(), params, query, router, pagetype));
    }
    return Promise.resolve([]);
  };
}
