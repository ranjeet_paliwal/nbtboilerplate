import fetch from "../../../utils/fetch/fetch";

import gnConfig from "../../../utils/gadgetsConfig";

export const actionTypes = {
  FETCH_GADGETLIST_REQUEST: "FETCH_GADGETLIST_REQUEST",
  FETCH_GADGETLIST_SUCCESS: "FETCH_GADGETLIST_SUCCESS",
  FETCH_GADGETLIST_FAILURE: "FETCH_GADGETLIST_FAILURE",
  FETCH_NEXT_GADGETLIST_REQUEST: "FETCH_NEXT_GADGETLIST_REQUEST",
  FETCH_NEXT_GADGETLIST_SUCCESS: "FETCH_NEXT_GADGETLIST_SUCCESS",
  FETCH_NEXT_GADGETLIST_FAILURE: "FETCH_NEXT_GADGETLIST_FAILURE",
};

const staticRoutes = [
  "/tech/mobile-phones/i-smart",
  "/tech/mobile-phones/hi-tech",
  "/tech/mobile-phones/sony-ericsson",
  "/tech/mobile-phones/t-mobile",
];

function fetchGadgetListDataFailure(error) {
  return {
    type: actionTypes.FETCH_GADGETLIST_FAILURE,
    payload: error.message,
  };
}
function fetchGadgetListDataSuccess(data) {
  return {
    type: actionTypes.FETCH_GADGETLIST_SUCCESS,
    payload: data,
  };
}

function fetchGadgetListData(state, params, query, router, dataType, categoryoverride) {
  // debugger;
  // const pathname =
  //   router && router.location && router.location.pathname
  //     ? router.location.pathname.substr(router.location.pathname.indexOf('/tech') + 5)
  //     : '';

  const pathname = (router && router.location && router.location.pathname) || "";
  const perpage = params && params.msid ? "&perpage=5" : "";
  let category = "";
  let brandName = "";
  let filters = "";

  if (params && params.category) {
    category = params.category;
    category = category ? gnConfig.gadgetMapping[category] : "";
  }

  const ismapped = gnConfig.brands.filter(item => pathname && pathname.includes(item));

  if (pathname && ismapped.length > 0) {
    brandName = pathname.split("/").pop();
  } else if (params && params.brand) {
    brandName = params.brand;
  }

  if (params && params.filters) {
    filters = `&${params.filters}`;
  }

  let uriPath = (pathname + perpage).replace("=", "%253D");
  uriPath = uriPath.replace("&", "%26");

  if (uriPath.includes("/amp_gadgetslist.cms")) {
    uriPath = uriPath.replace("/amp_gadgetslist.cms", "");
  }

  let apiUrl = `${process.env.API_BASEPOINT}/pwagn_gadgetlist.cms?pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=${uriPath}`;
  if (brandName) {
    apiUrl += "&brandname=" + brandName;
  }

  apiUrl += filters;

  if (pathname.indexOf("upcoming") > -1) {
    apiUrl += "&upcoming=1";
  }

  //console.log("apiUrl", apiUrl);

  // apiUrl = `${process.env.API_BASEPOINT}/pwagn_gadgetlist.cms?&pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=${uriPath}`;

  // params && params.brand
  //   ? (apiUrl += params.brand ? `&brandname=${params.brand}` : "")
  //   : params && params.filters
  //   ? (apiUrl += params.filters ? `&${params.filters}` : "")
  //   : dataType
  //   ? (apiUrl += `&sort=${dataType}`)
  //   : (apiUrl += "");
  // pathname.indexOf("upcoming") > -1 ? (apiUrl += "&upcoming=1") : null;

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGETLIST_REQUEST,
      payload: params,
    });

    const gadgetsListData = fetch(apiUrl).catch(e => null);

    return Promise.all([gadgetsListData])
      .then(
        gadgetsData =>
          dispatch(
            fetchGadgetListDataSuccess({
              gadgetListData: gadgetsData[0],
              params: params,
            }),
          ),
        error => dispatch(fetchGadgetListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchGadgetListDataFailure(error));
      });
  };
}

function shouldFetchGadgetListData(state, params, query, curpg, router) {
  const paramsStr = JSON.stringify(params);

  let pathName = router && router.location && router.location.pathname;
  pathName = pathName && pathName.substr(1);
  if (pathName && pathName.includes("/filters/")) {
    pathName = pathName.split("/filters/")[0];
  }

  const urlParamsStr = state.gadgetlist && state.gadgetlist.urlParams ? JSON.stringify(state.gadgetlist.urlParams) : "";
  const stateCanonical =
    state.gadgetlist && state.gadgetlist.gadgetsData && state.gadgetlist.gadgetsData.pwa_meta.canonical;
  const prepareCanonical = `${process.env.WEBSITE_URL}${pathName}`;

  // stateCanonical != prepareCanonical check was introducsed for upcoming devices
  if (params && !params.category) {
    return false;
  }
  if (state && state.gadgetlist && !state.gadgetlist.gadgetsData) {
    return true;
  } else if (paramsStr !== urlParamsStr || stateCanonical != prepareCanonical) {
    return true;
  } else if (curpg > 1) {
    return true;
  }
  return false;
}

export function fetchGadgetListDataIfNeeded(params, query, router, dataType, categoryoverride) {
  // debugger;
  return (dispatch, getState) => {
    if (shouldFetchGadgetListData(getState(), params, query, "", router)) {
      return dispatch(fetchGadgetListData(getState(), params, query, router, dataType, categoryoverride));
    }
    return Promise.resolve([]);
  };
}

// perpetcual
function fetchNextGadgetListDataFailure(error) {
  return {
    type: actionTypes.FETCH_NEXT_GADGETLIST_FAILURE,
    payload: error.message,
  };
}
function fetchNextGadgetListDataSuccess(data, curpg) {
  return {
    type: actionTypes.FETCH_NEXT_GADGETLIST_SUCCESS,
    payload: data,
    curpg,
  };
}

function fetchNextGadgetListData(state, params, query, router, curpg, dataType) {
  // const pathname =
  //   router && router.location && router.location.pathname
  //     ? router.location.pathname.substr(router.location.pathname.indexOf('/tech') + 5)
  //     : '';
  const pathname = router && router.location && router.location.pathname ? router.location.pathname : "";
  const perpage = params && params.msid ? "&perpage=5" : "";
  let category = params && params.category;
  if (category) {
    category = gnConfig.gadgetMapping[category];
  }
  // const category = params && params.category ? _getfeedCategory(params.category) : "";
  // let category = 'mobile';
  let apiUrl = `${process.env.API_BASEPOINT}/pwagn_gadgetlist.cms?pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=${pathname}&curpg=${curpg}${perpage}`;
  params && params.brand
    ? (apiUrl += params.brand ? `&brandname=${params.brand}` : "")
    : params && params.filters
    ? (apiUrl += params.filters ? `&${params.filters}` : "")
    : dataType
    ? (apiUrl += `&sort=${dataType}`)
    : (apiUrl += "");
  pathname.indexOf("upcoming") > -1 ? (apiUrl += "&upcoming=1") : null;

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_NEXT_GADGETLIST_REQUEST,
      payload: params,
      curpg,
    });
    return fetch(apiUrl)
      .then(
        data => {
          dispatch(fetchNextGadgetListDataSuccess(data, curpg));
        },
        error => dispatch(fetchNextGadgetListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchNextGadgetListDataFailure(error));
      });
  };
}

export function fetchNextGadgetListDataIfNeeded(params, query, router, curpg, dataType) {
  return (dispatch, getState) => {
    if (shouldFetchGadgetListData(getState(), params, query, curpg)) {
      return dispatch(fetchNextGadgetListData(getState(), params, query, router, curpg, dataType));
    }
    return Promise.resolve([]);
  };
}
