/* eslint-disable implicit-arrow-linebreak */
import fetch from "../../../../utils/fetch/fetch";

export const actionTypes = {
  FETCH_GADGET_LIST_REQUEST: "FETCH_GADGET_LIST_REQUEST",
  FETCH_GADGET_LIST_SUCCESS: "FETCH_GADGET_LIST_SUCCESS",
  UPDATE_GADGET_LIST_SUCCESS: "UPDATE_GADGET_LIST_SUCCESS",
  FETCH_GN_VIDEO_REQUEST: "FETCH_GN_VIDEO_REQUEST",
  FETCH_GN_VIDEO_SUCCESS: "FETCH_GN_VIDEO_SUCCESS",
  FETCH_GN_VIDEO_FAILURE: "FETCH_GN_VIDEO_FAILURE",
  FETCH_GN_PHOTO_REQUEST: "FETCH_GN_PHOTO_REQUEST",
  FETCH_GN_PHOTO_SUCCESS: "FETCH_GN_PHOTO_SUCCESS",
  FETCH_GN_PHOTO_FAILURE: "FETCH_GN_PHOTO_FAILURE",
};

//const siteConfig = require(`../../../../common/${process.env.SITE}`);

import gadgetConfig from "../../../../utils/gadgetsConfig";
import { isMobilePlatform, _getStaticConfig } from "../../../../utils/util";

const siteConfig = _getStaticConfig();

function fetchDataFailure(error) {
  // console.log(error);
}

function updateGadgetFailure(error) {
  // console.log(error);
}

function fetchDataSuccess(listData) {
  // console.log("listData", listData);
  return {
    type: actionTypes.FETCH_GADGET_LIST_SUCCESS,
    payload: listData,
  };
}

function fetchData({ query, params }) {
  // Main article data (LHS)
  const { device } = params; // articleId is defined in routes.js path expression

  // const pageno = query && query.curpg ? parseInt(query.curpg) : 1;
  let gadgetCategory = (gadgetConfig && gadgetConfig.gadgetMapping[device]) || "";
  if (!gadgetCategory && query && query.category) {
    gadgetCategory = query.category;
  }
  // console.log('fetchData [COM Detail]', compareString, device);
  // get articlelist data
  const platform = !isMobilePlatform() ? "&platform=desktop" : "";
  const promisePopularNLatest = fetch(
    `${process.env.API_BASEPOINT}/pwafeeds/pwagn_comparisonlist.cms?pagetype=comparelist&category=${gadgetCategory}${platform}&feedtype=sjson`,
  );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGET_LIST_REQUEST,
    });

    return Promise.all([promisePopularNLatest].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            compareData: data[0],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function fetchVideoData() {
  let apiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?type=video&count=11&feedtype=sjson&id=${siteConfig.techsectionID.video}`;

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GN_VIDEO_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => {
          const listData = data;
          return dispatch(fetchVideoDataSuccess(listData));
        },
        error => dispatch(fetchVideoDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchVideoDataFailure(error));
      });
  };
}

function fetchPhotoData() {
  let apiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?type=photo&count=11&feedtype=sjson&id=${siteConfig.techsectionID.photo}`;
  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GN_PHOTO_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => {
          const listData = data;

          return dispatch(fetchPhotoDataSuccess(listData));
        },
        error => dispatch(fetchPhotoDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchPhotoDataFailure(error));
      });
  };
}

function fetchPhotoDataSuccess(data, label) {
  return {
    type: actionTypes.FETCH_GN_PHOTO_SUCCESS,
    payload: data,
  };
}

function fetchPhotoDataFailure(error, params) {
  return {
    type: actionTypes.FETCH_GN_PHOTO_FAILURE,
    payload: {
      // id: rlvideoid,
      error: error.message,
    },
  };
}

function fetchVideoDataSuccess(data, label) {
  return {
    type: actionTypes.FETCH_GN_VIDEO_SUCCESS,
    payload: data,
  };
}

function fetchVideoDataFailure(error, params) {
  return {
    type: actionTypes.FETCH_GN_VIDEO_FAILURE,
    payload: {
      // id: rlvideoid,
      error: error.message,
    },
  };
}

function updateGadgetSuccess(data) {
  return {
    type: actionTypes.UPDATE_GADGET_LIST_SUCCESS,
    payload: data,
  };
}

function updateGadget(deviceType) {
  // const { articleId, pgno } = params;
  // console.log('updateGadget', deviceType);

  return dispatch => {
    return fetch(
      `${process.env.API_BASEPOINT}/pwafeeds/pwagn_comparisonlist.cms?pagetype=comparelist&category=${deviceType}&platform=desktop&feedtype=sjson`,
    ).then(
      data => dispatch(updateGadgetSuccess(data)),
      error => dispatch(updateGadgetFailure(error)),
    );
  };
}

export function updateGadgetData(deviceType) {
  return dispatch => {
    return dispatch(updateGadget(deviceType));
  };
}

function shouldFetchData(state, params) {
  if (state && state.comparisionList && !state.comparisionList.data) {
    return true;
  }

  if (
    // state.comparisionList.data.listData.compareData.popularGadgetPair.compare[0].category
    state &&
    state.comparisionList &&
    state.comparisionList.data &&
    state.comparisionList.data.listData &&
    state.comparisionList.data.listData.compareData &&
    state.comparisionList.data.listData.compareData.popularGadgetPair &&
    state.comparisionList.data.listData.compareData.popularGadgetPair.compare &&
    state.comparisionList.data.listData.compareData.popularGadgetPair.compare[0]
  ) {
    const gadgetCategory = state.comparisionList.data.listData.compareData.popularGadgetPair.compare[0].category;

    const mappedCategory = gadgetConfig.gadgetCategories[gadgetCategory];
    if (mappedCategory && params && params.device && params.device.indexOf(mappedCategory) === -1) {
      return true;
    }
    return false;
  }

  return false;
}

function shouldfetchVideoData(state, params) {
  if (state && state.comparisionList && !state.comparisionList.videoData) {
    return true;
  }
  return false;
}

function shouldfetchPhotoData(state, params) {
  if (state && state.comparisionList && !state.comparisionList.photoData) {
    return true;
  }
  return false;
}

export function fetchDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}

export function fetchVideoDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldfetchVideoData(getState(), params)) {
      return dispatch(fetchVideoData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}

export function fetchPhotoDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldfetchPhotoData(getState(), params)) {
      return dispatch(fetchPhotoData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
