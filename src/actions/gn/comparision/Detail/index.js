/* eslint-disable implicit-arrow-linebreak */
import fetch from "../../../../utils/fetch/fetch";

export const actionTypes = {
  FETCH_GADGET_DETAIL_REQUEST: "FETCH_GADGET_DETAIL_REQUEST",
  FETCH_GADGET_DETAIL_SUCCESS: "FETCH_GADGET_DETAIL_SUCCESS",
  ADD_GADGET_DETAIL_SUCCESS: "ADD_GADGET_DETAIL_SUCCESS",
  REMOVE_GADGET_DETAIL_SUCCESS: "REMOVE_GADGET_DETAIL_SUCCESS",
};

import { _getStaticConfig, _isCSR, _getCategory } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";

const siteConfig = _getStaticConfig();

function fetchDataFailure() {
  // console.log(error);
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_GADGET_DETAIL_SUCCESS,
    payload: data,
  };
}

function fetchData({ params }) {
  // Main article data (LHS)
  const { qstring, device } = params; // articleId is defined in routes.js path expression
  const compareString = (qstring && qstring.replace(/-vs-/g, ",")) || "";

  const gadgetCategory = (gadgetsConfig && gadgetsConfig.gadgetMapping && gadgetsConfig.gadgetMapping[device]) || "";
  const feedPath = `${process.env.API_BASEPOINT}/pwagn_comparisonshow.cms?pagetype=compareshow&platform=desktop&productid=${compareString}&category=${gadgetCategory}&feedtype=sjson`;

  const promiseGNShow = fetch(feedPath);

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGET_DETAIL_REQUEST,
    });

    // dispatch(fetchHeaderData(articleId));

    return Promise.all([promiseGNShow].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            compareData: data[0],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const devices = [];
  if (state && state.compareDetail && !state.compareDetail.data) {
    return true;
  }

  if (
    state.compareDetail &&
    state.compareDetail.data &&
    state.compareDetail.data.compareData &&
    state.compareDetail.data.compareData.techgadget &&
    state.compareDetail.data.compareData.techgadget.gadget
  ) {
    const gadgetInfo = state.compareDetail.data.compareData.techgadget.gadget;
    gadgetInfo.forEach(item => {
      devices.push(item.productname);
    });

    const devicesStr = devices.join("-vs-");
    if (params.qstring.toLowerCase() !== devicesStr.toLowerCase()) {
      return true;
    }
  }

  return false; // ToDo: need to handle this
}

function removeGadgetDataFailure() {}

function removeGadgetDataSuccess(data) {
  return {
    type: actionTypes.REMOVE_GADGET_DETAIL_SUCCESS,
    payload: data,
  };
}

function addGadgetDataFailure() {}

function addGadgetDataSuccess(data) {
  return {
    type: actionTypes.ADD_GADGET_DETAIL_SUCCESS,
    payload: data,
  };
}

function removeGadgetData({ finalSearchQuery, gadgetCategory }) {
  return dispatch => {
    return fetch(
      `${process.env.API_BASEPOINT}/pwagn_comparisonshow.cms?pagetype=compareshow&productid=${finalSearchQuery}&category=${gadgetCategory}&feedtype=sjson`,
    ).then(
      data => dispatch(removeGadgetDataSuccess(data)),
      error => dispatch(removeGadgetDataFailure(error)),
    );
  };
}

export function addGadgetData({ finalSearchQuery, gadgetCategory }) {
  return dispatch => {
    return fetch(
      `${process.env.API_BASEPOINT}/pwagn_comparisonshow.cms?pagetype=compareshow&productid=${finalSearchQuery}&category=${gadgetCategory}&feedtype=sjson`,
    ).then(
      data => dispatch(addGadgetDataSuccess(data)),
      error => dispatch(addGadgetDataFailure(error)),
    );
  };
}

export function removeGadget(params) {
  return dispatch => {
    return dispatch(removeGadgetData(params));
  };
}

export function addGadget(params) {
  return dispatch => {
    return dispatch(addGadgetData(params));
  };
}

export function fetchDataIfNeeded({ query, params, history, location }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
