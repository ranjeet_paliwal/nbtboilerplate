/* eslint-disable implicit-arrow-linebreak */
import fetch from "../../../../utils/fetch/fetch";

export const actionTypes = {
  FETCH_GADGET_TRENDS_REQUEST: "FETCH_GADGET_TRENDS_REQUEST",
  FETCH_GADGET_TRENDS_SUCCESS: "FETCH_GADGET_TRENDS_SUCCESS",
  UPDATE_GADGET_TRENDS_SUCCESS: "UPDATE_GADGET_TRENDS_SUCCESS",
};

import gadgetsConfig from "../../../../utils/gadgetsConfig";

function fetchDataFailure(error) {
  // console.log(error);
}

function updateGadgetFailure(error) {
  // console.log(error);
}

function fetchDataSuccess(listData) {
  return {
    type: actionTypes.FETCH_GADGET_TRENDS_SUCCESS,
    payload: listData,
  };
}

function fetchData({ query, params, router }) {
  const { device, criteria } = params;
  // let criteria = router.params && router.params.criteria;
  // criteria = +"comparisons";
  //let device = "mobile";
  // const pageno = query && query.curpg ? parseInt(query.curpg) : 1;
  const gadgetCategory = gadgetsConfig.gadgetMapping[device] || "";

  const apiPath = `${process.env.API_BASEPOINT}/pwagn_comparetrends.cms?pagetype=${criteria}-comparisons&category=${gadgetCategory}&feedtype=sjson`;

  const promisePopularNLatest = fetch(apiPath);

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGET_TRENDS_REQUEST,
    });

    // dispatch(fetchHeaderData(articleId));

    return Promise.all([promisePopularNLatest].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            compareData: data[0],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params, router) {
  const { articleId } = params;

  if (state && state.trendsComparision && !state.trendsComparision.data) {
    return true;
  }

  if (
    state &&
    state.trendsComparision &&
    state.trendsComparision.data &&
    state.trendsComparision.data.compareData &&
    state.trendsComparision.data.compareData.compare &&
    state.trendsComparision.data.compareData.compare[0] &&
    state.trendsComparision.data.compareData.compare[0].category
    //&& state.trendsComparision.data.
    //need to check feed path name
  ) {
    const deviceInFeed = state.trendsComparision.data.compareData.compare[0].category;
    const deviceFromURL = params && params.device ? gadgetsConfig.gadgetMapping[params.device] : "";
    if (deviceInFeed !== deviceFromURL) {
      return true;
    }
    return false;
  }

  return true;
}

export function fetchDataIfNeeded({ query, params, history, router }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params, router)) {
      return dispatch(fetchData({ query, params, history, router }));
    }
    return Promise.resolve([]);
  };
}
