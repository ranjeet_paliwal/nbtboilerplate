import fetch from "utils/fetch/fetch";
import gadgetsConfig from "../../../utils/gadgetsConfig";
import { isMobilePlatform, _getStaticConfig } from "../../../utils/util";
const gadgetMapping = (gadgetsConfig && gadgetsConfig.gadgetMapping) || "";
const siteConfig = _getStaticConfig();

export const actionTypes = {
  FETCH_GADGETSHOW_REQUEST: "FETCH_GADGETSHOW_REQUEST",
  FETCH_GADGETSHOW_SUCCESS: "FETCH_GADGETSHOW_SUCCESS",
  FETCH_GADGETSHOW_FAILURE: "FETCH_GADGETSHOW_FAILURE",
  FETCH_RELATED_GADGETS_REQUEST: "FETCH_RELATED_GADGETS_REQUEST",
  FETCH_RELATED_GADGETS_SUCCESS: "FETCH_RELATED_GADGETS_SUCCESS",
  FETCH_RELATED_GADGETS_FAILURE: "FETCH_RELATED_GADGETS_FAILURE",
  UPDATE_NAV_SUCCESS: "UPDATE_NAV_SUCCESS",
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_GADGETSHOW_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_GADGETSHOW_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchData(params) {
  let productId = "";

  // console.log("params", params);

  if (params.splat && Array.isArray(params.splat)) {
    productId = params.splat.join("-");
  }

  const platform = !isMobilePlatform() ? "&platform=desktop" : "";
  let category = gadgetsConfig.gadgetMapping[params.category];
  const url = `${
    process.env.API_BASEPOINT
    }/pwagn_productshow.cms?pagetype=gadgetshow&productid=${productId}&category=${category}&uri=/tech/${
    gadgetMapping[params.category]
    }/${productId}${platform}&feedtype=sjson`;

  //  console.log("apiPath", url);

  const gadgetData = fetch(url).then(gData => {
    const { article } = gData;

    if (article) {
      const latestTrendingData = fetch(
        `${process.env.API_BASEPOINT}/web_combine.cms?tag=secdata&secid=${siteConfig.techsectionID.latestGadgets},${siteConfig.techsectionID.trendingGadgets}&count=5&feedtype=sjson`,
      );
      return Promise.all([latestTrendingData]).then(
        data => {
          return { gadgetData: gData, latestTrendingData: data[0] };
        },
        error => {
          return { gadgetData: gData, latestTrendingData: null };
        },
      );
    }

    return { gadgetData: gData, latestTrendingData: null };
  });

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGETSHOW_REQUEST,
    });

    return Promise.all([gadgetData]).then(
      data =>
        dispatch(
          fetchDataSuccess({
            gadgetData: data[0] && data[0].gadgetData,
            latestTrendingData: data[0] && data[0].latestTrendingData,
          }),
        ),
      e => dispatch(fetchDataFailure(e)),
    ).catch(error => {
      dispatch(fetchDataFailure(error));
    });
  };
}

function fetchRelatedGadgetsSuccess(data) {
  return {
    type: actionTypes.FETCH_RELATED_GADGETS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchRelatedGadgetsFailure(e) {
  return {
    type: actionTypes.FETCH_RELATED_GADGETS_FAILURE,
    payload: e.message,
  };
}

function shouldFetchData(state, params) {
  let productId = "";
  let stateProductId = "";
  if (params.splat && Array.isArray(params.splat)) {
    productId = params.splat.join("-");
  }

  if (
    state.gadgetshow &&
    state.gadgetshow.data &&
    state.gadgetshow.data.gadgetData &&
    Array.isArray(state.gadgetshow.data.gadgetData.gadget) &&
    state.gadgetshow.data.gadgetData.gadget[0] &&
    state.gadgetshow.data.gadgetData.gadget[0].uname
  ) {
    stateProductId = state.gadgetshow.data.gadgetData.gadget[0].uname;
  }

  return !stateProductId || stateProductId.toLowerCase() !== productId.toLowerCase();
}

export default function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData(params));
    }
    return Promise.resolve([]);
  };
}

function fetchRelatedGadgetArticle(params) {
  // Left section data for all related GADGETS

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_RELATED_GADGETS_REQUEST,
    });

    const apiPath = `${process.env.API_BASEPOINT}/pwagn_productshow.cms?pagetype=gadgetshow&productid=${
      params.id
      }&category=${gadgetMapping[params.category]}&uri=/${gadgetMapping[params.category]}/${
      params.id
      }&feedtype=sjson&platform=desktop`;

    const gadgetData = fetch(apiPath).then(gData => {
      const { article } = gData;

      if (article) {
        return Promise.all([]).then(
          data => {
            return { gadgetData: gData };
          },
          error => {
            return { gadgetData: gData };
          },
        );
      }
      return { gadgetData: gData };
    });

    return Promise.all([gadgetData]).then(
      data => dispatch(fetchRelatedGadgetsSuccess(data[0])),
      error => dispatch(fetchRelatedGadgetsFailure(error)),
    );
  };
}

export function fetchRelatedGadgetData(params) {
  return (dispatch, getState) => {
    return dispatch(fetchRelatedGadgetArticle(params));
  };
}
