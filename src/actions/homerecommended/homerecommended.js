import fetch from "utils/fetch/fetch";
import fetchJsonP from "fetch-jsonp";
import { _filterALJSON, personalizedFeedParser } from "../../utils/util";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

export const FETCH_RECOMMENDED_TOPLIST_REQUEST =
  "FETCH_RECOMMENDED_TOPLIST_REQUEST";
export const FETCH_RECOMMENDED_TOPLIST_SUCCESS =
  "FETCH_RECOMMENDED_TOPLIST_SUCCESS";
export const FETCH_RECOMMENDED_TOPLIST_FAILURE =
  "FETCH_RECOMMENDED_TOPLIST_FAILURE";
export const FETCH_NEXT_RECOMMENDED_TOPLIST_REQUEST =
  "FETCH_NEXT_RECOMMENDED_TOPLIST_REQUEST";
export const FETCH_NEXT_RECOMMENDED_TOPLIST_SUCCESS =
  "FETCH_NEXT_RECOMMENDED_TOPLIST_SUCCESS";

function fetchRecommendedTopListDataFailure(error) {
  return {
    type: FETCH_RECOMMENDED_TOPLIST_FAILURE,
    payload: error.message
  };
}

function fetchRecommendedTopListDataSuccess(data) {
  return {
    type: FETCH_RECOMMENDED_TOPLIST_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}
function fetchNextRecommendedTopListDataSuccess(data) {
  return {
    type: FETCH_NEXT_RECOMMENDED_TOPLIST_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchRecommendedTopListData(state, params, query, fpc, referrer) {
  let randomNumber = Math.ceil(Math.random() * 1000000000);
  let apiUrl =
    "https://recade.clmbtech.com/cfp/327491/feed.htm?rcp=1&_t=10&_v=0" +
    (fpc ? "&fpc=" + fpc : "") +
    (randomNumber ? "&r=" + randomNumber : "") +
    (referrer ? "&_u=" + referrer : "");
  //let apiUrl = "https://langdev.indiatimes.com/recommended_home.cms";
  return dispatch => {
    dispatch({
      type: FETCH_RECOMMENDED_TOPLIST_REQUEST
    });
    return fetchJsonP(apiUrl, {
      method: "GET",
      mode: "cors",
      credentials: "include",
      jsonpCallback: "_c",
      timeout: 20000
    })
      .then(response => {
        // fetch(apiUrl).then(response => {
        return response.json();
      })
      .then(data => {
        data = JSON.parse(data);
        console.log("Personalized Data");
        console.log(data);
        data = data.resArr;
        data = personalizedFeedParser(data);
        dispatch(fetchRecommendedTopListDataSuccess(data));
      })
      .catch(err => {
        console.log("INSIDE FETCH Personalization err", err);
        //When adblocker is on
        let apiUrl =
          "https://navbharattimes.indiatimes.com/pwafeeds/pwa_recommended_home.cms?rcp=1&_t=9&_v=0";
        return fetch(apiUrl)
          .then(
            data => {
              console.log("Personalized Data");
              console.log(data);
              data = data.resArr;
              data = personalizedFeedParser(data);
              dispatch(fetchRecommendedTopListDataSuccess(data));
            },
            error => dispatch(fetchRecommendedTopListDataFailure(error))
          )
          .catch(function(error) {
            dispatch(fetchRecommendedTopListDataFailure(error));
          });
      });
  };
}

function shouldFetchRecommendedTopListData(state, params, query) {
  if (state.homerecommended.value && state.homerecommended.value.length > 0) {
    return false;
  } else {
    return true;
  }
}

export function fetchRecommendedTopListDataIfNeeded(
  params,
  query,
  fpc,
  referrer
) {
  return (dispatch, getState) => {
    if (shouldFetchRecommendedTopListData(getState(), params, query)) {
      return dispatch(
        fetchRecommendedTopListData(getState(), params, query, fpc, referrer)
      );
    } else {
      return Promise.resolve([]);
    }
  };
}

function fetchNextRecommendedTopListData(state, params, query, fpc, referrer) {
  let randomNumber = Math.ceil(Math.random() * 1000000000);
  //let fpc = params && params.fpc ? params.fpc : undefined;
  //Pagination variable
  let pn = 1;
  let itemLength =
    state.homerecommended.value && state.homerecommended.value.length > 0
      ? state.homerecommended.value.length
      : null;
  //WIll show the max 80 stories
  //The default feed will give 20 stories and the below feed will give 10 stories each and hence the below rule
  if (state.homerecommended.pn && state.homerecommended.pn > 5) {
    return dispatch => {
      dispatch(fetchRecommendedTopListDataFailure("Will not fetch more"));
    };
  } else if (state.homerecommended.pn && state.homerecommended.pn <= 5) {
    pn = state.homerecommended.pn + 1;
  }
  let apiUrl =
    `https://recade.clmbtech.com/cfp/327491/feed.htm?rcp=0&_t=10&_v=0&strt=${pn}` +
    (fpc ? "&fpc=" + fpc : "") +
    (randomNumber ? "&r=" + randomNumber : "") +
    (referrer ? "&_u=" + referrer : "");
  return dispatch => {
    dispatch({
      type: FETCH_NEXT_RECOMMENDED_TOPLIST_REQUEST
    });
    return fetchJsonP(apiUrl, {
      method: "GET",
      mode: "cors",
      credentials: "include",
      jsonpCallback: "_c",
      timeout: 20000
    })
      .then(response => {
        // fetch(apiUrl).then(response => {
        return response.json();
      })
      .then(data => {
        data = JSON.parse(data);
        console.log("Personalized Data Pagination " + data.pn);
        console.log(data);
        pn = data.pn;
        data = data.resArr;
        data = personalizedFeedParser(data, itemLength ? itemLength : 0);
        data = { value: data, pn: pn };
        dispatch(fetchNextRecommendedTopListDataSuccess(data));
      })
      .catch(err => {
        console.log("INSIDE FETCH Personalization err", err);
        //When adblocker is on
        let apiUrl = `https://navbharattimes.indiatimes.com/pwafeeds/pwa_recommended_home.cms?rcp=0&_t=9&_v=0&strt=${pn}`;
        return fetch(apiUrl)
          .then(
            data => {
              console.log("Personalized Data Pagination " + data.pn);
              console.log(data);
              pn = data.pn;
              data = data.resArr;
              data = personalizedFeedParser(data, itemLength ? itemLength : 0);
              data = { value: data, pn: pn };
              return dispatch(fetchNextRecommendedTopListDataSuccess(data));
            },
            error => dispatch(fetchRecommendedTopListDataFailure(error))
          )
          .catch(function(error) {
            dispatch(fetchRecommendedTopListDataFailure(error));
          });
      });
  };
}

export function fetchNextRecommendedTopListDataIfNeeded(
  params,
  query,
  fpc,
  referrer
) {
  return (dispatch, getState) => {
    return dispatch(
      fetchNextRecommendedTopListData(getState(), params, query, fpc, referrer)
    );
  };
}
