export const UPDATE_USER_DATA = "UPDATE_USER_DATA";

export const updateUserData = (data, userAction) => {
  return {
    type: UPDATE_USER_DATA,
    payload: data,
    userAction,
  };
};

// export function setDateTime(value) {
//   return {
//     type: CONFIG_DATETIME,
//     payload: value,
//   };
// }
