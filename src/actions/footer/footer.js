import fetch from "utils/fetch/fetch";

export const FETCH_QUICK_LINKS_REQUEST = "FETCH_QUICK_LINKS_REQUEST";
export const FETCH_QUICK_LINKS_SUCCESS = "FETCH_QUICK_LINKS_SUCCESS";
export const FETCH_QUICK_LINKS_FAILURE = "FETCH_QUICK_LINKS_FAILURE";

function fetchQuickLinksFailure(error) {
  return {
    type: FETCH_QUICK_LINKS_FAILURE,
    payload: error.message,
  };
}

function fetchQuickLinksSuccess(data) {
  return {
    type: FETCH_QUICK_LINKS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchQuickLinksData(state, params, query) {
  if (typeof params === "undefined" || params.msid == "") {
    return fetchQuickLinksSuccess({ msid: "" });
  }

  const apiUrl = `${process.env.API_BASEPOINT}/sc_quicklinks/${params.msid}.cms?feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_QUICK_LINKS_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchQuickLinksSuccess({ msid: params.msid, quick_links: data })),
        error => dispatch(fetchQuickLinksFailure(error)),
      )
      .catch(error => {
        dispatch(fetchQuickLinksFailure(error));
      });
  };
}

function shouldFetchQuickLinksData(state, params) {
  if (state.footer && state.footer.msid != params.msid) {
    return true;
  }
  return false;

  // return true;
}

export function fetchQuickLinksDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchQuickLinksData(getState(), params, query)) {
      return dispatch(fetchQuickLinksData(getState(), params, query));
    }
  };
}
