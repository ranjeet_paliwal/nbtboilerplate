import fetch from "utils/fetch/fetch";
export const FETCH_STATICPAGE_SUCCESS = "FETCH_STATICPAGE_SUCCESS";
export const FETCH_STATICPAGE_FAILURE = "FETCH_STATICPAGE_FAILURE";

function fetchStaticPageDataFailure(error) {
  return {
    type: FETCH_STATICPAGE_FAILURE,
    payload: error.message
  };
}

function fetchStaticPageDataSuccess(data) {
  return {
    type: FETCH_STATICPAGE_SUCCESS,
    payload: data
  };
}

export function fetchStaticPageData({ dispatch, params, query }) {
  let apiUrl =
    process.env.WEBSITE_URL + `/staticpage/file?filename=${params.staticpage}`;

  return dispatch => {
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchStaticPageDataSuccess(data)),
        error => dispatch(fetchStaticPageDataFailure(error))
      )
      .catch(error => dispatch(fetchStaticPageDataFailure(error)));
  };
}
