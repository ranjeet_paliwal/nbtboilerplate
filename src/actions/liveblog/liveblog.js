import fetch from "utils/fetch/fetch";

export const FETCH_LIVEBLOG_REQUEST = "FETCH_LIVEBLOG_REQUEST";
export const FETCH_LIVEBLOG_SUCCESS = "FETCH_LIVEBLOG_SUCCESS";
export const FETCH_LIVEBLOG_FAILURE = "FETCH_LIVEBLOG_FAILURE";
import { FETCH_RHS_DATA_SUCCESS } from "../../actions/app/app";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

function fetchLIVEBLOG_DataFailure(error) {
  return {
    type: FETCH_LIVEBLOG_FAILURE,
    payload: error.message,
  };
}

function fetchLIVEBLOG_DataSuccess(data) {
  if (data && data[0] && data[0].pwa_meta && data[0].pwa_meta.cricketlb) {
    const lbcontent = data[0] && data[0].lbcontent;
    console.log("**LB data[0]**", data[0]);
    if (lbcontent && lbcontent[1]) {
      data[0].pwa_meta.title = `${lbcontent[1].Score || ""} , ${lbcontent[1].Overs || ""} : ${data[0].pwa_meta.title}`;
    }
  }

  return {
    type: FETCH_LIVEBLOG_SUCCESS,
    payload: data,
  };
}

//Get Active Live Blog MSID
function fetchLIVEBLOG_Data(state, params, query) {
  let rhsApiUrl = "";
  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    rhsApiUrl = `${process.env.API_BASEPOINT}/pwa_home_new.cms?feedtype=sjson`;
  } else {
    rhsApiUrl = `${siteConfig.langnetstorageBasePath}/${siteConfig.channelCode}headline.htm`;
  }
  let pagination = siteConfig.channelCode == "nbt" ? "-1" : "";
  let lbapiUrl = `https://langnetstorage.indiatimes.com/Score/liveblog_${params.msid}.htm`;
  // let lbapiUrl = process.env.API_BASEPOINT + `/sc_liveblog.cms?msid=${params.msid}&feedtype=sjson`;
  // let lbmeta = process.env.API_BASEPOINT + `/pwa_liveblog.cms?version=v9&feedtype=sjson&msid=${params.msid}`;
  //console.log("API", lbapiUrl);
  //console.log('API', lbmeta);
  return dispatch => {
    dispatch({
      type: FETCH_LIVEBLOG_REQUEST,
    });
    let promise1 = fetch(lbapiUrl);
    let promise2 = fetch(rhsApiUrl);
    return Promise.all([promise1, promise2])
      .then(
        data => {
          let match = data[0];
          if (!match) throw new NetworkError("Invalid JSON Response", response.status);
          //data[0] = { lbcontent };
          // data[0] = {"lbcontent":JSON.parse(match)};
          let advertorialData = (data && data[1] && data[1].items) || [];
          dispatch({ type: FETCH_RHS_DATA_SUCCESS, payload: advertorialData });
          return dispatch(fetchLIVEBLOG_DataSuccess(data));
        },
        error => dispatch(fetchLIVEBLOG_DataFailure(error)),
      )
      .catch(function(error) {
        dispatch(fetchLIVEBLOG_DataFailure(error));
      });
  };
}

function shouldFetchLIVEBLOG_Data(state, params, query) {
  return true;
}

export function fetchLIVEBLOG_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLIVEBLOG_Data(getState(), params, query)) {
      return dispatch(fetchLIVEBLOG_Data(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}

//Get Live Blog update counts
export function fetchLiveBlogCount(dispatch, params, query) {
  // let lbapiUrl = process.env.API_ENDPOINT+`/getliveblogcount?msid=${params.msid}`;
  let lbapiUrl = `https://langnetstorage.indiatimes.com/configspace/msid-${params.msid},callback-livejsoncnt,host-${siteConfig.livebloghostname}.htm`;

  return fetch(lbapiUrl, { type: "jsonp" })
    .then(
      data => {
        data = JSON.parse(data.replace("livejsoncnt(", "").slice(0, -1));
        if (!data) throw new NetworkError("Invalid JSON Response", response.status);
        return data;
      },
      error => dispatch(fetchLIVEBLOG_DataFailure(error)),
    )
    .catch(function(error) {
      dispatch(fetchLIVEBLOG_DataFailure(error));
    });
}
