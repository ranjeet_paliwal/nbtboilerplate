/* eslint-disable eqeqeq */
/* eslint-disable radix */
/* eslint-disable prefer-destructuring */
import fetch from "utils/fetch/fetch";
import { _getStaticConfig, _apiBasepointUpdate, _getfeedCategory } from "../../utils/util";
const siteConfig = _getStaticConfig();

export const FETCH_TOPICS_REQUEST = "FETCH_TOPICS_REQUEST";
export const FETCH_TOPICS_SUCCESS = "FETCH_TOPICS_SUCCESS";
export const FETCH_TOPICS_FAILURE = "FETCH_TOPICS_FAILURE";
export const FETCH_NEXT_TOPICS_REQUEST = "FETCH_NEXT_TOPICS_REQUEST";
export const FETCH_NEXT_TOPICS_SUCCESS = "FETCH_NEXT_TOPICS_SUCCESS";
export const FETCH_TOPICS_DATA = "FETCH_TOPICS_DATA";
export const FETCH_PROFILE_SUCCESS = "FETCH_PROFILE_SUCCESS";
export const FETCH_PROFILE_FAILURE = "FETCH_PROFILE_FAILURE";

// fetchListDataFailure
function fetchListDataFailure(error) {
  return {
    type: FETCH_TOPICS_FAILURE,
    payload: error.message,
  };
}

// fetchListDataSuccess
function fetchListDataSuccess(data, businessType) {
  return {
    type: FETCH_TOPICS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
    businessType,
  };
}

// fetchNextListDataSuccess
function fetchNextListDataSuccess(data) {
  return {
    type: FETCH_NEXT_TOPICS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchProfileDataFailure(error) {
  return {
    type: FETCH_PROFILE_FAILURE,
    payload: error.message,
  };
}

function fetchProfileDataSuccess(data, businessType) {
  return {
    type: FETCH_PROFILE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    businessType,
    payload: data,
  };
}

function resolvePromise(apiurl) {
  if (apiurl) {
    return fetch(encodeURI(apiurl))
      .then(response => Promise.resolve(response))
      .catch(() => Promise.resolve([]));
  }
  return Promise.resolve([]);
}

function getPathURL(path) {
  let matchedStr;
  let regex;
  let splatToCheck;
  let glossaryLevel;
  let cat;
  let splatArr = [];
  matchedStr = path.replace(new RegExp("/glossary$"), "");
  // matchedStr = path.split("/glossary")[0];
  if (matchedStr) {
    splatArr = matchedStr.split("/");
    splatToCheck = splatArr[splatArr.length - 1];
    glossaryLevel = splatToCheck.length === 1 && /[a-z0-9]/.test(splatToCheck) ? "L2" : "L1";
    cat = splatArr[0];
    if (glossaryLevel === "L1") {
      return `cat=${cat}`;
    }
    return `cat=${cat}&path=${path}`;
  }
}

export function fetchProfileData(params, query, router, meta) {
  let profileUrl = "";
  let businessType = "";
  const pathName = router && router.location && router.location.pathname ? router.location.pathname : undefined;
  const profileid = meta.profileid;
  const profile = meta.profile && meta.profile !== "" ? meta.profile : undefined;

  if (pathName && pathName.indexOf("/topics/") > -1 && profile) {
    businessType = profile.toLowerCase();
  } else if (pathName && pathName.indexOf("/cricketer") > -1) {
    businessType = "cricketer";
  } else if (pathName && pathName.indexOf("/politician") > -1) {
    businessType = "politician";
  }

  switch (businessType) {
    case "cricketer":
      profileUrl = `https://toicri.timesofindia.indiatimes.com/jsons/players/${profileid}_player.json`;
      break;
    case "politician":
      profileUrl = `https://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-candidateshow/2019,2014/${profileid}.htm`;
      break;
    default:
  }

  return dispatch =>
    fetch(profileUrl)
      .then(
        data => dispatch(fetchProfileDataSuccess(data, businessType)),
        error => dispatch(fetchProfileDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchProfileDataFailure(error));
      });
}

// fetchListData
function fetchListData(state, params, query, router, type) {
  let curpg;

  let msid;

  let searchkey;

  let searchtype;

  let amptype = "";
  let businessType;
  let path;
  let pathURL;
  let pagetype = "";
  let tagparams = "";
  let dynamicAttr = "";
  let apiUrl;

  let splatStr;

  let splatArr = [];

  const paramsObj = {};

  if (params && params.splat) {
    splatStr = params.splat;
    splatArr = splatStr.split(",");
    splatArr.map(item => {
      paramsObj[item.split("-")[0]] = item.split("-")[1];
    });
  }

  if (params) {
    msid = params.msid !== undefined ? params.msid : undefined;
    curpg = params.curpg !== undefined ? params.curpg : undefined;
    searchkey = params.searchkey !== undefined ? params.searchkey : undefined;
    // path = params.pathkey !== undefined ? params.pathkey : undefined;
  }
  if (query && query.name) {
    searchkey = query.name;
  }

  if (params.pathkey && !(query && query.path)) {
    path = router.location && router.location.pathname.substring(1);
  }

  if (query && query.name) {
    searchkey = query.name;
  }
  if (query && query.path) {
    path = `${query.path}/glossary`;
  }

  if (searchkey === "ampdefault") {
    searchkey = undefined;
    amptype = "ampdefault";
    path = path.replace(".cms", "");
  }

  if (path && searchkey == undefined) {
    pathURL = getPathURL(path);
  } else {
    if (typeof params !== "undefined" && typeof params.searchtype !== "undefined") {
      searchtype = params.searchtype;
      // console.log("isNaN : ",isNaN(searchtype));
      if (searchtype.indexOf("amp") >= 0) {
        amptype = params.searchtype;
        searchtype = searchtype.replace("amp", "");
      } else if (!isNaN(searchtype)) {
        curpg = searchtype;
        searchtype = "default";
      } else if (searchtype === "all") {
        searchtype = "default";
      }
    } else {
      searchtype = "default";
    }

    if (searchtype == "default") {
      pagetype = "topicsall";
    } else if (searchtype == "news") {
      pagetype = "topicsnews";
    } else if (searchtype == "photos") {
      pagetype = "topicsphotos";
    } else if (searchtype == "videos") {
      pagetype = "topicsvideos";
    }

    tagparams += process.env.PLATFORM == "desktop" ? "&platform=desktop" : "";

    if (type === "dynamicPage") {
      dynamicAttr = "&dynamicpage=1";
    }
  }

  searchkey = searchkey && searchkey.replace(/['"]+/g, "");

  return dispatch => {
    dispatch({
      type: FETCH_TOPICS_REQUEST,
      payload: msid,
    });
    if (pathURL) {
      if (pathURL.indexOf("&path") === -1) {
        apiUrl = siteConfig.glossaryLanding;
      } else {
        apiUrl = `${_apiBasepointUpdate(
          router,
          params.splat,
        )}/sc_glossarylist.cms?${pathURL}&amptype=${amptype}&feedtype=sjson`;
      }
    } else {
      apiUrl = `${_apiBasepointUpdate(
        router,
        params.splat,
      )}/sc_topicfeeds.cms?feedtype=sjson&q=${searchkey}&type=${searchtype}&pagetype=${pagetype}&amptype=${amptype}${
        curpg && curpg > 1 ? `&curpg=${curpg}` : ""
      }${dynamicAttr}${tagparams}&version=v9`;
    }

    return fetch(encodeURI(apiUrl))
      .then(
        data => dispatch(fetchListDataSuccess(data, businessType)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

// fetchNextListData
function fetchNextListData(state, params, query, router, type) {
  // console.log("===========fetchNextListData======");
  let curpg;
  let searchkey;
  let searchtype;
  let amptype;
  curpg = 1;

  if (params) {
    curpg = params.curpg !== undefined ? params.curpg : undefined;
    searchkey = params.searchkey !== undefined ? params.searchkey : undefined;
  }

  if (state.topics.value !== "undefined" && state.topics.value.validtopic) {
    searchkey = state.topics.value.validtopic;
  }

  let dynamicAttr = "";
  if (type === "dynamicPage") {
    dynamicAttr = "&dynamicpage=1";
  }

  amptype = "";
  if (typeof params !== "undefined" && typeof params.searchtype !== "undefined") {
    searchtype = params.searchtype;
    // console.log("isNaN : ",isNaN(searchtype));
    if (searchtype.indexOf("amp") >= 0) {
      amptype = params.searchtype;
      searchtype = searchtype.replace("amp", "");
    } else if (!isNaN(searchtype)) {
      curpg = searchtype;
      searchtype = "default";
    } else if (searchtype === "all") {
      searchtype = "default";
    }
  } else {
    searchtype = "default";
  }

  let pagetype = "";
  if (searchtype == "default") {
    pagetype = "topicsall";
  } else if (searchtype == "news") {
    pagetype = "topicsnews";
  } else if (searchtype == "photos") {
    pagetype = "topicsphotos";
  } else if (searchtype == "videos") {
    pagetype = "topicsvideos";
  }

  if (typeof state.topics.value.pg.cp !== "undefined") {
    curpg = parseInt(state.topics.value.pg.cp) + 1;
    if (state.topics.value.pg.tp < curpg) {
      return Promise.resolve([]);
    }
  }

  let tagparams = "";
  tagparams += process.env.PLATFORM == "desktop" ? "&platform=desktop" : "";
  const apiUrl = `${_apiBasepointUpdate(
    router,
    params.splat,
  )}/sc_topicfeeds.cms?feedtype=sjson&q=${searchkey}&type=${searchtype}&pagetype=${pagetype}&amptype=${amptype}&fetchnext=1${
    curpg && curpg > 1 ? `&curpg=${curpg}` : ""
  }${dynamicAttr}${tagparams}&version=v9`;

  return dispatch => {
    dispatch({
      type: FETCH_NEXT_TOPICS_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchNextListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

// shouldFetchListData
function shouldFetchListData(state, params) {
  // let hl = state.topics.value && state.topics.value.hl ? state.topics.value.hl.toLowerCase().replace(" ", "-") : "";

  let bool = false;
  // initial call for fetching data
  if (
    typeof state.topics.value === "undefined" ||
    (Array.isArray(state.topics.value) && state.topics.value.length === 0)
  ) {
    bool = true;
  }
  // in case search param changes like from narender-modi to virat-kohli
  else if (
    typeof state.topics.value !== "undefined" &&
    state.topics.value.validtopic &&
    state.topics.value.validtopic != params.searchkey
  ) {
    bool = true;
  }
  // in case user changes tab e.g. All to video or news to photo.
  else if (params.searchtype !== undefined && state.topics.value.dtype != params.searchtype) {
    bool = true;
  }
  return bool;
}

// fetchListDataIfNeeded
export function fetchListDataIfNeeded(params, query, router, type) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), params, query)) {
      return dispatch(fetchListData(getState(), params, query, router, type));
    }
    return Promise.resolve([]);
  };
}

// fetchNextListDataIfNeeded
export function fetchNextListDataIfNeeded(params, query, router) {
  return (dispatch, getState) => dispatch(fetchNextListData(getState(), params, query, router));
}

export function fetchTopicsData(topic, dispatch, router) {
  const apiUrl = `${_apiBasepointUpdate(router)}/topiclinks.cms?feedtype=sjson&q=${topic}`;
  return fetch(apiUrl).then(data => {
    dispatch({
      type: FETCH_TOPICS_DATA,
      payload: data,
      topic,
    });
    return Promise.resolve(data);
  });
}
