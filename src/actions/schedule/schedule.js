import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_SCHEDULE_REQUEST = "FETCH_SCHEDULE_REQUEST";
export const FETCH_SCHEDULE_SUCCESS = "FETCH_SCHEDULE_SUCCESS";
export const FETCH_SCHEDULE_FAILURE = "FETCH_SCHEDULE_FAILURE";

function fetchScheduleDataFailure(error) {
  return {
    type: FETCH_SCHEDULE_FAILURE,
    payload: error.message,
  };
}
function fetchScheduleDataSuccess(data, params, isResultPage) {
  //data[0]['pwa_meta']=data[1].pwa_meta;
  let seriesid = data[0] && data[0].pwa_meta && data[0].pwa_meta.cricketlb ? data[0].pwa_meta.cricketlb : null;
  return {
    type: FETCH_SCHEDULE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    isResultPage: isResultPage,
    seriesid: seriesid,
    msid: params.msid,
    pastMatches: params.pastMatches,
    payload: data,
  };
}

function fetchScheduleData(state, params, query, router) {
  // let msid;
  // if (isNaN(msid)) {
  //   msid = siteConfig.pages[msid];
  // }

  let isResultPage = false;
  let pathname = router && router.location && router.location.pathname;
  if (pathname && (pathname.indexOf("/sportsresult/") > -1 || pathname.indexOf("/amp_sportsresult/") > -1)) {
    isResultPage = true;
  }

  let apiUrl = siteConfig.cricket_SIAPI.schedule;
  let sectionApi = process.env.API_BASEPOINT + "/pwafeeds/pwa_metalist.cms?msid=" + params.msid + "&feedtype=sjson";
  return dispatch => {
    dispatch({
      type: FETCH_SCHEDULE_REQUEST,
      payload: params.msid,
    });
    let Promise1 = fetch(apiUrl);
    let Promise2 = fetch(sectionApi);

    return Promise.all([Promise2, Promise1])
      .then(
        data => dispatch(fetchScheduleDataSuccess(data, params, isResultPage)),
        error => dispatch(fetchScheduleDataFailure(error)),
      )
      .catch(function(error) {
        dispatch(fetchScheduleDataFailure(error));
      });
  };
}

function shouldFetchScheduleData(state, params, query) {
  //console.log("Inside shouldFetchScheduleData",state.schedule);
  ////return true;state.schedule.isFetching==false &&
  if (
    typeof state.schedule.value[0] == "undefined" ||
    (typeof state.schedule.value[0] != "undefined" && state.schedule.value[0].id != params.msid)
  ) {
    return true;
  } else {
    return false;
  }
}

export function fetchScheduleDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchScheduleData(getState(), params, query)) {
      return dispatch(fetchScheduleData(getState(), params, query, router));
    } else {
      return Promise.resolve([]);
    }
  };
}
