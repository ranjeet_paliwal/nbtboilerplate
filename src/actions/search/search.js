import fetch from "utils/fetch/fetch";
export const FETCH_SEARCH_REQUEST = "FETCH_SEARCH_REQUEST";
export const FETCH_SEARCH_SUCCESS = "FETCH_SEARCH_SUCCESS";
export const FETCH_SEARCH_FAILURE = "FETCH_SEARCH_FAILURE";
export const FETCH_NEXT_SEARCH_REQUEST = "FETCH_NEXT_SEARCH_REQUEST";
export const FETCH_NEXT_SEARCH_SUCCESS = "FETCH_NEXT_SEARCH_SUCCESS";
export const CLEAR_SEARCH_RESULT = "CLEAR_SEARCH_RESULT";

function fetchSearchFailure(error) {
  return {
    type: FETCH_SEARCH_FAILURE,
    payload: error.message
  };
}

function fetchSearchSuccess(data) {
  return {
    type: FETCH_SEARCH_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchNextSearchSuccess(data) {
  return {
    type: FETCH_NEXT_SEARCH_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchSearch(state, params, query) {
  let curpg = 1;
  if (typeof params != "undefined" && typeof params.curpg != "undefined") {
    curpg = params.curpg;
  }
  let term = "";

  if (
    typeof params != "undefined" &&
    params.hasOwnProperty("term") &&
    params.term != "" &&
    typeof params.term != "undefined"
  ) {
    term = params.term;
  }
  let apiUrl = `${process.env.API_ENDPOINT}/NPRSS/search/trending-keywords?lang=2`;
  if (term != "") {
    apiUrl = `${process.env.API_ENDPOINT}/listing?msid=1564454&curpg=${curpg}`;
  }

  return dispatch => {
    dispatch({
      type: FETCH_SEARCH_REQUEST
    });

    return fetch(apiUrl).then(
      data => dispatch(fetchSearchSuccess(data)),
      error => dispatch(fetchSearchFailure(error))
    );
  };
}

export function fetchSearchIfNeeded(params, query) {
  return (dispatch, getState) => {
    return dispatch(fetchSearch(getState(), params, query));
  };
}

export function fetchNextSearchIfNeeded(params, query) {
  return (dispatch, getState) => {
    return dispatch(fetchNextSearchListDataIfNeeded(getState(), params, query));
  };
}

export function fetchNextSearchListDataIfNeeded(state, params, query) {
  let curpg = 1;
  if (typeof state.search.value.pg.cp != "undefined") {
    curpg = parseInt(state.search.value.pg.cp) + 1;
    if (state.search.value.pg.tp <= curpg) {
      return dispatch => {
        dispatch(fetchSearchFailure("no data found"));
      };
    }
  }
  let term = "";
  if (
    typeof params != "undefined" &&
    params.hasOwnProperty("term") &&
    params.term != ""
  ) {
    term = params.term;
  }

  let apiUrl = `${process.env.API_ENDPOINT}/listing?msid=1564454&curpg=${curpg}`;
  //apiUrl = apiUrl+"lang=2";

  return dispatch => {
    dispatch({
      type: FETCH_NEXT_SEARCH_REQUEST
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchNextSearchSuccess(data)),
        error => dispatch(fetchSearchFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchSearchFailure(error));
      });
  };
}

export function clearSearchResult() {
  return {
    type: CLEAR_SEARCH_RESULT
  };
}
