/* eslint-disable radix */
import fetch from "utils/fetch/fetch";
import {
  _getStaticConfig,
  _apiBasepointUpdate,
  _getBranchfeedCategory,
  getPageType,
  _isDesktop,
  isMobilePlatform,
  isTechSite,
} from "../../utils/util";
import { triggerServiceRequest } from "../../utils/serviceUtil";
const siteConfig = _getStaticConfig();
export const FETCH_LIST_REQUEST = "FETCH_LIST_REQUEST";
export const FETCH_LIST_SUCCESS = "FETCH_LIST_SUCCESS";
export const FETCH_LIST_FAILURE = "FETCH_LIST_FAILURE";
export const FETCH_BRAND_REQUEST = "FETCH_BRAND_REQUEST";
export const FETCH_BRAND_SUCCESS = "FETCH_BRAND_SUCCESS";
export const FETCH_BRAND_FAILURE = "FETCH_BRAND_FAILURE";
export const FETCH_NEXT_LIST_REQUEST = "FETCH_NEXT_LIST_REQUEST";
export const FETCH_NEXT_LIST_SUCCESS = "FETCH_NEXT_LIST_SUCCESS";

function fetchListDataFailure(error) {
  return {
    type: FETCH_LIST_FAILURE,
    payload: error.message,
  };
}
function fetchListDataSuccess(data) {
  return {
    type: FETCH_LIST_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchNextListDataSuccess(data, feedSection) {
  return {
    type: FETCH_NEXT_LIST_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
    feedSection,
  };
}

function fetchListData(state, params, query, router, pagetype) {
  let curpg;
  let msid;

  const pathName = router && router.location && router.location.pathname;
  if (typeof query !== "undefined" && typeof query.curpg !== "undefined") {
    curpg = query.curpg;
  }
  if (typeof params !== "undefined") {
    if (typeof params.msid !== "undefined") msid = params.msid;
    if (typeof params.curpg !== "undefined") curpg = params.curpg;
  }
  if (isNaN(msid)) {
    msid = siteConfig.pages[msid];
  }

  // handle short url like /tech or /review to passs section msid conditionally
  // This path /tech/articlelist/ handles the SSR long url(/tech/articlelist/msid.cms) for tech home
  /**
   * TODO- Right now we have hard coded the msid in Route.js so there is no splats and msid
   * formed for params object. When msid will be dynamic we can remove the check for "/tech/articlelist/" & /tech/amp_articlelist/
   */

  let isTechlistPage = false;
  if (isTechSite()) {
    const techSectionId = siteConfig.pages.tech;
    if (
      router &&
      (router.location.pathname == "/tech" ||
        router.location.pathname == "/tech/" ||
        router.location.pathname == "/tech/default.cms" ||
        router.location.pathname == `/tech/articlelist/${techSectionId}.cms` ||
        router.location.pathname == `/tech/amp_articlelist/${techSectionId}.cms` ||
        // MT
        router.location.pathname == `/gadget-news/articlelist/${techSectionId}.cms` ||
        router.location.pathname == `/gadget-news/amp_articlelist/${techSectionId}.cms`)
    ) {
      // 60023487 VK
      // 19615041 NBT
      msid = siteConfig.pages.tech;
      isTechlistPage = true;
    } else if (
      // This path /tech/reviews.cms satisy the short url in case of CSR
      // In case of SSR params object made because of dynamic path for reviews(params consists of splat and msid)
      router &&
      router.location.pathname.indexOf("/tech/reviews.cms") > -1
    ) {
      msid = siteConfig.pages.techreview;
    }
  }
  if (router && router.location.pathname.indexOf("/iplt20.cms") > -1) {
    msid = siteConfig.pages.iplHomePage;
  }
  // todo - need to pass msid via router(code written in router check commented line), i think we have to update react router v4.0 above & then validate
  // append tag params for data comes via solr tag in tech section
  // Below code is commented, Now data is coming from simple feed not from solr tag
  // const indexcat = router && router.location.pathname.indexOf("reviews");
  // const tagparams = indexcat > -1 ? "&tag=solrreview" : "";
  let listtype = "";

  if (
    (isTechSite() &&
      router &&
      router.location &&
      (router.location.pathname.indexOf("/tech/reviews") > -1 || pathName.includes("/gadget-news/reviews"))) ||
    (router && router.location && router.location.pathname.indexOf("/amp_reviews") > -1)
  ) {
    listtype = "/sc_reviewlist";
  } else if (pagetype) {
    listtype = `/sc_${pagetype}`;
  } else if (router && router.location && router.location.pathname) {
    const templateType = getPageType(router.location.pathname);
    if (templateType == "others" && router.location.pathname == "/tech") {
      listtype = "/sc_articlelist";
    } else {
      listtype = `/sc_${templateType}`;
    }
  } else {
    listtype = "/sc_articlelist";
  }

  // tech home  page condition added
  let apiUrl = "";
  if (
    router &&
    router.location &&
    router.location.pathname &&
    (router.location.pathname.endsWith("/tech") || router.location.pathname == "/tech/default.cms") &&
    process.env.SITE === "nbt"
  ) {
    apiUrl = `https://langnetstorage.indiatimes.com/langappfeeds/datanbttech.htm`;
  } else if (
    router &&
    router.location &&
    router.location.pathname &&
    (router.location.pathname.endsWith("/tech") || router.location.pathname == "/tech/default.cms") &&
    process.env.SITE === "vk"
  ) {
    apiUrl = `https://langnetstorage.indiatimes.com/langappfeeds/datavktech.htm`;
  } else {
    apiUrl = `${_apiBasepointUpdate(router, params.splat) + listtype}/${msid}.cms?feedtype=sjson${
      curpg && curpg > 1 ? `&curpg=${curpg}` : ""
    }`;
  }

  return dispatch => {
    dispatch({
      type: FETCH_LIST_REQUEST,
      payload: msid,
    });
    const Promise1 = fetch(apiUrl).catch(() => null);
    const astroData = fetch(
      `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${siteConfig.astrowidgetId}&type=list&feedtype=sjson&count=4&ctnadtype=homedefault`,
    ).catch(() => null);
    const PromiseGadgetsData = fetch(
      `${process.env.API_BASEPOINT}/sc_gadgetlist.cms?category=mobile&feedtype=sjson&pagetype=widget&perpage=10&sort=latest`,
    ).catch(() => null);
    // let Promise2 = fetch(sectionApi);

    const promiseArray = [Promise1, astroData];
    if (isTechlistPage) {
      // params && params.pageType == "gnhome"
      promiseArray.push(PromiseGadgetsData);
    }
    return Promise.all(promiseArray)
      .then(
        data => dispatch(fetchListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function fetchNextListData(state, params, query, router, pagetype) {
  let curpg = 1;
  let msid = params.msid;
  if (state.articlelist.value[0].pg && typeof state.articlelist.value[0].pg.cp !== "undefined") {
    curpg = parseInt(state.articlelist.value[0].pg.cp) + 1;
    if (parseInt(state.articlelist.value[0].pg.tp) < curpg) {
      return Promise.resolve([]);
    }
  }
  // todo - need to pass msid via router(code written in router check commented line), i think we have to update react router v4.0 above & then validate
  // handle short url like /tech or /review to passs section msid conditionally
  if (router.location.pathname == "/tech") {
    msid = siteConfig.pages.tech;
  } else if (router && router.location.pathname.indexOf("/tech/reviews.cms") > -1) {
    msid = siteConfig.pages.techreview;
  }
  // append tag params for data comes via solr tag in tech section
  // Below code is commented, Now data is coming from simple feed not from solr tag
  // const indexcat = router.location.pathname.indexOf("/tech/reviews");
  // const tagparams = indexcat > -1 ? "&tag=solrreview" : "";

  let listtype = "";

  if (
    (isTechSite() && router && router.location && router.location.pathname.indexOf("/tech/reviews") > -1) ||
    (router && router.location && router.location.pathname && router.location.pathname.indexOf("/amp_reviews") > -1)
  ) {
    listtype = "/sc_reviewlist";
  } else if (pagetype) {
    listtype = `/sc_${pagetype}`;
  } else {
    listtype =
      router && router.location && router.location.pathname
        ? `/sc_${getPageType(router.location.pathname)}`
        : "/sc_articlelist";
  }
  const apiUrl = `${_apiBasepointUpdate(router, params.splat) + listtype}/${msid}.cms?feedtype=sjson&curpg=${curpg}`;

  return dispatch => {
    dispatch({
      type: FETCH_NEXT_LIST_REQUEST,
    });
    return triggerServiceRequest({
      url: apiUrl,
      method: "GET",
    })
      .then(
        data => dispatch(fetchNextListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function shouldFetchListData(state, params, query, location) {
  // console.log("Inside shouldFetchListData",state.articlelist);
  // //return true;state.articlelist.isFetching==false &&
  // console.log("listDatalistData", listData);
  let tn = location && getPageType(location.pathname);
  if (params && params.pageType == "gnhome") {
    tn = "articlelist";
  }

  const listData = state && state.articlelist && state.articlelist.value && state.articlelist.value[0];
  if (
    !listData ||
    (state.articlelist && state.articlelist.msid != params.msid) ||
    (tn && listData.tn && tn !== listData.tn)
  ) {
    return true;
  }
  /*  if (
    typeof state.articlelist.value[0] === "undefined" ||
    (typeof state.articlelist.value[0] !== "undefined" && state.articlelist.value[0].id != params.msid)
  ) {
    return true;
  } */
  return false;
}

export function fetchListDataIfNeeded(params, query, router, pagetype) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), params, query, router && router.location)) {
      return dispatch(fetchListData(getState(), params, query, router, pagetype));
    }
    return Promise.resolve([]);
  };
}

export function fetchNextListDataIfNeeded(params, query, router, pagetype) {
  return (dispatch, getState) => dispatch(fetchNextListData(getState(), params, query, router, pagetype));
}

// fetch if brand widget category changes
export function fetchBrandListDataIfNeeded(params, query, router, category) {
  return (dispatch, getState) => dispatch(fetchBrandListData(getState(), params, query, router, category));
}

function fetchBrandListData(state, params, query, router, category) {
  category = _getBranchfeedCategory(category);
  const apiUrl =
    params && params.splat
      ? `${_apiBasepointUpdate(router, params.splat)}/pwa_brandlist.cms?feedtype=sjson&category=${category}`
      : `${_apiBasepointUpdate(router)}/pwa_brandlist.cms?feedtype=sjson&category=${category}`;

  return dispatch => {
    dispatch({
      type: FETCH_BRAND_REQUEST,
      payload: category,
    });
    const Promise1 = triggerServiceRequest({
      url: apiUrl,
      method: "GET",
    });
    // let Promise2 = fetch(sectionApi);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchBrandDataSuccess(data)),
        error => dispatch(fetchBrandDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchBrandDataFailure(error));
      });
  };
}

function fetchBrandDataFailure(error) {
  return {
    type: FETCH_BRAND_FAILURE,
    payload: error.message,
  };
}

function fetchBrandDataSuccess(data) {
  return {
    type: FETCH_BRAND_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}
