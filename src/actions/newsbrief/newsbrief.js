import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_BRIEF_REQUEST = "FETCH_BRIEF_REQUEST";
export const FETCH_BRIEF_SUCCESS = "FETCH_BRIEF_SUCCESS";
export const FETCH_BRIEF_FAILURE = "FETCH_BRIEF_FAILURE";

function fetchListDataFailure(error) {
  return {
    type: FETCH_BRIEF_FAILURE,
    payload: error.message,
  };
}
function fetchListDataSuccess(data) {
  return {
    type: FETCH_BRIEF_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchListData(state, params, query) {
  let msid;
  if (typeof params != "undefined" && typeof params.artid != "undefined") {
    msid = params.artid;
  } else if (typeof params != "undefined" && typeof params.msid != "undefined") {
    msid = params.msid;
  }
  let apiUrl = `${process.env.API_BASEPOINT}/pwa_brief.cms?${
    msid ? `msid=${msid}&tag=pwabriefcat` : "tag=pwabrief"
  }&feedtype=sjson`;

  //console.log("BRIEF APIII", apiUrl);

  return dispatch => {
    dispatch({
      type: FETCH_BRIEF_REQUEST,
      payload: params.msid,
    });
    let Promise1 = fetch(apiUrl);
    //let Promise2 = fetch(sectionApi);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(function(error) {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function shouldFetchListData(state, params, query) {
  if (typeof state.newsbrief.value[0] == "undefined") {
    return true;
  }

  return false;
}

export function fetchListDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), params, query)) {
      return dispatch(fetchListData(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}
