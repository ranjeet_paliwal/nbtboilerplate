export const CONFIG_PAGETYPE = "CONFIG_PAGETYPE";
export const CONFIG_PARENTID = "CONFIG_PARENTID";
export const CONFIG_SEARCH_HEADER = "CONFIG_SEARCH_HEADER";
export const CONFIG_DATETIME = "CONFIG_DATETIME";
export const CONFIG_REGION = "CONFIG_REGION";
export const CONFIG_RESET_REGION = "CONFIG_RESET_REGION";

export function changeSearchHeader(value, term) {
  return {
    type: CONFIG_SEARCH_HEADER,
    payload: value,
    term: term,
  };
}

export function setPageType(value, siteName) {
  return {
    type: CONFIG_PAGETYPE,
    payload: value,
    siteName: siteName || "",
  };
}

export function setRegion(value) {
  return {
    type: CONFIG_REGION,
    region: value,
  };
}

export function resetRegion() {
  return {
    type: CONFIG_RESET_REGION,
    region: "",
  };
}

export function setParentId(parentId, subsec1, secId, hierarchylevel) {
  // const { parentid, subsec1 } = params;
  return {
    type: CONFIG_PARENTID,
    payload: { parentId, subsec1, secId, hierarchylevel },
  };
}

export function setDateTime(value) {
  return {
    type: CONFIG_DATETIME,
    payload: value,
  };
}
