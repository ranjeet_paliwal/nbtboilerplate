import fetch from "utils/fetch/fetch";

import { _getStaticConfig, _apiBasepointUpdate } from "../../utils/util";
export const FETCH_HEADER_REQUEST = "FETCH_HEADER_REQUEST";
export const FETCH_HEADER_SUCCESS = "FETCH_HEADER_SUCCESS";
export const FETCH_HEADER_FAILURE = "FETCH_HEADER_FAILURE";

export const FETCH_DESKTOP_HEADER_REQUEST = "FETCH_DESKTOP_HEADER_REQUEST";
export const FETCH_DESKTOP_HEADER_SUCCESS = "FETCH_DESKTOP_HEADER_SUCCESS";
export const FETCH_DESKTOP_HEADER_FAILURE = "FETCH_DESKTOP_HEADER_FAILURE";

export const FETCH_TECH_HEADER_REQUEST = "FETCH_TECH_HEADER_REQUEST";
export const FETCH_TECH_HEADER_SUCCESS = "FETCH_TECH_HEADER_SUCCESS";
export const FETCH_TECH_HEADER_FAILURE = "FETCH_TECH_HEADER_FAILURE";

export const FETCH_HOVER_DATA_REQUEST = "FETCH_HOVER_DATA_REQUEST";
export const FETCH_HOVER_DATA_SUCCESS = "FETCH_HOVER_DATA_SUCCESS";
export const FETCH_HOVER_DATA_FAILURE = "FETCH_HOVER_DATA_FAILURE";

export const FETCH_NAV_DATA_SUCCESS = "FETCH_NAV_DATA_SUCCESS";

const siteConfig = _getStaticConfig();

function fetchHeaderFailure(error) {
  return {
    type: FETCH_HEADER_FAILURE,
    payload: error.message,
  };
}

function fetchHeaderSuccess(data) {
  return {
    type: FETCH_HEADER_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchDesktopHeaderFailure(e) {
  return { type: FETCH_DESKTOP_HEADER_FAILURE, payload: error.message };
}

function fetchDesktopHeaderSuccess(data) {
  return {
    type: FETCH_DESKTOP_HEADER_SUCCESS,
    meta: { receivedAt: Date.now() },
    payload: data,
  };
}

export function fetchNavDataSuccess(data) {
  return {
    type: FETCH_NAV_DATA_SUCCESS,
    meta: { receivedAt: Date.now() },
    payload: data,
  };
}

// TODO: Remove this and combine with action below
export function fetchDesktopHeaderPromise(dispatch, sectionId) {
  const apiUrl = `/pwafeeds/web_nav.cms?${`type=${sectionId ? "navigation" : "navbar"}`}${
    sectionId ? `msid=${sectionId}` : ""
  }&feedtype=sjson`;

  dispatch({
    type: FETCH_DESKTOP_HEADER_REQUEST,
  });

  return fetch(apiUrl)
    .then(
      data => {
        dispatch(fetchDesktopHeaderSuccess(data));
        // This parameter will be used by components where filtering is needed when header is fetched
      },
      error => dispatch(fetchDesktopHeaderFailure(error)),
    )
    .catch(error => {
      dispatch(fetchDesktopHeaderFailure(error));
    });
}

// TODO Filter header data fetched here with filterHeaderData function based on parameter
function fetchHeaderData(state, params, query) {
  // let apiUrl = state.app.urls.navigation;
  // let apiUrl = process.env.API_BASEPOINT + siteConfig.navfeedurl;
  // const apiUrl = siteConfig.navfeedurl;
  let apiUrl = "";
  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    apiUrl = `${process.env.API_BASEPOINT}/wdt_navjson_v2.cms`;
    // remove below code post MT GN Live.
    // if (siteConfig.channelCode == "mt") {
    //   apiUrl = `https://langdev8245.indiatimes.com/wdt_navjson_v2.cms`;
    // }
    //apiUrl = siteConfig.navfeedurl;
  } else {
    apiUrl = siteConfig.navfeedurl;
  }
  return dispatch => {
    dispatch({
      type: FETCH_HEADER_REQUEST,
    });
    // Check for caching
    return fetch(apiUrl, {}, true)
      .then(
        data => {
          dispatch(fetchHeaderSuccess(data));
          // This parameter will be used by components where filtering is needed when header is fetched
        },
        error => dispatch(fetchHeaderFailure(error)),
      )
      .catch(error => {
        dispatch(fetchHeaderFailure(error));
      });
    // return fetchHeaderPromise(dispatch);
  };
}

function shouldFetchHeaderData(state, params, query) {
  // const { header } = state;
  // if (!header.alaskaData) {
  // Should return true since we are deleting alaskadata on ssr for page size reduction
  return true;
  // }
  // return false;
}

export function fetchHeaderDataIfNeeded(params, query, filter, sectionId) {
  return (dispatch, getState) => {
    if (shouldFetchHeaderData(getState(), params, query)) {
      return dispatch(fetchHeaderData(getState(), params, query, filter, sectionId));
    }
    return Promise.resolve([]);
  };
}

// tech search action, seperate funcationality i used
function fetchTechHeaderFailure(error) {
  return {
    type: FETCH_TECH_HEADER_FAILURE,
    payload: error.message,
  };
}

function fetchTechHeaderSuccess(data) {
  return {
    type: FETCH_TECH_HEADER_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchTechHeaderData(state, params, query, router, keyword) {
  // let apiUrl = state.app.urls.navigation;
  // console.log(siteConfig.navfeedurl)
  const apiUrl = `${_apiBasepointUpdate(router, params.splat)}/pwa_search.cms?q=${keyword}&keyword=${keyword}`;
  return dispatch => {
    dispatch({
      type: FETCH_TECH_HEADER_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchTechHeaderSuccess(data)),
        error => dispatch(fetchtechHeaderFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTechHeaderFailure(error));
      });
  };
}

function shouldFetchTechHeaderData(state, params, query) {
  return true;
}

export function fetchTechHeaderDataIfNeeded(params, query, router, keyword) {
  return (dispatch, getState) => {
    if (shouldFetchTechHeaderData(getState(), params, query)) {
      return dispatch(fetchTechHeaderData(getState(), params, query, router, keyword));
    }
    return Promise.resolve([]);
  };
}

function fetchHoverDataFailure(error, params) {
  return {
    type: FETCH_HOVER_DATA_FAILURE,
    payload: { error: error.message, msid: params.msid },
  };
}

function fetchHoverDataSuccess(data, params) {
  return {
    type: FETCH_HOVER_DATA_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: { msid: params.msid, data },
  };
}

function fetchHoverData(params) {
  const { msid, tn } = params;

  return dispatch => {
    dispatch({
      type: FETCH_HOVER_DATA_REQUEST,
      payload: { msid },
    });

    const apiNewsFeed = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${msid}&type=${tn}&feedtype=sjson&count=6`;

    // let newsFeedApiUrl =
    //   process.env.API_BASEPOINT +
    //   "/pwa_homelist.cms?msid=" +
    //   params._sec_id +
    //   "&type=" +
    //   params._type +
    //   "&feedtype=sjson" +
    //   "&count=15" +
    //   tagparams;

    return fetch(apiNewsFeed)
      .then(
        data => dispatch(fetchHoverDataSuccess(data, params)),
        // let listData = data;
        // listData._rlvideoid = params._rlvideoid;
        // if (process.env.PLATFORM == "desktop") {
        //   return dispatch(
        //     fetchWidgetDataSuccessDesktop(params.label, listData)
        //   );
        // } else {
        //   return dispatch(fetchWidgetDataSuccess(listData));
        // }
        error => dispatch(fetchHoverDataFailure(error, params)),
      )
      .catch(error => {
        dispatch(fetchHoverDataFailure(error, params));
      });
  };
}

function shouldFetchHoverData(state, params) {
  const { msid } = params;
  // FIXME: Add check if data already exists in reducer( based on msid)
  // First 5 stories should exist
  if (!state.header.navHoverData || !state.header.navHoverData[msid]) {
    return true;
  }
  return false;
}

export function fetchHoverDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchHoverData(getState(), params)) {
      return dispatch(fetchHoverData(params));
    }
    return Promise.resolve([]);
  };
}
