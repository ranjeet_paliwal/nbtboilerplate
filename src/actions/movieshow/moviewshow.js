import fetch from "utils/fetch/fetch";
import { _apiBasepointUpdate } from "../../utils/util";
export const FETCH_MOVIESHOW_SUCCESS = "FETCH_MOVIESHOW_SUCCESS";
export const FETCH_MOVIESHOW_FAILURE = "FETCH_MOVIESHOW_FAILURE";
export const FETCH_MOVIESHOW_REQUEST = "FETCH_MOVIESHOW_REQUEST";
export const FETCH_NEXT_MOVIESHOW_SUCCESS = "FETCH_NEXT_MOVIESHOW_SUCCESS";
export const RESET_ON_UNMOUNT = "RESET_ON_UNMOUNT";

function fetchMovieShowDataFailure(error) {
  return {
    type: FETCH_MOVIESHOW_FAILURE,
    payload: error && error.message,
  };
}

function fetchMovieShowDataSuccess(data) {
  return {
    type: FETCH_MOVIESHOW_SUCCESS,
    payload: data,
  };
}

function fetchNextMovieShowDataSuccess(data) {
  return {
    type: FETCH_NEXT_MOVIESHOW_SUCCESS,
    payload: data,
  };
}

function fetchMovieShowData(state, params, router) {
  const tagparams = "";
  const apiUrl = `${_apiBasepointUpdate(router, params.splat)}/sc_movieshow.cms?feedtype=sjson&version=v9&msid=${
    params.msid
  }${tagparams}`;
  return dispatch => {
    const Promise1 = fetch(apiUrl, { credentials: "same-origin" });
    return Promise.all([Promise1])
      .then(
        data => {
          const articleData = (data && data[0]) || [];
          if (typeof data === "object") {
            return dispatch(fetchMovieShowDataSuccess(articleData));
          }
          return dispatch(fetchMovieShowDataFailure(articleData));
        },
        error => dispatch(fetchMovieShowDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchMovieShowDataFailure(error));
      });
  };
}

function shouldFetchMovieShowData(state, params) {
  if (state.movieshow.items.length > 0 && state.movieshow.items[0] && state.movieshow.items[0].id == params.msid) {
    return false;
  }
  return true;
}

export function fetchMovieShowDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchMovieShowData(getState(), params)) {
      return dispatch(fetchMovieShowData(getState(), params, router));
    }
    return Promise.resolve([]);
  };
}

export function fetchNextMovieShowData(nextmsid, dispatch, params, query, router) {
  const tagparams = "";
  const apiUrl = `${_apiBasepointUpdate(
    router,
    params.splat,
  )}/sc_movieshow.cms?feedtype=sjson&version=v9&msid=${nextmsid}${tagparams}`;

  dispatch({
    type: FETCH_MOVIESHOW_REQUEST,
  });

  return fetch(apiUrl)
    .then(
      data => dispatch(fetchNextMovieShowDataSuccess(data)),
      error => dispatch(fetchMovieShowDataFailure(error)),
    )
    .catch(error => {
      dispatch(fetchMovieShowDataFailure(error));
    });
}

export function resetOnUnmount(dispatch) {
  dispatch({
    type: RESET_ON_UNMOUNT,
  });
}
