import makeRequest from "../../components/common/TimesPoints/makeRequest";
import Axios from "axios";

// import { getTpConfig } from "../../components/common/TimesPoints/TimesPointConfig";

import { isProdEnv, _getStaticConfig } from "../../utils/util";
export const FETCH_LOGIN_POINTS_SUCCESS = "FETCH_LOGIN_POINTS_SUCCESS";

export const HEADER_CROWN_REWARD_POINTS = "HEADER_CROWN_REWARD_POINTS";

export const POINTS_RECEIVED_ERROR = "POINTS_RECEIVED_ERROR";

export const INITIALIZE_TP_FLAG = "INITIALIZE_TP_FLAG";

export const REQUEST_CHANNEL_INFO = "REQUEST_CHANNEL_INFO";

/*  Actions fetched from our feeds */

export const RECEIVE_ALL_ACTIVITIES = "RECEIVE_ALL_ACTIVITIES";
export const FETCH_ALL_ACTIVITIES_REQUEST = "FETCH_ALL_ACTIVITIES_REQUEST";

export const RECEIVE_FAQ = "RECEIVE_FAQ";

/*  Actions fetched from our feeds */

const siteConfig = _getStaticConfig();

const tpConfig = siteConfig.TimesPoint;

const ACTIVITY_MAXCAP_MAP = "allActivityConfig";

const DEV_ENV = process.env.DEV_ENV;
const siteDomain = process.env.WEBSITE_URL;

export function receiveHeaderRewardPoints(rewardPoints) {
  return {
    type: HEADER_CROWN_REWARD_POINTS,
    payload: rewardPoints,
  };
}

function fetchTimesPointDataFailure(error) {
  console.log("error");
}
function fetchTimesPointDataSuccess(data) {
  return {
    type: FETCH_LOGIN_POINTS_SUCCESS,
    payload: data,
  };
}

export function setInititializeTPFlag() {
  return {
    type: INITIALIZE_TP_FLAG,
    payload: "",
  };
}

function shouldFetchListData(state, params, query) {
  return true;
}

// BEING USED
function fetchTimesPointData(ssoid = "") {
  const apiUrl = `${tpConfig.TP_DOMAIN[DEV_ENV]}/nextEngagement?pfm=msite&pname=${
    isProdEnv() ? tpConfig.pcode : tpConfig.stg.pcode
  }&widget=true&uid=${ssoid}`;

  return dispatch => {
    return makeRequest
      .get(apiUrl)
      .then(
        data => dispatch(fetchTimesPointDataSuccess(data)),
        error => dispatch(fetchTimesPointDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTimesPointDataFailure(error));
      });
  };
}

export function loadLoginTimesPoints(ssoId) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), ssoId)) {
      return dispatch(fetchTimesPointData(ssoId));
    } else {
      return Promise.resolve([]);
    }
  };
}

/*FIXME: Fork and change according to need. */
// function fetchAllActivitiesData(isWapView) {
//   const templateName = "/tpallactivities.cms";
//   const pfmQueryParam = `pfm=${isWapView ? "msite" : "web"}`;
//   const url = `${siteDomain}${templateName}?${pfmQueryParam}`;
//   return makeRequest.get(url);
// }

function fetchActivitiesFailure(error) {
  //console.log("fetchAllActivitiesError", error);
  return {
    type: RECEIVE_ALL_ACTIVITIES,
    payload: {},
  };
}

function setActivityDataInLocalStore(data) {
  let counter = 0;
  const activityMap = {};
  if (data && data.length > 0 && typeof window.localStorage !== "undefined") {
    for (; counter < data.length; counter += 1) {
      activityMap[data[counter].code] = data[counter].maxcap;
    }

    try {
      window.localStorage.setItem(ACTIVITY_MAXCAP_MAP, JSON.stringify(activityMap));
    } catch (e) {
      // console.log(e);
    }
  }
}

function fetchActivitiesSuccess(data) {
  let allActivity = [];
  let activitiesMapList = {};
  let translations = "";
  if (data.activitiesList && data.activitiesList.data && data.activitiesList.data.activities) {
    allActivity = data.activitiesList.data.activities;
    activitiesMapList = data.activitiesMap;
    translations = data.translations;
  }

  setActivityDataInLocalStore(allActivity);
  return {
    type: RECEIVE_ALL_ACTIVITIES,
    payload: {
      allActivity,
      activitiesMapList,
      translations,
    },
  };
}

function shouldFetchActivitiesData(state) {
  // console.log("stateee", state);
  if (state && state.allActivity) {
    return false;
  }
  return true;
}

function fetchAllActivitiesData(isWapView) {
  return dispatch => {
    dispatch({
      type: FETCH_ALL_ACTIVITIES_REQUEST,
    });
    const templateName = `/tpallactivities${isProdEnv() ? "" : "_stg"}.cms`;
    const pfmQueryParam = `?pfm=${isWapView ? "msite" : "web"}`;
    // const url = `${
    //   isProdEnv() ? siteDomain : "https://langdev.indiatimes.com/"
    // }pwafeeds${templateName}${pfmQueryParam}`;

    const url = `${process.env.API_BASEPOINT}${templateName}${pfmQueryParam}`;
    return makeRequest.get(url).then(
      response => dispatch(fetchActivitiesSuccess(response.data)),
      error => dispatch(fetchActivitiesFailure(error)),
    );
  };
}

export function getAllActivities(isWapView) {
  return (dispatch, getState) => {
    if (shouldFetchActivitiesData(getState())) {
      return dispatch(fetchAllActivitiesData(isWapView));
    }
  };
}

// export function getAllActivities(isWapView) {
//   return (dispatch, getState) => {
//     if (shouldFetchActivitiesData(getState())) {
//       return fetchAllActivitiesData(isWapView)
//         .then(data => dispatch(receiveAllActivitiesData(data.data)))
//         .catch(error => dispatch(fetchAllActivitiesError(error)));
//     }
//   };
// }

// function shouldFetchQuote(state) {
//   const quote = state.quote;
//   return !(quote.value || quote.isFetching);
// }

// export function fetchQuoteIfNeeded() {
//   return (dispatch, getState) => {
//     if (shouldFetchQuote(getState())) {
//       return dispatch(fetchQuote());
//     }
//   };
// }
/* FAQ data actions start here */

function fetchFAQData() {
  // Site domain already has slash inside.
  const url = `${siteDomain}tp_faq.cms?msid=${tpConfig.TP_FAQ_MSID}`;
  const options = {
    headers: {
      Accept: "text/html",
    },
  };
  return Axios.get(url, options);
}

function receiveFAQData(data) {
  return {
    type: RECEIVE_FAQ,
    payload: (data && data.data) || "",
  };
}

function fetchFAQDataError(e) {
  return {
    type: RECEIVE_FAQ,
    payload: "",
  };
}

export function getFAQData() {
  return dispatch =>
    fetchFAQData()
      .then(data => dispatch(receiveFAQData(data)))
      .catch(error => dispatch(fetchFAQDataError(error)));
}

/* FAQ data actions end here */
