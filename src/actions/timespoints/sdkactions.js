import { isMobilePlatform, isProdEnv, _getStaticConfig } from "../../utils/util";
// import { getTpConfig } from "../../components/common/TimesPoints/TimesPointConfig";

const siteConfig = _getStaticConfig();

const tpConfig = siteConfig.TimesPoint;

const platform = isMobilePlatform(true);
// const campaingHistoryName = tpConfig.campaingHistoryName[platform];
let campaingHistoryName = "";
if (isProdEnv()) {
  campaingHistoryName = tpConfig.campaingHistoryName[platform];
} else if (tpConfig.stg && tpConfig.stg.campaingHistoryName) {
  campaingHistoryName = tpConfig.stg.campaingHistoryName[platform];
}

// console.log("campaingHistoryName", campaingHistoryName);

// TODO: RECIEVE CHANGED TO 3 DIFFERENT ACTIONS - DISCUSS WITH RANJEET
// export const FETCH_USER_REWARDS_REQUEST = "FETCH_USER_REWARDS_REQUEST";
// export const FETCH_USER_REWARDS_SUCCESS = "FETCH_USER_REWARDS_SUCCESS";
// export const FETCH_USER_REWARDS_FAILURE = "FETCH_USER_REWARDS_FAILURE";

export const RECEIVE_USER_REWARDS = "RECEIVE_USER_REWARDS";
export const RECEIVE_USER_PROFILE = "RECEIVE_USER_PROFILE";
export const RECEIVE_DAILY_CHECKIN_DETAILS = "RECEIVE_DAILY_CHECKIN_DETAILS";
export const RECEIVE_DAILY_CHECKIN_ERROR = "RECEIVE_DAILY_CHECKIN_ERROR";
export const RECEIVE_DAILY_ACTIVITY = "RECEIVE_DAILY_ACTIVITY";

function sanitizeResult(data) {
  let newData = data;
  if (typeof data === "string") {
    newData = JSON.parse(data);
  }

  return newData;
}

function receiveUserRewardsError() {
  return {
    type: RECEIVE_USER_REWARDS,
    payload: [],
  };
}

function fetchUserProfileError() {
  return {
    type: RECEIVE_USER_PROFILE,
    payload: "",
  };
}

function receiveDailyCheckInError() {
  return {
    type: RECEIVE_DAILY_CHECKIN_DETAILS,
    payload: [],
  };
}

function receiveDailyActivityError() {
  return {
    type: RECEIVE_DAILY_ACTIVITY,
    payload: {},
  };
}

function receiveUserRewards(val) {
  const data = sanitizeResult(val);
  let awayUserRewards = [];
  let allUserRewards = [];
  let topUserRewards = [];
  let exclusiveUserRewards = [];
  if (data && data.response && data.response.offers && data.responseCode === "TP_100") {
    awayUserRewards = data.response.offers.away || [];
    topUserRewards = data.response.offers.top || [];
    allUserRewards = data.response.offers.all || [];
    exclusiveUserRewards = data.response.offers.exclusive || [];
  }
  return {
    type: RECEIVE_USER_REWARDS,
    payload: { awayUserRewards, topUserRewards, allUserRewards, exclusiveUserRewards },
  };
}

function receiveUserProfile(val) {
  const data = sanitizeResult(val);
  let userProfile = {};
  if (data && data.response && data.responseCode === "TP_100") {
    userProfile = data.response;
  }
  return {
    type: RECEIVE_USER_PROFILE,
    payload: userProfile,
  };
}

function receiveDailyCheckInDetails(val) {
  const data = sanitizeResult(val);
  let dailyCheckin = [];
  if (
    data &&
    data.response &&
    data.responseCode === "TP_100" &&
    data.response.items &&
    data.response.items.length > 0
  ) {
    dailyCheckin = data.response.items;
  }
  return {
    type: RECEIVE_DAILY_CHECKIN_DETAILS,
    payload: dailyCheckin,
  };
}

function receiveDailyActivityDetails(val) {
  const data = sanitizeResult(val);
  let dailyActivity = {};
  if (data && data.response && data.responseCode === "TP_100" && data.response.activities) {
    dailyActivity = data.response.activities;
  }
  return {
    type: RECEIVE_DAILY_ACTIVITY,
    payload: dailyActivity,
  };
}

function fetchUserProfile(dispatch) {
  if (typeof window.tpsdk === "function") {
    window.tpsdk(
      "fetch",
      "userPoints",
      "",
      data => dispatch(receiveUserProfile(data)),
      error => dispatch(fetchUserProfileError(error)),
    );
  }
}

function fetchUserRewards(dispatch) {
  if (typeof window.tpsdk === "function") {
    window.tpsdk(
      "fetch",
      "offerDetails",
      "",
      data => dispatch(receiveUserRewards(data)),
      error => dispatch(receiveUserRewardsError(error)),
    );
  }
}

function getDailyCheckInDetail(dispatch) {
  const curDate = new Date().getTime();
  const period = 86400 * 1000 * 7;
  const sDate = curDate - period;
  const eDate = curDate + period;

  // FIXME: Remove this tp re initialization if not needed
  // if (typeof tpsdk !== "function") {
  //   fn.loadTpJS();
  //   fn.changeUserStatusHandler();
  // }

  // FIXME:  Correct these things picked from config

  if (typeof window.tpsdk === "function") {
    window.tpsdk(
      "fetch",
      "campaignHistory",
      {
        sdate: sDate,
        edate: eDate,
        cname: campaingHistoryName,
      },
      data => dispatch(receiveDailyCheckInDetails(data)),
      error => dispatch(receiveDailyCheckInError(error)),
    );
  }
}

function fetchDailyActivity(dispatch) {
  if (typeof window.tpsdk === "function") {
    window.tpsdk(
      "fetch",
      "dailyReport",
      "",
      data => dispatch(receiveDailyActivityDetails(data)),
      error => dispatch(receiveDailyActivityError(error)),
    );
  }
}

export function loadDailyCheckInDetails() {
  return dispatch => getDailyCheckInDetail(dispatch);
}

export function loadUserRewards() {
  return dispatch => fetchUserRewards(dispatch);
}

export function loadDailyActivity() {
  return dispatch => fetchDailyActivity(dispatch);
}

export function getUserProfile() {
  return dispatch => fetchUserProfile(dispatch);
}
