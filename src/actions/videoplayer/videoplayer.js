import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";
export const ADD_VIDEO = "ADD_VIDEO";
export const REMOVE_VIDEO = "REMOVE_VIDEO";
export const SHOW_VIDEOPOPUP = "SHOW_VIDEOPOPUP";
export const HIDE_VIDEOPOPUP = "HIDE_VIDEOPOPUP";

// storing config data of a player in reducer corresponding to its slike id
export const ADD_VIDEO_CONFIG = "ADD_VIDEO_CONFIG";
export const REMOVE_VIDEO_CONFIG = "REMOVE_VIDEO_CONFIG";
export const ADD_SLIDER_DATA = "ADD_SLIDER_DATA";
export const REMOVE_SLIDER_DATA = "REMOVE_SLIDER_DATA";
export const SET_VIDEO_HEADER = "SET_VIDEO_HEADER";
export const ADD_PREV_URL_POPUP = "ADD_PREV_URL_POPUP";
export const SET_VIDEO_MSID = "SET_VIDEO_MSID";
export const SET_VIDEO_TYPE = "SET_VIDEO_TYPE";
export const SET_MINITV_PLAYING = "SET_MINITV_PLAYING";
export const SET_MINITV_VISIBILITY = "SET_MINITV_VISIBILITY";
export const SET_MINITV_WIDGET_CLICK = "SET_MINITV_WIDGET_CLICK";
export const UPDATE_VIDEO_CONFIG = "UPDATE_VIDEO_CONFIG";
export const UPDATE_AD_STATE = "UPDATE_AD_STATE";
const siteConfig = _getStaticConfig();

export function updateAdState(isAdPlaying) {
  return {
    type: UPDATE_AD_STATE,
    payload: isAdPlaying,
  };
}

export function updateVideoConfig(config) {
  return {
    type: UPDATE_VIDEO_CONFIG,
    payload: config,
  };
}

export function setMinitvWidgetClick(minitvClicked) {
  return {
    type: SET_MINITV_WIDGET_CLICK,
    payload: minitvClicked,
  };
}

export function setMinitvVisibility(showMinitv) {
  return {
    type: SET_MINITV_VISIBILITY,
    payload: showMinitv,
  };
}

export function setMinitvPlaying(isMinitvPlaying) {
  return {
    type: SET_MINITV_PLAYING,
    payload: isMinitvPlaying,
  };
}

export function setVideoMsid(msid) {
  return {
    type: SET_VIDEO_MSID,
    payload: msid,
  };
}

export function setVideoType(videoType) {
  return {
    type: SET_VIDEO_TYPE,
    payload: videoType,
  };
}

export function addPrevURLPopup(url) {
  return {
    type: ADD_PREV_URL_POPUP,
    payload: url,
  };
}

export function setVideoHeader(title) {
  return {
    type: SET_VIDEO_HEADER,
    payload: title,
  };
}

export function addSliderData(sliderData) {
  return {
    type: ADD_SLIDER_DATA,
    payload: sliderData,
  };
}

export function removeSliderData() {
  return {
    type: REMOVE_SLIDER_DATA,
  };
}

export function addVideoConfig(videoData) {
  return {
    type: ADD_VIDEO_CONFIG,
    payload: videoData,
  };
}

export function removeVideoConfig() {
  return {
    type: REMOVE_VIDEO_CONFIG,
  };
}

export function showVideoPopup() {
  // For cases where redux isnt accessible like slike event callbacks
  window.is_popup_video = true;
  return {
    type: SHOW_VIDEOPOPUP,
  };
}

export function hideVideoPopup() {
  // For cases where redux isnt accessible like slike event callbacks
  window.is_popup_video = false;
  return {
    type: HIDE_VIDEOPOPUP,
  };
}

export function addVideo(videoData) {
  return {
    type: ADD_VIDEO,
    payload: videoData,
  };
}

export function removeVideo() {
  return {
    type: REMOVE_VIDEO,
  };
}
 
export function fetchMiniTVMoreVideosData() {
  return dispatch => {
    const miniTvMoreVideoApi = `${process.env.API_BASEPOINT}/sc_videolist.cms?feedtype=sjson&version=v9&msid=${siteConfig.miniTvMoreVideos}`;
    return fetch(miniTvMoreVideoApi).then(
        data => {
        let miniTvMoreVideosData = data && data.items;
        if (miniTvMoreVideosData && miniTvMoreVideosData.length > 0){
          return dispatch(addSliderData(miniTvMoreVideosData));
        } else {
          return dispatch(removeSliderData());
        }
        },
        error => dispatch(removeSliderData()),
      )
      .catch(error => {
        dispatch(removeSliderData());
      });
  };
}
