import fetch from "utils/fetch/fetch";
import { getCountryCode, isInternationalUrl, _filterALJSON, isMobilePlatform } from "../../utils/util";

import { _getStaticConfig, _getCookie } from "../../utils/util";
import { internationalconfig } from "../../utils/internationalpageConfig";
const siteName = process.env.SITE;

// import { fetchLOKSABHAELECTIONRESULT_DataSuccess } from "../../campaign/election/loksabhaelectionresult/action";
// import { electionConfig } from "../../campaign/election/utils/config";

const siteConfig = _getStaticConfig();
// const _electionConfig = electionConfig[process.env.SITE];

export const FETCH_WIDGETSEQUENCE_REQUEST = "FETCH_WIDGETSEQUENCE_REQUEST";
export const FETCH_WIDGETSEQUENCE_SUCCESS = "FETCH_WIDGETSEQUENCE_SUCCESS";
export const FETCH_WIDGETSEQUENCE_FAILURE = "FETCH_WIDGETSEQUENCE_FAILURE";
export const FETCH_TOPLIST_REQUEST = "FETCH_TOPLIST_REQUEST";
export const FETCH_TOPLIST_SUCCESS = "FETCH_TOPLIST_SUCCESS";
export const FETCH_TOPLIST_FAILURE = "FETCH_TOPLIST_FAILURE";
// export const FETCH_ARTICLELIST_SUCCESS = 'FETCH_ARTICLELIST_SUCCESS';
// export const FETCH_ARTICLELIST_FAILURE = 'FETCH_ARTICLELIST_FAILURE';
export const FETCH_WIDGETDATA_SUCCESS_DESKTOP = "FETCH_WIDGETDATA_SUCCESS_DESKTOP";
export const FETCH_WIDGETDATA_SUCCESS = "FETCH_WIDGETDATA_SUCCESS";
export const FETCH_WIDGETDATA_FAILURE = "FETCH_WIDGETDATA_FAILURE";
export const FETCH_RELATEDVIDEO_REQUEST = "FETCH_RELATEDVIDEO_REQUEST";
export const FETCH_RELATEDVIDEO_SUCCESS = "FETCH_RELATEDVIDEO_SUCCESS";
export const FETCH_RELATEDVIDEO_FAILURE = "FETCH_RELATEDVIDEO_FAILURE";
export const FETCH_POLL_REQUEST = "FETCH_POLL_REQUEST";
export const FETCH_POLL_SUCCESS = "FETCH_POLL_SUCCESS";
export const FETCH_POLL_FAILURE = "FETCH_POLL_FAILURE";
export const SET_REFRESH_HOMEFEED = "SET_REFRESH_HOMEFEED";
export const REMOVE_REFRESH_HOMEFEED = "REMOVE_REFRESH_HOMEFEED";
export const MAKE_DATA_CHANGED_FALSE = "MAKE_DATA_CHANGED_FALSE";

export function makeDataChangedFalse() {
  return {
    type: MAKE_DATA_CHANGED_FALSE,
  };
}

export function removeRefreshHomefeed() {
  return {
    type: REMOVE_REFRESH_HOMEFEED,
  };
}

export function setRefreshHomeFeed(data) {
  return {
    type: SET_REFRESH_HOMEFEED,
    payload: data,
  };
}

function fetchTopListDataFailure(error) {
  return {
    type: FETCH_TOPLIST_FAILURE,
    payload: error.message,
  };
}

function fetchTopListDataSuccess(data, isFromRefreshFeed) {
  return {
    type: FETCH_TOPLIST_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
    isFromRefreshFeed,
  };
}

export function topListDataPromise(dispatch, numItems = 10) {
  dispatch({ type: FETCH_TOPLIST_REQUEST });
  const apiUrl = `${process.env.API_BASEPOINT}
    /sc_home/
    ${siteConfig.siteid}
    .cms?type=headline&count=${numItems}&feedtype=sjson`;
  return fetch(apiUrl)
    .then(
      data => dispatch(fetchTopListDataSuccess(data)),
      error => dispatch(fetchTopListDataFailure(error)),
    )
    .catch(error => {
      dispatch(fetchTopListDataFailure(error));
    });
}

function fetchTopListData(state, params, query, router, isFromRefreshFeed) {
  // const apiUrl = `${process.env.API_BASEPOINT}/pwa_home_new.cms?feedtype=sjson`;
  const countryCode = getCountryCode();

  const isInternationalpage = isInternationalUrl(router);
  let apiUrl = "";
  let intAPI = `${process.env.API_BASEPOINT}/sc_home_international.cms?msid=${
    internationalconfig[siteName].urlMappingMsid[router.location.pathname]
  }&feedtype=sjson&adserving=on`;

  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    if (isInternationalpage) {
      apiUrl = intAPI;
    } else {
      apiUrl = `${process.env.API_BASEPOINT}/pwa_home_new.cms?feedtype=sjson`;
    }
  } else {
    if (isInternationalpage) {
      apiUrl = intAPI;
    } else {
      apiUrl = `${siteConfig.langnetstorageBasePath}/${siteConfig.channelCode}headline.htm`;
    }
  }
  return dispatch => {
    dispatch({
      type: FETCH_TOPLIST_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchTopListDataSuccess(data, isFromRefreshFeed)),
        error => dispatch(fetchTopListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTopListDataFailure(error));
      });
  };
}

function shouldFetchTopListData(state, params, query, isFromRefreshFeed, region) {
  const { home, config } = state;
  const homedata =
    home.value && Array.isArray(state.home.value) && state.home.value[0] && state.home.value[0].recommended;
  if (region || config.region || !homedata || isFromRefreshFeed) {
    return true;
  }

  return false;
}

export function fetchTopListDataIfNeeded(params, query, router, isFromRefreshFeed, region) {
  return (dispatch, getState) => {
    if (shouldFetchTopListData(getState(), params, query, isFromRefreshFeed, region)) {
      return dispatch(fetchTopListData(getState(), params, query, router, isFromRefreshFeed));
    }
    return Promise.resolve([]);
  };
}

function fetchRelatedVideoFailure(error, rlvideoid) {
  return {
    type: FETCH_RELATEDVIDEO_FAILURE,
    payload: {
      id: rlvideoid,
      error: error.message,
    },
  };
}

function fetchRelatedVideoDataSuccess(data, label) {
  return {
    type: FETCH_RELATEDVIDEO_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
    label,
  };
}

function fetchWidgetRelatedVideoData(state, params) {
  return dispatch => {
    dispatch({
      type: FETCH_RELATEDVIDEO_REQUEST,
      payload: {
        id: params._rlvideoid,
      },
    });
    const rlVideoApi = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${params._rlvideoid}&type=video&feedtype=sjson&count=15`;
    return fetch(rlVideoApi)
      .then(
        data => {
          // rlvideodata = data;
          if (process.env.PLATFORM == "desktop") {
            return dispatch(fetchRelatedVideoDataSuccess(data, params.label));
          }
          return dispatch(fetchRelatedVideoDataSuccess(data));
        },
        error => dispatch(fetchRelatedVideoFailure(error, params)),
      )
      .catch(error => {
        dispatch(fetchRelatedVideoFailure(error, params));
      });
  };
}

function shouldFetchWidgetRelatedVideoData(state, params, query) {
  if (process.env.PLATFORM == "desktop") {
    return true;
  }
  return true;
}

export function fetchWidgetRelatedVideoDataIfNeeded(rlvideoid) {
  return (dispatch, getState) => {
    if (shouldFetchWidgetRelatedVideoData(getState(), rlvideoid)) {
      return dispatch(fetchWidgetRelatedVideoData(getState(), rlvideoid));
    }
    return Promise.resolve([]);
  };
}

function fetchWidgetDataFailure() {
  return {
    type: FETCH_WIDGETDATA_FAILURE,
  };
}

function fetchWidgetDataSuccess(data) {
  return {
    type: FETCH_WIDGETDATA_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchWidgetDataSuccessDesktop(label, data) {
  return {
    type: FETCH_WIDGETDATA_SUCCESS_DESKTOP,
    meta: {
      receivedAt: Date.now(),
    },
    label,
    payload: data,
  };
}

function fetchHomeCityData(state, params, pagetype, widgetInfo, forceDesktop) {
  if (params) {
    let tagparams = params._count ? `&count=${params._count}` : `&count=4`;
    tagparams += `&ctnadtype=${params._ctnadtype ? `${params._ctnadtype}` : `homedefault`}`;
    if (params && params._layouttype) tagparams += `&layouttype=${params._layouttype}`;

    return dispatch => {
      const listApiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${
        params.citySecId ? params.citySecId : params._sec_id
      }&type=list&feedtype=sjson${tagparams}`;
      // const videoApiUrl = `${process.env.API_BASEPOINT}/videolist_items.cms?msid=${params.citySecId ? params.citySecId : params._rlvideoid}&inarticlelist=1&feedtype=sjson`;

      let videoApiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${
        params.citySecId ? params.citySecId : params._rlvideoid
      }&type=video&feedtype=sjson&count=15`;
      videoApiUrl = params && params._pl ? videoApiUrl + "&pl=0" : videoApiUrl;

      let Promise1 = fetch(listApiUrl);
      let Promise2 = fetch(videoApiUrl);

      return Promise.all([Promise1, Promise2])
        .then(
          data => {
            const { home } = state;
            let listData = {};
            let homeData;

            if (widgetInfo && widgetInfo.length > 0) {
              if (process.env.PLATFORM === "desktop") {
                homeData = home && home.value && home.value[2] && home.value[2][params.label];
              } else {
                homeData = widgetInfo && widgetInfo[0];
              }
            }

            if (data && data[0] && data[0].items && data[0].items.length > 0) {
              listData = data[0];
              listData.id = params._sec_id;
              if (homeData) {
                homeData.items = data[0].items;
              }
            } else {
              listData = homeData;
            }

            if (data && data[1] && data[1].items && data[1].items.length > 0) {
              //let rlvideoData = {};
              //rlvideoData.items = data[1].items;
              listData.rlvideo = data[1];
              if (homeData) {
                homeData.rlvideo = data[1];
              }
            } else {
              listData.rlvideo = homeData && homeData.rlvideo;
            }

            if (widgetInfo) {
              listData = homeData;
            }

            if (pagetype && pagetype == "home") {
              listData.citySecId = params.citySecId ? params.citySecId : params._sec_id;
            }

            listData.params = params;
            if (process.env.PLATFORM === "desktop" || forceDesktop) {
              return dispatch(fetchWidgetDataSuccessDesktop(params.label, listData));
            }
            return dispatch(fetchWidgetDataSuccess(listData));
          },
          error => dispatch(fetchWidgetDataFailure(error)),
        )
        .catch(() => {
          dispatch(fetchWidgetDataFailure());
        });
    };
  }
  return dispatch => {
    dispatch(fetchWidgetDataFailure());
  };
}

function fetchWidgetData(state, params, query, forceDesktop) {
  let tagparams = "&count=4";
  if (!isMobilePlatform() && params._count) {
    tagparams = `&count=${params._count}`;
  } else if (params._sec_id == "19615041" || params._sec_id == "2354729") {
    //This code is for couple of days , needs to remove this later
    tagparams = "&count=5";
  }
  tagparams += `&ctnadtype=${params._ctnadtype ? `${params._ctnadtype}` : `homedefault`}`;
  if (params._layouttype) tagparams += `&layouttype=${params._layouttype}`;

  return dispatch => {
    const listApiUrl = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${params._sec_id}&type=${params._type}&feedtype=sjson${tagparams}`;
    return fetch(listApiUrl)
      .then(
        data => {
          const listData = data;
          listData._rlvideoid = params._rlvideoid;
          listData.params = params;

          // if(params && params.citySecId != '' && params.citySecId != undefined){
          //   return dispatch(fetchHomeCityData(state, params, listData,"home",forceDesktop ))
          // }

          if (!isMobilePlatform() || forceDesktop) {
            return dispatch(fetchWidgetDataSuccessDesktop(params.label, listData));
          }
          return dispatch(fetchWidgetDataSuccess(listData));
        },
        error => dispatch(fetchWidgetDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchWidgetDataFailure(error));
      });
  };
}

function shouldFetchWidgetData(state, params, query) {
  return true;
}

export function fetchWidgetDataIfNeeded(params, query, forceDesktop) {
  return (dispatch, getState) => {
    if (shouldFetchWidgetData(getState(), params, query)) {
      return dispatch(fetchWidgetData(getState(), params, query, forceDesktop));
    }
    return Promise.resolve([]);
  };
}

export function fetchHomeCityWidgetDataIfNeeded(params, pagetype, widgetInfo) {
  return (dispatch, getState) => {
    if (shouldFetchWidgetData(getState(), params)) {
      return dispatch(fetchHomeCityData(getState(), params, pagetype, widgetInfo));
    }
    return Promise.resolve([]);
  };
}

function fetchWidgetSequenceDataFailure(error) {
  return {
    type: FETCH_WIDGETSEQUENCE_FAILURE,
    payload: error.message,
  };
}

function fetchWidgetSequenceDataSuccess(data) {
  return {
    type: FETCH_WIDGETSEQUENCE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchWidgetSequenceData(state, params, query) {
  const homepagewidgets = _filterALJSON(state.header, ["pwaconfig", "HomePageWidgets"]);

  // let apiUrl = state.app.urls.navigation;

  //   var homepagewidgets = [];
  // for(index in state.header){
  //   if(state.header[index].label == "Homepage Widgets"){
  //    var widgets = state.header[index];
  //     for(key in widgets){
  //       if(widgets[key].label){
  //         let section = {};
  //         section.id = widgets[key]._id ? widgets[key]._id : "";
  //         section.rlvideoid = widgets[key]._rlvideoid ? widgets[key]._rlvideoid : "";
  //         section.type = widgets[key]._type ? widgets[key]._type : "";
  //         section.override = widgets[key]._override ? widgets[key]._override : "";
  //         homepagewidgets.push(section);
  //       }
  //     }
  //   }
  // }

  // console.log(state.header);
  return dispatch => {
    dispatch({
      type: FETCH_WIDGETSEQUENCE_REQUEST,
    });
    // return fetch(apiUrl).then(
    //   data => {
    //     dispatch(fetchWidgetSequenceDataSuccess(data))
    //   },
    //   error => dispatch(fetchWidgetSequenceDataFailure(error))
    // ).catch(function (error) {
    //   dispatch(fetchWidgetSequenceDataFailure(error));
    // });
    return dispatch(fetchWidgetSequenceDataSuccess(homepagewidgets));
  };
}

function shouldFetchWidgetSequenceData(state, params, query) {
  return true;
  // if ((typeof (state.home.value[1]) == 'undefined'/*|| typeof(state.home.value[0].items)=='undefined'*/)) {
  //   return true;
  // } else {
  //   return false;
  // }
}

export function fetchWidgetSequenceDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchWidgetSequenceData(getState(), params, query)) {
      return dispatch(fetchWidgetSequenceData(getState(), params, query));
    }
    return Promise.resolve([]);
  };
}

function fetchPollFailure(error) {
  return {
    type: FETCH_POLL_FAILURE,
    payload: error.message,
  };
}

function fetchPollSuccess(data) {
  return {
    type: FETCH_POLL_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchPollData(state, params) {
  const apiUrl = `${process.env.API_BASEPOINT}/pwa_poll.cms?msid=${params.msid}&type=${params.type}&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_POLL_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchPollSuccess(data)),
        error => dispatch(fetchPollFailure(error)),
      )
      .catch(error => {
        dispatch(fetchPollFailure(error));
      });
  };
}

function shouldFetchPollData(state) {
  return true;
}

export function fetchPollDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchPollData(getState())) {
      return dispatch(fetchPollData(getState(), params));
    }
    return Promise.resolve([]);
  };
}
