import fetch from "utils/fetch/fetch";
import { _getStaticConfig, _apiBasepointUpdate, _getfeedCategory } from "../../utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_LIST_REQUEST = "FETCH_LIST_REQUEST";
export const FETCH_LIST_SUCCESS = "FETCH_LIST_SUCCESS";
export const FETCH_LIST_FAILURE = "FETCH_LIST_FAILURE";
export const FETCH_BRAND_REQUEST = "FETCH_BRAND_REQUEST";
export const FETCH_BRAND_SUCCESS = "FETCH_BRAND_SUCCESS";
export const FETCH_BRAND_FAILURE = "FETCH_BRAND_FAILURE";
export const FETCH_NEXT_LIST_REQUEST = "FETCH_NEXT_LIST_REQUEST";
export const FETCH_NEXT_LIST_SUCCESS = "FETCH_NEXT_LIST_SUCCESS";

function fetchListDataFailure(error) {
  return {
    type: FETCH_LIST_FAILURE,
    payload: error.message,
  };
}
function fetchListDataSuccess(data) {
  return {
    type: FETCH_LIST_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchNextListDataSuccess(data, feedSection) {
  return {
    type: FETCH_NEXT_LIST_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
    feedSection,
  };
}

function fetchListData(state, params, query, router) {
  let curpg;
  let msid;
  if (typeof query !== "undefined" && typeof query.curpg !== "undefined") {
    curpg = query.curpg;
  }
  if (typeof params !== "undefined" && typeof params.msid !== "undefined") {
    msid = params.msid;
  }
  if (isNaN(msid)) {
    msid = siteConfig.pages[msid];
  }

  // handle short url like /tech or /review to passs section msid conditionally
  if (
    router.location.pathname == "/tech" ||
    router.location.pathname == "/tech/" ||
    router.location.pathname == "/tech/default.cms"
  ) {
    msid = siteConfig.pages.tech;
  } else if (router.location.pathname == "/tech/reviews.cms") {
    msid = siteConfig.pages.techreview;
  }
  // todo - need to pass msid via router(code written in router check commented line), i think we have to update react router v4.0 above & then validate
  // append tag params for data comes via solr tag in tech section
  const indexcat = router.location.pathname.indexOf("reviews");
  let tagparams = indexcat > -1 ? "&tag=solrreview" : "";
  tagparams += "";

  const apiUrl = `${_apiBasepointUpdate(router, params.splat)}/sc_articlelist${
    router.location.pathname == "/tech/reviews.cms" ? "_review" : ""
  }/${msid}.cms?feedtype=sjson${curpg && curpg > 1 ? `&curpg=${curpg}` : ""}${tagparams}`;

  return dispatch => {
    dispatch({
      type: FETCH_LIST_REQUEST,
      payload: msid,
    });
    const Promise1 = triggerServiceRequest({
      url: apiUrl,
      method: "GET",
    });
    // let Promise2 = fetch(sectionApi);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function fetchNextListData(state, params, query, router) {
  let curpg = 1;
  let msid = params.msid;
  if (typeof state.articlelist.value[0].pg.cp !== "undefined") {
    curpg = parseInt(state.articlelist.value[0].pg.cp) + 1;
    if (state.articlelist.value[0].pg.tp < curpg) {
      return dispatch => {
        dispatch(fetchListDataFailure("no data found"));
      };
    }
  }
  // todo - need to pass msid via router(code written in router check commented line), i think we have to update react router v4.0 above & then validate
  // handle short url like /tech or /review to passs section msid conditionally
  if (router.location.pathname == "/tech") {
    msid = siteConfig.pages.tech;
  } else if (router.location.pathname == "/tech/reviews.cms") {
    msid = siteConfig.pages.techreview;
  }
  // append tag params for data comes via solr tag in tech section
  const indexcat = router.location.pathname.indexOf("/tech/reviews");
  const tagparams = indexcat > -1 ? "&tag=solrreview" : "";
  const apiUrl = `${_apiBasepointUpdate(router, params.splat)}/sc_articlelist${
    router.location.pathname == "/tech/reviews.cms" ? "_review" : ""
  }/${msid}.cms?feedtype=sjson&curpg=${curpg}${tagparams}`;
  return dispatch => {
    dispatch({
      type: FETCH_NEXT_LIST_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchNextListDataSuccess(data)),
        error => dispatch(fetchListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchListDataFailure(error));
      });
  };
}

function shouldFetchListData(state, params, query) {
  // console.log("Inside shouldFetchListData",state.articlelist);
  // //return true;state.articlelist.isFetching==false &&
  if (
    typeof state.articlelist.value[0] === "undefined" ||
    (typeof state.articlelist.value[0] !== "undefined" && state.articlelist.value[0].id != params.msid)
  ) {
    return true;
  }
  return false;
}

export function fetchListDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchListData(getState(), params, query)) {
      return dispatch(fetchListData(getState(), params, query, router));
    }
    return Promise.resolve([]);
  };
}

export function fetchNextListDataIfNeeded(params, query, router) {
  return (dispatch, getState) => dispatch(fetchNextListData(getState(), params, query, router));
}

// fetch if brand widget category changes
export function fetchBrandListDataIfNeeded(params, query, router, category) {
  return (dispatch, getState) => dispatch(fetchBrandListData(getState(), params, query, router, category));
}

function fetchBrandListData(state, params, query, router, category) {
  category = _getfeedCategory(category);
  const apiUrl =
    params && params.splat
      ? `${_apiBasepointUpdate(router, params.splat)}/pwa_brandlist.cms?feedtype=sjson&category=${category}`
      : `${_apiBasepointUpdate(router)}/pwa_brandlist.cms?feedtype=sjson&category=${category}`;

  return dispatch => {
    dispatch({
      type: FETCH_BRAND_REQUEST,
      payload: category,
    });
    const Promise1 = fetch(apiUrl);
    // let Promise2 = fetch(sectionApi);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchBrandDataSuccess(data)),
        error => dispatch(fetchBrandDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchBrandDataFailure(error));
      });
  };
}

function fetchBrandDataFailure(error) {
  return {
    type: FETCH_BRAND_FAILURE,
    payload: error.message,
  };
}
function fetchBrandDataSuccess(data) {
  return {
    type: FETCH_BRAND_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}
