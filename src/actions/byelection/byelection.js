import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_BYELECTION_REQUEST = "FETCH_BYELECTION_REQUEST";
export const FETCH_BYELECTION_SUCCESS = "FETCH_BYELECTION_SUCCESS";
export const FETCH_BYELECTION_FAILURE = "FETCH_BYELECTION_FAILURE";

function fetchByElectionDataFailure(error) {
  return {
    type: FETCH_BYELECTION_FAILURE,
    payload: error.message
  };
}
function fetchByElectionDataSuccess(data, params) {
  //data[0]['pwa_meta']=data[1].pwa_meta;
  let seriesid =
    data[0] && data[0].pwa_meta && data[0].pwa_meta.cricketlb
      ? data[0].pwa_meta.cricketlb
      : null;
  return {
    type: FETCH_BYELECTION_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    seriesid: seriesid,
    msid: params.msid,
    payload: data
  };
}

function fetchByElectionData(state, params, query) {
  let msid;
  if (isNaN(msid)) {
    msid = siteConfig.pages[msid];
  }
  let sectionApi =
    process.env.API_BASEPOINT +
    "/pwafeeds/pwa_metalist.cms?msid=" +
    params.msid +
    "&feedtype=sjson";
  let apiUrlalt = siteConfig.byelectionalt;
  let apiUrl = siteConfig.byelection;
  return dispatch => {
    dispatch({
      type: FETCH_BYELECTION_REQUEST,
      payload: params.msid
    });
    let Promise1 = fetch(sectionApi);
    let Promise2 = fetch(apiUrl);
    let Promise3 = fetch(apiUrlalt);

    return Promise.all([Promise1, Promise2, Promise3])
      .then(
        data => {
          dispatch(fetchByElectionDataSuccess(data, params));
        },
        error => dispatch(fetchByElectionDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchByElectionDataFailure(error));
      });
  };
}

function shouldFetchByElectionData(state, params, query) {
  return true;
  if (
    (typeof state.pointstable.value[0] == "undefined" &&
      typeof state.pointstable.value[1] == "undefined") ||
    (typeof state.pointstable.value[0] != "undefined" &&
      typeof state.pointstable.value[1] != "undefined" &&
      state.pointstable.value[0].id != params.msid)
  ) {
    return true;
  } else {
    return false;
  }
}

export function fetchByElectionDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchByElectionData(getState(), params, query)) {
      return dispatch(fetchByElectionData(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}
