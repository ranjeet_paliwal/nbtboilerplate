import fetch from "utils/fetch/fetch";

import { _getStaticConfig, isTechPage, isTechSite } from "../../utils/util";
export const FETCH_APP_REQUEST = "FETCH_APP_REQUEST";
export const FETCH_APP_SUCCESS = "FETCH_APP_SUCCESS";
export const FETCH_APP_FAILURE = "FETCH_APP_FAILURE";
export const FETCH_BNEWS_REQUEST = "FETCH_BNEWS_REQUEST";
export const FETCH_BNEWS_SUCCESS = "FETCH_BNEWS_SUCCESS";
export const FETCH_BNEWS_FAILURE = "FETCH_BNEWS_FAILURE";
export const FETCH_MINITV_REQUEST = "FETCH_MINITV_REQUEST";
export const FETCH_MINITV_SUCCESS = "FETCH_MINITV_SUCCESS";
export const FETCH_MINITV_FAILURE = "FETCH_MINITV_FAILURE";
export const FETCH_TRENDING_REQUEST = "FETCH_TRENDING_REQUEST";
export const FETCH_TRENDING_SUCCESS = "FETCH_TRENDING_SUCCESS";
export const FETCH_TRENDING_FAILURE = "FETCH_TRENDING_FAILURE";

export const FETCH_MINISCORECARD_REQUEST = "FETCH_MINISCORECARD_REQUEST";
export const FETCH_MINISCORECARD_SUCCESS = "FETCH_MINISCORECARD_SUCCESS";
export const FETCH_MINISCORECARD_FAILURE = "FETCH_MINISCORECARD_FAILURE";

export const FETCH_WEATHER_REQUEST = "FETCH_WEATHER_REQUEST";
export const FETCH_WEATHER_SUCCESS = "FETCH_WEATHER_SUCCESS";
export const FETCH_WEATHER_FAILURE = "FETCH_WEATHER_FAILURE";

export const FETCH_FUEL_REQUEST = "FETCH_FUEL_REQUEST";
export const FETCH_FUEL_SUCCESS = "FETCH_FUEL_SUCCESS";
export const FETCH_FUEL_FAILURE = "FETCH_FUEL_FAILURE";

export const FETCH_RHS_DATA_REQUEST = "FETCH_RHS_DATA_REQUEST";
export const FETCH_RHS_DATA_SUCCESS = "FETCH_RHS_DATA_SUCCESS";
export const FETCH_RHS_DATA_FAILURE = "FETCH_RHS_DATA_FAILURE";
export const FETCH_IPLCAP_REQUEST = "FETCH_IPLCAP_REQUEST";
export const FETCH_IPLCAP_SUCCESS = "FETCH_IPLCAP_SUCCESS";
export const FETCH_IPLCAP_FAILURE = "FETCH_IPLCAP_FAILURE";
const siteConfig = _getStaticConfig();

function fetchAppFailure(error) {
  return {
    type: FETCH_APP_FAILURE,
    payload: error.message,
  };
}

function fetchAppSuccess(data) {
  return {
    type: FETCH_APP_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

export function fetchApp(params, query, state) {
  return dispatch => {
    dispatch({
      type: FETCH_APP_REQUEST,
    });
    return fetch(`${process.env.API_MASTER_FEED}`, {})
      .then(
        data => dispatch(fetchAppSuccess(data)),
        error => dispatch(fetchAppFailure(error)),
      )
      .catch(error => {
        dispatch(fetchAppFailure(error));
      });
  };
}

function fetchBnewsFailure(error) {
  return {
    type: FETCH_BNEWS_FAILURE,
    payload: error.message,
  };
}

function fetchBnewsSuccess(data) {
  return {
    type: FETCH_BNEWS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchBnewsData(state, params, query) {
  // let apiUrl = state.app.urls.bnews;
  // let apiUrl = process.env.API_ENDPOINT+"/api_bnews";
  const apiUrl = siteConfig.breakingNewsFeedUrl;
  return dispatch => {
    dispatch({
      type: FETCH_BNEWS_REQUEST,
    });
    return fetch(apiUrl, { type: "jsonp" })
      .then(
        data => {
          data = { bnews: data };
          dispatch(fetchBnewsSuccess(data));
        },
        error => dispatch(fetchBnewsFailure(error)),
      )
      .catch(error => {
        dispatch(fetchBnewsFailure(error));
      });
  };
}

function shouldFetchBnewsData(state, params, query) {
  if (state.app && (!state.app.bnews || !state.app.bnews.length || state.app.bnews.length < 70)) {
    return true;
  }
  return false;
}

export function fetchBnewsDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchBnewsData(getState(), params, query)) {
      return dispatch(fetchBnewsData(getState(), params, query));
    }
    return Promise.resolve([]);
  };
}

function fetchMiniTvFailure(error) {
  return {
    type: FETCH_MINITV_FAILURE,
    payload: error.message,
  };
}

function fetchMiniTvSuccess(data) {
  return {
    type: FETCH_MINITV_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchMiniTvData(state, params, query) {
  const apiUrl = `${
    siteConfig.channelCode == "nbt" ? process.env.NODE_API_BASEPOINT : process.env.API_BASEPOINT
  }/pwa_minitv.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({
      type: FETCH_MINITV_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchMiniTvSuccess(data)),
        error => dispatch(fetchMiniTvFailure(error)),
      )
      .catch(error => {
        dispatch(fetchMiniTvFailure(error));
      });
  };
}

function shouldFetchMiniTvData(state, params, query) {
  if (state.app && (!state.app.minitv || !state.app.minitv.hl)) {
    return true;
  }
  return false;
}

export function fetchMiniTvDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchMiniTvData(getState(), params, query)) {
      return dispatch(fetchMiniTvData(getState(), params, query));
    }
    return Promise.resolve([]);
  };
}

function fetchMiniScorecardFailure(error) {
  return {
    type: FETCH_MINISCORECARD_FAILURE,
    payload: error.message,
  };
}

function fetchMiniScorecardSuccess(data) {
  return {
    type: FETCH_MINISCORECARD_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchTrendingData(state, params, query) {
  // apiUrl = `${process.env.API_BASEPOINT}/sc_trendinglist.cms?msid=${siteConfig.editTrendingMSID}&type=list&feedtype=sjson&layouttype=hpui3&count=7`;
  const apiUrl = `${process.env.API_BASEPOINT}/sc_trending.cms?${params && params.subsec1 ? `msid=${params.subsec1}&` : ""}tag=edittrending&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_TRENDING_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchTrendingSuccess(data)),
        error => dispatch(fetchTrendingFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTrendingFailure(error));
      });
  };
}

function fetchTrendingFailure(error) {
  return {
    type: FETCH_TRENDING_FAILURE,
    payload: error.message,
  };
}

function fetchTrendingSuccess(data) {
  return {
    type: FETCH_TRENDING_SUCCESS,
    payload: data,
  };
}

function shouldfetchTrendingData(state, params, query) {
  // To avoid double fetch scenario due to componentDidUpdate
  // if (state.config && ( (state.config.subsec1 === "" && state.config.pagetype==="home") || (state.config.subsec1 !== "" && state.config.pagetype!=="home") ) ) {
  return true;
  // }
  // return false;
}

export function fetchTrendingDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldfetchTrendingData(getState(), params, query)) {
      return dispatch(fetchTrendingData(getState(), params, query));
    }
    return Promise.resolve([]);
  };
}

function fetchMiniScorecardData(state, params, query) {
  const apiUrl = "https://nbtstorage.indiatimes.com/regionalscorecard/scorecard/scorecardnw_widget.json";
  // let apiUrl = process.env.API_ENDPOINT + "/api_miniscorecard";
  // let apiUrl= "https://langdev8002.indiatimes.com/feeds/appcricwidget.cms?feedtype=sjson&source=web&upcache=2";

  return dispatch => {
    dispatch({
      type: FETCH_MINISCORECARD_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchMiniScorecardSuccess(data)),
        error => dispatch(fetchMiniScorecardFailure(error)),
      )
      .catch(error => {
        dispatch(fetchMiniScorecardFailure(error));
      });
  };
}

function shouldFetchMiniScorecardData(state, params, query) {
  return true;
  // if(state.app && (!state.app.minitv || !state.app.minitv.hl)){
  //   return true;
  // } else {
  //   return false;
  // }
}

export function fetchMiniScorecardDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchMiniScorecardData(getState(), params, query)) {
      return dispatch(fetchMiniScorecardData(getState(), params, query));
    }
    return Promise.resolve([]);
  };
}

function fetchMiniScorecardFailure(error) {
  return {
    type: FETCH_MINISCORECARD_FAILURE,
    payload: error.message,
  };
}

function fetchMiniScorecardSuccess(data) {
  return {
    type: FETCH_MINISCORECARD_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchMiniScorecardData(state, params, query) {
  const apiUrl = "https://nbtstorage.indiatimes.com/regionalscorecard/scorecard/scorecardnw_widget.json";
  // let apiUrl = process.env.API_ENDPOINT + "/api_miniscorecard";
  // let apiUrl= "https://langdev8002.indiatimes.com/feeds/appcricwidget.cms?feedtype=sjson&source=web&upcache=2";

  return dispatch => {
    dispatch({
      type: FETCH_MINISCORECARD_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchMiniScorecardSuccess(data)),
        error => dispatch(fetchMiniScorecardFailure(error)),
      )
      .catch(error => {
        dispatch(fetchMiniScorecardFailure(error));
      });
  };
}

function shouldFetchMiniScorecardData(state, params, query) {
  return true;
  // if(state.app && (!state.app.minitv || !state.app.minitv.hl)){
  //   return true;
  // } else {
  //   return false;
  // }
}

/* 
  WHY HERE?
  Because the alternative is calling this in every action
  (photoshow) (articleshow) etc which would increase size 
  of those files, this will be directly called in desktop
  containers and update it's own reducer ( APP reducer )
  which would always fetch latest data and  

*/
export function fetchRHSDataPromise(dispatch) {
  dispatch({
    type: FETCH_RHS_DATA_REQUEST,
  });
  let apiUrl = "";
  if (["development", "stg1", "stg2", "stg3"].includes(process.env.DEV_ENV)) {
    apiUrl = `${process.env.API_BASEPOINT}/pwa_home_new.cms?feedtype=sjson`;
  } else {
    apiUrl = `${siteConfig.langnetstorageBasePath}/${siteConfig.channelCode}headline.htm`;
  }
  // FIXME: Change feed path to config
  // const url = !isGN
  //   ? `https://langnetstorage.indiatimes.com/Score/${siteConfig.channelCode}_rhs.htm`
  //   : `https://langnetstorage.indiatimes.com/Score/${siteConfig.channelCode}_techrhs.htm`;
  // const url = `https://langnetstorage.indiatimes.com/Score/${siteConfig.channelCode}_rhs.htm`;

  return (
    // fetch(`https://langnetstorage.indiatimes.com/Score/${siteConfig.channelCode}_rhs.htm`, {}, true)
    fetch(apiUrl)
      .then(
        data => {
          dispatch({ type: FETCH_RHS_DATA_SUCCESS, payload: data && data.items });
          // This parameter will be used by components where filtering is needed when header is fetched
        },
        error => dispatch({ type: FETCH_RHS_DATA_FAILURE, payload: error }),
      )
      .catch(error => {
        dispatch({ type: FETCH_RHS_DATA_FAILURE, payload: error });
      })
  );
}

function fetchWeatherFailure(error) {
  return {
    type: FETCH_WEATHER_FAILURE,
    payload: error.message,
  };
}

function fetchWeatherSuccess(data) {
  return {
    type: FETCH_WEATHER_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchCitiesData(state) {
  const citiesApi = siteConfig.weather.citiesJson;
  if (state.app && state.app.weather && state.app.weather.citiesJson) {
    return Promise.resolve({
      cityjson: state.app.weather.citiesJson,
    });
  }
  return fetch(citiesApi);
}

function fetchWeatherData(state, params) {
  return dispatch => {
    fetchCitiesData(state).then(
      citiesJson => {
        citiesJson = citiesJson.cityjson;
        const currentCity = params.cityName
          ? params.cityName
          : window.geoinfo && window.geoinfo.city
          ? window.geoinfo.city
          : siteConfig.weather.defaultCity;
        let cityData = citiesJson.filter(city => currentCity.toLowerCase() == city.city.toLowerCase());
        // If city fro geolocation API doesnot matches with any city in citiesJson then pick default city weather data
        cityData =
          cityData && cityData.length > 0
            ? cityData
            : citiesJson.filter(city => siteConfig.weather.defaultCity.toLowerCase() == city.city.toLowerCase());
        if (window.location.pathname.indexOf("/gulf") > -1) {
          cityData = [
            {
              city: siteConfig.uaeJson.city,
              country: siteConfig.uaeJson.country,
              lang: siteConfig.uaeJson.lang,
              lat: siteConfig.uaeJson.lat,
              long: siteConfig.uaeJson.long,
              mly: siteConfig.uaeJson.mly,
            },
          ];
        }
        const weatherApi = `${siteConfig.weather.weatherFeed + cityData[0].lat}&lng=${cityData[0].long}`;
        dispatch({
          type: FETCH_WEATHER_REQUEST,
        });
        fetch(weatherApi).then(
          weatherData => {
            dispatch(
              fetchWeatherSuccess({
                weatherData,
                citiesJson,
              }),
            );
          },
          error => {
            dispatch(fetchWeatherFailure(error));
          },
        );
      },
      error => {},
    );
  };
}

function shouldFetchWeatherData(state) {
  return true;
}

export function fetchWeatherDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchWeatherData(getState())) {
      return dispatch(fetchWeatherData(getState(), params));
    }
  };
}

function fetchFuelFailure(error) {
  return {
    type: FETCH_FUEL_FAILURE,
    payload: error.message,
  };
}

function fetchFuelSuccess(data) {
  return {
    type: FETCH_FUEL_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchFuelData() {
  return dispatch => {
    const apiURL = siteConfig.fuel.feedUrl;
    dispatch({
      type: FETCH_FUEL_REQUEST,
    });
    fetch(apiURL).then(
      data => {
        dispatch(
          fetchFuelSuccess({
            data,
          }),
        );
      },
      error => {
        dispatch(fetchFuelFailure(error));
      },
    );
  };
}

function shouldFetchFuelData(state) {
  return !(state.app && state.app.fuel && state.app.fuel.length > 0);
}

export function fetchFuelDataIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchFuelData(getState())) {
      return dispatch(fetchFuelData());
    }
  };
}

function fetchIplCapFailure(error) {
  return {
    type: FETCH_IPLCAP_FAILURE,
    payload: error.message,
  };
}

function fetchIplCapSuccess(data) {
  return {
    type: FETCH_IPLCAP_SUCCESS,
    payload: data,
  };
}

function fetchIplCapData() {
  const apiUrl = siteConfig.cricket_SIAPI.topPlayer;

  return dispatch => {
    dispatch({
      type: FETCH_IPLCAP_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchIplCapSuccess(data)),
        error => dispatch(fetchIplCapFailure(error)),
      )
      .catch(error => {
        dispatch(fetchIplCapFailure(error));
      });
  };
}

function shouldFetchIplCapData() {
  return true;
}

export function fetchIplCapDataIfNeeded() {
  return dispatch => {
    if (shouldFetchIplCapData()) {
      return dispatch(fetchIplCapData());
    }
  };
}
