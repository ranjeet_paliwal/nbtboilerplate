import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";

const siteConfig = _getStaticConfig();

export const FETCH_GADGETS_REQUEST = "FETCH_GADGETS_REQUEST";
export const FETCH_GADGETS_SUCCESS = "FETCH_GADGETS_SUCCESS";
export const FETCH_GADGETS_FAILURE = "FETCH_GADGETS_FAILURE";

function fetchGadgetsFailure(error) {
  return {
    type: FETCH_GADGETS_FAILURE,
    payload: error.message,
  };
}

function fetchGadgetsSuccess(data) {
  return {
    type: FETCH_GADGETS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchGadgetsData(state, params) {
  const apiUrl = `${process.env.API_BASEPOINT}/pwa_poll.cms?msid=${params.msid}&type=${params.type}&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_GADGETS_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchGadgetsSuccess(data)),
        error => dispatch(fetchGadgetsFailure(error)),
      )
      .catch(error => {
        dispatch(fetchPollFailure(error));
      });
  };
}

function shouldFetchGadgetsData(state) {
  return true;
}

export function fetchGadgetsDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchGadgetsData(getState())) {
      return dispatch(fetchGadgetsData(getState(), params));
    }
    return Promise.resolve([]);
  };
}
