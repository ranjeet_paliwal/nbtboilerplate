import fetch from "utils/fetch/fetch";

export const FETCH_FOOTER_LINKS_REQUEST = "FETCH_FOOTER_LINKS_REQUEST";
export const FETCH_FOOTER_LINKS_SUCCESS = "FETCH_FOOTER_LINKS_SUCCESS";
export const FETCH_FOOTER_LINKS_FAILURE = "FETCH_FOOTER_LINKS_FAILURE";

function fetchLinksFailure(error) {
  return {
    type: FETCH_FOOTER_LINKS_FAILURE,
    payload: error.message,
  };
}

function fetchLinksSuccess(data) {
  return {
    type: FETCH_FOOTER_LINKS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchLinksData(state, params, query) {
  if (typeof params === "undefined" || params.msid == "") {
    return fetchLinksSuccess({ msid: "" });
  }

  const apiUrl = `${process.env.API_BASEPOINT}/sc_quicklinks/${params.msid}.cms?feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_FOOTER_LINKS_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchLinksSuccess({ msid: params.msid, footer_links: data })),
        error => dispatch(fetchLinksFailure(error)),
      )
      .catch(error => {
        dispatch(fetchLinksFailure(error));
      });
  };
}

function shouldFetchLinksData(state, params) {
  if (state.footerlinks && state.footerlinks.msid != params.msid) {
    return true;
  }
  return false;

  // return true;
}

export function fetchLinksDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLinksData(getState(), params, query)) {
      return dispatch(fetchLinksData(getState(), params, query));
    }
  };
}
