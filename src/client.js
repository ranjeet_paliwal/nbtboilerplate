// import 'css-reset';
// import "babel-polyfill";
import React from "react";
import { hydrate } from "react-dom";
import { browserHistory } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import { AppContainer } from "react-hot-loader";
import Root from "containers/root/Root";
import configureStore from "store/configureStore";

const initialState = window.__INITIAL_STATE__;

const store = configureStore(initialState);
const history = syncHistoryWithStore(browserHistory, store);

// FIXME: As we are using initial state in Quicklinks in footer, need to find a good solution for this
// delete window.__INITIAL_STATE__;

hydrate(
  <AppContainer>
    <Root store={store} history={history} />
  </AppContainer>,
  document.getElementById("root"),
);

/* eslint-disable */
if (module.hot) {
  module.hot.accept("containers/root/Root", () => {
    const NextRoot = require("containers/root/Root").default;
    hydrate(
      <AppContainer>
        <NextRoot store={store} history={history} />
      </AppContainer>,
      document.getElementById("root"),
    );
  });
}
/* eslint-enable */
