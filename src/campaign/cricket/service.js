export const extractMiniSchedule = minischedule => {
  let tmpSchedule = minischedule ? JSON.parse(JSON.stringify(minischedule)) : {};
  let schedule = tmpSchedule && tmpSchedule.schedule;
  let date = new Date().toDateString();
  let todayDate = new Date(date).getTime();

  let itemDate;
  let prevMatches = [],
    currMatches = [],
    totMatches = [];

  schedule &&
    schedule.map(item => {
      itemDate = new Date(item.matchdate_ist).getTime();
      if (Number(itemDate) < Number(todayDate)) {
        prevMatches.push(item);
      } else if (Number(itemDate) >= Number(todayDate)) {
        currMatches.push(item);
      }
    });
  if (currMatches.length > 0) {
    totMatches.push(...currMatches.slice(0, 3));
  }
  if (prevMatches.length > 0) {
    totMatches.push(...prevMatches.slice(prevMatches.length - 2, prevMatches.length));
  }

  if (tmpSchedule && tmpSchedule.schedule) {
    tmpSchedule.schedule = totMatches;
  }

  return tmpSchedule;
};
