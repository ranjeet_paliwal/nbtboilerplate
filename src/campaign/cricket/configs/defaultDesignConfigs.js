export const defaultDesignConfigs = {
  gridView: [{ startIndex: 0, type: "vertical", className: "col12" }],
  gridLead: [{ startIndex: 0, type: "lead", className: "col12" }],
  gridHorizontalLeadAL: [
    {
      startIndex: 0,
      type: "vertical",
      className: "col12 view-horizontal",
      imgsize: "largethumb",
    },
  ],
  gridHorizontalAL: [
    {
      startIndex: 0,
      type: "horizontal",
      className: "col12",
      imgsize: "smallthumb",
      offDrawer: true,
    },
  ],
  gallery: [
    { startIndex: 0, noOfElements: 1, type: "lead", className: "col12" },
    { startIndex: 1, type: "vertical", className: "col12 view-horizontal" },
  ],
  webstories: [
    // { startIndex: 0, noOfElements: 1, type: "lead", className: "col12" },
    { startIndex: 0, type: "vertical", className: "col12 view-horizontal" },
  ],
  horizontalSlider: [{ startIndex: 0, type: "vertical", className: "col12 view-horizontal" }],
  movieListView: [
    {
      startIndex: 0,
      type: "horizontal",
      className: "col12",
      imgsize: "posterthumb",
    },
  ],
  listGallery: [
    { startIndex: 0, noOfElements: 1, type: "lead", className: "col12 hideSecName1" },
    {
      startIndex: 1,
      type: "horizontal",
      className: "col12 hideSecName1",
      imgsize: "smallthumb",
    },
  ],
  listWithOnlyInfo: [{ startIndex: 0, noOfElements: 4, type: "only-info", className: "col12" }],
  topNewsWidgetVertical: [
    {
      startIndex: 0,
      type: "vertical",
      noOfColumns: 1,
      className: "col12 most-read-stroies",
    },
  ],
  topicslisting: [
    {
      type: "horizontal",
      className: "col12 rest-topics",
      imgsize: "smallthumb",
    },
  ],
  reviewListingGn: [
    { startIndex: 0, noOfElements: 1, type: "lead", className: "col12 hideSecName1" },
    {
      startIndex: 1,
      noOfElements: 5,
      type: "horizontal",
      className: "col12 hideSecName1",
      imgsize: "smallthumb",
    },
  ],
}; // noOfElements: 10, // noOfElements: 10, // noOfElements: 10,
