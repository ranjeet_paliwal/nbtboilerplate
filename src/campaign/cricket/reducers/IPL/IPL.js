import {
  FETCH_TEAM_REQUEST,
  FETCH_TEAM_SUCCESS,
  FETCH_TEAM_FAILURE,
  FETCH_VENUE_REQUEST,
  FETCH_VENUE_SUCCESS,
  FETCH_VENUE_FAILURE,
  FETCH_TEAM_DETAIL_REQUEST,
  FETCH_TEAM_DETAIL_SUCCESS,
  FETCH_TEAM_DETAIL_FAILURE,
} from "../../actions/IPL/IPL";

function IPL(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    msid: "", //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_TEAM_REQUEST:
      state.msid = action.payload; // Usage explained above
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_TEAM_SUCCESS:
      //   if (action.payload && action.payload.length > 0 && action.payload[0].id == state.msid) {
      //     state.value = action.payload && action.payload[0] ? [action.payload[0]] : [];
      //     state.astroListingWidget = action.payload && action.payload[1] ? action.payload[1] : [];
      //   }
      if (action.payload && action.payload.length > 0) {
        state.teamlist = action.payload[0];
      }
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_TEAM_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_VENUE_REQUEST:
      state.msid = action.payload; // Usage explained above
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_VENUE_SUCCESS:
      if (action.payload && action.payload.length > 0) {
        state.venuelist = action.payload[0];
      }
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_VENUE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_TEAM_DETAIL_REQUEST:
      state.msid = action.payload; // Usage explained above
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_TEAM_DETAIL_SUCCESS:
      if (action.payload && action.payload.length > 0) {
        state.teamDetail = action.payload[0];
      }
      return {
        ...state,
        isFetching: false,
      };

    case FETCH_TEAM_DETAIL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    default:
      return state;
  }
}

export default IPL;
