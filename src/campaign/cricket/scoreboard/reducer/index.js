import {
  FETCH_SCOREBOARD_REQUEST,
  FETCH_SCOREBOARD_SUCCESS,
  FETCH_SCOREBOARD_FAILURE
} from "./../action/index";

function SCOREBOARD(state = {}, action) {
  switch (action.type) {
    case FETCH_SCOREBOARD_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_SCOREBOARD_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_SCOREBOARD_SUCCESS:
      state = { data: action.payload };
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default SCOREBOARD;
