import fetch from "./../../../../utils/fetch/fetch";
export const FETCH_SCOREBOARD_REQUEST = "FETCH_SCOREBOARD_REQUEST";
export const FETCH_SCOREBOARD_SUCCESS = "FETCH_SCOREBOARD_SUCCESS";
export const FETCH_SCOREBOARD_FAILURE = "FETCH_SCOREBOARD_FAILURE";

function fetchSCOREBOARD_DataFailure(error) {
  return {
    type: FETCH_SCOREBOARD_FAILURE,
    payload: error.message
  };
}

function fetchSCOREBOARD_DataSuccess(data) {
  return {
    type: FETCH_SCOREBOARD_SUCCESS,
    payload: data
  };
}

function getMatchID(state) {
  let id, miniScoreCard;

  if (state && state.app && state.app.mini_scorecard) {
    miniScoreCard = state.app.mini_scorecard;
    if (miniScoreCard.Calendar) {
      miniScoreCard.Calendar.forEach(function(item) {
        if (
          item.live == 1 &&
          (item.teama == "India" || item.teamb == "India")
        ) {
          id = item.matchid;
        }
      });
    }
  }

  return id;
}
function fetchSCOREBOARD_Data(state, params, query) {
  let id;
  id = getMatchID(state);
  const _url = `https://toicri.timesofindia.indiatimes.com/jsons/${id}.json`;

  return dispatch => {
    dispatch({ type: FETCH_SCOREBOARD_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('times.mobile.election.electionresults(', '').slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchSCOREBOARD_DataSuccess(JSON.parse(data)));
        },
        error => dispatch(fetchSCOREBOARD_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchSCOREBOARD_DataFailure(error));
      });
  };
}

function fetchSCOREBOARDPWA_Data(state, params, query) {
  //const _url = `https://toibnews.timesofindia.indiatimes.com/Election/election2017_sp_homepage_result_widget_2018_new.htm`;
  // const _url = `https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/may19/mobile_elections_merged_feeds.json`;
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/ae/common/results.htm`;
  const _pwaurl =
    process.env.API_BASEPOINT +
    "/pwafeeds/pwa_metalist.cms?msid=66895640&feedtype=sjson";

  return dispatch => {
    dispatch({ type: FETCH_SCOREBOARD_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          data = data
            .replace("times.mobile.election.electionresults(", "")
            .slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          data = JSON.parse(data);
          dispatch({
            type: FETCH_SCOREBOARD_REQUEST
          });
          return fetch(_pwaurl)
            .then(
              pwadata => {
                data["pwadata"] = pwadata;
                dispatch(fetchSCOREBOARD_DataSuccess(data));
              },
              error => dispatch(fetchSCOREBOARD_DataFailure(error))
            )
            .catch(error => {
              dispatch(fetchSCOREBOARD_DataFailure(error));
            });
        },
        error => dispatch(fetchSCOREBOARD_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchSCOREBOARD_DataFailure(error));
      });
  };
}

function shouldFetchSCOREBOARD_Data(state, params, query) {
  // return (typeof state.electionresult.data == 'undefined');
  return true;
}

export function fetchSCOREBOARD_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchSCOREBOARD_Data(getState(), params, query)) {
      return dispatch(fetchSCOREBOARD_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

export function fetchSCOREBOARDPWA_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchSCOREBOARD_Data(getState(), params, query)) {
      return dispatch(fetchSCOREBOARDPWA_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

export function fetchSCOREBOARD_Data_Polling(dispatch) {
  // const _url = `https://toibnews.timesofindia.indiatimes.com/Election/election2017_sp_homepage_result_widget_2018_new.htm`;
  // const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.htm';
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/ae/common/results.htm`;

  dispatch({ type: FETCH_SCOREBOARD_REQUEST });

  return fetch(_url, { type: "jsonp" })
    .then(
      data => {
        data = data
          .replace("times.mobile.election.electionresults(", "")
          .slice(0, -1);
        if (!data)
          throw new NetworkError("Invalid JSON Response", response.status);
        dispatch(fetchSCOREBOARD_DataSuccess(JSON.parse(data)));
      },
      error => dispatch(fetchSCOREBOARD_DataFailure(error))
    )
    .catch(function(error) {
      dispatch(fetchSCOREBOARD_DataFailure(error));
    });
}
