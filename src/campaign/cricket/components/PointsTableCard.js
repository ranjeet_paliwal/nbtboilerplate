import React, { Component } from "react";
import { connect } from "react-redux";
//import { Link } from 'react-router'
// import PropTypes from "prop-types";
import { fetchPointsTableDataIfNeeded } from "../actions/pointstable/pointstable";
import "../../../components/common/css/PointsTable.scss";
import AnchorLink from "../../../components/common/AnchorLink";
import { _isCSR, _getStaticConfig, isMobilePlatform, hasValue, generateUrl } from "../../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../../utils/cricket_util";

import ImageCard from "../../../components/common/ImageCard/ImageCard";
import "../../../components/common/css/commonComponents.scss";
import { withRouter } from "react-router";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
// const siteConfig = _getStaticConfig();

class PointsTableCard extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    let _this = this;
    const { dispatch, params, query, router, ptMsid } = _this.props;
    // let params = {
    //   seriesid: this.props.seriesid,
    // };
    //In case of CSR msid needs to passed via props
    if (!params.msid) {
      params.msid = ptMsid;
    }
    if (_isCSR()) {
      dispatch(fetchPointsTableDataIfNeeded(params, query, router)); //Load Schedule Widget
    }
  }

  render() {
    let pointstable = hasValue(this.props, "value.1.standings.stage1.team");
    pointstable = pointstable && pointstable.length > 0 ? pointstable : null;

    const { compType } = this.props;
    let isPointCard;
    if (compType) {
      isPointCard = compType == "pointtablecard" ? true : false;
    }

    let { ...parentSection } = this.props && this.props.value && this.props.value[0] ? this.props.value[0] : "";
    let heading = parentSection && parentSection.secname ? parentSection.secname : "";

    return (
      <React.Fragment>
        {pointstable && typeof pointstable != "null" ? (
          <div className={`wdt_pointsTable ${isMobilePlatform() ? "box-item" : ""}`}>
            {parentSection && parentSection.secname ? (
              <SectionHeader
                headingTag={isPointCard ? "h2" : "h1"}
                sectionhead={heading}
                weblink={isPointCard ? (parentSection && parentSection.override ? generateUrl(parentSection) : "") : ""}
                morelink={false}
              />
            ) : null}
            <div className="table_layout">
              <table>
                <tbody>
                  <tr>
                    <th>TEAM</th>
                    <th>{isPointCard ? "P" : "PLAYED"}</th>
                    <th>{isPointCard ? "W" : "WON"}</th>
                    <th>{isPointCard ? "L" : "LOSS"}</th>
                    {!isPointCard ? <th>NRR</th> : null}
                    <th>{isPointCard ? "PT" : "NET POINTS"}</th>
                  </tr>

                  {pointstable.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <ImageCard msid={_getTeamLogo(item.id, "square")} size="squarethumb" />
                          {item.short_name}
                        </td>
                        <td>{item.p}</td>
                        <td>{item.w}</td>
                        <td>{item.l}</td>
                        {!isPointCard ? <td>{item.nrr}</td> : null}
                        <td>{item.pts}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state.pointstable,
  };
}

export default withRouter(connect(mapStateToProps)(PointsTableCard));
// export default PointsTableCard;
