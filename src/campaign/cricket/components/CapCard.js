import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import ImageCard from "../../../components/common/ImageCard/ImageCard";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import SvgIcon from "../../../components/common/SvgIcon";
import { LoadingComponent } from "../../../utils/util";

class CapCard extends PureComponent {
  render() {
    const { ocap, pcap, type, className } = this.props;
    const data = type === "orange" ? ocap : pcap;
    if (!data) {
      return <LoadingComponent />;
    }
    return (
      <React.Fragment>
        <SectionHeader sectionhead={`${type === "orange" ? "Orange" : "Purple"} Cap`} />
        <div className={`${type} ${className || ""}`}>
          <div className="news-card horizontal">
            <span className="img_wrap">
              <ImageCard
                src={data ? data.img : "nn"}
                title=""
                size="smallthumb"
                type="absoluteImgSrc"
              />
            </span>
            <span className="con_wrap">
              <span className="name">
                <b>{data ? data.name : "nn"}</b> <span className="tm">({data.team})</span>
              </span>
              <span className="des">
                {type === "orange" ? `${data.r} RUNS` : `${data.w} WICKETS`}
                <SvgIcon name="ipl-caps" className="caps_icon" />
              </span>
            </span>
          </div>
          <ul>
            <li>
              <b>Mat</b>
              {data.m}
            </li>
            <li>
              <b>{type === "orange" ? "Avg" : "B Avg"}</b>
              {type === "orange" ? data.avg : data.boavg}
            </li>
            <li>
              <b>{type === "orange" ? "S/R" : "5W"}</b>
              {type === "orange" ? data.sr : data["five-wick"]}
            </li>
            <li>
              <b>{type === "orange" ? "100" : "3W"}</b>
              {type === "orange" ? data.huns : data["three-wick"]}
            </li>
            <li>
              <b>{type === "orange" ? "50" : "BB"}</b>
              {type === "orange" ? data.fifties : data["best-bowl"]}
            </li>
            <li>
              <b>{type === "orange" ? "HS" : "Econ"}</b>
              {type === "orange" ? data.hs : data.eco}
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state.app.capData,
  };
}

export default withRouter(connect(mapStateToProps)(CapCard));
