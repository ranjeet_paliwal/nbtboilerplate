import React, { PureComponent } from "react";

class PlayerStats extends PureComponent {
  render() {
    const { data } = this.props;

    const tablestats = [];
    const rowArr = ["Test", "ODI", "T20I", "Domestic-T20", "Domestic-Firstclass", "Domestic-Lista"];
    const colBatArr = ["Mat", "Inn", "Runs", "NO", "HS", "Avg", "SR", "100s", "50s"];
    const colBowlArr = ["Mat", "Overs", "Wkts", "Maidens", "Best", "Avg", "Eco", "4W", "5W"];

    const formattedStats = getStats(data);
    formattedStats &&
      formattedStats.forEach(item => {
        tablestats[rowArr.indexOf(item.formatBatRec["format"])] = item;
      });

    return (
      <React.Fragment>
        <StatsTable
          tablestats={tablestats}
          rowArr={rowArr}
          colData={colBatArr}
          profileStr={"formatBatRec"}
          tableHeading="Batting Statistics"
        />
        <StatsTable
          tablestats={tablestats}
          rowArr={rowArr}
          colData={colBowlArr}
          profileStr={"formatBowlRec"}
          tableHeading="Bowling Statistics"
        />
      </React.Fragment>
    );
  }
}
const getStats = profile => {
  const stats = profile && profile.stats && profile.stats.format;

  const formatstats =
    stats &&
    stats.map(item => {
      let batRecord = item.overall.batting_record,
        bowlRecord = item.overall.bowling_record;
      let formatBatRec = {},
        formatBowlRec = {};
      formatBatRec["format"] = item.comp_type;
      if (batRecord) {
        formatBatRec["Mat"] = batRecord.matches;
        formatBatRec["Inn"] = batRecord.innings;
        formatBatRec["Runs"] = batRecord.runs;
        formatBatRec["NO"] = batRecord.not_outs;
        formatBatRec["HS"] = batRecord.hs && batRecord.hs.score && batRecord.hs.score.text;
        formatBatRec["Avg"] = batRecord.average;
        formatBatRec["SR"] = batRecord.strike_rate;
        formatBatRec["100s"] = batRecord.hundreds;
        formatBatRec["50s"] = batRecord.fifties;
      }

      if (bowlRecord) {
        formatBowlRec["Mat"] = bowlRecord.matches;
        formatBowlRec["Overs"] = bowlRecord.overs;
        formatBowlRec["Wkts"] = bowlRecord.wickets;
        formatBowlRec["Maidens"] = bowlRecord.maidens;
        formatBowlRec["Best"] =
          bowlRecord.best_bowling && bowlRecord.best_bowling.figure && bowlRecord.best_bowling.figure.text;
        formatBowlRec["Avg"] = bowlRecord.average;
        formatBowlRec["Eco"] = bowlRecord.economy_rate;
        formatBowlRec["4W"] = bowlRecord.four_wk_hauls;
        formatBowlRec["5W"] = bowlRecord.five_wk_hauls;
      }

      return {
        formatBatRec: formatBatRec,
        formatBowlRec: formatBowlRec,
      };
    });

  return formatstats;
};

const toggleTable = e => {
  console.log(e && e.target.value);
  let selectedEle, attrVal;

  attrVal = e && e.target.getAttribute("attr") && e.target.getAttribute("attr");
  let read_more = document.querySelector(`.${attrVal}_togglebtn`);

  if (attrVal) {
    selectedEle = document.querySelector(`.${attrVal}`);
    if (selectedEle.classList.contains("collapse")) {
      selectedEle.classList.remove("collapse");
      read_more.classList.add("minus");
      read_more.classList.remove("plus");
    } else {
      selectedEle.classList.add("collapse");
      read_more.classList.add("plus");
      read_more.classList.remove("minus");
    }
  }
};

const StatsTable = props => {
  const { tablestats, rowArr, colData, profileStr, tableHeading } = props;

  const tableClass = tableHeading.replace(" ", "_");

  return (
    <React.Fragment>
      <div className="section tableHeading">
        <div className="top_section">
          <h2>
            <span>{tableHeading}</span>
          </h2>
          <span
            className={`minus read_more ${tableClass}_togglebtn`}
            attr={tableClass}
            onClick={e => toggleTable(e)}
          ></span>
        </div>
      </div>
      <div className={`col12 info_table ${tableClass}`}>
        <table>
          <tbody>
            <tr>
              <th></th>
              {colData.map(item => {
                return <th>{item}</th>;
              })}
            </tr>
            {tablestats.map((item, index) => {
              return (
                <tr>
                  <td>{rowArr[index]}</td>
                  {colData.map(colItem => {
                    return <td>{item[profileStr][colItem] ? item[profileStr][colItem] : "-"}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
};

export default PlayerStats;
