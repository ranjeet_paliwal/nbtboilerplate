import React, { Component } from "react";

import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { fetchMiniScheduleDataIfNeeded } from "../../../modules/minischedule/actions/minischedule";
import { _getShortMonthName, _getTeamLogo, getTeamColor } from "../../../utils/cricket_util";

import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/MiniSchedule.scss";

import globalconfig from "../../../globalconfig";
import AnchorLink from "../../../components/common/AnchorLink";
import ImageCard from "../../../components/common/ImageCard/ImageCard";
import { _getStaticConfig, checkIsAmpPage, _isCSR } from "../../../utils/util";
import { withRouter } from "react-router";
import { connect } from "react-redux";
const siteConfig = _getStaticConfig();

class ScheduleCard extends Component {
  constructor(props) {
    super(props);
    this.config = {
      scorecardInterval: 30000,
    };
  }

  componentDidMount() {
    const { dispatch, params, query } = this.props;
    if (!globalconfig.scorecardInterval) {
      globalconfig.scorecardInterval = setInterval(() => {
        dispatch(fetchMiniScheduleDataIfNeeded(params, query));
      }, this.config.scorecardInterval);
    }
  }

  componentWillUnmount() {
    let _this = this;
    if (_isCSR()) {
      try {
        clearInterval(globalconfig.scorecardInterval);
      } catch (ex) {}
    }
  }

  scheduleWidget(item) {
    let _this = this;
    let { compType } = this.props;
    let date = new Date(item.matchdate_ist);

    return (
      <React.Fragment>
        {compType == "minischedule" ? (
          <div className="match_date_venue">
            {item.matchnumber} - {_getShortMonthName(date.getMonth()) + " " + date.getDate()}
            <span />
          </div>
        ) : null}

        <div className="liveScore table">
          <div className="table">
            <span className="table_col" />
            <span className="table_col" />
          </div>
          <div className="table">
            <div className="teama table_col">
              {compType == "minischedule" ? <b className="country">{item.teama_short}</b> : null}

              <span className="teaminfo">
                {compType == "minischedule" ? (
                  // <span className={`team_color ${getTeamColor(item.teama_Id)}`}></span>
                  <ImageCard
                    msid={_getTeamLogo(item.teama_Id, "square")}
                    size={item.league == "IPL" ? "squarethumb" : "rectanglethumb"}
                  />
                ) : (
                  <ImageCard
                    msid={_getTeamLogo(item.teama_Id, "square")}
                    size={item.league == "IPL" ? "squarethumb" : "rectanglethumb"}
                  />
                )}
                {compType != "minischedule" ? <b className="country btm">{item.teama_short}</b> : null}
                {compType == "minischedule" ? (
                  <span className="con_wrap">
                    {item.matchtype && item.matchtype.toLowerCase() != "test" ? teamScore("A", item) : null}
                  </span>
                ) : null}
              </span>
            </div>

            {compType == "minischedule" ? (
              <div className="versus table_col">
                <span>VS</span>
              </div>
            ) : (
              <div className="calendar table_col">
                <span className="mt-mumber">{item.matchnumber}</span>
                <span className="mt-date">{`${Number(date.getMonth()) +
                  1}/${date.getDate()}/${date.getFullYear()}`}</span>
              </div>
              //   <div className=""></div>
            )}

            <div className="teamb table_col">
              {compType == "minischedule" ? <b className="country">{item.teamb_short}</b> : null}
              <span className="teaminfo">
                {compType == "minischedule" ? (
                  // <span className={`team_color ${getTeamColor(item.teamb_Id)}`}></span>
                  <ImageCard
                    msid={_getTeamLogo(item.teamb_Id, "square")}
                    size={item.league == "IPL" ? "squarethumb" : "rectanglethumb"}
                  />
                ) : (
                  <ImageCard
                    msid={_getTeamLogo(item.teamb_Id, "square")}
                    size={item.league == "IPL" ? "squarethumb" : "rectanglethumb"}
                  />
                )}
                {compType != "minischedule" ? <b className="country btm">{item.teamb_short}</b> : null}
                {compType == "minischedule" ? (
                  <span className="con_wrap">
                    {item.matchtype && item.matchtype.toLowerCase() != "test" ? teamScore("B", item) : null}
                  </span>
                ) : null}
              </span>
            </div>
          </div>
          {/* match_venue */}
          <div className="match_final_status">
            {(item.live == "1" || item.live == "2" || item.live == "3" || item.live == "4") &&
            compType == "minischedule" ? (
              <span className="status_live">Live</span>
            ) : item.matchresult && item.matchresult != "" ? (
              item.matchresult
            ) : (
              <React.Fragment>
                <span className="match_venue">
                  <span className="time">{`${item.matchtime_ist} IST`}</span> {item.venue}
                </span>
              </React.Fragment>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }

  render() {
    const { schedule, classes, compType, router } = this.props;

    let pathname = router && router.location && router.location.pathname;

    let isAmpPage = checkIsAmpPage(pathname);

    return (
      <ErrorBoundary key="schedule">
        {schedule && typeof schedule != "null" && schedule.length > 0
          ? schedule.map((item, index) => {
              return (
                <li
                  className={`wdt_schedule ${classes} ${
                    compType == "minischedule"
                      ? `minischedule`
                      : `team_${item.teama_Id} team_${item.teamb_Id} venue_${item.venue_Id}`
                  }`}
                  key={index}
                  teama={!isAmpPage ? item.teama_Id : undefined}
                  teamb={!isAmpPage ? item.teamb_Id : undefined}
                  venue={!isAmpPage ? item.venue_Id : undefined}
                >
                  {item.live == 1 || item.matchstatus_Id == "114" ? (
                    <AnchorLink
                      data-type="scorecard"
                      style={{ textDecoration: "none" }}
                      hrefData={{
                        override:
                          siteConfig.mweburl +
                          "/sports/cricket/live-score/" +
                          item.teama_short.toLowerCase() +
                          "-vs-" +
                          item.teamb_short.toLowerCase() +
                          "/" +
                          item.matchdate_ist.replace(/\//g, "-") +
                          "/scoreboard/matchid-" +
                          item.matchfile +
                          ".cms",
                      }}
                    >
                      {this.scheduleWidget(item)}
                    </AnchorLink>
                  ) : (
                    this.scheduleWidget(item)
                  )}
                </li>
              );
            })
          : null}
      </ErrorBoundary>
    );
  }
}

export const teamScore = (team, item) => {
  let inning1 = item && item.inn_team_1 != null && item.inn_team_1 != undefined ? parseInt(item.inn_team_1) : null;
  let inning2 = item && item.inn_team_2 != null && item.inn_team_2 != undefined ? parseInt(item.inn_team_2) : null;
  if (inning1 && team == "A" && inning1 == parseInt(item.teama_Id)) {
    return teamScoreFill(1, item);
  } else if (inning2 && team == "A" && inning2 == parseInt(item.teama_Id)) {
    return teamScoreFill(2, item);
  } else if (inning1 && team == "B" && inning1 == parseInt(item.teamb_Id)) {
    return teamScoreFill(1, item);
  } else if (inning2 && team == "B" && inning2 == parseInt(item.teamb_Id)) {
    return teamScoreFill(2, item);
  }
};

const teamScoreFill = (inning, item) => {
  if (inning == 1) {
    return (
      <React.Fragment>
        <span className="score">{item.inn_score_1.substring(0, item.inn_score_1.indexOf("("))}</span>
        <span className="over">
          {item.inn_score_1.substring(item.inn_score_1.indexOf("(") + 1, item.inn_score_1.length - 1)}
        </span>
      </React.Fragment>
    );
  } else if (inning == 2) {
    return (
      <React.Fragment>
        <span className="score">{item.inn_score_2.substring(0, item.inn_score_2.indexOf("("))}</span>
        <span className="over">
          {item.inn_score_2.substring(item.inn_score_2.indexOf("(") + 1, item.inn_score_2.length - 1)}
        </span>
      </React.Fragment>
    );
  }
};

export default withRouter(connect(null)(ScheduleCard));
