import React, { Component } from "react";
import { connect } from "react-redux";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { fetchListDataIfNeeded } from "../../../../actions/listpage/listpage";
import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import { throttle, scrollTo, updateConfig, _isCSR, hasValue } from "../../../../utils/util";

import { _getStaticConfig } from "../../../../utils/util";
const siteConfig = _getStaticConfig();

import { defaultDesignConfigs } from "../../configs/defaultDesignConfigs";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";

import Ads_module from "../../../../components/lib/ads/index";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import "../../../../components/common/css/ListPage.scss";
import "../../../../components/common/css/desktop/SportsCss.scss";
import KeyWordCard from "../../../../components/common/KeyWordCard";
import AdCard from "../../../../components/common/AdCard";
import { setParentId, setPageType } from "../../../../actions/config/config";
import ScheduleCard from "../../components/schedulecard";
import { fetchMiniScheduleDataIfNeeded } from "../../../../modules/minischedule/actions/minischedule";
import PointsTableCard from "../../components/PointsTableCard";
import { TeamListComp, VenueComp, GetPlayerDetails, PlayerOfTheDay, extractSection } from "../IPL/IPL";
import { fetchTeamDataIfNeeded, fetchVenueDataIfNeeded, fetchTeamDetailDataIfNeeded } from "../../actions/IPL/IPL";
import { extractMiniSchedule } from "../../service";
import PollCard from "../../../../components/common/PollCard";

export class ListPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: null,
      fetchingPlayers: false,
    };
    this.config = {
      componentType: "",
      teamsArr: [],
    };
  }

  componentDidMount() {
    const { dispatch, value, router } = this.props;
    const { query } = this.props.location;
    let { params } = this.props;
    let players;
    if (params && params.msid == undefined) {
      params = this.setParams(params, router);
    }

    let teamobj = { msid: "63489342", teamid: "1109" };

    dispatch(fetchMiniScheduleDataIfNeeded(params, query));

    if (_isCSR()) {
      dispatch(fetchVenueDataIfNeeded(params, query, router));
      dispatch(fetchTeamDataIfNeeded(params, query, router)).then(data => {
        this.setTeamData(data);

        teamobj = this.config.teamsArr && this.config.teamsArr.length > 0 ? this.config.teamsArr[0].teamdata : {};

        dispatch(fetchTeamDetailDataIfNeeded(params, query, router, teamobj)).then(data => {
          players = data && data.payload && data.payload[0].item && data.payload[0].item.squad;
          this.setState({ players });
        });
      });
    }

    //set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    ListPage.fetchData({ dispatch, query, params, router }).then(data => {
      const pwaMeta = data && data.payload && data.payload[0] && data.payload[0].pwa_meta;
      //attach scroll only when sections is not there in feed
      const { pwa_meta, pg } = this.props.value[0] ? this.props.value[0] : {};
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      }

      if (pwaMeta) {
        updateConfig(pwaMeta, dispatch, setParentId);
      }
    });

    if (value && value[0] && value[0].pwa_meta) {
      updateConfig(value && value[0] && value[0].pwa_meta, dispatch, setParentId);
    }
  }

  setParams(params, router) {
    // console.log("params, router------", params, router);
    let pathname = router.location.pathname;
    if (pathname.indexOf("/iplt20.cms") > -1) {
      params.msid = siteConfig.pages.iplHomePage;
    }
    return params;
  }

  setTeamData(data) {
    let teamlist = data && data.payload && data.payload[0];
    let teams = teamlist && teamlist.teams;
    // let teamsObj = [];
    teams &&
      teams.map(team => {
        this.config.teamsArr.push({
          teamname: team.name,
          teamdata: { msid: team.msid, teamid: team.tid },
        });
      });
  }
  generateListingLink(type) {
    let props = this.props;

    if (props.location && props.location.pathname && props.value[0] && props.value[0].pg && props.value[0].pg.cp) {
      let curpg = type == "moreButton" ? parseInt(props.value[0].pg.cp) + 1 : parseInt(props.value[0].pg.cp);
      if (props.location.pathname.indexOf("curpg") > -1) {
        let reExp = /curpg=\\d+/;
        return props.location.pathname.replace(reExp, "curpg=" + curpg);
      } else
        return props.location.pathname + ((props.location.pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params, router } = nextProps;
      const { query } = nextProps.location;
      try {
        ListPage.fetchData({ dispatch, query, params, router }).then(data => {
          let _data = data && data.payload ? data.payload[0] : {};
          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
          if (_data && _data.pwa_meta) {
            updateConfig(_data && _data.pwa_meta, dispatch, setParentId);
            Ads_module.setSectionDetail(_data.pwa_meta);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  /*Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({
    dataObj,
    datalabel,
    configlabel,
    gridclasses,
    override,
    sectionId,
    weblink,
    headingTag,
    compType,
    sectionHeading,
    pageH1Value,
    isSectionHead = true, // this is false if section head is not required
  }) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    let sectionhead = sectionHeading || (dataObj && dataObj.secname) || (dataObj && dataObj.name) || undefined;
    override = override ? override : {};
    let secId = sectionId || (dataObj[datalabel] && dataObj[datalabel].id);

    const { header } = this.props;
    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference

    try {
      if (dataObj && dataObj.items && !Array.isArray(dataObj.items)) {
        let arrObj = [];
        arrObj.push(dataObj.items);
        dataObj.items = arrObj.slice();
      }

      return dataObj && dataObj.items ? (
        <div className={gridclasses + " box-item row"}>
          {isSectionHead && sectionhead && (
            <ErrorBoundary>
              <SectionHeader
                data={secId && headerCopy ? headerCopy.alaskaData : undefined}
                sectionId={secId}
                sectionhead={sectionhead}
                weblink={weblink}
                headingTag={headingTag}
                compType={compType}
                pageH1Value={pageH1Value}
              />
            </ErrorBoundary>
          )}

          <GridSectionMaker
            type={defaultDesignConfigs[configlabel]}
            data={dataObj.items}
            override={override}
            // imgsize={imgsize}
            compType={compType}
          />
        </div>
      ) : // <FakeListing />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      //Reset section window object
      Ads_module.setSectionDetail();
    }
  }

  getDataList(section, noOfEle, startIndex = 0) {
    if (section) {
      let tmpSection = JSON.parse(JSON.stringify(section));
      if (tmpSection["items"] && tmpSection["items"].length > 0) {
        if (noOfEle > 0) {
          tmpSection["items"] = tmpSection["items"].splice(startIndex, noOfEle);
        } else {
          tmpSection["items"] = tmpSection["items"].splice(startIndex);
        }
      }
      return tmpSection;
    }
  }

  onTeamChange = e => {
    const { params, query, router, dispatch } = this.props;
    //  const { players } = this.state;
    let players;
    let items;
    let selectedTeam, teamsArr;
    let teamobj = { msid: "63489342", teamid: "1111" };

    teamsArr = this.config.teamsArr;
    selectedTeam = e.target.value;
    for (let i = 0; i < teamsArr.length; i++) {
      if (teamsArr[i].teamname == selectedTeam) {
        teamobj = teamsArr[i].teamdata;
        break;
      }
    }
    this.setState({ fetchingPlayers: true });
    dispatch(fetchTeamDetailDataIfNeeded(params, query, router, teamobj)).then(data => {
      players = data && data.payload && data.payload[0].item && data.payload[0].item.squad;
      this.setState({ players, fetchingPlayers: false });
    });
  };

  render() {
    let { isFetching, params, teamlist, venuelist, teamDetail, minischedule } = this.props;
    let { sections, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";

    this.config.componentType = parentSection ? parentSection.tn : "";
    let compType = this.config.componentType;
    let { players, fetchingPlayers } = this.state;

    let trendingItems =
      parentSection.recommended && parentSection.recommended.trending && parentSection.recommended.trending.items
        ? parentSection.recommended.trending.items
        : undefined;

    let extrctSchedule = extractMiniSchedule(minischedule);
    const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
    const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
    const pageH1Value = pageheading + " " + pageheadingSlug;

    return this.props.value[0] && (!isFetching || this.config.isFetchingNext) ? (
      <div
        className={`body-content ${compType} ${sections != undefined ? "L1" : "L2"}`}
        {...SeoSchema({ pagetype: compType }).attr().itemList}
      >
        {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
        {parentSection.items && parentSection.items.length > 0
          ? SeoSchema().metaTag({
              name: "numberOfItems",
              content: parentSection.items.filter(item => {
                return item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")
                  ? true
                  : false;
              }).length,
            })
          : null}
        {parentSection.pwa_meta
          ? SeoSchema().metaTag({
              name: "url",
              content: parentSection.pwa_meta.canonical,
            })
          : null}

        {
          <div className="mini_schedule">
            <ul className="view-horizontal">
              <ScheduleCard compType={"minischedule"} schedule={extrctSchedule && extrctSchedule.schedule} />
            </ul>
          </div>
        }

        {/* To show Top section of VL/PL/AL in L1 */}
        <TopSection
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          pageH1Value={pageH1Value}
          createSectionLayout={this.createSectionLayout.bind(this)}
        />

        <div className="row box-item" data-exclude="amp">
          <div className="col12 wdt_teams hp">
            {_isCSR() && teamlist ? (
              <React.Fragment>
                <SectionHeader
                  sectionhead={(teamlist.secmeta && teamlist.secmeta.secname) || "IPL Teams"}
                  weblink={teamlist.secmeta && teamlist.secmeta.override}
                />
                <TeamListComp teamlist={teamlist} />
              </React.Fragment>
            ) : null}
          </div>
        </div>
        <div className="row box-item" data-exclude="amp">
          <div className="wdt_venues hp">
            {venuelist && venuelist.items && venuelist.items.length > 0 ? (
              <React.Fragment>
                <SectionHeader
                  sectionhead={(venuelist.secmeta && venuelist.secmeta.secname) || "Stadium"}
                  weblink={venuelist.secmeta && venuelist.secmeta.override}
                />
                <div className="list-slider">
                  <ul className="col12 view-horizontal">
                    <VenueComp
                      items={venuelist.items}
                      type="slider"
                      isMobilePlatform={true}
                      classes={"news-card vertical"}
                    />
                  </ul>
                </div>
              </React.Fragment>
            ) : null}
          </div>
        </div>

        <div className="row wdt_players box-item" data-exclude="amp">
          <SectionHeader
            sectionhead={siteConfig.locale.players || "Players"}
            weblink={"#" || (teamDetail.secmeta && teamDetail.secmeta.override)}
          />
          <div className="col12">
            <div className="matchNvenue">
              <select id="teamSelect" onChange={this.onTeamChange}>
                {this.config.teamsArr.map((item, index) => {
                  return (
                    <option value={item.teamname} key={"team" + index}>
                      {item.teamname}
                    </option>
                  );
                })}
              </select>
            </div>
            {players && !fetchingPlayers ? (
              <div className="list-slider">
                <ul className="col12 view-horizontal">
                  <GetPlayerDetails items={players} />
                </ul>
              </div>
            ) : null}
          </div>
        </div>

        {/**
         * For rendering Articlelist sections in L1/L2.
         */}
        <ArticleListSections
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          getDataList={this.getDataList.bind(this)}
          createSectionLayout={this.createSectionLayout.bind(this)}
        />

        {hasValue(parentSection, "recommended.trendingnow.0.items") ? (
          <div className="row box-item">
            <div className="col12 list-trending-now">
              <KeyWordCard
                items={parentSection.recommended.trendingnow[0].items}
                secname={parentSection.recommended.trendingnow[0].secname}
              />
            </div>
          </div>
        ) : null}

        {/* To Display trending section and next Item section for VL/PL */}
        {/* Below code is used for next items and trending section in L1/L2 */}
        {trendingItems ? (
          <RecommendedSec
            recommendedSec={parentSection.recommended.trending}
            createSectionLayout={this.createSectionLayout.bind(this)}
            compType={compType}
            istrendingSec="true"
          />
        ) : null}

        {isFetching ? <FakeListing showImages={true} /> : null}

        {this.props &&
        this.props.value &&
        this.props.value[0] &&
        this.props.value[0].pwa_meta &&
        this.props.value[0].pwa_meta.syn != undefined &&
        typeof this.props.value[0].pwa_meta.syn == "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{this.props.value[0].pwa_meta.syn}</div>
          </ErrorBoundary>
        ) : null}

        {/* Breadcrumb  */}
        {parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

ListPage.fetchData = function({ dispatch, query, params, router }) {
  dispatch(setPageType("articlelist"));
  let pathname = router.location.pathname;
  if (pathname.indexOf("/iplt20.cms") > -1) {
    params.msid = siteConfig.pages.iplHomePage;
  }

  dispatch(fetchMiniScheduleDataIfNeeded(params, query));

  return dispatch(fetchListDataIfNeeded(params, query, router)).then(data => {
    let pwaMeta;
    if (data) {
      if (Array.isArray(data.payload)) {
        pwaMeta = data.payload.length > 0 && data.payload[0].pwa_meta;
      } else {
        pwaMeta = data.payload && data.payload.pwa_meta;
      }
    }

    if (pwaMeta) {
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

function mapStateToProps(state) {
  return {
    ...state.articlelist,
    venuelist: state.IPL && state.IPL.venuelist,
    minischedule: state.minischedule,
    teamlist: state.IPL && state.IPL.teamlist,
  };
}

const TopSection = props => {
  let { compType, parentSection, sections, createSectionLayout, pageH1Value } = props;
  return parentSection && parentSection.items && sections != undefined
    ? createSectionLayout({
        dataObj: parentSection,
        configlabel: "gridHorizontalAL",
        gridclasses: "",
        headingTag: "h1",
        compType: compType,
        pageH1Value: pageH1Value,
      })
    : null;
};

const sectionType = item => {
  let sectionType = "";
  if (item.seolocation.indexOf("iplt20/news") > -1) {
    sectionType = "news";
  } else if (item.seolocation.indexOf("iplt20/teams") > -1) {
    sectionType = "teams";
  } else if (item.seolocation.indexOf("iplt20/player-of-the-day") > -1) {
    sectionType = "pod";
  } else if (item.seolocation.indexOf("iplt20/points-table") > -1) {
    sectionType = "pointstable";
  } else if (item.seolocation.indexOf("iplt20/venues") > -1) {
    sectionType = "venues";
  } else if (item.seolocation.indexOf("iplt20/photos") > -1) {
    sectionType = "photos";
  } else if (item.seolocation.indexOf("iplt20/video") > -1) {
    sectionType = "videos";
  }

  return sectionType;
};

const RecommendedSec = props => {
  let { recommendedSec, createSectionLayout, compType, istrendingSec } = props;
  // console.log("recommendedSec", recommendedSec);
  return createSectionLayout({
    dataObj: recommendedSec,
    configlabel: "gridView",
    compType: compType,
    gridclasses: "wdt_next_sections",
    override: {
      type: "vertical",
      className: "col12 view-horizontal",
      imgsize: "midthumb",
      istrending: istrendingSec,
    },
  });
};

const ArticleListSections = props => {
  let { compType, parentSection, sections, getDataList, createSectionLayout } = props;

  const podSection = extractSection(sections, undefined, "pod");
  let newsSec = extractSection(sections, "iplt20/news");
  newsSec = getDataList(newsSec, 10);
  let photoSec = extractSection(sections, undefined, "photolist");
  photoSec = getDataList(photoSec, 10);
  let videoSec = extractSection(sections, undefined, "videolist");
  videoSec = getDataList(videoSec, 10);
  let pollSec = extractSection(sections, undefined, "poll");
  let pollItem = hasValue(pollSec, "items.0") ? pollSec.items[0] : null;

  return compType == "articlelist" && parentSection && sections != undefined ? (
    <React.Fragment>
      <React.Fragment>
        <AdCard mstype="mrec1" adtype="dfp" />
        {_isCSR() ? <PointsTableCard compType={"pointtablecard"} /> : null}
      </React.Fragment>

      <PlayerOfTheDay section={podSection} />
      {pollItem ? (
        <div className="row box-item wdt_sports_wap" data-exclude="amp">
          <SectionHeader sectionhead={"Poll"} />
          <div className="col12">
            <PollCard data={pollItem} />
          </div>
        </div>
      ) : null}
      <ListSections createSectionLayout={createSectionLayout} section={newsSec} />
      <ListSections createSectionLayout={createSectionLayout} section={photoSec} />
      <ListSections createSectionLayout={createSectionLayout} section={videoSec} />
    </React.Fragment>
  ) : null;
};

const ListSections = props => {
  const { section, createSectionLayout } = props;

  if (!section) {
    return null;
  }
  return createSectionLayout({
    dataObj: section,
    configlabel:
      section.tn == "articlelist"
        ? "listGallery"
        : section.tn == "photolist"
        ? "gridHorizontalLeadAL"
        : section.tn == "videolist"
        ? "gallery"
        : "listGallery", // here photolist/videolist means photo/video listing which are marked as special under articlelisting
    weblink: section.override || section.wu,
    gridclasses:
      section.tn == "photolist"
        ? "photo_video_section"
        : section.tn == "videolist"
        ? "photo_video_section video"
        : undefined,
  });
};

export default connect(mapStateToProps)(ListPage);
