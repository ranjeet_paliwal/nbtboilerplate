import React, { Component } from "react";
import { connect } from "react-redux";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import FakeDesktopDefault from "../../../../components/common/FakeCards/FakeDesktopDefault";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { scrollTo, _getStaticConfig, _isCSR, updateConfig, isMobilePlatform, hasValue } from "../../../../utils/util";
import "../../../../components/common/css/commonComponents.scss";
import "../../../../components/common/css/Desktop.scss";
import "../../../../components/common/css/desktop/SectionWrapper.scss";

const siteConfig = _getStaticConfig();

import Ads_module from "../../../../components/lib/ads/index";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";

import { setParentId, setPageType } from "../../../../actions/config/config";
import { fetchTeamsDataIfNeeded, fetchIPLDataIfNeeded } from "../../actions/IPL/IPL";
import ListComponent from "../../components/ListComponent";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import AdCard from "../../../../components/common/AdCard";
import { fetchMiniScheduleDataIfNeeded } from "../../../../modules/minischedule/actions/minischedule";
import ScheduleCard from "../../components/schedulecard";
import Slider from "../../../../components/desktop/Slider";
import PointsTableCard from "../../components/PointsTableCard";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import { designConfigs } from "../../configs/designConfigs";
import AnchorLink from "../../../../components/common/AnchorLink";
import { _getTeamLogo } from "../../../../utils/cricket_util";
import { extractMiniSchedule } from "../../service";

export class IPL extends Component {
  constructor(props) {
    super(props);
    this.config = {
      componentType: "",
    };
  }

  componentDidMount() {
    const { dispatch, value, router } = this.props;
    const { query, pathname } = this.props.location;
    let { params } = this.props;
    
    if (params && params.msid == undefined) {
      params = this.setParams(params, router);
    }

    //set section & subsection for first time
    // if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    IPL.fetchData({ dispatch, query, params, router }).then(data => {
      let pwa_meta;
      if (pathname.indexOf("/pointstable/") > -1) {
        pwa_meta = hasValue(this.props, "pointstable.value.0.pwa_meta") ? this.props.pointstable.value[0].pwa_meta : {};
      } else if (pathname.indexOf("/teams/") > -1 && this.props.teamlist && this.props.teamlist.secmeta) {
        pwa_meta = this.props.teamlist.secmeta.pwa_meta;
      } else if (pathname.indexOf("/venues/") > -1 && this.props.venuelist && this.props.venuelist.secmeta) {
        pwa_meta = this.props.venuelist.secmeta.pwa_meta;
      }

      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });
  }

  componentDidUpdate() {
    const { value, dispatch } = this.props;
    const { pathname } = this.props.location;
    let pwa_meta;
    if (pathname.indexOf("/pointstable/") > -1) {
      pwa_meta = hasValue(this.props, "pointstable.value.0.pwa_meta") ? this.props.pointstable.value[0].pwa_meta : {};
    } else if (pathname.indexOf("/teams/") > -1 && this.props.teamlist && this.props.teamlist.secmeta) {
      pwa_meta = this.props.teamlist.secmeta.pwa_meta;
    } else if (pathname.indexOf("/venues/") > -1 && this.props.venuelist && this.props.venuelist.secmeta) {
      pwa_meta = this.props.venuelist.secmeta.pwa_meta;
    }

    if (pwa_meta) {
      updateConfig(pwa_meta, dispatch, setParentId);
    }
  }

  setParams(params, router) {
    let pathname = router.location.pathname;
    if (pathname.indexOf("/iplt20.cms") > -1) {
      params.msid = siteConfig.pages.iplHomePage;
    }
    return params;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params, router } = nextProps;
      const { query } = nextProps.location;
      try {
        IPL.fetchData({ dispatch, query, params, router }).then(data => {
          let _data = data && data.payload ? data.payload[0] : {};
          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
          if (_data && _data.pwa_meta) {
            Ads_module.setSectionDetail(_data.pwa_meta);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    //Reset section window object
    Ads_module.setSectionDetail();
  }

  /*Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({
    dataObj,
    datalabel,
    configlabel,
    gridclasses,
    override,
    imgsize,
    sectionId,
    weblink,
    isSectionHead,
    compType,
    sectionHeading,
    noSeo,
  }) {
    const { header } = this.props;
    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference

    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    let sectionhead;
    sectionhead = sectionHeading || dataObj ? dataObj.secname : undefined;
    sectionhead = isSectionHead == false ? undefined : sectionhead;
    override = override ? override : {};

    try {
      // if (dataObj && dataObj.items) {
      //   if (!Array.isArray(dataObj.items)) {
      //     let arrObj = [];
      //     arrObj.push(dataObj.items);
      //     dataObj.items = arrObj.slice();
      //   }
      // }

      if (dataObj) {
        dataObj.map(item => (item["tn"] = "photo"));
      }

      return dataObj && dataObj.length > 0 ? (
        <React.Fragment>
          {sectionhead && (
            <SectionHeader
              sectionhead={sectionhead}
              weblink={dataObj && (dataObj.override || dataObj.wu)}
              sectionTag={dataObj && dataObj.tag ? dataObj.tag : ""}
              compType={compType}
            />
          )}

          <GridSectionMaker
            type={designConfigs[configlabel]}
            data={dataObj}
            override={override}
            compType={compType}
            noSeo={noSeo}
          />
        </React.Fragment>
      ) : // <FakeDesktopDefault />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    let {
      isFetching,
      params,
      minischedule,
      pointstable,
      teamlist,
      teamDetail,
      venuelist,
      ...parentSection
    } = this.props;
    const { query, pathname } = this.props.location;
    let widget, breadcrumb, pwa_meta;

    let extrctSchedule = extractMiniSchedule(minischedule);

    if (pathname.indexOf("/pointstable/") > -1) {
      pwa_meta = hasValue(this.props, "pointstable.value.0.pwa_meta") ? this.props.pointstable.value[0].pwa_meta : {};
      widget = <PointstableWidget value={pointstable.value} isMobilePlatform={isMobilePlatform()} />;
      breadcrumb = pointstable.value && pointstable.value[0] && pointstable.value[0].breadcrumb;
    } else if (pathname.indexOf("/teams/") > -1) {
      pwa_meta =
        this.props.teamlist && this.props.teamlist.secmeta && this.props.teamlist.secmeta.pwa_meta
          ? this.props.teamlist.secmeta.pwa_meta
          : {};
      widget = <TeamLandingComp teamlist={teamlist} isMobilePlatform={isMobilePlatform()} />;
      breadcrumb = teamlist && teamlist.secmeta && teamlist.secmeta.breadcrumb;
    } else if (pathname.indexOf("/team/") > -1) {
      widget = <GetPlayerDetails teamDetail={teamDetail} isMobilePlatform={isMobilePlatform()} />;
      breadcrumb = teamlist && teamlist.secmeta && teamlist.secmeta.breadcrumb;
    } else if (pathname.indexOf("/venues/") > -1) {
      pwa_meta =
        this.props.venuelist && this.props.venuelist.secmeta && this.props.venuelist.secmeta.pwa_meta
          ? this.props.venuelist.secmeta.pwa_meta
          : {};
      breadcrumb = venuelist && venuelist.secmeta && venuelist.secmeta.breadcrumb;
      widget = venuelist && venuelist.items && (
        <div className={`row wdt_venues ${isMobilePlatform() ? "box-item" : "sports-section"}`}>
          <VenueComp
            items={venuelist.items}
            type="listing"
            isMobilePlatform={isMobilePlatform()}
            secmeta={venuelist.secmeta}
          />
        </div>
      );
    }

    this.config.componentType = parentSection ? parentSection.tn : "";
    let compType = this.config.componentType;

    return (teamlist || (pointstable && pointstable.value)) && !isFetching ? (
      <div className={`body-content`} {...SeoSchema({ pagetype: compType }).attr().itemList}>
        {/* For SEO Meta */ pwa_meta ? PageMeta(pwa_meta) : null}
        {parentSection.items && parentSection.items.length > 0
          ? SeoSchema().metaTag({
              name: "numberOfItems",
              content: parentSection.items.filter(item => {
                return item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")
                  ? true
                  : false;
              }).length,
            })
          : null}
        {pwa_meta
          ? SeoSchema().metaTag({
              name: "url",
              content: pwa_meta.canonical,
            })
          : null}
        {/* Breadcrumb  */}
        {!isMobilePlatform() && breadcrumb && breadcrumb.div && breadcrumb.div.ul ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}

        {isMobilePlatform() ? (
          <ErrorBoundary key="schedule">
            <div className="mini_schedule">
              <ul className="view-horizontal">
                <ScheduleCard compType={"minischedule"} schedule={extrctSchedule && extrctSchedule.schedule} />
              </ul>
            </div>
          </ErrorBoundary>
        ) : (
          <ErrorBoundary key="schedule">
            <div className="mini_schedule">
              <Slider
                margin="15"
                size="3"
                sliderData={extrctSchedule && extrctSchedule.schedule}
                width="280"
                type="iplwidgets"
                compType="minischedule"
                sliderClass="grid_slider"
              />
            </div>
          </ErrorBoundary>
        )}

        {widget ? widget : null}

        {<ListComponent query={query} params={params} />}

        {pwa_meta && pwa_meta.syn != undefined && pwa_meta.syn == "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{pwa_meta.syn}</div>
          </ErrorBoundary>
        ) : null}

        {/* To show fake listing until data is data is rendered on screen */}
        {isFetching ? <FakeDesktopDefault showImages={true} /> : null}

        {isMobilePlatform() && breadcrumb && breadcrumb.div && breadcrumb.div.ul ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
      </div>
    ) : (
      <FakeDesktopDefault />
    );
  }
}

IPL.fetchData = function({ dispatch, query, params, router }) {
  dispatch(setPageType("articlelist"));
  dispatch(fetchMiniScheduleDataIfNeeded(params, query));
  return dispatch(fetchIPLDataIfNeeded(params, query, router)).then(data => {
    let pwaMeta;
    if (data) {
      if (Array.isArray(data.payload)) {
        pwaMeta = data.payload.length > 0 && data.payload[0].pwa_meta;
      } else {
        pwaMeta = data.payload && data.payload.pwa_meta;
      }
    }

    if (pwaMeta) {
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

function mapStateToProps(state) {
  return {
    ...state.IPL,
    minischedule: state.minischedule,
    pointstable: state.pointstable,
    header: state.header,
  };
}

const TeamLandingComp = props => {
  let { teamlist, isMobilePlatform } = props;

  return (
    <div className={!isMobilePlatform ? "row sports-section" : "row box-item"}>
      <div className={!isMobilePlatform ? "col8 wdt_teams" : "col12 wdt_teams"}>
        {/* this need to be dynamic through section header */}
        <SectionHeader
          sectionhead={(teamlist && teamlist.secmeta && teamlist.secmeta.secname) || "IPL Teams"}
          headingTag="h1"
        />

        <TeamListComp teamlist={teamlist} />
      </div>
      {!isMobilePlatform ? (
        <div className="col4 rhs-widget">
          <AdCard mstype="mrec1" adtype="dfp" className="box-item" />
        </div>
      ) : null}
    </div>
  );
};

export const GetPlayerDetails = props => {
  const { items, classes, style } = props;
  // const items = teamDetail && teamDetail.item && teamDetail.item.squad ? teamDetail.item.squad : null;

  return items && items.length > 0
    ? items.map(item => {
        return (
          <li className={classes} style={style}>
            <AnchorLink href={`/sports/cricketer/${item.englishname ? item.englishname : ""}`}>
              <span className="img_wrap">
                <ImageCard type={"absoluteImgSrc"} src={item.image} size="smallthumb" />
              </span>
              <span className="con_wrap">
                <span className="text_ellipsis">{item.name}</span>
              </span>
            </AnchorLink>
          </li>
        );
      })
    : null;
};

export const VenueComp = props => {
  const { items, type, isMobilePlatform, classes, style, secmeta } = props;

  // let venueItems = venuelist && venuelist.items && venuelist.items.length > 0 ? venuelist.items : null;
  let TopVenues = items ? items.slice(0, 2) : null;
  let BottomVenues = items ? items.slice(2) : null;

  return items ? (
    type == "slider" ? (
      items.map((venue, index) => {
        return (
          <React.Fragment>
            <li className={classes} style={style}>
              <AnchorLink href={venue.link}>
                <span className="img_wrap">
                  <ImageCard msid={venue.msid} size="smallthumb" />
                </span>
                <span className="con_wrap">
                  <span className="text_ellipsis">{venue.name}</span>
                </span>
              </AnchorLink>
            </li>
          </React.Fragment>
        );
      })
    ) : type == "listing" ? (
      <React.Fragment>
        <div className={!isMobilePlatform ? "col8 pd0" : "col12 pd0"}>
          <SectionHeader sectionhead={(secmeta && secmeta.secname) || "Stadium"} headingTag="h1" />
          <ul className="col12">
            {TopVenues.map(venue => (
              <li className="news-card col6">
                <AnchorLink href={venue.link}>
                  <span className="img_wrap">
                    <ImageCard msid={venue.msid} size="smallthumb" />
                  </span>
                  <span className="con_wrap">
                    <span className="text_ellipsis">{venue.name}</span>
                  </span>
                </AnchorLink>
              </li>
            ))}
          </ul>
        </div>
        {!isMobilePlatform ? (
          <div className="col4 rhs-widget">
            <AdCard mstype="mrec2" adtype="dfp" className="box-item" />
          </div>
        ) : null}
        <ul className="col12">
          {BottomVenues.map(venue => (
            <li className={`news-card ${!isMobilePlatform ? "col4" : "col6"}`}>
              <AnchorLink href={venue.link}>
                <span className="img_wrap">
                  <ImageCard msid={venue.msid} size="smallthumb" />
                </span>
                <span className="con_wrap">
                  <span className="text_ellipsis">{venue.name}</span>
                </span>
              </AnchorLink>
            </li>
          ))}
        </ul>
      </React.Fragment>
    ) : null
  ) : null;
};

export const TeamListComp = props => {
  const { teamlist } = props;
  let teams = teamlist && teamlist.teams;
  return (
    <ul>
      {teams
        ? teams.map(item => {
            return (
              <li>
                <AnchorLink href={item.link}>
                  <ImageCard msid={_getTeamLogo(item.tid, "square")} size="squarethumb" />
                  <span className="name">{item.name}</span>
                </AnchorLink>
              </li>
            );
          })
        : null}
    </ul>
  );
};

export const PlayerOfTheDay = props => {
  let { section } = props;

  let item = hasValue(section, "items.0") ? section.items[0] : null;
  return item ? (
    <div className="playerOftheDay">
      <AnchorLink href={section.wu ? section.wu : ""}>
        <ImageCard msid={item.imageid ? item.imageid : item.id} size="smallthumb" />
        <div className="rgt">
          <h4>{section.secname ? section.secname : "Player of the Day"}</h4>
          <span className="player_name">
            <span className="text_ellipsis">{item.hl ? item.hl : null}</span>
          </span>
        </div>
        <span className="btm-bg"></span>
      </AnchorLink>
    </div>
  ) : null;
};

export const extractSection = (sections, seoloc, tn) => {
  if (sections && sections.length > 0) {
    for (let i = 0; i < sections.length; i++) {
      if ((tn && sections[i].tn === tn) || (seoloc && sections[i].seolocation.indexOf(seoloc) > -1)) {
        return sections[i];
      }
    }
  }
};

const PointstableWidget = props => {
  const { value, isMobilePlatform } = props;

  return isMobilePlatform ? (
    <ErrorBoundary key="PointsTable">
      <PointsTableCard value={value} />
    </ErrorBoundary>
  ) : (
    <div className="row col12 sports-section">
      <div className="col8">
        <ErrorBoundary key="PointsTable">
          <PointsTableCard value={value} />
        </ErrorBoundary>
      </div>
      <div className="col4 rhs-widget">
        <AdCard mstype="mrec1" adtype="dfp" className="box-item" />
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(IPL);
