import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../../components/common/Breadcrumb";
import { fetchScheduleDataIfNeeded } from "../../../../actions/schedule/schedule";

import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { scrollTo, _isCSR, isMobilePlatform, updateConfig, checkIsAmpPage } from "../../../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../../../utils/cricket_util";

import styles from "../../../../components/common/css/commonComponents.scss";

// import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta"; //For Page SEO/Head Part
import AnchorLink from "../../../../components/common/AnchorLink";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import AdCard from "../../../../components/common/AdCard";
import Ads_module from "../../../../components/lib/ads/index";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { setPageType, setParentId } from "../../../../actions/config/config";

import { _getStaticConfig } from "../../../../utils/util";
import ScheduleCard from "../../components/schedulecard";
import { fetchMiniScheduleDataIfNeeded } from "../../../../modules/minischedule/actions/minischedule";
import Slider from "../../../../components/desktop/Slider";
import { extractMiniSchedule } from "../../service";
const siteConfig = _getStaticConfig();

export class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBind: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params, value, router } = this.props;
    const { query, pathname } = this.props.location;
    dispatch(setPageType("schedule"));
    //set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);

    if (
      this.props.value &&
      this.props.value[0] &&
      this.props.value[0].pwa_meta &&
      this.props.value[0].pwa_meta.cricketlb
    ) {
      params.seriesid = this.props.value[0].pwa_meta.cricketlb;
      pathname.indexOf("/sportsresult/") > -1 ? (params.pastMatches = true) : false;
    }

    Schedule.fetchData({ dispatch, query, params, router }).then(data => {
      //attach scroll only when sections is not there in feed
      if (typeof this.props.value[0] != "undefined" && !this.props.value[0].sections) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
      // if (pwa_meta && typeof pwa_meta == "object") Ads_module.setSectionDetail(pwa_meta);
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        // pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    // if (_isCSR() && params.seriesid && params.seriesid != "") {
    //   dispatch(fetchMiniScheduleDataIfNeeded(params)); //Load Schedule Widget
    // }

    //reset slect box
    if (document.getElementById("TeamsSelect")) {
      document.getElementById("TeamsSelect").selectedIndex = "0";
    }
    if (document.getElementById("VenuesSelect")) {
      document.getElementById("VenuesSelect").selectedIndex = "0";
    }
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      // this.scrollHandler = throttle(this.handleScroll.bind(_this));
      //window.addEventListener('scroll', this.scrollHandler);
      // this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params, router } = nextProps;
      const { query } = nextProps.location;
      try {
        Schedule.fetchData({ dispatch, query, params, router }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();

          if (!data.payload[0].sections) {
            this.scrollBind();
          } else {
            this.scrollUnbind();
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
      //reset slect box
      if (document.getElementById("TeamsSelect")) {
        document.getElementById("TeamsSelect").selectedIndex = "0";
      }
      if (document.getElementById("VenuesSelect")) {
        document.getElementById("VenuesSelect").selectedIndex = "0";
      }
      // let navigations = document.querySelector("#paginationHomePage").children;
      // if(navigations.length>0){
      //   for(let nav of navigations){
      //     try{
      //       nav.removeEventListener('click',this.handleNavigationClick, true);
      //     }catch(ex){}
      //     nav.classList.remove("active");
      //   }
      // }
      // document.querySelector(".mymenu_slide") ? (document.querySelector(".mymenu_slide").scrollLeft = 0) : '';
    }
  }
  handlefilterteam = event => {
    document.querySelector(".schedule_container").setAttribute("team", event.target.value);
    this.filter(event.target.value);
  };
  handlefiltervenue = event => {
    document.querySelector(".schedule_container").setAttribute("venue", event.target.value);
    this.filter(event.target.value);
  };
  filter(value) {
    let filter_team = document.querySelector(".schedule_container").getAttribute("team");
    let filter_venue = document.querySelector(".schedule_container").getAttribute("venue");
    let allschedule = document.querySelectorAll(".wdt_schedule:not(.minischedule)");
    let recordexist = false;
    if (filter_team == "" && filter_venue == "") {
      let i;
      for (i = 0; i < allschedule.length; i++) {
        allschedule[i].style.display = "block";
      }
    } else {
      //hide all object show filter content
      let i;

      for (i = 0; i < allschedule.length; i++) {
        allschedule[i].style.display = "none";
      }

      if (filter_team != "" && filter_venue != "") {
        for (i = 0; i < allschedule.length; i++) {
          if (
            (allschedule[i].getAttribute("teama") == filter_team ||
              allschedule[i].getAttribute("teamb") == filter_team) &&
            allschedule[i].getAttribute("venue") == filter_venue
          ) {
            allschedule[i].style.display = "block";
            recordexist = true;
          }
        }
      } else if (filter_team != "") {
        let filterteam = document.querySelectorAll(`.team_${filter_team}`);
        let ii;
        for (ii = 0; ii < filterteam.length; ii++) {
          filterteam[ii].style.display = "block";
          recordexist = true;
        }
      } else if (filter_venue != "") {
        let filtervenue = document.querySelectorAll(`.venue_${filter_venue}`);
        let ii;
        for (ii = 0; ii < filtervenue.length; ii++) {
          filtervenue[ii].style.display = "block";
          recordexist = true;
        }
      }
      if (recordexist) document.querySelector(".no-record-found").style.display = "none";
      else document.querySelector(".no-record-found").style.display = "block";
    }
  }

  render() {
    const pagetype = "schedule";
    let _this = this;
    const { query } = this.props.location;
    const { router } = this.props;
    let date = new Date(new Date().toJSON().slice(0, 10));
    let { isFetching, params, minischedule } = this.props;
    let { sections, subsec1, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    let schedule =
      this.props.value[1] && this.props.value[1].data && this.props.value[1].data.matches
        ? this.props.value[1].data.matches
        : null;
    if (schedule && this.props.location.pathname.indexOf("/sportsresult/") > -1) {
      let pastmatches = schedule.filter(function(data) {
        return data.matchstatus_Id == 114;
      });
      schedule = pastmatches;
    }

    if (schedule && this.props.location.pathname.indexOf("/schedule/") > -1) {
      let upcomingMatches = schedule.filter(function(item) {
        let matchdate = new Date(item.matchdate_ist);
        return date.getTime() < matchdate.getTime();
      });
      schedule = upcomingMatches;
    }

    let distinctteams = [];
    let distinctvenues = [];
    if (schedule && typeof schedule != "null" && schedule.length > 0) {
      //filter distinct team list name and id
      let schedulemap = new Map();
      schedule.map((item, index) => {
        if (!schedulemap.has(item.teama_Id) && item.teama_Id != "") {
          schedulemap.set(item.teama_Id, true); // set team A mapping existance
          distinctteams.push({
            id: item.teama_Id,
            name: item.teama,
          });
        }
        if (!schedulemap.has(item.teamb_Id) && item.teamb_Id != "") {
          schedulemap.set(item.teamb_Id, true); // set team B mapping existance
          distinctteams.push({
            id: item.teamb_Id,
            name: item.teamb,
          });
        }
      });

      //filter distinct venue list
      let venuesmap = new Map();
      schedule.map((item, index) => {
        if (item.venue_Id && !venuesmap.has(item.venue_Id) && item.venue_Id != "") {
          venuesmap.set(item.venue_Id, true); // set venue mapping
          distinctvenues.push({
            id: item.venue_Id,
            name: item.venue,
          });
        }
      });
    }

    let extrctSchedule = extractMiniSchedule(minischedule);

    let pathname = router && router.location && router.location.pathname;

    let isAmpPage = checkIsAmpPage(pathname);

    return this.props.value[0] && !isFetching ? (
      <div className="body-content" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(this.props.value[0].pwa_meta)}

        {!isMobilePlatform() && parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}

        {
          /*Cricket Schedule Widget for event based page */
          <React.Fragment>
            {/* {this.props.value[0].pwa_meta.cricketlb && this.props.value[0].pwa_meta.cricketlb != "" ? ( */}
            {
              <ErrorBoundary key="schedule">
                <div className="mini_schedule">
                  {isMobilePlatform() ? (
                    <ul className="view-horizontal">
                      <ScheduleCard compType={"minischedule"} schedule={extrctSchedule && extrctSchedule.schedule} />
                    </ul>
                  ) : (
                    <Slider
                      margin="15"
                      size="3"
                      sliderData={extrctSchedule && extrctSchedule.schedule}
                      width="280"
                      type="iplwidgets"
                      compType="minischedule"
                      sliderClass="grid_slider"
                    />
                  )}
                </div>
              </ErrorBoundary>
              // ) : null
            }
          </React.Fragment>
        }

        {/* TO DO Currently Heading is dependent on parentection items as in case when for a section child section exist but parent does not have ant stories */}
        <div className={`sports-section ${isMobilePlatform() ? "" : "row col12"}`}>
          <div className={`${isMobilePlatform() ? "" : "col8"}`}>
            {parentSection && parentSection.secname ? (
              <div className="schedule_intro">
                <div className="section">
                  <div className="top_section">
                    <h1>
                      <span>
                        {parentSection.pwa_meta && parentSection.pwa_meta.pageheading
                          ? parentSection.pwa_meta.pageheading
                          : parentSection.secname}
                        {parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug
                          ? " " + parentSection.pwa_meta.pageheadingslug
                          : null}
                      </span>
                    </h1>
                  </div>
                </div>
                <div className="matchNvenue">
                  {distinctteams && typeof distinctteams != "null" && distinctteams.length > 0 ? (
                    <select id="VenuesSelect" onChange={this.handlefilterteam.bind(this)} data-exclude="amp">
                      <option value="" selected="selected">
                        All Teams
                      </option>
                      {distinctteams.map((item, index) => {
                        return <option value={item.id}>{item.name}</option>;
                      })}
                    </select>
                  ) : null}
                  {distinctvenues && typeof distinctvenues != "null" && distinctvenues.length > 0 ? (
                    <select id="VenuesSelect" onChange={this.handlefiltervenue.bind(this)} data-exclude="amp">
                      <option value="" selected="selected">
                        All Venues
                      </option>
                      {distinctvenues.map((item, index) => {
                        return <option value={item.id}>{item.name}</option>;
                      })}
                    </select>
                  ) : null}
                </div>
              </div>
            ) : null}

            <ul className="schedule_container" team={isAmpPage ? undefined : ""} venue={isAmpPage ? undefined : ""}>
              <ScheduleCard schedule={schedule} />
            </ul>
            <div className={`no-record-found ${schedule && schedule.length > 0 ? "hide" : ""} `}>No Record Found</div>

            {isFetching ? <FakeListing showImages={true} /> : null}
          </div>

          {!isMobilePlatform() ? (
            <div className="col4 rhs-widget">
              <AdCard mstype="mrec1" adtype="dfp" className="box-item" />
            </div>
          ) : null}
        </div>

        {this.props &&
        this.props.value &&
        this.props.value[0] &&
        this.props.value[0].pwa_meta &&
        this.props.value[0].pwa_meta.seodescription != undefined &&
        typeof this.props.value[0].pwa_meta.seodescription == "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{this.props.value[0].pwa_meta.seodescription}</div>
          </ErrorBoundary>
        ) : null}

        {/* Breadcrumb  */}
        {isMobilePlatform() && parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}
        {/* For SEO Schema */ SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

Schedule.fetchData = function({ dispatch, query, params, router }) {
  dispatch(fetchMiniScheduleDataIfNeeded(params));
  return dispatch(fetchScheduleDataIfNeeded(params, query, router));
};

function mapStateToProps(state) {
  return {
    ...state.schedule,
    minischedule: state.minischedule,
  };
}

export default connect(mapStateToProps)(Schedule);
