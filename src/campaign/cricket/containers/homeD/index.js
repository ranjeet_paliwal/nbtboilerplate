import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded } from "../../../../actions/listpage/listpage";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import FakeDesktopDefault from "../../../../components/common/FakeCards/FakeDesktopDefault";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { hasValue, scrollTo, updateConfig } from "../../../../utils/util";
import "../../../../components/common/css/commonComponents.scss";
import "../../../../components/common/css/Desktop.scss";
import "../../../../components/common/css/desktop/SectionWrapper.scss";

import { _getStaticConfig, _isCSR } from "../../../../utils/util";
const siteConfig = _getStaticConfig();
import { designConfigs } from "../../configs/designConfigs";

import { AnalyticsGA } from "../../../../components/lib/analytics/index";
import Ads_module from "../../../../components/lib/ads/index";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import Slider from "../../../../components/desktop/Slider/index";
// import { SuperhitWidget } from "../../../components/common/SuperhitWidget";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";

import AdCard from "../../../../components/common/AdCard";
import VideoSection from "../../../../components/common/VideoSection/VideoSection";
import KeyWordCard from "../../../../components/common/KeyWordCard";
import { setParentId, setPageType } from "../../../../actions/config/config";
import PointsTableCard from "../../components/PointsTableCard";
import { fetchMiniScheduleDataIfNeeded } from "../../../../modules/minischedule/actions/minischedule";
import { extractSection, PlayerOfTheDay, TeamListComp } from "../IPL/IPL";
import { fetchVenueDataIfNeeded, fetchTeamDataIfNeeded, fetchTeamDetailDataIfNeeded } from "../../actions/IPL/IPL";
import { fetchScheduleDataIfNeeded } from "../../../../actions/schedule/schedule";
import ScheduleCard from "../../components/schedulecard";
import { extractMiniSchedule } from "../../service";
import AnchorLink from "../../../../components/common/AnchorLink";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import PollCard from "../../../../components/common/PollCard";
import "../../../../components/common/css/desktop/SportsCss.scss";

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: null,
      fetchingPlayers: false,
    };
    this.config = {
      componentType: "",
      teamsArr: [],
    };
  }

  componentDidMount() {
    const { dispatch, value, router } = this.props;
    const { query } = this.props.location;
    let { params } = this.props;
    let players;
    const parentId = (value[0] && value[0].pwa_meta && value[0].pwa_meta.parentidnew) || "";
    const subsec1 = (value[0] && value[0].pwa_meta && value[0].pwa_meta.subsec1) || "";

    if (params && params.msid == undefined) {
      params = this.setParams(params, router);
    }

    let teamobj = {};

    if (_isCSR()) {
      dispatch(fetchVenueDataIfNeeded(params, query, router));
      dispatch(fetchTeamDataIfNeeded(params, query, router)).then(data => {
        this.setTeamData(data);

        teamobj = this.config.teamsArr && this.config.teamsArr.length > 0 ? this.config.teamsArr[0].teamdata : {};

        dispatch(fetchTeamDetailDataIfNeeded(params, query, router, teamobj)).then(data => {
          players = data && data.payload && data.payload[0].item && data.payload[0].item.squad;
          this.setState({ players });
        });
      });

      dispatch(fetchScheduleDataIfNeeded(params, query));
    }

    //set section & subsection for first time
    // if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    HomePage.fetchData({ dispatch, query, params, router }).then(data => {
      const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};

      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });
  }

  setTeamData(data) {
    let teamlist = data && data.payload && data.payload[0];
    if (!teamlist) {
      teamlist = this.props && this.props.teamlist;
    }
    let teams = teamlist && teamlist.teams;
    // let teamsObj = [];
    teams &&
      teams.map(team => {
        this.config.teamsArr.push({
          teamname: team.name,
          teamdata: { msid: team.msid, teamid: team.tid },
        });
      });
  }

  componentDidUpdate() {
    const { value, dispatch } = this.props;
    if (value && value[0] && value[0].pwa_meta) {
      const pwaMeta = { ...value[0].pwa_meta };
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  }

  generateListingLink(type) {
    let props = this.props;

    if (props.location && props.location.pathname && props.value[0] && props.value[0].pg && props.value[0].pg.cp) {
      let curpg = type == "moreButton" ? parseInt(props.value[0].pg.cp) + 1 : parseInt(props.value[0].pg.cp);
      if (props.location.pathname.indexOf("curpg") > -1) {
        let reExp = /curpg=\\d+/;
        return props.location.pathname.replace(reExp, "curpg=" + curpg);
      } else
        return props.location.pathname + ((props.location.pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
    }
  }

  setParams(params, router) {
    // console.log("params, router------", params, router);
    let pathname = router.location.pathname;
    if (pathname.indexOf("/iplt20.cms") > -1) {
      params.msid = siteConfig.pages.iplHomePage;
    }
    return params;
  }

  /**
   * Return only items whose type matches with the parameter type value
   * @param {*} arrItems
   * @param {*} type
   */
  getTypeItems(arrItems, type, len) {
    let arrData;
    arrData = arrItems && arrItems.length > 0 && arrItems.filter(item => type.indexOf(item.tn) > -1);
    arrData = arrData && arrData.length > 0 && arrData.slice(0, len);
    return arrData;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params, router } = nextProps;
      const { query } = nextProps.location;
      try {
        ListPage.fetchData({ dispatch, query, params, router }).then(data => {
          let _data = data && data.payload ? data.payload[0] : {};

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
          if (_data && _data.pwa_meta) {
            Ads_module.setSectionDetail(_data.pwa_meta);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      // this.state._scrollEventBind = false;
      //Reset section window object
      Ads_module.setSectionDetail();
    }
  }

  getSectionItems(sections, lbl) {
    for (let section of sections) {
      if (section.name == lbl) {
        return section;
      }
    }
  }

  /*Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({
    dataObj,
    datalabel,
    configlabel,
    gridclasses,
    override,
    imgsize,
    sectionId,
    weblink,
    isSectionHead,
    compType,
    sectionHeading,
    noSeo,
  }) {
    const { header } = this.props;
    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference

    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    let sectionhead;
    sectionhead = sectionHeading || dataObj ? dataObj.secname : undefined;
    sectionhead = isSectionHead == false ? undefined : sectionhead;
    override = override ? override : {};

    try {
      if (dataObj && dataObj.items) {
        if (!Array.isArray(dataObj.items)) {
          let arrObj = [];
          arrObj.push(dataObj.items);
          dataObj.items = arrObj.slice();
        }
      }

      return dataObj && dataObj.items && dataObj.items.length > 0 ? (
        <React.Fragment>
          {sectionhead && (
            <SectionHeader
              sectionhead={sectionhead}
              weblink={dataObj && (dataObj.override || dataObj.wu)}
              sectionTag={dataObj && dataObj.tag ? dataObj.tag : ""}
              compType={compType}
            />
          )}

          <GridSectionMaker
            type={designConfigs[configlabel]}
            data={dataObj.items}
            override={override}
            compType={compType}
            noSeo={noSeo}
          />
        </React.Fragment>
      ) : // <FakeDesktopDefault />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  onTeamChange = e => {
    const { params, query, router, dispatch } = this.props;
    //  const { players } = this.state;
    let players;
    let items;
    let selectedTeam, teamsArr;
    let teamobj = { msid: "63489342", teamid: "1111" };

    teamsArr = this.config.teamsArr;
    selectedTeam = e.target.value;
    for (let i = 0; i < teamsArr.length; i++) {
      if (teamsArr[i].teamname == selectedTeam) {
        teamobj = teamsArr[i].teamdata;
        break;
      }
    }
    this.setState({ fetchingPlayers: true });
    dispatch(fetchTeamDetailDataIfNeeded(params, query, router, teamobj)).then(data => {
      players = data && data.payload && data.payload[0].item && data.payload[0].item.squad;
      this.setState({ players, fetchingPlayers: false });
    });
  };

  render() {
    let { isFetching, params, router, minischedule, schedule, teamlist, teamDetail, venuelist } = this.props;

    let { sections, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    const { query } = this.props.location;
    let { players, fetchingPlayers } = this.state;
    let self = this;

    let scheduleMatches =
      schedule.value[1] && schedule.value[1].data && schedule.value[1].data.matches
        ? schedule.value[1].data.matches
        : null;

    let date = new Date(new Date().toJSON().slice(0, 10));

    if (scheduleMatches) {
      let upcomingMatches = scheduleMatches.filter(function(item) {
        let matchdate = new Date(item.matchdate_ist);
        return date.getTime() < matchdate.getTime();
      });
      scheduleMatches = upcomingMatches.slice(0, 2);
    }

    this.config.componentType = parentSection ? parentSection.tn : "";
    let compType = this.config.componentType;

    let trendingItems =
      parentSection.recommended && parentSection.recommended.trending && parentSection.recommended.trending.items
        ? parentSection.recommended.trending.items
        : undefined;

    let trendingNowItems =
      parentSection.recommended &&
      parentSection.recommended.trendingnow &&
      parentSection.recommended.trendingnow[0] &&
      parentSection.recommended.trendingnow[0].items
        ? // &&
          // parentSection.recommended.trendingnow[0].items.length > 0
          parentSection.recommended.trendingnow[0].items
        : undefined;

    const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
    const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
    const pageH1Value = pageheading + " " + pageheadingSlug;

    let newssection = extractSection(sections, "iplt20/news");
    let scheduleSec = extractSection(sections, "iplt20/schedule");

    let playerOfTheDaySec = extractSection(sections, undefined, "pod");
    let pollSec = extractSection(sections, undefined, "poll");
    let pollItem = hasValue(pollSec, "items.0") ? pollSec.items[0] : null;

    let extrctSchedule = extractMiniSchedule(minischedule);

    // let players = teamDetail && teamDetail.item && teamDetail.item.squad;
    // console.log("players-----------", players);

    return this.props.value[0] && (!isFetching || this.config.isFetchingNext) ? (
      <div
        className={"section-wrapper L1 articlelist body-content"}
        {...SeoSchema({ pagetype: compType }).attr().itemList}
      >
        {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
        {parentSection.items && parentSection.items.length > 0
          ? SeoSchema().metaTag({
              name: "numberOfItems",
              content: parentSection.items.filter(item => {
                return item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")
                  ? true
                  : false;
              }).length,
            })
          : null}
        {parentSection.pwa_meta
          ? SeoSchema().metaTag({
              name: "url",
              content: parentSection.pwa_meta.canonical,
            })
          : null}
        {/* Breadcrumb  */}
        {parentSection &&
        typeof parentSection.breadcrumb != "undefined" &&
        parentSection.breadcrumb.div &&
        parentSection.breadcrumb.div.ul ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
        <div className="only-heading">
          <SectionHeader sectionhead={pageH1Value} headingTag="h1" />
        </div>
        {
          <ErrorBoundary key="schedule">
            <div className="mini_schedule">
              <Slider
                margin="15"
                size="3"
                sliderData={extrctSchedule && extrctSchedule.schedule}
                width="280"
                type="iplwidgets"
                compType="minischedule"
                sliderClass="grid_slider"
              />
            </div>
          </ErrorBoundary>
        }

        {/* To Display PL articlelist  */}
        {compType == "articlelist" ? (
          parentSection ? (
            parentSection.items && parentSection.items.length > 0 ? (
              <div className="row">
                {this.createSectionLayout({
                  dataObj: { items: parentSection.items },
                  configlabel: "headline",
                })}
                <div className="col4">
                  <AdCard mstype="mrec1" adtype="dfp" className="box-item mr15 ad-top" />
                  <div className="box-item adblockdiv">
                    <AdCard className="emptyAdBox" mstype="ctnbccl" className="" adtype="ctn" />
                  </div>
                </div>
              </div>
            ) : null
          ) : (
            <FakeDesktopDefault />
          )
        ) : null}

        {parentSection.recommended &&
        parentSection.recommended.trendingtopics &&
        parentSection.recommended.trendingtopics[0] &&
        parentSection.recommended.trendingtopics[0].items ? (
          // &&
          // parentSection.recommended.trendingtopics[0].items.length > 0
          <div className="trending-bullet">
            <KeyWordCard items={parentSection.recommended.trendingtopics[0].items} secname=" " />
          </div>
        ) : null}

        <div className="row wdt_teams hp">
          {_isCSR() && teamlist && teamlist.teams ? (
            <React.Fragment>
              <SectionHeader
                sectionhead={(teamlist.secmeta && teamlist.secmeta.secname) || "IPL Teams"}
                weblink={teamlist.secmeta && teamlist.secmeta.override}
              />
              <TeamListComp teamlist={teamlist} />
            </React.Fragment>
          ) : null}
        </div>

        {
          <div className="row">
            <div className="col8 pd0">
              <ArticlelistSections item={newssection} createSectionLayout={this.createSectionLayout.bind(this)} />
            </div>
            <div className="col4">
              {_isCSR() ? (
                <React.Fragment>
                  <PointsTableCard compType={"pointtablecard"} />
                  <PlayerOfTheDay section={playerOfTheDaySec} />
                </React.Fragment>
              ) : null}
            </div>
          </div>
        }

        {pollItem ? (
          <div className="wdt_sports_poll">
            <PollCard data={pollItem} />
          </div>
        ) : null}

        <div className="row">
          <div className="col8 pd0 wdt_players">
            <React.Fragment>
              {teamDetail ? <SectionHeader sectionhead={siteConfig.locale.players || "Players"} /> : null}
              {players && !fetchingPlayers ? (
                <div className="col12">
                  <div className="matchNvenue">
                    <select id="teamSelect" onChange={this.onTeamChange}>
                      {this.config.teamsArr.map((item, index) => {
                        return (
                          <option value={item.teamname} key={"team" + index}>
                            {item.teamname}
                          </option>
                        );
                      })}
                    </select>
                  </div>

                  <Slider
                    margin="0"
                    size="4"
                    sliderData={players}
                    width="150"
                    type="iplwidgets"
                    compType="players"
                    sliderClass="players_slider"
                  />
                </div>
              ) : null}
            </React.Fragment>
          </div>

          {scheduleMatches && scheduleMatches.length > 0 ? (
            <div className="col4">
              <div className="row schedule hp">
                <SectionHeader
                  sectionhead={(scheduleSec && scheduleSec.secname) || "Schedule"}
                  weblink={scheduleSec && scheduleSec.override}
                />
                <ul className="col12">
                  <ScheduleCard schedule={scheduleMatches} />
                </ul>
              </div>
            </div>
          ) : null}
        </div>

        <div className="row wdt_venues hp">
          {venuelist && venuelist.items && venuelist.items.length > 0 ? (
            <div className="col8 pd0">
              <div className="row">
                <SectionHeader
                  sectionhead={(venuelist.secmeta && venuelist.secmeta.secname) || "Stadium"}
                  weblink={venuelist.secmeta && venuelist.secmeta.override}
                />
                <Slider
                  margin="15"
                  size="2"
                  sliderData={venuelist.items}
                  width="310"
                  type="iplwidgets"
                  compType="venues"
                  sliderClass="grid_slider"
                />
              </div>
            </div>
          ) : null}
          <div className="col4">
            <AdCard mstype="mrec2" adtype="dfp" className="box-item" />
          </div>
        </div>

        {
          <div className="btf-placeholder">
            <AdCard mstype="btf" adtype="dfp" />
          </div>
        }

        {/* Below code is used to render sections for Articlelist L1/L2 */}
        {compType == "articlelist" && sections && sections instanceof Array
          ? sections.map(item => {
              return item.seolocation.indexOf("iplt20/news") == -1 ? (
                <ArticlelistSections item={item} createSectionLayout={this.createSectionLayout.bind(this)} />
              ) : null;
            })
          : null}

        {/* Trending section AL */}
        {compType == "articlelist" && trendingItems && parentSection.items && (
          <div className="highlight_most_read">
            <div className="row">
              <TrendingSectionGrid
                compType={compType}
                parentSection={parentSection}
                configLbl={"gridHorizontal"}
                createSectionLayout={this.createSectionLayout.bind(this)}
              />
            </div>
          </div>
        )}
        {compType == "articlelist" && trendingNowItems && (
          <div className="row">
            <div className="col12 list-trending-now">
              {<KeyWordCard items={trendingNowItems} secname={parentSection.recommended.trendingnow[0].secname} />}
            </div>
          </div>
        )}

        {this.props &&
        this.props.value &&
        this.props.value[0] &&
        this.props.value[0].pwa_meta &&
        this.props.value[0].pwa_meta.syn != undefined &&
        typeof this.props.value[0].pwa_meta.syn == "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{this.props.value[0].pwa_meta.syn}</div>
          </ErrorBoundary>
        ) : null}

        {/* To show fake listing until data is data is rendered on screen */}
        {isFetching ? <FakeDesktopDefault showImages={true} /> : null}
      </div>
    ) : (
      <FakeDesktopDefault />
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.articlelist,
    venuelist: state.IPL && state.IPL.venuelist,
    teamlist: state.IPL && state.IPL.teamlist,
    minischedule: state.minischedule,
    schedule: state.schedule,
    teamDetail: state.IPL && state.IPL.teamDetail,
  };
}

HomePage.fetchData = ({ dispatch, query, params, router }) => {
  dispatch(setPageType("articlelist"));

  let pathname = router.location.pathname;
  if (pathname.indexOf("/iplt20.cms") > -1) {
    params.msid = siteConfig.pages.iplHomePage;
  }
  dispatch(fetchMiniScheduleDataIfNeeded(params, query));
  return dispatch(fetchListDataIfNeeded(params, query, router));
};

const ArticlelistSections = props => {
  let { item, createSectionLayout } = props;
  let _item = item ? JSON.parse(JSON.stringify(item)) : null;
  let types = ["news", "photos", "videos"];
  let section = sectionType(_item);
  if (types.indexOf(section) == -1) {
    return null;
  }
  return item.tn == "photolist" ? (
    item.items && Array.isArray(item.items) && item.items.length > 0 ? (
      <div className="wdt_photo">
        <SectionHeader sectionhead={item.secname} weblink={item.override || item.wu} />

        <Slider
          margin="10"
          size="4"
          sliderData={item.items}
          width="230"
          type="grid"
          sliderClass="showcase"
          movesize="4"
        />
      </div>
    ) : null
  ) : item.tn == "articlelist" ? (
    createSectionLayout({
      dataObj: _item,
      configlabel: "listSection",
    })
  ) : item.tn == "videolist" ? (
    <VideoSection videoData={item} />
  ) : null;
};

const sectionType = item => {
  let sectionType = "";
  if (item) {
    if (item.seolocation.indexOf("iplt20/news") > -1) {
      sectionType = "news";
    } else if (item.seolocation.indexOf("iplt20/teams") > -1) {
      sectionType = "teams";
    } else if (item.seolocation.indexOf("iplt20/player-of-the-day") > -1) {
      sectionType = "player";
    } else if (item.seolocation.indexOf("iplt20/points-table") > -1) {
      sectionType = "pointstable";
    } else if (item.seolocation.indexOf("iplt20/venues") > -1) {
      sectionType = "venues";
    } else if (item.seolocation.indexOf("iplt20/photos") > -1) {
      sectionType = "photos";
    } else if (item.seolocation.indexOf("iplt20/video") > -1) {
      sectionType = "videos";
    }
  }

  return sectionType;
};

const TrendingSectionGrid = props => {
  let { compType, configLbl, parentSection, createSectionLayout, noSeo } = props;

  let trendingSection = parentSection.recommended.trending;
  if (!(parentSection && parentSection.recommended && parentSection.recommended.trending)) {
    return null;
  }

  return trendingSection && trendingSection.items
    ? createSectionLayout({
        dataObj: trendingSection,
        configlabel: configLbl,
        override: {
          imgsize: compType == "videolist" ? "largewidethumb" : "smallthumb",
        },
        compType: compType,
        noSeo: noSeo,
      })
    : null;
};

export default connect(mapStateToProps)(HomePage);
