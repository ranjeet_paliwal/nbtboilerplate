import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "../../../../utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_POINTSTABLE_REQUEST = "FETCH_POINTSTABLE_REQUEST";
export const FETCH_POINTSTABLE_SUCCESS = "FETCH_POINTSTABLE_SUCCESS";
export const FETCH_POINTSTABLE_FAILURE = "FETCH_POINTSTABLE_FAILURE";

function fetchPointsTableDataFailure(error) {
  return {
    type: FETCH_POINTSTABLE_FAILURE,
    payload: error.message,
  };
}
function fetchPointsTableDataSuccess(data, params) {
  //data[0]['pwa_meta']=data[1].pwa_meta;
  let seriesid = data[0] && data[0].pwa_meta && data[0].pwa_meta.cricketlb ? data[0].pwa_meta.cricketlb : null;
  return {
    type: FETCH_POINTSTABLE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    seriesid: seriesid,
    msid: params.msid,
    payload: data,
  };
}

// function getParams(params, router) {
//   let pathname = router && router.location.pathname;
//   if (pathname.indexOf("/cricket/iplt20.cms") > -1) {
//     params.msid = siteConfig.pages.iplHomePage;
//   }
//   return params;
// }

function fetchPointsTableData(state, params, query, router) {
  // FIXME- This MSID is kept in config so that when landing page call pointstable widget,
  // meta call is sent with pointstable msid and not with landing page msid.
  // soln send msid from pointstable card or meta from feed.
  let pointstableMSID = siteConfig.iplpages && siteConfig.iplpages.pointsTable;

  let sectionApi = process.env.API_BASEPOINT + "/pwafeeds/pwa_metalist.cms?msid=" + pointstableMSID + "&feedtype=sjson";
  return dispatch => {
    dispatch({
      type: FETCH_POINTSTABLE_REQUEST,
      payload: pointstableMSID,
    });
    let Promise1 = fetch(sectionApi);

    return Promise.all([Promise1])
      .then(
        data => {
          //dispatch(fetchPointsTableDataSuccess(data,params));
          let apiUrl = siteConfig.cricket_SIAPI.points_table;
          apiUrl = apiUrl.replace("%seriesid%", data[0].pwa_meta.cricketlb);
          let response = [];
          response[0] = data[0];
          if (data[0] && data[0].pwa_meta && data[0].pwa_meta.cricketlb) {
            let Promise3 = fetch(apiUrl);
            return Promise.all([Promise3])
              .then(
                data => {
                  response[1] = data[0];
                  return dispatch(fetchPointsTableDataSuccess(response, params));
                },
                error => dispatch(fetchPointsTableDataFailure(error)),
              )
              .catch(function(error) {
                return dispatch(fetchPointsTableDataFailure(error));
              });
          } else {
            return dispatch(fetchPointsTableDataSuccess(data, params));
          }
        },
        error => dispatch(fetchPointsTableDataFailure(error)),
      )
      .catch(function(error) {
        return dispatch(fetchPointsTableDataFailure(error));
      });
  };
}

function shouldFetchPointsTableData(state, params, query) {
  if (
    typeof state.pointstable.value[0] == "undefined" ||
    typeof state.pointstable.value[1] == "undefined" ||
    (typeof state.pointstable.value[0] != "undefined" &&
      typeof state.pointstable.value[1] != "undefined" &&
      state.pointstable.value[0].id != params.msid)
  ) {
    return true;
  } else {
    return false;
  }
}

export function fetchPointsTableDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchPointsTableData(getState(), params, query)) {
      return dispatch(fetchPointsTableData(getState(), params, query, router));
    } else {
      return Promise.resolve([]);
    }
  };
}
