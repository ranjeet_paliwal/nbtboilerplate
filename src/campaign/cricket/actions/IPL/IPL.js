/* eslint-disable radix */
import fetch from "utils/fetch/fetch";
import {
  _getStaticConfig,
  _apiBasepointUpdate,
  _getBranchfeedCategory,
  getPageType,
  isMobilePlatform,
} from "../../../../utils/util";
import { fetchPointsTableDataIfNeeded } from "../pointstable/pointstable";
const siteConfig = _getStaticConfig();
export const FETCH_TEAM_REQUEST = "FETCH_TEAM_REQUEST";
export const FETCH_TEAM_SUCCESS = "FETCH_TEAM_SUCCESS";
export const FETCH_TEAM_FAILURE = "FETCH_TEAM_FAILURE";

export const FETCH_VENUE_REQUEST = "FETCH_VENUE_REQUEST";
export const FETCH_VENUE_SUCCESS = "FETCH_VENUE_SUCCESS";
export const FETCH_VENUE_FAILURE = "FETCH_VENUE_FAILURE";

export const FETCH_TEAM_DETAIL_REQUEST = "FETCH_TEAM_DETAIL_REQUEST";
export const FETCH_TEAM_DETAIL_SUCCESS = "FETCH_TEAM_DETAIL_SUCCESS";
export const FETCH_TEAM_DETAIL_FAILURE = "FETCH_TEAM_DETAIL_FAILURE";

function fetchTeamDataFailure(error) {
  return {
    type: FETCH_TEAM_FAILURE,
    payload: error.message,
  };
}
function fetchTeamDataSuccess(data) {
  return {
    type: FETCH_TEAM_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchVenueDataFailure(error) {
  return {
    type: FETCH_VENUE_FAILURE,
    payload: error.message,
  };
}
function fetchVenueDataSuccess(data) {
  return {
    type: FETCH_VENUE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchTeamDetailDataFailure(error) {
  return {
    type: FETCH_TEAM_DETAIL_FAILURE,
    payload: error.message,
  };
}
function fetchTeamDetailDataSuccess(data) {
  return {
    type: FETCH_TEAM_DETAIL_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchTeamData(state, params, query, router) {
  let msid, apiUrl;

  msid = siteConfig.iplpages["teams"];

  apiUrl = `${process.env.API_BASEPOINT}/feed_teams.cms?msid=${msid}&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_TEAM_REQUEST,
      payload: msid,
    });
    const Promise1 = fetch(apiUrl);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchTeamDataSuccess(data)),
        error => dispatch(fetchTeamDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTeamDataFailure(error));
      });
  };
}

//
function fetchVenueData(state, params, query, router) {
  let msid, apiUrl;
  msid = siteConfig.iplpages && siteConfig.iplpages["venues"];

  apiUrl = `${process.env.API_BASEPOINT}/feed_venues.cms?msid=${msid}&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_VENUE_REQUEST,
      payload: msid,
    });
    const Promise1 = fetch(apiUrl);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchVenueDataSuccess(data)),
        error => dispatch(fetchVenueDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchVenueDataFailure(error));
      });
  };
}

function fetchTeamDetailData(state, params, query, router, teamObj = {}) {
  let apiUrl;

  apiUrl = `${process.env.API_BASEPOINT}/feed_teamdetails.cms?msid=${teamObj.msid}&teamid=${teamObj.teamid}&feedtype=sjson`;

  return dispatch => {
    dispatch({
      type: FETCH_TEAM_DETAIL_REQUEST,
      payload: params.msid,
    });
    const Promise1 = fetch(apiUrl);

    return Promise.all([Promise1])
      .then(
        data => dispatch(fetchTeamDetailDataSuccess(data)),
        error => dispatch(fetchTeamDetailDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchTeamDetailDataFailure(error));
      });
  };
}

function shouldFetchTeamData(state, params, query) {
  if (
    typeof state.IPL.teamlist === "undefined"
    // ||
    // (typeof state.articlelist.value[0] !== "undefined" && state.articlelist.value[0].id != params.msid)
  ) {
    return true;
  }
  return false;
}

function shouldFetchTeamDetailData(state, params, query) {
  if (
    typeof state.IPL.teamDetail === "undefined"
    // ||
    // (typeof state.articlelist.value[0] !== "undefined" && state.articlelist.value[0].id != params.msid)
  ) {
    return true;
  }
  return false;
}

function shouldFetchVenueData(state, params, query) {
  if (
    typeof state.IPL.venues === "undefined"
    // ||
    // (typeof state.articlelist.value[0] !== "undefined" && state.articlelist.value[0].id != params.msid)
  ) {
    return true;
  }
  return false;
}

export function fetchTeamDetailDataIfNeeded(params, query, router, teamObj) {
  return (dispatch, getState) => {
    // if (shouldFetchTeamDetailData(getState(), params, query, teamObj)) {
    return dispatch(fetchTeamDetailData(getState(), params, query, router, teamObj));
    // }
    return Promise.resolve([]);
  };
}

export function fetchVenueDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchVenueData(getState(), params, query)) {
      return dispatch(fetchVenueData(getState(), params, query, router));
    }
    return Promise.resolve([]);
  };
}

export function fetchTeamDataIfNeeded(params, query, router) {
  return (dispatch, getState) => {
    if (shouldFetchTeamData(getState(), params, query)) {
      return dispatch(fetchTeamData(getState(), params, query, router));
    }
    return Promise.resolve([]);
  };
}

export function fetchIPLDataIfNeeded(params, query, router) {
  const { location } = router;
  const pathname = location && location.pathname;

  return dispatch => {
    if (pathname) {
      if (pathname.indexOf("/pointstable/") > -1) {
        return dispatch(fetchPointsTableDataIfNeeded(params, query));
      } else if (pathname.indexOf("/teams/") > -1) {
        return dispatch(fetchTeamDataIfNeeded(params, query, router));
      } else if (pathname.indexOf("/venues/") > -1) {
        return dispatch(fetchVenueDataIfNeeded(params, query, router));
      } else if (pathname.indexOf("/team/") > -1) {
        return dispatch(fetchTeamDetailDataIfNeeded(params, query, router));
      }
      return Promise.resolve([]);
    }
  };
}

//
