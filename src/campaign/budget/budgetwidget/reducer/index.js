import {
  FETCH_BUDGETLIVEBLOG_REQUEST,
  FETCH_BUDGETLIVEBLOG_SUCCESS,
  FETCH_BUDGETLIVEBLOG_FAILURE,
  FETCH_BUDGETIMPACT_REQUEST,
  FETCH_BUDGETIMPACT_SUCCESS,
  FETCH_BUDGETIMPACT_FAILURE
} from "../action/index";

function BUDGETWIDGET(state = {}, action) {
  switch (action.type) {
    case FETCH_BUDGETLIVEBLOG_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_BUDGETLIVEBLOG_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_BUDGETLIVEBLOG_SUCCESS:
      state.liveblog = { data: action.payload };
      return {
        ...state,
        isFetching: false,
        error: false
      };
    case FETCH_BUDGETIMPACT_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_BUDGETIMPACT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_BUDGETIMPACT_SUCCESS:
      state.budget = { data: action.payload };
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default BUDGETWIDGET;
