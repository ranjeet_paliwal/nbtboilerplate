import fetch from "./../../../../utils/fetch/fetch";
import { _getStaticConfig } from "./../../../../utils/util";
const siteConfig = _getStaticConfig();

export const FETCH_BUDGETLIVEBLOG_REQUEST = "FETCH_BUDGETLIVEBLOG_REQUEST";
export const FETCH_BUDGETLIVEBLOG_SUCCESS = "FETCH_BUDGETLIVEBLOG_SUCCESS";
export const FETCH_BUDGETLIVEBLOG_FAILURE = "FETCH_BUDGETLIVEBLOG_FAILURE";

export const FETCH_BUDGETIMPACT_REQUEST = "FETCH_BUDGETIMPACT_REQUEST";
export const FETCH_BUDGETIMPACT_SUCCESS = "FETCH_BUDGETIMPACT_SUCCESS";
export const FETCH_BUDGETIMPACT_FAILURE = "FETCH_BUDGETIMPACT_FAILURE";

function fetchBUDGETIMPACT_DataFailure(error) {
  return {
    type: FETCH_BUDGETIMPACT_FAILURE,
    payload: error.message
  };
}

function fetchBUDGETIMPACT_DataSuccess(data) {
  return {
    type: FETCH_BUDGETIMPACT_SUCCESS,
    payload: data
  };
}
function shouldFetchBUDGETIMPACT_Data(state, params, query) {
  // return (typeof state.electionresult.data == 'undefined');
  return true;
}

function fetchBUDGETIMPACT_Data(state, params, query) {
  const _url =
    "https://navbharattimes.indiatimes.com/pwafeeds/pwa_homelist.cms?msid=51007363&type=budget&feedtype=sjson&lang=eng";
  // const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.json';

  return dispatch => {
    dispatch({ type: FETCH_BUDGETIMPACT_REQUEST });

    return fetch(_url)
      .then(
        data => {
          dispatch(fetchBUDGETIMPACT_DataSuccess(data));
        },
        error => dispatch(fetchBUDGETIMPACT_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchBUDGETIMPACT_DataFailure(error));
      });
  };
}
export function fetchBUDGETIMPACT_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchBUDGETIMPACT_Data(getState(), params, query)) {
      return dispatch(fetchBUDGETIMPACT_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

function fetchBUDGETLIVEBLOG_DataFailure(error) {
  return {
    type: FETCH_BUDGETLIVEBLOG_FAILURE,
    payload: error.message
  };
}

function fetchBUDGETLIVEBLOG_DataSuccess(data) {
  return {
    type: FETCH_BUDGETLIVEBLOG_SUCCESS,
    payload: data
  };
}

function fetchBUDGETLIVEBLOG_Data(state, params, query) {
  let pagination = siteConfig.channelCode == "nbt" ? "-1" : "";
  let _url = `https://langnetstorage.indiatimes.com/configspace/msid-${params.msid},callback-liveBlogTypeALL,host-${siteConfig.livebloghostname}${pagination}.htm`;
  // const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.json';

  return dispatch => {
    dispatch({ type: FETCH_BUDGETLIVEBLOG_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          let match = data
            .replace("liveBlogTypeALL (", "")
            .replace("liveBlogTypeALL(", "")
            .slice(0, -1);
          data = { lbcontent: JSON.parse(match) };
          dispatch(fetchBUDGETLIVEBLOG_DataSuccess(data));
        },
        error => dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchBUDGETLIVEBLOG_DataFailure(error));
      });
  };
}

// function fetchBUDGETLIVEBLOGPWA_Data(state, params, query) {
// 	const _url = `https://langnetstorage.indiatimes.com/configspace/msid-58166012,callback-liveBlogTypeALL,host-${siteConfig.livebloghostname}-1.htm`;
// 	const _pwaurl = 'https://navbharattimes.indiatimes.com/pwafeeds/pwa_homelist.cms?msid=51007363&type=budget&feedtype=sjson';

// 	return dispatch => {
// 		dispatch({ type: FETCH_BUDGETLIVEBLOG_REQUEST });

// 		return fetch(_url,{type:'jsonp'}).then(
// 			(data) => {
// 				let match = data.replace('liveBlogTypeALL (' , '').slice(0, -1);
// 				data = {"lbcontent":JSON.parse(match)};
// 				dispatch({
// 					type: FETCH_BUDGETLIVEBLOG_REQUEST
// 				});
// 				return fetch(_pwaurl).then(
// 					(pwadata) => {
// 						data['pwadata'] = pwadata;
// 						dispatch(fetchBUDGETLIVEBLOG_DataSuccess(data));
// 					},
// 					error =>dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 				).catch((error)=>{
// 					dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 				});
// 			},
// 			error =>dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 		).catch((error)=>{
// 			dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 		});
// 	}
// }

function shouldFetchBUDGETLIVEBLOG_Data(state, params, query) {
  // return (typeof state.electionresult.data == 'undefined');
  return true;
}

export function fetchBUDGETLIVEBLOG_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchBUDGETLIVEBLOG_Data(getState(), params, query)) {
      return dispatch(fetchBUDGETLIVEBLOG_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

// export function fetchBUDGETLIVEBLOGPWA_IfNeeded(params, query) {
// 	return (dispatch, getState) => {
// 		if (shouldFetchBUDGETLIVEBLOG_Data(getState(),params, query)) {
// 			return dispatch(fetchBUDGETLIVEBLOGPWA_Data(getState(), params, query));
// 		}else{
// 			return Promise.resolve({})
// 		}
// 	};
// }

// export function fetchBUDGETLIVEBLOG_Data_Polling(dispatch) {
// 	const _url = `https://langnetstorage.indiatimes.com/configspace/msid-58166012,callback-liveBlogTypeALL,host-${siteConfig.livebloghostname}-1.htm`;
// 	// const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.htm';

// 	dispatch({ type: FETCH_BUDGETLIVEBLOG_REQUEST });

// 	return fetch(_url,{type:'jsonp'}).then(
// 		(data) => {

// 			let match = data.replace('liveBlogTypeALL (' , '').slice(0, -1);
// 			data = {"lbcontent":JSON.parse(match)};
// 			dispatch(fetchBUDGETLIVEBLOG_DataSuccess(data))
// 		},
// 		error =>dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 	).catch(function(error){
// 		dispatch(fetchBUDGETLIVEBLOG_DataFailure(error))
// 	});
// }
