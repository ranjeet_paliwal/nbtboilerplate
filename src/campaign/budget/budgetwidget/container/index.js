import React, { Component } from "react";
import { connect } from "react-redux";

import { fetchBUDGETLIVEBLOG_IfNeeded, fetchBUDGETIMPACT_IfNeeded } from "../action";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import AnchorLink from "../../../../components/common/AnchorLink";
import { _isCSR, setHyp1Data, _isFrmApp, _filterALJSON } from "../../../../utils/util";
import VideoPlayer from "./../../../../modules/videoplayer/index";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
const slikeApiKey = siteConfig.slike.apikey;

class BudgetWidget extends Component {
  constructor(props) {
    super(props);
    this.state = { hide: false };
    this.config = {
      budgetInterval: 30000,
      pollingClear: false,
    };
    this.closeBudgetWidget = this.closeBudgetWidget.bind(this);
    this.refreshBudget = this.refreshBudget.bind(this);
  }

  createConfig = minitv => {
    let slikeConfig = {
      video: {
        id: minitv.slikeid || defaultSlikeId, // Default Times Now Channel slike ID
        title: minitv.hl || "",
        msid: minitv.id || "",
        shareUrl: minitv.wu + "?" + siteConfig.slikeshareutm || "",
      },
      sdk: {
        apikey: slikeApiKey,
      },
    };
    return slikeConfig;
  };
  onVideoClickHandler = (item, event) => {
    let _this = this;
    // event.currentTarget.previousSibling.innerText
    let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
    if (videourl) {
      try {
        //To maintain previous url
        window.history.pushState({}, "", videourl);
        //AnalyticsGA.pageview(location.origin + _this.generateUrl(item));
      } catch (ex) {
        console.log("Exception in history: ", ex);
      }
    }
  };
  componentDidMount() {
    let _this = this;
    const { dispatch, query, navigations } = _this.props;
    // BudgetResult.fetchData({ dispatch, params, query }).catch(err => {
    //   console.log(err.message);
    // });
    let pwaconfig = _filterALJSON(navigations, ["pwaconfig"]);
    let budgetconfig = this.getbudgetconfig(pwaconfig);
    let params = {
      msid: budgetconfig._lbmsid,
    };

    dispatch(fetchBUDGETLIVEBLOG_IfNeeded(params, query));
    dispatch(fetchBUDGETIMPACT_IfNeeded(params, query));
    _this.refreshBudget();

    //set hyp1 variable
    setHyp1Data({ hyp1: "budget_campaign" });
  }

  componentWillUnmount() {
    //reset hyp1 variable
    setHyp1Data();

    //clear timer
    // clearInterval(this.config.timer);
  }
  closeBudgetWidget() {
    clearInterval(this.config.timer);
    this.setState({ hide: true });
    document.getElementById("budgetwidget").style.display = "none";
  }
  refreshBudget() {
    let _this = this;
    const { dispatch, query, navigations } = _this.props;
    let pwaconfig = _filterALJSON(navigations, ["pwaconfig"]);
    let budgetconfig = this.getbudgetconfig(pwaconfig) ? this.getbudgetconfig(pwaconfig) : {};
    let params = {
      msid: budgetconfig._lbmsid,
    };
    _this.config.timer = setInterval(() => {
      //fetchBUDGETLIVEBLOG_Data_Polling(dispatch);
      dispatch(fetchBUDGETLIVEBLOG_IfNeeded(params, query));
      dispatch(fetchBUDGETIMPACT_IfNeeded(params, query));
      //   if (_this.config.pollingClear) clearInterval(timer);
    }, window.budgetInterval || _this.config.budgetInterval);
  }

  getbudgetconfig(pwaconfig) {
    let budgetconfig;
    Object.keys(pwaconfig).map(function(key, index) {
      if (
        pwaconfig[key].label != undefined &&
        pwaconfig[key].label != null &&
        pwaconfig[key].label.toLowerCase() == "pwaconfig"
      ) {
        let L2 = pwaconfig[key];
        Object.keys(L2).map(function(key, index) {
          if (L2[key].label != undefined && L2[key].label != null && L2[key].label.toLowerCase() == "budget") {
            budgetconfig = L2[key];
          }
        });
      }
    });
    return budgetconfig;
  }

  render() {
    let { liveblog, budget, navigations } = this.props;
    let filteredlbdata = [];
    let minitv = {
      slikeid: siteConfig.slike.default_slikeid_minitv,
    };
    //filter live blog
    if (liveblog && liveblog.data && liveblog.data.lbcontent && liveblog.data.lbcontent.length > 0) {
      let textnode = 0;
      for (var i = 0; i < liveblog.data.lbcontent.length; i++) {
        if (liveblog.data.lbcontent[i] && liveblog.data.lbcontent[i].type == "t") {
          filteredlbdata.push(liveblog.data.lbcontent[i]);
          textnode += 1;
        }
        if (textnode >= 3) break;
      }
    }
    //filter configuration of Budget
    let pwaconfig = _filterALJSON(navigations, ["pwaconfig"]);
    let budgetconfig = this.getbudgetconfig(pwaconfig);
    return (
      <ErrorBoundary>
        <React.Fragment>
          {budgetconfig && (budgetconfig._lb == "true" || budgetconfig._cheaper_dearer == "true") ? (
            <div id="budgetwidget" className="box-content wdt_budget" hidden={this.state.hide}>
              {budgetconfig && budgetconfig._lb == "true" && filteredlbdata.length ? (
                <div>
                  <div data-videourl="#" className="budget_head">
                    <h3>
                      <AnchorLink href={"/liveblog/" + budgetconfig._lbmsid + ".cms"}>
                        {budgetconfig._heading}
                      </AnchorLink>
                    </h3>
                    <VideoPlayer
                      data-videourl="#"
                      key={"livetv_widget"}
                      apikey={slikeApiKey}
                      wrapper="masterVideoPlayer"
                      config={this.createConfig(minitv)}
                      onVideoClick={this.onVideoClickHandler.bind(this, minitv)}
                    >
                      <span href="#" className="live_tv minitv_icon">
                        {siteConfig.locale.live_tv}
                      </span>
                    </VideoPlayer>
                    {/* <span className="close_icon" onClick={this.closeBudgetWidget}></span> */}
                  </div>
                  <ul>
                    {filteredlbdata && filteredlbdata.length > 0
                      ? filteredlbdata.map((item, index) => {
                          return item.title ? <li>{item.title}</li> : null;
                        })
                      : null}
                  </ul>
                  <div className="budget_btn">
                    <a href="/business/budget/budget-news/data-hub/budget_datahub.cms" className="btn">
                      {siteConfig.locale.data_hub}
                    </a>
                    <a href="/business/budget/budget_fantasygame.cms" className="btn">
                      {siteConfig.locale.fantasy_game}
                    </a>
                  </div>
                </div>
              ) : null}

              {budgetconfig &&
              budgetconfig._cheaper_dearer == "true" &&
              budget &&
              budget.data &&
              budget.data.items &&
              budget.data.items.length > 0 ? (
                <div className="cheaper_dearer">
                  <h3>{budgetconfig._cheaper_dearer_txt}</h3>
                  <div className="scroll">
                    <ul className="list" style={{ width: 90 * budget.data.items.length + "px" }}>
                      {budget.data.items.map((item, index) => {
                        return item.hl ? (
                          <li>
                            <div className="top">
                              <span className="tbl_col icon_col">
                                <img
                                  height="40"
                                  width="40"
                                  src={
                                    siteConfig.mweburl +
                                    "/photo/msid-" +
                                    item.imageid +
                                    ",width-40,height-40/" +
                                    siteConfig.wapsitename +
                                    ".jpg"
                                  }
                                  alt="प्रोसेस्ड काजू"
                                />
                              </span>
                              <span className={"tbl_col" + (item.syn == 1 ? " cheaper" : " dearer")}>
                                <b></b>
                              </span>
                            </div>
                            <div className="itemtext">{item.hl}</div>
                          </li>
                        ) : null;
                      })}
                    </ul>
                  </div>
                </div>
              ) : null}
            </div>
          ) : null}
        </React.Fragment>
      </ErrorBoundary>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.budgetwidget,
  };
}

// BudgetWidget.fetchData = ({dispatch, params, query }) => {
//   dispatch(fetchBUDGETLIVEBLOG_IfNeeded(params, query));
//   return dispatch(fetchBUDGETIMPACT_IfNeeded(params, query));
// };

export default connect(mapStateToProps)(BudgetWidget);
