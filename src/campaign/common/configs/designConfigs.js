export const designConfigs = {
  headline: {
    sections: [
      {
        className: "col8 top_headlines",
        type: [
          {
            startIndex: 0,
            noOfElements: 1,
            type: "lead",
            className: "col6 pd0",
            imgsize: "midthumb",
          },
          {
            startIndex: 1,
            noOfElements: 6,
            type: "horizontal",
            offDrawer: true,
            className: "col6",
            offads: true,
            imgsize: "smallthumb",
          },
        ],
      },
    ],
  },
  trending: [
    {
      startIndex: 0,
      noOfColumns: 1,
      type: "lead",
      istrending: "true",
    },
  ],
  main: {
    sections: [
      {
        type: [
          {
            startIndex: 0,
            noOfElements: 2,
            noOfColumns: 2,
            type: "lead",
            imgsize: "largethumb",
            className: "col12 pd0",
          },
        ],
      },
    ],
  },
  listSection: {
    sections: [
      {
        className: "col12 pd0",
        type: [
          {
            startIndex: 0,
            noOfElements: 5,
            type: "horizontal",
            className: "col6",
            imgsize: "smallthumb",
          },
          {
            startIndex: 5,
            noOfElements: 5,
            type: "horizontal",
            className: "col6",
            imgsize: "smallthumb",
          },
        ],
      },
    ],
  },
  newsSection: {
    sections: [
      {
        className: "col12 pd0",
        type: [
          {
            startIndex: 0,
            noOfElements: 5,
            type: "horizontal",
            className: "col4",
            imgsize: "smallthumb",
          },
          {
            startIndex: 5,
            noOfElements: 5,
            type: "horizontal",
            className: "col4",
            imgsize: "smallthumb",
          },
          {
            startIndex: 10,
            noOfElements: 7,
            offads: true,
            type: "horizontal",
            className: "col4",
            imgsize: "smallthumb",
          },
        ],
      },
    ],
  },
  photoVideoHeaderL2: [
    {
      startIndex: 0,
      noOfElements: 2,
      noOfColumns: 2,
      className: "col12",
    },
  ],
  PLlisting: [
    {
      startIndex: 2,
      noOfColumns: 3,
      className: "col12",
    },
  ],
  default: [
    {
      startIndex: 0,
      type: "horizontal-lead",
      className: "col12 pd0 medium_listing",
      noOfColumns: 3,
    },
  ],
  gridHorizontal: [
    {
      startIndex: 0,
      type: "vertical",
      noOfColumns: 5,
      className: "col12 most-read-stroies",
      istrending: "true",
    },
  ],
};
