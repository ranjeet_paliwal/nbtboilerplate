import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded } from "../../../actions/listpage/listpage";
import FakeDesktopDefault from "../../../components/common/FakeCards/FakeDesktopDefault";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/Desktop.scss";
import "../../../components/common/css/desktop/SectionWrapper.scss";
import "../../../components/common/css/ListPage.scss";

import { _getStaticConfig, _isCSR, isMobilePlatform } from "../../../utils/util";
const siteConfig = _getStaticConfig();
import { designConfigs } from "../configs/designConfigs";

import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import Slider from "../../../components/desktop/Slider/index";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";

import VideoSection from "../../../components/common/VideoSection/VideoSection";
import KeyWordCard from "../../../components/common/KeyWordCard";
import { defaultDesignConfigs } from "../configs/defaultDesignConfigs";
import AdCard from "../../../components/common/AdCard";

class VideoPhotoNewsCombined extends Component {
  constructor(props) {
    super(props);
    this.config = {
      componentType: "",
    };
  }

  componentDidMount() {
    const { dispatch, query, router, sectionMsid } = this.props;
    // let { params } = this.props;
    let params;
    if (this.props && this.props.params) {
      params = Object.assign({}, this.props.params);
      params.msid = sectionMsid;
    }

    if (_isCSR()) {
      dispatch(fetchListDataIfNeeded(params, query, router));
    }
  }

  getDataList(section, noOfEle, startIndex = 0) {
    let tmpSection = JSON.parse(JSON.stringify(section));
    if (tmpSection["items"] && tmpSection["items"].length > 0) {
      if (noOfEle > 0) {
        tmpSection["items"] = tmpSection["items"].splice(startIndex, noOfEle);
      } else {
        tmpSection["items"] = tmpSection["items"].splice(startIndex);
      }
    }
    return tmpSection;
  }

  // pushPLDatatoSections = data => {
  //   if (data.items) {
  //     if (data.sections) {
  //       const plData = {
  //         secname: data.secname,
  //         items: data.items,
  //         tn: "articlelist",
  //         seolocation: "elections/assembly-elections/bihar/news",
  //       };
  //       data.sections.push(plData);
  //     }
  //   }
  // };

  /*Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({ dataObj, datalabel, configlabel, override, isSectionHead, compType, sectionHeading, noSeo }) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    let sectionhead;
    sectionhead = sectionHeading || dataObj ? dataObj.secname : undefined;
    sectionhead = isSectionHead == false ? undefined : sectionhead;
    override = override ? override : {};

    try {
      if (dataObj && dataObj.items) {
        if (!Array.isArray(dataObj.items)) {
          let arrObj = [];
          arrObj.push(dataObj.items);
          dataObj.items = arrObj.slice();
        }
      }

      return dataObj && dataObj.items && dataObj.items.length > 0 ? (
        <React.Fragment>
          {sectionhead && (
            <SectionHeader
              sectionhead={sectionhead}
              weblink={dataObj && (dataObj.override || dataObj.wu)}
              sectionTag={dataObj && dataObj.tag ? dataObj.tag : ""}
              compType={compType}
            />
          )}

          <GridSectionMaker
            type={isMobilePlatform() ? defaultDesignConfigs[configlabel] : designConfigs[configlabel]}
            data={dataObj.items}
            override={override}
            compType={compType}
            noSeo={noSeo}
          />
        </React.Fragment>
      ) : // <FakeDesktopDefault />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const { isFetching } = this.props;

    let { sections, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    // push pl data in the sections
    // if (this.props.value && this.props.value[0]) {
    //   this.pushPLDatatoSections(this.props.value[0]);
    // }
    this.config.componentType = parentSection ? parentSection.tn : "";
    let compType = this.config.componentType;

    return this.props.value[0] && !isFetching ? (
      <div className={`${!isMobilePlatform() ? "section-wrapper list-component" : ""}`}>
        {/* Below code is used to render sections for Articlelist L1/L2 */}

        {!isMobilePlatform() &&
        parentSection.recommended &&
        parentSection.recommended.trendingtopics &&
        parentSection.recommended.trendingtopics[0] &&
        parentSection.recommended.trendingtopics[0].items ? (
          // &&
          // parentSection.recommended.trendingtopics[0].items.length > 0
          <div className="trending-bullet">
            <KeyWordCard items={parentSection.recommended.trendingtopics[0].items} secname=" " />
          </div>
        ) : null}

        {!isMobilePlatform() ? (
          <div className="btf-placeholder">
            <AdCard mstype="btf" adtype="dfp" />
          </div>
        ) : null}

        {isMobilePlatform() ? (
          <MobileSections
            compType={compType}
            parentSection={parentSection}
            sections={sections}
            getDataList={this.getDataList.bind(this)}
            createSectionLayout={this.createSectionLayout.bind(this)}
          />
        ) : (
          sections &&
          sections instanceof Array &&
          sections.map(item => {
            return <DesktopSections item={item} createSectionLayout={this.createSectionLayout.bind(this)} />;
          })
        )}

        {/* To show fake listing until data is data is rendered on screen */}
        {isFetching ? <FakeDesktopDefault showImages={true} /> : null}
      </div>
    ) : (
      <FakeDesktopDefault />
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.articlelist,
  };
}

const DesktopSections = props => {
  let { item, createSectionLayout } = props;
  let _item = JSON.parse(JSON.stringify(item));

  if (checkwidgetToRender(_item) && typeof _item.items == "object") {
    return item.tn == "photolist" ? (
      <div className="wdt_photo">
        <SectionHeader sectionhead={item.secname} weblink={item.override || item.wu} />

        <Slider
          margin="10"
          size="4"
          sliderData={item.items}
          width="230"
          type="grid"
          sliderClass="showcase"
          movesize="4"
        />
      </div>
    ) : item.tn == "articlelist" ? (
      <div className="row pd0">
        {createSectionLayout({
          dataObj: _item,
          configlabel: "newsSection",
        })}
      </div>
    ) : item.tn == "videolist" ? (
      <VideoSection videoData={item} />
    ) : null;
  } else {
    return null;
  }
};

const checkwidgetToRender = section => {
  const widgetLocArr = [
    "iplt20/news",
    "iplt20/video",
    "iplt20/photos",
    "elections/assembly-elections/bihar/news",
    "elections/assembly-elections/bihar/photos",
    "elections/assembly-elections/bihar/video",
    "elections/assembly-elections/west-bengal/news",
    "elections/assembly-elections/west-bengal/photos",
    "elections/assembly-elections/west-bengal/video",
    "elections/assembly-elections/tamil-nadu/news",
    "elections/assembly-elections/tamil-nadu/photos",
    "elections/assembly-elections/tamil-nadu/video",
    "elections/assembly-elections/assam/news",
    "elections/assembly-elections/assam/photos",
    "elections/assembly-elections/assam/video",
    "elections/assembly-elections/puducherry/news",
    "elections/assembly-elections/puducherry/photos",
    "elections/assembly-elections/puducherry/video",
    "elections/assembly-elections/kerala/news",
    "elections/assembly-elections/kerala/photos",
    "elections/assembly-elections/kerala/video",
  ];
  return widgetLocArr.some(item => section.seolocation.indexOf(item) > -1);
};

const MobileSections = props => {
  let { compType, parentSection, sections, getDataList, createSectionLayout } = props;

  return compType == "articlelist" && parentSection && sections != undefined
    ? sections.map((section, index) => {
        if (checkwidgetToRender(section)) {
          section = getDataList(section, 10);
          return (
            <div
              className={`box-item row ${
                section.tn == "photolist"
                  ? "photo_video_section"
                  : section.tn == "videolist"
                  ? "photo_video_section video"
                  : undefined
              }`}
            >
              {createSectionLayout({
                dataObj: section,
                configlabel:
                  section.tn == "articlelist"
                    ? "listGallery"
                    : section.tn == "photolist"
                    ? "gridHorizontalLeadAL"
                    : section.tn == "videolist"
                    ? "gallery"
                    : "listGallery", // here photolist/videolist means photo/video listing which are marked as special under articlelisting
                weblink: section.override || section.wu,
              })}
            </div>
          );
        } else {
          return null;
        }
      })
    : null;
};

export default connect(mapStateToProps)(VideoPhotoNewsCombined);
