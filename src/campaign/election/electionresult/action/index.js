import fetch from "./../../../../utils/fetch/fetch";
export const FETCH_ELECTIONRESULT_REQUEST = "FETCH_ELECTIONRESULT_REQUEST";
export const FETCH_ELECTIONRESULT_SUCCESS = "FETCH_ELECTIONRESULT_SUCCESS";
export const FETCH_ELECTIONRESULT_FAILURE = "FETCH_ELECTIONRESULT_FAILURE";

function fetchELECTIONRESULT_DataFailure(error) {
  return {
    type: FETCH_ELECTIONRESULT_FAILURE,
    payload: error.message
  };
}

function fetchELECTIONRESULT_DataSuccess(data) {
  return {
    type: FETCH_ELECTIONRESULT_SUCCESS,
    payload: data
  };
}

function fetchELECTIONRESULT_Data(state, params, query) {
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/sp_common_2019_results.htm`;
  // const _url = `https://toibnews.timesofindia.indiatimes.com/Election/ae/common/results.htm`;
  // const _url = `https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/may19/mobile_elections_merged_feeds.json`;
  // const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.json';

  return dispatch => {
    dispatch({ type: FETCH_ELECTIONRESULT_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          data = data
            .replace("times.mobile.election.electionresults(", "")
            .slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchELECTIONRESULT_DataSuccess(JSON.parse(data)));
        },
        error => dispatch(fetchELECTIONRESULT_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchELECTIONRESULT_DataFailure(error));
      });
  };
}

function fetchELECTIONRESULTPWA_Data(state, params, query) {
  //const _url = `https://toibnews.timesofindia.indiatimes.com/Election/election2017_sp_homepage_result_widget_2018_new.htm`;
  // const _url = `https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/may19/mobile_elections_merged_feeds.json`;
  // const _url = `https://toibnews.timesofindia.indiatimes.com/Election/ae/common/results.htm`;
  // const _pwaurl = process.env.API_BASEPOINT + '/pwafeeds/pwa_metalist.cms?msid=66895640&feedtype=sjson';
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/sp_common_2019_results.htm`;
  const _pwaurl =
    process.env.API_BASEPOINT +
    "/pwafeeds/pwa_metalist.cms?msid=" +
    params.msid +
    "&feedtype=sjson";

  return dispatch => {
    dispatch({ type: FETCH_ELECTIONRESULT_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          data = data
            .replace("times.mobile.election.electionresults(", "")
            .slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          data = JSON.parse(data);
          dispatch({
            type: FETCH_ELECTIONRESULT_REQUEST
          });
          return fetch(_pwaurl)
            .then(
              pwadata => {
                data["pwadata"] = pwadata;
                dispatch(fetchELECTIONRESULT_DataSuccess(data));
              },
              error => dispatch(fetchELECTIONRESULT_DataFailure(error))
            )
            .catch(error => {
              dispatch(fetchELECTIONRESULT_DataFailure(error));
            });
        },
        error => dispatch(fetchELECTIONRESULT_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchELECTIONRESULT_DataFailure(error));
      });
  };
}

function shouldFetchELECTIONRESULT_Data(state, params, query) {
  // return (typeof state.electionresult.data == 'undefined');
  return true;
}

export function fetchELECTIONRESULT_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchELECTIONRESULT_Data(getState(), params, query)) {
      return dispatch(fetchELECTIONRESULT_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

export function fetchELECTIONRESULTPWA_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchELECTIONRESULT_Data(getState(), params, query)) {
      return dispatch(fetchELECTIONRESULTPWA_Data(getState(), params, query));
    } else {
      return Promise.resolve({});
    }
  };
}

export function fetchELECTIONRESULT_Data_Polling(dispatch) {
  // const _url = `https://toibnews.timesofindia.indiatimes.com/Election/election2017_sp_homepage_result_widget_2018_new.htm`;
  // const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.htm';
  // const _url = `https://toibnews.timesofindia.indiatimes.com/Election/ae/common/results.htm`;
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/sp_common_2019_results.htm`;

  dispatch({ type: FETCH_ELECTIONRESULT_REQUEST });

  return fetch(_url, { type: "jsonp" })
    .then(
      data => {
        data = data
          .replace("times.mobile.election.electionresults(", "")
          .slice(0, -1);
        if (!data)
          throw new NetworkError("Invalid JSON Response", response.status);
        dispatch(fetchELECTIONRESULT_DataSuccess(JSON.parse(data)));
      },
      error => dispatch(fetchELECTIONRESULT_DataFailure(error))
    )
    .catch(function(error) {
      dispatch(fetchELECTIONRESULT_DataFailure(error));
    });
}
