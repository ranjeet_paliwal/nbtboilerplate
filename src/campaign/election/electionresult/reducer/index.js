import {
  FETCH_ELECTIONRESULT_REQUEST,
  FETCH_ELECTIONRESULT_SUCCESS,
  FETCH_ELECTIONRESULT_FAILURE
} from "./../action/index";

function ELECTIONRESULT(state = {}, action) {
  switch (action.type) {
    case FETCH_ELECTIONRESULT_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_ELECTIONRESULT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_ELECTIONRESULT_SUCCESS:
      state = { data: action.payload };
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default ELECTIONRESULT;
