import React, { Component } from "react";
import { connect } from "react-redux";

import styles from "./../../common/css/Election.scss";
import PieTableItems from "./../../common/component/PieTableItems";
import { fetchELECTIONRESULTPWA_IfNeeded, fetchELECTIONRESULT_Data_Polling } from "../action";
import { AnalyticsGA } from "../../../../components/lib/analytics";
import { PageMeta } from "./../../../../components/common/PageMeta";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import AnchorLink from "../../../../components/common/AnchorLink";
import { _isCSR, setHyp1Data, _isFrmApp, getPageType } from "../../../../utils/util";
import AdCard from "../../../../components/common/AdCard";
import Ads_module from "../../../../components/lib/ads";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import WebTitleCard from "../../../../components/common/WebTitleCard";

import { _getStaticConfig } from "./../../../../utils/util";
import { electionConfig } from "./../../utils/config";
const _electionConfig = electionConfig[process.env.SITE];

const siteConfig = _getStaticConfig();

class Electionresult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderPie: false,
    };
    this.config = {
      pollingInterval: 30000,
      pollingClear: false,
      pwadata: {},
    };
    this.dataPooling = this.dataPooling.bind(this);
  }
  componentDidMount() {
    let _this = this;
    const { dispatch, params, query } = _this.props;

    Electionresult.fetchData({ dispatch, params, query }).catch(err => {
      console.log(err.message);
    });

    _this.dataPooling();

    //set hyp1 variable
    setHyp1Data({ hyp1: "election_campaign" });
    Ads_module.setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
  }

  componentWillUnmount() {
    //reset hyp1 variable
    setHyp1Data();

    // reset  section details
    Ads_module.setSectionDetail();

    //clear timer
    clearInterval(this.config.timer);
  }

  dataPooling() {
    let _this = this;
    const { dispatch, params, query, appdata } = _this.props;
    if (appdata && appdata.election.result.isdatapolling == "false") {
      return false;
    }
    _this.config.timer = setInterval(() => {
      dispatch(fetchELECTIONRESULT_Data_Polling(dispatch));
      //   if (_this.config.pollingClear) clearInterval(timer);
    }, window.pollingInterval || _this.config.pollingInterval);
  }

  render() {
    let isFrmApp = _isFrmApp(this.props.router);
    let _this = this;

    let frmAppLink = isFrmApp ? "?frmapp=yes" : "";
    let { data, pagetype, router, config } = this.props;
    let arrData, breadcrumbData;
    let mapStateMSID = {
      maharashtra: "71452334",
      haryana: "71452456",
    };

    this.config.mapMSID = _electionConfig.mapResultMSID;
    this.config.mapElectoralMSID = _electionConfig.mapElectoralMSID;

    if (data) {
      if (!pagetype && router) {
        pagetype = getPageType(router.location.pathname);
      }
      breadcrumbData =
        this.props.data &&
        this.props.data.pwadata &&
        this.props.data.pwadata.breadcrumb &&
        this.props.data.pwadata.breadcrumb.div &&
        this.props.data.pwadata.breadcrumb.div.ul
          ? this.props.data.pwadata.breadcrumb.div.ul
          : null;

      arrData = Array.from(Object.keys(data), key => {
        if (key == "items") return data[key];
        let _data = data[key];
        if (typeof _data == "object") {
          _data.state = key.toLocaleLowerCase().replace("_", "-");
        }
        return _data;
      });
      this.config.pwadata = this.config.pwadata.pwa_meta
        ? this.config.pwadata.pwa_meta
        : data.pwadata
        ? data.pwadata
        : {};

      //Temp handling for NP
      let isFrmNP = false;
      if (_isCSR() && location.search.indexOf("frmnp=yes") > -1) {
        isFrmNP = true;
      }
    }

    return arrData && arrData.length > 0 ? (
      <ErrorBoundary>
        <div className="elm_container election_con">
          {/* For SEO Meta */
          this.config.pwadata.pwa_meta ? PageMeta(this.config.pwadata.pwa_meta) : null}
          {/* <div className="sectionheading">
              {(this.config.pwadata && this.config.pwadata.pwa_meta) ? <h1>{this.config.pwadata.pwa_meta.pageheading}</h1> : "विधानसभा चुनाव परिणाम"}
              {
                isFrmNP ?
                  null :
                  <select onChange={(e) => window.location.href = e.target.value}>
                    <option value={`/elections/assembly-elections/results${frmAppLink}`}>All</option>
                    <option value={`/elections/assembly-elections/madhya-pradesh/results${frmAppLink}`}>MP</option>
                    <option value={`/elections/assembly-elections/rajasthan/results${frmAppLink}`}>RAJASTHAN</option>
                    <option value={`/elections/assembly-elections/telangana/results${frmAppLink}`}>TELANGANA</option>
                    <option value={`/elections/assembly-elections/chhattisgarh/results${frmAppLink}`}>CHHATTISGARH</option>
                    <option value={`/elections/assembly-elections/mizoram/results${frmAppLink}`}>MIZORAM</option>
                  </select>
              }
            </div> */}
          <div className="result_inner">
            <h1
              dangerouslySetInnerHTML={{
                __html: `${_electionConfig.assemblyResultHeading}`,
              }}
            />
            {arrData && arrData.length > 0 ? null : (
              <div className="ctyLoaderCont" data-ele="cg-graph-loader">
                <img
                  style={{ fontSize: "10px" }}
                  src={`${siteConfig.weburl}/photo/47444548.cms`}
                  className="ctyLoader"
                />
              </div>
            )}
            {arrData && arrData.length > 0
              ? arrData.map((item, index) => {
                  let pieChartObj = true;
                  return item.al_rslt ? (
                    <div key={"resultfragment_" + item.state} className="resultfragment">
                      {
                        (pieChartObj = PieTableItems(
                          item.al_rslt,
                          index,
                          item.ttl_seat,
                          item.state,
                          true,
                          config.resultLbl,
                        ))
                      }

                      {!pieChartObj ? (
                        <div>
                          <h2 className="tbl_txt">
                            <u>
                              <a
                                href={`/elections/assembly-elections/${item.state}/results/${
                                  this.config.mapMSID[item.state]
                                }.cms`}
                              >
                                {" "}
                                {item.state.split("-").join(" ")}
                              </a>
                            </u>
                          </h2>

                          <RefreshBtnComponent />
                        </div>
                      ) : null}

                      {!isFrmApp ? (
                        <div className="btn_group">
                          <a
                            href={`/elections/assembly-elections/${item.state}/electoralmap/${
                              this.config.mapElectoralMSID[item.state]
                            }.cms${frmAppLink}`}
                          >
                            electoral map
                          </a>
                          {/* <a href={`/elections/assembly-elections/${item.state}/candidates-list${frmAppLink}`}>candidates</a> */}
                        </div>
                      ) : null}
                      <div className="result_source">Source: RebusCode</div>
                      {/*Ad MREC after 1st */}
                      {index == 0 && !isFrmApp ? <AdCard mstype="mrec1" /> : null}

                      {item.state.toLocaleLowerCase() == "rajasthan" ? (
                        <div className="disclaimer" style={{ whiteSpace: "pre-wrap" }}>
                          नोट : राजस्‍थान व‍िधानसभा की 200 में 199 सीटों पर हुआ मतदान, बहुमत‍ का आंकड़ा 100 है।
                        </div>
                      ) : null}
                    </div>
                  ) : null;
                })
              : null}
          </div>

          {_this.props.data.pwadata &&
            _this.props.data.pwadata.pwa_meta &&
            _this.props.data.pwadata.pwa_meta.seodescription &&
            WebTitleCard(_this.props.data.pwadata.pwa_meta)}
          {/* Breadcrumb  */}
          {breadcrumbData && pagetype != "home" && pagetype != "liveblog" ? (
            <ErrorBoundary>
              <Breadcrumb items={breadcrumbData} />
            </ErrorBoundary>
          ) : null}
        </div>
      </ErrorBoundary>
    ) : null;
  }
}

const RefreshBtnComponent = () => {
  return (
    <div className="refresh_btn">
      <a
        href="javascript:void(0);"
        data-ele="refresh"
        onClick={() => {
          window.location = window.location.href;
        }}
      >
        <img style={{ fontSize: "10px" }} src={`${siteConfig.weburl}/photo/45565565.cms`} alt="" className="ex_res" />
        <span>Refresh</span>
      </a>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    ...state.electionresult,
    config: state.config,
  };
}

Electionresult.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchELECTIONRESULTPWA_IfNeeded(params, query));
};

export default connect(mapStateToProps)(Electionresult);
