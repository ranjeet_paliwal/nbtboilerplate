import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";

import styles from "./../../common/css/Election.scss";
import drawPieChart from "./../../../../modules/charts/piechart";
import { _isCSR, silentRedirect, _getStaticConfig } from "./../../../../utils/util";
import { fetchELECTIONRESULT_IfNeeded, fetchELECTIONRESULT_Data_Polling } from "./../../electionresult/action";
import { AnalyticsGA } from "../../../../components/lib/analytics";
import AnchorLink from "../../../../components/common/AnchorLink";
import { constants } from "../../utils/constants";
import { electionConfig } from "../../utils/config";

const _electionConfig = electionConfig[process.env.SITE];
const siteConfig = _getStaticConfig();

class ElectionWidget extends Component {
  constructor(props) {
    super(props);
    this.config = {
      pollingInterval: 30000,
      pollingClear: false,
      renderPie: false,
    };
    this.state = {
      renderPie: false,
    };
    this.config = {
      coustomData: [],
      pollingInterval: 30000,
    };
    this.dataPooling = this.dataPooling.bind(this);
  }

  componentDidMount() {
    let _this = this;
    const { dispatch, params, query } = _this.props;

    ElectionWidget.fetchData({ dispatch, params, query }).catch(err => {
      console.log(err.message);
    });

    _this.dataPooling();
  }

  componentWillUnmount() {
    // clear timer
    clearInterval(this.config.timer);
  }

  dataPooling() {
    let _this = this;
    const { dispatch, params, query, appdata } = _this.props;
    this.config.renderPie = true;

    if (appdata && appdata.election.result.isdatapolling == "false") {
      return false;
    }
    _this.config.timer = setInterval(() => {
      fetchELECTIONRESULT_Data_Polling(dispatch);
      //first silent redirect
      //silentRedirect('/elections/assembly-elections/results');
      //hit page view for election result page
      //AnalyticsGA.pageview('/elections/assembly-elections/results')
      //if (_this.config.pollingClear) clearInterval(timer);
    }, window.pollingInterval || _this.config.pollingInterval);
  }

  getAppDowld = () => {
    window.open(
      "https://navbharattimes.indiatimes.com/off-url-forward/appdownload_mweb.cms?wapcode=nbt&applink=https://navbharattimes.onelink.me/cMxT/406fd538",
    );
  };

  render() {
    let { data, isFrmapp, appdata, config } = this.props;
    let _link =
      `/elections/assembly-elections/results/${_electionConfig.assemblyResultsMSID}.cms` +
      (isFrmapp ? "?frmapp=yes" : "");

    let { renderPie } = this.config;
    let arrData = data
      ? Array.from(Object.keys(data), key => {
          if (key == "items") return Object.assign({}, data[key]);
          let _data = Object.assign({}, data[key]);
          if (typeof _data == "object") {
            _data.state = key.toLocaleLowerCase().replace("_", " ");
            if (_data.al_rslt instanceof Array && _data.al_rslt.length == 1) _data.al_rslt = _data.al_rslt[0];
          }
          return _data;
        })
      : [];
    return (
      <div className="wdt_election_result" data-exclude="amp">
        <div className="election_con chart-horizontal inner_box_widget" id="assemblyElectionWidget">
          <input type="checkbox" id="tableview" style={{ display: "none" }} />
          <div className="election_head">
            <h2>
              <AnchorLink href={_link}>
                <b
                  dangerouslySetInnerHTML={{
                    __html: `${_electionConfig.assemblyResultHeading}`,
                  }}
                />
                <span>{appdata.election.result.counttext}</span>
              </AnchorLink>
            </h2>
            <label className="el_resulttable" htmlFor="tableview">
              <i className="grid_view" data-ele="table-view" />
            </label>
            {isFrmapp ? null : (
              <a
                className="actionbtn"
                onClick={() => {
                  // AnalyticsGA.event({
                  //   category: "app_download",
                  //   action: "click",
                  //   label: "election_result_widget"
                  // });
                  this.getAppDowld();
                }}
              >
                App Download
              </a>
            )}
          </div>
          <AnchorLink id="eresult-container" className="electionWidget_con election_result_widget" href={_link}>
            <div className="eresult_table">{arrData && arrData.length > 0 ? TableWidget(arrData) : null}</div>
            <div className="chart-horizontal eresult_pie">
              {_isCSR() && arrData && arrData.length > 0
                ? arrData.map((item, index) => {
                    return item.al_rslt ? (
                      <React.Fragment key={"resultfragment" + index}>
                        {PieWidget(this, item.al_rslt, index, item.ttl_seat, item.state, renderPie, config.resultLbl)}
                      </React.Fragment>
                    ) : null;
                  })
                : null}
            </div>
            {/*<div className="disclaimer" style={{ "whiteSpace": "pre-wrap" }}>
            नोट : <sup>*</sup>राजस्‍थान व‍िधानसभा की 200 में 199 सीटों पर हुआ मतदान, बहुमत‍ का आंकड़ा 100 है।
            </div>*/}
          </AnchorLink>
          <div className="result_source">Source: RebusCode</div>
        </div>
      </div>
    );
  }
}

function TableWidget(arrData) {
  return (
    <table cellPadding="0" cellSpacing="0" border="0" data-attr="eticker-table">
      <tbody>
        {arrData.map((item, index) => {
          let chartObj = { total_leads: 0, total_wins: 0 };
          if (item.al_rslt) {
            item.al_rslt.map(item => {
              chartObj.total_leads += parseInt(item.ls);
              chartObj.total_wins += parseInt(item.ws);
            });
          }

          return item.al_rslt ? (
            <tr key={"tablerow" + chartObj.total_leads + index}>
              <td className="state">
                <div>
                  {item.state}
                  <sup>{item.state.toLocaleLowerCase() == "rajasthan" ? "*" : ""}</sup>
                </div>
                <span className="seats">
                  {chartObj.total_leads + chartObj.total_wins} / {item.ttl_seat}
                </span>
              </td>
              {item.al_rslt.map((item, index) => {
                return index < 4 ? (
                  <td className="party" key={"tablecol" + parseInt(item.ls) + index}>
                    <div className="seats">{parseInt(item.ws) + parseInt(item.ls)}</div>
                    <span>{item.an}</span>
                  </td>
                ) : null;
              })}
            </tr>
          ) : null;
        })}
      </tbody>
    </table>
  );
}

function PieWidget(_this, items, index, total, state, renderPie, resultLbl) {
  state =
    state == "madhya pradesh" || state == "madhya-pradesh"
      ? "mp"
      : state == "arunachal pradesh" || state == "arunachal-pradesh"
      ? "ar"
      : state == "andhra pradesh" || state == "andhra-pradesh"
      ? "ap"
      : state;
  let elemid = "elem" + Math.floor(Math.random() * 1000000);
  // let elemid = "elem" + state;
  let chartObj = {
    data: {},
    colors: [],
    radius: 100,
    canvasid: elemid,
    total_leads: 0,
    total_wins: 0,
    totalValue: parseInt(total),
  };
  items.map(item => {
    chartObj.data[item.an] = parseInt(item.ws) + parseInt(item.ls);
    chartObj.colors.push(item.cc);
    chartObj.total_leads += parseInt(item.ls);
    chartObj.total_wins += parseInt(item.ws);
  });

  chartObj.total_leads_wins = chartObj.total_leads + chartObj.total_wins;

  _this.config.pollingClear = total == chartObj.total_leads_wins ? true : false;

  return (
    <React.Fragment>
      {chartObj.total_leads_wins > 0 ? (
        <div className="pie-chart result_inner" data-ele="piechart_mp_0" key={"chart" + index}>
          <div className="el_graphdim chart highcharts-container">
            <div id={elemid} ref={() => drawPieChart(chartObj)} />
            <div visibility="visible" className="chart-detail">
              <div className="charttxt">
                <div className="chartInnerText">
                  <div className="seatcounted">{chartObj.total_leads + chartObj.total_wins}</div>
                  <div className="totalseat">{total}</div>
                  <div className="seatstatus">{resultLbl}</div>
                </div>
              </div>
              <div className="charttitle">
                {state && state != "" ? (
                  <div className="title">
                    <span>{state.toLocaleUpperCase()}</span>
                    <sup>{state.toLocaleLowerCase() == "rajasthan" ? "*" : ""}</sup>
                  </div>
                ) : null}
                <div className="subtitle">Majority Mark: {Math.floor(total / 2) + 1}</div>
              </div>
              <div className="titlebackground" />
            </div>
          </div>
          <div className="legend">
            <ul>
              {items.map((item, index) => {
                return parseInt(item.ws) + parseInt(item.ls) > 0 ? (
                  <li key={index}>
                    <div className="trend-text">{item.an}</div>
                    <div className="trend-color" style={{ background: `${item.cc}` }} />
                  </li>
                ) : null;
              })}
            </ul>
          </div>
        </div>
      ) : null}
    </React.Fragment>
  );
}

function mapStateToProps(state) {
  return {
    ...state.electionresult,
    config: state.config,
    appdata: state.app,
  };
}

ElectionWidget.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchELECTIONRESULT_IfNeeded(params, query));
};

export default connect(mapStateToProps)(ElectionWidget);
