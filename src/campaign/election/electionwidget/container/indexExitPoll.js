import React, { Component } from "react";

// import { Link } from "react-router";
import { connect } from "react-redux";

import { fetchExitPollIfNeeded } from "../../actions/exitpoll/exitpoll";
import { _isCSR, _getStaticConfig } from "../../../../utils/util";
// import FakeStoryCard from '../../../../components/common/FakeCards/FakeStoryCard';
// import WebTitleCard from '../../../../components/common/WebTitleCard';
import { PieTableWidget } from "../../common/component/ResultPieDotsWidget";

import TabsWidget from "../../common/component/TabsWidget";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { electionConfig } from "../../utils/config";
import AnchorLink from "../../../../components/common/AnchorLink";

const _electionConfig = electionConfig[process.env.SITE];
const siteConfig = _getStaticConfig();

class ExitPollWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      sourceType: "",
      stateName: "",
    };
    this.config = {
      custom_data: null,
      type: "",
      shouldRenderPie: "",
      stateItems: [],
      srcItems: [],
    };
  }

  componentDidMount() {
    let _this = this;
    const { dispatch, params, query, routeParams, data } = this.props;
    let __data = Object.assign({}, data);

    ExitPollWidget.fetchData({ dispatch, params, query })
      .then(function(_data) {
        if (_this.config.srcItems.length == 0) {
          if (_data && _data.payload) __data = _data.payload;
          if (__data && !__data.isFetching && Object.keys(__data).length > 0) {
            _this.setStateData(__data);
            _this.state.stateName != "" ? _this.setSourceArr(__data, _this.state.stateName) : null;
          }
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  onTabClick(event) {
    let _this = this;
    let { data } = _this.props;
    let elem = event.currentTarget;

    elem.parentElement.querySelectorAll("li").forEach(item => item.classList.remove("active"));
    elem.classList.add("active");

    if (elem.getAttribute("data-type") == "state") {
      let stateKey = elem.getAttribute("data-name");
      this.setSourceArr(data, stateKey);
      _this.setState({ stateName: elem.getAttribute("data-name") });
    } else {
      //select Box implementation , click check box if its isSelectBox
      if (
        elem.parentElement.previousSibling &&
        elem.parentElement.previousSibling.classList.value == "toggleCheckBox"
      ) {
        let is_Checked = elem.parentElement.previousSibling.checked;
        elem.parentElement.previousSibling.checked = !elem.parentElement.previousSibling.checked;

        if (!is_Checked) return false;
      }
      this.setSourceArr(data, _this.state.stateName);
      // _this.setState({ sourceType: elem.getAttribute('data-name') });
      // _this.state.sourceType = elem.getAttribute('data-name');
    }

    // _this.config.shouldRenderPie = true;
  }

  setStateData(data) {
    let _this = this;

    try {
      _this.config.stateItems = [];
      //iternate the state name keys and check each sourch those  for which combined data is not zero
      //then push to stateItems
      data &&
        Object.keys(data).map(function(statename, i) {
          let pushToState = false;
          data[statename] &&
            data[statename].al_rslt &&
            data[statename].al_rslt.map(function(item, i) {
              if (parseInt(item[0].ws) + parseInt(item[0].ls) > 0) {
                pushToState = true;
              }
            });
          pushToState ? _this.config.stateItems.push({ nm: statename }) : "";
        });

      //then push remaining states stateItems by checking non-duplicacy
      if (_this.config.stateItems.length > 0) {
        // data && Object.keys(data).map(function (statename, i) {
        //     let pushToState = true;
        //     _this.config.stateItems.map(function(item,i){
        //         if(statename == item.nm){
        //             pushToState = false;
        //         }
        //     })
        //     if(pushToState) _this.config.stateItems.push({ 'nm': statename })
        // })
        //set first as state name
        _this.state.stateName = _this.config.stateItems[0] ? _this.config.stateItems[0].nm : "";
      }
    } catch (e) {}
  }

  setSourceArr(data, statename) {
    let _this = this;
    this.config.srcItems = [];

    try {
      let selectBoxElem;
      let activeElemDataName;
      let _sourceType = undefined;

      if (_isCSR()) {
        selectBoxElem = document.querySelector(".ae_exitpoll_widget .tabs_select_box");
        activeElemDataName =
          selectBoxElem && selectBoxElem.querySelector("li.active")
            ? selectBoxElem.querySelector("li.active").getAttribute("data-name")
            : "";
      }
      //iterate source in that particular state
      //and push only which have more then 0 count
      if (statename && statename != "") {
        data[statename] &&
          data[statename].al_rslt &&
          data[statename].al_rslt.map(function(item, i) {
            if (parseInt(item[0].ws) + parseInt(item[0].ls) > 0) {
              //check active state name src
              //if its active set it active
              if (activeElemDataName == item[0].src) {
                // if(_this.config.srcItems.length == 0) {
                _sourceType = item[0].src;
                _this.config.srcItems.push({ nm: item[0].src, active: true });
              } else {
                _this.config.srcItems.push({ nm: item[0].src, active: false });
              }
            }
          });
        if (_this.config.srcItems.length > 0) {
          //if no active class present set first item as active
          if (!_sourceType) _this.config.srcItems[0].active = true;
          if (!_isCSR()) _this.state.sourceType = _sourceType || _this.config.srcItems[0].nm;
          _this.setState({
            sourceType: _sourceType || _this.config.srcItems[0].nm,
          });
        }
      }
    } catch (e) {}
  }

  modifyStateName(stateName) {
    let stateNameArr = [],
      actualStateName = "";

    if (stateName.indexOf("_") > -1) {
      stateNameArr = stateName.split("_");
      for (let i = 0; i < stateNameArr.length; i++) {
        stateNameArr[i] = stateNameArr[i].charAt(0).toLowerCase() + stateNameArr[i].slice(1);
      }
      actualStateName = stateNameArr.join("-");
    } else {
      actualStateName = stateName.charAt(0).toLowerCase() + stateName.slice(1);
    }
    return actualStateName;
  }

  createPropsData(data) {
    let _this = this;
    if (data && _this.state.stateName != "" && _this.state.sourceType != "") {
      try {
        data[_this.state.stateName] &&
          data[_this.state.stateName].al_rslt.map(function(item, i) {
            if (item[0].src == _this.state.sourceType) {
              _this.config.propsData = {
                items: item,
                ttl_seats: data[_this.state.stateName].ttl_seat,
              };
            }
          });
      } catch (e) {}
    }
  }

  render() {
    let { data, isFetching, pagetype } = this.props;
    let _this = this;
    let __data = Object.assign({}, data);

    if (!isFetching && _this.config.srcItems.length == 0 && __data && Object.keys(__data).length > 0) {
      _this.setStateData(__data);
      _this.state.stateName != "" ? _this.setSourceArr(__data, _this.state.stateName) : null;
    }

    _this.createPropsData(__data);

    if (data && _this.config.stateItems && _this.config.stateItems.length > 0) {
      _this.config.mapMSID = _electionConfig.mapExitpollMSID;
      let changeStateName = this.modifyStateName(_this.state.stateName);
      let _msid = this.config.mapMSID[changeStateName];
      return (
        <div className="wdt_election_result ae_exitpoll_widget">
          {
            <div className="inner_box_widget white-bg">
              <h1>ASSEMBLY EXIT POLL 2019</h1>
              <span
                className="share_icon"
                style={{ top: "30px", right: "20px" }}
                onClick={() => {
                  window.location.href = window.location.href;
                }}
              >
                <svg
                  version="1.1"
                  id="Layer_1"
                  x="0px"
                  y="0px"
                  width="30px"
                  height="30px"
                  viewBox="0 0 512 512"
                  enableBackground="new 0 0 512 512"
                >
                  <g>
                    <path d="M479.971,32.18c-21.72,21.211-42.89,43-64.52,64.301c-1.05,1.23-2.26-0.16-3.09-0.85   c-24.511-23.98-54.58-42.281-87.221-52.84c-37.6-12.16-78.449-14.07-117.029-5.59c-68.67,14.67-128.811,64.059-156.44,128.609   c0.031,0.014,0.062,0.025,0.093,0.039c-2.3,4.537-3.605,9.666-3.605,15.1c0,18.475,14.977,33.451,33.451,33.451   c15.831,0,29.084-11.002,32.555-25.773c19.757-41.979,58.832-74.445,103.967-85.527c52.2-13.17,111.37,1.33,149.4,40.041   c-22.03,21.83-44.391,43.34-66.33,65.26c59.52-0.32,119.06-0.141,178.59-0.09C480.291,149.611,479.931,90.891,479.971,32.18z" />
                    <path d="M431.609,297.5c-14.62,0-27.041,9.383-31.591,22.453c-0.009-0.004-0.019-0.008-0.027-0.012   c-19.11,42.59-57.57,76.219-102.84,88.18c-52.799,14.311-113.45,0.299-152.179-39.051c21.92-21.76,44.369-43.01,66.189-64.869   c-59.7,0.049-119.41,0.029-179.11,0.01c-0.14,58.6-0.159,117.189,0.011,175.789c21.92-21.91,43.75-43.91,65.79-65.699   c14.109,13.789,29.76,26.07,46.92,35.869c54.739,31.971,123.399,38.602,183.299,17.891   c57.477-19.297,106.073-63.178,131.212-118.318c3.645-5.357,5.776-11.824,5.776-18.793C465.06,312.477,450.083,297.5,431.609,297.5   z" />
                  </g>
                </svg>
              </span>
              <ErrorBoundary>
                <TabsWidget
                  onTabClick={_this.onTabClick.bind(_this)}
                  tabtype="tabs_normal"
                  datatype="state"
                  items={_this.config.stateItems}
                />
                <TabsWidget
                  onTabClick={_this.onTabClick.bind(_this)}
                  tabtype="tabs_normal"
                  datatype="source"
                  items={_this.config.srcItems}
                  isSelectBox={true}
                />
              </ErrorBoundary>
              {data && _this.config.propsData ? (
                <ErrorBoundary>
                  <PieTableWidget
                    data={_this.config.propsData}
                    type={_this.config.type}
                    shouldRenderPie={_this.config.shouldRenderPie}
                    anchorLink={`/elections/assembly-elections/${changeStateName}/exitpolls/${_msid}.cms`}
                    pagetype={pagetype}
                  />
                </ErrorBoundary>
              ) : null}
            </div>

            // })
            // : null
          }
        </div>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  return {
    ...state.exitpoll,
  };
}

ExitPollWidget.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchExitPollIfNeeded(params, query));
};

export default connect(mapStateToProps)(ExitPollWidget);
