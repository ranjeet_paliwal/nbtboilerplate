import React from "react";
import PropTypes from "prop-types";

import { _getStaticConfig, isMobilePlatform } from "../../../utils/util";
const siteConfig = _getStaticConfig();

const Table = props => {
  const {
    items,
    _index,
    total,
    type,
    isPrevYear,
    headRowData,
    hideTotalRow,
    hideTotalInWidget,
    hideSource,
    greaterThanZero,
    source,
    countText,
  } = props;

  let leadWinsCount = 0;

  const headRow = headRowData || [type === "Results" ? countText || "Results" : "Projected"];

  let drawFlag = true;
  if (greaterThanZero) {
    if (parseInt(items[0].ws, 10) + parseInt(items[0].ls, 10) > 0) {
      drawFlag = true;
    } else {
      drawFlag = false;
    }
  }

  return items.length > 0 && drawFlag ? (
    // return items.length > 0 && (isPrevYear || parseInt(items[0].ws, 10) + parseInt(items[0].ls, 10) > 0) ? (
    <div data-ele="table_mp_0" key={_index} className="table-chart">
      {!isMobilePlatform() && !hideSource && (items[0].src || source) ? (
        <div className="clearfix powerbycont mp ">
          <div className="el_powerdby">Source: {items[0].src || source}</div>
        </div>
      ) : null}
      <div className="result-tbl">
        <div className="tbl_heading">
          <div className="tbl_col  first">Party</div>
          {headRow.map(item => (
            <div className="tbl_col  tac" key={item}>
              {item}
            </div>
          ))}
        </div>
        {items.map((item, index) => {
          leadWinsCount += parseInt(item.ws, 10) + parseInt(item.ls, 10);
          let isGreater = parseInt(item.ws, 10) + parseInt(item.ls, 10) - parseInt(item.pws, 10) > 0 ? true : false;
          return (
            <div className="tbl_row" key={index.toString()}>
              <div className="tbl_col col party_logo">
                {/* {item.lg && <img alt="logo" src={`${siteConfig.weburl}/photo/${item.lg}.cms`} />} */}
                <span className="partyclr" style={{ background: `${item.cc || "#c0c0c0"}` }}>
                  {item.an}
                </span>
              </div>
              {/* <td className="tbl_col tac col1 ">{parseInt(item.ws)}</td> */}
              <div className="tbl_col tac col ">{parseInt(item.ws, 10) + parseInt(item.ls, 10)}</div>
              {isPrevYear ? <div className="tbl_col tac col ">{parseInt(item.pws, 10)}</div> : ""}
              {isPrevYear ? (
                <div
                  className="tbl_col tac col "
                  style={{
                    fontWeight: "bold",
                    color: isGreater ? "green" : "red",
                  }}
                >
                  {(isGreater ? "+" : "") + (parseInt(item.ws, 10) + parseInt(item.ls, 10) - parseInt(item.pws, 10))}
                </div>
              ) : (
                ""
              )}
            </div>
          );
        })}

        <div className="tbl_row">
          {!hideTotalRow && !hideTotalInWidget && (
            <React.Fragment>
              <div className="tbl_col col1 party_logo">
                <span>Majority Mark:</span> {Math.floor(total / 2) + 1}
              </div>
              <div className="tbl_col col1 tac">
                <span>TOTAL SEATS</span> : {`${leadWinsCount}/${total}`}
              </div>
            </React.Fragment>
          )}
        </div>
      </div>
      {isMobilePlatform() && !hideSource && (items[0].src || source) ? (
        <div className="clearfix powerbycont mp ">
          <div className="el_powerdby">Source: {items[0].src || source}</div>
        </div>
      ) : null}
    </div>
  ) : null;
};

Table.propTypes = {
  items: PropTypes.array,
  _index: PropTypes.number,
  total: PropTypes.number,
  type: PropTypes.string,
  hideTotalRow: PropTypes.bool,
  hideTotalInWidget: PropTypes.bool,
  hideSource: PropTypes.bool,
  greaterThanZero: PropTypes.bool,
};

function arePropsEqual(prevProps, nextProps) {
  let itemFlag = false;

  if (prevProps.items && nextProps.items) {
    itemFlag = JSON.stringify(prevProps.items) === JSON.stringify(nextProps.items);
  }

  return itemFlag && prevProps.total === nextProps.total;
}

export default React.memo(Table, arePropsEqual);
