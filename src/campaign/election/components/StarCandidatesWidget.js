/* eslint-disable indent */
/* eslint-disable no-param-reassign */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "../common/css/Election.scss";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import Slider from "../../../components/desktop/Slider/Slider";
import { filterCandidateData } from "../utils/util";
import { isMobilePlatform } from "../../../utils/util";
import { electionConfig } from "../utils/config";
import fetch from "../../../utils/fetch/fetch";
import FakeProfileList from "../../../components/common/FakeCards/FakeProfileList";
import { setAutoRefresh, removeAutoRefresh } from "../actions/candidatelisting/candidatelisting";

const siteElectionConfig = electionConfig[process.env.SITE];
const refreshInterval = 15000;

class StarCandidatesWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      starcandidatesData: "",
      isLoading: true,
    };
  }

  componentDidMount() {
    const { msid } = this.props;
    if (msid !== "undefined") {
      this.fetchCandidateData(msid);
    } else {
      // this.setState({ isLoading: false });
      this.fetchCandidateData();
    }
  }

  componentWillUnmount() {
    this.props.dispatch(removeAutoRefresh());
  }

  componentDidUpdate(prevProps) {
    const { dispatch } = this.props;
    if (prevProps.stateName !== this.props.stateName) {
      if (prevProps.candidatelisting.refreshProcessId) {
        dispatch(removeAutoRefresh());
      }
      // caution - don't put any check on the state
      // bad practice used here of updating state from componentdidupdate lifecycle method
      this.setState({ isLoading: true }, () => {
        this.fetchCandidateData();
      });
    }
  }

  fetchStarCandidateData = url => {
    fetch(url, { type: "jsonp" }).then(candidateData => {
      let parsedData;
      if (typeof data === "string") {
        candidateData = candidateData.replace(/[^\(]*/, ""); // replace anything before first occurance of "("
        candidateData = candidateData.substring(1, candidateData.length - 1); // remove first and last character of the string to make it parsable
        parsedData = JSON.parse(candidateData);
      } else {
        parsedData = candidateData;
      }
      if (this.props.stateName && !parsedData.cnt_rslt) {
        parsedData = parsedData[this.props.stateName];
      }
      // let data = candidateData.replace(/[^\(]*/, ""); // replace anything before first occurance of "("
      // data = data.substring(1, data.length - 1); // remove first and last character of the string to make it parsable
      // data = JSON.parse(data);
      const starcandidatesData = parsedData.cnt_rslt ? filterCandidateData(parsedData.cnt_rslt) : [];
      this.setState({ starcandidatesData, isLoading: false });
    });
  };

  fetchCandidateData(msid) {
    const { configData, electionData, dispatch } = this.props;
    const autoRefreshFlag = electionData.starcandrefresh && electionData.starcandrefresh === "true";
    let url = `${process.env.API_BASEPOINT}/feed_elections.cms?feedtype=sjson`;
    url += this.props.stateName
      ? `&location=${this.props.stateName
          .replace(/_/g, "")
          .replace(/-/, "")
          .toLowerCase()}`
      : "";
    url += msid ? `&msid=${msid}` : "";
    try {
      fetch(url).then(response => {
        if (response && response[0] && response[0].starcandidateurl) {
          this.fetchStarCandidateData(response[0].starcandidateurl, this);
          if (autoRefreshFlag) {
            if (this.props.candidatelisting.refreshProcessId) {
              dispatch(removeAutoRefresh());
            }
            const refreshProcessId = setInterval(() => {
              this.setState({ isLoading: true }, () => {
                this.fetchStarCandidateData(response[0].starcandidateurl, this);
              });
            }, refreshInterval);
            dispatch(setAutoRefresh(refreshProcessId));
          }
        } else {
          this.setState({ isLoading: false });
        }
      });
    } catch (e) {
      this.setState({ isLoading: false });
    }
    // if (configData && configData[0] && configData[0].starcandidateurl) {
    //   this.fetchStarCandidateData(configData[0].starcandidateurl);
    //   if (autoRefreshFlag) {
    //     if (this.props.refreshProcessId) {
    //       dispatch(removeAutoRefresh());
    //     }
    //     const refreshProcessId = setInterval(() => {
    //       this.setState({ isLoading: true }, () => {
    //         this.fetchStarCandidateData(configData[0].starcandidateurl, this);
    //       });
    //     }, refreshInterval);
    //     dispatch(setAutoRefresh(refreshProcessId));
    //   }
    // } else {
    // }
  }

  render() {
    let { showCandidateStatus } = this.props;
    if (showCandidateStatus && showCandidateStatus === "false") {
      showCandidateStatus = false;
    } else {
      showCandidateStatus = true;
    }

    if (this.state.isLoading) {
      return <FakeProfileList elemSize={isMobilePlatform() ? "3" : this.props.size || "4"} cardheight="200" />;
    }
    return this.state.starcandidatesData ? (
      <div className="star_candidates_widget inner_box_widget">
        <SectionHeader sectionhead={siteElectionConfig.starCandidateTxt} headerClass="noborder" />
        <div>
          <Slider
            margin="15"
            size={this.props.size}
            sliderData={this.state.starcandidatesData}
            width="140"
            type="starCandidates"
            movesize={this.props.size}
            sliderClass="candidates-newlist"
            showCandidateStatus={showCandidateStatus}
          />
        </div>
      </div>
    ) : null;
  }
}

StarCandidatesWidget.propTypes = {
  size: PropTypes.number,
  msid: PropTypes.any,
  showCandidateStatus: PropTypes.any,
  dispatch: PropTypes.func,
  configData: PropTypes.object,
  electionData: PropTypes.object,
  refreshProcessId: PropTypes.any,
};

function mapStateToProps(state) {
  return {
    ...state.electoralmap,
    electionData: state.app.election,
    candidatelisting: state.candidatelist,
  };
}

export default connect(mapStateToProps)(StarCandidatesWidget);
