import React from "react";
import PropTypes from "prop-types";
import SvgIcon from "../../../components/common/SvgIcon";

const ChartTableToggleButton = props => (
  <div
    className="clearfix el_resulttable"
    tabIndex={0}
    role="button"
    onKeyUp={() => props.toggleChart()}
    onClick={() => props.toggleChart()}
    data-exclude="amp"
  >
    <i data-ele="result-view">
      {props.isTablurForm ? (
        <SvgIcon name="chartview" className="chartview" />
      ) : (
        <SvgIcon name="tableview" className="tableview" />
      )}
    </i>
  </div>
);

ChartTableToggleButton.propTypes = {
  toggleChart: PropTypes.func,
  isTablurForm: PropTypes.bool,
};

export default ChartTableToggleButton;
