/* eslint-disable no-param-reassign */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import styles from "../common/css/Election.scss";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import Table from "./Table";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";

import { fetchElectionConfigData } from "../actions/electoralmap/electoralmap";
import { fetchResultsIfNeeded } from "../actions/results/results";
import {
  countSeats,
  modifyStateName,
  getFeedUrlObjectFromElectionConfigData,
  generateStateResultslLinks,
} from "../utils/util";
import { isMobilePlatform } from "../../../utils/util";
import { electionConfig } from "../utils/config";

const siteElectionConfig = electionConfig[process.env.SITE];

class ResultsASWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedStatePosition: 0,
    };
  }

  componentDidMount() {
    const { query, dispatch, location, electionyear } = this.props;
    let { params } = this.props;
    if (!params) {
      params = {};
    }
    if (location) {
      params.location = location;
    }
    if (electionyear) {
      params.electionyear = electionyear;
    }
    ResultsASWidget.fetchData({ dispatch, params, query });
  }

  getConstituencyData = (data, id) => {
    let constituencydata = [];
    if (data && data.cns_rslt) {
      constituencydata = data.cns_rslt.filter(individualdata => individualdata.cns_id == id);
    }
    return constituencydata[0];
  };

  handleStateChange = index => {
    this.setState({ selectedStatePosition: index });
  };

  render() {
    const { data, config, electionData, constituencyid, electionyear } = this.props;
    if (!data) {
      return null;
    }
    let stateName;
    let renderData = {};
    let sequenceArray = [];
    const position = this.state.selectedStatePosition;
    if (!constituencyid && !data.al_rslt && Object.keys(data).length > 0) {
      sequenceArray = config && config[0].sequence ? config[0].sequence : Object.keys(data);
      stateName = modifyStateName({ stateName: sequenceArray[position] });
      renderData = data[sequenceArray[position]];
    } else {
      stateName = config && config[0] ? modifyStateName({ stateName: config[0].state }) : "Other";
      renderData = (data && data[stateName.replace(/-/g, "_")]) || {};
    }
    const statemsid = siteElectionConfig.resultMsidStateMap[stateName.toLowerCase()];
    const srcwiseTableData = makeSourcewisemapFromData(renderData.al_rslt);
    const total = renderData.ttl_seat || 0;
    const src = Object.keys(srcwiseTableData)[0];
    if (constituencyid) {
      const constituencyData = this.getConstituencyData(renderData, constituencyid);
      return constituencyData ? (
        <ConstituencyResult constituencyData={constituencyData} electionyear={electionyear} />
      ) : null;
    }
    return src ? (
      <div className="election-as-widget resultwdt">
        {sequenceArray.length > 1 && (
          <div className="statestoggle">
            <div className="states">
              {sequenceArray.map((elem, index) => (
                  <span
                    className={position === index ? "active" : ""}
                    onClick={() => this.handleStateChange(index)}
                    role="button"
                    tabIndex={0}
                    onKeyPress={() => this.handleStateChange(index)}
                  >
                    <b>{modifyStateName({ stateName: elem })}</b>
                  </span>
              ))}
            </div>
          </div>
        )}
        <div className="row">
          {!isMobilePlatform() && (
            <div className="col6 source-container">
              <SectionHeader
                sectionhead={`${stateName.toUpperCase()} ELECTION RESULTS ${
                  electionyear || (config && config[0] && config[0].resultyear) ? config[0].resultyear : ""
                }`}
                weblink={generateStateResultslLinks(stateName.toLowerCase(), statemsid)}
              />
              {total > 0 && (
                <div className="info-wrapper">
                  <div>
                    <span>Total Seats</span> : {total}
                  </div>
                  <div>
                    <span>Majority Mark</span> : {Math.floor(total / 2) + 1}
                  </div>
                </div>
              )}
            </div>
          )}
          <div className={`${isMobilePlatform() ? "col12" : "col6"} table container`}>
            {isMobilePlatform() && (
              <SectionHeader
                sectionhead={`${stateName.toUpperCase()} ELECTION RESULTS ${
                  electionyear || (config && config[0] && config[0].resultyear) ? config[0].resultyear : ""
                }`}
                weblink={generateStateResultslLinks(stateName.toLowerCase(), statemsid)}
              />
            )}
            {src && (
              <ErrorBoundary>
                <Table
                  items={srcwiseTableData[src]}
                  total={243}
                  hideTotalInWidget={!isMobilePlatform()}
                  seats={countSeats(srcwiseTableData[src])}
                  type="Results"
                  countText={electionData.counttext}
                  source={electionData.source}
                />
              </ErrorBoundary>
            )}
          </div>
        </div>
      </div>
    ) : null;
  }
}

const ConstituencyResult = props => (
  <div className="election-as-widget resultwdt">
    <div className="row">
      {!isMobilePlatform() && (
        <div className="col6 source-container">
          <SectionHeader
            sectionhead={`${props.constituencyData.cns_name.toUpperCase()} CONSTITUENCY RESULTS ${props.electionyear}`}
          />
        </div>
      )}
      <div className={`${isMobilePlatform() ? "col12" : "col6"} table container`}>
        {isMobilePlatform() && (
          <SectionHeader
            sectionhead={`${props.constituencyData.cns_name.toUpperCase()} CONSTITUENCY RESULTS ${props.electionyear}`}
          />
        )}

        <div className="result-tbl">
          <div className="tbl_heading">
            <div className="tbl_col">Won</div>
            <div className="tbl_col">Incumbent</div>
          </div>
          <div className="tbl_row">
            <div className="tbl_col" style={{ fontWeight: "bold", color: "green" }}>
              {props.constituencyData.lwan}
            </div>
            <div className="tbl_col" style={{ fontWeight: "bold", color: "red " }}>
              {props.constituencyData.inan}
            </div>
          </div>
          <div className="tbl_row">
            <div className="tbl_col" style={{ fontWeight: "bold", color: "green" }}>
              {props.constituencyData.lwcn}
            </div>
            <div className="tbl_col" style={{ fontWeight: "bold", color: "red" }}>
              {props.constituencyData.incn}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const makeSourcewisemapFromData = data => {
  if (!data) {
    return {};
  }
  const sourceMap = {};
  data.forEach(info => {
    const source = info && info[0] && info[0].src ? info[0].src : "no-src";
    sourceMap[source] = info;
  });
  return sourceMap;
};

ResultsASWidget.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.object,
  data: PropTypes.object,
  config: PropTypes.array,
};

function mapStateToProps(state) {
  return {
    ...state.results,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

ResultsASWidget.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    let configData = {};
    if (data && data.payload) {
      configData = getFeedUrlObjectFromElectionConfigData(data.payload, `resulturl${params.electionyear || ""}`);
    }
    dispatch(fetchResultsIfNeeded(params, query, configData));
  });
};

export default connect(mapStateToProps)(ResultsASWidget);
