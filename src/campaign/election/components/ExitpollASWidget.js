/* eslint-disable no-param-reassign */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import styles from "../common/css/Election.scss";

import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import Table from "./Table";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import Select from "./Select";

import { fetchElectionConfigData } from "../actions/electoralmap/electoralmap";
import { fetchExitPollIfNeeded } from "../actions/exitpoll/exitpoll";
import {
  countSeats,
  modifyStateName,
  getFeedUrlObjectFromElectionConfigData,
  generateStateExitPollLinks,
} from "../utils/util";
import { isMobilePlatform } from "../../../utils/util";
import { electionConfig } from "../utils/config";

const siteElectionConfig = electionConfig[process.env.SITE];

class ExitpollASWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      selectedStatePosition: 0,
    };
  }

  componentDidMount() {
    const { query, dispatch, location } = this.props;
    let { params } = this.props;
    if (!params) {
      params = {};
    }
    if (location) {
      params.location = location;
    }
    ExitpollASWidget.fetchData({ dispatch, params, query });
  }

  handleSelectChange = index => {
    this.setState({ selectedIndex: index });
  };

  handleStateChange = index => {
    this.setState({ selectedStatePosition: index, selectedIndex: 0 });
  };

  render() {
    console.log("exitpoll");
    const { data, config } = this.props;
    // const stateName = config && config[0] ? modifyStateName({ stateName: config[0].state }) : "Other";
    // const renderData = (data && data[stateName]) || {};
    if (!data) {
      return null;
    }
    let stateName;
    let renderData = {};
    let sequenceArray = [];
    const position = this.state.selectedStatePosition;
    if (!data.al_rslt && Object.keys(data).length > 0 && config && config[0] && !config[0].state) {
      sequenceArray = config && config[0].sequence ? config[0].sequence : Object.keys(data);
      stateName = modifyStateName({ stateName: sequenceArray[position] });
      renderData = data[sequenceArray[position]];
    } else {
      stateName = config && config[0] ? modifyStateName({ stateName: config[0].state }) : "Other";
      renderData = (data && data[stateName.replace(/-/g, "_")]) || {};
    }
    const statemsid = siteElectionConfig.exitpollMsidStateMap[stateName.toLowerCase()];
    const srcwiseTableData = makeSourcewisemapFromData(renderData.al_rslt);
    const total = renderData.ttl_seat || 0;
    const src = Object.keys(srcwiseTableData);
    const { selectedIndex } = this.state;
    return src && src.length > 0 ? (
      <div className="election-as-widget" data-exclude="amp">
        {sequenceArray.length > 1 && (
          <div className="statestoggle">
            <div className="states">
              {sequenceArray.map((elem, index) => (
                <span
                  className={position === index ? "active" : ""}
                  onClick={() => this.handleStateChange(index)}
                  role="button"
                  tabIndex={0}
                  onKeyPress={() => this.handleStateChange(index)}
                >
                  <b>{modifyStateName({ stateName: elem })}</b>
                </span>
              ))}
            </div>
          </div>
        )}
        <div className="row">
          {!isMobilePlatform() && (
            <div className="col6 source-container">
              <SectionHeader
                sectionhead={`${stateName.toUpperCase()} ELECTION EXIT POLL ${
                  config && config[0] && config[0].year ? config[0].year : ""
                }`}
                weblink={generateStateExitPollLinks(stateName.toLowerCase(), statemsid)}
              />
              {total > 0 && (
                <div className="info-wrapper">
                  <div>
                    <span>Total Seats</span> : {total}
                  </div>
                  <div>
                    <span>Majority Mark</span> : {Math.floor(total / 2) + 1}
                  </div>
                </div>
              )}
            </div>
          )}
          <div className={`${isMobilePlatform() ? "col12" : "col6"} table container`}>
            {isMobilePlatform() && (
              <SectionHeader
                sectionhead={`${stateName.toUpperCase()} ELECTION EXIT POLL ${
                  config && config[0] && config[0].year ? config[0].year : ""
                }`}
                weblink={generateStateExitPollLinks(stateName.toLowerCase(), statemsid)}
              />
            )}
            {src && src.length > 0 && (
              <ErrorBoundary>
                <div className="aspage_source">
                  <span>Source:</span>
                  <Select
                    selectList={src}
                    selectedValue={src[selectedIndex]}
                    handleSelectChange={this.handleSelectChange}
                  />
                </div>
                <Table
                  items={srcwiseTableData[src[selectedIndex]]}
                  total={243}
                  hideTotalInWidget={!isMobilePlatform()}
                  seats={countSeats(srcwiseTableData[src[selectedIndex]])}
                  hideSource
                />
              </ErrorBoundary>
            )}
          </div>
        </div>
      </div>
    ) : (
      ""
    );
  }
}

const makeSourcewisemapFromData = data => {
  if (!data) {
    return {};
  }
  const sourceMap = {};
  data.forEach(info => {
    if (info.length > 0 && parseInt(info[0].ws, 10) + parseInt(info[0].ls, 10) > 0) {
      const source = info && info[0] && info[0].src ? info[0].src : "no-src";
      sourceMap[source] = info;
    }
  });
  return sourceMap;
};

ExitpollASWidget.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.object,
  data: PropTypes.object,
  config: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    ...state.exitpoll,
    config: state.electoralmap.configData,
  };
}

ExitpollASWidget.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    let configData = {};
    if (data && data.payload) {
      configData = getFeedUrlObjectFromElectionConfigData(data.payload, "exitpollurl");
    }
    dispatch(fetchExitPollIfNeeded(params, query, configData));
  });
};

export default connect(mapStateToProps)(ExitpollASWidget);
