/* eslint-disable indent */
import React from "react";
import PropTypes from "prop-types";

import PieChart from "./PieChart";
import Slider from "../../../components/desktop/NewSlider/Slider";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";

import { _isCSR, isMobilePlatform } from "../../../utils/util";
import {
  modifyStateName,
  countSeats,
  generateStateExitPollLinks,
  getDataLength,
  checkStateInData,
  checkWidgetToRender,
} from "../utils/util";
import { electionConfig } from "../utils/config";
import NoData from "./NoData";

const siteElectionConfig = electionConfig[process.env.SITE];
const statemsid = siteElectionConfig.exitpollMsidStateMap.bihar;

const ExitPollCombinedPie = props => {
  const { data, combineData, stateName, exitPollWidget, sequence } = props;

  let renderData = {};
  let sequenceData;
  const TagName = combineData ? "div" : React.Fragment;
  const SliderTagName = combineData && !isMobilePlatform() ? Slider : React.Fragment;
  if (!combineData) {
    renderData[stateName] = data[stateName] || {};
    sequenceData = Object.keys(renderData);
  } else {
    renderData = data;
    if (sequence) {
      sequenceData = sequence;
    } else {
      sequenceData = Object.keys(renderData);
    }
  }

  return (
    <div
      data-ele="graph-container"
      className={`exitpoll_graph_container ${isMobilePlatform() || exitPollWidget ? "col12" : "col8"}`}
    >
      {/* {combineData && !exitPollWidget && !checkStateInData(renderData, "Bihar") && (
        <SectionHeader sectionhead="BIHAR" weblink={generateStateExitPollLinks("bihar", statemsid)} />
      )} */}
      {_isCSR() && sequenceData
        ? sequenceData.map(key => {
            const formattedStateName = modifyStateName({ stateName: key }).toLowerCase();
            const msid = siteElectionConfig.exitpollMsidStateMap[formattedStateName];
            return (
              <TagName className="combinedchartbox">
                {combineData && !exitPollWidget && (
                  <SectionHeader
                    sectionhead={formattedStateName.toUpperCase()}
                    weblink={generateStateExitPollLinks(formattedStateName, msid)}
                  />
                )}
                <TagName key={`resultfragment_${key}`} className="chart-horizontal">
                  <SliderTagName
                    margin="10"
                    width={exitPollWidget ? "977" : "620"}
                    type="grid"
                    moveByPixel={exitPollWidget ? "520" : "450"}
                    dataLength={getDataLength(renderData[key].al_rslt)}
                    sliderClass="elections-slider"
                  >
                    {renderData[key].al_rslt && checkWidgetToRender(renderData[key].al_rslt) ? (
                      renderData[key].al_rslt.map((item, index) => (
                        <PieChart
                          items={item}
                          _index={index}
                          total={renderData[key].ttl_seat}
                          state={formattedStateName}
                          seats={countSeats(item)}
                          greaterThanZero
                        />
                      ))
                    ) : (
                      <NoData />
                    )}
                  </SliderTagName>
                </TagName>
              </TagName>
            );
          })
        : null}
    </div>
  );
};

ExitPollCombinedPie.propTypes = {
  data: PropTypes.object,
  combineData: PropTypes.bool,
  stateName: PropTypes.string,
};

function arePropsEqual(prevProps, nextProps) {
  return (
    prevProps.combineData === nextProps.combineData &&
    prevProps.data === nextProps.data &&
    prevProps.stateName === nextProps.stateName
  );
}

// React memo is used to stop rerendering of the component if the props remain same
export default React.memo(ExitPollCombinedPie, arePropsEqual);
