/* eslint-disable no-param-reassign */
import React from "react";
import PropTypes from "prop-types";
import fetch from "../../../utils/fetch/fetch";

import Adcard from "../../../components/common/AdCard";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";

import { generateUrl } from "../../../utils/util";
import { electionConfig } from "../utils/config";

const siteElectionConfig = electionConfig[process.env.SITE];

class ElectionRHSWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      rhsCSRData: null,
    };
  }

  componentDidMount() {
    const url = `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${siteElectionConfig.electionRhsNewsMsid}&type=list&feedtype=sjson&count=5&ctnadtype=homedefault&layouttype=hpui2`;
    fetch(url).then(data => {
      this.setState({ rhsCSRData: data });
    });
  }

  render() {
    const data = this.state.rhsCSRData;
    return (
      <React.Fragment>
        <div className="box-item">
          <Adcard mstype="mrec1" adtype="dfp" />
        </div>
        <div className="box-item rhs-news">
          <SectionHeader sectionhead={data ? data.secname : ""} weblink={generateUrl(data)} />
          <ErrorBoundary>
            <GridSectionMaker
              data={this.state.rhsCSRData ? this.state.rhsCSRData.items : []}
              type={[
                {
                  type: "horizontal",
                  className: "col12",
                  startIndex: 0,
                  imgsize: "smallthumb",
                },
              ]}
            />
          </ErrorBoundary>
        </div>
        {/* Hiding second ad provision is provided just to manage the extra space created 
        when there is not of the enough content on the left side of the rhs widget */}
        {!this.props.hideSecondAd && (
          <div className="box-item">
            <Adcard mstype="mrec2" adtype="dfp" />
          </div>
        )}
      </React.Fragment>
    );
  }
}

ElectionRHSWidget.propTypes = {
  hideSecondAd: PropTypes.bool,
};

export default ElectionRHSWidget;
