import React from "react";

const Disclaimer = props => {
  const { electionData, stateName } = props;
  return (electionData && electionData.disclaimer && stateName == "") ||
    electionData.disclaimer.statename === stateName ? (
    <div className="widget_disclaimer">
      <div>{electionData.disclaimer.itemhead || "Disclaimer"}</div>
      <ul>
        {electionData.disclaimer.item.map(inditem => (
          <li>{inditem}</li>
        ))}
      </ul>
    </div>
  ) : null;
};

export default Disclaimer;
