/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";

const PartyAllianceSwitcher = props => (
  <div className="switcher col12" data-exclude="amp">
    <span
      className={props.selectedValue === "al_rslt" ? "selected" : ""}
      onClick={() => props.toggleDatatype("al_rslt")}
    >
      Party
    </span>
    <span
      className={props.selectedValue === "pg_rslt" ? "selected" : ""}
      onClick={() => props.toggleDatatype("pg_rslt")}
    >
      Alliance
    </span>
  </div>
);

PartyAllianceSwitcher.propTypes = {
  selectedValue: PropTypes.object,
  toggleDatatype: PropTypes.func,
};

export default PartyAllianceSwitcher;
