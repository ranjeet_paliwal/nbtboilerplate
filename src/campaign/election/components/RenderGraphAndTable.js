/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React from "react";
import PropTypes from "prop-types";
import PieChart from "./PieChart";
import Table from "./Table";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import { _isCSR } from "../../../utils/util";
import {
  modifyStateName,
  generateStateResultslLinks,
  countSeats,
  checkStateInData,
  checkWidgetToRender,
} from "../utils/util";
import { electionConfig } from "../utils/config";
import NoData from "./NoData";
import PartyAllianceSwitcher from "./PartyAllianceSwitcher";
const siteElectionConfig = electionConfig[process.env.SITE];
const statemsid = siteElectionConfig.resultMsidStateMap.bihar;

const RenderGraphAndTable = props => {
  const { type, data, combineData, showType, toggleShowType, source, countText, sequence } = props;
  if (!_isCSR() && type === "graph") {
    return "";
  }
  const TagName = combineData ? "div" : React.Fragment;
  const Component = type === "graph" ? PieChart : Table;
  let filteredSequence;
  if (sequence) {
    filteredSequence = sequence.filter(state => Object.keys(data).indexOf(state) > -1);
  }
  const sequenceData = filteredSequence && filteredSequence.length > 0 ? filteredSequence : Object.keys(data);
  return (
    <React.Fragment>
      {/* {combineData && !checkStateInData(data, "Bihar") && (
        <SectionHeader sectionhead="BIHAR" weblink={generateStateResultslLinks("bihar", statemsid)} />
      )} */}
      {sequenceData.map(key => {
        const formattedStateName = modifyStateName({ stateName: key }).toLowerCase();
        const msid = siteElectionConfig.resultMsidStateMap[formattedStateName];
        let partyAllianceSwitch = false;
        if (data[key] && data[key].al_rslt && data[key].pg_rslt) {
          partyAllianceSwitch = true;
        }
        let item = [];
        if (partyAllianceSwitch) {
          if (showType === "pg_rslt") {
            item = data[key].pg_rslt ? data[key].pg_rslt[0] : [];
          } else if (showType === "al_rslt") {
            item = data[key].al_rslt ? data[key].al_rslt[0] : [];
          }
        } else {
          item = data[key] && data[key].al_rslt ? data[key].al_rslt[0] : [];
        }
        return (
          <TagName className="combinedchartbox">
            {combineData && (
              <SectionHeader
                sectionhead={formattedStateName.toUpperCase()}
                weblink={generateStateResultslLinks(formattedStateName, msid)}
              />
            )}
            {partyAllianceSwitch && <PartyAllianceSwitcher selectedValue={showType} toggleDatatype={toggleShowType} />}
            {/* {checkWidgetToRender([item]) ? ( */}
            <Component
              items={item}
              total={data[key].ttl_seat}
              state={formattedStateName}
              seats={countSeats(item)}
              type="Results"
              countText={countText}
              source={source}
            />
            {/* ) : (
              <NoData />
            )} */}
          </TagName>
        );
      })}
    </React.Fragment>
  );
};

//   export const RenderGraphAndTable

RenderGraphAndTable.propTypes = {
  data: PropTypes.object,
  combineData: PropTypes.bool,
  type: PropTypes.string,
  showType: PropTypes.string,
  toggleShowType: PropTypes.func,
};

function arePropsEqual(prevProps, nextProps) {
  let flag = false;
  if (prevProps.data && nextProps.data) {
    flag = JSON.stringify(prevProps.data) === JSON.stringify(nextProps.data);
  }
  return prevProps.combineData === nextProps.combineData && flag && prevProps.showType === nextProps.showType;
}

// React memo is used to stop rerendering of the component if the props remain same
export default React.memo(RenderGraphAndTable, arePropsEqual);
