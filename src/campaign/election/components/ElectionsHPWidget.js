/* eslint-disable indent */
/* eslint-disable camelcase */
import React from "react";

// import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "../common/css/Election.scss";
import AnchorLink from "../../../components/common/AnchorLink";
import { setAutoRefresh, removeAutoRefresh, fetchResultsIfNeeded } from "../actions/results/results";
import { setAutoRefreshExitPoll, removeAutoRefreshExitpoll, fetchExitPollIfNeeded } from "../actions/exitpoll/exitpoll";
import {
  countSeats,
  modifyStateName,
  generateElectionLinks,
  generateReadMoreLinks,
  getFeedUrlObjectFromElectionConfigData,
} from "../utils/util";
import { isMobilePlatform, _isCSR, _getStaticConfig, checkIsAmpPage, _isMobile } from "../../../utils/util";
// // import { electionConfig } from "../utils/config";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import ExitPollCombinedTable from "./ExitPollCombinedTable";
import StarCandidatesWidget from "./StarCandidatesWidget";
import PieChart from "./PieChart";
import Table from "./Table";
import PartyAllianceSwitcher from "./PartyAllianceSwitcher";
import ChartTableToggleButton from "./ChartTableToggleButton";
import { fetchElectionConfigData } from "../actions/electoralmap/electoralmap";
import Disclaimer from "./Disclaimer";
import Select from "./Select";

import { electionConfig } from "../utils/config";
import AdCard from "../../../components/common/AdCard";
const siteConfig = _getStaticConfig();

const siteElectionConfig = electionConfig[process.env.SITE];
const refreshInterval = 15000;
class ElectionHPWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    const { sequence } = props;
    const sequenceArr = sequence[process.env.SITE].split(",");
    this.state = {
      sequenceArr,
      selectedStatePosition: 0,
      selectedExitpollSource: 0,
      showType: "al_rslt",
    };
  }

  toggleDatatype = type => {
    if (type === this.state.showType) {
      return;
    }
    this.setState({ showType: type });
  };

  toggleChart() {
    this.setState(prevState => ({
      isTablurForm: !prevState.isTablurForm,
    }));
  }

  componentDidMount() {
    const { config, dispatch, params, query, electionData, widgetType } = this.props;
    const fetchDataSilently = true;
    if (widgetType === "results") {
      const autoRefreshFlag = electionData.resultrefresh && electionData.resultrefresh === "false" ? false : true;
      dispatch(fetchElectionConfigData(params, query)).then(returndata => {
        let configData = {};
        if (returndata && returndata.payload) {
          configData = getFeedUrlObjectFromElectionConfigData(returndata.payload, "resulturl");
        }
        dispatch(fetchResultsIfNeeded(params, query, configData, fetchDataSilently));
        if (autoRefreshFlag) {
          if (this.props.resultData.refreshProcessId) {
            dispatch(removeAutoRefresh());
          }
          const refreshProcessId = setInterval(() => {
            dispatch(fetchResultsIfNeeded(params, query, configData, fetchDataSilently));
          }, refreshInterval);
          dispatch(setAutoRefresh(refreshProcessId));
        }
      });
    } else {
      const autoRefreshFlag = electionData.exitpollrefresh && electionData.exitpollrefresh === "false" ? false : true;
      dispatch(fetchElectionConfigData(params, query)).then(returndata => {
        let configData = {};
        if (returndata && returndata.payload) {
          configData = getFeedUrlObjectFromElectionConfigData(returndata.payload, "exitpollurl");
        }
        dispatch(fetchExitPollIfNeeded(params, query, configData, fetchDataSilently));
        if (autoRefreshFlag) {
          if (this.props.exitpollData.refreshProcessId) {
            dispatch(removeAutoRefreshExitpoll());
          }
          const refreshProcessId = setInterval(() => {
            dispatch(fetchExitPollIfNeeded(params, query, configData, fetchDataSilently));
          }, refreshInterval);
          dispatch(setAutoRefreshExitPoll(refreshProcessId));
        }
      });
    }
  }

  componentWillUnmount() {
    this.props.dispatch(removeAutoRefresh());
    this.props.dispatch(removeAutoRefreshExitpoll());
  }

  changeState = index => {
    this.setState({ selectedStatePosition: index, selectedExitpollSource: 0 });
  };

  handleSelectChange = index => {
    this.setState({ selectedExitpollSource: index });
  };

  render() {
    const { resultData, exitpollData, config, electionData, sequence, widgetType } = this.props;
    const { data } = widgetType === "results" ? resultData : exitpollData;
    const { selectedStatePosition, sequenceArr, selectedExitpollSource } = this.state;
    const stateName = sequenceArr[selectedStatePosition];
    const modifiedStateName = modifyStateName({ stateName });
    let stateData = data && data[stateName] ? data[stateName] : {};
    let item = [];

    let partyAllianceSwitch = false;
    let srcList = [];

    if (widgetType === "results") {
      if (stateData.al_rslt && stateData.pg_rslt) {
        partyAllianceSwitch = true;
      }
      if (partyAllianceSwitch) {
        if (this.state.showType === "pg_rslt") {
          item = stateData.pg_rslt ? stateData.pg_rslt[0] : [];
        } else if (this.state.showType === "al_rslt") {
          item = stateData.al_rslt ? stateData.al_rslt[0] : [];
        }
      } else {
        item = stateData.al_rslt ? stateData.al_rslt[0] : [];
      }
    } else {
      const srcwiseTableData = makeSourcewisemapFromData(stateData.al_rslt);
      srcList = Object.keys(srcwiseTableData);
      item = srcList && srcList[0] ? srcwiseTableData[srcList[selectedExitpollSource]] : [{ ws: 0, ls: 0 }];
    }

    const formattedStateName = modifyStateName({ stateName: stateName === "Other" ? "Bihar" : stateName });
    const statemsid = siteElectionConfig.resultMsidStateMap[stateName.toLowerCase()];

    const Tagname = isMobilePlatform() ? "div" : React.Fragment;

    const link = generateReadMoreLinks({
      msid: widgetType === "results" ? siteElectionConfig.combinedResultsMsid : siteElectionConfig.combinedExitPollMsid,
      type: widgetType === "results" ? "results" : "exitpolls",
    });

    return (
      <div className="election-hpwidget">
        <div className="election-header-wrapper">
          <ElectionHeader />
          <StateSwitcher
            states={sequenceArr}
            selectedStatePosition={selectedStatePosition}
            changeState={this.changeState}
            url={this.props.url}
            widgetType={widgetType}
          />
          <AnchorLink className="read_more" href={link}>
            {siteConfig.locale.readmore}
          </AnchorLink>
        </div>
        <div className="election-data-wrapper result-widget result_inner">
          <ElectionDataHeader widgetType={this.props.widgetType} statename={sequenceArr[selectedStatePosition]} />
          {isMobilePlatform() && (
            <ChartTableToggleButton toggleChart={() => this.toggleChart(this)} isTablurForm={this.state.isTablurForm} />
          )}
          <div className="row election-data">
            {data && (
              <div className="result-data col12">
                {partyAllianceSwitch && (
                  <PartyAllianceSwitcher selectedValue={this.state.showType} toggleDatatype={this.toggleDatatype} />
                )}
                {widgetType === "exitpoll" && !checkIsAmpPage(this.props.url) && srcList.length > 0 && (
                  <div className="aspage_source">
                    <span>Source:</span>
                    <Select
                      selectList={srcList}
                      selectedValue={srcList[selectedExitpollSource]}
                      handleSelectChange={this.handleSelectChange}
                    />
                  </div>
                )}
                <Tagname className={`${this.state.isTablurForm ? "hide chartcomp" : "show chartcomp"}`}>
                  <div className={`graph-container ${isMobilePlatform() ? "col12" : "col4"}`} data-exclude="amp">
                    {_isCSR() && (
                      <PieChart
                        items={item}
                        _index={0}
                        total={stateData.ttl_seat}
                        state={formattedStateName}
                        seats={countSeats(item)}
                        type={widgetType === "results" ? "Results" : ""}
                        countText={electionData.counttext}
                        source={widgetType === "results" ? electionData.source : ""}
                        hideLegend
                      />
                    )}
                  </div>
                </Tagname>
                <Tagname className={`${this.state.isTablurForm ? "show tablecomp" : "hide tablecomp"}`}>
                  <div
                    className={`graph-container ${isMobilePlatform() ? "col12" : "col4"}`}
                    style={{ alignContent: "center" }}
                    data-exclude="amp"
                  >
                    {widgetType === "exitpoll" && srcList.length === 0 ? (
                      <div className="nodatawidget">Data awaited</div>
                    ) : (
                      <Table
                        items={item}
                        _index={0}
                        total={stateData.ttl_seat}
                        state={formattedStateName}
                        seats={countSeats(item)}
                        type={widgetType === "results" ? "Results" : ""}
                        countText={electionData.counttext}
                        hideSource
                      />
                    )}
                  </div>
                </Tagname>
                {isMobilePlatform() && checkIsAmpPage(this.props.url) && (
                  <div className="tablecount show" data-elem="table">
                    <ErrorBoundary>
                      <ExitPollCombinedTable
                        data={{ stateName: data[stateName] }}
                        combineData
                        stateName
                        exitPollWidget
                      />
                    </ErrorBoundary>
                  </div>
                )}
                {isMobilePlatform() && <Disclaimer stateName={stateName} electionData={electionData} />}
                {!isMobilePlatform() && (
                  <div className="col4 ad-count">
                    <AdCard mstype="mrec1" adtype="dfp" />
                  </div>
                )}
                {!isMobilePlatform() && <Disclaimer stateName={stateName} electionData={electionData} />}

                {/* <AdCard mstype="mrec1" adtype="dfp" /> */}
                {/* <Disclaimer electionData={electionData} /> */}
              </div>
            )}
          </div>
          {widgetType === "results" && (
            <div className="star-candidates-wrapper">
              <StarCandidatesWidget
                showCandidateStatus={electionData.showcandidatestatus}
                stateName={stateName}
                size={6}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const ElectionHeader = () => (
  <div className="assembly-election-header">
    Assembly
    <span>Elections 2021</span>
  </div>
);

const getModifiedStateName = stateName => modifyStateName({ stateName, search: "_", replaceWith: "-" }).toLowerCase();

const StateSwitcher = props => {
  return (
    <div className="statestoggle">
      <div className="states">
        {isMobilePlatform() && checkIsAmpPage(props.url)
          ? props.states.map((state, index) => (
              <AnchorLink
                href={
                  props.widgetType === "exitpoll"
                    ? generateElectionLinks({
                        statename: getModifiedStateName(state),
                        msid: siteElectionConfig.exitpollMsidStateMap[getModifiedStateName(state)],
                        type: "exitpolls",
                      })
                    : generateElectionLinks({
                        statename: getModifiedStateName(state),
                        msid: siteElectionConfig.resultMsidStateMap[getModifiedStateName(state)],
                        type: "results",
                      })
                }
              >
                <b>{modifyStateName({ stateName: state, search: "_", replaceWith: " " })}</b>
              </AnchorLink>
            ))
          : props.states.map((state, index) => (
              <span
                className={props.selectedStatePosition === index ? "active" : ""}
                onClick={() => props.changeState(index)}
                role="button"
                tabIndex={0}
                onKeyPress={() => props.changeState(index)}
              >
                <b>{modifyStateName({ stateName: state, search: "_", replaceWith: " " })}</b>
              </span>
            ))}
      </div>
    </div>
  );
};

const ElectionDataHeader = props => {
  const modifiedStateName = modifyStateName({
    stateName: props.statename,
    search: "_",
    replaceWith: "-",
  }).toLowerCase();
  return (
    <div className="election-data-header">
      <SectionHeader
        sectionhead={`${siteElectionConfig.stateRegText[modifiedStateName]} ${
          siteElectionConfig.assemblyElectionRegText
        } ${
          props.widgetType === "results"
            ? `${siteElectionConfig.resultsRegText}`
            : `${siteElectionConfig.exitPollRegText}`
        }`}
        weblink={
          props.widgetType === "exitpoll"
            ? generateElectionLinks({
                statename: modifiedStateName,
                msid: siteElectionConfig.exitpollMsidStateMap[modifiedStateName],
                type: "exitpolls",
              })
            : generateElectionLinks({
                statename: modifiedStateName,
                msid: siteElectionConfig.resultMsidStateMap[modifiedStateName],
                type: "results",
              })
        }
      />
      <div className="electionsnav">
        <AnchorLink
          href={generateElectionLinks({
            statename: modifiedStateName,
            msid: siteElectionConfig.electralMsidStateMap[modifiedStateName],
            type: "electoralmap",
          })}
        >
          {siteElectionConfig.mapRegText}
        </AnchorLink>
        <AnchorLink
          href={generateElectionLinks({
            statename: modifiedStateName,
            msid: siteElectionConfig.candidateMsidStateMap[modifiedStateName],
            type: "candidates",
          })}
        >
          {siteElectionConfig.candidateRegText}
        </AnchorLink>
        {/* <AnchorLink href={"http://linktomap"}>{siteElectionConfig.comparisonRegText}</AnchorLink> */}
        {props.widgetType === "results" && (
          <AnchorLink
            href={generateElectionLinks({
              statename: modifiedStateName,
              msid: siteElectionConfig.exitpollMsidStateMap[modifiedStateName],
              type: "exitpolls",
            })}
          >
            {siteElectionConfig.exitPollRegText}
          </AnchorLink>
        )}
        {props.widgetType === "exitpoll" && (
          <AnchorLink
            href={generateElectionLinks({
              statename: modifiedStateName,
              msid: siteElectionConfig.resultMsidStateMap[modifiedStateName],
              type: "results",
            })}
          >
            {siteElectionConfig.resultsRegText}
          </AnchorLink>
        )}
      </div>
    </div>
  );
};

const makeSourcewisemapFromData = data => {
  if (!data) {
    return {};
  }
  const sourceMap = {};
  data.forEach(info => {
    if (info.length > 0 && parseInt(info[0].ws, 10) + parseInt(info[0].ls, 10) > 0) {
      const source = info && info[0] && info[0].src ? info[0].src : "no-src";
      sourceMap[source] = info;
    }
  });
  return sourceMap;
};

function mapStateToProps(state) {
  return {
    resultData: state.results,
    exitpollData: state.exitpoll,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

ElectionHPWidget.propTypes = {
  dispatch: PropTypes.func,
  config: PropTypes.object,
  refreshProcessId: PropTypes.number,
  params: PropTypes.object,
  query: PropTypes.object,
  data: PropTypes.object,
};

export default connect(mapStateToProps)(ElectionHPWidget);
