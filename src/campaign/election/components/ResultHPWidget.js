/* eslint-disable camelcase */
import React from "react";

// import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "../common/css/Election.scss";
import { setAutoRefresh, removeAutoRefresh, fetchResultsIfNeeded } from "../actions/results/results";
import {
  countSeats,
  modifyStateName,
  getFeedUrlObjectFromElectionConfigData,
  generateStateResultslLinks,
} from "../utils/util";
import { isMobilePlatform, _isCSR } from "../../../utils/util";
// import { electionConfig } from "../utils/config";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
// import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import StarCandidatesWidget from "./StarCandidatesWidget";
import PieChart from "./PieChart";
import Table from "./Table";
import PartyAllianceSwitcher from "./PartyAllianceSwitcher";
import ChartTableToggleButton from "./ChartTableToggleButton";
import { fetchElectionConfigData } from "../actions/electoralmap/electoralmap";
import Disclaimer from "./Disclaimer";

import { electionConfig } from "../utils/config";
const siteElectionConfig = electionConfig[process.env.SITE];

// const siteElectionConfig = electionConfig[process.env.SITE];
const refreshInterval = 15000;

class ResultsHPWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isTablurForm: false,
      showType: "al_rslt",
    };
  }

  toggleDatatype = type => {
    if (type === this.state.showType) {
      return;
    }
    this.setState({ showType: type });
  };

  toggleChart() {
    this.setState(prevState => ({
      isTablurForm: !prevState.isTablurForm,
    }));
  }

  componentDidMount() {
    const { config, dispatch, params, query, electionData } = this.props;
    const fetchDataSilently = true;
    const autoRefreshFlag = electionData.resultrefresh && electionData.resultrefresh === "false" ? false : true;
    if (config && config[0] && config[0].resulturl) {
      if (autoRefreshFlag) {
        let data = {};
        data = getFeedUrlObjectFromElectionConfigData(config, "resulturl");
        if (this.props.refreshProcessId) {
          dispatch(removeAutoRefresh());
        }
        const refreshProcessId = setInterval(() => {
          dispatch(fetchResultsIfNeeded(params, query, data, fetchDataSilently));
        }, refreshInterval);
        dispatch(setAutoRefresh(refreshProcessId));
      }
    } else {
      dispatch(fetchElectionConfigData(params, query)).then(returndata => {
        let configData = {};
        if (returndata && returndata.payload) {
          configData = getFeedUrlObjectFromElectionConfigData(returndata.payload, "resulturl");
        }
        dispatch(fetchResultsIfNeeded(params, query, configData, fetchDataSilently));
        if (autoRefreshFlag) {
          if (this.props.refreshProcessId) {
            dispatch(removeAutoRefresh());
          }
          const refreshProcessId = setInterval(() => {
            dispatch(fetchResultsIfNeeded(params, query, configData, fetchDataSilently));
          }, refreshInterval);
          dispatch(setAutoRefresh(refreshProcessId));
        }
      });
    }
  }

  componentWillUnmount() {
    this.props.dispatch(removeAutoRefresh());
  }

  render() {
    const { data, config, electionData } = this.props;
    const stateName = config && config[0] ? config[0].state : "Other";
    const modifiedStateName = modifyStateName({ stateName });
    const stateData = data && data[modifiedStateName] ? data[modifiedStateName] : {};
    let partyAllianceSwitch = false;
    if (stateData.al_rslt && stateData.pg_rslt) {
      partyAllianceSwitch = true;
    }
    let item = [];
    if (partyAllianceSwitch) {
      if (this.state.showType === "pg_rslt") {
        item = stateData.pg_rslt ? stateData.pg_rslt[0] : [];
      } else if (this.state.showType === "al_rslt") {
        item = stateData.al_rslt ? stateData.al_rslt[0] : [];
      }
    } else {
      item = stateData.al_rslt ? stateData.al_rslt[0] : [];
    }

    const formattedStateName = modifyStateName({ stateName: stateName === "Other" ? "Bihar" : stateName });
    const statemsid = siteElectionConfig.resultMsidStateMap[stateName.toLowerCase()];

    const Tagname = isMobilePlatform() ? "div" : React.Fragment;

    return (
      <div className="result-widget result_inner">
        <div className="row">
          {/* <SectionHeader sectionhead="बिहार विधानसभा चुनाव 2020" /> */}
          <SectionHeader
            sectionhead={siteElectionConfig.hpResultWidgetHeading}
            weblink={generateStateResultslLinks(stateName, statemsid)}
          />
          {isMobilePlatform() && (
            <ChartTableToggleButton toggleChart={() => this.toggleChart(this)} isTablurForm={this.state.isTablurForm} />
          )}

          {data && (
            <div className="result-data col12">
              {partyAllianceSwitch && (
                <PartyAllianceSwitcher selectedValue={this.state.showType} toggleDatatype={this.toggleDatatype} />
              )}
              <Tagname className={`${this.state.isTablurForm ? "hide chartcomp" : "show chartcomp"}`}>
                <div className={`graph-container ${isMobilePlatform() ? "col12" : "col6"}`} data-exclude="amp">
                  {_isCSR() && (
                    <PieChart
                      items={item}
                      _index={0}
                      total={stateData.ttl_seat}
                      state={formattedStateName}
                      seats={countSeats(item)}
                      type="Results"
                      countText={electionData.counttext}
                      source={electionData.source}
                    />
                  )}
                </div>
              </Tagname>

              <Tagname className={`${this.state.isTablurForm ? "show tablecomp" : "hide tablecomp"}`}>
                <div
                  className={`graph-container ${isMobilePlatform() ? "col12" : "col6"}`}
                  style={{ alignContent: "center" }}
                >
                  <Table
                    items={item}
                    _index={0}
                    total={stateData.ttl_seat}
                    state={formattedStateName}
                    seats={countSeats(item)}
                    type="Results"
                    countText={electionData.counttext}
                    hideSource
                  />
                </div>
              </Tagname>
              <Disclaimer stateName={formattedStateName} electionData={electionData} />
            </div>
          )}
        </div>
        <StarCandidatesWidget showCandidateStatus={electionData.showcandidatestatus} state={"bihar"} size={6} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.results,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

ResultsHPWidget.propTypes = {
  dispatch: PropTypes.func,
  config: PropTypes.object,
  refreshProcessId: PropTypes.number,
  params: PropTypes.object,
  query: PropTypes.object,
  data: PropTypes.object,
};

export default connect(mapStateToProps)(ResultsHPWidget);
