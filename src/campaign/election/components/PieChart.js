import React from "react";
import PropTypes from "prop-types";
import { isMobilePlatform, _isCSR } from "../../../utils/util";

import drawPieChart from "../../../modules/charts/piechart";

const PieChart = props => {
  const {
    items,
    _index,
    total,
    state,
    seats,
    type,
    isPrevYear,
    greaterThanZero,
    source,
    countText,
    hideLegend,
  } = props;
  const elemid = `elem${Math.floor(Math.random() * 1000000)}`;
  const chartObj = {
    data: {},
    colors: [],
    radius: _isCSR() ? Math.min(100, window.innerWidth / 2) : 100,
    canvasid: elemid,
  };
  items.forEach(item => {
    const count = isPrevYear ? parseInt(item.pws, 10) : parseInt(item.ws, 10) + parseInt(item.ls, 10);
    chartObj.data[item.an] = count;
    chartObj.colors.push(item.cc || "#c0c0c0"); // if no color is present in the feed, it should have by default colour
  });
  chartObj.totalValue = total;
  if (hideLegend) {
    chartObj.hideLegend = true;
  } else {
    chartObj.hideLegend = false;
  }
  let drawFlag = true;
  if (greaterThanZero) {
    if (parseInt(items[0].ws, 10) + parseInt(items[0].ls, 10) > 0) {
      drawFlag = true;
    } else {
      drawFlag = false;
    }
  }

  // return items.length > 0 && (isPrevYear || parseInt(items[0].ws, 10) + parseInt(items[0].ls, 10) > 0) ? (
  return items.length > 0 && drawFlag ? (
    <div className="pie-chart" data-ele="piechart_mp_0" key={_index}>
      <div className="el_graphdim chart highcharts-container">
        {/* <div id={elemid}></div> */}
        <div
          id={elemid}
          className="chartcountnew"
          ref={() => {
            if (_isCSR()) drawPieChart(chartObj);
            return false;
          }}
        ></div>
        {/* {setTimeout(() => { drawPieChart(chartObj); return false }, 200)} */}
        <div visibility="visible" className="chart-detail">
          <div className="charttxt">
            <div className="chartInnerText">
              {seats ? <div className="seatcounted">{seats.seatCounted}</div> : <div className="seatcounted"></div>}
              <div className="totalseat">{total}</div>
              <div className="seatstatus">{type === "Results" ? countText || "Results" : "Exit Poll"}</div>
            </div>
          </div>
          <div className="charttitle">
            {state && state !== "" ? (
              <div className="title">
                <span>{state.toLocaleUpperCase()}</span>
              </div>
            ) : null}
            {total ? <div className="subtitle">Majority Mark: {Math.floor(total / 2) + 1}</div> : null}
          </div>
          <div className="titlebackground"></div>
        </div>
        {!(isMobilePlatform() || type === "Results") && (items[0].src || source) ? (
          <div className="clearfix powerbycont">
            <div className="el_powerdby"> Source: {items[0].src || source}</div>
          </div>
        ) : null}
      </div>
      {isMobilePlatform() ? (
        <div className="legend">
          <ul>
            {items.map((item, index) => {
              return parseInt(item.ws, 10) + parseInt(item.ls, 10) > 0 ? (
                <li key={index.toString()} style={{ background: `${item.cc}` }}>
                  <div className="trend-text">{item.an}</div>
                  <div className="seatcount">{parseInt(item.ws, 10) + parseInt(item.ls, 10)}</div>
                </li>
              ) : null;
            })}
          </ul>
        </div>
      ) : null}
      {(isMobilePlatform() || type === "Results") && (items[0].src || source) ? (
        <div className="clearfix powerbycont">
          <div className="el_powerdby"> Source: {items[0].src || source}</div>
        </div>
      ) : null}
    </div>
  ) : null;
};

PieChart.propTypes = {
  items: PropTypes.object,
  _index: PropTypes.number,
  total: PropTypes.number,
  state: PropTypes.object,
  seats: PropTypes.number,
  type: PropTypes.string,
  isPrevYear: PropTypes.bool,
  greaterThanZero: PropTypes.bool,
  source: PropTypes.string,
  countText: PropTypes.string,
};

function arePropsEqual(prevProps, nextProps) {
  let itemFlag = false;
  let seatsFlag = false;

  if (prevProps.items && nextProps.items) {
    itemFlag = JSON.stringify(prevProps.items) === JSON.stringify(nextProps.items);
  }
  if (prevProps.seats && nextProps.seats) {
    seatsFlag = JSON.stringify(prevProps.seats) === JSON.stringify(nextProps.seats);
  }

  return itemFlag && seatsFlag && prevProps.total === nextProps.total && prevProps.state === nextProps.state;
}

export default React.memo(PieChart, arePropsEqual);
