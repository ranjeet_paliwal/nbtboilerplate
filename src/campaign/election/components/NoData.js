/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import SvgIcon from "../../../components/common/SvgIcon";

const NoData = props => (
  <div className="nodatawidget">
    <div>{props.data || "Data awaited"}</div>
    <div className="refresh_btn" data-exclude="amp">
      <a
        href="#"
        data-ele="refresh"
        onClick={event => {
          event.preventDefault();
          window.location = window.location.href;
        }}
      >
        <SvgIcon name="refresh" />
        <span>Refresh</span>
      </a>
    </div>
  </div>
);

NoData.propTypes = {
  data: PropTypes.object,
};

export default NoData;
