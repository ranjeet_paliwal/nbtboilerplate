/* eslint-disable no-param-reassign */
import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";

const Select = props => {
  const { selectList, selectedValue } = props;
  const [showDropdown, changeDropDownState] = useState(false);

  const divRef = useRef(null);

  function handleClickOutside(event) {
    if (divRef.current && !divRef.current.contains(event.target)) {
      changeDropDownState(false);
    }
  }

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const handleSelectChangeAndClose = index => {
    props.handleSelectChange(index);
    changeDropDownState(!showDropdown);
  };

  return (
    <div className="select-box" ref={divRef}>
      <div className="selected-value" onClick={() => changeDropDownState(!showDropdown)}>
        <span>{selectedValue}</span>
      </div>
      {selectList && selectList.length > 0 && (
        <ul className={showDropdown ? "show" : "hide"}>
          {selectList.map((item, index) => (
            <li key={index.toString()} onClick={() => handleSelectChangeAndClose(index)}>
              {item}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

Select.propTypes = {
  selectList: PropTypes.array,
  selectedValue: PropTypes.any,
  handleSelectChange: PropTypes.func,
};

export default Select;
