/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import { isMobilePlatform, _getStaticConfig, _isCSR } from "../../../utils/util";
const siteConfig = _getStaticConfig();

const ElectoralMapWidget = props => {
  const hideImage = () => {
    if (_isCSR()) {
      const loader = document.getElementById("iframe-loader");
      if (loader) {
        loader.style.display = "none";
      }
    }
  };

  return (
    <React.Fragment className="refresh_btn">
      <iframe
        src={props.url}
        width="100%"
        height={isMobilePlatform() ? "810" : "720"}
        border="0"
        marginWidth="0"
        marginHeight="0"
        frameBorder="0"
        scrolling={isMobilePlatform() ? "auto" : "no"}
        align="center"
        title="electoralmapwidget"
        id="electoral-map-iframe"
        onLoad={() => hideImage()}
      />
      {!isMobilePlatform() && (
        <img id="iframe-loader" src={`${siteConfig.weburl}/photo/47444548.cms`} alt="loading" className="ctyLoader" />
      )}
    </React.Fragment>
  );
};

ElectoralMapWidget.propTypes = {
  url: PropTypes.string,
};

export default ElectoralMapWidget;
