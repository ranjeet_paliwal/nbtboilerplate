/* eslint-disable camelcase */
import React from "react";

import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "../common/css/Election.scss";
// import { isMobilePlatform, _isCSR } from "../../../utils/util";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import ExitPollCombinedPie from "./ExitPollCombinedPie";
import ExitPollCombinedTable from "./ExitPollCombinedTable";
import ChartTableToggleButton from "./ChartTableToggleButton";
import { electionConfig } from "../utils/config";
import { generateStateExitPollLinks, getFeedUrlObjectFromElectionConfigData } from "../utils/util";
import Disclaimer from "./Disclaimer";
import { fetchElectionConfigData } from "../actions/electoralmap/electoralmap";
import { fetchExitPollIfNeeded } from "../actions/exitpoll/exitpoll";
const siteElectionConfig = electionConfig[process.env.SITE];

class ExitPollHPWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isTablurForm: false,
    };
  }

  componentDidMount() {
    // if (!config)
    const { params, query, dispatch } = this.props;
    ExitPollHPWidget.fetchData({ dispatch, params, query });
  }

  componentWillUnmount() {}

  toggleChart() {
    this.setState(prevState => ({
      isTablurForm: !prevState.isTablurForm,
    }));
  }

  render() {
    const { data, config, electionData } = this.props;
    let renderData = {};
    if (data && data.Other) {
      renderData.Bihar = data.Other;
    } else {
      renderData = data || {};
    }
    const stateName = "bihar"; // harcoding the state here
    const statemsid = siteElectionConfig.exitpollMsidStateMap[stateName];

    return (
      <div className="row exitpoll-widget result_inner" style={{ width: "auto" }}>
        <SectionHeader
          sectionhead={siteElectionConfig.hpExitpollWidgetHeading}
          weblink={generateStateExitPollLinks(stateName, statemsid)}
        />
        <ChartTableToggleButton toggleChart={() => this.toggleChart(this)} isTablurForm={this.state.isTablurForm} />
        <div className={`tablecount ${this.state.isTablurForm ? "show" : "hide"}`} data-elem="table">
          <ErrorBoundary>
            <ExitPollCombinedTable data={renderData} combineData stateName="Bihar" exitPollWidget />
          </ErrorBoundary>
        </div>
        <div className={`graphcount ${this.state.isTablurForm ? "hide" : "show"}`} data-elem="pie-chart">
          <ErrorBoundary>
            <ExitPollCombinedPie data={renderData} combineData stateName="Bihar" exitPollWidget />
          </ErrorBoundary>
        </div>
        <Disclaimer electionData={electionData} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.exitpoll,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

ExitPollHPWidget.fetchData = ({ dispatch, params, query }) => {
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    let configData = {};
    if (data && data.payload) {
      configData = getFeedUrlObjectFromElectionConfigData(data.payload, "exitpollurl");
    }
    dispatch(fetchExitPollIfNeeded(params, query, configData));
  });
};

ExitPollHPWidget.propTypes = {
  config: PropTypes.object,
  data: PropTypes.object,
};

export default connect(mapStateToProps)(ExitPollHPWidget);
