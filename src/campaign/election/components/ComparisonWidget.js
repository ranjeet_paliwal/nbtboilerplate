/* eslint-disable indent */
import React from "react";
import PropTypes from "prop-types";
import PieChart from "./PieChart";
import Table from "./Table";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import { isMobilePlatform, _isCSR } from "../../../utils/util";
import {
  modifyStateName,
  generateStateResultslLinks,
  countSeats,
  checkStateInData,
  extractPrevDataInCurrentFormat,
} from "../utils/util";
import { electionConfig } from "../utils/config";
import AdCard from "../../../components/common/AdCard";
const siteElectionConfig = electionConfig[process.env.SITE];
const statemsid = siteElectionConfig.resultMsidStateMap.bihar;

const ComparisonWidget = props => {
  const { type, data, combineData, config, countText, source } = props;
  if (!_isCSR() && type === "graph") {
    return "";
  }
  const TagName = combineData ? "div" : React.Fragment;
  // const Component = type === "graph" ? PieChart : Table;
  return (
    <React.Fragment>
      {combineData && !checkStateInData(data, "Bihar") && (
        <SectionHeader sectionhead="BIHAR" weblink={generateStateResultslLinks("bihar", statemsid)} />
      )}
      {Object.keys(data).map(key => {
        const formattedStateName = modifyStateName({ stateName: key }).toLowerCase();
        const msid = siteElectionConfig.resultMsidStateMap[formattedStateName];
        return (
          <TagName className="combinedchartbox">
            {config && config[0] && <SectionHeader sectionhead={`${config[0].resultyear} VS ${config[0].prevyear}`} />}
            {data && data[key] && data[key].al_rslt
              ? data[key].al_rslt.map((item, index) => {
                  const prevYearData = extractPrevDataInCurrentFormat(item);
                  const prevYearSeatsData = countSeats(prevYearData);
                  return (
                    <React.Fragment>
                      {_isCSR() && (
                        <div className={isMobilePlatform() ? "col12" : "col6"} data-exclude="amp">
                          <PieChart
                            items={item}
                            _index={index}
                            total={data[key].ttl_seat}
                            state={(config && config[0].resultyear) || formattedStateName}
                            seats={countSeats(item)}
                            type="Results"
                            source={source}
                            countText={countText}
                          />
                        </div>
                      )}
                      {/* Prev Year PieChart */}
                      {_isCSR() && (
                        <div className={isMobilePlatform() ? "col12" : "col6"} data-exclude="amp">
                          <PieChart
                            items={prevYearData}
                            _index={index}
                            total={prevYearSeatsData.seatCounted || ""}
                            state={(config && config[0].prevyear) || formattedStateName}
                            seats={prevYearSeatsData}
                            source={source}
                            type="Results"
                            isPrevYear
                          />
                        </div>
                      )}
                      <div className={isMobilePlatform() ? "col12" : "col8"}>
                        <Table
                          items={item}
                          _index={index}
                          total={data[key].ttl_seat}
                          state={formattedStateName}
                          seats={countSeats(item)}
                          type="Results"
                          hideTotalRow
                          source={source}
                          isPrevYear
                          headRowData={
                            config
                              ? [config[0].resultyear, config[0].prevyear, "Change(+/-)"]
                              : ["2020", "2015", "Change(+/-)"]
                          }
                        />
                      </div>
                      {isMobilePlatform() ? null : (
                        <div className="col4" style={{ margin: "20px auto" }}>
                          <AdCard mstype="mrec2" adtype="dfp" />
                        </div>
                      )}
                    </React.Fragment>
                  );
                })
              : null}
          </TagName>
        );
      })}
    </React.Fragment>
  );
};

//   export const RenderGraphAndTable

ComparisonWidget.propTypes = {
  data: PropTypes.object,
  combineData: PropTypes.bool,
  type: PropTypes.string,
};

function arePropsEqual(prevProps, nextProps) {
  let flag = false;
  if (prevProps.data && nextProps.data) {
    flag = JSON.stringify(prevProps.data) === JSON.stringify(nextProps.data);
  }
  return prevProps.combineData === nextProps.combineData && flag;
}

// React memo is used to stop rerendering of the component if the props remain same
export default React.memo(ComparisonWidget, arePropsEqual);
