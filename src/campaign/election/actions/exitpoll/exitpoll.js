/* eslint-disable no-unused-vars */
import fetch from "../../../../utils/fetch/fetch";
import { feedUrls } from "../../utils/config";
import { modifyStateName } from "../../utils/util";

export const FETCH_EXITPOLL_REQUEST = "FETCH_EXITPOLL_REQUEST";
export const FETCH_EXITPOLL_SUCCESS = "FETCH_EXITPOLL_SUCCESS";
export const FETCH_EXITPOLL_FAILURE = "FETCH_EXITPOLL_FAILURE";
export const FETCH_EXITPOLL_META_REQUEST = "FETCH_EXITPOLL_META_REQUEST";
export const FETCH_EXITPOLL_META_SUCCESS = "FETCH_EXITPOLL_META_SUCCESS";
export const FETCH_EXITPOLL_META_FAILURE = "FETCH_EXITPOLL_META_FAILURE";
export const REMOVE_DATA_IF_PRESENT = "REMOVE_DATA_IF_PRESENT";
export const SET_AUTO_REFRESH_EXITPOLL = "SET_AUTO_REFRESH_EXITPOLL";
export const REMOVE_AUTO_REFRESH_EXITPOLL = "REMOVE_AUTO_REFRESH_EXITPOLL";

export function removeDataIfPresent() {
  return {
    type: REMOVE_DATA_IF_PRESENT,
  };
}

function fetchExitPollDataFailure(error) {
  return {
    type: FETCH_EXITPOLL_FAILURE,
    payload: error.message,
  };
}

function fetchExitPollDataSuccess(data) {
  return {
    type: FETCH_EXITPOLL_SUCCESS,
    payload: data,
  };
}

function fetchExitPollMetaDataFailure(error) {
  return {
    type: FETCH_EXITPOLL_META_FAILURE,
    payload: error.message,
  };
}

function fetchExitPollMetaDataSuccess(data) {
  return {
    type: FETCH_EXITPOLL_META_SUCCESS,
    payload: data,
  };
}

export function setAutoRefreshExitPoll(data) {
  return {
    type: SET_AUTO_REFRESH_EXITPOLL,
    payload: data,
  };
}

export function removeAutoRefreshExitpoll() {
  return {
    type: REMOVE_AUTO_REFRESH_EXITPOLL,
  };
}

function fetchExitPollData(state, params, query, config, fetchDataSilently) {
  let exitpollurl = config && config.url ? config.url : undefined;
  if (!exitpollurl) {
    return Promise.resolve([]);
  }
  const time = new Date().getTime();
  exitpollurl += `?tp=${time}`;
  // const exitpollurl = feedUrls.exitpolls;
  return dispatch => {
    if (!fetchDataSilently) {
      dispatch({ type: FETCH_EXITPOLL_REQUEST });
    }

    return fetch(exitpollurl, { type: "jsonp" })
      .then(
        data => {
          let parsedData;
          if (typeof data === "string") {
            data = data.replace(/[^\(]*/, ""); // replace anything before first occurance of "("
            data = data.substring(1, data.length - 1); // remove first and last character of the string to make it parsable
            parsedData = JSON.parse(data);
          } else {
            parsedData = data;
          }
          if (parsedData.al_rslt) {
            const newParsedData = {};
            const stateName = config.state
              ? modifyStateName({ stateName: config.state, search: "-", replaceWith: "_" })
              : "Other";
            // const stateName = modifyStateName({ stateName: config.state }) || "Other";
            newParsedData[stateName] = parsedData;
            // newParsedData.push({ Maharashtra: parsedData });
            parsedData = newParsedData;
          }
          return dispatch(fetchExitPollDataSuccess(parsedData));
          // dispatch(fetchExitPollDataSuccess(JSON.parse(data)));
          // dispatch(fetchExitPollDataSuccess(JSON.parse(JSON.parse(data))));
        },
        error => {
          console.log(error);
          return dispatch(fetchExitPollDataFailure(error));
        },
      )
      .catch(error => {
        console.log(error);
        return dispatch(fetchExitPollDataFailure(error));
      });
  };
}

function shouldfetchExitPollData(state, params, query) {
  // debugger;
  // if (state.exitpoll.data !== undefined) {
  // 	return false;
  // }

  // if(state.exitpoll && state.exitpoll.data){
  // 	return false
  // }else{
  return true;
  // }
}

export function fetchExitPollIfNeeded(params, query, data, fetchDataSilently) {
  return (dispatch, getState) => {
    if (shouldfetchExitPollData(getState(), params, query)) {
      return dispatch(fetchExitPollData(getState(), params, query, data, fetchDataSilently));
    }
    return Promise.resolve([]);
  };
}

function fetchExitPollMetaData(state, params, query) {
  if (!params || !params.msid) {
    return dispatch => {
      return;
    };
  }

  const { msid } = params;
  const url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_EXITPOLL_META_REQUEST });

    return fetch(url)
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);
          return dispatch(fetchExitPollMetaDataSuccess(data));
        },
        error => dispatch(fetchExitPollMetaDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchExitPollMetaDataFailure(error));
      });
  };
}

function shouldfetchExitPollMetaData(state, params, query) {
  return true;
}

export function fetchExitPollMetaIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldfetchExitPollMetaData(getState())) {
      return dispatch(fetchExitPollMetaData(getState(), params, query));
    }
    return new Promise.resolve([]);
  };
}
