/* eslint-disable no-unused-vars */
import fetch from "../../../../utils/fetch/fetch";
// import { feedUrls } from "../../utils/config";
import { modifyStateName } from "../../utils/util";
import resultObject from "../../utils/combinedresultsdata";

export const FETCH_RESULTS_REQUEST = "FETCH_RESULTS_REQUEST";
export const FETCH_RESULTS_SUCCESS = "FETCH_RESULTS_SUCCESS";
export const FETCH_RESULTS_FAILURE = "FETCH_RESULTS_FAILURE";
export const FETCH_RESULTS_META_REQUEST = "FETCH_RESULTS_META_REQUEST";
export const FETCH_RESULTS_META_SUCCESS = "FETCH_RESULTS_META_SUCCESS";
export const FETCH_RESULTS_META_FAILURE = "FETCH_RESULTS_META_FAILURE";
export const SET_AUTO_RERESH = "SET_AUTO_RERESH";
export const REMOVE_AUTO_REFRESH = "REMOVE_AUTO_REFRESH";
export const REMOVE_DATA_IF_PRESENT = "REMOVE_DATA_IF_PRESENT";

export function removeDataIfPresent() {
  return {
    type: REMOVE_DATA_IF_PRESENT,
  };
}

export function setAutoRefresh(data) {
  return {
    type: SET_AUTO_RERESH,
    payload: data,
  };
}

export function removeAutoRefresh() {
  return {
    type: REMOVE_AUTO_REFRESH,
  };
}

function fetchResultsDataFailure(error) {
  return {
    type: FETCH_RESULTS_FAILURE,
    payload: error.message,
  };
}

function fetchResultsDataSuccess(data) {
  return {
    type: FETCH_RESULTS_SUCCESS,
    payload: data,
  };
}

function fetchResultsMetaDataFailure(error) {
  return {
    type: FETCH_RESULTS_META_FAILURE,
    payload: error.message,
  };
}

function fetchResultsMetaDataSuccess(data) {
  return {
    type: FETCH_RESULTS_META_SUCCESS,
    payload: data,
  };
}

function fetchResultsData(state, params, query, config, fetchDataSilently) {
  let resultsUrl = config && config.url ? config.url : undefined;
  if (!resultsUrl) {
    return Promise.resolve([]);
  }
  const time = new Date().getTime();
  resultsUrl += `?tp=${time}`;
  // resultsUrl = "https://toibnews.timesofindia.indiatimes.com/Election/jharkhandresult.htm";
  return dispatch => {
    if (!fetchDataSilently) {
      dispatch({ type: FETCH_RESULTS_REQUEST });
    }

    return fetch(resultsUrl, { type: "jsonp" })
      .then(
        data => {
          let parsedData;
          if (typeof data === "string") {
            data = data.replace(/[^\(]*/, ""); // replace anything before first occurance of "("
            data = data.substring(1, data.length - 1); // remove first and last character of the string to make it parsable
            parsedData = JSON.parse(data);
          } else {
            parsedData = data;
          }
          // parsedData = resultObject;
          if (parsedData.al_rslt) {
            const newParsedData = {};
            const stateName = config.state
              ? modifyStateName({ stateName: config.state, search: "-", replaceWith: "_" })
              : "West_Bengal";
            newParsedData[stateName] = parsedData;
            // newParsedData.push({ Maharashtra: parsedData });
            parsedData = newParsedData;
          }
          return dispatch(fetchResultsDataSuccess(parsedData));
        },
        error => {
          console.log(error);
          return dispatch(fetchResultsDataFailure(error));
        },
      )
      .catch(error => {
        console.log(error);
        return dispatch(fetchResultsDataFailure(error));
      });
  };
}

function shouldfetchResultsData(state, params, query) {
  // debugger;
  // if (state.Results.data !== undefined) {
  // 	return false;
  // }

  // if(state.Results && state.Results.data){
  // 	return false
  // }else{
  return true;
  // }
}

export function fetchResultsIfNeeded(params, query, data, fetchDataSilently) {
  return (dispatch, getState) => {
    if (shouldfetchResultsData(getState(), params, query)) {
      return dispatch(fetchResultsData(getState(), params, query, data, fetchDataSilently));
    }
    return Promise.resolve([]);
  };
}

function fetchResultsMetaData(state, params, query) {
  if (!params || !params.msid) {
    return dispatch => {
      return;
    };
  }

  const { msid } = params;
  const url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_RESULTS_META_REQUEST });

    return fetch(url)
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);
          return dispatch(fetchResultsMetaDataSuccess(data));
        },
        error => dispatch(fetchResultsMetaDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchResultsMetaDataFailure(error));
      });
  };
}

function shouldfetchResultsMetaData(state, params, query) {
  return true;
}

export function fetchResultsMetaIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldfetchResultsMetaData(getState())) {
      return dispatch(fetchResultsMetaData(getState(), params, query));
    }
    return new Promise.resolve([]);
  };
}
