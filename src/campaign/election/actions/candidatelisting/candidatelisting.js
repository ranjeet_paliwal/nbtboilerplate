/* eslint-disable no-unused-vars */
import fetch from "../../../../utils/fetch/fetch";
import { feedUrls } from "../../utils/config";
import { modifyStateName } from "../../utils/util";

export const FETCH_CANDIDATELIST_REQUEST = "FETCH_CANDIDATELIST_REQUEST";
export const FETCH_CANDIDATELIST_SUCCESS = "FETCH_CANDIDATELIST_SUCCESS";
export const FETCH_CANDIDATELIST_FAILURE = "FETCH_CANDIDATELIST_FAILURE";
export const FETCH_CANDIDATELIST_META_REQUEST = "FETCH_CANDIDATELIST_META_REQUEST";
export const FETCH_CANDIDATELIST_META_SUCCESS = "FETCH_CANDIDATELIST_META_SUCCESS";
export const FETCH_CANDIDATELIST_META_FAILURE = "FETCH_CANDIDATELIST_META_FAILURE";
export const REMOVE_DATA_IF_PRESENT = "REMOVE_DATA_IF_PRESENT";
export const SET_AUTO_RERESH_CANDLIST = "SET_AUTO_RERESH_CANDLIST";
export const REMOVE_AUTO_REFRESH_CANDLIST = "REMOVE_AUTO_REFRESH_CANDLIST";

export function removeDataIfPresent() {
  return {
    type: REMOVE_DATA_IF_PRESENT,
  };
}

export function setAutoRefresh(data) {
  return {
    type: SET_AUTO_RERESH_CANDLIST,
    payload: data,
  };
}

export function removeAutoRefresh() {
  return {
    type: REMOVE_AUTO_REFRESH_CANDLIST,
  };
}

function fetchCandidateListDataFailure(error) {
  return {
    type: FETCH_CANDIDATELIST_FAILURE,
    payload: error.message,
  };
}

function fetchCandidateListDataSuccess(data) {
  return {
    type: FETCH_CANDIDATELIST_SUCCESS,
    payload: data,
  };
}

function fetchCandidateListMetaDataFailure(error) {
  return {
    type: FETCH_CANDIDATELIST_META_FAILURE,
    payload: error.message,
  };
}

function fetchCandidateListMetaDataSuccess(data) {
  return {
    type: FETCH_CANDIDATELIST_META_SUCCESS,
    payload: data,
  };
}

function fetchCandidateListData(state, params, query, config) {
  const candidateUrl = config && config.url ? config.url : feedUrls.candidates;
  // const candidateUrl = feedUrls.exitpolls;
  return dispatch => {
    dispatch({ type: FETCH_CANDIDATELIST_REQUEST });

    return fetch(candidateUrl, { type: "jsonp" })
      .then(
        data => {
          // eslint-disable-next-line no-param-reassign
          let parsedData;
          if (typeof data === "string") {
            data = data.replace(/[^\(]*/, ""); // replace anything before first occurance of "("
            data = data.substring(1, data.length - 1); // remove first and last character of the string to make it parsable
            parsedData = JSON.parse(data);
          } else {
            parsedData = data;
          }
          if (!parsedData.cnt_rslt) {
            const stateName = config.state
              ? modifyStateName({ stateName: config.state, search: "-", replaceWith: "_" })
              : "Other";
            parsedData = parsedData[stateName];
          }
          return dispatch(fetchCandidateListDataSuccess(parsedData));
        },
        error => {
          console.log(error);
          return dispatch(fetchCandidateListDataFailure(error));
        },
      )
      .catch(error => {
        console.log(error);
        return dispatch(fetchCandidateListDataFailure(error));
      });
  };
}

function shouldfetchCandidateListData(state, params, query) {
  // debugger;
  // if (state.exitpoll.data !== undefined) {
  // 	return false;
  // }

  // if(state.exitpoll && state.exitpoll.data){
  // 	return false
  // }else{
  return true;
  // }
}

export function fetchCandidateListIfNeeded(params, query, data) {
  return (dispatch, getState) => {
    if (shouldfetchCandidateListData(getState(), params, query)) {
      return dispatch(fetchCandidateListData(getState(), params, query, data));
    }
    return Promise.resolve([]);
  };
}

function fetchCandidateListMetaData(state, params, query) {
  if (!params || !params.msid) {
    return dispatch => {
      return;
    };
  }

  const { msid } = params;
  const url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_CANDIDATELIST_META_REQUEST });

    return fetch(url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchCandidateListMetaDataSuccess(data));
        },
        error => dispatch(fetchCandidateListMetaDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchCandidateListMetaDataFailure(error));
      });
  };
}

function shouldfetchCandidateListMetaData(state, params, query) {
  return true;
}

export function fetchCandidateListMetaIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldfetchCandidateListMetaData(getState())) {
      return dispatch(fetchCandidateListMetaData(getState(), params, query));
    }
    return new Promise.resolve([]);
  };
}
