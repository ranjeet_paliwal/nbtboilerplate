/* eslint-disable no-unused-vars */
import fetch from "../../../../utils/fetch/fetch";
import { getFeedUrlObjectFromElectionConfigData } from "../../utils/util";
export const FETCH_ELECTORALMAP_META_REQUEST = "FETCH_ELECTORALMAP_META_REQUEST";
export const FETCH_ELECTORALMAP_META_SUCCESS = "FETCH_ELECTORALMAP_META_SUCCESS";
export const FETCH_ELECTORALMAP_META_FAILURE = "FETCH_ELECTORALMAP_META_FAILURE";
export const FETCH_ELECTION_CONFIG_SUCCESS = "FETCH_ELECTION_CONFIG_SUCCESS";
export const FETCH_ELECTION_CONFIG_FAILURE = "FETCH_ELECTION_CONFIG_FAILURE";
export const FETCH_ELECTION_CONFIG_REQUEST = "FETCH_ELECTION_CONFIG_REQUEST";

export function fetchElectionConfigData(params, query, callbackFunction, type) {
  const { msid, location } = params || {};
  let url = `${process.env.API_BASEPOINT}/feed_elections.cms?feedtype=sjson`;
  url += msid ? `&msid=${msid}` : location ? `&location=${location}` : "";
  return dispatch => {
    return fetch(url)
      .then(
        data => {
          dispatch({ type: FETCH_ELECTION_CONFIG_REQUEST });
          return dispatch(fetchElectionConfigSuccess(data));
        },
        error => {
          console.error(error);
          return dispatch(fetchElectionConfigFailure(error));
        },
      )
      .catch(error => {
        console.error(error);
        return dispatch(fetchElectionConfigFailure(error));
      });
  };
}

function fetchElectionConfigFailure(error) {
  return {
    type: FETCH_ELECTION_CONFIG_FAILURE,
    payload: error.message,
  };
}

export function fetchElectionConfigSuccess(data) {
  return {
    type: FETCH_ELECTION_CONFIG_SUCCESS,
    payload: data,
  };
}

function fetchElectoralMapMetaDataFailure(error) {
  return {
    type: FETCH_ELECTORALMAP_META_FAILURE,
    payload: error.message,
  };
}

function fetchElectoralMapMetaDataSuccess(data) {
  return {
    type: FETCH_ELECTORALMAP_META_SUCCESS,
    payload: data,
  };
}

function fetchElectoralMapMetaData(state, params, query) {
  if (!params || !params.msid) {
    return dispatch => {
      return;
    };
  }

  const { msid } = params;
  const url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_ELECTORALMAP_META_REQUEST });

    return fetch(url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchElectoralMapMetaDataSuccess(data));
        },
        error => dispatch(fetchElectoralMapMetaDataFailure(error)),
      )
      .catch(error => {
        console.error(error);
        dispatch(fetchElectoralMapMetaDataFailure(error));
      });
  };
}

function shouldfetchElectoralMapMetaData(state, params, query) {
  return true;
}

export function fetchElectoralMapMetaIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldfetchElectoralMapMetaData(getState())) {
      return dispatch(fetchElectoralMapMetaData(getState(), params, query));
    }
    return new Promise.resolve([]);
  };
}
