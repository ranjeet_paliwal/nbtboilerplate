import React from "react";
import "../../common/css/Election.scss";
// import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { setPageType, setParentId } from "../../../../actions/config/config";
import { getIframeUrl, modifyStateName } from "../../utils/util";
import { isMobilePlatform, updateConfig } from "../../../../utils/util";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import StarCandidatesWidget from "../../components/StarCandidatesWidget";
import VideoPhotoNewsCombined from "../../../common/components/videoPhotoNewsCombined";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import ElectoralMapWidget from "../../components/ElectoralMapWidget";
import { electionConfig } from "../../utils/config";
import { setSectionDetail } from "../../../../components/lib/ads/index";
import { fetchElectoralMapMetaIfNeeded, fetchElectionConfigData } from "../../actions/electoralmap/electoralmap";
const siteElectionConfig = electionConfig[process.env.SITE];

class ElectoralMap extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { dispatch, params, query } = this.props;
    ElectoralMap.fetchData({ dispatch, params, query })
      .then(() => {
        // set section for election related pages
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
        const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
        if (pwa_meta && typeof pwa_meta == "object") {
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.error(err.message);
        // set section for election related pages
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      });
  }

  componentWillUnmount() {
    // reset  section details
    setSectionDetail();
  }

  getURLForMapFromConfig = () => {
    let url = "";
    if (this.props.configError) {
      url = getIframeUrl(this.props.routeParams.statename);
      return url;
    }
    if (this.props.configData && this.props.configData[0]) {
      url = this.props.configData[0].electoralpageiframeurl;
    }
    isMobilePlatform() ? (url += "?frmapp=yes&isiframe=yes") : (url += "");
    return url;
  };

  render() {
    const { metadata, electionData, routeParams } = this.props;
    const { pwa_meta } = metadata || {};
    const { breadcrumb } = metadata || {};
    const url = this.getURLForMapFromConfig();
    return (
      <div className="electoralmap_body">
        {/* For SEO Meta */ pwa_meta ? PageMeta(pwa_meta) : null}
        {breadcrumb && !isMobilePlatform() && <Breadcrumb items={breadcrumb.div.ul} />}
        <div className="map-container row">
          <div className={isMobilePlatform() ? "col12" : "col12"}>
            <div className={`${isMobilePlatform() ? "wdt_map_mobile" : "wdt_map_desktop"}`}>
              <SectionHeader
                sectionhead={pwa_meta ? `${pwa_meta.pageheading}` : siteElectionConfig.assemblyMapHeading}
                headingTag="h1"
              />
              <ElectoralMapWidget url={url} />
            </div>
            <StarCandidatesWidget
              showCandidateStatus={electionData.showcandidatestatus}
              stateName={
                routeParams.statename
                  ? modifyStateName({ stateName: routeParams.statename, search: "-", replaceWith: "_" })
                  : "Other"
              }
              size={6}
            />
          </div>
        </div>
        <VideoPhotoNewsCombined
          params={this.props.params}
          query={this.props.query}
          sectionMsid={siteElectionConfig.electionSectionMsidMap[this.props.params.statename] || 123456}
        />
        {pwa_meta && pwa_meta.syn != undefined && typeof pwa_meta.syn == "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{pwa_meta.syn}</div>
          </ErrorBoundary>
        ) : null}
        {breadcrumb && isMobilePlatform() && <Breadcrumb items={breadcrumb.div.ul} />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.electoralmap,
    electionData: state.app.election,
  };
}

ElectoralMap.propTypes = {
  routeParams: PropTypes.object,
};

ElectoralMap.fetchData = ({ dispatch, params, query }) => {
  dispatch(setPageType("elections"));
  dispatch(fetchElectionConfigData(params, query));
  return dispatch(fetchElectoralMapMetaIfNeeded(params, query));
};

export default connect(mapStateToProps)(ElectoralMap);
