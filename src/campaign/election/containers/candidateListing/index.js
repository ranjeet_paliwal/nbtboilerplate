/* eslint-disable camelcase */
import React from "react";

import { connect } from "react-redux";
import PropTypes from "prop-types";

import { setPageType, setParentId } from "../../../../actions/config/config";
import { fetchElectionConfigData } from "../../actions/electoralmap/electoralmap";
import { PageMeta } from "../../../../components/common/PageMeta"; // For Page SEO/Head Part
import { getPageType, isMobilePlatform, _isCSR, updateConfig } from "../../../../utils/util";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import StarCandidatesWidget from "../../components/StarCandidatesWidget";
import AdCard from "../../../../components/common/AdCard";
// import ElectionRHSWidget from "../../components/ElectionRHSWidget";
import { electionConfig } from "../../utils/config";
import {
  getFeedUrlObjectFromElectionConfigData,
  getPartyData,
  modifyCandidatesData,
  modifyStateName,
} from "../../utils/util";
import { setSectionDetail } from "../../../../components/lib/ads/index";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import {
  fetchCandidateListIfNeeded,
  fetchCandidateListMetaIfNeeded,
} from "../../actions/candidatelisting/candidatelisting";
import Select from "../../components/Select";
import FakeProfileList from "../../../../components/common/FakeCards/FakeProfileList";

const siteElectionConfig = electionConfig[process.env.SITE];

class CandidateListing extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      filterData: "",
    };
    this.config = {
      partyIndex: 0,
      cnsIndex: 0,
      statusIndex: 0,
      partyObj: null,
    };
  }

  componentDidMount() {
    const { dispatch, params, query, config } = this.props;
    const self = this;
    if (config) {
      const configData = getFeedUrlObjectFromElectionConfigData(config, "electioncandidates");
      dispatch(fetchCandidateListIfNeeded(params, query, configData)).then(data => {
        data.payload && data.payload.cnt_rslt ? modifyCandidatesData(data.payload.cnt_rslt) : [];
        self.config.partyObj = getPartyData(data.payload.cnt_rslt);
        self.setState({ filterData: data.payload.cnt_rslt });
      });
    }
    CandidateListing.fetchData({ dispatch, params, query })
      .then(() => {
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
        const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
        if (pwa_meta && typeof pwa_meta === "object") {
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.error(err);
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      });
  }

  componentWillUnmount() {
    // reset  section details
    setSectionDetail();
  }

  handlePartyChange = index => {
    this.config.partyIndex = index;
    // this.setState({ filterData: null }, () => {
    //   this.getSelectedData();
    // });
    this.getSelectedData();
  };

  handleConstituencyChange = index => {
    this.config.cnsIndex = index;
    this.getSelectedData();
  };

  handleStatusChange = index => {
    this.config.statusIndex = index;
    this.getSelectedData();
  };

  getSelectedData = () => {
    const { partyIndex, cnsIndex, statusIndex, partyObj } = this.config;
    const { filterData } = this.props;

    const selectedParty = partyIndex > 0 ? partyObj && partyObj.partyList && partyObj.partyList[partyIndex] : "";
    const selectedConstituency =
      cnsIndex > 0 ? partyObj && partyObj.constituencyList && partyObj.constituencyList[cnsIndex] : "";
    const selectedStatus = statusIndex > 0 ? partyObj && partyObj.statList && partyObj.statList[statusIndex] : "";

    const selectedData =
      filterData &&
      filterData.filter(
        item =>
          (selectedParty !== "" ? item.pn === selectedParty : true) &&
          (selectedConstituency !== "" ? item.cns_name === selectedConstituency : true) &&
          (selectedStatus !== "" ? item.statusClass === selectedStatus : true),
      );

    // console.log("selectedData-----------", selectedData);
    this.setState({ filterData: selectedData });
  };

  render() {
    const { isFetching, metadata, params, electionData, config } = this.props;
    const { breadcrumb } = metadata || {};
    const { pwa_meta } = metadata || {};
    const { filterData } = this.state;
    const { partyIndex, cnsIndex, statusIndex, partyObj } = this.config;
    // console.log("partyObj---------", partyObj);

    // const states = candidateData && candidateData.yrdata ? candidateData.yrdata[0].states : undefined;
    return (
      <div className="electioncandidates_body">
        {pwa_meta && PageMeta(pwa_meta)}
        {/* Breadcrumb  */}
        {!isMobilePlatform() && breadcrumb && (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        )}
        <div className="widget-container row">
          <div className={isMobilePlatform() ? "col12" : "col8"}>
            <StarCandidatesWidget
              siteElectionConfig={siteElectionConfig}
              size={isMobilePlatform() ? 2 : 4}
              msid={params && params.msid ? params.msid : ""}
              showCandidateStatus={electionData.showcandidatestatus}
              stateName={
                config[0] && config[0].state
                  ? modifyStateName({ stateName: config[0].state, search: "-", replaceWith: "_" })
                  : "Other"
              }
            />
          </div>
          {!isMobilePlatform() && (
            <div className="col4">
              <AdCard mstype="mrec1" adtype="dfp" />
            </div>
          )}
        </div>
        <div className="row">
          <div className="col12">
            <SectionHeader
              sectionhead={pwa_meta ? `${pwa_meta.pageheading}` : siteElectionConfig.candidateTxt}
              headingTag="h1"
            />
          </div>
        </div>

        <div className="map-container row candidatelist">
          {partyObj ? (
            <div className="col12 filtersdiv">
              <Select
                selectList={partyObj.partyList}
                selectedValue={partyObj.partyList[partyIndex]}
                handleSelectChange={this.handlePartyChange}
              />
              <Select
                selectList={partyObj.constituencyList}
                selectedValue={partyObj.constituencyList[cnsIndex]}
                handleSelectChange={this.handleConstituencyChange}
              />
              <Select
                selectList={partyObj.statList}
                selectedValue={partyObj.statList[statusIndex]}
                handleSelectChange={this.handleStatusChange}
              />
            </div>
          ) : null}

          <div className="col12">
            {filterData ? (
              <CandidateCards filterData={filterData} showCandidateStatus={electionData.showcandidatestatus} />
            ) : (
              <FakeProfileList elemSize={isMobilePlatform() ? "2" : "6"} />
            )}
          </div>
        </div>
        {pwa_meta && pwa_meta.syn !== undefined && typeof pwa_meta.syn === "string" ? (
          <ErrorBoundary>
            <div className="btf-content">{pwa_meta.syn}</div>
          </ErrorBoundary>
        ) : null}
        {isMobilePlatform() && breadcrumb && (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        )}
      </div>
    );
  }
}

const CandidateCards = props => {
  const { filterData } = props;
  let { showCandidateStatus } = props;
  if (showCandidateStatus && showCandidateStatus === "false") {
    showCandidateStatus = false;
  } else {
    showCandidateStatus = true;
  }
  console.log("candidates data --- ", filterData);
  return filterData.length > 0 ? (
    filterData.map(item => (
      <div className="slide" key={item.c_id}>
        <div
          className={`candidate_box ${showCandidateStatus ? item.statusClass : ""}`}
          onClick={e => e.preventDefault()}
        >
          <span className={`candidate_imgwrap ${showCandidateStatus ? item.statusClass : ""}`}>
            <ImageCard
              msid={item.img === "0" ? "69347424" : item.img || "69347424"}
              title="title"
              size="smallthumb"
              className="candidate_img"
              imgver={item.imgsize}
            />
          </span>
          <span className="candidate_name">{item.cn}</span>
          <span className="constituency">{item.cns_name}</span>
          <span className="party-name" style={{ backgroundColor: item.cc }}>
            {item.an}
          </span>
          {/* <span className="state">{item.statenm}</span> */}
        </div>
      </div>
    ))
  ) : (
    <div className="nodataeletions">
      <div>
        <span>No results found</span> Try adjusting your filter to find what you're looking for.
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    ...state.candidatelist,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

CandidateListing.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.string,
  routeParams: PropTypes.object,
  pagetype: PropTypes.string,
  router: PropTypes.any,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
  metadata: PropTypes.object,
};

CandidateListing.fetchData = ({ dispatch, params, query }) => {
  dispatch(setPageType("elections"));
  dispatch(fetchCandidateListMetaIfNeeded(params, query)).then(data => {
    let pwaMeta;
    if (data) {
      if (data.payload) {
        pwaMeta = data.payload.pwa_meta;
      }
    }
    if (pwaMeta) {
      setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    // let configData = {};
    // if (data && data.payload) {
    //   configData = getFeedUrlObjectFromElectionConfigData(data.payload, "electioncandidates");
    // }
    // console.log("configData---------", configData);
    // dispatch(fetchCandidateListIfNeeded(params, query, configData));
  });
};

export default connect(mapStateToProps)(CandidateListing);
