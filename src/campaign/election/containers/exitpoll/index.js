import React from "react";
import ReactDOM from "react-dom";
// import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "../../common/css/Election.scss";
import ExitPollCombinedPie from "../../components/ExitPollCombinedPie";
import ExitPollCombinedTable from "../../components/ExitPollCombinedTable";
// import RefreshBtnComponent from "../../components/RefreshBtnComponent";
import { setPageType, setParentId } from "../../../../actions/config/config";
import {
  fetchExitPollIfNeeded,
  fetchExitPollMetaIfNeeded,
  removeDataIfPresent,
  setAutoRefreshExitPoll,
  removeAutoRefreshExitpoll,
} from "../../actions/exitpoll/exitpoll";
import { fetchElectionConfigData } from "../../actions/electoralmap/electoralmap";
import { PageMeta } from "../../../../components/common/PageMeta"; // For Page SEO/Head Part
import { getPageType, isMobilePlatform, updateConfig, _isCSR } from "../../../../utils/util";
import FakeStoryCard from "../../../../components/common/FakeCards/FakeStoryCard";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import ElectionRHSWidget from "../../components/ElectionRHSWidget";
import ChartTableToggleButton from "../../components/ChartTableToggleButton";
import VideoPhotoNewsCombined from "../../../common/components/videoPhotoNewsCombined";
import SocialShare from "../../../../components/common/SocialShare";
import { electionConfig } from "../../utils/config";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { setSectionDetail } from "../../../../components/lib/ads/index";
import { modifyStateName, getFeedUrlObjectFromElectionConfigData } from "../../utils/util";
import Disclaimer from "../../components/Disclaimer";

const siteElectionConfig = electionConfig[process.env.SITE];
const refreshInterval = 15000;
class ExitPoll extends React.PureComponent {
  constructor(props) {
    super(props);
    const { routeParams } = this.props;
    const openData = !!routeParams.statename;
    let stateName = "";
    if (openData) {
      stateName = modifyStateName({
        stateName: routeParams.statename,
        search: "-",
        replaceWith: "_",
      });
    }

    this.state = {
      isTablurForm: false,
      showCombinedData: !openData,
      stateName: stateName,
    };
  }

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;

    let sharedata = {
      title: document.title,
      url: location.href,
    };

    if (!isShareAll) return sharedata;
    return false;
  }

  componentDidMount() {
    const { dispatch, params, query, electionData, config } = this.props;
    ExitPoll.fetchData({ dispatch, params, query })
      .then(() => {
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
        const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
        if (pwa_meta && typeof pwa_meta == "object") {
          updateConfig(pwa_meta, dispatch, setParentId);
        }
        const autoRefreshFlag = electionData.exitpollrefresh && electionData.exitpollrefresh === "false" ? false : true;
        if (autoRefreshFlag && config && config[0]) {
          if (this.props.refreshProcessId) {
            dispatch(removeAutoRefreshExitpoll());
          }
          const fetchDataSilently = true;
          const data = getFeedUrlObjectFromElectionConfigData(config, "exitpollurl");
          const refreshProcessId = setInterval(() => {
            dispatch(fetchExitPollIfNeeded(params, query, data, fetchDataSilently));
          }, refreshInterval);
          console.log(refreshProcessId, "is refreshprocessid");
          dispatch(setAutoRefreshExitPoll(refreshProcessId));
        }
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.error(err.message);
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      });
  }

  componentDidUpdate(prevProps) {
    const prevStateName = prevProps.routeParams.statename;
    const curStateName = this.props.routeParams.statename;
    let modifiedStateName = curStateName
      ? modifyStateName({ stateName: curStateName, search: "-", replaceWith: "_" })
      : "";
    if ((!prevStateName && curStateName) || (prevStateName && !curStateName)) {
      const showCombinedData = !curStateName;
      this.setState({ showCombinedData, stateName: modifiedStateName });
      const { dispatch, params, query, electionData, config } = this.props;
      dispatch(fetchExitPollMetaIfNeeded(params, query)).then(data => {
        let pwaMeta;
        if (data) {
          if (data.payload) {
            pwaMeta = data.payload.pwa_meta;
          }
        }
        if (pwaMeta) {
          setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
          updateConfig(pwaMeta, dispatch, setParentId);
        }
      });
      if (curStateName && this.props.data && modifiedStateName in this.props.data) {
        return;
      }
      if (!curStateName && this.props.data && Object.keys(this.props.data).length > 1) {
        this.props.data;
        return;
      }
      dispatch(removeAutoRefreshExitpoll());
      dispatch(removeDataIfPresent());
      ExitPoll.fetchData({ dispatch, params, query })
        .then(() => {
          const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
          if (pwa_meta && typeof pwa_meta == "object") {
            updateConfig(pwa_meta, dispatch, setParentId);
          }
          const autoRefreshFlag =
            electionData.exitpollrefresh && electionData.exitpollrefresh === "false" ? false : true;
          dispatch(fetchExitPollIfNeeded(params, query, data, fetchDataSilently));

          if (autoRefreshFlag && config && config[0]) {
            if (this.props.refreshProcessId) {
              dispatch(removeAutoRefreshExitpoll());
            }
            const fetchDataSilently = true;
            const data = getFeedUrlObjectFromElectionConfigData(config, "exitpollurl");
            const refreshProcessId = setInterval(() => {
              dispatch(fetchExitPollIfNeeded(params, query, data, fetchDataSilently));
            }, refreshInterval);
            dispatch(setAutoRefreshExitPoll(refreshProcessId));
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.error(err.message);
        });
    }
  }

  componentWillUnmount() {
    // reset  section details
    setSectionDetail();
    this.props.dispatch(removeAutoRefreshExitpoll());
  }

  toggleChart() {
    this.setState(prevState => ({
      isTablurForm: !prevState.isTablurForm,
    }));
  }

  render() {
    const { data, isFetching, metadata, electionData } = this.props;
    let { pagetype } = this.props;
    if (!pagetype && this.props.router) {
      pagetype = getPageType(this.props.router.location.pathname);
    }
    const { showCombinedData, stateName } = this.state;

    const { breadcrumb } = metadata || {};
    const { pwa_meta } = metadata || {};

    if (data && !isFetching) {
      return (
        <div className="election_con exitpoll_container">
          {pwa_meta && PageMeta(pwa_meta)}
          {/* Breadcrumb  */}
          {!isMobilePlatform() && breadcrumb && (
            <ErrorBoundary>
              <Breadcrumb items={breadcrumb.div.ul} />
            </ErrorBoundary>
          )}
          <div className="el_exitpoll el_widget">
            <div className="result_inner">
              {isMobilePlatform() ? (
                <React.Fragment>
                  <SectionHeader
                    sectionhead={pwa_meta ? `${pwa_meta.pageheading}` : siteElectionConfig.assemblyExitPollHeading}
                    headingTag="h1"
                    headerClass="noborder"
                  />
                  {!showCombinedData && this.props.config && this.props.config[0] && this.props.config[0].year && (
                    <h2 className="tbl_txt">{this.props.config[0].year}</h2>
                  )}
                </React.Fragment>
              ) : (
                <div className="row">
                  <div className="col8 posrelation">
                    <SectionHeader
                      sectionhead={pwa_meta ? `${pwa_meta.pageheading}` : siteElectionConfig.assemblyExitPollHeading}
                      headingTag="h1"
                      headerClass="noborder"
                    />
                    <ChartTableToggleButton
                      toggleChart={() => this.toggleChart(this)}
                      isTablurForm={this.state.isTablurForm}
                    />
                    {!showCombinedData && this.props.config && this.props.config[0] && this.props.config[0].year && (
                      <h2 className="tbl_txt">{this.props.config[0].year}</h2>
                    )}
                  </div>
                  <div className="col4"></div>
                </div>
              )}

              <div className={`row ${this.state.isTablurForm ? "hide" : "show"}`} data-elem="pie-chart">
                <ErrorBoundary>
                  <ExitPollCombinedPie
                    sequence={
                      this.props.config && this.props.config[0] && this.props.config[0].sequence
                        ? this.props.config[0].sequence
                        : undefined
                    }
                    data={data}
                    combineData={showCombinedData}
                    stateName={stateName}
                  />
                </ErrorBoundary>
                {!isMobilePlatform() && (
                  <div className="col4">
                    <ElectionRHSWidget hideSecondAd={showCombinedData} />
                  </div>
                )}
              </div>

              {isMobilePlatform() &&
                _isCSR() &&
                ReactDOM.createPortal(
                  <div className="share_container">
                    <ChartTableToggleButton
                      toggleChart={() => this.toggleChart(this)}
                      isTablurForm={this.state.isTablurForm}
                    />
                    <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                  </div>,
                  document.getElementById("share-container-wrapper"),
                )}

              <div className={`row ${this.state.isTablurForm ? "show" : "hide"}`} data-elem="table">
                <ErrorBoundary>
                  <ExitPollCombinedTable
                    sequence={
                      this.props.config && this.props.config[0] && this.props.config[0].sequence
                        ? this.props.config[0].sequence
                        : undefined
                    }
                    data={data}
                    combineData={showCombinedData}
                    stateName={stateName}
                  />
                </ErrorBoundary>
                {!isMobilePlatform() && (
                  <div className="col4">
                    <ElectionRHSWidget />
                  </div>
                )}
              </div>
              <Disclaimer stateName={stateName} electionData={electionData} />
            </div>
          </div>
          <VideoPhotoNewsCombined
            params={this.props.params}
            query={this.props.query}
            sectionMsid={siteElectionConfig.electionSectionMsidMap[this.props.params.statename] || 123456}
          />
          {pwa_meta && pwa_meta.syn != undefined && typeof pwa_meta.syn == "string" ? (
            <ErrorBoundary>
              <div className="btf-content">{pwa_meta.syn}</div>
            </ErrorBoundary>
          ) : null}
          {isMobilePlatform() && breadcrumb && (
            <ErrorBoundary>
              <Breadcrumb items={breadcrumb.div.ul} />
            </ErrorBoundary>
          )}
        </div>
      );
    }
    return <FakeStoryCard />;
  }
}

function mapStateToProps(state) {
  return {
    ...state.exitpoll,
    config: state.electoralmap.configData,
    electionData: state.app.election,
  };
}

ExitPoll.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.string,
  routeParams: PropTypes.object,
  pagetype: PropTypes.string,
  router: PropTypes.any,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
  metadata: PropTypes.object,
};

ExitPoll.fetchData = ({ dispatch, params, query }) => {
  dispatch(setPageType("elections"));
  dispatch(fetchExitPollMetaIfNeeded(params, query)).then(data => {
    let pwaMeta;
    if (data) {
      if (data.payload) {
        pwaMeta = data.payload.pwa_meta;
      }
    }
    if (pwaMeta) {
      setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    let configData = {};
    if (data && data.payload) {
      configData = getFeedUrlObjectFromElectionConfigData(data.payload, "exitpollurl");
    }
    return dispatch(fetchExitPollIfNeeded(params, query, configData));
  });
};

export default connect(mapStateToProps)(ExitPoll);
