/* eslint-disable camelcase */
import React from "react";
import ReactDOM from "react-dom";

// import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styles from "../../common/css/Election.scss";

import { setPageType, setParentId } from "../../../../actions/config/config";
import { fetchElectionConfigData } from "../../actions/electoralmap/electoralmap";
import { PageMeta } from "../../../../components/common/PageMeta"; // For Page SEO/Head Part
import { getPageType, isMobilePlatform, _isCSR, updateConfig } from "../../../../utils/util";
import FakeStoryCard from "../../../../components/common/FakeCards/FakeStoryCard";
// import WebTitleCard from "../../../../components/common/WebTitleCard";
import SocialShare from "../../../../components/common/SocialShare";
import VideoPhotoNewsCombined from "../../../common/components/videoPhotoNewsCombined";
import { electionConfig } from "../../utils/config";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ElectionRHSWidget from "../../components/ElectionRHSWidget";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import RenderGraphAndTable from "../../components/RenderGraphAndTable";
import ChartTableToggleButton from "../../components/ChartTableToggleButton";
import StarCandidatesWidget from "../../components/StarCandidatesWidget";
import ElectoralMapWidget from "../../components/ElectoralMapWidget";
import Adcard from "../../../../components/common/AdCard";
import { setSectionDetail } from "../../../../components/lib/ads/index";
import { modifyStateName, getIframeUrl, getFeedUrlObjectFromElectionConfigData } from "../../utils/util";
import {
  fetchResultsIfNeeded,
  fetchResultsMetaIfNeeded,
  setAutoRefresh,
  removeAutoRefresh,
  removeDataIfPresent,
} from "../../actions/results/results";
import ComparisonWidget from "../../components/ComparisonWidget";
import Disclaimer from "../../components/Disclaimer";

const siteElectionConfig = electionConfig[process.env.SITE];
const refreshInterval = 15000;

class Results extends React.PureComponent {
  constructor(props) {
    super(props);
    const { routeParams } = this.props;
    const openData = !!routeParams.statename;
    let stateName = "";
    if (openData) {
      stateName = modifyStateName({
        stateName: routeParams.statename,
        search: "-",
        replaceWith: "_",
      });
    }
    const isTabularForm = !_isCSR();
    this.state = {
      isTabularForm,
      showCombinedData: !openData,
      stateName,
      showType: "al_rslt",
    };
  }

  componentDidMount() {
    const { dispatch, params, query, configData, electionData } = this.props;
    Results.fetchData({ dispatch, params, query })
      .then(() => {
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
        const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
        if (pwa_meta && typeof pwa_meta == "object") {
          updateConfig(pwa_meta, dispatch, setParentId);
        }
        const autoRefreshFlag = electionData.resultrefresh && electionData.resultrefresh === "false" ? false : true;
        if (autoRefreshFlag && configData && configData.configData && configData.configData[0]) {
          if (this.props.refreshProcessId) {
            dispatch(removeAutoRefresh());
          }
          const fetchDataSilently = true;
          const data = getFeedUrlObjectFromElectionConfigData(configData.configData, "resulturl");
          const refreshProcessId = setInterval(() => {
            dispatch(fetchResultsIfNeeded(params, query, data, fetchDataSilently));
          }, refreshInterval);
          dispatch(setAutoRefresh(refreshProcessId));
        }
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.error(err);
        setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      });
  }

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;

    let sharedata = {
      title: document.title,
      url: location.href,
    };

    if (!isShareAll) return sharedata;
    return false;
  }

  componentDidUpdate(prevProps) {
    const prevStateName = prevProps.routeParams.statename;
    const curStateName = this.props.routeParams.statename;
    let modifiedStateName = curStateName
      ? modifyStateName({ stateName: curStateName, search: "-", replaceWith: "_" })
      : "";
    if ((!prevStateName && curStateName) || (prevStateName && !curStateName)) {
      const showCombinedData = !curStateName;
      this.setState({ showCombinedData, stateName: modifiedStateName });
      // this is optimization for stopping the hit of data when moving from one page to the other
      // if (curStateName && this.props.data && modifiedStateName in this.props.data) {
      //   return;
      // }
      // if (!curStateName && this.props.data && Object.keys(this.props.data).length > 1) {
      //   this.props.data;
      //   return;
      // }
      const { dispatch, params, query, configData, electionData } = this.props;
      dispatch(removeAutoRefresh());
      dispatch(removeDataIfPresent());
      Results.fetchData({ dispatch, params, query })
        .then(() => {
          const { pwa_meta } = this.props.metadata ? this.props.metadata : {};
          if (pwa_meta && typeof pwa_meta == "object") {
            updateConfig(pwa_meta, dispatch, setParentId);
          }
          const autoRefreshFlag = electionData.resultrefresh && electionData.resultrefresh === "false" ? false : true;
          dispatch(fetchResultsIfNeeded(params, query, data, fetchDataSilently));

          if (autoRefreshFlag && configData && configData.configData && configData.configData[0]) {
            if (this.props.refreshProcessId) {
              dispatch(removeAutoRefresh());
            }
            const fetchDataSilently = true;
            const data = getFeedUrlObjectFromElectionConfigData(configData.configData, "resulturl");
            const refreshProcessId = setInterval(() => {
              dispatch(fetchResultsIfNeeded(params, query, data, fetchDataSilently));
            }, refreshInterval);
            dispatch(setAutoRefresh(refreshProcessId));
          }
        })
        .catch(err => {
          // eslint-disable-next-line no-console
          console.error(err.message);
        });
    }
  }

  getURLForMapFromConfig = () => {
    let url = "";
    if (this.props.configData.configError) {
      url = getIframeUrl(this.state.stateName.toLowerCase());
      return url;
    }
    if (this.props.configData && this.props.configData.configData && this.props.configData.configData[0]) {
      url = this.props.configData.configData[0].electoralpageiframeurl;
    }
    if (url) {
      isMobilePlatform() ? (url += "?frmapp=yes&isiframe=yes") : (url += "");
    }
    return url;
  };

  componentWillUnmount() {
    // reset  section details
    setSectionDetail();
    this.props.dispatch(removeAutoRefresh());
  }

  toggleChart() {
    this.setState(prevState => ({
      isTablurForm: !prevState.isTablurForm,
    }));
  }

  toggleShowType = type => {
    if (type === this.state.showType) {
      return;
    }
    this.setState({ showType: type });
  };

  render() {
    const { data, isFetching, metadata, configData, electionData } = this.props;
    let config = configData.configData;
    let renderData = {};
    let { pagetype } = this.props;
    if (!pagetype && this.props.router) {
      pagetype = getPageType(this.props.router.location.pathname);
    }
    const { showCombinedData, stateName } = this.state;

    if (showCombinedData) {
      renderData = data;
    } else {
      renderData[stateName] = data && data[stateName];
    }

    const { breadcrumb } = metadata || {};
    const { pwa_meta } = metadata || {};
    const url = this.getURLForMapFromConfig();

    const TagName = showCombinedData ? "div" : React.Fragment;

    // const renderData = data ? data[stateName] : [];
    // console.log(renderData, "is the renderdata on results page");

    if (data && !isFetching) {
      return (
        <div className="elm_container election_con results_container">
          {pwa_meta && PageMeta(pwa_meta)}
          {/* Breadcrumb  */}
          {!isMobilePlatform() && breadcrumb && (
            <ErrorBoundary>
              <Breadcrumb items={breadcrumb.div.ul} />
            </ErrorBoundary>
          )}
          <div className="el_result el_widget">
            <div className="result_inner">
              {isMobilePlatform() ? (
                <React.Fragment>
                  <SectionHeader
                    sectionhead={
                      pwa_meta && pwa_meta.pageheading
                        ? `${pwa_meta.pageheading}`
                        : siteElectionConfig.assemblyResultsHeading
                    }
                    headingTag="h1"
                    headerClass="noborder"
                  />
                  {config && config[0] && config[0].resultyear && <h2 className="tbl_txt">{config[0].resultyear}</h2>}
                </React.Fragment>
              ) : (
                <div className="row">
                  <div className="col8 posrelation">
                    <SectionHeader
                      sectionhead={
                        pwa_meta && pwa_meta.pageheading
                          ? `${pwa_meta.pageheading}`
                          : siteElectionConfig.assemblyResultsHeading
                      }
                      headingTag="h1"
                      headerClass="noborder"
                    />
                    {!isMobilePlatform() && (
                      <ChartTableToggleButton
                        toggleChart={() => this.toggleChart()}
                        isTablurForm={this.state.isTablurForm}
                      />
                    )}
                    {config && config[0] && config[0].resultyear && <h2 className="tbl_txt">{config[0].resultyear}</h2>}
                  </div>
                  <div className="col4"></div>
                </div>
              )}
              <div className="row stat-container">
                <div className={`results_container ${isMobilePlatform() ? "col12" : "col8"}`}>
                  <div className={`graph-container ${this.state.isTablurForm ? "hide" : "show"}`}>
                    <ErrorBoundary>
                      <RenderGraphAndTable
                        showType={this.state.showType}
                        toggleShowType={this.toggleShowType}
                        data={renderData}
                        type="graph"
                        combineData={showCombinedData}
                        electionData={electionData}
                        source={electionData.source}
                        countText={electionData.counttext}
                        sequence={
                          configData.configData && configData.configData[0] && configData.configData[0].sequence
                            ? configData.configData[0].sequence
                            : undefined
                        }
                      />
                    </ErrorBoundary>
                  </div>

                  {isMobilePlatform() &&
                    _isCSR() &&
                    ReactDOM.createPortal(
                      <div className="share_container" data-exclude="amp">
                        <ChartTableToggleButton
                          toggleChart={() => this.toggleChart(this)}
                          isTablurForm={this.state.isTablurForm}
                        />
                        <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                      </div>,
                      document.getElementById("share-container-wrapper"),
                    )}

                  <div className={`table-container ${this.state.isTablurForm ? "show" : "hide"}`}>
                    <ErrorBoundary>
                      <RenderGraphAndTable
                        showType={this.state.showType}
                        toggleShowType={this.toggleShowType}
                        data={renderData}
                        type="table"
                        combineData={showCombinedData}
                        source={electionData.source}
                        countText={electionData.counttext}
                        sequence={
                          configData.configData && configData.configData[0] && configData.configData[0].sequence
                            ? configData.configData[0].sequence
                            : undefined
                        }
                      />
                    </ErrorBoundary>
                  </div>
                  <Disclaimer stateName={stateName} electionData={electionData} />
                </div>
                {!isMobilePlatform() && (
                  <div className="col4">
                    <Adcard mstype="mrec1" adtype="dfp" />
                    {showCombinedData && <ElectionRHSWidget />}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="el_rest_page">
            {!showCombinedData && (
              <React.Fragment>
                <div className="star-candidates-container row" data-exclude="amp">
                  <div className="col12">
                    <StarCandidatesWidget
                      showCandidateStatus={electionData.showcandidatestatus}
                      stateName={stateName}
                      size={6}
                    />
                  </div>
                </div>
                {url && (
                  <div data-exclude="amp" className="electoralmapwidgetnw">
                    {/* {isMobilePlatform() && <SectionHeader sectionhead="INTERACTIVE DATA HUB" />} */}
                    <ElectoralMapWidget url={url} />
                  </div>
                )}
                <div className="result_inner row">
                  <ComparisonWidget
                    data={renderData}
                    config={config}
                    countText={electionData.counttext}
                    source={electionData.source}
                  />
                </div>
                <div data-exclude="amp">
                  <VideoPhotoNewsCombined
                    params={this.props.params}
                    query={this.props.query}
                    sectionMsid={siteElectionConfig.electionSectionMsidMap[this.props.params.statename] || 123456}
                  />
                </div>
              </React.Fragment>
            )}
            {pwa_meta && pwa_meta.syn !== undefined && typeof pwa_meta.syn === "string" ? (
              <ErrorBoundary>
                <div className="btf-content">{pwa_meta.syn}</div>
              </ErrorBoundary>
            ) : null}
            {isMobilePlatform() && breadcrumb && (
              <ErrorBoundary>
                <Breadcrumb items={breadcrumb.div.ul} />
              </ErrorBoundary>
            )}
          </div>
        </div>
      );
    }
    return <FakeStoryCard />;
  }
}

function mapStateToProps(state) {
  return {
    ...state.results,
    configData: state.electoralmap,
    electionData: state.app.election,
  };
}

Results.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.string,
  routeParams: PropTypes.object,
  pagetype: PropTypes.string,
  router: PropTypes.any,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
  metadata: PropTypes.object,
};

Results.fetchData = ({ dispatch, params, query }) => {
  dispatch(setPageType("elections"));
  dispatch(fetchResultsMetaIfNeeded(params, query)).then(data => {
    let pwaMeta;
    if (data) {
      if (data.payload) {
        pwaMeta = data.payload.pwa_meta;
      }
    }
    if (pwaMeta) {
      setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
  return dispatch(fetchElectionConfigData(params, query)).then(data => {
    let configData = {};
    if (data && data.payload) {
      configData = getFeedUrlObjectFromElectionConfigData(data.payload, "resulturl");
    }
    return dispatch(fetchResultsIfNeeded(params, query, configData));
  });
};

export default connect(mapStateToProps)(Results);
