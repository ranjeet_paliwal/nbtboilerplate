import {
  FETCH_EXITPOLL_REQUEST,
  FETCH_EXITPOLL_SUCCESS,
  FETCH_EXITPOLL_FAILURE,
  FETCH_EXITPOLL_META_REQUEST,
  FETCH_EXITPOLL_META_SUCCESS,
  FETCH_EXITPOLL_META_FAILURE,
  SET_AUTO_REFRESH_EXITPOLL,
  REMOVE_AUTO_REFRESH_EXITPOLL,
  REMOVE_DATA_IF_PRESENT,
} from "../../actions/exitpoll/exitpoll";

function EXITPOLL(state = {}, action) {
  switch (action.type) {
    case FETCH_EXITPOLL_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_EXITPOLL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_EXITPOLL_SUCCESS:
      state.data = action.payload;
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case SET_AUTO_REFRESH_EXITPOLL:
      state.refreshProcessId = action.payload;
      return {
        ...state,
      };

    case REMOVE_AUTO_REFRESH_EXITPOLL:
      if (state.refreshProcessId) {
        clearInterval(state.refreshProcessId);
      }
      state.refreshProcessId = "";
      return {
        ...state,
      };

    case FETCH_EXITPOLL_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_EXITPOLL_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_EXITPOLL_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case REMOVE_DATA_IF_PRESENT:
      if (state.data) {
        state.data = "";
      }
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default EXITPOLL;
