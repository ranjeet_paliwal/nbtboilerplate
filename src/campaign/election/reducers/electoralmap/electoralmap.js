import {
  FETCH_ELECTORALMAP_META_REQUEST,
  FETCH_ELECTORALMAP_META_SUCCESS,
  FETCH_ELECTORALMAP_META_FAILURE,
  FETCH_ELECTION_CONFIG_FAILURE,
  FETCH_ELECTION_CONFIG_SUCCESS,
} from "../../actions/electoralmap/electoralmap";

function ELECTORALMAP(state = { configData: "" }, action) {
  switch (action.type) {
    case FETCH_ELECTORALMAP_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_ELECTORALMAP_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_ELECTORALMAP_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_ELECTION_CONFIG_SUCCESS:
      state.configData = action.payload;
      return {
        ...state,
      };

    case FETCH_ELECTION_CONFIG_FAILURE:
      state.configError = true;
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default ELECTORALMAP;
