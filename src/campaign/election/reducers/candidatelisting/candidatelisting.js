import {
  FETCH_CANDIDATELIST_REQUEST,
  FETCH_CANDIDATELIST_SUCCESS,
  FETCH_CANDIDATELIST_FAILURE,
  FETCH_CANDIDATELIST_META_REQUEST,
  FETCH_CANDIDATELIST_META_SUCCESS,
  FETCH_CANDIDATELIST_META_FAILURE,
  REMOVE_DATA_IF_PRESENT,
  SET_AUTO_RERESH_CANDLIST,
  REMOVE_AUTO_REFRESH_CANDLIST,
} from "../../actions/candidatelisting/candidatelisting";

import { modifyCandidatesData } from "../../utils/util";

function CANDIDATELIST(state = {}, action) {
  switch (action.type) {
    case FETCH_CANDIDATELIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_CANDIDATELIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case SET_AUTO_RERESH_CANDLIST:
      state.refreshProcessId = action.payload;
      return {
        ...state,
      };

    case REMOVE_AUTO_REFRESH_CANDLIST:
      if (state.refreshProcessId) {
        clearInterval(state.refreshProcessId);
      }
      state.refreshProcessId = "";
      return {
        ...state,
      };

    case FETCH_CANDIDATELIST_SUCCESS: {
      const data = action.payload && action.payload.cnt_rslt ? modifyCandidatesData(action.payload.cnt_rslt) : [];
      // const data = [];
      state.data = data;
      state.filterData = data;
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    }

    case FETCH_CANDIDATELIST_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_CANDIDATELIST_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_CANDIDATELIST_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case REMOVE_DATA_IF_PRESENT:
      if (state.data) {
        state.data = "";
      }
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default CANDIDATELIST;
