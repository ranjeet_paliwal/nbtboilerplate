import {
  FETCH_RESULTS_REQUEST,
  FETCH_RESULTS_SUCCESS,
  FETCH_RESULTS_FAILURE,
  FETCH_RESULTS_META_REQUEST,
  FETCH_RESULTS_META_SUCCESS,
  FETCH_RESULTS_META_FAILURE,
  SET_AUTO_RERESH,
  REMOVE_AUTO_REFRESH,
  REMOVE_DATA_IF_PRESENT,
} from "../../actions/results/results";

function RESULTS(state = {}, action) {
  switch (action.type) {
    case FETCH_RESULTS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_RESULTS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_RESULTS_SUCCESS:
      state.data = action.payload;
      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_RESULTS_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_RESULTS_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false,
      };

    case FETCH_RESULTS_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case SET_AUTO_RERESH:
      state.refreshProcessId = action.payload;
      return {
        ...state,
      };

    case REMOVE_AUTO_REFRESH:
      if (state.refreshProcessId) {
        clearInterval(state.refreshProcessId);
      }
      state.refreshProcessId = "";
      return {
        ...state,
      };

    case REMOVE_DATA_IF_PRESENT:
      if (state.data) {
        state.data = "";
      }
      return {
        ...state,
      };

    default:
      return state;
  }
}

export default RESULTS;
