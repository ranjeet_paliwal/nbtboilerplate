/* eslint-disable no-unused-expressions */
import { constants } from "./constants";
import { mapPathsStateWise } from "./config";
import { isMobilePlatform } from "../../../utils/util";

export const coustomizeFeedData = (data, type, _yrdata, sourceType, isNational) => {
  const custom_data = {};
  if (data) {
    // check for state data
    if (_yrdata && !isNational) {
      // check for source in exit poll
      if (sourceType) {
        _yrdata.source &&
          _yrdata.source.length > 0 &&
          _yrdata.source.map(_yrdata => {
            if (_yrdata.nm == sourceType) {
              custom_data.items = type == constants.PARTYWISE ? _yrdata.starpr_rslt : _yrdata.al_rslt;
              custom_data.items_index = type == constants.PARTYWISE ? _yrdata.starpr_rslt_index : _yrdata.al_rslt_index;
            }
          });
      } else {
        custom_data.items = type == constants.PARTYWISE ? _yrdata.starpr_rslt : _yrdata.al_rslt;
        custom_data.items_index = type == constants.PARTYWISE ? _yrdata.starpr_rslt_index : _yrdata.al_rslt_index;
      }
    } else if (sourceType) {
      _yrdata.source &&
        _yrdata.source.length > 0 &&
        _yrdata.source.map(_yrdata => {
          if (_yrdata.nm == sourceType) {
            custom_data.items = type == constants.PARTYWISE ? _yrdata.starpr_rslt : _yrdata.al_rslt;
            custom_data.items_index = type == constants.PARTYWISE ? _yrdata.starpr_rslt_index : _yrdata.al_rslt_index;
          }
        });
    } else {
      custom_data.items = type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt : data.yrdata[0].al_rslt;
      custom_data.items_index =
        type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt_index : data.yrdata[0].al_rslt_index;
    }

    custom_data.master = type == constants.PARTYWISE ? data.pr_master : data.al_master;
    custom_data.master_index = type == constants.PARTYWISE ? data.pr_master_index : data.al_master_index;
  }

  return custom_data;
};

const statusList = {
  // 0: "leading",
  1: "leading",
  2: "trailing",
  3: "won",
  4: "lost",
};

export const candidateListMaker = statesList => {
  const candidatesList = [];
  statesList.forEach(state => {
    if (state.cns) {
      state.cns.forEach(constituency => {
        if (constituency.cnt) {
          constituency.cnt.forEach(candidate => {
            candidate.cnsnm = constituency.nm;
            candidate.statenm = state.nm;
            candidate.statusClass = statusList[candidate.status];
            candidatesList.push(candidate);
          });
        }
      });
    }
  });
  return candidatesList;
};

export const modifyStateName = ({ stateName, search, replaceWith }) => {
  let modifiedStateName = stateName || "";
  const matchPattern = search || "_";
  const replacePattern = replaceWith || "-";

  if (modifiedStateName.indexOf(matchPattern) > -1) {
    modifiedStateName = modifiedStateName.split(matchPattern);
    for (let i = 0; i < modifiedStateName.length; i++) {
      modifiedStateName[i] = modifiedStateName[i].charAt(0).toUpperCase() + modifiedStateName[i].slice(1);
    }
    modifiedStateName = modifiedStateName.join(replacePattern);
  } else {
    modifiedStateName = modifiedStateName.charAt(0).toUpperCase() + modifiedStateName.slice(1);
  }
  return modifiedStateName;
};

export const modifyCandidatesData = candidatesData => {
  candidatesData.forEach(candidate => {
    candidate.statusClass = statusList[candidate.cs];
  });
  return candidatesData;
};

export const getPartyData = candidatesData => {
  const partyList = ["Party"];
  const constituencyList = ["Constituency"];
  const statList = ["Status"];
  candidatesData &&
    candidatesData.forEach(candidate => {
      if (partyList.indexOf(candidate.pn) == -1) {
        partyList.push(candidate.pn);
      }
      if (constituencyList.indexOf(candidate.cns_name) == -1) {
        constituencyList.push(candidate.cns_name);
      }
      if (candidate.cs && candidate.cs > 0 && statList.indexOf(statusList[candidate.cs]) == -1) {
        statList.push(statusList[candidate.cs]);
      }
    });

  return {
    partyList,
    constituencyList,
    statList,
  };
};

export const filterCandidateData = candidatesData =>
  modifyCandidatesData(candidatesData).filter(candidate => candidate.star);

export const countSeats = (partywiseData, isPrevYear) => {
  const seats = { seatCounted: 0 };
  partywiseData.forEach(datum => {
    // for prev year data we are using pws
    seats.seatCounted += isPrevYear ? datum.pws : datum.ws + datum.ls;
  });
  seats.mMark = partywiseData && partywiseData[0] ? partywiseData[0].ws : 0;
  return seats;
};

export const extractPrevDataInCurrentFormat = currentYearData => {
  const prevYearData = [];
  currentYearData.forEach(partywiseData => {
    const prevYearPartyWiseData = { ...partywiseData };
    prevYearPartyWiseData.ws = partywiseData.pws || 0;
    prevYearPartyWiseData.ls = 0;
    prevYearData.push(prevYearPartyWiseData);
  });
  return prevYearData;
};

export const getIframeUrl = stateName => {
  let url = mapPathsStateWise[stateName] || mapPathsStateWise.defaultlink;
  if (isMobilePlatform()) {
    url += "?frmapp=yes&isiframe=yes";
  }
  return url;
};

export const getFeedUrlObjectFromElectionConfigData = (data, type) => {
  let returnData = {};
  if (!data) {
    // return the default link from config for the type
  }
  if (data && data.length === 1) {
    returnData = { ...data[0] };
    returnData.state = modifyStateName({ stateName: data[0].state, search: "-", replaceWith: "_" });
    returnData.url = data[0][type];
    // returnData.url = "https://toibnews.timesofindia.indiatimes.com/electionfeed/brnov2020/br_exit_2020.htm";
    // returnData.url = "https://toibnews.timesofindia.indiatimes.com/electionfeed/dlfeb2020/dl_al_pr_2020_mob.htm";
  }
  return returnData;
};

export const generateStateExitPollLinks = (statename, msid) =>
  `/elections/assembly-elections/${statename}/exitpolls/${msid}.cms`;

export const generateStateResultslLinks = (statename, msid) =>
  `/elections/assembly-elections/${statename}/results/${msid}.cms`;

export const generateElectorallLinks = (statename, msid) =>
  `/elections/assembly-elections/${statename}/electoralmap/${msid}.cms`;

export const generateCandidateslLinks = (statename, msid) =>
  `/elections/assembly-elections/${statename}/candidates/${msid}.cms`;

export const generateElectionLinks = ({ statename, msid, type }) =>
  `/elections/assembly-elections/${statename}/${type}/${msid}.cms`;

export const generateReadMoreLinks = ({ msid, type }) => `/elections/assembly-elections/${type}/${msid}.cms`;

export const getDataLength = stateData => {
  let count = 0;
  stateData.forEach(datum => {
    if (parseInt(datum[0].ws, 10) + parseInt(datum[0].ls, 10) > 0) {
      count += 1;
    }
  });
  return count;
};

export const checkStateInData = (data, stateName) => {
  const stateArray = Object.keys(data);
  return stateArray.indexOf(stateName) > -1;
};

export const checkWidgetToRender = data => {
  let sum = 0;
  data.forEach(widgetData => {
    if (widgetData && widgetData[0]) {
      sum += parseInt(widgetData[0].ws, 10) + parseInt(widgetData[0].ls, 10);
    }
  });
  return sum !== 0;
};
