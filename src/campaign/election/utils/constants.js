export const constants = {
  PARTYWISE: "partywise",
  ALLIANCEWISE: "alliancewise",
  MAXITEMS: 50,
  MAXITEMS_LIMITER: 45,
  POWER_STATES: "UP-MH-WB-BR-TN-",
  LEADS_WINS: "LEADS + WINS",
  ELECTION_YEAR: "2019",
};

export const electionUrls = {
  exitpollslisturl: "",
  exitpollstateurl: "",
};
