export const electionConfig = {
  common: {
    disclaimer: `<ul><li>Election was cancelled in Vellore by EC</li> <li>MGB (Mahagathbandhan) consists of SP, BSP, and RLD</li></ul>`,
  },
  nbt: {
    exitpoll_liveblog_link: "/india/lok-sabha-chunav-2019-exit-polls-live-updates/liveblog/69396951.cms",
    result_liveblog_link:
      "https://navbharattimes.indiatimes.com/elections/lok-sabha-elections/news/lok-sabha-chunav-parinam-2019-live-news-and-updates/articleshow/69453254.cms",
    loksabhaExitpollMSID: "69253509",
    loksabhaResultsMSID: "69253542",
    assemblyResultsMSID: "66895640",
    mapExitpollMSID: { maharashtra: "71452301", haryana: "71452442" },
    mapResultMSID: { maharashtra: "71452334", haryana: "71452456" },
    mapElectoralMSID: { maharashtra: "71452345", haryana: "71452529" },

    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68077184",
    loksabhaResultInfocusMSID: "69266205",
    feedlanguage: "hindi",
    loksabhaExitpollHeading: "लोकसभा चुनाव एग्जिट पोल 2019",
    totalSeats: "कुुल सीटें ",
    majorityMark: "बहुमत ",

    loksabhaResultsHeading: "लोकसभा चुनाव परिणाम",
    allianceViewTxt: "गठबंधन",
    partyViewTxt: "पार्टी",
    liveUpdatesTxt: "लाइव अपडेट्स",
    viewDetailsTxt: "डीटेल्स देखें",
    loksabhaDisclaimer: "543 में से 542 सीटों के परिणाम (*वेल्लोर सीट पर चुनाव रद्द हुए हैं)",
    stateWise: "राज्यवार परिणाम",
    partyWise: "पार्टीवार",
    liveBlogTxt: "लाइव ब्लॉग",
    viewUpdatesTxt: "अपडेट्स देखें",
    starCandidateTxt: "प्रमुख उम्मीदवार",
    candidateTxt: "उम्मीदवार",
    inFocus: "चर्चित",
    assemblyResultHeading: "विधानसभा चुनाव परिणाम <i>2019</i>",
    assemblyDisclaimer: "",
    parties: "",
    highlights: "हाइलाइट्स",
    tap_to_see_assembly_results: "विधानसभा चुनाव परिणामों के लिए टैप करें",
    view_all_updates: "View All Updates",
    powerStates: "महत्वपूर्ण राज्य",
    allConstituencies: "संसदीय क्षेत्र",
    viewMap: "मैप पर देखें",
    electionSectionMsid: 74439866,
    electionSectionMsidMap: {
      bihar: 74439866,
      "west-bengal": 80127453,
      assam: 81326297,
      "tamil-nadu": 81326372,
      puducherry: 81326339,
      kerala: 81326357,
    },
    electionRhsNewsMsid: 63828428,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78404532",
      haryana: "71452442",
      delhi: "73155714",
      jharkhand: "72760331",
      "west-bengal": "81430996",
      assam: "81430929",
      "tamil-nadu": "81430380",
      puducherry: "81430637",
      kerala: "81430799",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78404547",
      haryana: "71452456",
      delhi: "73155671",
      jharkhand: "72760401",
      "west-bengal": "81431024",
      assam: "81430946",
      "tamil-nadu": "81430402",
      puducherry: "81430667",
      kerala: "81430877",
    },
    hpExitpollWidgetHeading: "बिहार एग्जिट पोल २०२०",
    hpResultWidgetHeading: "बिहार विधानसभा चुनाव नतीजे २०२०",
    candidateMsidStateMap: {
      maharashtra: "71452362",
      bihar: "78404601",
      haryana: "71452488",
      delhi: "73155429",
      jharkhand: "72829675",
      "west-bengal": "81431024",
      assam: "81430967",
      "tamil-nadu": "81430574",
      puducherry: "81430717",
      kerala: "81430893",
    },
    electralMsidStateMap: {
      maharashtra: "71452345",
      bihar: "78404577",
      haryana: "71452529",
      delhi: "73155465",
      jharkhand: "72760614",
      "west-bengal": "81431033",
      assam: "81430959",
      "tamil-nadu": "81430551",
      puducherry: "81430684",
      kerala: "81430829",
    },
    mapRegText: "चुनावी नक्शा",
    candidateRegText: "उम्मीदवार",
    assemblyElectionRegText: "विधानसभा चुनाव",
    comparisonRegText: "कंपैरिज़न",
    exitPollRegText: "एग्जिट पोल",
    resultsRegText: "नतीजे",
    stateRegText: {
      maharashtra: "महाराष्ट्र",
      bihar: "बिहार",
      haryana: "हरियाणा",
      delhi: "दिल्ली",
      jharkhand: "झारखंड",
      "west-bengal": "पश्चिम बंगाल",
      assam: "असम",
      "tamil-nadu": "तमिलनाडु",
      puducherry: "पुडुचेरी",
      kerala: "केरल",
    },
    combinedExitPollMsid: 66895631,
    combinedResultsMsid: 66895640,
  },
  mt: {
    exitpoll_liveblog_link: "/india-news/lok-sabha-elections-2019-live-updates-of-exit-polls/articleshow/69398376.cms",
    result_liveblog_link:
      "https://maharashtratimes.com/india-news/lok-sabha-nivadnuk-2019-result-live-updates/articleshow/69449291.cms ",
    loksabhaExitpollMSID: "69262433",
    loksabhaResultsMSID: "69262468",
    assemblyResultsMSID: "66916326",
    mapExitpollMSID: { maharashtra: "71459917", haryana: "71460037" },
    mapResultMSID: { maharashtra: "71459927", haryana: "71460032" },
    mapElectoralMSID: { maharashtra: "71459945", haryana: "71460023" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68080659",
    loksabhaResultInfocusMSID: "69340671",
    feedlanguage: "marathi",
    loksabhaExitpollHeading: "लोकसभा निवडणुकांचे एक्झिट पोल्स २०१९",
    totalSeats: "एकूण ",
    majorityMark: "बहुमताचा आकडा ",

    loksabhaResultsHeading: "लोकसभा निकाल",
    allianceViewTxt: "आघाडी",
    partyViewTxt: " पक्ष",
    liveUpdatesTxt: "क्षणोक्षणीचे अपडेट्स",
    viewDetailsTxt: " पाहा सविस्तर",
    loksabhaDisclaimer: `<ul><li>Election was cancelled in Vellore by EC</li> <li>MGB (Mahagathbandhan) consists of SP, BSP, and RLD</li></ul>`,
    stateWise: "राज्यनिहाय",
    partyWise: "पक्षनिहाय",
    liveBlogTxt: "लाइव्ह ब्लॉग",
    viewUpdatesTxt: "अपडेट्स",
    starCandidateTxt: "प्रमुख उमेदवार",
    candidateTxt: "उमेदवार",
    inFocus: "चर्चेतील",
    assemblyResultHeading: "विधानसभा निकाल <i>2019</i>",
    assemblyDisclaimer: " ",
    parties: "",
    highlights: "हायलाइट्स",
    tap_to_see_assembly_results: "निवडणुकांचे निकाल पाहण्यासाठी इथे क्लिक करा ",
    view_all_updates: "",
    powerStates: "महत्त्वाचे राज्य ",
    allConstituencies: "मतदारसंघ",
    viewMap: "नकाशावर पहा",
    electionSectionMsid: 78361043,
    electionSectionMsidMap: {
      bihar: 78361043,
      "west-bengal": 80681007,
      assam: 81387288,
      "tamil-nadu": 81387314,
      puducherry: 81387329,
      kerala: 81387380,
    },
    electionRhsNewsMsid: 64159144,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78361138",
      haryana: "71460037",
      delhi: "73153890",
      jharkhand: "72741749",
      "west-bengal": "80681054",
      assam: "81431496",
      "tamil-nadu": "81431468",
      puducherry: "81431454",
      kerala: "81431380",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78361122",
      haryana: "71460032",
      delhi: "73153860",
      jharkhand: "72741989",
      "west-bengal": "80681048",
      assam: "81431505",
      "tamil-nadu": "81431479",
      puducherry: "81431462",
      kerala: "81431386",
    },
    hpExitpollWidgetHeading: "बिहार एक्झिट पोल 2020",
    hpResultWidgetHeading: "बिहार विधानसभा निवडणूक 2020",
    candidateMsidStateMap: {
      bihar: "78361071",
      haryana: "71460008",
      delhi: "73153794",
      jharkhand: "72841304",
      "west-bengal": "80681025",
      kerala: "81431416",
      assam: 82164303,
      puducherry: 82164105,
      "tamil-nadu": 82164178,
    },
    electralMsidStateMap: {
      bihar: "78361096",
      haryana: "71460023",
      delhi: "73153827",
      jharkhand: "72741874",
      "west-bengal": "80681036",
      kerala: "81431403",
      assam: 82164335,
      puducherry: 82164124,
      "tamil-nadu": 82164218,
    },
    mapRegText: "मतदारसंघाचा नकाशा",
    candidateRegText: "उमेदवार",
    assemblyElectionRegText: "विधानसभा निवडणूक",
    comparisonRegText: "तुलना",
    exitPollRegText: "एक्झिट पोल",
    resultsRegText: "निकाल",
    stateRegText: {
      maharashtra: "महाराष्ट्र",
      bihar: "बिहार",
      haryana: "हरयाणा",
      delhi: "दिल्ली",
      jharkhand: "झारखंड",
      "west-bengal": "पश्चिम बंगाल",
      assam: "आसाम",
      "tamil-nadu": "तमिळनाडू",
      puducherry: "पुद्दुचेरी",
      kerala: "केरळ",
    },
    combinedExitPollMsid: 66916300,
    combinedResultsMsid: 66916326,
  },
  eisamay: {
    exitpoll_liveblog_link:
      "/elections/lok-sabha-elections/news/lok-sabha-nirbachan-2019-exit-polls-live-updates/articleshow/69398775.cms",
    result_liveblog_link:
      "https://eisamay.indiatimes.com/elections/lok-sabha-elections/news/lok-sabha-chunav-parinam-live-2019-general-election-results-2019-in-bengali/articleshow/69453024.cms",
    loksabhaExitpollMSID: "69253132",
    loksabhaResultsMSID: "69253166",
    assemblyResultsMSID: "66893147",
    mapExitpollMSID: { maharashtra: "71452643", haryana: "71452801" },
    mapResultMSID: { maharashtra: "71452631", haryana: "71452798" },
    mapElectoralMSID: { maharashtra: "71452621", haryana: "71452795" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68078469",
    loksabhaResultInfocusMSID: "69340658",
    feedlanguage: "bengali",
    loksabhaExitpollHeading: "লোকসভার এক্সিট পোল 2019",
    totalSeats: "মোট আসন ",
    majorityMark: "সংখ্যাগরিষ্ঠতা ",

    loksabhaResultsHeading: "লোকসভার ফলাফল",
    allianceViewTxt: "জোটের ফলাফল",
    partyViewTxt: "দলের ফলাফল",
    liveUpdatesTxt: "লাইভ আপডেট",
    viewDetailsTxt: "আরও তথ্য",
    loksabhaDisclaimer: `<ul><li>Election was cancelled in Vellore by EC</li> <li>MGB (Mahagathbandhan) consists of SP, BSP, and RLD</li></ul>`,
    stateWise: "রাজ্য-ভিত্তিক",
    partyWise: "দল-ভিত্তিক",
    liveBlogTxt: "লাইভ ব্লগ",
    viewUpdatesTxt: "আরও আপডেট",
    starCandidateTxt: "হেভিওয়েট প্রার্থী ",
    candidateTxt: "প্রার্থী",
    inFocus: "ফোকাস",
    assemblyResultHeading: "বিধানসভার ফলাফল <i>2019</i>",
    assemblyDisclaimer: "",
    parties: "দল",
    highlights: "হাইলাইটস",
    tap_to_see_assembly_results: "বিধানসভার ফলাফল দেখতে ট্যাপ",
    view_all_updates: "",
    powerStates: "গুরুত্বপূর্ণ রাজ্য",
    allConstituencies: "আসন",
    viewMap: "ম্যাপ দেখুন",
    electionSectionMsid: 78312858,
    electionSectionMsidMap: {
      bihar: 78312858,
      "west-bengal": 80667926,
      assam: 81388186,
      "tamil-nadu": 81389122,
      puducherry: 81389298,
      kerala: 81388422,
    },
    electionRhsNewsMsid: 64158416,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78312973",
      haryana: "71452801",
      delhi: "73119797",
      jharkhand: "72738726",
      "west-bengal": "81448706",
      assam: "81448330",
      "tamil-nadu": "81449160",
      puducherry: "81449281",
      kerala: "81449081",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78313028",
      haryana: "71452798",
      delhi: "73153646",
      jharkhand: "72738816",
      "west-bengal": "81448706",
      assam: "81448299",
      "tamil-nadu": "81449168",
      puducherry: "81449296",
      kerala: "81449097",
    },
    hpExitpollWidgetHeading: "বিহার এক্সিট পোল 2020",
    hpResultWidgetHeading: "বিহার বিধানসভা রেজাল্ট 2020",
    candidateMsidStateMap: {
      bihar: "78313047",
      haryana: "71452792",
      delhi: "73153668",
      jharkhand: "72841874",
      "west-bengal": "81387967",
      assam: "81448682",
      "tamil-nadu": "81449214",
      puducherry: "81449326",
      kerala: "81449123",
    },
    electralMsidStateMap: {
      bihar: "78313039",
      haryana: "71452795",
      delhi: "73153655",
      jharkhand: "72738866",
      "west-bengal": "81388028",
      assam: "81448394",
      "tamil-nadu": "81449190",
      puducherry: "81449320",
      kerala: "81449113",
    },
    mapRegText: "নির্বাচনী নকশা",
    candidateRegText: "প্রার্থী",
    assemblyElectionRegText: "বিধানসভা নির্বাচন",
    comparisonRegText: "তুলনা",
    exitPollRegText: "এক্সিট পোল",
    resultsRegText: "ফলাফল",
    stateRegText: {
      maharashtra: "महाराष्ट्र",
      bihar: "বিহার",
      haryana: "হরিয়ানা",
      delhi: "দিল্লি",
      jharkhand: "ঝাড়খণ্ড",
      "west-bengal": "পশ্চিমবঙ্গ",
      assam: "অসম",
      "tamil-nadu": "তুামিলনাডু",
      puducherry: "পুদুচেরি",
      kerala: "কেরালা",
    },
    combinedExitPollMsid: 66893100,
    combinedResultsMsid: 66893147,
  },
  vk: {
    exitpoll_liveblog_link:
      "/elections/lok-sabha-elections/news/lok-sabha-2019-chunavana-india-exit-poll-results-live-news-future-of-nda-upa-and-mahagathbandhan/articleshow/69399105.cms",
    result_liveblog_link:
      "https://vijaykarnataka.com/elections/lok-sabha-elections/news/live-updates-of-2019-general-election-india-vote-counting-and-results/articleshow/69452990.cms",
    loksabhaExitpollMSID: "69265428",
    loksabhaResultsMSID: "69265495",
    assemblyResultsMSID: "66894391",
    mapExitpollMSID: { maharashtra: "71454170", haryana: "71454325" },
    mapResultMSID: { maharashtra: "71454153", haryana: "71454300" },
    mapElectoralMSID: { maharashtra: "71454120", haryana: "71454294" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68080548",
    loksabhaResultInfocusMSID: "69266338",
    feedlanguage: "kannada",
    loksabhaExitpollHeading: "ಲೋಕಸಭಾ ಚುನಾವಣೆ ಎಕ್ಸಿಟ್‌ ಪೋಲ್‌ 2019",
    totalSeats: "ಒಟ್ಟಾರೆ ಸ್ಥಾನಗಳು ",
    majorityMark: "ಬಹುಮತ ಸಂಖ್ಯೆ ",

    loksabhaResultsHeading: "ಲೋಕಸಭೆ ಚುನಾವಣೆ ಫಲಿತಾಂಶ",
    allianceViewTxt: "ಮೈತ್ರಿವಾರು",
    partyViewTxt: "ಪಕ್ಷವಾರು",
    liveUpdatesTxt: "ಲೈವ್‌ ಅಪ್‌ಡೇಟ್‌",
    viewDetailsTxt: "ವಿವರಗಳು",
    loksabhaDisclaimer: "ಲೋಕಸಭಾ ಡಿಸ್‌ಕ್ಲೈಮರ್‌",
    stateWise: "ರಾಜ್ಯವಾರು",
    partyWise: "ಪಕ್ಷವಾರು",
    liveBlogTxt: "ಲೈವ್‌ಬ್ಲಾಗ್‌",
    viewUpdatesTxt: "ತಾಜಾ ಮಾಹಿತಿ",
    starCandidateTxt: "ಪ್ರಮುಖ ಅಭ್ಯರ್ಥಿಗಳು",
    candidateTxt: "ಅಭ್ಯರ್ಥಿಗಳು",
    inFocus: "ಇನ್‌ಫೋಕಸ್‌",
    assemblyResultHeading: "ಅಸೆಂಬ್ಲಿ ಫಲಿತಾಂಶ 2019",
    assemblyDisclaimer: "ಅಸೆಂಬ್ಲಿ ಡಿಸ್‌ಕ್ಲೈಮರ್‌",
    parties: "ಪಕ್ಷಗಳು",
    highlights: "ಹೈಲೈಟ್ಸ್‌",
    tap_to_see_assembly_results: "ಅಸೆಂಬ್ಲಿ ಚುನಾವಣೆ ಫಲಿತಾಂಶಕ್ಕೆ ಇಲ್ಲಿ ಕ್ಲಿಕ್‌ ಮಾಡಿ",
    view_all_updates: "",
    powerStates: "ಪವರ್‌ ಸ್ಟೇಟ್ಸ್‌",
    allConstituencies: "ಕ್ಷೇತ್ರಗಳು",
    viewMap: "ಮ್ಯಾಪ್‌ನಲ್ಲಿ ನೋಡಿ",
    electionSectionMsid: 78402695,
    electionSectionMsidMap: {
      bihar: 78402695,
      "west-bengal": 80668432,
      assam: 81437250,
      "tamil-nadu": 80668813,
      puducherry: 81437392,
      kerala: 80668829,
    },
    electionRhsNewsMsid: 63427277,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78402787",
      haryana: "71454325",
      delhi: "73172203",
      jharkhand: "72745161",
      "west-bengal": "81437123",
      assam: "81437325",
      "tamil-nadu": "81436983",
      puducherry: "81437428",
      kerala: "81436714",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78402780",
      haryana: "71454300",
      delhi: "73172189",
      jharkhand: "72745266",
      "west-bengal": "81437117",
      assam: "81437317",
      "tamil-nadu": "81436938",
      puducherry: "81437422",
      kerala: "81436679",
    },
    hpExitpollWidgetHeading: "ಬಿಹಾರ ಎಕ್ಸಿಟ್‌ ಪೋಲ್‌ 2020",
    hpResultWidgetHeading: "ಬಿಹಾರ ವಿಧಾನಸಭೆ ಚುನಾವಣಾ ಫಲಿತಾಂಶ 2020",
    candidateMsidStateMap: {
      bihar: "78402743",
      haryana: "71454270",
      delhi: "73578372",
      jharkhand: "72830212",
      "west-bengal": "81437092",
      assam: "81437283",
      "tamil-nadu": "81436956",
      puducherry: "81437407",
      kerala: "81436649",
    },
    electralMsidStateMap: {
      bihar: "78402756",
      haryana: "71454294",
      delhi: "73172142",
      jharkhand: "72745236",
      "west-bengal": "81437111",
      assam: "81437305",
      "tamil-nadu": "81436932",
      puducherry: "81437420",
      kerala: "81436621",
    },
    mapRegText: "ಕ್ಷೇತ್ರವಾರು ನಕ್ಷೆ",
    candidateRegText: "ಅಭ್ಯರ್ಥಿಗಳು",
    assemblyElectionRegText: "ಅಸೆಂಬ್ಲಿ ಚುನಾವಣೆ",
    comparisonRegText: "ಹೋಲಿಕೆ",
    exitPollRegText: "ಎಕ್ಸಿಟ್‌ ಪೋಲ್‌",
    resultsRegText: "ಫಲಿತಾಂಶ",
    stateRegText: {
      maharashtra: "महाराष्ट्र",
      bihar: "बिहार",
      haryana: "ಹರಿಯಾಣ",
      delhi: "ದಿಲ್ಲಿ",
      jharkhand: "ಜಾರ್ಖಂಡ್‌",
      "west-bengal": "ಪಶ್ಚಿಮ ಬಂಗಾಳ",
      assam: "ಆಸಮ್",
      "tamil-nadu": "ತಮಿಳುನಾಡು",
      puducherry: "ಪುದುಚೇರಿ",
      kerala: " ಕೇರಳ",
    },
    combinedExitPollMsid: 66894361,
    combinedResultsMsid: 66894391,
  },
  tml: {
    exitpoll_liveblog_link:
      "/elections/lok-sabha-elections/news/general-elections-2019-exit-poll-results-live-updates-who-will-win-nda-or-upa/articleshow/69399551.cms",
    result_liveblog_link:
      "https://tamil.samayam.com/elections/lok-sabha-elections/news/2019-lok-sabha-elections-india-vote-counting-and-results-live-updates-in-tamil/articleshow/69452551.cms",
    loksabhaExitpollMSID: "69252898",
    loksabhaResultsMSID: "69252886",
    assemblyResultsMSID: "66894374",
    mapExitpollMSID: { maharashtra: "71455367", haryana: "71455284" },
    mapResultMSID: { maharashtra: "71455356", haryana: "71455276" },
    mapElectoralMSID: { maharashtra: "71455350", haryana: "71455270" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68081091",
    loksabhaResultInfocusMSID: "69266301",
    feedlanguage: "tamil",
    loksabhaExitpollHeading: "மக்களவைத் தேர்தல் எக்ஸித் போல் 2019",
    totalSeats: "மொத்த இடங்கள் ",
    majorityMark: "மெஜாரிட்டி மார்க்",

    loksabhaResultsHeading: "மக்களவைத் தேர்தல் முடிவுகள்",
    allianceViewTxt: "கூட்டணி",
    partyViewTxt: "கட்சி",
    liveUpdatesTxt: "லைவ் அப்டேட்",
    viewDetailsTxt: "மேலும் விவரம்",
    loksabhaDisclaimer: "வேலூரில் தேர்தல் ரத்து - தேர்தல் ஆணையம்",
    stateWise: "மாநிலம்",
    partyWise: "கட்சிகள்",
    liveBlogTxt: "லைவ் பிளாக்",
    viewUpdatesTxt: "அப்டேட் ",
    starCandidateTxt: "முக்கிய வேட்பாளர்கள்",
    candidateTxt: "வேட்பாளர்கள்",
    inFocus: "மேலும் விபரம்",
    assemblyResultHeading: "சட்டசபை முடிவுகள் <i>2019</i>",
    assemblyDisclaimer: "",
    parties: "",
    highlights: "லைவ் பிளாக்",
    tap_to_see_assembly_results: "சட்டப்பேரவை முடிவுகளை பார்க்க",
    view_all_updates: "",
    powerStates: "பவர் ஸ்டேட்",
    allConstituencies: "தொகுதிகள்",
    viewMap: "மேப் பார்க்க",
    electionSectionMsid: 78399853,
    electionSectionMsidMap: {
      bihar: 78399853,
      "west-bengal": 80132259,
      assam: 80132243,
      "tamil-nadu": 80132310,
      puducherry: 80132278,
      kerala: 80132271,
    },
    electionRhsNewsMsid: 63812194,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78399983",
      haryana: "71455284",
      delhi: "73184748",
      jharkhand: "72821700",
      "west-bengal": "81432378",
      assam: "81432485",
      "tamil-nadu": "81431516",
      puducherry: "81431682",
      kerala: "81432295",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78399969",
      haryana: "71455276",
      delhi: "73184615",
      jharkhand: "72821663",
      "west-bengal": "81432368",
      assam: "81432474",
      "tamil-nadu": "81431533",
      puducherry: "81431660",
      kerala: "81432283",
    },
    hpExitpollWidgetHeading: "பிகார் வாக்குக் கணிப்பு 2020",
    hpResultWidgetHeading: "பிகார் சட்டமன்றத் தேர்தல் முடிவுகள்",
    candidateMsidStateMap: {
      bihar: "78399932",
      haryana: "71455265",
      delhi: "73578400",
      jharkhand: "72864217",
      "west-bengal": "81432329",
      assam: "81432430",
      "tamil-nadu": "81431561",
      puducherry: "81431619",
      kerala: "81432043",
    },
    electralMsidStateMap: {
      bihar: "78399951",
      haryana: "71455270",
      delhi: "73184591",
      jharkhand: "72821627",
      "west-bengal": "81432348",
      assam: "81432449",
      "tamil-nadu": "81431553",
      puducherry: "81431626",
      kerala: "81432072",
    },
    mapRegText: "தொகுதி வரைபடம்",
    candidateRegText: "வேட்பாளர் பட்டியல்",
    assemblyElectionRegText: "சட்டப்பேரவைத் தேர்தல்",
    comparisonRegText: "ஒப்பீடு",
    exitPollRegText: "எக்ஸிட் போல்‌",
    resultsRegText: "முடிவுகள்",
    stateRegText: {
      maharashtra: "மகாராஷ்டிரா",
      bihar: "பிஹார்",
      haryana: "ஹரியானா",
      delhi: "டெல்லி",
      jharkhand: "ஜார்கண்ட்",
      "west-bengal": "மேற்கு வங்கம்",
      assam: "அசாம்",
      "tamil-nadu": "தமிழ்நாடு",
      puducherry: "புதுச்சேரி",
      kerala: " கேரளா",
    },
    combinedExitPollMsid: 66894367,
    combinedResultsMsid: 66894374,
  },
  tlg: {
    exitpoll_liveblog_link:
      "/elections/assembly-elections/andhra-pradesh/news/andhra-pradesh-assembly-election-exit-poll-2019-results-live-updates/articleshow/69399499.cms",
    result_liveblog_link:
      "https://telugu.samayam.com/elections/lok-sabha-elections/news/andhra-pradesh-lok-sabha-election-results-2019-live-news-updates-in-telugu/articleshow/69453041.cms",
    loksabhaExitpollMSID: "69253339",
    loksabhaResultsMSID: "69253464",
    assemblyResultsMSID: "66884436",
    mapExitpollMSID: { maharashtra: "71454692", haryana: "71455005" },
    mapResultMSID: { maharashtra: "71454688", haryana: "71454982" },
    mapElectoralMSID: { maharashtra: "71454678", haryana: "71454977" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "67128739",
    loksabhaResultInfocusMSID: "69266324",
    feedlanguage: "telugu",
    loksabhaExitpollHeading: "లోక్‌సభ ఎన్నికల ఎగ్జిట్ పోల్స్ 2019",
    totalSeats: "మొత్తం సీట్లు ",
    majorityMark: "మెజార్టీ సీట్లు",

    loksabhaResultsHeading: "లోక్‌సభ ఎన్నికల ఫలితాలు",
    allianceViewTxt: "కూటమి",
    partyViewTxt: "పార్టీలవారీగా",
    liveUpdatesTxt: "లైవ్ అప్‌డేట్స్",
    viewDetailsTxt: "మరిన్ని వివరాలు",
    loksabhaDisclaimer: "వెల్లూరులో ఎన్నికలు రద్దు చేసిన ఈసీ",
    stateWise: "రాష్ట్రాలు",
    partyWise: "పార్టీ వారీగా",
    liveBlogTxt: "లైవ్ బ్లాగ్",
    viewUpdatesTxt: "మరిన్ని అప్‌డేట్స్",
    starCandidateTxt: "బరిలో ప్రముఖులు",
    candidateTxt: "అభ్యర్థులు",
    inFocus: "ఫోకస్",
    assemblyResultHeading: "అసెంబ్లీ ఫలితాలు <i>2019</i>",
    assemblyDisclaimer: "",
    parties: "పార్టీలు",
    highlights: "హైలైట్స్",
    tap_to_see_assembly_results: "అసెంబ్లీ ఎన్నికల ఫలితాలు చూడండి",
    view_all_updates: "",
    powerStates: "பவர் ஸ்டேட்",
    allConstituencies: "నియోజకవర్గాలు",
    viewMap: "మ్యాప్‌లో చూడండీ",
    electionSectionMsid: 78400544,
    electionSectionMsidMap: {
      bihar: 78400544,
      "west-bengal": 81314749,
      assam: 81314790,
      "tamil-nadu": 81314823,
      puducherry: 81314842,
      kerala: 81314867,
    },
    electionRhsNewsMsid: 63802075,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78400657",
      haryana: "71455005",
      delhi: "73166711",
      jharkhand: "72742772",
      "west-bengal": "81362248",
      assam: "81362396",
      "tamil-nadu": "81363055",
      puducherry: "81363155",
      kerala: "81363248",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78400686",
      haryana: "71454982",
      delhi: "73166762",
      jharkhand: "72742681",
      "west-bengal": "81362260",
      assam: "81362413",
      "tamil-nadu": "81363072",
      puducherry: "81363167",
      kerala: "81363260",
    },
    hpExitpollWidgetHeading: "బీహార్ ఎగ్జిట్ పోల్ 2020",
    hpResultWidgetHeading: "బీహార్ అసెంబ్లీ ఎన్నికల ఫలితాలు",
    candidateMsidStateMap: {
      bihar: "78400704",
      haryana: "71454957",
      delhi: "73166855",
      jharkhand: "72830153",
      "west-bengal": "81362316",
      assam: "81362427",
      "tamil-nadu": "81363120",
      puducherry: "81363198",
      kerala: "81363282",
    },
    electralMsidStateMap: {
      bihar: "78400697",
      haryana: "71454977",
      delhi: "73166812",
      jharkhand: "72742608",
      "west-bengal": "81362272",
      assam: "81362424",
      "tamil-nadu": "81363089",
      puducherry: "81363178",
      kerala: "81363273",
    },
    mapRegText: "నియోజకవర్గాలు",
    candidateRegText: "అభ్యర్థుల జాబితా",
    assemblyElectionRegText: "అసెంబ్లీ ఎన్నికలు",
    comparisonRegText: "పోలిక",
    exitPollRegText: "ఎగ్జిట్ పోల్స్",
    resultsRegText: "ఫలితాలు",
    stateRegText: {
      maharashtra: "మహారాష్ట్ర",
      bihar: "బిహార్",
      haryana: "హర్యానా",
      delhi: "డెల్హి",
      jharkhand: "జార్ఖండ్",
      "west-bengal": "పశ్చిమ బెంగాల్",
      assam: "అస్సాం",
      "tamil-nadu": "తమిళనాడు",
      puducherry: "పుదుచ్చేరి",
      kerala: " కేరళ",
    },
    combinedExitPollMsid: 66884417,
    combinedResultsMsid: 66884436,
  },
  mly: {
    exitpoll_liveblog_link: "",
    result_liveblog_link:
      "https://malayalam.samayam.com/elections/lok-sabha-elections/news/2019-india-general-elections-vote-counting-and-results-live-news-updates-bjp-vs-congress/articleshow/69452587.cms",
    loksabhaExitpollMSID: "69320925",
    loksabhaResultsMSID: "69320892",
    assemblyResultsMSID: "66894323",
    mapExitpollMSID: { maharashtra: "71562928", haryana: "71563010" },
    mapResultMSID: { maharashtra: "71562941", haryana: "71563018" },
    mapElectoralMSID: { maharashtra: "71562959", haryana: "71563027" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "68080135",
    loksabhaResultInfocusMSID: "69266331",
    feedlanguage: "malayalam",
    loksabhaExitpollHeading: "ലോക്സഭ തെരഞ്ഞെടുപ്പ് എക്സിറ്റ് പോള്‍ 2019",
    totalSeats: "Total",
    majorityMark: "Majority Mark",

    loksabhaResultsHeading: "ലോക്സഭ ഫലം",
    allianceViewTxt: "സഖ്യം",
    partyViewTxt: "പാര്‍ട്ടി",
    liveUpdatesTxt: "ലൈവ് അപ്ഡേറ്റ്സ്",
    viewDetailsTxt: "VIEW DETAILS",
    loksabhaDisclaimer: "  വെല്ലൂരില്‍ തെരഞ്ഞെടുപ്പ് ഇലക്ഷന്‍ കമ്മിഷന്‍ റദ്ദാക്കി",
    stateWise: "STATE-WISE",
    partyWise: "",
    liveBlogTxt: "ലൈവ്ബ്ലോഗ്‌",
    viewUpdatesTxt: "അപ്ഡേറ്റ്സ് കാണുക",
    starCandidateTxt: "പ്രമുഖ സ്ഥാനാർത്ഥികൾ",
    candidateTxt: "സ്ഥാനാർത്ഥികൾ",
    inFocus: "കൂടുതല്‍ വിവരങ്ങള്‍",
    assemblyResultHeading: "അസംബ്ലി ഫലം 2019",
    assemblyDisclaimer: "",
    parties: "പാര്‍ട്ടികള്‍",
    highlights: "",
    tap_to_see_assembly_results: "Tap to see Assembly Election Results",
    view_all_updates: "",
    powerStates: "Power States",
    allConstituencies: "Constituencies",
    viewMap: "View on Map",
    electionSectionMsid: 78313930,
    electionSectionMsidMap: {
      bihar: 78313930,
      "west-bengal": 80685139,
      assam: 80685242,
      "tamil-nadu": 80685198,
      puducherry: 80685248,
      kerala: 80685232,
    },
    electionRhsNewsMsid: 63830006,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78314158",
      haryana: "71563010",
      delhi: "73156013",
      jharkhand: "72744200",
      "west-bengal": "80763352",
      assam: "80764599",
      "tamil-nadu": "80763193",
      puducherry: "80764831",
      kerala: "80762831",
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78314067",
      haryana: "71563018",
      delhi: "73156064",
      jharkhand: "72744371",
      "west-bengal": "80763382",
      assam: "80764616",
      "tamil-nadu": "80763215",
      puducherry: "80764842",
      kerala: "80762866",
    },
    hpExitpollWidgetHeading: "ബിഹാർ എക്സിറ്റ് പോൾ 2020",
    hpResultWidgetHeading: "ബിഹാർ നിയമസഭാ തിരഞ്ഞെടുപ്പ് ഫലം 2020",
    candidateMsidStateMap: {
      bihar: "78314034",
      haryana: "71563042",
      delhi: "73156105",
      jharkhand: "72829424",
      "west-bengal": "80763456",
      assam: "80764743",
      "tamil-nadu": "80763295",
      puducherry: "80764865",
      kerala: "80762923",
    },
    electralMsidStateMap: {
      bihar: "78314051",
      haryana: "71563027",
      delhi: "73156081",
      jharkhand: "72744428",
      "west-bengal": "80763436",
      assam: "80764726",
      "tamil-nadu": "80763253",
      puducherry: "80764856",
      kerala: "80762898",
    },
    mapRegText: "നിയമസഭ ഭൂപടം",
    candidateRegText: "സ്ഥാനാർഥികൾ",
    assemblyElectionRegText: "നിയമസഭ തെരഞ്ഞെടുപ്പ്",
    comparisonRegText: "താരതമ്യം",
    exitPollRegText: "എക്സിറ്റ് പോള്‍",
    resultsRegText: "തെരഞ്ഞെടുപ്പ് ഫലം",
    stateRegText: {
      maharashtra: "മഹാരാഷ്ട്ര",
      bihar: "ബീഹാർ",
      haryana: "ഹരിയാന",
      delhi: "ദില്ലി",
      jharkhand: "ജാർഖണ്ഡ്",
      "west-bengal": "പശ്ചിമ ബംഗാള്‍",
      assam: "അസം",
      "tamil-nadu": "തമിഴ്‌നാട്",
      puducherry: "പുതുച്ചേരി",
      kerala: "കേരളം",
    },
    combinedExitPollMsid: 66894319,
    combinedResultsMsid: 66894323,
  },
  iag: {
    exitpoll_liveblog_link:
      "/elections/assembly-elections/andhra-pradesh/news/andhra-pradesh-assembly-election-exit-poll-2019-results-live-updates/articleshow/69399499.cms",
    result_liveblog_link:
      "https://telugu.samayam.com/elections/lok-sabha-elections/news/andhra-pradesh-lok-sabha-election-results-2019-live-news-updates-in-telugu/articleshow/69453041.cms",
    loksabhaExitpollMSID: "69253339",
    loksabhaResultsMSID: "69253464",
    assemblyResultsMSID: "66884436",
    mapExitpollMSID: { maharashtra: "71454692", haryana: "71455005" },
    mapResultMSID: { maharashtra: "71454688", haryana: "71454982" },
    mapElectoralMSID: { maharashtra: "71454678", haryana: "71454977" },
    loksabhaResultLiveblogURL: "",
    loksabhaResultLiveblogMSID: "",
    loksabhaResultHighlightMSID: "67128739",
    loksabhaResultInfocusMSID: "69266324",
    feedlanguage: "telugu",
    loksabhaExitpollHeading: "లోక్‌సభ ఎన్నికల ఎగ్జిట్ పోల్స్ 2019",
    totalSeats: "Total Seats",
    majorityMark: "Majority Mark",

    loksabhaResultsHeading: "లోక్‌సభ ఎన్నికల ఫలితాలు",
    allianceViewTxt: "కూటమి",
    partyViewTxt: "పార్టీలవారీగా",
    liveUpdatesTxt: "లైవ్ అప్‌డేట్స్",
    viewDetailsTxt: "మరిన్ని వివరాలు",
    loksabhaDisclaimer: "వెల్లూరులో ఎన్నికలు రద్దు చేసిన ఈసీ",
    stateWise: "రాష్ట్రాలు",
    partyWise: "పార్టీ వారీగా",
    liveBlogTxt: "లైవ్ బ్లాగ్",
    viewUpdatesTxt: "మరిన్ని అప్‌డేట్స్",
    starCandidateTxt: "મુખ્ય ઉમેદવારો",
    candidateTxt: "అభ్యర్థులు",
    inFocus: "ఫోకస్",
    assemblyResultHeading: "વિધાનસભા ચૂંટણી પરિણામ <i>2021</i>",
    assemblyDisclaimer: "",
    parties: "పార్టీలు",
    highlights: "హైలైట్స్",
    tap_to_see_assembly_results: "વિધાનસભા ચૂંટણી પરિણામ",
    view_all_updates: "",
    powerStates: "பவர் ஸ்டேட்",
    allConstituencies: "నియోజకవర్గాలు",
    viewMap: "మ్యాప్‌లో చూడండీ",
    electionSectionMsid: 78695352,
    electionSectionMsidMap: {
      bihar: 78695352,
      "west-bengal": 80681323, // not given
      assam: 81433051,
      "tamil-nadu": 81433343,
      puducherry: 81433501,
      kerala: 78695352, // not given
    },
    electionRhsNewsMsid: 78694922,
    assemblyExitPollHeading: "विधानसभा चुनाव एग्जिट पोल्स",
    assemblyMapHeading: "चुनावी नक्शा",
    assemblyResultsHeading: "चुनाव परिणाम ",
    exitpollMsidStateMap: {
      maharashtra: "71452301",
      bihar: "78695512",
      haryana: "71452442",
      delhi: "73155714",
      jharkhand: "72760331",
      "west-bengal": "80681308",
      assam: "81431496", // not given
      "tamil-nadu": "81431468", // not given
      puducherry: "81431454", // not given
      kerala: "81431380", // not given
    },
    resultMsidStateMap: {
      maharashtra: "71452334",
      bihar: "78695498",
      haryana: "71452456",
      delhi: "73155671",
      jharkhand: "72760401",
      "west-bengal": "80681297",
      assam: "81433233",
      "tamil-nadu": "81433483",
      puducherry: "81433565",
      kerala: "81433703",
    },
    hpExitpollWidgetHeading: "બિહાર એક્ઝિટ પોલ 2020",
    hpResultWidgetHeading: "બિહાર વિધાનસભા ચૂંટણી પરિણામ 2020",
    candidateMsidStateMap: {
      bihar: "78695439",
      "west-bengal": "80681254",
      assam: "81707359",
      "tamil-nadu": "81707588",
      puducherry: "81707867",
      kerala: "81707424",
    },
    electralMsidStateMap: {
      bihar: "78695478",
      "west-bengal": "80681283",
      assam: "81707246",
      "tamil-nadu": "81707820",
      puducherry: "81707902",
      kerala: "81707479",
    },
    mapRegText: "બેઠકનો નક્શો",
    candidateRegText: "ઉમેદવારો",
    assemblyElectionRegText: "વિધાનસભાની ચૂંટણી",
    comparisonRegText: "સરખામણી",
    exitPollRegText: "એક્ઝિટ પોલ્સ",
    resultsRegText: "પરિણામ",
    stateRegText: {
      maharashtra: "મહારાષ્ટ્ર",
      bihar: "બિહાર",
      haryana: "હરિયાણા",
      delhi: "દિલ્હી",
      jharkhand: "ઝારખંડ",
      "west-bengal": "પશ્ચિમ બંગાળ",
      assam: "આસામ",
      "tamil-nadu": "તમિલ નાડુ",
      puducherry: "પુડ્ડુચેરી",
      kerala: "કેરાલા",
    },
    combinedExitPollMsid: 78694981,
    combinedResultsMsid: 78695001,
  },
};

export const mapPathsStateWise = {
  jharkhand: "https://navbharattimes.indiatimes.com/electoralmap_iframe/72760614.cms",
  bihar: "https://navbharattimes.indiatimes.com/electoralmap_iframe/78404577.cms",
  defaultlink: "https://navbharattimes.indiatimes.com/electoralmap_iframe/78404577.cms",
};

export const feedUrls = {
  // const exitpollurl = `https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/mobile_elections_RJ_2013_exit-polls.json`
  // const exitpollurl = process.env.WEBSITE_URL + '/staticpage/file?filename=mobile_elections_RJ_2013_exit-polls.json';
  // const exitpollurl = 'http://jcmsapp.indiatimes.com/pollfeeds/resultdata?electionid=27&resulttype=2&content=al,pr&callback=times.mobile.election.electionresults';
  // const exitpollurl = process.env.WEBSITE_URL + '/staticpage/file?filename=mobile_exitpolls_assemblyElections_2019.json'
  // Below URL is for Assembly 2019 Andhra, Arunachal, etc
  // const exitpollurl = 'https://toibnews.timesofindia.indiatimes.com/Election/ae/common/exitpoll.htm';
  // exitpolls: "https://toibnews.timesofindia.indiatimes.com/Election/election2019_sp_common_exitpoll.htm",
  exitpolls:
    "https://s3-ap-southeast-1.amazonaws.com/toibnews.timesofindia.indiatimes.com/bhihar-2020-exitpollfeed-mobile.json",
  // results:
  //   "https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/nov2020/web_elections_BR_wc_2015_result.json",
  bihar: {
    candidates: "https://toibnews.timesofindia.indiatimes.com/electionfeed/dlfeb2020/dl_cnt_2020_cand_V1.htm",
    starCandidates: "https://toibnews.timesofindia.indiatimes.com/electionfeed/dlfeb2020/dl_cnt_2020_cand_V1.htm",
  },
  // results: "https://toibnews.timesofindia.indiatimes.com/electionfeed/dlfeb2020/dl_al_cns_star_2020_rs_V1.htm",
  // results: "https://toibnews.timesofindia.indiatimes.com/Election/sp_common_2019_results.htm",
  // results:
  //   "https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/nov2020/web_elections_BR_wc_2015_result.json",
  results: "https://toibnews.timesofindia.indiatimes.com/Election/jharkhandresult.htm",
  candidates: "https://toibnews.timesofindia.indiatimes.com/electionfeed/brnov2020/br_pr_cnt_2020_mob.htm", // fallback link
  starCandidates: "https://toibnews.timesofindia.indiatimes.com/electionfeed/brnov2020/br_star_2020_cand.htm", // fallback link
};
