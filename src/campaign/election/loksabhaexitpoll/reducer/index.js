import {
  FETCH_LOKSABHAEXITPOLL_REQUEST,
  FETCH_LOKSABHAEXITPOLL_SUCCESS,
  FETCH_LOKSABHAEXITPOLL_FAILURE,
  FETCH_LOKSABHAEXITPOLL_META_REQUEST,
  FETCH_LOKSABHAEXITPOLL_META_SUCCESS,
  FETCH_LOKSABHAEXITPOLL_META_FAILURE
} from "./../action/index";

function LOKSABHAEXITPOLL(state = {}, action) {
  switch (action.type) {
    case FETCH_LOKSABHAEXITPOLL_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_LOKSABHAEXITPOLL_SUCCESS:
      state.data = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_LOKSABHAEXITPOLL_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_LOKSABHAEXITPOLL_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_LOKSABHAEXITPOLL_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_LOKSABHAEXITPOLL_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default LOKSABHAEXITPOLL;
