import fetch from "utils/fetch/fetch";
export const FETCH_LOKSABHAEXITPOLL_REQUEST = "FETCH_LOKSABHAEXITPOLL_REQUEST";
export const FETCH_LOKSABHAEXITPOLL_SUCCESS = "FETCH_LOKSABHAEXITPOLL_SUCCESS";
export const FETCH_LOKSABHAEXITPOLL_FAILURE = "FETCH_LOKSABHAEXITPOLL_FAILURE";
export const FETCH_LOKSABHAEXITPOLL_META_REQUEST =
  "FETCH_LOKSABHAEXITPOLL_META_REQUEST";
export const FETCH_LOKSABHAEXITPOLL_META_SUCCESS =
  "FETCH_LOKSABHAEXITPOLL_META_SUCCESS";
export const FETCH_LOKSABHAEXITPOLL_META_FAILURE =
  "FETCH_LOKSABHAEXITPOLL_META_FAILURE";

function fetchLOKSABHAEXITPOLL_DataFailure(error) {
  return {
    type: FETCH_LOKSABHAEXITPOLL_FAILURE,
    payload: error.message
  };
}

function fetchLOKSABHAEXITPOLL_DataSuccess(data) {
  return {
    type: FETCH_LOKSABHAEXITPOLL_SUCCESS,
    payload: data
  };
}

function fetchLOKSABHAEXITPOLL_META_DataFailure(error) {
  return {
    type: FETCH_LOKSABHAEXITPOLL_META_FAILURE,
    payload: error.message
  };
}

function fetchLOKSABHAEXITPOLL_META_DataSuccess(data) {
  return {
    type: FETCH_LOKSABHAEXITPOLL_META_SUCCESS,
    payload: data
  };
}

function fetchLOKSABHAEXITPOLL_Data(state, params, query) {
  const _url = `https://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-exitpoll/2019.htm`;
  // const _url = `http://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-nationaldatahub/2014/hindi.htm`;

  return dispatch => {
    dispatch({ type: FETCH_LOKSABHAEXITPOLL_REQUEST });
    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchLOKSABHAEXITPOLL_DataSuccess(JSON.parse(data)));
        },
        error => dispatch(fetchLOKSABHAEXITPOLL_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchLOKSABHAEXITPOLL_DataFailure(error));
      });
  };
}

function shouldFetchLOKSABHAEXITPOLL_Data(state, params, query) {
  return true;
}

export function fetchLOKSABHAEXITPOLL_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLOKSABHAEXITPOLL_Data(getState(), params, query)) {
      return dispatch(fetchLOKSABHAEXITPOLL_Data(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}

function fetchLOKSABHAEXITPOLL_META_Data(state, params, query) {
  let msid = "";

  if (params && params.msid) {
    msid = params.msid;
  } else {
    return dispatch => {
      return;
    };
  }
  let _url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_LOKSABHAEXITPOLL_META_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data)
            throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchLOKSABHAEXITPOLL_META_DataSuccess(JSON.parse(data)));
        },
        error => dispatch(fetchLOKSABHAEXITPOLL_META_DataFailure(error))
      )
      .catch(error => {
        dispatch(fetchLOKSABHAEXITPOLL_META_DataFailure(error));
      });
  };
}

function shouldFetchLOKSABHAEXITPOLL_META_Data(state, params, query) {
  return true;
}

export function fetchLOKSABHAEXITPOLL_META_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLOKSABHAEXITPOLL_META_Data(getState())) {
      return dispatch(
        fetchLOKSABHAEXITPOLL_META_Data(getState(), params, query)
      );
    } else {
      return new Promise.resolve([]);
    }
  };
}
