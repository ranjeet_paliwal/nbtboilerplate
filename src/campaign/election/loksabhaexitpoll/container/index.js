import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import PropTypes from "prop-types";
import { fetchLOKSABHAEXITPOLL_IfNeeded, fetchLOKSABHAEXITPOLL_META_IfNeeded } from "../action";
import { constants } from "./../../utils/constants";

import StateWiseWidget from "./../../common/component/StateWiseWidget";
import ResultTableWidget from "./../../common/component/ResultTableWidget";
import { PageMeta } from "../../../../components/common/PageMeta"; //For Page SEO/Head Part
import WebTitleCard from "../../../../components/common/WebTitleCard";
import TabsWidget from "./../../common/component/TabsWidget";
import ResultPieDotsWidget, { renderDotsPieWidget, PieTableWidget } from "../../common/component/ResultPieDotsWidget";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { _isCSR, getPageType } from "../../../../utils/util";
import SocialShare from "../../../../components/common/SocialShare";
import Breadcrumb from "../../../../components/common/Breadcrumb";

import { electionConfig } from "../../utils/config";
import Ads_module from "../../../../components/lib/ads";
const _electionConfig = electionConfig[process.env.SITE];

let svgElectionWidget = "";

class LOKSABHAEXITPOLL extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderPie: false,
      type: "alliancewise",
      sourceType: "",
    };
    this.config = {
      pollingInterval: 30000,
      pollingClear: false,
      pwadata: {},
    };
    // this.dataPooling = this.dataPooling.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
    this.createCoustomData = this.createCoustomData.bind(this);
  }

  componentDidMount() {
    let _this = this;
    const { dispatch, params, query, data } = _this.props;
    let __data = data;

    LOKSABHAEXITPOLL.fetchData({ dispatch, params, query })
      .then(_data => {
        // fetchLOKSABHAEXITPOLL_IfNeeded({ dispatch, params, query }).then((_data)=>{
        if (typeof _data != "undefined") __data = _data;
        // renderDotsPieWidget(_this.config.custom_data, _this.state.type);

        if (
          __data &&
          __data.yrdata &&
          __data.yrdata.length > 0 &&
          __data.yrdata[0] &&
          __data.yrdata[0].source &&
          __data.yrdata[0].source.length > 0
        ) {
          _this.state.sourceType = __data.yrdata[0].source[0].nm;
        }
      })
      .catch(err => {
        console.log(err.message);
      });

    //set section for election related pages
    Ads_module.setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
  }

  componentWillUnmount() {
    // reset  section details
    Ads_module.setSectionDetail();
  }

  onTabClick(event) {
    let _this = this;
    let { data } = _this.props;
    let elem = event.currentTarget;

    elem.parentElement.querySelectorAll("li").forEach(item => item.classList.remove("active"));
    elem.classList.add("active");

    if (elem.getAttribute("tab-type")) {
      _this.setState({ sourceType: elem.getAttribute("data-name") });
    } else {
      _this.setState({ type: elem.getAttribute("data-name") });
    }

    _this.config.shouldRenderPie = true;
  }

  createCoustomData(data) {
    let _this = this;

    _this.config.custom_data = {};
    if (data && data.yrdata && data.yrdata.length > 0 && data.yrdata[0]) {
      _this.config.custom_data.items =
        _this.state.type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt : data.yrdata[0].al_rslt;
      if (data.resulttype && data.yrdata[0].source && data.yrdata[0].source.length > 0) {
        let sourceType_index = 0;
        _this.state.sourceType != "" &&
          data.yrdata[0].source.map((item, i) => {
            if (item.nm == _this.state.sourceType) sourceType_index = i;
          });
        // _this.config.custom_data.states = []
        // data.yrdata[0].states.map((item)=>{
        // 	if(item.nm == _this.state.sourceType){
        // 		_this.config.custom_data.states.push(item)
        // 	}
        // })
        _this.config.custom_data.items =
          _this.state.type == constants.PARTYWISE
            ? data.yrdata[0].source[sourceType_index].starpr_rslt
            : data.yrdata[0].source[sourceType_index].al_rslt;
        _this.config.custom_data.source = data.yrdata[0].source;
        _this.config.custom_data.states = data.yrdata[0].states;
        _this.state.sourceType = data.yrdata[0].source[sourceType_index].nm;
      }
      _this.config.custom_data.items_index =
        _this.state.type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt_index : data.yrdata[0].al_rslt_index;
      _this.config.custom_data.master = _this.state.type == constants.PARTYWISE ? data.pr_master : data.al_master;
      _this.config.custom_data.master_index =
        _this.state.type == constants.PARTYWISE ? data.pr_master_index : data.al_master_index;
      _this.config.custom_data.year = data.yrdata[0].yr;
      _this.config.custom_data.ttl_seats = data.yrdata[0].ttl_seats;
    }
  }

  render() {
    let _this = this;
    let { data, pagetype, router } = _this.props;
    let breadcrumbData =
      this.props.metadata &&
      this.props.metadata.breadcrumb &&
      this.props.metadata.breadcrumb.div &&
      this.props.metadata.breadcrumb.div.ul
        ? this.props.metadata.breadcrumb.div.ul
        : null;

    if (!pagetype && router) {
      pagetype = getPageType(router.location.pathname);
    }

    _this.createCoustomData(data);

    return (
      <div
        className={"wdt_election_result exit-poll " + (pagetype == "home" || pagetype == "liveblog" ? "" : "white-bg")}
      >
        {/* For SEO */
        _this.props.metadata && _this.props.metadata.pwa_meta && PageMeta(_this.props.metadata.pwa_meta)}
        {pagetype == "home" || pagetype == "liveblog" ? (
          <h2>{`${_electionConfig.loksabhaExitpollHeading}`}</h2>
        ) : (
          <h1>{`${_electionConfig.loksabhaExitpollHeading}`}</h1>
        )}
        {/* tabs Widget */}
        {
          <ErrorBoundary>
            <TabsWidget onTabClick={this.onTabClick} />
            <TabsWidget onTabClick={this.onTabClick} tabtype="tabs_normal" items={_this.config.custom_data.source} />
            <SocialShare />
          </ErrorBoundary>
        }
        {
          <ErrorBoundary>
            <PieTableWidget
              data={_this.config.custom_data}
              type={_this.state.type}
              pagetype={pagetype}
              shouldRenderPie={_this.config.shouldRenderPie}
              _disclaimer={" Election was cancelled in Vellore by EC"}
            />
          </ErrorBoundary>
        }

        {/* State Wise Widget */}
        {
          <ErrorBoundary>
            <StateWiseWidget
              data={data}
              states={_this.config.custom_data.states}
              type={_this.state.type}
              sourceType={_this.state.sourceType}
              showPowerState={pagetype == "home" || pagetype == "liveblog" ? false : true}
            />
          </ErrorBoundary>
        }

        {_this.props.metadata &&
          _this.props.metadata.pwa_meta &&
          _this.props.metadata.pwa_meta.seodescription &&
          WebTitleCard(_this.props.metadata.pwa_meta)}
        {/* Breadcrumb  */}
        {breadcrumbData && pagetype != "home" && pagetype != "liveblog" ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumbData} />
          </ErrorBoundary>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.loksabhaexitpoll,
  };
}

LOKSABHAEXITPOLL.fetchData = ({ dispatch, params, query }) => {
  dispatch(fetchLOKSABHAEXITPOLL_META_IfNeeded(params, query));
  return dispatch(fetchLOKSABHAEXITPOLL_IfNeeded(params, query));
};

export default connect(mapStateToProps)(LOKSABHAEXITPOLL);
