import fetch from "utils/fetch/fetch";
// import { electionConfig } from "../../utils/config";

// const _electionConfig = electionConfig[process.env.SITE];

export const FETCH_LOKSABHAELECTIONRESULT_REQUEST = "FETCH_LOKSABHAELECTIONRESULT_REQUEST";
export const FETCH_LOKSABHAELECTIONRESULT_SUCCESS = "FETCH_LOKSABHAELECTIONRESULT_SUCCESS";
export const FETCH_LOKSABHAELECTIONRESULT_FAILURE = "FETCH_LOKSABHAELECTIONRESULT_FAILURE";
export const FETCH_LOKSABHAELECTIONRESULT_META_REQUEST = "FETCH_LOKSABHAELECTIONRESULT_META_REQUEST";
export const FETCH_LOKSABHAELECTIONRESULT_META_SUCCESS = "FETCH_LOKSABHAELECTIONRESULT_META_SUCCESS";
export const FETCH_LOKSABHAELECTIONRESULT_META_FAILURE = "FETCH_LOKSABHAELECTIONRESULT_META_FAILURE";

function fetchLOKSABHAELECTIONRESULT_DataFailure(error) {
  return {
    type: FETCH_LOKSABHAELECTIONRESULT_FAILURE,
    payload: error.message,
  };
}

export function fetchLOKSABHAELECTIONRESULT_DataSuccess(data) {
  return {
    type: FETCH_LOKSABHAELECTIONRESULT_SUCCESS,
    payload: data,
  };
}

function fetchLOKSABHAELECTIONRESULT_META_DataFailure(error) {
  return {
    type: FETCH_LOKSABHAELECTIONRESULT_META_FAILURE,
    payload: error.message,
  };
}

function fetchLOKSABHAELECTIONRESULT_META_DataSuccess(data) {
  return {
    type: FETCH_LOKSABHAELECTIONRESULT_META_SUCCESS,
    payload: data,
  };
}

function fetchLOKSABHAELECTIONRESULT_Data(state, params, query, widgetType) {
  // console.log(widgetType)
  let _url;
  let payloadData = {};
  let language = "tml";
  //  TODO :- Below written is not the right way to pass language just pick it from config files

  // let language = _electionConfig && _electionConfig.feedlanguage ? `/${_electionConfig.feedlanguage}` : "";
  if (widgetType == "result") {
    // params = params ? params : {};
    // params.value = params.value ? `/${params.value}` : "";

    _url = `https://toibnews.timesofindia.indiatimes.com/Election/result/2019${language}.htm`;
    //_url = `https://toibnews.timesofindia.indiatimes.com/jcms-fail.htm`;
  } else if (widgetType == "highlights" && params && params.value) {
    _url = `${process.env.API_BASEPOINT}/pwafeeds/pwa_articlelist/${params.value}.cms?feedtype=sjson`;
  } else if (widgetType == "candidates") {
    params = params ? params : {};
    params.value = params.value ? `/${params.value}` : "";
    _url = `https://toibnews.timesofindia.indiatimes.com/Election/candidate/2019${language}.htm`;
  } else if (widgetType == "infocus" && params && params.value) {
    _url = `${process.env.API_BASEPOINT}/pwafeeds/pwa_articlelist/${params.value}.cms?feedtype=sjson`;
  }

  return dispatch => {
    dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);

          if (widgetType == "result" || !widgetType) {
            payloadData = { data: JSON.parse(data) };
          } else if (widgetType == "highlights") {
            payloadData = { highlightsData: JSON.parse(data) };
          } else if (widgetType == "candidates") {
            payloadData = { candidateData: JSON.parse(data) };
          } else if (widgetType == "infocus") {
            payloadData = { infocusData: JSON.parse(data) };
          }

          dispatch(fetchLOKSABHAELECTIONRESULT_DataSuccess(payloadData));
        },
        error => {
          dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error));
        },
      )
      .catch(error => {
        dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error));
      });
  };
}

function shouldFetchLOKSABHAELECTIONRESULT_Data(state, params, query) {
  return true;
}

export function fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, widgetType) {
  return (dispatch, getState) => {
    if (shouldFetchLOKSABHAELECTIONRESULT_Data(getState())) {
      return dispatch(fetchLOKSABHAELECTIONRESULT_Data(getState(), params, query, widgetType));
    } else {
      return new Promise.resolve([]);
    }
  };
}

function fetchLOKSABHAELECTIONRESULT_META_Data(state, params, query) {
  let msid = _electionConfig.loksabhaResultsMSID;
  if (params && params.msid) {
    msid = params.msid;
  }
  let _url = `${process.env.API_BASEPOINT}/pwa_metalist/${msid}.cms?feedtype=sjson`;
  //let _url = `https://navbharattimes.indiatimes.com/pwafeeds/pwa_metalist/69253542.cms?feedtype=sjson`;
  return dispatch => {
    dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_META_REQUEST });

    return fetch(_url, { type: "jsonp" })
      .then(
        data => {
          // data = data.replace('' , '').slice(0, -1);
          if (!data) throw new NetworkError("Invalid JSON Response", response.status);
          dispatch(fetchLOKSABHAELECTIONRESULT_META_DataSuccess(JSON.parse(data)));
        },
        error => dispatch(fetchLOKSABHAELECTIONRESULT_META_DataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchLOKSABHAELECTIONRESULT_META_DataFailure(error));
      });
  };
}

function shouldFetchLOKSABHAELECTIONRESULT_META_Data(state, params, query) {
  return true;
}

export function fetchLOKSABHAELECTIONRESULT_META_IfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLOKSABHAELECTIONRESULT_META_Data(getState())) {
      return dispatch(fetchLOKSABHAELECTIONRESULT_META_Data(getState(), params, query));
    } else {
      return new Promise.resolve([]);
    }
  };
}

// export function fetchELECTIONRESULT_Data_Polling() {
// 	const _url = `https://toibnews.timesofindia.indiatimes.com/Election/result/2019.htm`;
// 	// const _url = process.env.WEBSITE_URL+'/staticpage/file?filename=mobile_elections_merged_2018.htm';

// 	return dispatch => {
// 		//dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_REQUEST });

// 		return fetch(_url, { type: 'jsonp' }).then(
// 			(data) => {
// 				// data = data.replace('' , '').slice(0, -1);
// 				if (!data) throw new NetworkError('Invalid JSON Response', response.status);
// 				dispatch(fetchLOKSABHAELECTIONRESULT_DataSuccess({ data: JSON.parse(data) }))
// 			},
// 			error => dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		).catch((error) => {
// 			dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		});
// 	}
// }

// function fetchLOKSABHAELECTIONHIGHLIGHTS_Data(state, params, query) {
// 	const _url = `https://navbharattimes.indiatimes.com/pwafeeds/pwa_articlelist/68077184.cms?feedtype=sjson`;
// 	// const _url = `http://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-nationaldatahub/2014/hindi.htm`;

// 	return dispatch => {
// 		dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_REQUEST });

// 		return fetch(_url, { type: 'jsonp' }).then(
// 			(data) => {
// 				// data = data.replace('' , '').slice(0, -1);
// 				if (!data) throw new NetworkError('Invalid JSON Response', response.status);
// 				dispatch(fetchLOKSABHAELECTIONRESULT_DataSuccess({ highlightsData: JSON.parse(data) }))
// 			},
// 			error => dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		).catch((error) => {
// 			dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		});
// 	}
// }

// function shouldFetchLOKSABHAELECTIONHIGHLIGHTS_Data(state, params, query) {
// 	return true;
// }

// export function fetchLOKSABHAELECTIONHIGHLIGHTS_IfNeeded(params, query) {
// 	return (dispatch, getState) => {
// 		if (shouldFetchLOKSABHAELECTIONHIGHLIGHTS_Data(getState(), params, query)) {
// 			return dispatch(fetchLOKSABHAELECTIONHIGHLIGHTS_Data(getState(), params, query));
// 		} else {
// 			return new Promise.resolve([]);
// 		}
// 	};
// }

// function fetchLOKSABHAELECTIONCANDIDATE_Data(state, params, query) {
// 	const _url = `https://toibnews.timesofindia.indiatimes.com/Election/candidate/2019.htm`;
// 	// const _url = `http://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-nationaldatahub/2014/hindi.htm`;

// 	return dispatch => {
// 		dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_REQUEST });

// 		return fetch(_url, { type: 'jsonp' }).then(
// 			(data) => {
// 				// data = data.replace('' , '').slice(0, -1);
// 				if (!data) throw new NetworkError('Invalid JSON Response', response.status);
// 				dispatch(fetchLOKSABHAELECTIONRESULT_DataSuccess({ candidateData: JSON.parse(data) }))
// 			},
// 			error => dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		).catch((error) => {
// 			dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		});
// 	}
// }

// function shouldFetchLOKSABHAELECTIONCANDIDATE_Data(state, params, query) {
// 	return true;
// }

// export function fetchLOKSABHAELECTIONCANDIDATE_IfNeeded(params, query) {
// 	return (dispatch, getState) => {
// 		if (shouldFetchLOKSABHAELECTIONCANDIDATE_Data(getState(), params, query)) {
// 			return dispatch(fetchLOKSABHAELECTIONCANDIDATE_Data(getState(), params, query));
// 		} else {
// 			return new Promise.resolve([]);
// 		}
// 	};
// }

// function fetchLOKSABHAELECTIONINFOCUS_Data(state, params, query) {
// 	const _url = `https://navbharattimes.indiatimes.com/pwafeeds/pwa_articlelist/69340532.cms?feedtype=sjson`;
// 	// const _url = `http://toibnews.timesofindia.indiatimes.com/Election/lok-sabha-nationaldatahub/2014/hindi.htm`;

// 	return dispatch => {
// 		dispatch({ type: FETCH_LOKSABHAELECTIONRESULT_REQUEST });

// 		return fetch(_url, { type: 'jsonp' }).then(
// 			(data) => {
// 				// data = data.replace('' , '').slice(0, -1);
// 				if (!data) throw new NetworkError('Invalid JSON Response', response.status);
// 				dispatch(fetchLOKSABHAELECTIONRESULT_DataSuccess({ infocusData: JSON.parse(data) }))
// 			},
// 			error => dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		).catch((error) => {
// 			dispatch(fetchLOKSABHAELECTIONRESULT_DataFailure(error))
// 		});
// 	}
// }

// function shouldFetchLOKSABHAELECTIONINFOCUS_Data(state, params, query) {
// 	return true;
// }

// export function fetchLOKSABHAELECTIONINFOCUS_IfNeeded(params, query) {
// 	return (dispatch, getState) => {
// 		if (shouldFetchLOKSABHAELECTIONINFOCUS_Data(getState(), params, query)) {
// 			return dispatch(fetchLOKSABHAELECTIONINFOCUS_Data(getState(), params, query));
// 		} else {
// 			return new Promise.resolve([]);
// 		}
// 	};
// }
