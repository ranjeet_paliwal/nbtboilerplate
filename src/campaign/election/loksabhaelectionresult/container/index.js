import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import PropTypes from "prop-types";
//import { fetchLOKSABHAELECTIONRESULT_IfNeeded, fetchLOKSABHAELECTIONRESULT_META_IfNeeded, fetchELECTIONRESULT_Data_Polling, fetchLOKSABHAELECTIONHIGHLIGHTS_IfNeeded, fetchLOKSABHAELECTIONCANDIDATE_IfNeeded, fetchLOKSABHAELECTIONINFOCUS_IfNeeded } from '../action';
import { fetchLOKSABHAELECTIONRESULT_IfNeeded, fetchLOKSABHAELECTIONRESULT_META_IfNeeded } from "../action";
import { constants } from "./../../utils/constants";

import StarCandidatesWidget from "./../../common/component/StarCandidatesWidget";
import StateWiseWidget from "./../../common/component/StateWiseWidget";
import ResultTableWidget from "./../../common/component/ResultTableWidget";
import InFocusWidget from "./../../common/component/InFocusWidget";
import TabsWidget from "./../../common/component/TabsWidget";
import HighlightsWidget from "./../../common/component/HighlightsWidget";
import { PageMeta } from "../../../../components/common/PageMeta"; //For Page SEO/Head Part
import WebTitleCard from "../../../../components/common/WebTitleCard";

import ResultPieDotsWidget, { renderDotsPieWidget } from "../../common/component/ResultPieDotsWidget";
import LiveBlogStrip from "./../../../../modules/liveblogstrip/LiveBlogStrip";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { _getStaticConfig, _deferredDeeplink, setHyp1Data } from "../../../../utils/util";
import { electionConfig } from "../../utils/config";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import Ads_module from "../../../../components/lib/ads";
import SocialShare from "../../../../components/common/SocialShare";
import AnchorLink from "../../../../components/common/AnchorLink";
const siteConfig = _getStaticConfig();

const _electionConfig = electionConfig[process.env.SITE];

const feedTimer = {
  resultfeedtimer: null,
  candidatefeedtimer: null,
  pollingInterval: 30000,
};

class LokSabhaElectionResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderPie: false,
      type: constants.ALLIANCEWISE,
    };
    this.config = {
      type: constants.ALLIANCEWISE,
      shouldRenderPie: false,
      pollingInterval: 30000,
      pollingClear: false,
      pwadata: {},
    };
    // this.dataPooling = this.dataPooling.bind(this);
    this.onTabClick = this.onTabClick.bind(this);
    this.createCoustomData = this.createCoustomData.bind(this);
    // this.dataPooling = this.dataPooling.bind(this);
  }

  componentDidMount() {
    let _this = this;
    let { dispatch, params, query, data } = _this.props;
    // let __data = data;
    params = params ? params : {};
    params.value = "";

    LokSabhaElectionResult.fetchData({ dispatch, params, query })
      .then(_data => {
        if (typeof _data != "undefined") __data = _data;
        renderDotsPieWidget(_this.config.custom_data, _this.state.type);
      })
      .catch(err => {
        // console.log(err.message);
      });

    // dataPooling("result", _this.props);

    if (_electionConfig.loksabhaResultHighlightMSID) {
      params.value = _electionConfig.loksabhaResultHighlightMSID;
      dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "highlights"));
    }

    if (_electionConfig.feedlanguage) {
      params.value = _electionConfig.feedlanguage;
      dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "candidates"));
      // dataPooling("candidates", _this.props);
    }

    if (_electionConfig.loksabhaResultInfocusMSID) {
      params.value = _electionConfig.loksabhaResultInfocusMSID;
      dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "infocus"));
    }

    // dispatch(fetchLOKSABHAELECTIONCANDIDATE_IfNeeded(params, query));
    // _this.config.candidatefeedtimer = setInterval(() => {
    // 	dispatch(fetchLOKSABHAELECTIONCANDIDATE_IfNeeded(params, query));
    // 	//   if (_this.config.pollingClear) clearInterval(timer);
    // }, window.pollingInterval || _this.config.pollingInterval);

    // dispatch(fetchLOKSABHAELECTIONINFOCUS_IfNeeded(params, query));

    //set hyp1 variable
    setHyp1Data({ hyp1: "election_campaign" });

    //set section for election related pages
    Ads_module.setSectionDetail({ subsectitle1: "elections", adsec: "microsite" });
  }

  componentWillUnmount() {
    //reset hyp1 variable
    setHyp1Data();

    //clear timer
    clearInterval(feedTimer.resultfeedtimer);
    clearInterval(feedTimer.candidatefeedtimer);

    // reset  section details
    Ads_module.setSectionDetail();
  }

  // dataPooling(widgetType) {
  // 	let _this = this;
  // 	const { dispatch, params, query } = _this.props;
  // 	let pollingInterval = widgetType == "result" ? _this.config.pollingInterval : widgetType == "candidates" ? _this.config.pollingInterval : 30000;

  // 	let electioninterval = setInterval(() => {
  // 		dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, widgetType));
  // 		//   if (_this.config.pollingClear) clearInterval(timer);
  // 	}, window.pollingInterval || pollingInterval);

  // 	if (widgetType == "result") {
  // 		_this.config.resultfeedtimer = electioninterval;
  // 	}
  // 	else if (widgetType == "candidates") {
  // 		_this.config.candidatefeedtimer = electioninterval;
  // 	}
  // }

  onTabClick(event) {
    let _this = this;
    let { data } = _this.props;
    let elem = event.currentTarget;
    let elemSibling = elem.previousSibling ? elem.previousSibling : elem.nextSibling;

    elem.classList.add("active");
    elemSibling.classList.remove("active");

    _this.setState({ type: elem.getAttribute("data-name") });
    _this.config.shouldRenderPie = true;
  }

  createCoustomData(data) {
    let _this = this;
    _this.config.custom_data = {};
    if (data && data.yrdata && data.yrdata.length > 0 && data.yrdata[0]) {
      _this.config.custom_data.items =
        _this.state.type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt : data.yrdata[0].al_rslt;
      _this.config.custom_data.items_index =
        _this.state.type == constants.PARTYWISE ? data.yrdata[0].starpr_rslt_index : data.yrdata[0].al_rslt_index;
      _this.config.custom_data.master = _this.state.type == constants.PARTYWISE ? data.pr_master : data.al_master;
      _this.config.custom_data.master_index =
        _this.state.type == constants.PARTYWISE ? data.pr_master_index : data.al_master_index;
      _this.config.custom_data.year = data.yrdata[0].yr;
      _this.config.custom_data.ttl_seats = data.yrdata[0].ttl_seats;
    }
  }

  render() {
    let _this = this;
    let { data, pagetype, highlightsData, candidateData, infocusData, appdata } = _this.props;
    _this.createCoustomData(data);
    let breadcrumbData =
      this.props.metadata &&
      this.props.metadata.breadcrumb &&
      this.props.metadata.breadcrumb.div &&
      this.props.metadata.breadcrumb.div.ul
        ? this.props.metadata.breadcrumb.div.ul
        : null;

    return (
      <div className={"wdt_election_result " + (pagetype == "home" || pagetype == "liveblog" ? "" : "white-bg")}>
        {/* For SEO */
        pagetype != "home" &&
          pagetype != "liveblog" &&
          _this.props.metadata &&
          _this.props.metadata.pwa_meta &&
          PageMeta(_this.props.metadata.pwa_meta)}
        {pagetype == "home" || pagetype == "liveblog" ? (
          <h2>
            <AnchorLink href={_electionConfig.result_liveblog_link}>
              {_electionConfig.loksabhaResultsHeading} {_this.config.custom_data.year}
            </AnchorLink>
          </h2>
        ) : (
          <h1>
            {_electionConfig.loksabhaResultsHeading} {_this.config.custom_data.year}
          </h1>
        )}
        {/* tabs Widget */}
        {
          <ErrorBoundary>
            <TabsWidget _electionConfig={_electionConfig} onTabClick={this.onTabClick} />
            <SocialShare />
          </ErrorBoundary>
        }
        <a
          href={_deferredDeeplink(
            "campaignHome",
            siteConfig.appdeeplink,
            null,
            siteConfig.mweburl +
              "/elections/lok-sabha-elections/results/" +
              _electionConfig.loksabhaResultsMSID +
              ".cms?frmapp=yes",
          )}
          className="right-open-in-app"
        >
          <span>Open</span> in App
        </a>
        <ErrorBoundary>
          <ResultPieDotsWidget
            _electionConfig={_electionConfig}
            data={_this.config.custom_data}
            type={_this.state.type}
            shouldRenderPie={_this.config.shouldRenderPie}
            isTabularLayout={pagetype == "home" ? false : true}
            appdata={appdata}
          />
          <ResultTableWidget
            _electionConfig={_electionConfig}
            data={_this.config.custom_data}
            type={_this.state.type}
            isTabularLayout={pagetype == "home" ? false : true}
            pagetype={pagetype}
            _disclaimer={_electionConfig.loksabhaDisclaimer}
          />
        </ErrorBoundary>

        {/* Star Candidates Widget */}
        {pagetype != "home" && pagetype != "liveblog" ? (
          <ErrorBoundary>
            <StarCandidatesWidget
              states={candidateData && candidateData.yrdata ? candidateData.yrdata[0].states : undefined}
              _electionConfig={_electionConfig}
            />
          </ErrorBoundary>
        ) : null}

        {/* State Wise Widget */}
        {pagetype != "liveblog" ? (
          <ErrorBoundary>
            <StateWiseWidget
              data={data}
              states={data && data.yrdata ? data.yrdata[0].states : undefined}
              type={_this.state.type}
              showPowerState={pagetype == "home" || pagetype == "liveblog" ? false : true}
              _electionConfig={_electionConfig}
            />
          </ErrorBoundary>
        ) : null}

        {/*Widgets to show on Home page only*/}
        {pagetype == "home" ? (
          <ErrorBoundary>
            {/* inSide link Move to Assembly Widget*/}
            <div className="inner_box_widget">
              <a style={{ textAlign: "center", display: "block" }} href="#assemblyElectionWidget">
                {_electionConfig.tap_to_see_assembly_results
                  ? _electionConfig.tap_to_see_assembly_results
                  : "Tap to see Assembly Election Results"}
              </a>
            </div>
            {/* in Liveblog Widget */}
            {_electionConfig.loksabhaResultLiveblogMSID && (
              <LiveBlogStrip
                _electionConfig={_electionConfig}
                msid={_electionConfig.loksabhaResultLiveblogMSID}
                liveblog_page_url={_electionConfig.loksabhaResultLiveblogURL}
                widgetType={"multipleposts"}
              />
            )}
            {/* in Highlights Widget */}
            <HighlightsWidget _electionConfig={_electionConfig} data={highlightsData && highlightsData.items} />
            {/* in focus Widget */}
            <InFocusWidget data={infocusData} />
            {/* Star Candidates Widget */}
            <StarCandidatesWidget
              states={candidateData && candidateData.yrdata ? candidateData.yrdata[0].states : null}
              _electionConfig={_electionConfig}
            />
          </ErrorBoundary>
        ) : null}

        {(pagetype != "home" || pagetype != "liveblog") &&
          _this.props.metadata &&
          _this.props.metadata.pwa_meta &&
          _this.props.metadata.pwa_meta.seodescription &&
          WebTitleCard(_this.props.metadata.pwa_meta)}

        {/* Breadcrumb  */}
        {breadcrumbData && pagetype != "home" && pagetype != "liveblog" ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumbData} />
          </ErrorBoundary>
        ) : null}
      </div>
    );
  }
}

export function dataPooling(widgetType, props) {
  let { dispatch, query, appdata, election } = props;
  // console.trace("hello--");
  if (appdata == undefined) {
    appdata = { election: election };
  }
  if (appdata && appdata.election && appdata.election.result.isdatapolling == "false") {
    return false;
  }

  let pollingInterval =
    widgetType == "result" ? feedTimer.pollingInterval : widgetType == "candidates" ? feedTimer.pollingInterval : 30000;

  let electioninterval = setInterval(() => {
    let params = params ? params : {};
    params.value = _electionConfig.feedlanguage;
    dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, widgetType));
    //   if (_this.config.pollingClear) clearInterval(timer);
  }, window.pollingInterval || pollingInterval);

  if (widgetType == "result") {
    feedTimer.resultfeedtimer = electioninterval;
  } else if (widgetType == "candidates") {
    feedTimer.candidatefeedtimer = electioninterval;
  }
}

function mapStateToProps(state) {
  return {
    ...state.loksabhaelectionresult,
    appdata: state.app,
  };
}

LokSabhaElectionResult.fetchData = ({ dispatch, params, query }) => {
  params && (params.value = _electionConfig.feedlanguage);
  dispatch(fetchLOKSABHAELECTIONRESULT_META_IfNeeded(params, query));
  return dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "result"));
};

export default connect(mapStateToProps)(LokSabhaElectionResult);
