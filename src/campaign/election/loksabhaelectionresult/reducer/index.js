import {
  FETCH_LOKSABHAELECTIONRESULT_REQUEST,
  FETCH_LOKSABHAELECTIONRESULT_SUCCESS,
  FETCH_LOKSABHAELECTIONRESULT_FAILURE,
  FETCH_LOKSABHAELECTIONRESULT_META_REQUEST,
  FETCH_LOKSABHAELECTIONRESULT_META_SUCCESS,
  FETCH_LOKSABHAELECTIONRESULT_META_FAILURE
} from "./../action/index";

function LOKSABHAELECTIONRESULT(state = {}, action) {
  switch (action.type) {
    case FETCH_LOKSABHAELECTIONRESULT_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_LOKSABHAELECTIONRESULT_SUCCESS:
      let { data, candidateData, infocusData, highlightsData } = action.payload;
      state = data
        ? { ...state, data: data }
        : candidateData
        ? { ...state, candidateData: candidateData }
        : infocusData
        ? { ...state, infocusData: infocusData.items }
        : highlightsData
        ? { ...state, highlightsData: highlightsData }
        : null;
      //state.data = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_LOKSABHAELECTIONRESULT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    case FETCH_LOKSABHAELECTIONRESULT_META_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };

    case FETCH_LOKSABHAELECTIONRESULT_META_SUCCESS:
      state.metadata = action.payload;

      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_LOKSABHAELECTIONRESULT_META_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default LOKSABHAELECTIONRESULT;
