import React, { Component } from "react";
import "./../css/Election.scss";
import SocialShare from "./../../../../components/common/SocialShare";
// import AnchorLink from '../../../../components/common/AnchorLink';
const ConstituenciesWidget = props => {
  return (
    <div className="chart_container wdt_constituencies">
      <div className="top_search_container">
        <h1>constituencies</h1>
        <h2>Lok Sabha Elections 2019</h2>
        <div className="searchNshare">
          <div className="input_search">
            <input
              type="text"
              placeholder="Search candidate, state, party and constituency"
            ></input>
          </div>
          <SocialShare />
        </div>
      </div>
      <div className="bottom_result_container">
        <div className="total_count">
          Leads + Wins <strong>(265/543)</strong>
          <span className="liveTicker">Live</span>
        </div>
        <ul>
          <li>
            <h3>Tonk - Sawai Madhopur, Rajasthan</h3>
            <div className="party_result">
              <div className="left">
                <h4>Bhusanur Ramesh Balappa</h4>
                <span className="current_position leading">Leading</span>
              </div>
              <span className="party_name">
                <b style={{ background: "#ff9c65" }}>BJP</b>
              </span>
            </div>
            <div className="party_result">
              <div className="left">
                <h4>Bhusanur Ramesh Balappa</h4>
                <span className="current_position">incumbent</span>
              </div>
              <span className="party_name">
                <b style={{ background: "#ff9c65" }}>BJP</b>
              </span>
            </div>
          </li>
          <li>
            <h3>Tonk - Sawai Madhopur, Rajasthan</h3>
            <div className="party_result">
              <div className="left">
                <h4>Bhusanur Ramesh Balappa</h4>
                <span className="current_position won">Won</span>
              </div>
              <span className="party_name">
                <b style={{ background: "#ff9c65" }}>BJP</b>
              </span>
            </div>
            <div className="party_result">
              <div className="left">
                <h4>Bhusanur Ramesh Balappa</h4>
                <span className="current_position lost">Lost</span>
              </div>
              <span className="party_name">
                <b style={{ background: "#ff9c65" }}>BJP</b>
              </span>
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ConstituenciesWidget;
