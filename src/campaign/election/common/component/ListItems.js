import React from "react";

import { _getStaticConfig, isMobilePlatform } from "./../../../../utils/util";
const siteConfig = _getStaticConfig();

const ListItems = (items, _index, total) => {
  return (items.length > 0 && parseInt(items[0].ws) + parseInt(items[0].ls)) > 0 ? (
    <div data-ele="table_mp_0" key={_index} className="table-chart">
      {!isMobilePlatform() ? (<div className="clearfix powerbycont mp ">
        <div className="el_powerdby">Source: {items[0].src}</div>
      </div>) : null}
      <div className="result-tbl">
        <div className="tbl_heading">
            <div className="tbl_col  first">Party</div>
            <div className="tbl_col  tac">Projected</div>
        </div>
        {items.map((item, index) => {
          return (
            <div className="tbl_row" key={index}>
              <div className="tbl_col col1 party_logo">
                <img src={`${siteConfig.weburl}/photo/${item.lg}.cms`} />
                {item.an}
              </div>
              {/* <td className="tbl_col tac col1 ">{parseInt(item.ws)}</td> */}
              <div className="tbl_col tac col1 ">{parseInt(item.ws) + parseInt(item.ls)}</div>
            </div>
          );
        })}

        <div className="tbl_row">
      <div className="tbl_col col1 party_logo">TOTAL SEATS : {total}</div>
          <div className="tbl_col tac col1 ">Majority Mark: {Math.floor(total / 2) + 1}</div>
        </div>
      </div>
      {isMobilePlatform() ? (<div className="clearfix powerbycont mp ">
        <div className="el_powerdby">Source: {items[0].src}</div>
      </div>) : null}
    </div>
  ) : null;
};

export default ListItems;
