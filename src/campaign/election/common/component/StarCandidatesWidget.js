import React, { Component } from "react";
import "./../css/Election.scss";
import AnchorLink from "../../../../components/common/AnchorLink";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";

const StarCandidatesWidget = props => {
  let { states, _electionConfig } = props;
  //let statusMapping = {"1":"leading", "2":"trailing", "3":"won", "4":"lost"}
  let statusMapping = ["leading", "trailing", "won", "lost"];
  return states ? (
    <div className="star_candidates_widget inner_box_widget">
      <div className="h2WithViewMore">
        <AnchorLink href="/elections/candidates">
          <h2 className="election_h2">{_electionConfig.starCandidateTxt}</h2>
        </AnchorLink>
        {/* <AnchorLink href="/elections/candidates" className="view_more">View All Candidates</AnchorLink> */}
      </div>

      <div className="scroll_content">
        <ul className="candidates-column">
          {states &&
            states.length > 0 &&
            states.map((state, i) => {
              return (
                state &&
                state.cns.length > 0 &&
                state.cns.map((cns, i) => {
                  return (
                    cns &&
                    cns.cnt.length > 0 &&
                    cns.cnt.map((cnt, i) => {
                      return (
                        <li
                          key={cnt.nm}
                          className={
                            cnt.status == 3
                              ? "won_status"
                              : cnt.status == 4
                              ? "lost_status"
                              : cnt.status == 1
                              ? "leading_status"
                              : cnt.status == 2
                              ? "trailing_status"
                              : ""
                          }
                        >
                          <a href={`/elections/candidates/${cnt.seonm || ""}`}>
                            <span className="candidate_status">
                              <ImageCard
                                msid={cnt.img ? cnt.img : "69347424"}
                                title=""
                                size="smallthumb"
                                className="candidate_img"
                              />
                              {/* <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img> */}
                              {cnt.status && cnt.status != 0 && <b className={statusMapping[cnt.status - 1]}></b>}
                            </span>
                            <h3>
                              <span className="text_ellipsis">{cnt.nm}</span>
                            </h3>
                            <span className="BJP_state party">{cnt.pr}</span>
                            <span className="district">{cns.nm}</span>
                            <span className="state">{state.nm}</span>
                          </a>
                        </li>
                      );
                    })
                  );
                })
              );
            })}
          {/* statusText: {"1":"leading", "2":"trailing", "3":"won", "4":"lost"} */}
          {/* <li>
                        <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img>
                        <h3>cns.cnt.pr</h3>
                        <span className="BJP_state party">cns.cnt.pr</span>
                        <span className="district">cns.nm</span>
                        <span className="state">nm</span>
                    </li>
                    <li>
                        <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img>
                        <h3>Rajyavardhan Singh Rathore</h3>
                        <span className="BJP_state party">BJP</span>
                        <span className="district">Tonk-Sawai Madhopur</span>
                        <span className="state">Rajasthan</span>
                    </li>
                    <li>
                        <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img>
                        <h3>Rajyavardhan Singh Rathore</h3>
                        <span className="BJP_state party">BJP</span>
                        <span className="district">Tonk-Sawai Madhopur</span>
                        <span className="state">Rajasthan</span>
                    </li>
                    <li>
                        <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img>
                        <h3>Rajyavardhan Singh Rathore</h3>
                        <span className="BJP_state party">BJP</span>
                        <span className="district">Tonk-Sawai Madhopur</span>
                        <span className="state">Rajasthan</span>
                    </li>
                    <li>
                        <img src="https://static.langimg.com/thumb/msid-69300410,width-135,height-102,resizemode-4/navbharat-times.jpg" className="candidate_img"></img>
                        <h3>Rajyavardhan Singh Rathore</h3>
                        <span className="BJP_state party">BJP</span>
                        <span className="district">Tonk-Sawai Madhopur</span>
                        <span className="state">Rajasthan</span>
                    </li> */}
        </ul>
      </div>
    </div>
  ) : null;
};

export default StarCandidatesWidget;
