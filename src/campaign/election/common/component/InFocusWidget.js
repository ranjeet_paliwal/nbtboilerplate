import React, { Component } from "react";
import "./../css/Election.scss";
import AnchorLink from "../../../../components/common/AnchorLink";
import { electionConfig } from "../../utils/config";
const _electionConfig = electionConfig[process.env.SITE];

const InFocusWidget = props => {
  //let {items} = this.props;

  let { data } = props;
  let keywordlink = {};
  return data ? (
    <div className="in_focus_widget inner_box_widget">
      <h2 className="election_h2"> {_electionConfig.inFocus} </h2>
      <div className="scroll_content">
        <ul>
          {data &&
            data.length > 0 &&
            data.map((item, index) => {
              keywordlink = item.override
                ? { override: item.override }
                : {
                    seo: item.seolocation,
                    tn: item.tn,
                    id:
                      item.tn == "photo"
                        ? item.id && item.id != ""
                          ? item.id
                          : item.imageid
                        : item.id
                  };
              return (
                item.tn != "ad" && (
                  <li key={"infocus" + index}>
                    <AnchorLink hrefData={keywordlink}>{item.hl}</AnchorLink>
                  </li>
                )
              );
            })}
          {/* <li><AnchorLink ></AnchorLink><a href="#">Lok sabha Elections Result</a></li>
                    <li><a href="#">Assembly Election Results</a></li> */}
        </ul>
      </div>
    </div>
  ) : null;
};

export default InFocusWidget;
