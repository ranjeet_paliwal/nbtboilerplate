import React from "react";
import "./../css/Election.scss";
import { coustomizeFeedData } from "../../utils/util";
import { constants } from "../../utils/constants";
import AnchorLink from "../../../../components/common/AnchorLink";

const StateWiseWidget = props => {
  //let {items} = this.props;

  let {
    data,
    states,
    type,
    sourceType,
    showPowerState,
    _electionConfig
  } = props;
  let showStateWise = false;
  let showPowerStateWise = false;
  let stateDisClaimer = "State data will populate once available";

  states &&
    states.length > 0 &&
    states.forEach((state, i) => {
      let _data = coustomizeFeedData(data, type, state, sourceType);
      _data &&
        _data.items &&
        _data.items.length > 0 &&
        _data.items.forEach((item, i) => {
          if (showPowerState) {
            if (constants.POWER_STATES.indexOf(state.an + "-") == -1) {
              parseInt(item.ls) + parseInt(item.ws) > 0
                ? (showStateWise = true)
                : null;
            } else {
              parseInt(item.ls) + parseInt(item.ws) > 0
                ? (showPowerStateWise = true)
                : null;
            }
          } else {
            parseInt(item.ls) + parseInt(item.ws) > 0
              ? (showStateWise = true)
              : null;
          }
        });
    });

  return (
    <React.Fragment>
      {showPowerState && showPowerStateWise ? (
        <div className="state_wise_widget power_state_wise inner_box_widget">
          <h2 className="election_h2">{_electionConfig.powerStates}</h2>
          <div className="scroll_content">
            {states &&
              states.length > 0 &&
              states.map((state, i) => {
                if (constants.POWER_STATES.indexOf(state.an + "-") > -1) {
                  let _data = coustomizeFeedData(data, type, state, sourceType);
                  //Its a hack put for showing total at top of list
                  //as its bottom li now
                  //so adding +1 at last
                  let _ttl_win_lead = 0;
                  let _oth_win_lead = 0;
                  return (
                    <ul key={"power_state" + i} className="state-column">
                      {_data && _data.items && _data.items.length > 0
                        ? _data.items.map((item, i) => {
                            //calculate total win+lead
                            _ttl_win_lead +=
                              parseInt(item.ws) + parseInt(item.ls);
                            let _backgroundColor = _data.master[
                              _data.master_index[item.nm]
                            ].cc
                              ? _data.master[_data.master_index[item.nm]].cc
                              : "rgb(128, 128, 128, .2)";
                            // let _style = {backgroundColor : _backgroundColor};
                            if (
                              i <
                              constants.MAXITEMS - constants.MAXITEMS_LIMITER
                            ) {
                              return (
                                <li
                                  key={"stateNode" + i}
                                  style={{ backgroundColor: _backgroundColor }}
                                >
                                  <span>{item.nm.split(" ")[0]}</span>{" "}
                                  <span>
                                    {parseInt(item.ls) + parseInt(item.ws)}
                                  </span>
                                </li>
                              );
                            } else if (i == _data.items.length - 1) {
                              _oth_win_lead +=
                                parseInt(item.ws) + parseInt(item.ls);
                              // _ttl_win_lead += _oth_win_lead;
                              return (
                                <li
                                  key={"stateNode" + i}
                                  style={{ backgroundColor: _backgroundColor }}
                                >
                                  <span>{item.nm.slice(0, 3)}</span>{" "}
                                  <span>{_oth_win_lead}</span>
                                </li>
                              );
                            } else {
                              //calculate total other win+lead
                              _oth_win_lead +=
                                parseInt(item.ws) + parseInt(item.ls);
                              return null;
                            }
                          })
                        : // :
                          // (i < constants.MAXITEMS - 1) ?
                          // [1,2,3,4,5].map((item,i)=>{return <li key={"fakeCard"+i} style={{backgroundColor:'#f9f9f9',height:'40px'}}></li>})
                          null}
                      {state.an && _ttl_win_lead > -1 ? (
                        <li className="stateName">
                          <span>{state.nm}</span>{" "}
                          <span>
                            {_ttl_win_lead}/{state.ttl_seats}
                          </span>
                        </li>
                      ) : null}
                    </ul>
                  );
                } else {
                  return null;
                }
              })}
          </div>
        </div>
      ) : null}
      {showStateWise ? (
        <div className="state_wise_widget inner_box_widget">
          <h2 className="election_h2">{_electionConfig.stateWise}</h2>
          <div className="scroll_content">
            {states &&
              states.length > 0 &&
              states.map((state, i) => {
                if (
                  !showPowerState ||
                  constants.POWER_STATES.indexOf(state.an + "-") == -1
                ) {
                  let _data = coustomizeFeedData(data, type, state, sourceType);
                  let _ttl_win_lead = 0;
                  let _oth_win_lead = 0;
                  return (
                    <ul key={"state" + i} className="state-column">
                      {_data && _data.items && _data.items.length > 0
                        ? _data.items.map((item, i) => {
                            //calculate total win+lead
                            _ttl_win_lead +=
                              parseInt(item.ws) + parseInt(item.ls);
                            let _backgroundColor = _data.master[
                              _data.master_index[item.nm]
                            ].cc
                              ? _data.master[_data.master_index[item.nm]].cc
                              : "rgb(128, 128, 128, .2)";
                            // let _style = {backgroundColor : _backgroundColor};
                            if (
                              i <
                              constants.MAXITEMS - constants.MAXITEMS_LIMITER
                            ) {
                              return (
                                <li
                                  key={"stateNode" + i}
                                  style={{ backgroundColor: _backgroundColor }}
                                >
                                  <span>{item.nm.split(" ")[0]}</span>{" "}
                                  <span>
                                    {parseInt(item.ls) + parseInt(item.ws)}
                                  </span>
                                </li>
                              );
                            } else if (i == _data.items.length - 1) {
                              _oth_win_lead +=
                                parseInt(item.ws) + parseInt(item.ls);
                              // _ttl_win_lead += 1;
                              return (
                                <li
                                  key={"stateNode" + i}
                                  style={{ backgroundColor: _backgroundColor }}
                                >
                                  <span>{item.nm.split(" ")[0]}</span>{" "}
                                  <span>{_oth_win_lead}</span>
                                </li>
                              );
                            } else {
                              //calculate total other win+lead
                              _oth_win_lead +=
                                parseInt(item.ws) + parseInt(item.ls);
                              return null;
                            }
                          })
                        : // :
                          // (i < constants.MAXITEMS - 1) ?
                          // [1,2,3,4,5].map((item,i)=>{return <li key={"fakeCard_"+i} style={{backgroundColor:'#f9f9f9',height:'40px'}}></li>})
                          null}
                      {state.an && _ttl_win_lead > -1 ? (
                        <li className="stateName">
                          <span>{state.an}</span>{" "}
                          <span>
                            {_ttl_win_lead}/{state.ttl_seats}
                          </span>
                        </li>
                      ) : null}
                    </ul>
                  );
                } else {
                  return null;
                }
              })}
          </div>
          {
            <div className="wdt_btn">
              <AnchorLink
                href={`/elections/lok-sabha/states`}
                target="_blank"
                className="btn_result"
              >
                {_electionConfig.stateWise}
              </AnchorLink>
              <AnchorLink
                href={`/elections/lok-sabha-constituencies`}
                target="_blank"
                className="btn_result"
              >
                {_electionConfig.allConstituencies}
              </AnchorLink>
            </div>
          }
        </div>
      ) : null}
    </React.Fragment>
  );
};

export default StateWiseWidget;
