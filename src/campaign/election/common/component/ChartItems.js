import React from "react";

import drawPieChart from "../../../../modules/charts/piechart";

const ChartItems = (items, _index, total, state, seats) => {
  const elemid = `elem${Math.floor(Math.random() * 1000000)}`;
  const chartObj = {
    data: {},
    colors: [],
    radius: Math.min(100, window.innerWidth / 2),
    canvasid: elemid,
  };
  items.forEach(item => {
    // chartObj.data[item.an] = parseInt(item.ws);
    chartObj.data[item.an] = parseInt(item.ws, 10) + parseInt(item.ls, 10);
    chartObj.colors.push(item.cc);
  });
  chartObj.totalValue = total;

  return (items.length > 0 && parseInt(items[0].ws, 10) + parseInt(items[0].ls, 10)) > 0 ? (
    <div className="pie-chart" data-ele="piechart_mp_0" key={_index}>
      <div className="el_graphdim chart highcharts-container">
        {/* <div id={elemid}></div> */}
        <div
          id={elemid}
          ref={() => {
            drawPieChart(chartObj);
            return false;
          }}
        ></div>
        {/* {setTimeout(() => { drawPieChart(chartObj); return false }, 200)} */}
        <div visibility="visible" className="chart-detail">
          <div className="charttxt">
            <div className="chartInnerText">
              {seats ? (
                <div className="seatcounted">{seats[_index].seatCounted}</div>
              ) : (
                <div className="seatcounted"></div>
              )}
              <div className="totalseat">{total}</div>
              <div className="seatstatus">Exit Polls</div>
            </div>
          </div>
          <div className="charttitle">
            {state && state !== "" ? (
              <div className="title">
                <span>{state.toLocaleUpperCase()}</span>
              </div>
            ) : null}
            {total ? <div className="subtitle">Majority Mark: {Math.floor(total / 2) + 1}</div> : null}
          </div>
          <div className="titlebackground"></div>
        </div>
      </div>
      <div className="legend">
        <ul>
          {items.map((item, index) => {
            return parseInt(item.ws, 10) + parseInt(item.ls, 10) > 0 ? (
              <li key={index.toString()}>
                <div className="trend-text">{item.an}</div>
                <div className="trend-color" style={{ background: `${item.cc}` }}></div>
              </li>
            ) : null;
          })}
        </ul>
      </div>
      <div className="clearfix powerbycont">
        <div className="el_powerdby"> Source: {items[0].src}</div>
      </div>
    </div>
  ) : null;
};

export default ChartItems;
