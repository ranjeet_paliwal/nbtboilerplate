import React, { Component } from "react";
import "./../css/TabsWidget.scss";
import { electionConfig } from "../../../election/utils/config";
// import AnchorLink from '../../../../components/common/AnchorLink';
const _electionConfig = electionConfig[process.env.SITE];

const TabsWidget = props => {
  let {
    items,
    onTabClick,
    tabtype,
    datatype,
    isSelectBox,
    _electionConfig
  } = props;
  return (
    <div className={"tabs_widget " + (tabtype ? tabtype : "tabs_big")}>
      {!tabtype && !items ? (
        <ul>
          <li
            tab-type={tabtype}
            data-name="alliancewise"
            onClick={onTabClick ? onTabClick.bind(this) : null}
            className="active"
          >
            <a href="javascript:void(0)">
              {_electionConfig && _electionConfig.allianceViewTxt
                ? _electionConfig.allianceViewTxt
                : "Alliance View"}
            </a>
          </li>
          <li
            tab-type={tabtype}
            data-name="partywise"
            onClick={onTabClick ? onTabClick.bind(this) : null}
          >
            <a href="javascript:void(0)">
              {_electionConfig && _electionConfig.partyViewTxt
                ? _electionConfig.partyViewTxt
                : "Party View"}
            </a>
          </li>
        </ul>
      ) : items && items.length > 0 ? (
        <React.Fragment>
          {isSelectBox ? (
            <input type="checkbox" className="toggleCheckBox" />
          ) : null}
          <ul
            className={isSelectBox && items.length > 1 ? "tabs_select_box" : ""}
          >
            {items.map((item, i) => {
              let _key =
                "tabkey_" + (isSelectBox ? Math.random() * 1000000 : i);
              return (
                <li
                  className={
                    typeof item.active == "undefined"
                      ? i == 0
                        ? "active"
                        : ""
                      : item.active
                      ? "active"
                      : ""
                  }
                  key={_key}
                  tab-type={tabtype}
                  data-name={item.an || item.nm}
                  data-type={datatype}
                  onClick={onTabClick ? onTabClick.bind(this) : null}
                >
                  <a href="javascript:void(0)">
                    {item.an || item.nm.split("_")[0]}
                  </a>
                </li>
              );
            })}
          </ul>
        </React.Fragment>
      ) : null}
    </div>
  );
};

export default TabsWidget;
