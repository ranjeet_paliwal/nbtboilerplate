import React from "react";

import drawPieChart from "./../../../../modules/charts/piechart";
import { _isCSR, _getStaticConfig } from "../../../../utils/util";
import AnchorLink from "../../../../components/common/AnchorLink";
import { electionConfig } from "../../../../campaign/election/utils/config";

const _electionConfig = electionConfig[process.env.SITE];

const siteConfig = _getStaticConfig();

const PieTableItems = (items, _index, total, state, anchorlink, resultLbl) => {
  let _state =
    state == "madhya pradesh" || state == "madhya-pradesh"
      ? "mp"
      : state == "arunachal pradesh" || state == "arunachal-pradesh"
      ? "ar"
      : state == "andhra pradesh" || state == "andhra-pradesh"
      ? "ap"
      : state;
  let elemid = "elem" + _state;
  let chartObj = {
    data: {},
    colors: [],
    radius: 100,
    canvasid: elemid,
    total_leads: 0,
    total_wins: 0,
    totalValue: parseInt(total),
  };
  items = items[0];
  items.map((item, index) => {
    // item = items[index];
    chartObj.data[item.an] = parseInt(item.ws) + parseInt(item.ls);
    chartObj.colors.push(item.cc);
    chartObj.total_leads += parseInt(item.ls);
    chartObj.total_wins += parseInt(item.ws);
  });

  let mapStateMSID = {
    maharashtra: "71452334",
    haryana: "71452456",
  };
  let mapMSID = {};

  mapMSID = _electionConfig.mapResultMSID;

  return (items.length > 0 && parseInt(items[0].ws) + parseInt(items[0].ls)) > 0 ? (
    <div className="pietable_container" key={"container_" + _state}>
      <h2 className="tbl_txt">
        {anchorlink ? (
          <u>
            <a href={`/elections/assembly-elections/${state}/results/${mapMSID[state]}.cms`}>
              {" "}
              {state.split("-").join(" ")}
            </a>
          </u>
        ) : (
          <span>{state}</span>
        )}
      </h2>
      <input type="checkbox" id={"toggleview_" + _state} className="toggleview" style={{ display: "none" }} />
      <label className="el_resulttable" htmlFor={"toggleview_" + _state}>
        <i className="grid_view" data-ele="table-view" />
      </label>
      <div className="electionWidget_con">
        <div className="pie-chart eresult_pie result_inner" key={"chart" + _state}>
          {_isCSR() ? (
            <React.Fragment>
              <div className="el_graphdim chart highcharts-container">
                <div id={elemid} ref={() => drawPieChart(chartObj)}></div>
                <div visibility="visible" className="chart-detail">
                  <div className="charttxt">
                    <div className="chartInnerText">
                      <div className="seatcounted">{chartObj.total_leads + chartObj.total_wins}</div>
                      <div className="totalseat">{total}</div>
                      <div className="seatstatus">{resultLbl}</div>
                    </div>
                  </div>
                  <div className="charttitle">
                    {_state && _state != "" ? (
                      <div className="title">
                        <span>{_state.toLocaleUpperCase()}</span>
                      </div>
                    ) : null}
                    <div className="subtitle">Majority Mark: {Math.floor(total / 2) + 1}</div>
                  </div>
                  <div className="titlebackground"></div>
                </div>
              </div>
              <div className="legend">
                <ul>
                  {items.map((item, index) => {
                    return parseInt(item.ws) + parseInt(item.ls) > 0 ? (
                      <li key={index}>
                        <div className="trend-text">{item.an}</div>
                        <div className="trend-color" style={{ background: `${item.cc}` }}></div>
                      </li>
                    ) : null;
                  })}
                </ul>
              </div>
            </React.Fragment>
          ) : null}
        </div>

        <div className="result_inner eresult_table" key={"table" + _index}>
          <table className="result-tbl">
            <thead>
              <tr className="tbl_heading">
                <th className="tbl_col  first">PARTY</th>
                {/*<th className="tbl_col  first">LEADS</th>
                    <th className="tbl_col  first">WINS</th>*/}
                <th className="tbl_col  tac">LEADS</th>
                <th className="tbl_col  tac">WINS</th>
                <th className="tbl_col  tac">TOTAL</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, index) => {
                // item = items[index];
                return (
                  <tr className="tbl_row" key={index}>
                    <td className="tbl_col col1 party_logo">
                      {item.an}
                      <img src={`${siteConfig.weburl}/photo/${item.lg}.cms`} />
                    </td>
                    {/*<td className="tbl_col tac col1 ">{item.ls}</td>
                                <td className="tbl_col tac col1 ">{item.ws}</td>*/}
                    <td className="tbl_col tac col1 ">{parseInt(item.ls)}</td>
                    <td className="tbl_col tac col1 ">{parseInt(item.ws)}</td>
                    <td className="tbl_col tac col1 ">{parseInt(item.ws) + parseInt(item.ls)}</td>
                  </tr>
                );
              })}
              <tr className="tbl_row">
                <td className="tbl_col col1 tot" colSpan="3">
                  Total
                </td>
                {/*<td className="tbl_col tac col1 ">{chartObj.total_leads}</td>
                    <td className="tbl_col tac col1 ">{chartObj.total_wins}</td>*/}
                <td className="tbl_col tac col1 ">
                  {chartObj.total_leads + chartObj.total_wins}/{total}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  ) : null;
};

export default PieTableItems;
