import React, { Component } from "react";
import "../../../../components/common/css/electionCube.scss";
// import './css/electionCube.scss';
import { _getStaticConfig, _isCSR, _deferredDeeplink, _sessionStorage } from "../../../../utils/util";
const siteConfig = _getStaticConfig();
import Ads_module from "./../../../../components/lib/ads/index";
import { electionConfig } from "../../../../campaign/election/utils/config";
import { coustomizeFeedData } from "../../../../campaign/election/utils/util";
import { AnalyticsGA } from "./../../../../components/lib/analytics/index";
const timeInterval = 5000;
const _electionConfig = electionConfig[process.env.SITE];

export const ExitPollRotateCube = props => {
  if (_isCSR()) {
    let cube = document.querySelector(".flipbox-box.flipbox-horizontal");

    if (cube && !cube.getAttribute("isRotateAttached")) {
      // setInterval(() => {
      //     Ads_module.refreshAds(['special1']);
      // }, timeInterval * 4);
      // setInterval(() => {
      //     Ads_module.refreshAds(['special2']);
      // }, timeInterval * 5);

      AnalyticsGA.pageview("/elections/lok-sabha-elections/exitpolls/69253509.cms?cube=exitpoll");
      // https://navbharattimes.indiatimes.com

      Ads_module.render({});

      let currentVal = 0;
      setInterval(() => {
        cube.style.transform = "rotateY(" + currentVal + "deg)";
        currentVal -= 90;
      }, timeInterval);

      cube.setAttribute("isRotateAttached", true);
    }
  }
};

export const ExitPollElectionCube = props => {
  // let { isFetching } = props;
  let { data, isFetching } = props.data;

  if (data && !isFetching) {
    let sourceArr = ["POLL OF POLLS", "Times Now-VMR"];
    let pollData = coustomizeFeedData(data, "alliancewise", data.yrdata[0], "POLL OF POLLS", true);
    let timesData = coustomizeFeedData(data, "alliancewise", data.yrdata[0], "Times Now-VMR", true);

    pollData._ttl_win_lead = 0;
    pollData._oth_win_lead = 0;

    timesData._ttl_win_lead = 0;
    timesData._oth_win_lead = 0;

    const hideCube = () => {
      if (document.querySelector(".election_cube")) {
        document.querySelector(".election_cube").className += " hide";
        _sessionStorage().set("ExitPollCube", false);
      }
    };

    return (
      <div className="election_cube">
        <div id="content">
          <span className="close_icon" onClick={hideCube}></span>
          <div id="box1" className="box-cube flipbox-wrapper">
            <div className="flipbox-box flipbox-horizontal">
              <div className="flipbox-side flipbox-front">
                <div className="top_ad">
                  {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                  <div
                    data-adtype="headAd1"
                    className="ad1 special3"
                    data-id="div-gpt-ad-1540206148700-20"
                    data-name={siteConfig.ads.dfpads.cubeheadad}
                    // //data-mlb="[[195, 30]]"
                    data-size="[[195, 30]]"
                  />
                </div>
                <a
                  href={_deferredDeeplink(
                    "campaign",
                    siteConfig.appdeeplink,
                    null,
                    siteConfig.mweburl +
                      "/elections/lok-sabha-elections/exitpolls/" +
                      _electionConfig.loksabhaResultsMSID +
                      ".cms?frmapp=yes",
                  )}
                  target="_blank"
                  className="bottom_cube_content"
                >
                  <div className="resultTable-cube">
                    <div className="election_result_table">
                      <div className="ele_caption clearfix">
                        {/* <h3>Live</h3> */}
                        <h6>Lok Sabha {pollData && pollData.items ? pollData.year : 2013}</h6>
                      </div>
                      <div className="result_table">
                        <ul>
                          {pollData &&
                            pollData.items &&
                            pollData.items.length &&
                            pollData.items.map((item, i) => {
                              //calculate total win+lead
                              pollData._ttl_win_lead += parseInt(item.ws) + parseInt(item.ls);
                              let _style = {
                                backgroundColor: pollData.master[pollData.master_index[item.nm]].cc,
                              };
                              return (
                                <li style={_style}>
                                  {item.nm} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                                </li>
                              );
                            })}
                        </ul>
                        <ul className="table_tot">
                          <li>
                            Seats {pollData && pollData.items ? pollData._ttl_win_lead : 0}
                            /543
                          </li>
                          <li>
                            Majority <strong>272</strong>
                          </li>
                        </ul>
                        <p>SOURCE : {sourceArr[0]}</p>
                        <button className="open-in-app">Open in App</button>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="flipbox-side flipbox-back" style={{ transform: "translateZ(-195px) rotateY(180deg)" }}>
                <div className="top_ad">
                  {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                  <div
                    data-adtype="headAd2"
                    className="ad1 special3"
                    data-id="div-gpt-ad-1540206148700-21"
                    data-name={siteConfig.ads.dfpads.cubeheadad}
                    // //data-mlb="[[195, 30]]"
                    data-size="[[195, 30]]"
                  />
                </div>
                <a
                  href={_deferredDeeplink(
                    "campaign",
                    siteConfig.appdeeplink,
                    null,
                    siteConfig.mweburl +
                      "/elections/lok-sabha-elections/exitpolls/" +
                      _electionConfig.loksabhaResultsMSID +
                      ".cms?frmapp=yes",
                  )}
                  target="_blank"
                  className="bottom_cube_content"
                >
                  <div className="resultTable-cube">
                    <div className="election_result_table">
                      <div className="ele_caption clearfix">
                        {/* <h3>Live</h3> */}
                        <h6>Lok Sabha 2019</h6>
                      </div>
                      <div className="result_table">
                        <ul>
                          {timesData &&
                            timesData.items &&
                            timesData.items.length &&
                            timesData.items.map((item, i) => {
                              //calculate total win+lead
                              timesData._ttl_win_lead += parseInt(item.ws) + parseInt(item.ls);
                              let _style = {
                                backgroundColor: timesData.master[timesData.master_index[item.nm]].cc,
                              };
                              return (
                                <li style={_style}>
                                  {item.nm} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                                </li>
                              );
                            })}
                        </ul>
                        <ul className="table_tot">
                          <li>
                            Seats {timesData && timesData.items ? timesData._ttl_win_lead : 0}
                            /543
                          </li>
                          <li>
                            Majority <strong>: 272</strong>
                          </li>
                        </ul>
                        <p>SOURCE : {sourceArr[1]}</p>
                        <button className="open-in-app">Open in App</button>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="flipbox-side flipbox-left" style={{ transform: "rotateY(270deg) translateX(-195px)" }}>
                <div className="top_ad">
                  {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                  <div
                    data-adtype="headAd3"
                    className="ad1 special3"
                    data-id="div-gpt-ad-1540206148700-22"
                    data-name={siteConfig.ads.dfpads.cubeheadad}
                    // //data-mlb="[[195, 30]]"
                    data-size="[[195, 30]]"
                  />
                </div>
                <div
                  data-adtype="special1"
                  className="ad1 special"
                  data-id="div-gpt-ad-1540206148700-10"
                  data-name={siteConfig.ads.dfpads.cubead}
                  // //data-mlb="[[320, 100], [320, 50]]"
                  data-size="[[195, 85]]"
                />
                <a
                  href={_deferredDeeplink(
                    "campaign",
                    siteConfig.appdeeplink,
                    null,
                    siteConfig.mweburl +
                      "/elections/lok-sabha-elections/exitpolls/" +
                      _electionConfig.loksabhaResultsMSID +
                      ".cms?frmapp=yes",
                  )}
                  target="_blank"
                  className="open-app-with-ad"
                >
                  <button className="open-in-app">Open in App</button>
                </a>
              </div>
              <div className="flipbox-side flipbox-right" style={{ transform: "rotateY(-270deg) translateX(195px)" }}>
                <div className="top_ad">
                  {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                  <div
                    data-adtype="headAd4"
                    className="ad1 special3"
                    data-id="div-gpt-ad-1540206148700-23"
                    data-name={siteConfig.ads.dfpads.cubeheadad}
                    // //data-mlb="[[195, 30]]"
                    data-size="[[195, 30]]"
                  />
                </div>
                <div
                  data-adtype="special2"
                  className="ad1 special"
                  data-id="div-gpt-ad-1540206148700-11"
                  data-name={siteConfig.ads.dfpads.cubead}
                  // //data-mlb="[[320, 100], [320, 50]]"
                  data-size="[[195, 85]]"
                />
                <a
                  href={_deferredDeeplink(
                    "campaign",
                    siteConfig.appdeeplink,
                    null,
                    siteConfig.mweburl +
                      "/elections/lok-sabha-elections/exitpolls/" +
                      _electionConfig.loksabhaResultsMSID +
                      ".cms?frmapp=yes",
                  )}
                  target="_blank"
                  className="open-app-with-ad"
                >
                  <button className="open-in-app">Open in App</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default { ExitPollElectionCube, ExitPollRotateCube };
