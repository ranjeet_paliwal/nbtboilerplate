import React, { Component } from "react";
import "./../css/Election.scss";
import AnchorLink from "../../../../components/common/AnchorLink";
import NewsListCard from "../../../../components/common/NewsListCard";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";

const HighlightsWidget = props => {
  //let {items} = this.props;
  let { data, _electionConfig } = props;
  // let item = {
  //     hl: "माया के सामने जूते उतारे? अजित ने तोड़ी चुप्पी ",
  //     imageid: "69335674",
  //     imgsize: "175695",
  //     lu: "",
  //     id: "69335623",
  //     tn: "news",
  //     dl: "",
  //     mstype: "2",
  //     seolocation: "elections/lok-sabha-elections/news/rld-chief-ajit-singh-says-mahagathbandhan-will-have-a-role-in-deciding-next-pm-of-india"
  //     }

  return data ? (
    <div className="highlights_widget inner_box_widget">
      <h2 className="election_h2">
        {_electionConfig && _electionConfig.highlights
          ? _electionConfig.highlights
          : "Highlights"}
      </h2>
      <ul className="nbt-list">
        {data &&
          data.length > 0 &&
          data.map((item, index) => {
            return (
              item.tn &&
              item.tn != "ad" &&
              index < 4 && (
                <ErrorBoundary key={index}>
                  <NewsListCard
                    pagetype={"articlelist"}
                    item={item}
                    leadpost={index == 0 ? true : false}
                  />
                </ErrorBoundary>
              )
            );
          })}
        {/* <NewsListCard pagetype={"articlelist"} item={item} leadpost={true} />
               <NewsListCard pagetype={"articlelist"} item={item} leadpost={false} /> */}
      </ul>
    </div>
  ) : null;
};

export default HighlightsWidget;
