import React, { Component } from "react";
import "./../css/ResultTableWidget.scss";
import { constants } from "../../utils/constants";
import AnchorLink from "../../../../components/common/AnchorLink";

let svgElectionWidget = "";

const ResultTableWidget = props => {
  let {
    data,
    type,
    isTabularLayout,
    pagetype,
    _disclaimer,
    anchorLink,
    _electionConfig
  } = props;
  let { items, master, master_index } = data;
  data.ttl_seats = 543;
  let _ttl_win_lead = 0;
  let _oth_win_lead = 0;
  let _oth_prev_win_lead = 0;

  return (
    <React.Fragment>
      {isTabularLayout ? (
        <div className="result_table_widget">
          <table cellPadding="0" cellSpacing="0">
            <tbody>
              <tr>
                <td></td>
                <td>
                  2019 <br />
                  Results
                </td>
                <td>
                  Seats in <br />
                  2014
                </td>
                <td>Change in Seats</td>
              </tr>
              {items &&
                items.length > 0 &&
                items.map((item, i) => {
                  //calculate total win+lead
                  _ttl_win_lead += parseInt(item.ws) + parseInt(item.ls);
                  let _style = {
                    backgroundColor: master[master_index[item.nm]].cc
                  };

                  if (i < constants.MAXITEMS - 1) {
                    return (
                      <tr className="dataCell">
                        <td style={_style}>{item.nm}</td>
                        <td style={_style}>
                          {parseInt(item.ls) + parseInt(item.ws)}
                        </td>
                        <td style={_style}>{item.pws}</td>
                        <td style={_style}>
                          {parseInt(item.ls) + parseInt(item.ws) - item.pws}
                        </td>
                      </tr>
                    );
                  } else if (i == items.length - 1) {
                    _oth_prev_win_lead += item.pws;
                    _oth_win_lead += parseInt(item.ws) + parseInt(item.ls);
                    return (
                      <tr className="dataCell">
                        <td style={_style}>{item.nm}</td>
                        <td style={_style}>{_oth_win_lead}</td>
                        <td style={_style}>{_oth_prev_win_lead}</td>
                        <td style={_style}>
                          {_oth_win_lead - _oth_prev_win_lead}
                        </td>
                      </tr>
                    );
                  } else {
                    //calculate prev other win+lead
                    _oth_prev_win_lead += item.pws;
                    //calculate other win+lead
                    _oth_win_lead += parseInt(item.ws) + parseInt(item.ls);
                    return null;
                  }
                })}
              <tr className="total">
                <td>
                  <span>{_electionConfig.totalSeats}</span> {data.ttl_seats}
                </td>
                <td></td>
                <td colSpan="2">
                  <span>{_electionConfig.majorityMark}</span>{" "}
                  {Math.floor(data.ttl_seats / 2) + 1}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      ) : (
        <div>
          {data.ttl_seats ? (
            <div className="wdt_majority">
              <span>
                {_electionConfig.totalSeats}
                <strong>{data.ttl_seats}</strong>
              </span>
              <span>
                {_electionConfig.majorityMark}
                <strong>{Math.floor(data.ttl_seats / 2) + 1}</strong>
              </span>
            </div>
          ) : null}
          <div className="horizontal-table">
            <ul className="state-column">
              {items &&
                items.length &&
                items.map((item, i) => {
                  //calculate total win+lead
                  _ttl_win_lead += item.ws + item.ls;
                  let _style = {
                    backgroundColor: item.cc
                      ? item.cc
                      : master[master_index[item.nm]].cc
                      ? master[master_index[item.nm]].cc
                      : "rgba(128,128,128,.2)"
                  };
                  if (i < constants.MAXITEMS - 1) {
                    return (
                      <li key={"stateNode" + i} style={_style}>
                        <span>{item.an ? item.an : item.nm.split(" ")[0]}</span>{" "}
                        <strong>{item.ls + item.ws}</strong>
                      </li>
                    );
                  } else if (i == items.length - 1) {
                    _oth_win_lead += item.ws + item.ls;
                    return (
                      <li key={"stateNode" + i} style={_style}>
                        <span>{item.an ? item.an : item.nm.split(" ")[0]}</span>{" "}
                        <strong>{_oth_win_lead}</strong>
                      </li>
                    );
                  } else {
                    //calculate total other win+lead
                    _oth_win_lead += parseInt(item.ws) + parseInt(item.ls);
                    return null;
                  }
                })}
            </ul>
          </div>
        </div>
      )}
      <div className="wdt_btn">
        {_electionConfig &&
        _electionConfig["result_liveblog_link"] &&
        pagetype != "home" && pagetype != "liveblog" ? (
          <AnchorLink
            href={_electionConfig["result_liveblog_link"]}
            className="btn_result live_updates"
          >
            <span> {_electionConfig.liveUpdatesTxt} </span>
          </AnchorLink>
        ) : null}
        {pagetype == "home" ? (
          <AnchorLink
            href={
              anchorLink && anchorLink != ""
                ? anchorLink
                : `/elections/lok-sabha-elections/parties`
            }
            className="btn_result"
          >
            {_electionConfig && _electionConfig.partyWise
              ? _electionConfig.partyWise
              : "Party-Wise"}
          </AnchorLink>
        ) : null}
        {pagetype != "home" && pagetype != "liveblog" ? (
          <AnchorLink
            href={
              anchorLink && anchorLink != ""
                ? anchorLink
                : `/elections/constituency-map`
            }
            className="btn_result"
          >
            {" "}
            {_electionConfig.viewMap}{" "}
          </AnchorLink>
        ) : null}
        {pagetype == "home" || pagetype == "liveblog" ? (
          <AnchorLink
            href={
              anchorLink && anchorLink != ""
                ? anchorLink
                : `/elections/lok-sabha-elections/results/${_electionConfig.loksabhaResultsMSID}.cms`
            }
            className="btn_result"
          >
            {_electionConfig && _electionConfig.viewDetailsTxt
              ? _electionConfig.viewDetailsTxt
              : "View Details"}
          </AnchorLink>
        ) : null}
      </div>
      <div className="wdt_note" style={{ textAlign: "center" }}>
        Source: C-Voter
      </div>
      {_disclaimer && _disclaimer != "" ? (
        <div className="wdt_note">
          <strong>Note:</strong>
          <span dangerouslySetInnerHTML={{ __html: _disclaimer }} />
        </div>
      ) : null}
    </React.Fragment>
  );
};

export default ResultTableWidget;
