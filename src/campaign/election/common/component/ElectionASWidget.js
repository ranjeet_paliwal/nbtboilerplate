import React from "react";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";

import styles from "./../../../election/common/css/ElectionASwidget.scss";
// import AnchorLink from '../../../../components/common/AnchorLink';
import { _isCSR } from "../../../../utils/util";
// import { _getStaticConfig } from './../../../../utils/util';
// const siteConfig = _getStaticConfig();

const ElectionASWidget = props => {
  let { data, href } = props;
  let _data = {};
  let frmAppLink = "";
  if (_isCSR() && location.search.indexOf("frmapp=yes")) {
    frmAppLink = "?frmapp=yes";
  }
  if (data && href && href.toLocaleLowerCase().indexOf("rajasthan") > -1) {
    _data = data["Rajasthan"];
    _data["stateName"] = "राजस्थान";
    _data[
      "link"
    ] = `/elections/assembly-elections/rajasthan/results${frmAppLink}`;
  } else if (data && href && href.toLocaleLowerCase().indexOf("madhya") > -1) {
    _data = data["Madhya_Pradesh"];
    _data["stateName"] = "मध्यप्रदेश";
    _data[
      "link"
    ] = `/elections/assembly-elections/madhya-pradesh/results${frmAppLink}`;
  } else if (
    data &&
    href &&
    href.toLocaleLowerCase().indexOf("telangana") > -1
  ) {
    _data = data["Telangana"];
    _data["stateName"] = "तेलंगाना";
    _data[
      "link"
    ] = `/elections/assembly-elections/telangana/results${frmAppLink}`;
  } else if (
    data &&
    href &&
    href.toLocaleLowerCase().indexOf("chhattisgarh") > -1
  ) {
    _data = data["Chhattisgarh"];
    _data["stateName"] = "छत्तीसगढ़";
    _data[
      "link"
    ] = `/elections/assembly-elections/chhattisgarh/results${frmAppLink}`;
  } else if (data && href && href.toLocaleLowerCase().indexOf("mizoram") > -1) {
    _data = data["Mizoram"];
    _data["stateName"] = "मिजोरम";
    _data[
      "link"
    ] = `/elections/assembly-elections/mizoram/results${frmAppLink}`;
  }

  if (_data && _data.al_rslt) {
    _data.total_leads = 0;
    _data.total_wins = 0;
    _data.al_rslt.map(item => {
      _data.total_leads += parseInt(item.ls);
      _data.total_wins += parseInt(item.ws);

      _data.total_lead_wins = _data.total_leads + _data.total_wins;
    });
  }

  return (
    <ErrorBoundary>
      {data && _data.al_rslt ? (
        <a className="election_result_table resultTable" href={_data["link"]}>
          <div className="election_caption">
            <h3>Live</h3>
            <h6>चुनाव नतीजे 2018 {_data.stateName}</h6>
            <p>
              {"बहुमत"} <strong>{parseInt(_data.ttl_seat) / 2 + 1}</strong>{" "}
            </p>
          </div>
          <div className="result_table">
            <table className="result-tbl">
              <tbody>
                <tr className="tbl_row">
                  <td className="tbl_col col1 "></td>
                  <td className="tbl_col col1">{"बढ़त+ जीत"}</td>
                </tr>
                {_data.al_rslt.map((item, index) => {
                  return (
                    <tr
                      className="tbl_row data"
                      key={"row" + index}
                      style={{
                        background: `linear-gradient(to right, ${item.cc} , ${item.cc})`
                      }}
                    >
                      <td className="tbl_col col1 party">{item.an}</td>
                      <td className="tbl_col col1 ">
                        {parseInt(item.ls) + parseInt(item.ws)}
                      </td>
                    </tr>
                  );
                })}
                <tr className="tbl_row">
                  <td className="tbl_col col1 ">{"कुल"}</td>
                  <td className="tbl_col col1">
                    {_data.total_lead_wins} / {_data.ttl_seat}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {/*  (_data['stateName'] == 'राजस्थान') ?
                    <div className="disclaimer">
                    नोट : राजस्‍थान व‍िधानसभा की 200 में 199 सीटों पर हुआ मतदान, बहुमत‍ का आंकड़ा 100 है।  
                    </div>
                    :
                    null
                        */}
        </a>
      ) : null}
    </ErrorBoundary>
  );
};

export default ElectionASWidget;
