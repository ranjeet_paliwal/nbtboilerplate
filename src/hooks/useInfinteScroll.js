import { useState, useEffect } from "react";
import { debounce } from "../utils/util";
const useInfiniteScroll = (callback, loaderRef, offset) => {
  const [isFetching, setIsFetching] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", debounce(handleScroll, 500));
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    callback();
  }, [isFetching]);

  function handleScroll() {
    if (!loaderRef || !loaderRef.current) {
      return;
    }
    const { top } = loaderRef.current.getBoundingClientRect();
    if (top + offset >= 0 && top - offset <= window.innerHeight) {
      setIsFetching(true);
    }
  }

  return [isFetching, setIsFetching];
};

export default useInfiniteScroll;
