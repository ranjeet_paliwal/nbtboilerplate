import {
  FETCH_MINISCHEDULE_REQUEST,
  FETCH_MINISCHEDULE_SUCCESS,
  FETCH_MINISCHEDULE_FAILURE,
} from "./../actions/minischedule";

function custom_sort(a, b) {
  return new Date(a.matchdate_ist).getTime() - new Date(b.matchdate_ist).getTime();
}
function minischedule(
  state = {
    isFetching: false,
    error: false,
    msid: "", //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_MINISCHEDULE_REQUEST:
      state.minischedule = state.minischedule && state.minischedule.schedule ? { ...state.minischedule } : {};
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_MINISCHEDULE_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_MINISCHEDULE_SUCCESS:
      let schedule_data = action.payload[0];
      let seriesid = action.payload[1] && action.payload[1]["pwa_meta"] && action.payload[1]["pwa_meta"].cricketlb;
      let matches;
      if (schedule_data && schedule_data.data && schedule_data.data.matches && schedule_data.data.matches.length > 0) {
        if (seriesid && seriesid != "") {
          matches = schedule_data.data.matches.filter(function(data) {
            //return seriesid == action.seriesid;
            //return true;3156,3168, world cup (2995), warm um world (3204)
            return data.series_Id == seriesid;
          });
        } else {
          matches = schedule_data.data.matches;
        }
        // if (matches && matches.length > 0) {
        //   state.minischedule = { schedule: matches.sort(custom_sort) };
        // }
        state.minischedule = { schedule: matches };
        // state.minischedule = { schedule: schedule_data.data.matches.sort(custom_sort) };
      }
      return {
        ...state.minischedule,
        isFetching: false,
        error: false,
      };

    default:
      return state;
  }
}

export default minischedule;
