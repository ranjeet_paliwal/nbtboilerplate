import React, { Component } from "react";
import { connect } from "react-redux";
//import { Link } from 'react-router'
// import PropTypes from "prop-types";
import { fetchMiniScheduleDataIfNeeded } from "./actions/minischedule";
import "./../../components/common/css/MiniSchedule.scss";
import AnchorLink from "./../../components/common/AnchorLink";
import { _isCSR, _getStaticConfig } from "./../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../utils/cricket_util";

import ImageCard from "./../../components/common/ImageCard/ImageCard";
//import { _getStaticConfig } from 'utils/util';
const siteConfig = _getStaticConfig();

class MiniSchedule extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    let _this = this;
    const { dispatch } = _this.props;
    let params = {
      seriesid: this.props.seriesid,
    };
    if (_isCSR() && this.props.seriesid && this.props.seriesid != "") {
      dispatch(fetchMiniScheduleDataIfNeeded(params)); //Load Schedule Widget
    }
  }
  teamScore(team, item) {
    let inning1 = item && item.inn_team_1 != null && item.inn_team_1 != undefined ? parseInt(item.inn_team_1) : null;
    let inning2 = item && item.inn_team_2 != null && item.inn_team_2 != undefined ? parseInt(item.inn_team_2) : null;
    if (inning1 && team == "A" && inning1 == parseInt(item.teama_Id)) {
      return this.teamScoreFill(1, item);
    } else if (inning2 && team == "A" && inning2 == parseInt(item.teama_Id)) {
      return this.teamScoreFill(2, item);
    } else if (inning1 && team == "B" && inning1 == parseInt(item.teamb_Id)) {
      return this.teamScoreFill(1, item);
    } else if (inning2 && team == "B" && inning2 == parseInt(item.teamb_Id)) {
      return this.teamScoreFill(2, item);
    }
  }
  teamScoreFill(inning, item) {
    if (inning == 1) {
      return (
        <React.Fragment>
          <span className="score">{item.inn_score_1.substring(0, item.inn_score_1.indexOf("("))}</span>
          <span className="over">
            {item.inn_score_1.substring(item.inn_score_1.indexOf("(") + 1, item.inn_score_1.length - 1)}
          </span>
        </React.Fragment>
      );
    } else if (inning == 2) {
      return (
        <React.Fragment>
          <span className="score">{item.inn_score_2.substring(0, item.inn_score_2.indexOf("("))}</span>
          <span className="over">
            {item.inn_score_2.substring(item.inn_score_2.indexOf("(") + 1, item.inn_score_2.length - 1)}
          </span>
        </React.Fragment>
      );
    }
  }
  scheduleWidget(item) {
    let _this = this;
    let date = new Date(item.matchdate_ist);
    return (
      <React.Fragment>
        <div className="match_date_venue">
          {item.matchnumber} - {_getShortMonthName(date.getMonth()) + " " + date.getDate()}
          <span />
        </div>
        <div className="liveScore table">
          <div className="table">
            <div className="teama table_col">
              <b className="country">{item.teama_short}</b>
              <span className="teaminfo">
                <ImageCard msid={_getTeamLogo(item.teama_Id, "square")} size="rectanglethumb" />
                <span className="con_wrap">
                  {item.matchtype.toLowerCase() != "test" ? _this.teamScore("A", item) : null}
                </span>
              </span>
            </div>
            <div className="versus table_col">
              <span>VS</span>
            </div>
            <div className="teamb table_col">
              <b className="country">{item.teamb_short}</b>
              <span className="teaminfo">
                <ImageCard msid={_getTeamLogo(item.teamb_Id, "square")} size="rectanglethumb" />
                <span className="con_wrap">
                  {item.matchtype.toLowerCase() != "test" ? _this.teamScore("B", item) : null}
                </span>
              </span>
            </div>
          </div>
          <div className="match_final_status">
            {item.matchresult && item.matchresult != "" ? (
              item.matchresult
            ) : (
              <span className="match_venue">{item.venue}</span>
            )}
            {item.live == "1" && item.live == "2" && item.live == "3" && item.live == "4" ? (
              <span className="status_live">Live</span>
            ) : null}
          </div>
        </div>
      </React.Fragment>
    );
  }
  render() {
    let _this = this;
    let schedule = this.props.schedule && this.props.schedule.length > 0 ? this.props.schedule : null;
    return schedule && typeof schedule != "null"
      ? schedule.map((item, index) => {
          return (
            <div className="wdt_schedule" key={index}>
              {item.live == 1 || item.matchstatus_Id == "114" ? (
                <AnchorLink
                  data-type="scorecard"
                  style={{ textDecoration: "none" }}
                  hrefData={{
                    override:
                      siteConfig.mweburl +
                      "/sports/cricket/live-score/" +
                      item.teama_short.toLowerCase() +
                      "-vs-" +
                      item.teamb_short.toLowerCase() +
                      "/" +
                      item.matchdate_ist.replace(/\//g, "-") +
                      "/scoreboard/matchid-" +
                      item.matchfile +
                      ".cms",
                  }}
                >
                  {_this.scheduleWidget(item)}
                </AnchorLink>
              ) : (
                _this.scheduleWidget(item)
              )}
            </div>
          );
        })
      : null;
  }
}
function mapStateToProps(state) {
  return {
    ...state.minischedule,
  };
}
export default connect(mapStateToProps)(MiniSchedule);
