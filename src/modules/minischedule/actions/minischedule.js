import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_MINISCHEDULE_REQUEST = "FETCH_MINISCHEDULE_REQUEST";
export const FETCH_MINISCHEDULE_SUCCESS = "FETCH_MINISCHEDULE_SUCCESS";
export const FETCH_MINISCHEDULE_FAILURE = "FETCH_MINISCHEDULE_FAILURE";

function fetchMiniScheduleFailure(error) {
  return {
    type: FETCH_MINISCHEDULE_FAILURE,
    payload: error.message,
  };
}

function fetchMiniScheduleSuccess(data, params) {
  return {
    type: FETCH_MINISCHEDULE_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    // seriesid: params.seriesid,
    payload: data,
  };
}

function fetchMiniScheduleData(state, params, query, strmsid) {
  let msid = strmsid ? strmsid : params.msid;
  let apiUrl = siteConfig.cricket_SIAPI.minischedule;
  let sectionApi = process.env.API_BASEPOINT + "/pwafeeds/pwa_metalist.cms?msid=" + msid + "&feedtype=sjson";

  return dispatch => {
    dispatch({
      type: FETCH_MINISCHEDULE_REQUEST,
    });

    let Promise1 = fetch(apiUrl);
    let Promise2 = fetch(sectionApi);

    return Promise.all([Promise1, Promise2])
      .then(
        data => dispatch(fetchMiniScheduleSuccess(data, params)),
        error => dispatch(fetchMiniScheduleFailure(error)),
      )
      .catch(function(error) {
        dispatch(fetchMiniScheduleFailure(error));
      });
  };
}

function shouldFetchMiniScheduleData(state, params, query) {
  // return true;
  // if (state.minischedule && state.minischedule.schedule == undefined) {
  return true;
  // } else {
  //   return false;
  // }
}

export function fetchMiniScheduleDataIfNeeded(params, query, strmsid) {
  return (dispatch, getState) => {
    if (shouldFetchMiniScheduleData(getState(), params, query)) {
      return dispatch(fetchMiniScheduleData(getState(), params, query, strmsid));
    } else {
      return Promise.resolve([]);
    }
  };
}
