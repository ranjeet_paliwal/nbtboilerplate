import fetch from "utils/fetch/fetch";
export const FETCH_PAYTM_REQUEST = "FETCH_PAYTM_REQUEST";
export const FETCH_PAYTM_SUCCESS = "FETCH_PAYTM_SUCCESS";
export const FETCH_PAYTM_FAILURE = "FETCH_PAYTM_FAILURE";

function fetchPaytmSuccess(data) {
  return {
    type: FETCH_PAYTM_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchPaytmFailure(error) {
  return {
    type: FETCH_PAYTM_FAILURE,
    payload: error.message
  };
}

// For Related Products API Change
function fetchPaytmRelatedData(state, params) {
  // API for Paytm Widgets
  let apiUrl = "https://shop.gadgetsnow.com/api/nbt/paytmSeeAlso.php?keyword=";
  return dispatch => {
    dispatch({ type: FETCH_PAYTM_REQUEST });

    return fetch(apiUrl)
      .then(
        data => dispatch(fetchPaytmSuccess(data)),
        error => dispatch(fetchPaytmFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchPaytmFailure(error));
      });
  };
}

function fetchPaytmData(state, params) {
  // API for Paytm Widgets
  let paytm_sponsoredApi =
    "https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json";
  let apiUrl =
    "https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json";
  return dispatch => {
    dispatch({ type: FETCH_PAYTM_REQUEST });

    return fetch(apiUrl)
      .then(
        data => dispatch(fetchPaytmSuccess(data)),
        error => dispatch(fetchPaytmFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchPaytmFailure(error));
      });
  };
}

function shouldFetchPaytmData(state) {
  if (state.paytmwidget) {
    return true;
  } else {
    return false;
  }
}

export function fetchPaytmDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchPaytmData(getState())) {
      return dispatch(fetchPaytmData(getState()));
    }
  };
}
