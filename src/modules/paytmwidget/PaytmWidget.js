import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPaytmDataIfNeeded } from "./actions/paytmwidget";
import styles from "./../../components/common/css/PaytmWidget.scss";
import { AnalyticsGA } from "../../components/lib/analytics";

export class PaytmWidget extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, params, keyword } = this.props;
    dispatch(fetchPaytmDataIfNeeded(dispatch, params));
  }

  render() {
    const { value, pagename, subsec1 } = this.props;
    return value != undefined &&
      value.product != undefined &&
      process.env.SITE == "nbt" ? (
      <div className="_paytmsponsored">
        <h3>
          NBT पर खरीदें{" "}
          <img
            src="https://navbharattimes.indiatimes.com/photo/64589919.cms"
            width="56"
          />
        </h3>
        <ul>
          {value.product.map((item, index) => {
            let url = item.url ? item.url : "";
            let title = item.titleEn ? item.titleEn : "";
            let price = item.offerPrice ? item.offerPrice : "";
            let prodUrl =
              "https://navbharattimes.indiatimes.com/affiliate_paytm.cms?url=" +
              getURL(url) +
              "&tag=mweb_nbt_sponsored_" +
              pagename +
              "&title=" +
              removespace(title) +
              "&price=" +
              price +
              "&utm_source=gadgetsnow";
            return index < 3 ? (
              <li className="_paytm_card" key={"paytm" + index}>
                <a
                  target="_blank"
                  rel="nofollow"
                  href={prodUrl}
                  onClick={e =>
                    AnalyticsGA.event({
                      category: "Paytm Mall Widget",
                      action: "click",
                      label: pagename
                    })
                  }
                >
                  <div className="wgt-paytm tbl">
                    <div className="items tbl_row">
                      <div className="tbl_col prod_img">
                        <img
                          src={item.imageUrl}
                          alt={item.titleEn}
                          title={item.title}
                        />
                      </div>
                      <div className="des tbl_col">
                        {item.title && item.title != "" ? (
                          <h4 className="heading">{item.title}</h4>
                        ) : null}
                        {item.cashBack && item.cashBack != "" ? (
                          <span className="cashback">
                            {"Cashback on offer price: Rs." + item.cashBack}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  <div className="tbl _paytm_price">
                    <div className="tbl_row">
                      <div className="tbl_col">
                        {item.listPrice &&
                        item.offerPrice &&
                        item.listPrice != item.offerPrice ? (
                          <span className="old-price">
                            &#x20b9; {item.listPrice.split(".")[0]}
                          </span>
                        ) : null}
                        {item.offerPrice && item.offerPrice != "" ? (
                          <span className="main-price">
                            &#x20b9;{" "}
                            {item.offerPrice.split(".")[0] + " (Offer Price)"}
                          </span>
                        ) : null}
                      </div>
                      <div className="tbl_col rgt-btn">
                        <span className="btn-buy">खरीदे</span>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
            ) : null;
          })}
        </ul>
      </div>
    ) : null;
  }
}

const getURL = url => {
  if (url.indexOf("https://paytmmall.com/") > -1) {
    var rawurl = url
      .split("?")[0]
      .split("-pdp")[0]
      .split(".com/")[1];
    return "https://paytm.com/shop/p/" + rawurl;
  } else {
    return url;
  }
};

const removespace = title => {
  if (typeof title != "undefined" && typeof title != "object") {
    title = title
      .trim()
      .replace(/\s+/g, "-")
      .toLowerCase();
    return title;
  } else return;
};

function mapStateToProps(state) {
  return {
    ...state.paytmwidget
  };
}

export default connect(mapStateToProps)(PaytmWidget);
