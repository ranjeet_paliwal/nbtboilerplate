import {
  FETCH_PAYTM_REQUEST,
  FETCH_PAYTM_SUCCESS,
  FETCH_PAYTM_FAILURE
} from "./../actions/paytmwidget";

function paytmwidget(
  state = {
    isFetching: false,
    error: false
  },
  action
) {
  switch (action.type) {
    case FETCH_PAYTM_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_PAYTM_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_PAYTM_SUCCESS:
      state.value = action.payload ? action.payload : "";
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default paytmwidget;
