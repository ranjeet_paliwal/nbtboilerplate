import React, { Component } from "react";

class PhotoItem extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }
  render() {
    const { item, index, totalItem } = this.props;
    return (
      <div className="photoItem">
        <div className="pg">
          {index}/{totalItem}
        </div>
        <img
          src={process.env.IMG_URL + "photo/" + item.id + ".cms"}
          className="responsive"
        />
        <div className="photoTitle">
          <h6>{item.hl}</h6>
          <p>{item.cap}</p>
        </div>
      </div>
    );
  }
}

export default PhotoItem;
