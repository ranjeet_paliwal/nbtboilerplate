import React, { Component } from "react";
import SwipeableViews from "react-swipeable-views";
import { virtualizedetail } from "./../../../utils/virtualizedetail";
import PhotoItem from "./PhotoItem";

const VirtualizeSwipeableViews = virtualizedetail(SwipeableViews);

class Swiper extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.handleCloseClick.bind(this);
    this.slideRenderer = this.handleSlideRenderer.bind(this);
    this.state = { subItems: [], stateSelectedItem: 1 };
    this.subItems = [];
    this.currentIndex = 0;
  }

  handleCloseClick(event) {
    this.props.closeSwiper(this, event);
  }

  componentDidMount() {
    this.currentIndex = this.props.selectedItem;
    if (typeof this.state.subItems[this.currentIndex] == "undefined") {
      this.fetchSubItems(this.props.selectedItem);
    }
  }

  fetchSubItems(index) {
    const _this = this;
    const { items, app } = this.props;
    const item = items[index];
    const itemType = item.dm;
    const itemMsid = item.id;
    const itemDomain = app.urls[itemType] + app.urls.FEED_PHOTOSTORY;
    let itemUrl = itemDomain.replace("<msid>", itemMsid);

    fetch(itemUrl, { cors: true })
      .then(r => r.json())
      .then(data => {
        if (data.items && data.items.length > 0) {
          let subItems = _this.state.subItems;
          subItems[index] = data.items;
          _this.setState({
            subItems: subItems,
            stateSelectedItem: parseInt(index) + 1
          });
        }
      });
  }

  onChangeIndex(index, lastIndex) {
    this.currentIndex = index;
    const _this = this;
    if (typeof this.state.subItems[index] == "undefined") {
      this.fetchSubItems(index);
    } else {
      _this.setState({ stateSelectedItem: parseInt(index) + 1 });
    }
  }

  handleSlideRenderer({ key, index }) {
    if (this.currentIndex != index) {
      return <div key={key}></div>;
    }

    let { items } = this.props;
    let item = items[index];
    let subItems = typeof (this.state.subItems[index] != "undefined")
      ? this.state.subItems[index]
      : null;

    if (subItems) {
      return (
        <div key={key} id="itemContainer" className="itemContainer">
          <h4>{item.hl}</h4>
          {subItems.map((it, idx) => {
            return (
              <PhotoItem
                item={it}
                key={idx}
                index={idx + 1}
                totalItem={subItems.length}
              />
            );
          })}
        </div>
      );
    } else {
      return (
        <div key={key} id="itemContainer" className="itemContainer">
          <h4>{item.hl}</h4>
          <img
            src={process.env.IMG_URL + "photo/" + item.imageid + ".cms"}
            className="responsive"
          />
        </div>
      );
    }
  }

  render() {
    let { items, selectedItem, onChange } = this.props;
    selectedItem = parseInt(selectedItem);
    return (
      <div
        className={"animated slideInUp article-section swiperContainer"}
        id="swiperContainerDiv"
      >
        <div className={"swiperHeader noselect"} onClick={this.onClick}>
          ❮ फोटो मज़ा (
          <span id="selectedItemCont">{this.state.stateSelectedItem}</span>/
          {items.length})
        </div>
        {
          <VirtualizeSwipeableViews
            hysteresis={0.3}
            index={selectedItem}
            slideRenderer={this.slideRenderer.bind(this)}
            ignoreNativeScroll={true}
            slideCount={items.length}
            onChangeIndex={this.onChangeIndex.bind(this)}
          />
        }
      </div>
    );
  }
}

export default Swiper;
