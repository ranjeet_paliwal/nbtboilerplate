import fetch from "utils/fetch/fetch";
export const FETCH_PHOTO_REQUEST = "FETCH_PHOTO_REQUEST";
export const FETCH_PHOTO_SUCCESS = "FETCH_PHOTO_SUCCESS";
export const FETCH_PHOTO_FAILURE = "FETCH_PHOTO_FAILURE";

function fetchPhotosDataFailure(error) {
  return {
    type: FETCH_PHOTO_FAILURE,
    payload: error.message
  };
}
function fetchPhotosSuccess(data) {
  return {
    type: FETCH_PHOTO_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchPhotosData(state, params, query) {
  if (
    typeof state.header.items != "undefined" &&
    state.header.items.length > 0
  ) {
    let photoMenu = state.header.items.filter(item => {
      if (item.uid == "Photo-01") {
        return item;
      }
    });
    if (photoMenu.length > 0) {
      let apiUrl = photoMenu[0].defaulturl;
      return dispatch => {
        /*dispatch({
          type: FETCH_PHOTO_REQUEST
        });*/

        return fetch(apiUrl)
          .then(
            data => dispatch(fetchPhotosSuccess(data)),
            error => dispatch(fetchPhotosDataFailure(error))
          )
          .catch(function(error) {
            dispatch(fetchPhotosDataFailure(error));
          });
      };
    }
  } else {
    //dispatch(fetchPhotosDataFailure(error));
  }
}

function shouldFetchPhotosData(state, params, query) {
  if (
    state.photo.isFetching == false &&
    (typeof state.photo.value[0] == "undefined" ||
      typeof state.photo.value.items == "undefined")
  ) {
    return true;
  } else {
    return false;
  }
}

export function fetchPhotosIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchPhotosData(getState(), params, query)) {
      return dispatch(fetchPhotosData(getState(), params, query));
    }
  };
}
