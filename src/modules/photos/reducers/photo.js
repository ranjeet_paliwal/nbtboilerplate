import {
  FETCH_PHOTO_REQUEST,
  FETCH_PHOTO_SUCCESS,
  FETCH_PHOTO_FAILURE
} from "./../actions/photo";

function photo(
  state = {
    isFetching: false,
    error: false,
    value: {}
  },
  action
) {
  switch (action.type) {
    case FETCH_PHOTO_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_PHOTO_SUCCESS:
      state.value = action.payload;
      return {
        ...state,
        isFetching: false
      };

    case FETCH_PHOTO_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default photo;
