import React, { Component } from "react";
import { Link, browserHistory } from "react-router";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import { fetchPhotosIfNeeded } from "./actions/photo";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
//import {getSectionName} from './../../utils/util';
import styles from "./../../components/common/css/commonComponents.scss";
import Swiper from "./components/Swiper";
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;

export class Photos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: 0,
      showSwiper: false
    };
  }

  componentDidMount() {
    const { dispatch, query, params } = this.props;
    Photos.fetchData({ dispatch, query, params });
  }

  openSwiper(event) {
    let target = event.currentTarget;
    this.setState({
      selectedItem: target.getAttribute("data-index"),
      showSwiper: true
    });
  }

  closeSwiper() {
    this.setState({ showSwiper: false });
  }

  render() {
    const { value } = this.props;
    if (typeof value != "undefined" && value.hasOwnProperty("items")) {
      return (
        <li className={styles["home-section"] + " home-section"}>
          <h2>
            <Link to="/">
              <span>{locale.photo_maza}</span>
            </Link>
          </h2>
          <ul className="scroll">
            {value.items.map((item, index) => {
              return (
                <li
                  className="pcontainer"
                  key={index}
                  onClick={this.openSwiper.bind(this)}
                  data-index={index}
                >
                  <img
                    className="responsive"
                    src={
                      process.env.IMG_URL +
                      "/thumb/" +
                      item.imageid +
                      ".cms?width=150"
                    }
                  />
                  <span>{item.hl}</span>
                </li>
              );
            })}
          </ul>
          {this.state.showSwiper ? (
            <Swiper
              items={value.items}
              closeSwiper={this.closeSwiper.bind(this)}
              selectedItem={this.state.selectedItem}
              app={this.props.app}
            />
          ) : null}
        </li>
      );
    } else return null;
  }
}

Photos.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchPhotosIfNeeded(params, query));
};

Photos.propTypes = {};

function mapStateToProps(state) {
  return {
    app: state.app,
    ...state.photo
  };
}

export default connect(mapStateToProps)(Photos);
