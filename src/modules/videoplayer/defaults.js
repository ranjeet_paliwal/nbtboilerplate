/* eslint-disable no-restricted-globals */
/* eslint-disable func-names */
/* eslint-disable spaced-comment */
import { AnalyticsGA } from "../../components/lib/analytics/index";
import { handleNextVideo } from "./utils";
import { removeAllSlikePlayerInitializations } from "./slike";
import { fireGRXEvent } from "../../components/lib/analytics/src/ga";
// import { _isCSR } from "../../utils/util";

function handleAdEvents(player, eventName, eventData) {
  //console.log("=handleAdEvents=");
  //console.log("eventName :",eventName,eventData);
  eventName = eventName.replace("spl", "on");
  const adType = eventData.type == "pre" ? "Preroll" : "Postroll";
  const eventTypeConfig = {
    onAdResume: "PLAYING",
    onAdStart: "ADVIEW",
    onAdPause: "PAUSED",
    onAdComplete: "ADCOMPLETE",
    onAdSkip: "ADSKIP",
    onAdRequest: "ADREQUEST",
    onAdLoaded: "ADLOADED",
    onAdError: "ADERROR",
    onAdBlocked: "ADBLOCKED",
    onAdEnded: "ADENDED",
  };

  // const eventType = eventTypeConfig[eventName];
  const currentEvent = eventTypeConfig[eventName];
  if (currentEvent && adType) {
    //console.log("category : ",eventTypeConfig[eventName]);
    //console.log("action : ",location.pathname);

    if (
      currentEvent === "ADREQUEST" ||
      currentEvent === "ADERROR" ||
      currentEvent === "ADSKIP" ||
      currentEvent === "ADCOMPLETE"
    ) {
      AnalyticsGA.event({
        category: currentEvent,
        action: location.pathname,
        label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
      });
    }

    const eventType = eventData && eventData.state ? eventData.state.toUpperCase() : "";
    // FIXME: Can't see GA being fired for this in old code, confirm.
    if (currentEvent === "ADREQUEST") {
      window.adPlaying = true;
      const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
        detail: { videoType: "adStarted" },
      });

      // IF video is requested as muted, unmute it for ADS only
      // DO NOT CHANGE clientConfig directly
      // if (player && player.store && player.store.clientConfig && player.store.clientConfig.mute) {
      //   //Unmute player for ads specifically
      //   if (typeof player.unmute === "function") {
      //     player.unmute();
      //   }
      // }
      document.dispatchEvent(playerTypeEvent);
    }
    if (
      currentEvent === "ADCOMPLETE" ||
      currentEvent === "ADERROR" ||
      currentEvent === "ADBLOCKED" ||
      currentEvent === "ADENDED" ||
      currentEvent === "ADSKIP"
    ) {
      window.adPlaying = false;
      const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
        detail: { videoType: "adEnded" },
      });
      // if (player && player.store && player.store.clientConfig && player.store.clientConfig.mute) {
      //   //Mute player after ads complete, for any reason
      //   if (typeof player.mute === "function") {
      //     player.mute();
      //   }
      // }
      document.dispatchEvent(playerTypeEvent);
    }

    // AnalyticsGA.GTM({
    //   category: eventTypeConfig[eventName],
    //   action: location.pathname,
    //   label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
    //   event: "tvc_adevent",
    // });
  }
}

const getDefaultConfig = function() {
  return {
    config: {
      apiKey: "",
      contEl: "",
      colombiaCookieId: "", //shivam ask to client
      GDPR_MODE: false,
      comscoreId: "",
      gaId: "",
      video: {
        id: "",
        playerType: "",
        image: "",
        title: "",
        startTime: 0,
        pid: "", //agency
        msid: "",
        shareUrl: "",
      },
      player: {
        autoPlay: true,
        fallbackMute: true,
        midOverlayState: 1,
        mute: false,
        skipAd: false,
        volume: 70,
        playlist: false,
        playlistUrl: "",
        pageSection: "",
      },
      //Disabling dvr button for live videos ( didnt work for fb videos)
      live: {
        loadDVR: false,
      },
    },
    playerEvents: {
      onInit(player, eventName, eventData) {
        //onReady

        const youtubeVideo = !!(
          window.playerConfig.id.startsWith("YT") ||
          (eventData && eventData[1] && eventData[1].type[0] == "yt")
        );

        const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // const seoPath = document.location.pathname;
        const template = (window.playerConfig && window.playerConfig.template) || "";

        // if (!window.undockToPopup) {
        //   AnalyticsGA.event({
        //     category: window.playerConfig.isAudio ? "AUDIOREADY" : "VIDEOREADY",
        //     action: seoPath + (youtubeVideo ? "_youtube" : ""),
        //     label: template ? template.toUpperCase() : "",
        //   });
        // }
        window.videoUrl = location.pathname;

        // document.body.classList.add("videoplaying");
        // AnalyticsGA.event({ category: 'VIDEOREADY', action: location.pathname + (youtubeVideo ? "_youtube" : ""), label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        // AnalyticsGA.GTM({
        //   category: "VIDEOREADY",
        //   action: location.pathname + (youtubeVideo ? "_youtube" : ""),
        //   label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        //   event: "tvc_videoready",
        // });
        // const  category =  "VIDEOREADY";
        // const action =  location.pathname + (youtubeVideo ? "_youtube" : "");
        // const label = : window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        // event: "tvc_videoready",

        // fireAnalyticsGTM("VIDEOREADY",youtubeVideo,label,"tvc_videoready")
      },
      onVideoStarted(player, eventName, eventData) {
        try {
          if (!window.isVideoPlaying) {
            removeAllSlikePlayerInitializations();
            return;
          }
        } catch (e) {}

        // if (player && player.store && player.store.clientConfig && player.store.clientConfig.mute) {
        //   //Mute player after video starts if its in config
        //   if (typeof player.mute === "function") {
        //     player.mute();
        //   }
        // }

        const youtubeVideo = !!(
          window.playerConfig.id.startsWith("YT") ||
          (eventData && eventData[1] && eventData[1].type[0] == "yt")
        );
        const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // const seoPath = document.location.pathname;
        const template = (window.playerConfig && window.playerConfig.template) || "";
        //   //onPlayStart

        //Times Point API for Points
        const videoId =
          player &&
          player.store &&
          player.store.playerConfig &&
          player.store.playerConfig.video &&
          player.store.playerConfig.video.id;

        import("../../components/common/TimesPoints/timespoints.util").then(comp => {
          if (typeof comp.fireActivity === "function" && videoId) {
            comp.fireActivity("WATCH_VIDEOS", videoId);
          }
        });

        if (!window.undockToPopup) {
          if (window.minitvType) {
            let minitvType = "autoplay";
            if (window.minitvType === "auto") {
              AnalyticsGA.event({
                category: window.playerConfig.isAudio ? "AUDIOSTARTED" : "VIDEOVIEW",
                action: "autoplay/minitv",
                label: `${template ? template.toUpperCase() : ""}-${window.playerConfig.msid || ""}`,
              });
            } else if (window.minitvType === "user") {
              minitvType = "user-initiated";
              AnalyticsGA.event({
                category: window.playerConfig.isAudio ? "AUDIOSTARTED" : "VIDEOVIEW",
                action: "user-initiated/minitv",
                //FIXME: Doubt, verify
                label: `${template ? template.toUpperCase() : ""}-${window.playerConfig.msid || ""}`,
              });
            } else {
              // eslint-disable-next-line no-console
              console.error("GA call for the videoview is messed up");

              // console.log("onPlayerEvent1", data);
            }
            const miniTvEventDetails = {
              video_type: `${minitvType}/minitv`,
              video_template: template ? template.toUpperCase() : "",
              video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
              url: window.videoUrl,
              video_seo: seoPath || "",
            };
            fireGRXEvent("track", "video_view", miniTvEventDetails);
          } else {
            AnalyticsGA.event({
              category: window.playerConfig.isAudio ? "AUDIOSTARTED" : "VIDEOVIEW",
              action: seoPath + (youtubeVideo ? "_youtube" : ""),
              //FIXME: Doubt, verify window.playerConfig.template
              label: template ? template.toUpperCase() : "",
            });
            const videoEventDetails = {
              video_type: `${template}/video`,
              // video_template: template ? template.toUpperCase() : "",
              // video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
              video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
              url: window.videoUrl,
              is_popup_video: window.is_popup_video || false,
              is_pip_video: false,
            };
            const dockedVideoElement = document.getElementById("dock-root-container");
            // Seems to be triggered on next video cases
            // If docked video container is currently playing add is_pip_video to grx call
            if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
              videoEventDetails.is_pip_video = true;
            }
            fireGRXEvent("track", "video_view", videoEventDetails);
            // window.grx("track", "video_view",videoEventDetails);
          }
        } else {
          // reversing the flag when video is played
          window.undockToPopup = false;
        }

        // AnalyticsGA.GTM({
        //   category: "VIDEOVIEW",
        //   action: seoPath + (youtubeVideo ? "_youtube" : ""),
        //   label: template ? template.toUpperCase() : "",
        //   event: "tvc_videoview",
        // });
        const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
          detail: { videoType: youtubeVideo ? "youtube" : "slike" },
        });
        document.dispatchEvent(playerTypeEvent);

        //   let youtubeVideo =
        //     window.playerConfig.id.startsWith("YT") || (eventData && eventData[1] && eventData[1].type[0] == "yt")
        //       ? true
        //       : false;
        //   // AnalyticsGA.event({ category: 'VIDEOVIEW', action: location.pathname + (youtubeVideo ? "_youtube" : ""), label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        //   AnalyticsGA.GTM({
        //     category: "VIDEOVIEW",
        //     action: location.pathname + (youtubeVideo ? "_youtube" : ""),
        //     label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        //     event: "tvc_videoview",
        //   });
        // },
        // onVideoResumed: function(player, eventName, eventData) {
        //   // onStateChange
        //   var eventType = "PLAYING";
        //   // AnalyticsGA.event({ category: 'VIDEOPLAY', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        //   AnalyticsGA.GTM({
        //     category: "VIDEOPLAY",
        //     action: location.pathname,
        //     label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        //     event: "tvc_videoplay",
        //   });
        // },

        // onVideoFullscreenchange: function(player, eventName, eventData) {
        //   //onFullScreen
        //   var fullScreen = player.getVideoState().fullScreen;
        //   if (fullScreen) {
        //     //console.log('full screen');
        //   } else {
        //     //console.log('not in full screen');
        //   }
        //   // AnalyticsGA.event({ category: 'VIDEOFULLSCREEN', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        //   AnalyticsGA.GTM({
        //     category: "VIDEOFULLSCREEN",
        //     action: location.pathname,
        //     label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        //     event: "tvc_videofullscreen",
        //   });
      },
      onVideoPaused(player, eventName, eventData) {
        // let youtubeVideo =
        //   window.playerConfig.id.startsWith("YT") || (eventData && eventData[1] && eventData[1].type[0] == "yt")
        //     ? true
        //     : false;
        // const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // // const seoPath = document.location.pathname;
        // const template = (window.playerConfig && window.playerConfig.template) || "";
        // // onVideoPaused
        // let video_type = `${template}/video`;
        // if (window.minitvType) {
        //   video_type = `${window.minitvType === "auto" ? "autoplay" : "user-initiated"}/minitv`;
        // }
        // let timePlayed = 0;
        // let percentagePlayed = 0;
        // if (player && player.getVideoState) {
        //   const playerState = player.getVideoState();
        //   timePlayed = Math.round(parseFloat(playerState.currentTime).toPrecision(2));
        //   percentagePlayed = Math.round(
        //     ((parseFloat(playerState.currentTime) / parseFloat(playerState.duration)) * 100).toPrecision(2),
        //   );
        // }
        // fireGRXEvent("track", "video_played", {
        //   video_type: video_type,
        //   video_template: template ? template.toUpperCase() : "",
        //   video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
        //   video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
        //   percentage: percentagePlayed,
        //   time: timePlayed,
        // });
        // AnalyticsGA.event({ category: 'VIDEOPAUSE', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        // window.grx("track", "video_played", {
        //   video_type: video_type,
        //   video_template: template ? template.toUpperCase() : "",
        //   video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
        //   video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
        //   percentage: percentagePlayed,
        //   time: timePlayed,
        // });
      },
      onVideoCompleted(player, eventName, eventData) {
        // console.log('video play completed');
        let percentagePlayed = 0;
        let timePlayed = 0;
        if (player && player.getVideoState) {
          const playerState = player.getVideoState();
          timePlayed = Math.round(parseFloat(playerState.currentTime).toPrecision(2));
          percentagePlayed = Math.round(
            ((parseFloat(playerState.currentTime) / parseFloat(playerState.duration)) * 100).toPrecision(2),
          );
        }

        const youtubeVideo = !!(
          window.playerConfig.id.startsWith("YT") ||
          (eventData && eventData[1] && eventData[1].type[0] == "yt")
        );
        const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // const seoPath = document.location.pathname;
        const template = (window.playerConfig && window.playerConfig.template) || "";
        //   //onPlayStart
        // window.grx("track", "video_played", {
        //   video_type: `${template}/video`,
        //   video_template: template ? template.toUpperCase() : "",
        //   video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
        //   video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
        //   percentage: percentagePlayed,
        //   time: timePlayed,
        // });

        let is_pip_video = false;
        const dockedVideoElement = document.getElementById("dock-root-container");
        // Seems to be triggered on next video cases
        // If docked video container is currently playing add is_pip_video to grx call
        if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
          is_pip_video = true;
        }

        fireGRXEvent("track", "video_played", {
          video_type: `${template}/video`,
          is_popup_video: window.is_popup_video || false,
          // video_template: template ? template.toUpperCase() : "",
          // video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
          video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
          percentage: percentagePlayed,
          url: window.videoUrl,
          time: timePlayed,
          is_pip_video,
        });
        document.body.classList.remove("videoplaying");
        AnalyticsGA.event({
          category: window.playerConfig.isAudio ? "AUDIOENDED" : "VIDEOCOMPLETE",
          action: seoPath + (youtubeVideo ? "_youtube" : ""),
          label: template ? template.toUpperCase() : "",
        });
      },
      onVideoEnded(player, eventName, eventData) {
        if (!window.playerConfig.isAudio) {
          const youtubeVideo = !!(
            window.playerConfig.id.startsWith("YT") ||
            (eventData && eventData[1] && eventData[1].type[0] == "yt")
          );
          const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
          // const seoPath = document.location.pathname;
          const template = (window.playerConfig && window.playerConfig.template) || "";
          // onEnd
          document.body.classList.remove("videoplaying");
          // AnalyticsGA.event({
          //   category: "VIDEOENDED",
          //   action: seoPath + (youtubeVideo ? "_youtube" : ""),
          //   label: template ? template.toUpperCase() : "",
          // });
        }
      },
      //FIXME: Ask product to get this fixed.
      // onVolumeChange: function(player, eventName, eventData) {
      //   var volume = player.getVideoState().volume;
      //   // console.log(volume);
      //   //AnalyticsGA.event({ category: 'VIDEOVOLUMECHANGE', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
      //   AnalyticsGA.GTM({
      //     category: "VIDEOVOLUMECHANGE",
      //     action: location.pathname,
      //     label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
      //     event: "tvc_videovolumechange",
      //   });
      // },
      // onVideoMuted: function(player, eventName, eventData) {
      //   //console.log('player muted');
      //   // AnalyticsGA.event({ category: 'VIDEOMUTE', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
      //   AnalyticsGA.GTM({
      //     category: "VIDEOMUTE",
      //     action: location.pathname,
      //     label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
      //     event: "tvc_videomute",
      //   });
      // },
      onVideoPrev(player, eventName, eventData) {
        //console.log('prev video');
        // AnalyticsGA.event({ category: 'VIDEOPREV', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });

        const playerData = eventData;
        const newConfig = {};
        Object.keys(window.playerConfig).forEach(key => {
          if (eventData[key]) {
            newConfig[key] = eventData[key];
          }
        });
        // handleNextVideo functionalities
        window.playerConfig = newConfig;
        handleNextVideo(playerData, "auto");

        // AnalyticsGA.event({
        //   category: "VIDEOREQUEST_PREV",
        //   action: location.pathname,
        //   label: "auto",
        // });
      },
      onVideoNext(player, eventName, eventData) {
        const playerData = eventData;
        const newConfig = {};
        Object.keys(window.playerConfig).forEach(key => {
          if (eventData[key]) {
            newConfig[key] = eventData[key];
          }
        });

        AnalyticsGA.event({
          category: "VIDEOREQUEST_NEXT",
          action: location.pathname,
          label: "auto",
        });

        // handleNextVideo functionalities
        window.playerConfig = newConfig;

        const youtubeVideo = !!(
          window.playerConfig.id.startsWith("YT") ||
          (eventData && eventData[1] && eventData[1].type[0] == "yt")
        );
        const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // const seoPath = document.location.pathname;
        const template = (window.playerConfig && window.playerConfig.template) || "";

        const videoEventDetails = {
          video_type: `${template}/video`,
          // video_template: template ? template.toUpperCase() : "",
          // video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
          video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
          url: window.location.pathname,
          is_popup_video: window.is_popup_video || false,
          is_pip_video: false,
        };
        const dockedVideoElement = document.getElementById("dock-root-container");
        // Seems to be triggered on next video cases
        // If docked video container is currently playing add is_pip_video to grx call
        if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
          videoEventDetails.is_pip_video = true;
        }
        fireGRXEvent("track", "video_request", videoEventDetails);

        //   //onPlayStart
        handleNextVideo(playerData, "auto");
      },
      onVideoReload(player, eventName, eventData) {
        //onEndScreenReplay
        //console.log('replay video and onEndScreenReplay');
        // ga event for video Replay
        //       AnalyticsGA.event({ category: "VIDEOREPLAY", action: location.pathname });
        //ga event for video Replay
        // AnalyticsGA.event({ category: 'VIDEOREPLAY', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });

        const youtubeVideo = !!(
          window.playerConfig.id.startsWith("YT") ||
          (eventData && eventData[1] && eventData[1].type[0] == "yt")
        );
        const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
        // const seoPath = document.location.pathname;
        const template = (window.playerConfig && window.playerConfig.template) || "";

        // ga event for video Replay
        AnalyticsGA.event({
          category: window.playerConfig.isAudio ? "AUDIOREQUEST_REPLAY" : "VIDEOREQUEST_REPLAY",
          action: seoPath + (youtubeVideo ? "_youtube" : ""),
          label: template ? template.toUpperCase() : "",
        });

        // AnalyticsGA.GTM({
        //   category: "VIDEOREPLAY",
        //   action: location.pathname,
        //   label: template ? template.toUpperCase() : "",
        //   event: "tvc_videocomplete",
        // });
      },
    },
    adEvents: {
      onAdComplete(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdSkip(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdLoaded(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdRequest(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdError(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdResume(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdStart(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
      onAdPause(player, eventName, eventData) {
        handleAdEvents(player, eventName, eventData);
      },
    },
  };
};

// /* eslint-disable no-restricted-globals */
// import { AnalyticsGA } from "../../components/lib/analytics/index";
// import { handleNextVideo } from "./utils";

// const defaults = {
//   sdk: {
//     apikey: "",
//     referrer: typeof document === "object" ? document.referrer : "",
//   },
//   page: {
//     origin: typeof location !== "undefined" ? `${location.protocol}//${location.host}` : "",
//   },
//   player: {
//     skipAd: false,
//     autoplay: true,
//     startFromSec: -1,
//     nextCardCounter: 1,
//     isLoop: true,
//     isComScorePlugin: false,
//     isGAPlugin: false,
//     fallbackMute: true,
//   },
//   controls: {
//     controlsType: "custom",
//     showFullScreen: true,
//     showAutoPlayNext: true,
//     showShare: false,
//     volume: 70,
//     mute: false,
//     autoPlayNext: true,
//     showNextButton: true,
//     showPrevButton: true,
//     showPipIcon: false,
//   },
//   video: {
//     id: "",
//     image: "",
//   },
//   events: {
//     onAdEvent: adData => {
//       const eventType = adData.state ? adData.state.toUpperCase() : "";
//       if (eventType === "REQUEST") {
//         window.adPlaying = true;
//         const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
//           detail: { videoType: "adStarted" },
//         });
//         document.dispatchEvent(playerTypeEvent);
//       }
//       if (eventType === "ENDED" || eventType === "AD_BLOCKED") {
//         window.adPlaying = false;
//         const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
//           detail: { videoType: "adEnded" },
//         });
//         document.dispatchEvent(playerTypeEvent);
//       }
//       const adType = adData.adtype === "pre" ? "Preroll" : "Postroll";
//       const eventTypeConfig = {
//         LOADED: "ADLOADED",
//         START: "ADVIEW",
//         COMPLETE: "ADCOMPLETE",
//         SKIP: "ADSKIP",
//         ERROR: "ADERRORBLANK",
//         REQUEST: "ADREQUEST",
//         AD_BLOCKED: "ADBLOCKER",
//       };
//       // if (eventTypeConfig[eventType] && adType) {
//       //   AnalyticsGA.event({
//       //     category: eventTypeConfig[eventType],
//       //     action: location.pathname,
//       //   });
//       // }
//     },
//     onPlayerEvent: (evtname, data) => {
//       let youtubeVideo = "";
//       if (data && data[1] && data[1].type && data[1].type[0] && window.playerConfig && window.playerConfig.id) {
//         youtubeVideo = window.playerConfig.id.startsWith("YT") || (data && data[1] && data[1].type[0] === "yt");
//       }

//       const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
//       // const seoPath = document.location.pathname;
//       const template = (window.playerConfig && window.playerConfig.template) || "";

//       switch (evtname) {
//         case "onReady":
//           // document.body.classList.add("videoplaying");
//           if (!window.undockToPopup) {
//             AnalyticsGA.event({
//               category: "VIDEOREADY",
//               action: seoPath + (youtubeVideo ? "_youtube" : ""),
//               label: template ? template.toUpperCase() : "",
//             });
//           }
//           window.videoUrl = location.pathname;
//           break;

//         case "onPlayStart": {
//           if (!window.undockToPopup) {
//             if (window.minitvType) {
//               if (window.minitvType === "auto") {
//                 AnalyticsGA.event({
//                   category: "VIDEOVIEW",
//                   action: "autoplay/minitv",
//                   label: "",
//                 });
//               } else if (window.minitvType === "user") {
//                 AnalyticsGA.event({
//                   category: "VIDEOVIEW",
//                   action: "user-initiated/minitv",
//                   label: "",
//                 });
//               } else {
//                 // eslint-disable-next-line no-console
//                 console.error("GA call for the videoview is messed up");
//               }
//             } else {
//               AnalyticsGA.event({
//                 category: "VIDEOVIEW",
//                 action: seoPath + (youtubeVideo ? "_youtube" : ""),
//                 label: template ? template.toUpperCase() : "",
//               });
//             }
//           } else {
//             // reversing the flag when video is played
//             window.undockToPopup = false;
//           }

//           // AnalyticsGA.GTM({
//           //   category: "VIDEOVIEW",
//           //   action: seoPath + (youtubeVideo ? "_youtube" : ""),
//           //   label: template ? template.toUpperCase() : "",
//           //   event: "tvc_videoview",
//           // });
//           const playerTypeEvent = new CustomEvent("setVideoPlayerType", {
//             detail: { videoType: youtubeVideo ? "youtube" : "slike" },
//           });
//           document.dispatchEvent(playerTypeEvent);
//           break;
//         }

//         case "onComplete":
//           document.body.classList.remove("videoplaying");
//           AnalyticsGA.event({
//             category: "VIDEOCOMPLETE",
//             action: location.pathname,
//           });
//           // AnalyticsGA.GTM({
//           //   category: "VIDEOCOMPLETE",
//           //   action: location.pathname,
//           //   label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
//           //   event: "tvc_videocomplete",
//           // });
//           break;

//         default:
//           break;
//       }
//     },
//     onEndScreenReplay: () => {
//       // ga event for video Replay
//       AnalyticsGA.event({ category: "VIDEOREPLAY", action: location.pathname });
//     },
//     onEndScreenAutoPlay: playerData => {
//       // handleNextVideo functionalities
//       window.playerConfig = playerData;
//       handleNextVideo(playerData, "auto");
//     },
//     onEndScreenNextClick: playerData => {
//       // handleNextVideo functionalities
//       handleNextVideo(playerData, "click");
//     },
//     onEndScreenPlay: playerData => {
//       // handleNextVideo functionalities
//       handleNextVideo(playerData, "endplay");
//     },
//   },
// };

// export default defaults;

export default getDefaultConfig;
