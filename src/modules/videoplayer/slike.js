/* eslint-disable no-plusplus */
/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */
import { AnalyticsGA } from "../../components/lib/analytics/index";
import OnDemandLoader from "../ondemandloader/index";
import defaultObj from "./defaults";
import { _getStaticConfig, isMobilePlatform, _isCSR, _getCookie } from "../../utils/util";
import { fireGRXEvent } from "../../components/lib/analytics/src/ga";
import { closeVideo } from "./dockvideoplayer";
import fetch from "../../utils/fetch/fetch";

const siteConfig = _getStaticConfig();
const slikeApiKey = siteConfig.slike.apikey;

let scriptCache;

function initializeSlike(properties) {
  if (typeof window !== "undefined") {
    window.SPL = window.SPL || {};
    window.SPL.config = {
      apiKey: slikeApiKey,
      sdk: {
        apikey: slikeApiKey,
      },
    };
  }
  setScriptCache(properties);
}

function setScriptCache(properties) {
  // todo for optimization
  // if (!scriptCache || !scriptCache.slikeSDK) {

  // }
  scriptCache = OnDemandLoader({
    imaSDK1: "https://imasdk.googleapis.com/js/sdkloader/ima3.js",
    imaSDK2: "https://videoplayer.indiatimes.com/v2/includes/ima3.js",
    // slikeSDK: "https://videoplayer.indiatimes.com/v2.5.8/sdk.js",
  });
  // scriptCache.slikeSDK.onLoad(() => {
  fetchVideo(properties);
  // });
}

function generateNextVideoUrl(parentmsid, msid) {
  const nextVideoUrl = `${siteConfig.slike.nextvideo_domain}/feeds/videpostroll_v5_slike/${parentmsid}.cms?feedtype=json&dontshow=${msid}&callback=cached`;
  return nextVideoUrl;
}

function fetchVideo(properties) {
  let { config } = properties;
  const videoWrapper = properties.wrapper;
  let defaults = {}; // Setting default config properties in this object
  if (defaultObj) {
    defaults = defaultObj();
    // Copy events separately as stringify doesn't copy function
    // TODO : create a util function for deep copying
    // defaults.events = defaultObj.events;
  }

  // merge defaults slikeconfig properties with template setup
  // eslint-disable-next-line no-restricted-syntax
  for (const prop in defaults.config) {
    if (Object.prototype.hasOwnProperty.call(config, prop)) {
      defaults.config[prop] = { ...defaults[prop], ...config[prop] };
    }
  }

  // defaults  returns an object with config as a property
  if (defaults && defaults.config && defaults.config.video && defaults.config.video.msid) {
    const apiUrl = `${process.env.API_BASEPOINT}/pwa_videoshow/${defaults.config.video.msid}.cms?feedtype=sjson`;
    if (config.controls && config.controls.ui === "podcast") {
      defaults.config.player.skipAd = true;
    }
    fetch(apiUrl)
      .then(
        body => {
          if (body && body.items) {
            if (body.items[0]) {
              emitHeaderData(body.items[0].wu, body.items[0].hl);
            }
            if (document.getElementById("video_pop_up_player")) {
              emitSliderData(body.items[0]);
              if (body.items[0] && body.items[0].wu) {
                pushURLAndFireGA(body);
              }
            }
            const nextVideoUrl = generateNextVideoUrl(body.pwa_meta.subsecmsid2, body.items[0].id);
            config.player.nextVideoUrl = nextVideoUrl;
          } else if (body && body.wu) {
            emitHeaderData(body.wu, body.hl);
          }

          if (body && body.items && body.items[0].Story && body.items[0].Story !== defaults.config.video.Story) {
            defaults.config.video.Story = body.items[0].Story;
            const updateConfigEvent = new CustomEvent("updateVideoConfig", {
              detail: { config: defaults.config.video },
            });
            document.dispatchEvent(updateConfigEvent);
          }
          // update video config in redux store

          // if (data && data.pwa_meta && data.pwa_meta.AdServingRules === "Turnoff") {
          //   defaults.player.skipAd = true;
          // } else {
          //   defaults.player.skipAd = false;
          // }
          config = Object.assign({}, config, defaults.config);

          slikeVideoLoad(videoWrapper, config, { playerEvents: defaults.playerEvents, adEvents: defaults.adEvents });
        },
        error => {
          // defaults.player.skipAd = true;
          config = Object.assign({}, config, defaults.config);
          slikeVideoLoad(videoWrapper, config, { playerEvents: defaults.playerEvents, adEvents: defaults.adEvents });
          // eslint-disable-next-line no-console
          console.error(error);
        },
      )
      .catch(error => {
        // defaults.player.skipAd = true;
        config = Object.assign({}, config, defaults.config);
        slikeVideoLoad(videoWrapper, config, { playerEvents: defaults.playerEvents, adEvents: defaults.adEvents });
        // eslint-disable-next-line no-console
        console.error(error);
      });
  }
}

function emitSliderData(data) {
  const event = new CustomEvent("addSliderData", { detail: data });
  document.dispatchEvent(event);
}

function pushURLAndFireGA(data) {
  if (!data) {
    return;
  }
  const { items, pwa_meta, audetails } = data;
  let url;
  try {
    url = new URL(items[0].wu);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
  }
  if (url) {
    try {
      window.history.pushState({}, "", url.pathname);
      if (pwa_meta && typeof pwa_meta === "object") {
        if (audetails) {
          window._editorname = audetails.cd;
        } else {
          window._editorname =
            typeof pwa_meta.editorname !== "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
        }
        // Hit ga
        AnalyticsGA.pageview(window.location.pathname, {
          setEditorName: true,
          is_popup_video: window.is_popup_video || false,
        });
      }
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
    }
  }
}

function emitHeaderData(url, title) {
  const headerData = { url: url || "", title: title || "" };
  const newEvent = new CustomEvent("setVideoHeader", {
    detail: headerData,
  });
  document.dispatchEvent(newEvent);
}

function playerHeightWidthGiver(videoWrapper, playerId) {
  const imgPlaceholder = videoWrapper.querySelector("img");
  if (imgPlaceholder) {
    const imageContainer = imgPlaceholder.parentElement;
    if (imageContainer.style.display === "none") {
      return;
    }
    let imgPlaceholderHeight = imgPlaceholder ? imgPlaceholder.height || imgPlaceholder.clientHeight : 250;
    let imgPlaceholderWidth = imgPlaceholder ? imgPlaceholder.width || imgPlaceholder.clientWidth : 300;
    if (!imgPlaceholderHeight) {
      imgPlaceholderHeight = isMobilePlatform() ? 200 : 353;
    }
    if (imgPlaceholderHeight === 300 && imgPlaceholderWidth === 100) {
      imgPlaceholderHeight = isMobilePlatform() ? 200 : 353;
      if (!isMobilePlatform()) {
        imgPlaceholderWidth = 630;
      }
    }
    if (document.querySelector(`#${playerId}`)) {
      document.querySelector(`#${playerId}`).style.height = `${imgPlaceholderHeight}px`;
      document.querySelector(`#${playerId}`).style.width = `${imgPlaceholderWidth}px`;
    }
  }
}

function hidePlaceholderImage(videoWrapper) {
  if (videoWrapper.getElementsByClassName("image-container")[0]) {
    // eslint-disable-next-line no-param-reassign
    videoWrapper.getElementsByClassName("image-container")[0].style.display = "none";
  }
}

// function showPlaceholderImage(videoWrapper) {
//   videoWrapper.getElementsByClassName("image-container")[0].style.display = "block";
// }

function mapPlayerEvents(events) {
  // Take playerEvents and adEvents out of config file
  const { playerEvents, adEvents } = events;

  // Mapper function for player events and ad events
  const eventToFunction = (player, eventName, data) => {
    const funcName = eventName.replace("spl", "on");
    const eventData = data || {};
    if (typeof playerEvents[funcName] === "function") {
      playerEvents[funcName](player, eventName, eventData);
    } else if (typeof adEvents[funcName] === "function") {
      adEvents[funcName](player, eventName, eventData);
    }
  };

  Object.keys(window.SlikePlayer.Events).forEach(eventKey => {
    const eventName = window.SlikePlayer.Events[eventKey];
    window.currentPlayerInst.on(eventName, eventToFunction.bind(null, window.currentPlayerInst));
  });

  Object.keys(window.SlikePlayer.AdEvents).forEach(eventKey => {
    const eventName = window.SlikePlayer.AdEvents[eventKey];
    window.currentPlayerInst.on(eventName, eventToFunction.bind(null, window.currentPlayerInst));
  });
}

function slikeVideoLoad(videoWrapper, config, events) {
  const isAudio = config.controls && config.controls.ui == "podcast";
  if (typeof document !== "undefined") {
    let playerId;

    let _auds;
    if (typeof window !== "undefined" && typeof colaud !== "undefined") {
      localStorage.setItem("colaud", colaud.aud);
    }
    if (typeof colaud !== "undefined") {
      _auds = colaud.aud;
    } else if (window !== "undefined" && window.localStorage && typeof localStorage !== "undefined") {
      _auds = localStorage.getItem("colaud") ? localStorage.getItem("colaud") : null;
    }
    config.video.sg = _auds;

    let player = videoWrapper.querySelector(".default-player");
    if (!player) {
      player = videoWrapper.querySelector(".minitv-player");
    }
    if (player) {
      playerId = player.getAttribute("id");
    }
    if (typeof window.spl !== "undefined" && typeof window.spl.load === "function") {
      if (playerId !== "minitv_player") {
        playerHeightWidthGiver(videoWrapper, playerId);
      }
      hidePlaceholderImage(videoWrapper);
      window.playerConfig = config.video;
      if (config.controls && config.controls.ui == "podcast") {
        window.playerConfig.isAudio = true;
      }
      if (!window.undockToPopup) {
        // AnalyticsGA.event({
        //   category: "VIDEOBEFORESLOAD",
        //   action: location.pathname,
        // });
      }
      config.apiKey = slikeApiKey;
      config.contEl = playerId;
      const fpc = _getCookie("_col_uuid");
      if (fpc) {
        config.colombiaCookieId = fpc;
      }

      // FIXME: Earlier passed  playerId, config, inst now only config
      window.spl.load(config, sdkLoadStatus => {
        window.currentPlayerInst = new window.SlikePlayer(config);
        const youtubeVideo =
          window.playerConfig.id.startsWith("YT") ||
          (currentPlayerInst.videoData &&
            currentPlayerInst.videoData.AG &&
            currentPlayerInst.videoData.AG === "Youtube");

        mapPlayerEvents(events);

        if (!window.undockToPopup) {
          const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
          // const seoPath = document.location.pathname;
          const template = (window.playerConfig && window.playerConfig.template) || "";

          const videoEventDetails = {
            video_type: `${template}/video`,
            // video_template: template ? template.toUpperCase() : "",
            // video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
            video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
            url: window.videoUrl,
            is_popup_video: window.is_popup_video || false,
            is_pip_video: false,
          };
          const dockedVideoElement = document.getElementById("dock-root-container");
          // Seems to be triggered on next video cases
          // If docked video container is currently playing add is_pip_video to grx call
          if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
            videoEventDetails.is_pip_video = true;
          }
          let minitvType = "";
          if (window.minitvType) {
            if (window.minitvType === "auto") {
              minitvType = "autoplay";
              AnalyticsGA.event({
                category: isAudio ? "AUDIOREQUEST" : "VIDEOREQUEST",
                action: "autoplay/minitv",
                label: `${window.playerConfig.template ? window.playerConfig.template.toUpperCase() : ""}-${window
                  .playerConfig.msid || ""}`,
              });
              window.minitvType = "user";
            } else if (window.minitvType === "user") {
              minitvType = "user-initiated";
              AnalyticsGA.event({
                category: isAudio ? "AUDIOREQUEST" : "VIDEOREQUEST",
                action: "user-initiated/minitv",
                label: `${window.playerConfig.template ? window.playerConfig.template.toUpperCase() : ""}-${window
                  .playerConfig.msid || ""}`,
              });
            } else {
              // eslint-disable-next-line no-console
              console.error("GA call for the videorequest is messed up");
            }
          } else {
            AnalyticsGA.event({
              category: isAudio ? "AUDIOREQUEST" : "VIDEOREQUEST",
              action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""),
              label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
            });
          }

          // If minitv exists , change type to minitv
          videoEventDetails.video_type = minitvType ? `${minitvType}/minitv` : `${template}/video`;
          fireGRXEvent("track", "video_request", videoEventDetails);
        }

        // AnalyticsGA.GTM({
        //   category: "VIDEOREQUEST",
        //   action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""),
        //   label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        //   event: "tvc_videorequest",
        // });

        if (!window.undockToPopup) {
          // AnalyticsGA.event({
          //   category: "VIDEOAFTERSLOAD",
          //   action: location.pathname,
          // });
        }
      });
    }
  }
}

// for player.js
function playVideoViaSlike(properties, fromMiniTv) {
  if (!fromMiniTv) {
    closeMinitv();
  }
  removeAllSlikePlayerInitializations();
  // properties.config.version = "3.5.5";
  // properties.config.env = "stg";
  const event = new CustomEvent("playVideo", { detail: properties });
  document.dispatchEvent(event);

  // console.log(properties);
  // properties.config.video.id = "1xn4c53ouu";

  if (properties.wrapper) {
    window.isVideoPlaying = true;

    initializeSlike(properties);
  }
}

// for all files who want to play video in specific container
function playVideo(config, wrapperName, fromMiniTv) {
  const videoPlayer = document.getElementById(wrapperName);
  let videoPlayerWrapper;
  let parentName;
  if (videoPlayer) {
    parentName = videoPlayer.getAttribute("data-parent");
    if (parentName) {
      videoPlayerWrapper = document.getElementById(parentName);
    }
  }

  if (!videoPlayerWrapper) {
    videoPlayerWrapper = document.getElementById(wrapperName).parentElement;
  }

  if (!videoPlayerWrapper) {
    // eslint-disable-next-line no-console
    console.error("No such wrapper exists. Will not be able to run video");
  }
  const properties = {};
  properties.config = config;
  properties.wrapper = videoPlayerWrapper;
  playVideoViaSlike(properties, fromMiniTv);
}

/**
 *
 * @description Central helper function which fires growthrx event
 *  wherever video is exit / closed from.
 */
function fireVideoPlayedGRXEvent() {
  try {
    const youtubeVideo = window.playerConfig.id.startsWith("YT");
    const seoPath = (window.playerConfig && window.playerConfig.seopath) || "";
    // const seoPath = document.location.pathname;
    const template = (window.playerConfig && window.playerConfig.template) || "";
    // onVideoPaused
    let video_type = `${template}/video`;
    if (window.minitvType) {
      video_type = `${window.minitvType === "auto" ? "autoplay" : "user-initiated"}/minitv`;
    }
    let timePlayed = 0;
    let percentagePlayed = 0;
    if (_isCSR() && window.currentPlayerInst && window.currentPlayerInst.getVideoState) {
      const playerState = window.currentPlayerInst.getVideoState();
      timePlayed = Math.round(parseFloat(playerState.currentTime).toPrecision(2));
      percentagePlayed = Math.round(
        ((parseFloat(playerState.currentTime) / parseFloat(playerState.duration)) * 100).toPrecision(2),
      );
    }

    let is_pip_video = false;
    const dockedVideoElement = document.getElementById("dock-root-container");
    // Seems to be triggered on next video cases
    // If docked video container is currently playing add is_pip_video to grx call
    if (dockedVideoElement && !dockedVideoElement.classList.contains("hide")) {
      is_pip_video = true;
    }

    // Additional check to only track it if actually played or played less than 100 (to avoid double calls)
    if (timePlayed && percentagePlayed < 100) {
      fireGRXEvent("track", "video_played", {
        video_type,
        is_popup_video: window.is_popup_video || false,
        // video_template: template ? template.toUpperCase() : "",
        // video_playback_format: window.playerConfig.isAudio ? "audio" : "video",
        video_seo: seoPath + (youtubeVideo ? "_youtube" : ""),
        url: window.videoUrl || window.location.pathname,
        percentage: percentagePlayed,
        time: timePlayed,
        is_pip_video,
      });
    }
  } catch (e) {
    console.log("EXCEPTION IN FIRING VIDEO EXIT GRX", e);
  }
}

function removeAllSlikePlayerInitializations() {
  if (document) {
    // If video was already playing, and we are calling this method
    // We are trying to exit and close a video, In this case GRX should be fired.
    if (window.isVideoPlaying) {
      fireVideoPlayedGRXEvent();
    }

    if (window.currentPlayerInst) {
      try {
        window.currentPlayerInst.destroy();
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    }
    window.isVideoPlaying = false;

    // class changed to grab the real players
    const realPlayers = document.getElementsByClassName("__player");
    const audioPlayers = document.getElementsByTagName("saudio");
    const defaultPlayers = [];
    if (realPlayers) {
      for (let i = 0; i < realPlayers.length; i++) {
        // if (realPlayers[i].parentElement.id !== "minitv_player") {
        //   defaultPlayers.push(realPlayers[i].parentElement);
        // }
        defaultPlayers.push(realPlayers[i].parentElement);
        realPlayers[i].remove();
      }
    }
    if (audioPlayers) {
      for (let i = 0; i < audioPlayers.length; i++) {
        defaultPlayers.push(audioPlayers[i].parentElement);
        audioPlayers[i].remove();
      }
    }

    if (defaultPlayers && defaultPlayers.length > 0) {
      for (let i = 0; i < defaultPlayers.length; i++) {
        if (defaultPlayers[i].parentElement && defaultPlayers[i].parentElement.id === "dock-video-player") {
          // close
          if (!window.showMinitv) {
            closeVideo();
          }
        }
        defaultPlayers[i].style = "";
        if (defaultPlayers[i].parentElement) {
          const imageContainer = defaultPlayers[i].parentElement.getElementsByClassName("image-container")[0];
          if (imageContainer) {
            imageContainer.style.display = "block";
          }
        }
      }
    }
  }
}

function playMinitvInPopup(config) {
  closeMinitv();
  window.minitvType = "user";
  setTimeout(() => {
    const playerTypeEvent = new CustomEvent("playVideoInPopup", {
      detail: { config },
    });
    document.dispatchEvent(playerTypeEvent);
  }, 0);
  const newplayerTypeEvent = new CustomEvent("setMinitvPlaying", {
    detail: { isMinitvPlaying: true },
  });
  document.dispatchEvent(newplayerTypeEvent);
}

function playMinitv(minitvConfig, autoplay) {
  if (!minitvConfig) {
    return;
  }
  const playerTypeEvent = new CustomEvent("setMinitvVisibility", {
    detail: { showMinitv: true },
  });
  document.dispatchEvent(playerTypeEvent);
  if (autoplay) {
    window.minitvType = "auto";
  } else {
    window.minitvType = "user";
  }
  if (!isMobilePlatform()) {
    playVideo(minitvConfig, "minitv_container", true);
  } else if (autoplay) {
    playVideo(minitvConfig, "minitv_container", true);
  } else {
    playMinitvInPopup(minitvConfig);
  }
}

function closeMinitv() {
  const newplayerTypeEvent = new CustomEvent("setMinitvPlaying", {
    detail: { isMinitvPlaying: false },
  });
  document.dispatchEvent(newplayerTypeEvent);
  removeAllSlikePlayerInitializations();
  // this variable is to keep track of minitv events(ga and grx) and handling accordingly
  window.minitvType = "";
  const playerTypeEvent = new CustomEvent("setMinitvVisibility", {
    detail: { showMinitv: false },
  });
  document.dispatchEvent(playerTypeEvent);
}

function isMiniTvVisible() {
  let isMinitvVisible = false;
  const minitvWrapper = document.getElementById("outer_minitv_container");
  if (minitvWrapper) {
    isMinitvVisible = !minitvWrapper.classList.contains("hide");
  }
  return isMinitvVisible;
}

function isDockPlayerVisible() {
  let isDockWrapperVisible = false;
  const dockWrapper = document.getElementById("dock-root-container");
  if (dockWrapper && !dockWrapper.classList.contains("hide")) {
    isDockWrapperVisible = true;
  }
  return isDockWrapperVisible;
}

// function countGenerator() {
//   var i = 0;
//   return function() {
//     return i++;
//   };
// }

export {
  playVideoViaSlike,
  playVideo,
  fireVideoPlayedGRXEvent,
  removeAllSlikePlayerInitializations,
  playMinitv,
  closeMinitv,
  isMiniTvVisible,
  isDockPlayerVisible,
};
