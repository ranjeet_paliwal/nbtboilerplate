// This is now a redundunt file. Could be deleted safely
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import fetch from "utils/fetch/fetch";
import styles from "../../components/common/css/VideoPlayer.scss";
import OnDemandLoader from "../ondemandloader/index";
import defaultObj from "./defaults";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import { AnalyticsGA } from "../../components/lib/analytics/index";
import { getMobileOperatingSystem, _getCookie, _getStaticConfig, _isCSR } from "../../utils/util";
import { SeoSchema } from "../../components/common/PageMeta";
const siteConfig = _getStaticConfig();

class VideoPlayer extends PureComponent {
  // Specifies the default values for props:
  static defaultProps = {
    videoClassName: "inline",
    overlay: false,
  };

  static propTypes = {
    apikey: PropTypes.string.isRequired,
    config: PropTypes.object.isRequired,
    videoClassName: PropTypes.string,
    msid: PropTypes.string,
    slikeId: PropTypes.string,
    overlay: PropTypes.bool,
    wrapper: PropTypes.string,
    onVideoClick: PropTypes.func,
    inline: PropTypes.bool,
  };

  constructor(props, defaultProps) {
    super(props, defaultProps);
    this.videoPlayerWrapper = null;
    this.defaultPlayerId = `defaultPlayer_${this.getRandomInt(1, 1000)}`;
    this.currentPlayerInst = null;
    this.masterPlayerContainer = null;
  }

  componentWillMount() {
    if (typeof window !== "undefined") {
      window.SPL = window.SPL || {};
      window.SPL.config = {
        sdk: {
          apikey: this.props.apikey,
        },
      };
    }

    // -----For adaptive custom ad wrapper live only on samayam properties.
    // if ((process.env.SITE == 'mly') || (process.env.SITE == 'tml') || (process.env.SITE == 'tlg')) {
    //   this.scriptCache = OnDemandLoader({
    //     imaSDK1: 'https://imasdk.googleapis.com/js/sdkloader/ima3.js',
    //     imaSDK2: 'https://videoplayer.indiatimes.com/v2/includes/ima3.js',
    //     slikeSDK: 'https://videoplayer.indiatimes.com/v2.5.3/sdk.js'
    //   });
    // } else {
    //   this.scriptCache = OnDemandLoader({
    //     imaSDK1: 'https://imasdk.googleapis.com/js/sdkloader/ima3.js',
    //     imaSDK2: 'https://videoplayer.indiatimes.com/v2/includes/ima3.js',
    //     slikeSDK: 'https://videoplayer.indiatimes.com/v2.4/sdk.js'
    //   });
    // }

    // version rollback (2.5 back to 2.4), pre roll ads are not displaying in malayalam sites
    this.scriptCache = OnDemandLoader({
      imaSDK1: "https://imasdk.googleapis.com/js/sdkloader/ima3.js",
      imaSDK2: "https://videoplayer.indiatimes.com/v2/includes/ima3.js",
      slikeSDK: "https://videoplayer.indiatimes.com/v2.4/sdk.js",
    });
  }

  componentDidMount() {
    const self = this;
    const os = getMobileOperatingSystem();
    self.scriptCache.slikeSDK.onLoad(() => {
      if (self.videoPlayerWrapper) {
        self.videoPlayerWrapper.addEventListener("click", function handler(event) {
          event.preventDefault();
          const tthis = this;
          // if(tthis.getAttribute('slikeready') === 'true' ){
          //     return false;
          // }
          // tthis.setAttribute('slikeready' , 'true');

          // enable full screen for video which has following cases
          // 1. no autoplay
          // 2. only for android device
          // 3. no video currently playing
          if (!self.props.manuallytriggerClick && os != "ios") {
            self.props.config.player = self.props.config.player ? self.props.config.player : {};
            self.props.config.player.fallbackMute = false;
            self.masterPlayerContainer.webkitRequestFullscreen();
            self.masterPlayerContainer.classList.add("mPlayer_fullScreen");
          }

          // assign fallbackmute true for autoplay video
          else {
            self.props.config.player = self.props.config.player ? self.props.config.player : {};
            self.props.config.player.fallbackMute = true;
          }
          if (self.props.onVideoClick) self.loadVideo(self.videoPlayerWrapper); // this.removeEventListener('click', handler);
          // self.props.onVideoClick(event);
        });
      }
      if (self.props.manuallytriggerClick && !document.body.classList.contains("videoplaying")) {
        self.videoPlayerWrapper.click();
        // let event = new CustomEvent("dockMasterPlayer");
        // document.dispatchEvent(event);
      }
    });
    self.masterPlayerContainer = !self.masterPlayerContainer
      ? document.querySelector(".master-player-container")
      : self.masterPlayerContainer;
    window.addEventListener(
      "orientationchange",
      () => {
        const masterVideoPlayer = document.getElementById("masterVideoPlayer");
        if (
          masterVideoPlayer &&
          window.innerHeight < window.innerWidth &&
          !self.masterPlayerContainer.classList.contains("enable_dock")
        ) {
          masterVideoPlayer.setAttribute("style", "min-width :100% !important; min-height : 100% !important");
        }
      },
      false,
    );
  }

  componentWillUnmount() {
    // if (document.body.classList.contains('videoplaying')) {
    //   let self = this;
    //   self.masterPlayerContainer = !self.masterPlayerContainer ? document.querySelector('.master-player-container') : self.masterPlayerContainer;
    //   let event = new CustomEvent("dockMasterPlayer");
    //   document.dispatchEvent(event);
    // }
    // if (typeof S != "undefined" && typeof S.pauseAllPlayers == "function" && typeof S.destroy == "function") {
    //   try{
    //     if(typeof window.SPL != 'undefined' && typeof window.SPL.pauseAllPlayers == "function" && typeof window.SPL.destroy == "function") {
    //       S.pauseAllPlayers();
    //       if(this.currentPlayerInst) {
    //         S.destroy (this.currentPlayerInst,()=>{
    //           console.log('Player destroyed!!!');
    //         });
    //       }
    //     }
    //   }catch(e){console.log('Logging exception ' , e);}
    // }
    // if(this.props.wrapper && this.masterPlayerContainer) {
    //   let masterPlayer = this.masterPlayerContainer.querySelector('#'+this.props.wrapper);
    //   let defaultPlayer = this.masterPlayerContainer;
    //   this.detach(masterPlayer, defaultPlayer, true, function(reattach) {
    //     // this == elem, do stuff here, call reattach() when done!
    //     setTimeout(reattach, 100);
    //   });
    //   let event = new CustomEvent("hideMasterPlayer");
    //   document.dispatchEvent(event);
    //   document.body.classList.remove('videoplaying');
    // }
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
  }

  detach(node, next, async, fn) {
    const parent = node ? node.parentNode : null;
    // No parent node? Abort!
    if (!parent) {
      return;
    }
    // Detach node from DOM.
    parent.removeChild(node);
    // Handle case where optional `async` argument is omitted.
    if (typeof async !== "boolean") {
      fn = async;
      async = false;
    }
    // Note that if a function wasn't passed, the node won't be re-attached!
    if (fn && async) {
      // If async == true, reattach must be called manually.
      fn.call(node, reattach);
    } else if (fn) {
      // If async != true, reattach will happen automatically.
      fn.call(node);
      reattach();
    }
    // Re-attach node to dom.
    function reattach() {
      next.appendChild(node);
    }
  }

  playerload() {
    const _this = this;
    let playerId = _this.playerId;
    const videoWrapper = _this.videoWrapper;
    const masterPlayerContainer = document.querySelector(".master-player-container");

    // let imgPlaceholder = videoWrapper.querySelector("img");
    // let imgPlaceholderHeight = imgPlaceholder ? imgPlaceholder.height || imgPlaceholder.clientHeight : 250;
    // if (imgPlaceholderHeight > 125 && imgPlaceholderHeight != 300) {
    //   document.querySelector("#" + playerId).style.height = imgPlaceholderHeight + "px";
    //   masterPlayerContainer.setAttribute("data-height", imgPlaceholderHeight + "px");
    //   masterPlayerContainer.style.height = masterPlayerContainer.getAttribute("data-height");
    // } else {
    //   document.querySelector("#" + playerId).style.height = "250px";
    //   masterPlayerContainer.setAttribute("data-height", "250px");
    //   masterPlayerContainer.style.height = masterPlayerContainer.getAttribute("data-height");
    // }
    // masterPlayerContainer.classList.add("enable-video-show");
    // masterPlayerContainer.classList.remove("enable_dock");
    // document.body.classList.add("enable-video-show");
    // document.body.classList.remove("enable_dock");
    // masterPlayerContainer.setAttribute("data-videourl", _this.props["data-videourl"]);

    // if (_this.props && _this.props.manuallytriggerClick && !document.body.classList.contains("videoplaying")) {
    //   let event = new CustomEvent("dockMasterPlayer");
    //   document.dispatchEvent(event);
    // }

    // window.playerConfig = config.video;
    // AnalyticsGA.event({
    //   category: "VIDEOBEFORESLOAD",
    //   action: location.pathname,
    // });

    if (_this.props.wrapper && typeof _this.props.overlay !== "undefined" && _this.props.overlay == true) {
      playerId = _this.props.wrapper;
      const event = new CustomEvent("showVideoWithOverlay");
      document.dispatchEvent(event);
    } else if (_this.props.wrapper && (typeof _this.props.overlay === "undefined" || _this.props.overlay == false)) {
      playerId = _this.props.wrapper;
      const event = new CustomEvent("showVideoWithoutOverlay");
      document.dispatchEvent(event);
    } else {
      playerId = videoWrapper.querySelector(".default-player").getAttribute("id");
    }

    const imgPlaceholder = videoWrapper.querySelector("img");
    const imgPlaceholderHeight = imgPlaceholder ? imgPlaceholder.height || imgPlaceholder.clientHeight : 250;
    if (imgPlaceholderHeight > 125 && imgPlaceholderHeight != 300) {
      document.querySelector(`#${playerId}`).style.height = `${imgPlaceholderHeight}px`;
      masterPlayerContainer.setAttribute("data-height", `${imgPlaceholderHeight}px`);
      masterPlayerContainer.style.height = masterPlayerContainer.getAttribute("data-height");
    } else {
      document.querySelector(`#${playerId}`).style.height = "250px";
      masterPlayerContainer.setAttribute("data-height", "250px");
      masterPlayerContainer.style.height = masterPlayerContainer.getAttribute("data-height");
    }

    masterPlayerContainer.classList.add("enable-video-show");
    masterPlayerContainer.classList.remove("enable_dock");
    document.body.classList.add("enable-video-show");
    document.body.classList.remove("enable_dock");
    masterPlayerContainer.setAttribute("data-videourl", _this.props["data-videourl"]);

    if (_this.props && _this.props.manuallytriggerClick && !document.body.classList.contains("videoplaying")) {
      const event = new CustomEvent("dockMasterPlayer");
      document.dispatchEvent(event);
    }
  }

  // NEW video load  for Slike V3

  loadVideo(videoWrapper) {
    const _this = this;
    let defaults = {};

    if (defaultObj) {
      //   commented by slike
      //   defaults = JSON.parse(JSON.stringify(defaultObj));
      //   //Copy events separately as stringify doesn't copy function
      //   //TODO : create a util function for deep copying
      //   defaults.events = defaultObj.events;

      // new changes by slike
      defaults = defaultObj();
    }

    // commented by slike
    // merge defaults slikeconfig properties with template setup
    //  for (let prop in defaults) {
    //     if (_this.props.config.hasOwnProperty(prop)) {
    //       defaults[prop] = { ...defaults[prop], ..._this.props.config[prop] };
    //     }
    //   }

    // if (defaults && defaults.video && defaults.video.msid) {
    //     let apiUrl = process.env.API_BASEPOINT + "/pwa_videoshow/" + defaults.video.msid + ".cms?feedtype=sjson";
    //     fetch(apiUrl).then(
    //       data => {
    //         if (data && data.pwa_meta && data.pwa_meta.AdServingRules === "Turnoff") {
    //           defaults.player.skipAd = true;
    //         } else {
    //           defaults.player.skipAd = false;
    //         }
    //         config = Object.assign({}, _this.props.config, defaults);
    //         _this.slikeVideoLoad(videoWrapper, config);
    //       },
    //       error => {
    //         defaults.player.skipAd = true;
    //         config = Object.assign({}, _this.props.config, defaults);
    //         _this.slikeVideoLoad(videoWrapper, config);
    //       }
    //     ).catch(function (error) {
    //       defaults.player.skipAd = true;
    //       config = Object.assign({}, _this.props.config, defaults);
    //       _this.slikeVideoLoad(videoWrapper, config);
    //     });

    //   }

    // new changes by slike
    const { config = {}, playerEvents = {}, adEvents = {} } = _this.props.config;

    for (const prop in defaults.config) {
      if (config.hasOwnProperty(prop)) {
        defaults.config[prop] =
          typeof config[prop] === "object" ? { ...defaults.config[prop], ...config[prop] } : config[prop];
      }
    }
    for (const prop in defaults.playerEvents) {
      if (playerEvents.hasOwnProperty(prop)) {
        defaults.playerEvents[prop] =
          typeof playerEvents[prop] === "function" ? playerEvents[prop] : defaults.playerEvents[prop];
      }
    }
    for (const prop in defaults.adEvents) {
      if (adEvents.hasOwnProperty(prop)) {
        defaults.adEvents[prop] = typeof adEvents[prop] === "function" ? adEvents[prop] : defaults.adEvents[prop];
      }
    }

    if (defaults.config && defaults.config.video && defaults.config.video.msid) {
      const apiUrl = `${process.env.API_BASEPOINT}/pwa_videoshow/${defaults.config.video.msid}.cms?feedtype=sjson`;
      // console.log("apiUrl:",apiUrl);
      fetch(apiUrl)
        .then(
          data => {
            if (data && data.pwa_meta && data.pwa_meta.AdServingRules === "Turnoff") {
              defaults.config.player.skipAd = true;
            } else {
              defaults.config.player.skipAd = false;
            }
            _this.slikeVideoLoad(videoWrapper, defaults);
          },
          error => {
            defaults.config.player.skipAd = true;
            _this.slikeVideoLoad(videoWrapper, defaults);
          },
        )
        .catch(error => {
          defaults.config.player.skipAd = true;
          _this.slikeVideoLoad(videoWrapper, defaults);
        });
    }
  }

  // FIXME: Old video configuration for Slike V2.
  // loadVideo(videoWrapper) {
  //   let config,
  //     _this = this;
  //   let defaults = {};

  //   if (defaultObj) {
  //     defaults = JSON.parse(JSON.stringify(defaultObj));
  //     //Copy events separately as stringify doesn't copy function
  //     //TODO : create a util function for deep copying
  //     defaults.events = defaultObj.events;
  //   }

  //   //merge defaults slikeconfig properties with template setup
  //   for (let prop in defaults) {
  //     if (_this.props.config.hasOwnProperty(prop)) {
  //       defaults[prop] = { ...defaults[prop], ..._this.props.config[prop] };
  //     }
  //   }

  //   if (defaults && defaults.video && defaults.video.msid) {
  //     let apiUrl = process.env.API_BASEPOINT + "/pwa_videoshow/" + defaults.video.msid + ".cms?feedtype=sjson";
  //     fetch(apiUrl)
  //       .then(
  //         data => {
  //           if (data && data.pwa_meta && data.pwa_meta.AdServingRules === "Turnoff") {
  //             defaults.player.skipAd = true;
  //           } else {
  //             defaults.player.skipAd = false;
  //           }
  //           config = Object.assign({}, _this.props.config, defaults);
  //           _this.slikeVideoLoad(videoWrapper, config);
  //         },
  //         error => {
  //           defaults.player.skipAd = true;
  //           config = Object.assign({}, _this.props.config, defaults);
  //           _this.slikeVideoLoad(videoWrapper, config);
  //         },
  //       )
  //       .catch(function(error) {
  //         defaults.player.skipAd = true;
  //         config = Object.assign({}, _this.props.config, defaults);
  //         _this.slikeVideoLoad(videoWrapper, config);
  //       });
  //   }
  // }

  // FIXME: OLD SLIKEVIDEOLOAD function
  // slikeVideoLoad(videoWrapper, config) {
  //   let playerId;
  //   let _this = this;

  //   let masterPlayerContainer = document.querySelector(".master-player-container");
  //   if (_this.props.wrapper && typeof _this.props.inline != "undefined") {
  //     let defaultPlayer = videoWrapper.querySelector(".default-player");
  //     let masterPlayer = document.querySelector("#" + _this.props.wrapper);
  //     //TODO check with rathi why async set true
  //     _this.detach(masterPlayer, defaultPlayer, false, function(reattach) {
  //       // this == elem, do stuff here, call reattach() when done!
  //       setTimeout(reattach, 100);
  //       playerId = _this.props.wrapper;
  //     });
  //     let event = new CustomEvent("showVideoWithoutOverlay");
  //     document.dispatchEvent(event);
  //   }
  //   if (_this.props.wrapper && typeof _this.props.overlay != "undefined" && _this.props.overlay == true) {
  //     playerId = _this.props.wrapper;
  //     let event = new CustomEvent("showVideoWithOverlay");
  //     document.dispatchEvent(event);
  //   } else if (_this.props.wrapper && (typeof _this.props.overlay == "undefined" || _this.props.overlay == false)) {
  //     playerId = _this.props.wrapper;
  //     let event = new CustomEvent("showVideoWithoutOverlay");
  //     document.dispatchEvent(event);
  //   } else {
  //     playerId = videoWrapper.querySelector(".default-player").getAttribute("id");
  //   }

  //   if (typeof window.spl != "undefined" && typeof window.spl.load == "function") {
  //     _this.playerload();

  //     let ua = navigator.userAgent.toLowerCase();

  //     if (typeof os !== "undefined" && os != "ios" && process.env.PLATFORM != "desktop") {
  //       _this.masterPlayerContainer.webkitRequestFullscreen();
  //     }

  //     S.load(playerId, config, function(inst) {
  //       _this.currentPlayerInst = inst;
  //       window.currentPlayerInst = inst;
  //       let youtubeVideo =
  //         window.playerConfig.id.startsWith("YT") ||
  //         (inst.videoData && inst.videoData.AG && inst.videoData.AG == "Youtube")
  //           ? true
  //           : false;
  //       AnalyticsGA.event({
  //         category: "VIDEOREQUEST",
  //         action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""),
  //         label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
  //       });
  //       // AnalyticsGA.GTM({ category: 'VIDEOREQUEST', action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""), label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "" , event: 'tvc_videorequest'})
  //       console.log("Player load Completed");

  //       if (document.querySelector(".spl_actiondiv")) {
  //         document.querySelector(".spl_actiondiv").classList.remove("spl_actiondiv");
  //       }

  //       AnalyticsGA.event({
  //         category: "VIDEOAFTERSLOAD",
  //         action: location.pathname,
  //       });
  //     });
  //   }
  // }

  // FIXME: NEW SLIKEVIDEOLOAD function
  slikeVideoLoad(videoWrapper, dconfig) {
    let playerId;
    const _this = this;
    const { config, playerEvents, adEvents } = dconfig;
    const masterPlayerContainer = document.querySelector(".master-player-container");
    let _auds;

    if (typeof window !== "undefined" && typeof colaud !== "undefined") {
      localStorage.setItem("colaud", colaud.aud);
    }
    if (typeof colaud !== "undefined") {
      _auds = colaud.aud;
    } else if (window !== "undefined" && window.localStorage && typeof localStorage !== "undefined") {
      _auds = localStorage.getItem("colaud") ? localStorage.getItem("colaud") : null;
    }
    config.player.sg = _auds;

    // if (_this.props.wrapper && typeof (_this.props.inline) != 'undefined') {
    //   let defaultPlayer = videoWrapper.querySelector('.default-player');
    //   let masterPlayer = document.querySelector('#' + _this.props.wrapper);
    //   //TODO check with rathi why async set true
    //   _this.detach(masterPlayer, defaultPlayer, false, function (reattach) {
    //     // this == elem, do stuff here, call reattach() when done!
    //     setTimeout(reattach, 100);
    //     playerId = _this.props.wrapper;
    //   });
    //   let event = new CustomEvent("showVideoWithoutOverlay");
    //   document.dispatchEvent(event);
    // }

    // console.log("masterPlayerContainer",masterPlayerContainer);

    if (_this.props.wrapper && typeof _this.props.overlay !== "undefined" && _this.props.overlay == true) {
      playerId = _this.props.wrapper;
      const event = new CustomEvent("showVideoWithOverlay");
      document.dispatchEvent(event);
    } else if (_this.props.wrapper && (typeof _this.props.overlay === "undefined" || _this.props.overlay == false)) {
      playerId = _this.props.wrapper;
      const event = new CustomEvent("showVideoWithoutOverlay");
      document.dispatchEvent(event);
    } else {
      playerId = videoWrapper.querySelector(".default-player").getAttribute("id");
    }

    _this.playerId = playerId;
    _this.videoWrapper = videoWrapper;

    // commented by slike
    // if (typeof S != "undefined" && typeof S.load == "function") {
    // console.log("playerId :", playerId);
    // console.log("window.spl :", window.spl,window.spl.load);

    // some new changes by slike
    if (typeof window.spl !== "undefined" && typeof window.spl.load === "function") {
      _this.playerload();

      window.playerConfig = config.video;
      // AnalyticsGA.event({
      //   category: "VIDEOBEFORESLOAD",
      //   action: location.pathname,
      //   label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
      // });

      const ua = navigator.userAgent.toLowerCase();

      // if (os != "ios") {
      // _this.masterPlayerContainer.webkitRequestFullscreen();
      // }

      // new changes by slike
      const eventToFunction = function(player, eventName, data) {
        const funcName = eventName.replace("spl", "on");
        const eventData = data || {};
        if (typeof playerEvents[funcName] === "function") {
          playerEvents[funcName](player, eventName, eventData);
        } else if (typeof adEvents[funcName] === "function") {
          adEvents[funcName](player, eventName, eventData);
        }
      };

      config.contEl = playerId;
      config.player.skipAd = false;
      const fpc = _getCookie("_col_uuid");
      if (fpc) {
        config.colombiaCookieId = fpc;
      }
      this.config = config;

      function initPlayer(config) {
        _this.currentPlayerInst = new window.SlikePlayer(config);
        window.currentPlayerInst = _this.currentPlayerInst;
        Object.keys(window.SlikePlayer.Events).forEach(eventKey => {
          const eventName = window.SlikePlayer.Events[eventKey];
          _this.currentPlayerInst.on(eventName, eventToFunction.bind(null, _this.currentPlayerInst));
        });
        Object.keys(window.SlikePlayer.AdEvents).forEach(eventKey => {
          const eventName = window.SlikePlayer.AdEvents[eventKey];
          _this.currentPlayerInst.on(eventName, eventToFunction.bind(null, _this.currentPlayerInst));
        });

        // FIXME: Check if this is being fired
        const youtubeVideo = !!window.playerConfig.id.startsWith("YT");
        // AnalyticsGA.event({ category: 'VIDEOREQUEST', action:location.pathname + (youtubeVideo ? "_youtube" : ""), label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
        AnalyticsGA.event({
          category: isAudio ? "AUDIOREQUEST" : "VIDEOREQUEST",
          action: location.pathname + (youtubeVideo ? "_youtube" : ""),
          label: window.playerConfig.template ? window.playerConfig.template.toUpperCase() : "",
        });
        // AnalyticsGA.event({ category: 'VIDEOAFTERSLOAD', action: location.pathname, label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" });
      }

      // console.log("config",JSON.stringify(config));
      if (window.SlikePlayer) {
        initPlayer(config);
      } else {
        // For slike testing
        // config.env = "stg";
        // config.version = "3.1.3";
        // config.debug = true;
        window.spl.load(config, sdkLoadStatus => {
          if (sdkLoadStatus) {
            // console.log("sdkLoadStatus",sdkLoadStatus);
            initPlayer(config);
          } else {
          }
        });
      }

      // commented by slike
      //   S.load(playerId, config, function (inst) {
      //     _this.currentPlayerInst = inst;
      //     window.currentPlayerInst = inst;
      //     let youtubeVideo = (window.playerConfig.id.startsWith("YT")) || (inst.videoData && inst.videoData.AG && inst.videoData.AG == "Youtube") ? true : false;
      //     AnalyticsGA.event({ category: 'VIDEOREQUEST', action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""), label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "" })
      //     AnalyticsGA.GTM({ category: 'VIDEOREQUEST', action: window.playerConfig.seopath + (youtubeVideo ? "_youtube" : ""), label: (window.playerConfig.template) ? (window.playerConfig.template).toUpperCase() : "", event: 'tvc_videorequest'})
      //     console.log("Player load Completed");

      //     if (document.querySelector('.spl_actiondiv')) {
      //       document.querySelector('.spl_actiondiv').classList.remove('spl_actiondiv');
      //     }

      //     AnalyticsGA.event({ category: 'VIDEOAFTERSLOAD', action: location.pathname });
      //   });
    }
  }

  render() {
    const { children, imageid, lead, schema } = this.props;
    const isLead = !!lead;
    const isAudio = this.props.config.controls && this.props.config.controls.ui === "podcast";
    return (
      <React.Fragment>
        <div className={`default-outer-player ${isAudio ? "__sast" : ""}`} ref={el => (this.videoPlayerWrapper = el)}>
          {children}
          {this.props.inline && imageid ? (
            <React.Fragment>
              <ImageCard noLazyLoad={isLead} size="largewidethumb" msid={imageid} schema={schema} />
              <span className="type_icon video_icon" />
              <span className="table_col con_wrap inlinevideo">
                <span className="text_ellipsis">{this.props.config.video.title}</span>
              </span>
            </React.Fragment>
          ) : null}
          <div className={`default-player ${isAudio ? "__sast" : ""}`} id={this.defaultPlayerId} />
        </div>
        {_isCSR() ? (
          <div dangerouslySetInnerHTML={{ __html: `` }} />
        ) : isLead ? (
          <div
            className="yContainer"
            dangerouslySetInnerHTML={{
              __html: `<span>Subscribe us on</span>
                <div class="g-ytsubscribe" data-channelid=${
                  siteConfig.youtubeUrl.split("/channel/")[1]
          } data-layout="default" data-count="default"></div>`,
            }}
          />
        ) : null}
      </React.Fragment>
    );
  }
}

export default VideoPlayer;
