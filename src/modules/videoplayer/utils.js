/* eslint-disable no-restricted-globals */
/* eslint-disable no-else-return */
import { AnalyticsGA } from "../../components/lib/analytics/index";
import { isMobilePlatform } from "../../utils/util";
import { refreshAds } from "../../components/lib/ads";

/**
 * function used to create config for Slike video
 * @param {Object} item contains detail for video data
 * @param {String} subsecmsid2 value of parent section
 * @param {Object} siteConfig site related details
 * @param {String} template name of the tempaltepage
 * */

export const createConfig = (item, subsecmsid2, siteConfig, template, articledata) => {
  let adCode = "others";
  const { slikeAdCodes } = siteConfig;
  let nextVideoUrl;
  const adSection = "default";

  // const adSection = "test";

  if (subsecmsid2 && subsecmsid2 !== "") {
    // eslint-disable-next-line no-restricted-syntax
    for (const ad in slikeAdCodes) {
      if (slikeAdCodes[ad].indexOf(subsecmsid2) > -1) adCode = ad;
    }
  }

  // as per template type nextvideourl must change
  if (template === "articleshow" || template === "moviereview") {
    // if in articleshow more then 1 embeded video present , play one after other by calling articleshow_vidpostroll
    if (articledata.vdo && articledata.vdo.length > 1) {
      nextVideoUrl = getNextVideoUrl("articleshow", item.id, item.id, siteConfig);
    }
    // if in articleshow is 1 embeded video present , play from its category videos calling videpostroll_v6_slike
    else {
      nextVideoUrl = getNextVideoUrl("articleshow_video", item.id, item.id, siteConfig);
    }
  } else {
    nextVideoUrl = getNextVideoUrl(template, item.id, subsecmsid2, siteConfig);
  }

  const slikeConfig = {
    apiKey: siteConfig.slike.apikey,
    env: item.env || "",
    controls: item.controls || {},
    version: item.version || "",
    //Disabling dvr button for all videos ( dvr didnt work for fb videos)
    live: {
      loadDVR: false,
    },
    player: {
      section: adCode,
      adSection,
      midOverlayState: 1,
      slikeAdPercent: 0, // ads persent recommbed by ad team
      playlist: true,
      playlistUrl: nextVideoUrl || siteConfig.slike.nextvideo_domain + siteConfig.slike.default_pl_videolist,
    },
    video: {
      id: item.eid || item.slikeid || "",
      title: item.hl || "",
      Story: item.Story || "",
      msid: item.id || "",
      shareUrl: item.wu ? item.wu : item.override ? `${item.override}?${siteConfig.slikeshareutm}` : "",
      seopath: item.seolocation ? item.seolocation : "",
      template,
      image: ImgProp({
        msid: item.imageid ? item.imageid : item.id,
        title: item.seotitle ? item.seotitle : "",
        size: "largewidethumb",
        siteConfig,
      }).src,
    },
    // sdk: {
    //   apikey: siteConfig.slike.apikey,
    //   // apikey: "nbtweba5ec97054033e061",
    //   // apikey: ""
    // },

    subsecmsid2,
  };

  return slikeConfig;
};

const getNextVideoUrl = (videosrc, msid, parentmsid, siteConfig) => {
  // hit templates in navbharattimes for getting json for next related videos
  // set parent section msid , and current msid to skip
  let postTemplateType = "";

  if (!msid || !parentmsid || !videosrc || msid === "" || parentmsid === "" || videosrc === "") {
    return undefined;
  } else if (videosrc.indexOf("home") > -1) {
    postTemplateType = "homepage_videpostroll";
  }
  // else if (videosrc.indexOf("videoshow") > -1) {
  //   postTemplateType = "videpostroll_v5_slike";
  // } else if (videosrc.indexOf("articleshow_video") > -1 || videosrc.indexOf("liveblog") > -1) {
  //   postTemplateType = "videpostroll_v5_slike";
  // }
  else {
    postTemplateType = "videpostroll_v5_slike";
  }

  const nextVidUrl = `${siteConfig.slike.nextvideo_domain}/feeds/${postTemplateType}/${parentmsid}.cms?feedtype=json&callback=cached`;
  // const nextVidUrl = "https://langdev.indiatimes.com/pwafeeds/videpostroll_v6_slike/82368611.cms?feedtype=json&callback=cached";
  return nextVidUrl;
};

const ImgProp = prop => {
  const { siteConfig } = prop;
  const defaultOptions = {
    msid: siteConfig.imageconfig.thumbid,
    title: siteConfig.wapsitename,
    alt: siteConfig.wapsitename,
    size: "largethumb",
  };

  const Props = prop || {};

  Props.msid = Props.msid ? Props.msid : defaultOptions.msid;
  Props.title = Props.title ? Props.title : defaultOptions.title.toLowerCase().replace(" ", "-");
  Props.alt = Props.alt ? Props.alt : defaultOptions.alt.toLowerCase();
  Props.size = Props.size ? Props.size : defaultOptions.size;
  const imgsrc =
    Props.type && Props.src && Props.type === "absoluteImgSrc"
      ? Props.src
      : `${siteConfig.imageconfig.imagedomain + Props.msid},${siteConfig.imageconfig[Props.size]}/${Props.title}.jpg`;
  const img = {
    src: imgsrc,
    title: Props.title,
    size: Props.size,
    alt: Props.alt,
  };

  return img;
};

/**
 * function used to handle nextVideo for Slike
 * @param {Object} vidData contains detail for video data
 * @param {String} label   used for ga label
 * */

// eslint-disable-next-line consistent-return
export const handleNextVideo = (vidData, label) => {
  if (typeof vidData !== "object" || typeof vidData === "undefined") return false;
  let originalVideoMsid = vidData.msid;

  // msid can come with or without seopath, handling both cases
  const parsedVideoMsid =
    typeof originalVideoMsid === "string" && originalVideoMsid.includes(".cms")
      ? originalVideoMsid.slice(originalVideoMsid.lastIndexOf("/") + 1, originalVideoMsid.indexOf(".cms"))
      : originalVideoMsid;
  // reset title and href url
  document.title = vidData.title;

  if (!window.minitvType) {
    const href = `${location.origin}/${vidData.seopath}/videoshow/${parsedVideoMsid}.cms`;
    history.replaceState({}, document.title, href);
  }

  // update video config in redux store
  const updateConfigEvent = new CustomEvent("updateVideoConfig", {
    detail: { config: { Story: vidData.description || "", ...vidData, msid: parsedVideoMsid } },
  });
  document.dispatchEvent(updateConfigEvent);

  // capture pageview
  AnalyticsGA.pageview(location.pathname, {
    forcefulGaPageviewWithoutEditor: true,
  });

  const url = location.pathname;
  // Capture nextvideo event
  // AnalyticsGA.event({
  //   category: "VIDEONEXT",
  //   action: location.pathname,
  //   label,
  // });

  // handling of the ads refresh on new video in case of popup in desktop
  if (!isMobilePlatform()) {
    const adDiv = document.querySelector("#modal .mrec1");
    if (adDiv) {
      const adId = adDiv.getAttribute("data-id");
      if (adId) {
        refreshAds([adId]);
      }
    }
  }

  // playing videourl set in a global variable for ga purpose
  window.videoUrl = location.pathname;

  // update Videoshow page for Next Video
  const eventUpdateVS = new CustomEvent("updateVideoShow", {
    detail: {
      msid: parsedVideoMsid,
    },
  });
  document.dispatchEvent(eventUpdateVS);

  // updating headerdata in redux
  const headerData = { url, title: vidData.title };
  const newEvent = new CustomEvent("setVideoHeader", { detail: headerData });
  document.dispatchEvent(newEvent);

  // add new msid in reducer
  const videoMsidEvent = new CustomEvent("setPlayingVideoMsid", {
    detail: { msid: parsedVideoMsid, video: { ...vidData, msid: parsedVideoMsid } },
  });
  document.dispatchEvent(videoMsidEvent);
};

// pipswipable code is removed as not necessary. Could be taken back fromgit if ever needed

// export function isFirefox() {
//   return typeof InstallTrigger !== "undefined";
// }
