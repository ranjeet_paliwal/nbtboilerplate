import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import { getImageSrc } from "../../components/common/ImageCard/ImageCardUtils";
import { playVideoViaSlike, removeAllSlikePlayerInitializations } from "./slike";
import { _isCSR, isMobilePlatform, throttle } from "../../utils/util";
import {
  implementAutoDock,
  checkParentOfPlayerSameAsEntry,
  dockVideo,
  isYoutubeVideo,
  imageStyleChanger,
  removeEntryFromObserverList,
} from "./dockvideoplayer";
// eslint-disable-next-line no-unused-vars
import styles from "../../components/common/css/VideoPlayer.scss";
import {
  addVideoConfig,
  removeVideoConfig,
  showVideoPopup,
  removeSliderData,
} from "../../actions/videoplayer/videoplayer";

// const idGenerator = countGenerator();

// By default player is assumed to be inline
class Player extends React.PureComponent {
  constructor(props) {
    super(props);
    this.videoPlayerWrapper = null;
    this.imageWrapper = null;
    this.state = {};
    this.videoplayer = React.createRef();
    this.throttledImageClick = throttle(this.imageClick, 6000, this);
  }

  // player types : link, pop-up, inline(default)
  playerIdMaker = () => {
    if (this.props.playerWrapper) {
      return this.props.playerWrapper;
    }
    let id = "";
    let slikeId = this.props.config ? this.props.config.video.id : "";
    if (!slikeId) {
      slikeId = "NoSlikeId";
    }
    if (!this.props.type) {
      id += `inline_player_${slikeId}`;
      id = this.uniqueIdMaker(id);
    } else if (this.props.type === "pop-up") {
      id += "popup_player";
    }

    if (!id) {
      id = "default_player";
    }

    return id;
  };

  uniqueIdMaker = playerId => {
    if (this.checkPlayerExists(playerId)) {
      return this.uniqueIdMaker(`${playerId}_1`);
    }
    return playerId;
  };

  // todo article show bug for player same id fixing
  // approach by checking parent id div present on document or not
  checkPlayerExists = playerId => {
    let flag = false;
    if (typeof document !== "undefined") {
      const player = document.getElementById(playerId);
      if (player) {
        flag = true;
      }
    }
    return flag;
  };

  imageClick = () => {
    const properties = {};
    properties.config = this.props.config;
    properties.wrapper = this.videoPlayerWrapper;
    if (this.props.type === "pop-up") {
      this.props.dispatch(removeVideoConfig());
      this.props.dispatch(addVideoConfig(properties.config));
      this.openPopUp();
    } else {
      playVideoViaSlike(properties);
    }
  };

  openPopUp = () => {
    if (typeof document !== "undefined") {
      document.body.style.overflow = "hidden";
    }
    this.props.dispatch(showVideoPopup());
    this.props.dispatch(removeSliderData());
  };

  componentDidMount() {
    // autoplay handling here - only in case of autoplay on and type is inline
    if (_isCSR() && this.props.autoplay) {
      if (!this.props.type || this.props.type === "inline") {
        this.imageWrapper.click();
      }
    }

    if (this.props.autoDock) {
      implementAutoDock(this.videoplayer.current);
    }
  }

  componentWillUnmount() {
    if (this.props.autoDock) {
      removeEntryFromObserverList(this.videoPlayerWrapper);
      if (
        typeof window !== "undefined" &&
        window.currentPlayerInst &&
        typeof window.currentPlayerInst.getVideoState === "function" &&
        window.currentPlayerInst.getVideoState() &&
        !window.currentPlayerInst.getVideoState().paused &&
        !isYoutubeVideo() &&
        checkParentOfPlayerSameAsEntry(this.videoPlayerWrapper) &&
        !window.adPlaying &&
        !window.location.pathname.includes("videoshow")
      ) {
        if (this.videoplayer.current.hasChildNodes()) {
          dockVideo(this.videoplayer.current);
        } else {
          this.videoplayer.current.remove();
          removeAllSlikePlayerInitializations();
        }
        imageStyleChanger(this.videoPlayerWrapper, "block");
      } else if (window.currentPlayerInst && !window.adPlaying && window.currentPlayerInst.getVideoState().paused) {
        removeAllSlikePlayerInitializations();
      }
    }
  }

  render() {
    const { imageid, lead, schema, imgsize, config } = this.props;
    const isLead = !!lead;
    const playerId = this.playerIdMaker();
    const isAudio = this.props.config.controls && this.props.config.controls.ui === "podcast";
    const vidSEO = this.props.config && this.props.config.video && this.props.config.video.seopath;
    const imgSrc = getImageSrc({ msid: imageid, size: "amplargethumb", imgsize: "", imgver: imgsize || "" });

    return (
      <div
        className={`default-outer-player ${isAudio ? "__sast" : ""} ${isMobilePlatform() ? `mobile` : `desktop`}`}
        ref={el => {
          this.videoPlayerWrapper = el;
        }}
        id={`parent_${playerId}`}
        data-attr-slk={this.props.config ? this.props.config.video.msid : ""}
        data-video-seo={this.props.config ? vidSEO : "video"}
        style={{
          backgroundImage: `url(${imgSrc})`,
        }}
      >
        {schema && (
          <span itemProp="image" itemScope itemType="https://schema.org/ImageObject">
            <meta itemProp="url" content={imgSrc} />
            <meta content="1600" itemProp="width" />
            <meta content="900" itemProp="height" />
          </span>
        )}
        {imageid ? (
          <div
            className="image-container"
            tabIndex={0}
            role="button"
            ref={el => {
              this.imageWrapper = el;
            }}
            onClick={() => this.throttledImageClick()}
            onKeyUp={() => this.throttledImageClick()}
          >
            {isAudio && config.video.title ? <div className="audiotitle truncate">{config.video.title}</div> : null}
            {isAudio && (
              <ImageCard noLazyLoad={isLead} size="largewidethumb" msid={imageid} schema={schema} imgver={imgsize} />
            )}
          </div>
        ) : null}
        <div
          className={`default-player ${isAudio ? "__sast" : ""}`}
          id={playerId}
          ref={this.videoplayer}
          data-parent={`parent_${playerId}`}
        />
      </div>
    );
  }
}

Player.propTypes = {
  playerWrapper: PropTypes.any,
  config: PropTypes.object,
  autoplay: PropTypes.bool,
  autoDock: PropTypes.bool,
  type: PropTypes.string,
  dispatch: PropTypes.func,
  imageid: PropTypes.string,
  lead: PropTypes.bool,
  schema: PropTypes.any,
  imgsize: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    ...state.videoplayer,
  };
}

export default connect(mapStateToProps)(Player);
