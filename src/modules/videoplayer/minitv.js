import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { createConfig } from "./utils";
import {
  _getStaticConfig,
  isMobilePlatform,
  _isCSR,
  filterAlaskaData,
  throttle,
  _sessionStorage,
} from "../../utils/util";
import { playMinitv, closeMinitv } from "./slike";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import AnchorLink from "../../components/common/AnchorLink";
import { setMinitvPlaying, setMinitvWidgetClick } from "../../actions/videoplayer/videoplayer";
import "../../components/common/css/desktop/DockPortal.scss";
import SvgIcon from "../../components/common/SvgIcon";

const siteConfig = _getStaticConfig();

const videoPlayerContainerName = "minitv_container";

class MiniTv extends React.PureComponent {
  constructor(props) {
    super(props);
    this.videoplayer = React.createRef();
    this.config = createConfig(props.item, "", siteConfig, "videoshow");
    // Mini tv ad section needs to be this for tracking
    this.config.player.adSection = "live-tv-mini";

    // For minitv since size is smaller, change this to 2
    this.config.player.midOverlayState = 2;
    // this.config.player.adSection = "test";
    // Enabling this throws error of minitv related to video playlist feed
    this.config.player.playlistUrl = `${siteConfig.slike.nextvideo_domain}${siteConfig.slike.default_pl_videolist}`;
    this.config.player.playlist = true;
    this.outerMinitvContainer = "";
    if (_isCSR()) {
      this.outerMinitvContainer = document.getElementById("outer_minitv_container");
    }
    this.throttledImageClick = throttle(this.imageClick, 6000, this);
  }

  checkOrganicUserAutoplay = () => {
    const minitvOrganicUserData = filterAlaskaData(this.props.alaskaData, ["pwaconfig", "minitv"], "label");
    // console.log(minitvOrganicUserData, "minitvorganicdata");
    if (minitvOrganicUserData) {
      // eslint-disable-next-line no-underscore-dangle
      if (minitvOrganicUserData._organicautoplay && minitvOrganicUserData._organicautoplay === "true") {
        return true;
      }
    }
    return false;
  };

  checkAutoplay = () => {
    // return true;
    let autoPlay = false;
    if (this.props.item.autoplay) {
      if (this.props.item.autoplay === "false") {
        autoPlay = false;
      } else if (this.props.item.autoplay === "true") {
        // change in logic
        // if (typeof window !== "undefined") {
        //   if (window.location.href.includes("utm_source")) {
        //     autoPlay = true;
        //   }
        // }

        // logic according to referrer - if no referrer or google - no autoplay else true
        if (document && document.referrer) {
          if (document.referrer.includes("www.google.com")) {
            // in case of organic user we can handle at feed level to autoplay or not
            if (this.checkOrganicUserAutoplay()) {
              return true;
            }
            return false;
          }
          return true;
        }
        // in case of organic user we can handle at feed level to autoplay or not
        if (this.checkOrganicUserAutoplay()) {
          return true;
        }
        return false;
      }
    }
    return autoPlay;
  };

  componentDidMount() {
    if (this.outerMinitvContainer) {
      this.outerMinitvContainer.classList.remove("hide");
    }

    // Minitv ads are toggled through  feed
    const { item } = this.props;

    const skipAd = Boolean(item && item.AdServingRules && item.AdServingRules === "Turnoff");

    // Skip ads is true if above is true
    this.config.player.skipAd = skipAd;

    const muteMiniTv = Boolean(item && item.MuteOn && item.MuteOn === "mute");
    // Mute if above is true
    this.config.player.mute = muteMiniTv;

    if (this.props.isMinitvPlaying) {
      playMinitv(this.config);
    } else if (this.checkAutoplay()) {
      playMinitv(this.config, true);
      this.props.dispatch(setMinitvPlaying(true));
    }
  }

  componentWillUnmount() {
    if (this.outerMinitvContainer) {
      this.outerMinitvContainer.classList.add("hide");
    }
  }

  imageClick = () => {
    this.props.dispatch(setMinitvPlaying(true));
    if (isMobilePlatform()) {
      this.props.dispatch(setMinitvWidgetClick(true));
    }
    playMinitv(this.config);
  };

  closeMinitv = () => {
    this.props.dispatch(setMinitvPlaying(false));

    closeMinitv();
    _sessionStorage().set("MINITV_CLOSED", true);
  };

  render() {
    const { item } = this.props;
    const { imageid } = item;

    const playerId = videoPlayerContainerName;
    return (
      <div className="minitv-container">
        <button type="button" className="close_icon" onClick={() => this.closeMinitv()} />
        {!isMobilePlatform() && <SvgIcon name="drag-icon" className="dragicon" />}
        {!isMobilePlatform() && (
          <span className="video-header">
            <AnchorLink href="" preventDefault>
              {item.hl}
            </AnchorLink>
          </span>
        )}
        <div
          className={`default-outer-player ${isMobilePlatform() ? `mobile` : `desktop`}`}
          ref={el => {
            this.videoPlayerWrapper = el;
          }}
          id={`parent_${playerId}`}
        >
          {imageid ? (
            <div
              className={`image-container ${this.props.isMinitvPlaying ? `hide` : ``}`}
              role="button"
              tabIndex={0}
              ref={el => {
                this.imageWrapper = el;
              }}
              onClick={() => this.throttledImageClick()}
              onKeyUp={() => this.throttledImageClick()}
            >
              <ImageCard noLazyLoad size="largewidethumb" msid={imageid} />
            </div>
          ) : null}
          <div
            className={`default-player ${this.props.isMinitvPlaying ? `` : `hide`}`}
            id={playerId}
            ref={this.videoplayer}
            // data-parent={`parent_${playerId}`}
          />
          {isMobilePlatform() && (
            <div
              role="button"
              tabIndex={0}
              className="transparent-layer"
              onClick={() => this.throttledImageClick()}
              onKeyUp={() => this.throttledImageClick()}
            />
          )}
        </div>
      </div>
    );
  }
}

MiniTv.propTypes = {
  dispatch: PropTypes.func,
  item: PropTypes.object,
  isMinitvPlaying: PropTypes.bool,
  alaskaData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    ...state.videoplayer,
    alaskaData: state.header.alaskaData,
  };
}

export default connect(mapStateToProps)(MiniTv);
