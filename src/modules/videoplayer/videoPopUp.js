/* eslint-disable indent */
import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Modal from "../modal/modal";
import { AnalyticsGA } from "../../components/lib/analytics/index";
import {
  _isCSR,
  _getStaticConfig,
  isMobilePlatform,
  throttle,
  _sessionStorage,
  _deferredDeeplink,
  getMobileOs,
} from "../../utils/util";
import Slider from "../../components/desktop/Slider/index";
import ErrorBoundary from "../../components/lib/errorboundery/ErrorBoundary";
import SocialShare from "../../components/common/SocialShare";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import Adcard from "../../components/common/AdCard";
import { refreshAds } from "../../components/lib/ads/index";
import { createConfig } from "./utils";
import {
  hideVideoPopup,
  addPrevURLPopup,
  setMinitvPlaying,
  setMinitvWidgetClick,
  fetchMiniTVMoreVideosData,
} from "../../actions/videoplayer/videoplayer";
import { playVideo, closeMinitv, removeAllSlikePlayerInitializations, fireVideoPlayedGRXEvent } from "./slike";
import { dockVideo } from "./dockvideoplayer";
import YSubscribeCard from "../../components/common/YSubscribeCard";
import SvgIcon from "../../components/common/SvgIcon";

const siteConfig = _getStaticConfig();
const appdeeplink = siteConfig.appdeeplink;
const playerId = "video_pop_up_player";
class VideoPopUp extends React.Component {
  constructor(props) {
    super(props);
    this.videoPlayer = React.createRef();
    this.adDiv = React.createRef();
    this.throttledSliderClick = throttle(this.sliderClick, 6000, this);
  }

  onBackButtonEvent = () => {
    if (this.props.showPopup) {
      // If minitv is playing / started to initialize and back is pressed, close popup.
      if (this.props.isMinitvPlaying || this.props.isMinitvClicked) {
        this.closePopUp();
      } else if (this.props.videoType === "slike") {
        this.dockVideoAndCloseModal();
      } else {
        this.closePopUp();
      }
    }
  };

  componentDidMount() {
    window.addEventListener("popstate", this.onBackButtonEvent);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.showPopup !== this.props.showPopup) {
      if (this.props.showPopup) {
        const elmDesc = document.querySelector(".popup-slider .videodesc");
        let isMinitv;
        if (this.props.isMinitvClicked) {
          isMinitv = true;
          this.props.dispatch(setMinitvWidgetClick(false));
          this.props.dispatch(fetchMiniTVMoreVideosData())
        }
        playVideo(this.props.videoConfig, playerId, isMinitv);
        // eslint-disable-next-line no-restricted-globals
        const url = location.pathname;
        this.props.dispatch(addPrevURLPopup(url));
        if (elmDesc && elmDesc.scrollHeight < 40) {
          elmDesc.classList.add("hidemoreicon");
          // need to wirte code for hiding arrow
        }
      }
    }
  }

  componentWillUnmount() {
    window.addEventListener("popstate", this.onBackButtonEvent);
  }

  closePopUp = fromButton => {
    if (isMobilePlatform()) {
      if (this.props.isMinitvPlaying || this.props.isMinitvClicked) {
        closeMinitv();
        _sessionStorage().set("MINITV_CLOSED", true);
      }

      this.props.dispatch(setMinitvPlaying(false));
      window.minitvType = "";
    }
    if (document) {
      document.body.style.overflow = "scroll";
    }
    // if (!this.props.isMinitvPlaying && !this.props.isMinitvClicked) {
    //   fireVideoPlayedGRXEvent();
    // }

    if (fromButton) {
      AnalyticsGA.event({
        category: "Video Layer",
        action: "close",
        label: window.videoUrl || "",
      });
      // Safely removes the containers and fires video played event
      if (!this.props.isMinitvClicked && !this.props.isMinitvPlaying) {
        removeAllSlikePlayerInitializations();
      }
    }
    this.props.dispatch(hideVideoPopup());
    window.history.pushState({}, "", this.props.prevUrlPopup);
    if (window.undockToPopup) {
      window.undockToPopup = false;
    }
  };

  dockVideoAndCloseModal = () => {
    dockVideo(this.videoPlayer.current, true);
    this.closePopUp();
    // this.props.onClose();
  };

  getShareDetail() {
    const videoData = this.props.videoConfig.video;
    const sharedata = {
      title: videoData.title,
      url: videoData.shareUrl,
      short_url: "",
      msid: videoData.id,
    };
    return sharedata;
  }

  // This is code is for firing ga event and changing url
  // It works for videopopup video play only
  pushUrlAndFireGA = data => {
    if (!data) {
      return;
    }
    let url;
    try {
      url = new URL(data);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
    }
    if (url) {
      try {
        window.history.pushState({}, "", url.pathname);

        const sendurl = `/popup/${url.pathname}`;
        // Additional param in pageview used by grx to know that this is a popup
        AnalyticsGA.pageview(sendurl, { is_popup_video: true });
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    }
  };

  sliderClick = item => {
    if (!isMobilePlatform()) {
      const parentDiv = document.getElementById(`parent_${playerId}`);
      if (parentDiv) {
        parentDiv.scrollIntoView({ behavior: "smooth", block: "start" });
      }
    }
    let adId;
    if (this.adDiv.current && this.adDiv.current.adElem) {
      adId = this.adDiv.current.adElem.getAttribute("data-id");
    }
    if (adId) {
      refreshAds([adId]);
    }
    // if (item.override) {
    //   this.pushUrlAndFireGA(item.override);
    // }
    const config = createConfig(item, this.props.videoConfig.subsecmsid2 || "", siteConfig, "videoshow");
    const videoPlayerContainerName = playerId;
    playVideo(config, videoPlayerContainerName);
  };

  // refreshMrecAds = () => {
  // refreshAds(["mrec1"]);
  // };

  showMoreDesc = elm => {
    // const hh = elm.target.scrollHeight;
    const classList = elm.target.classList;
    if (classList.contains("expanded")) {
      classList.remove("expanded");
    } else {
      classList.add("expanded");
    }
  };

  render() {
    const { videoConfig } = this.props;
    const playerConfig = {
      template: "videoshow",
      seopath: "",
    };
    let isAudio = this.props.videoConfig.controls && this.props.videoConfig.controls.ui === "podcast";
    return _isCSR() ? (
      <Modal showModal={this.props.showPopup} closeModal={() => this.closePopUp(true)}>
        <div className="row inner-content">
          <div className="col8 advert-player">
            <div className="inner">
              <div className="thumb_video">
                <div className={`default-outer-player ${isAudio ? "__sast" : ""}`} id={`parent_${playerId}`}>
                  <div
                    className="image-container"
                    style={{
                      width: `${typeof isMobilePlatform === "function" && isMobilePlatform() ? "100%" : "630"}px`,
                      height: `${typeof isMobilePlatform === "function" && isMobilePlatform() ? "200" : "354"}px`,
                    }}
                  >
                    <ImageCard noLazyLoad size="largewidethumb" msid={this.props.msid} />
                  </div>
                  <div
                    className={`default-player ${isAudio ? "__sast" : ""}`}
                    id={playerId}
                    ref={this.videoPlayer}
                    data-parent={`parent_${playerId}`}
                  />
                </div>
              </div>
              {!isAudio && (
                <div className="con_social">
                  <div id="popup-ycontainer">
                    <YSubscribeCard />
                  </div>
                  <div className="right_icons">
                    {!this.props.isMinitvPlaying && <SocialShare sharedata={() => this.getShareDetail()} />}
                    {!this.props.isAdPlaying && this.props.videoType === "slike" && !this.props.isMinitvPlaying && (
                      <button type="button" onClick={() => this.dockVideoAndCloseModal()} className="dock_icon">
                        Dock
                      </button>
                    )}
                    <div className="app_in_modal">
                      <a
                        className="appdownload_icon"
                        rel="nofollow"
                        title="download app"
                        href={_deferredDeeplink("hrnav", appdeeplink)}
                      >
                        {getMobileOs() == "android" ? (
                          <SvgIcon name="android" className="android" />
                        ) : getMobileOs() == "ios" ? (
                            <SvgIcon name="apple" className="ios" />
                        ) : null}
                      </a>
                    </div>
                  </div>
                </div>
              )}
              {!isAudio && !isMobilePlatform() && (
                <div className="videotitle">
                  {this.props.videoConfig.video ? this.props.videoConfig.video.title : ""}
                </div>
              )}
            </div>
          </div>
          {!isMobilePlatform() && (
            <div className="col4 advert-left-ad">
              <Adcard ref={this.adDiv} mstype="mrec1" adtype="dfp" pagetype="videoshow" />

              {videoConfig && videoConfig.video && videoConfig.video.Story && (
                <span
                  className="videodesc"
                  dangerouslySetInnerHTML={{ __html: videoConfig && videoConfig.video && videoConfig.video.Story }}
                ></span>
              )}
            </div>
          )}

          {isMobilePlatform() && _isCSR()
            ? ReactDOM.createPortal(
                <a
                  className="btn_openinapp modal_openapp"
                  data-attr="btn_openinapp"
                  href={_deferredDeeplink(
                    "videoshow",
                    siteConfig.appdeeplink,
                    this.props.msid,
                    null,
                    null,
                    "videoshow",
                  )}
                >
                  {siteConfig.locale.see_in_app}
                </a>,
                document.getElementById("share-container-wrapper"),
              )
            : null}

          <div className="col12 popup-slider">
            {isMobilePlatform() && videoConfig && videoConfig.video && videoConfig.video.Story && (
              <span
                onClick={this.showMoreDesc.bind(this)}
                className="videodesc"
                dangerouslySetInnerHTML={{ __html: videoConfig && videoConfig.video && videoConfig.video.Story }}
              ></span>
            )}
            {this.props.sliderData && this.props.sliderData.length > 0 && (
              <React.Fragment>
                {isMobilePlatform() && (
                  <div className="section">
                    <div className="top_section">
                      <h3>
                        <span>{siteConfig.locale.related_videos}</span>
                      </h3>
                    </div>
                  </div>
                )}
                <ErrorBoundary>
                  <Slider
                    type="grid"
                    className="videopopup"
                    size="5"
                    width={`${typeof isMobilePlatform === "function" && isMobilePlatform() ? "320" : "221"}`}
                    height="125"
                    sliderData={this.props.sliderData}
                    margin={`${typeof isMobilePlatform === "function" && isMobilePlatform() ? "0" : "15"}px`}
                    sliderClick={this.throttledSliderClick}
                    playerConfig={playerConfig}
                  />
                </ErrorBoundary>
              </React.Fragment>
            )}
          </div>
        </div>
      </Modal>
    ) : null;
  }
}

VideoPopUp.propTypes = {
  dispatch: PropTypes.func,
  showPopup: PropTypes.bool,
  videoType: PropTypes.string,
  isMinitvClicked: PropTypes.bool,
  videoConfig: PropTypes.object,
  prevUrlPopup: PropTypes.string,
  isMinitvPlaying: PropTypes.bool,
  sliderData: PropTypes.array,
  msid: PropTypes.any,
  isAdPlaying: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.videoplayer,
  };
}

export default connect(mapStateToProps)(VideoPopUp);
