/* eslint-disable no-plusplus */
/* eslint-disable import/no-mutable-exports */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import AnchorLink from "../../components/common/AnchorLink";
import { _isCSR, isMobilePlatform } from "../../utils/util";
import { AnalyticsGA } from "../../components/lib/analytics/index";
import { removeAllSlikePlayerInitializations } from "./slike";
// import { browserHistory } from "react-router";

let observer;
let undockVideo;
let dockVideo;
let closeVideo;
let implementAutoDock;
let removeEntryFromObserverList;
let videoPlayerDiv;
let showButtons;
let hideButtons;
let showDockPortal;
let hideDockPortal;
let checkParentOfPlayerSameAsEntry;
let isYoutubeVideo;
let showDockContainer;
let imageStyleChanger;

const divRefs = [];

const options = {
  root: null,
  threshold: 0.00001,
};

const callback = entries => {
  entries.forEach(entry => {
    if (
      window.currentPlayerInst &&
      !isYoutubeVideo() &&
      checkParentOfPlayerSameAsEntry(entry.target) &&
      !window.adPlaying
    ) {
      if (entry.intersectionRatio > options.threshold) {
        undockVideo();
      } else {
        // remove unnecessary player divs if present
        const childEntities = entry.target.getElementsByClassName("default-player");
        if (childEntities && childEntities.length > 1) {
          for (let i = 0; i < childEntities.length; i++) {
            if (!childEntities[i].hasChildNodes()) {
              childEntities[i].remove();
            }
          }
        }
        const child = entry.target.getElementsByClassName("default-player")[0];
        if (child && child.hasChildNodes()) {
          dockVideo(child);
          imageStyleChanger(entry.target, "block");
        }
      }
    }
  });
};

isYoutubeVideo = () => {
  let isYoutube = false;
  //FIXME: Changed method to determine video type as per slike
  if (window.currentPlayerInst.store.playerType.includes("yt")) {
    isYoutube = true;
  }
  return isYoutube;
};

checkParentOfPlayerSameAsEntry = parent => {
  const parentName = parent.getAttribute("id");
  //FIXME: Changed actualplayer div as per slike
  // const actualPlayer = window.currentPlayerInst.playerDiv;
  const actualPlayer = window.currentPlayerInst.ui.controlLayer;
  const player = actualPlayer.closest(".default-player");
  const parentPlayerName = player ? player.getAttribute("data-parent") : "";
  return parentName === parentPlayerName;
};

if (_isCSR()) {
  if ("IntersectionObserver" in window) {
    observer = new IntersectionObserver(callback, options);
  }

  showDockContainer = () => {
    showDockPortal();
    showButtons();
  };

  showDockPortal = () => {
    document.getElementById("dock-root-container").classList.remove("hide");
  };

  hideDockPortal = () => {
    document.getElementById("dock-root-container").classList.add("hide");
  };

  showButtons = () => {
    const buttons = videoPlayerDiv.getElementsByTagName("button");
    if (buttons && buttons.length) {
      for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove("hide");
      }
    }
  };

  hideButtons = () => {
    const buttons = videoPlayerDiv.getElementsByTagName("button");
    if (buttons && buttons.length) {
      for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.add("hide");
      }
    }
  };

  undockVideo = fromButton => {
    hideDockPortal();
    hideButtons();
    const playerDiv = videoPlayerDiv.getElementsByClassName("default-player")[0];
    // if (isMinitv) {
    //   playerDiv = document
    //     .getElementById("outer_minitv_container")
    //     .getElementsByClassName("default-player")[0];
    // } else {
    //   playerDiv = videoPlayerDiv.getElementsByClassName("default-player")[0];
    // }
    if (playerDiv) {
      const targetDivName = playerDiv.getAttribute("data-parent");
      const navBar = document.getElementsByClassName(isMobilePlatform() ? "nav_scroll" : "first-level-menu");
      const navBarHeight = navBar[0] && navBar[0].offsetHeight;
      const targetDiv = document.getElementById(targetDivName);
      if (targetDiv) {
        imageStyleChanger(targetDiv, "none");
        targetDiv.appendChild(playerDiv);
        // targetDiv.scrollIntoView({ behavior: "smooth", block: "start" });
        window.scrollTo({
          top:
            targetDiv.getBoundingClientRect().top +
            (window.pageYOffset || document.documentElement.scrollTop) -
            (navBarHeight ? navBarHeight : 0),
          behavior: "smooth",
        });
      } else {
        playerDiv.remove();
        const event = new CustomEvent("showVideoPopup", { detail: "" });
        document.dispatchEvent(event);

        // code to refrain the video from making ga calls when clicked from button using flag
        window.undockToPopup = true;

        // code for redirecting to videoshow page
        // browserHistory.push(
        //   "/news-video/news/telengana-woman-doctor-disha-case-encounter-place-video/videoshow/72398639.cms"
        // );
      }

      if (fromButton) {
        // AnalyticsGA.event({
        //   category: "PIP UnDock",
        //   action: "click",
        //   label: window.videoUrl || "",
        // });
      } else {
        AnalyticsGA.event({
          category: "PIP UnDock",
          action: "default",
          label: window.videoUrl || "",
        });
      }
    }
    if (window && window.currentPlayerInst) {
      // Add resizing with mini timeout to prevent docking issue
      setTimeout(() => window.currentPlayerInst.bpl.adjustDisplayWindow(), 500);
    }
  };

  imageStyleChanger = (parentElement, style) => {
    const imageContainer = parentElement.getElementsByClassName("image-container")[0];
    if (imageContainer) {
      imageContainer.style.display = style;
    }
  };

  dockVideo = (childRef, fromButton) => {
    closeVideo();
    if (childRef) {
      videoPlayerDiv.appendChild(childRef);
      // showDockPortal();
      // showButtons();
      showDockContainer();
      if (fromButton) {
        AnalyticsGA.event({
          category: "PIP Dock",
          action: "dockVideo",
          label: window.videoUrl || "",
        });
        // AnalyticsGA.GTM({
        //   category: "PIP Dock",
        //   action: "dockVideo",
        //   label: window.videoUrl || "",
        //   event: "tvc_pipdock",
        // });
      } else {
        AnalyticsGA.event({
          category: "PIP Dock",
          action: "default",
          label: window.videoUrl || "",
        });
        // AnalyticsGA.GTM({
        //   category: "PIP Dock",
        //   action: "default",
        //   label: window.videoUrl || "",
        //   event: "tvc_pipdock",
        // });
      }
    }
    if (window && window.currentPlayerInst) {
      // Add resizing with mini timeout to prevent docking issue
      setTimeout(() => window.currentPlayerInst.bpl.adjustDisplayWindow(), 500);
    }
  };
  // This function is for docked video which tries to:
  // * Pause the video (if clicked from button and then put back in original place ) Preserving video state
  // * If user is on a different page where the video cannot be put back into original place,  just  remove parent div of video.
  closeVideo = clickedFromButton => {
    
    const parentDiv = videoPlayerDiv.getElementsByClassName("default-player")[0];
    if (parentDiv) {
      const targetDivName = parentDiv.getAttribute("data-parent");
      const targetDiv = document.getElementById(targetDivName);
      if (targetDiv) {
        targetDiv.appendChild(parentDiv);
        const imageContainer = targetDiv.querySelector(".image-container");
        if (imageContainer) {
          imageContainer.style.display = "none";
        }
      } else {
        // This is the case where we are on a different page and target div doesn't exist anymore ( to dock back to)
        // The following function removes the associated divs safely
        removeAllSlikePlayerInitializations();
        parentDiv.remove();
      }
    }

    // Calling this here ensures video_played event gets fired correctly 
    // As it checks whether current video was pip or not based on docked video container class
    // These functions change that class
    hideDockPortal();
    hideButtons();

    if (clickedFromButton) {
      if (window.currentPlayerInst) {
        window.currentPlayerInst.pause();
      }
      AnalyticsGA.event({
        category: "PIP Close",
        action: "Button",
        label: window.videoUrl || "",
      });
    }
    if (window && window.currentPlayerInst) {
      // Add resizing with mini timeout to prevent docking issue
      setTimeout(() => window.currentPlayerInst.bpl.adjustDisplayWindow(), 500);
    }
  };

  implementAutoDock = divRef => {
    if (divRefs.indexOf(divRef.parentElement) === -1) {
      if (divRef && divRef.parentElement) {
        divRefs.push(divRef.parentElement);
        divRefs.forEach(div => {
          observer.observe(div);
        });
      }
    }
  };

  removeEntryFromObserverList = divRef => {
    if (divRefs.indexOf(divRef) > -1) {
      observer.unobserve(divRef);
      divRefs.splice(divRefs.indexOf(divRef), 1);
    }
  };
}

class NewDockVideoPlayer extends React.Component {
  undockContainer = () => {
    if (isMobilePlatform()) {
      undockVideo(true);
    }
  };

  componentDidMount() {
    videoPlayerDiv = document.getElementById("dock-video-player");
  }

  render() {
    let isAudio = this.props.videoConfig.controls && this.props.videoConfig.controls.ui === "podcast";
    let showUndockIcon =
      this.props.videoType && this.props.videoType === "slike" && !this.props.isAdPlaying
        ? isAudio
          ? true
          : !isMobilePlatform()
          ? true
          : false
        : false;
    return (
      <div id="dock-video-player" className={`dock-video-player ${isMobilePlatform() ? `mobile` : `desktop`}`}>
        {showUndockIcon && (
          <button type="button" onClick={() => undockVideo(true)} className="undock_icon hide">
            Undock
          </button>
        )}
        <button type="button" onClick={() => closeVideo(true)} className="close_icon hide">
          Close
        </button>

        {!isMobilePlatform() && !isAudio && (
          <span className="video-header">
            <AnchorLink href={this.props.videoUrl}>{this.props.videoHeader}</AnchorLink>
          </span>
        )}
        {isMobilePlatform() &&
          !isAudio &&
          this.props.videoType &&
          this.props.videoType === "slike" &&
          !this.props.isAdPlaying && (
            <div
              role="button"
              tabIndex={0}
              className="transparent-layer"
              onClick={() => this.undockContainer()}
              onKeyUp={() => this.undockContainer()}
            />
          )}
      </div>
    );
  }
}

NewDockVideoPlayer.propTypes = {
  videoType: PropTypes.string,
  videoUrl: PropTypes.string,
  videoHeader: PropTypes.string,
  isAdPlaying: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.videoplayer,
  };
}

const DockVideoPlayer = connect(mapStateToProps)(NewDockVideoPlayer);

export {
  DockVideoPlayer,
  dockVideo,
  undockVideo,
  implementAutoDock,
  closeVideo,
  showDockContainer,
  checkParentOfPlayerSameAsEntry,
  isYoutubeVideo,
  imageStyleChanger,
  removeEntryFromObserverList,
};
