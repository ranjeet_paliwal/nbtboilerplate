import React from "react";
import ReactDOM from "react-dom";
import { DockVideoPlayer } from "../videoplayer/dockvideoplayer";
import "../../components/common/css/desktop/DockPortal.scss";

const DockPortal = () => {
  return ReactDOM.createPortal(
    <div>
      <DockVideoPlayer />
    </div>,
    document.getElementById("dock-root-container"),
  );
};

export default DockPortal;
