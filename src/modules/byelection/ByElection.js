import React, { Component } from "react";
import { _getStaticConfig, isSCR } from "../../utils/util";
import AnchorLink from "../../components/common/AnchorLink";
const siteConfig = _getStaticConfig();

class ByElectionWidget extends Component {
  constructor(props) {
    super(props);
    this.config = {
      Interval: 30000
    };
    this.state = {
      json: null
    };
  }

  refresh() {
    let _this = this;
    _this.config.timer = setInterval(() => {
      this.ByElectionContent(); //refetch latest content
    }, _this.config.Interval);
  }
  ByElectionContent() {
    let _this = this;
    let { apiUrl } = this.props;
    //if api url exist then show data of particlaur api else by default api data will shown
    let URL = apiUrl ? apiUrl : siteConfig.byelection;

    fetch(URL)
      .then(function(response) {
        let myData = response.json();
        return myData;
      })
      .then(function(json) {
        _this.setState({ json: json });
      })
      .catch(function(ex) {
        //console.log('parsing failed', ex)
      });
  }
  componentDidMount() {
    !this.props.pagetype || this.props.pagetype != "byelection"
      ? this.ByElectionContent()
      : null; //call first time directly when page load//
    this.refresh(); //set interval
  }
  componentWillUnmount() {
    clearInterval(this.config.timer);
  }

  render() {
    let _this = this;
    let headinghyperlink =
      siteConfig && siteConfig.byelectionurl ? siteConfig.byelectionurl : "";
    //this.props.pagetype && this.props.pagetype=='byelection' ? _this.setState({ json : this.props.ByElectioncontent }) : null;
    let ByElectionData =
      !this.state.json &&
      this.props.ByElectioncontent &&
      this.props.ByElectioncontent.json &&
      this.props.ByElectioncontent.json.values &&
      this.props.ByElectioncontent.json.values.length > 0
        ? this.props.ByElectioncontent.json.values
        : _this.state && _this.state.json && _this.state.json.json
        ? _this.state.json.json.values
        : null;
    return ByElectionData && ByElectionData.length > 0 ? (
      <div className="ele_table_result">
        {this.props.heading == false ? null : (
          <AnchorLink href={headinghyperlink}>
            <h2>
              {siteConfig.locale.by_election
                ? siteConfig.locale.by_election
                : "Assembly By-Election"}
            </h2>
          </AnchorLink>
        )}
        <div className="election_table" data-exclude="amp">
          <table cellPadding="0" cellSpacing="0">
            <tbody>
              <tr key="ByElectionrowhead">
                {Object.keys(ByElectionData).map(function(key, index) {
                  return typeof ByElectionData[key] == "string" ? (
                    <td key={`ByElectionhead${index}`}>
                      {ByElectionData[key]}
                    </td>
                  ) : null;
                })}
              </tr>
              {Object.keys(ByElectionData).map(function(key, index) {
                return ByElectionData[key] instanceof Array &&
                  typeof ByElectionData[key] == "object" ? (
                  <tr key={`ByElectionrow${index}`}>
                    {ByElectionData[key].map(function(item, index) {
                      return item != "-" ? (
                        <td
                          key={`ByElection${index}`}
                          className={item == "-" ? "hide" : ""}
                        >
                          {item != "-" ? item : ""}
                        </td>
                      ) : null;
                    })}
                  </tr>
                ) : null;
              })}
            </tbody>
          </table>
        </div>
      </div>
    ) : null;
  }
}

export default ByElectionWidget;
