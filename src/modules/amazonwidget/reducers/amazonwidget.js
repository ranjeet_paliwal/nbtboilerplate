import {
  FETCH_AMAZON_REQUEST,
  FETCH_AMAZON_SUCCESS,
  FETCH_AMAZON_NEXT_SUCCESS,
  FETCH_AMAZON_FAILURE
} from "./../actions/amazonwidget";

function amazonwidget(
  state = {
    isFetching: false,
    error: false
  },
  action
) {
  switch (action.type) {
    case FETCH_AMAZON_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_AMAZON_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_AMAZON_SUCCESS:
      //state.value = action.payload ? action.payload : "";
      state.items = []; // empty storylist items
      state.items[0] = action.payload ? action.payload : "";
      return {
        ...state,
        isFetching: false,
        error: false
      };
    case FETCH_AMAZON_NEXT_SUCCESS:
      //state.value = action.payload ? action.payload : "";
      let item = action.payload;
      if (action.params && action.params.amazonkey)
        item.amazonkey = action.params.amazonkey;

      if (
        action.params &&
        action.params.pagename &&
        action.params.pagename == "articleshow"
      ) {
        let preserve = state.items;
        state.items = []; // empty storylist items
        if (preserve) state.items = preserve.concat(item);
        else state.items[0] = item;
      } else {
        state.items = []; // empty storylist items
        state.items[0] = item;
      }
      return {
        ...state,
        isFetching: false,
        error: false
      };
    default:
      return state;
  }
}

export default amazonwidget;
