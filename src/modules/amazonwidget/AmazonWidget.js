import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./../../components/common/css/PaytmWidget.scss";
import CommonSlideWidget from "./../../components/common/CommonSlideWidget";
import { checkIsAmpPage, _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();

export class AmazonWidget extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, params, query, subsec, router } = this.props;
    // dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router));
    // Below data is fetching for photoshow page
    // dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router));
  }

  shuffle(array) {
    var currentIndex = array.length,
      temporaryValue,
      randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  render() {
    const { items, params, pagename, subsec1, index, subsec, router, position, type } = this.props;
    let classname = position == "incontent" ? " amz_strip" : "";
    const amzIndex = index ? index : 0;
    return (
      <div className="amazon">
        {type == "amazondeal" && items && items instanceof Array ? (
          amazonDeal(items[0], classname)
        ) : type == "amazonMobile" && items && items instanceof Array ? (
          amazonMobile(items[0], pagename, subsec)
        ) : (
          <CommonSlideWidget index={amzIndex} router={router} pagename={pagename} {...this.props} />
        )}
      </div>
    );
  }
}

const shuffle = array => {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

const amazonMobile = (value, pagename, subsec) => {
  return value != undefined && value.product != undefined && process.env.SITE == "nbt" ? (
    <div className="_paytmsponsored wdt_amazon">
      <h3>
        यहा पर खरीदें <img src="https://navbharattimes.indiatimes.com/photo/58606011.cms" width="80" />
      </h3>
      <ul>
        {value.product.map((item, index) => {
          let url = item.url ? item.url : "";
          let title = item.titleEn ? item.titleEn : item.title;
          let price = item.price ? item.price : "";
          // let prodUrl = 'https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=' + getURL(url) + '&tag=mweb_nbt_sponsored_' + pagename + '&title=' + removespace(title) + '&price=' + price + '&utm_source=gadgetsnow&'
          let prodUrl =
            "https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=" +
            getURL(url) +
            "&utm_campaign=" +
            getTagName(pagename, subsec) +
            "&title=" +
            removespace(title) +
            "&price=" +
            price +
            "&utm_source=" +
            pagename +
            "&utm_medium=Affiliate";
          return (
            // index < 3 ?
            <li className="_paytm_card" key={"paytm" + index}>
              <a target="_blank" rel="nofollow" href={prodUrl}>
                <div className="wgt-paytm tbl">
                  <div className="items tbl_row">
                    <div className="tbl_col prod_img">
                      {typeof item.imageUrl == "string" ? (
                        <img src={item.imageUrl} alt={item.titleEn} title={item.title} />
                      ) : (
                        <img
                          src={"https://navbharattimes.indiatimes.com/photo/" + siteConfig.imageconfig.thumbid + ".cms"}
                          alt={item.titleEn}
                          title={item.title}
                        />
                      )}
                    </div>
                    <div className="des tbl_col">
                      {item.title && item.title != "" ? (
                        <div className="heading">
                          <span className="text_ellipsis">{item.title}</span>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div className="tbl _paytm_price">
                  <div className="tbl_row">
                    <div className="tbl_col">
                      {item.price && item.price != "" ? (
                        <span className="main-price">&#x20b9; {item.price.split(".")[0]}</span>
                      ) : null}
                    </div>
                    <div className="tbl_col rgt-btn">
                      <span className="btn-buy">खरीदे</span>
                    </div>
                  </div>
                </div>
              </a>
            </li>
            // : null
            /*: null*/
           /*: null*/);
        })}
      </ul>
    </div>
  ) : null;
};

const amazonDeal = (value, classname) => {
  if (value && value.product && value.product.length > 0) {
    value.product = shuffle(value.product);
  }
  return value != undefined && value.product != undefined ? (
    <div className={"_paytmsponsored wdt_amazon" + classname}>
      <h3>{siteConfig.locale.amazonDeals}</h3>
      {/* <img src="https://navbharattimes.indiatimes.com/photo/58606011.cms" width="80" /> */}
      <ul>
        {value.product.map((item, index) => {
          let url = item.url ? item.url : "";
          let title = item.titleEn ? item.titleEn : item.title;
          let price = item.price ? item.price : "";
          // let prodUrl = 'https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=' + getURL(url) + '&tag=mweb_nbt_sponsored_' + pagename + '&title=' + removespace(title) + '&price=' + price + '&utm_source=gadgetsnow&'
          // let prodUrl = 'https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=' + getURL(url) + '&utm_campaign=' + getTagName(pagename, subsec) + '&title=' + removespace(title) + '&price=' + price + '&utm_source=' + pagename + '&utm_medium=Affiliate'
          let tag =
            siteConfig.channelCode === "eisamay"
              ? "eis"
              : siteConfig.channelCode === "ml"
              ? "mly"
              : siteConfig.channelCode;
          let prodUrl = url + "&tag=" + tag + "_as_feed-21";
          return (
            // index < 3 ?
            <li className="_paytm_card" key={"paytm" + index}>
              <a target="_blank" rel="nofollow" href={prodUrl}>
                <div className="wgt-paytm tbl">
                  <div className="items tbl_row">
                    <div className="tbl_col prod_img">
                      {typeof item.imageUrl == "string" ? (
                        <img src={item.imageUrl} alt={item.titleEn} title={item.title} />
                      ) : (
                        <img
                          src={"https://navbharattimes.indiatimes.com/photo/" + siteConfig.imageconfig.thumbid + ".cms"}
                          alt={item.titleEn}
                          title={item.title}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <div className="tbl amz_strip_txt">
                  <div className="tbl_row">
                    <div className="tbl_col lft">
                      {item.title && item.title != "" ? (
                        <div className="heading">
                          <span className="text_ellipsis">{item.title}</span>
                        </div>
                      ) : null}
                      {item.offerText && item.offerText != "" ? (
                        <span className="main-price text_ellipsis">{item.offerText}</span>
                      ) : null}
                    </div>
                    <div className="tbl_col rgt">
                      <img src="https://navbharattimes.indiatimes.com/photo/58606011.cms" width="55" />
                      <span className="btn-buy">{item.callAction} </span>
                    </div>
                  </div>
                </div>
              </a>
            </li>
            // : null
            /*: null*/
           /*: null*/);
        })}
      </ul>
    </div>
  ) : null;
};

const getURL = url => {
  if (url.indexOf("https://www.amazon.in") > -1) {
    return url.replace("https://www.amazon.in", "");
  } else {
    return url;
  }
};

const getTagName = (pagename, subsec) => {
  let pagetype = "";

  if (subsec == "2354729") {
    pagetype = "nbt_wap_relationship_articleshow_sponsoredwidget-21";
    return pagetype;
  }
  if (pagename) {
    switch (pagename) {
      case "home":
        pagetype = "nbt_wap_mainhome_sponsoredwidget-21";
        break;
      case "photoshow":
        pagetype = "nbt_wap_photoshow_sponsoredwidget-21";
        break;
      case "compare":
        pagetype = "nbt_wap_articlelist_sponsoredwidget-21";
        break;
    }
    return pagetype;
  }
};

const removespace = title => {
  if (typeof title != "undefined" && typeof title != "object") {
    title = title
      .trim()
      .replace(/\s+/g, "-")
      .toLowerCase();
    return title;
  } else return;
};

function mapStateToProps(state) {
  return {
    ...state.amazonwidget,
  };
}

export default connect(mapStateToProps)(AmazonWidget);
