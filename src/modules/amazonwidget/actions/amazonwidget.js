import fetch from "utils/fetch/fetch";
import { _apiBasepointUpdate, _getfeedCategory } from "../../../utils/util";
export const FETCH_AMAZON_REQUEST = "FETCH_AMAZON_REQUEST";
export const FETCH_AMAZON_SUCCESS = "FETCH_AMAZON_SUCCESS";
export const FETCH_AMAZON_NEXT_SUCCESS = "FETCH_AMAZON_NEXT_SUCCESS";
export const FETCH_AMAZON_FAILURE = "FETCH_AMAZON_FAILURE";

function fetchAmazonSuccess(data) {
  return {
    type: FETCH_AMAZON_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}
function fetchAmazonNextSuccess(data, params) {
  return {
    type: FETCH_AMAZON_NEXT_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data,
    params: params
  };
}
function fetchAmazonFailure(error) {
  return {
    type: FETCH_AMAZON_FAILURE,
    payload: error.message
  };
}

// For Related Products API Change
function fetchAmazonRelatedData(state, subsec) {
  // API for Amazon Widgets
  let apiUrl = "https://shop.gadgetsnow.com/api/nbt/AmazonSeeAlso.php?keyword=";
  return dispatch => {
    dispatch({ type: FETCH_AMAZON_REQUEST });

    return fetch(apiUrl)
      .then(
        data => dispatch(fetchAmazonSuccess(data)),
        error => dispatch(fetchAmazonFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchAmazonFailure(error));
      });
  };
}

function fetchAmazonData(state, params, router, type) {
  if (
    params &&
    params.pagename &&
    params.pagename == "articleshow" &&
    params.amazonkey &&
    router &&
    router.location.pathname.indexOf("/tech") > -1
  ) {
    // API for Amazon show page Widgets
    let apiUrl =
      _apiBasepointUpdate(router, params.splat) +
      "/amazonshowwidget.cms?feedtype=sjson&productid=" +
      params.amazonkey;
    return dispatch => {
      dispatch({
        type: FETCH_AMAZON_REQUEST
      });
      return (
        fetch(apiUrl).then(data => {
          if (data.product && data.product.length > 0) {
            return dispatch(fetchAmazonSuccess(data, params));
          } else {
            return fetchAmazonInnerData(state, params, router, type); //if data not exist calling best seller widget
          }
        }),
        error =>
          dispatch(fetchAmazonFailure(error)).catch(function(error) {
            dispatch(fetchAmazonFailure(error));
          })
      );
    };
  } else {
    return fetchAmazonInnerData(state, params, router, type); //if data not exist calling best seller widget
  }
}

function fetchAmazonInnerData(state, params, router, type) {
  let apiUrl, isLifestyle, pathname;
  //get catgeory from route else mobile (for tech only)
  let category =
    params && params.category ? _getfeedCategory(params.category) : "mobile";

  if (
    state &&
    state.routing &&
    state.routing.locationBeforeTransitions &&
    state.routing.locationBeforeTransitions.pathname
  ) {
    pathname = state.routing.locationBeforeTransitions.pathname;
    isLifestyle = pathname.indexOf("/lifestyle/relationship/") > -1;
  }

  // if (router) {
  //     isLifestyle = (router.location && router.location.pathname && (router.location.pathname.indexOf('/lifestyle/relationship/') > -1));
  // }

  // API for Amazon Widgets
  // let Amazon_sponsoredApi = 'https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json';
  // let apiUrl = 'https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json';
  // if (subsec == "2354729") {
  // if (isLifestyle) {
  //     apiUrl = 'https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=wellness&format=json&country=in&product_per_page=20';
  // } else {
  //     apiUrl = 'https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=mobile&format=json';
  // }

  switch (type) {
    case "amazondeal":
      apiUrl =
        "https://shop.gadgetsnow.com/api/json/gadgets-mrec-banner.php?siteid=GN&channel=mobile&format=json";
      break;
    case "amazonMobile":
      apiUrl =
        "https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=mobile&format=json";
      break;
    default:
      apiUrl =
        "https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=" +
        category +
        "&format=json";
      break;
  }

  return dispatch => {
    dispatch({ type: FETCH_AMAZON_REQUEST });

    return fetch(apiUrl)
      .then(
        data => dispatch(fetchAmazonSuccess(data)),
        error => dispatch(fetchAmazonFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchAmazonFailure(error));
      });
  };
}

export function fetchAmazonNextData(dispatch, params, router, type) {
  if (
    params &&
    params.pagename &&
    params.pagename == "articleshow" &&
    params.amazonkey &&
    router &&
    router.location.pathname.indexOf("/tech") > -1
  ) {
    // API for Amazon show page Widgets
    let apiUrl =
      _apiBasepointUpdate(router, params.splat) +
      "/amazonshowwidget.cms?feedtype=sjson&productid=" +
      params.amazonkey;

    dispatch({
      type: FETCH_AMAZON_REQUEST
    });
    return (
      fetch(apiUrl).then(data => {
        if (data.product && data.product.length > 0) {
          return dispatch(fetchAmazonNextSuccess(data, params));
        } else {
          return fetchAmazonNextInnerData(dispatch, params, router, type); //if data not exist calling best seller widget
        }
      }),
      error =>
        dispatch(fetchAmazonFailure(error)).catch(function(error) {
          dispatch(fetchAmazonFailure(error));
        })
    );
  } else {
    return fetchAmazonNextInnerData(dispatch, params, router, type); //if data not exist calling best seller widget
  }
}

function fetchAmazonNextInnerData(dispatch, params, router, type) {
  let apiUrl, isLifestyle, pathname;
  //get catgeory from route else mobile (for tech only)
  let category =
    params && params.category ? _getfeedCategory(params.category) : "mobile";

  // if (state && state.routing && state.routing.locationBeforeTransitions && state.routing.locationBeforeTransitions.pathname) {
  //     pathname = state.routing.locationBeforeTransitions.pathname;
  //     isLifestyle = (pathname.indexOf('/lifestyle/relationship/') > -1);
  // }

  // if (router) {
  //     isLifestyle = (router.location && router.location.pathname && (router.location.pathname.indexOf('/lifestyle/relationship/') > -1));
  // }

  // API for Amazon Widgets
  // let Amazon_sponsoredApi = 'https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json';
  // let apiUrl = 'https://shop.gadgetsnow.com/api/nbt/paytm-sponsored.php?format=json';
  // if (subsec == "2354729") {
  // if (isLifestyle) {
  //     apiUrl = 'https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=wellness&format=json&country=in&product_per_page=20';
  // } else {
  //     apiUrl = 'https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=mobile&format=json';
  // }

  switch (type) {
    case "amazondeal":
      apiUrl =
        "https://shop.gadgetsnow.com/api/json/gadgets-mrec-banner.php?siteid=GN&channel=mobile&format=json";
      break;
    case "amazonMobile":
      apiUrl =
        "https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=mobile&format=json";
      break;
    default:
      apiUrl =
        "https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=" +
        category +
        "&format=json";
      break;
  }

  dispatch({ type: FETCH_AMAZON_REQUEST });

  return fetch(apiUrl)
    .then(
      data => dispatch(fetchAmazonNextSuccess(data, params)),
      error => dispatch(fetchAmazonFailure(error))
    )
    .catch(function(error) {
      dispatch(fetchAmazonFailure(error));
    });
}

function shouldFetchAmazonData(state) {
  if (state.amazonwidget) {
    return true;
  } else {
    return false;
  }
}

export function fetchAmazonDataIfNeeded(dispatch, params, query, router, type) {
  return (dispatch, getState) => {
    if (shouldFetchAmazonData(getState())) {
      return dispatch(fetchAmazonData(getState(), params, router, type));
    } else {
      return Promise.resolve([]);
    }
  };
}

export function fetchAmazonNextDataIfNeeded(
  dispatch,
  params,
  query,
  router,
  type
) {
  return (dispatch, getState) => {
    if (shouldFetchAmazonData(getState())) {
      return dispatch(fetchAmazonNextData(getState(), params, router, type));
    } else {
      return Promise.resolve([]);
    }
  };
}
