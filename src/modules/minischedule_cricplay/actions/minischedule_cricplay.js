import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_MINISCHEDULE_CRICPLAY_REQUEST =
  "FETCH_MINISCHEDULE_CRICPLAY_REQUEST";
export const FETCH_MINISCHEDULE_CRICPLAY_SUCCESS =
  "FETCH_MINISCHEDULE_CRICPLAY_SUCCESS";
export const FETCH_MINISCHEDULE_CRICPLAY_FAILURE =
  "FETCH_MINISCHEDULE_CRICPLAY_FAILURE";

function fetchMiniScheduleFailure(error) {
  return {
    type: FETCH_MINISCHEDULE_CRICPLAY_FAILURE,
    payload: error.message
  };
}

function fetchMiniScheduleSuccess(data, params) {
  return {
    type: FETCH_MINISCHEDULE_CRICPLAY_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchMiniScheduleData(state, params, query) {
  //let apiUrl = siteConfig.cricket_SIAPI.minischedule;
  let apiUrl = "https://image.cricplay.com/banner/schedule.json";
  //let apiUrl = "https://navbharattimes.indiatimes.com/pwafeeds/cricplay_testfeed.cms";
  //let apiUrl = "https://toicri.timesofindia.indiatimes.com/jsons/calendar_new_liupre.json";
  //let apiUrl = process.env.API_ENDPOINT + "/api_miniscorecard";

  return dispatch => {
    dispatch({
      type: FETCH_MINISCHEDULE_CRICPLAY_REQUEST
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchMiniScheduleSuccess(data, params)),
        error => dispatch(fetchMiniScheduleFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchMiniScheduleFailure(error));
      });
  };
}

function shouldFetchMiniScheduleData(state, params, query) {
  return true;
  // if(state.app && (!state.app.minitv || !state.app.minitv.hl)){
  //   return true;
  // } else {
  //   return false;
  // }
}

export function fetchMiniScheduleDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchMiniScheduleData(getState(), params, query)) {
      return dispatch(fetchMiniScheduleData(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}
