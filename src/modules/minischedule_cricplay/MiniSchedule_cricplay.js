import React, { PureComponent } from "react";
import { connect } from "react-redux";
//import { Link } from 'react-router'
// import PropTypes from "prop-types";
import { fetchMiniScheduleDataIfNeeded } from "./actions/minischedule_cricplay";
import "./../../components/common/css/CricPlay.scss";
import AnchorLink from "../../components/common/AnchorLink";
import { _isCSR, _getStaticConfig } from "../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../utils/cricket_util";
import Timer from "../../components/common/Timer";
//import { _getStaticConfig } from 'utils/util';
const siteConfig = _getStaticConfig();
const cricplayconfig = siteConfig.cricplay_widget;

class MiniSchedule_cricplay extends PureComponent {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    let _this = this;
    const { dispatch } = _this.props;
    let params = {
      seriesid: this.props.seriesid,
    };
    if (_isCSR()) {
      dispatch(fetchMiniScheduleDataIfNeeded(params)); //Load Schedule Widget
    }
  }

  scheduleWidget(item, index) {
    let _this = this;
    let timestampDiff = new Date(item.cutoffTime.replace(/-/g, "/")).getTime() - new Date().getTime();
    if (timestampDiff <= 0) return null;

    return (
      <div className="wdt_cricplay" key={index}>
        <AnchorLink href={cricplayconfig.schedulelink}>
          <div className="table top_bar">
            <div className="table_col">
              <span className="powered_by">Powered by</span>
              <span className="cricplay_icon" />
            </div>
            <div className="table_col">
              <span className="counter" id="counter">
                <Timer time={timestampDiff} />
              </span>
              <span className="match_date_venue">{item.tournament}</span>
              {/* <span className="match_date_venue">{item.tournament + _getShortMonthName(date.getMonth()) + " " + date.getDate()}</span> */}
            </div>
            <div className="table_col">
              <span className="game_price">&#x20b9; {item.prize}</span>
              {/* <span className="people_playing">45K Playing</span> */}
            </div>
          </div>
          <div className="table">
            <div className="teama table_col">
              <b className="country">{item.teamA.alias}</b>
              <span className="teaminfo">
                <img src={item.teamA.image} />
              </span>
            </div>
            <div className="versus table_col">
              <span>VS</span>
            </div>
            <div className="teamb table_col">
              <span className="teaminfo">
                <img src={item.teamB.image} />
              </span>
              <b className="country">{item.teamB.alias}</b>
            </div>
          </div>
          <div className="create_team_status">
            {cricplayconfig.wincashtxt1}{" "}
            <b className="win_cash">
              {cricplayconfig.wincashtxt2}
              <i />
            </b>
            {/* { item.matchresult && item.matchresult!='' ?
                            item.matchresult
                            :
                            <span className="match_venue">{item.venue}</span>
                        }
                        { item.live=='1' &&  item.live=='2' && item.live=='3' && item.live=='4' ? <span className="status_live">Live</span> : null} */}
          </div>
        </AnchorLink>
        <div className="btn_block">
          <a href={cricplayconfig.deeplink} className="btn_app_download">
            {cricplayconfig.appdwntxt}
          </a>
        </div>
      </div>
    );
  }
  render() {
    let _this = this;
    let schedule = this.props.data && this.props.data.length > 0 ? this.props.data : null;
    return schedule && typeof schedule != "null"
      ? schedule.map((item, index) => {
          return _this.scheduleWidget(item, index);
        })
      : null;
  }
}
function mapStateToProps(state) {
  return {
    ...state.minischedule_cricplay,
  };
}
export default connect(mapStateToProps)(MiniSchedule_cricplay);
