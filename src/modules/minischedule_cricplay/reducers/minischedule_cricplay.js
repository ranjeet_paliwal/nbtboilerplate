import {
  FETCH_MINISCHEDULE_CRICPLAY_REQUEST,
  FETCH_MINISCHEDULE_CRICPLAY_SUCCESS,
  FETCH_MINISCHEDULE_CRICPLAY_FAILURE
} from "./../actions/minischedule_cricplay";

function custom_sort(a, b) {
  return (
    new Date(a.matchdate_ist).getTime() - new Date(b.matchdate_ist).getTime()
  );
}
function minischedule(
  state = {
    isFetching: false,
    error: false,
    msid: "" //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action
) {
  switch (action.type) {
    case FETCH_MINISCHEDULE_CRICPLAY_REQUEST:
      state.minischedule =
        state.minischedule && state.minischedule.schedule
          ? { ...state.minischedule }
          : {};
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_MINISCHEDULE_CRICPLAY_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_MINISCHEDULE_CRICPLAY_SUCCESS:
      let matches =
        action.payload && action.payload.length > 0 ? action.payload : null;

      state.data = matches;

      return {
        ...state,
        isFetching: false,
        error: false
      };

    default:
      return state;
  }
}

export default minischedule;
