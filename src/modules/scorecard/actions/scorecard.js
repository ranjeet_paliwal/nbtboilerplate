import fetch from "utils/fetch/fetch";
export const FETCH_SCORECARD_REQUEST = "FETCH_SCORECARD_REQUEST";
export const FETCH_SCORECARD_SUCCESS = "FETCH_SCORECARD_SUCCESS";
export const FETCH_SCORECARD_FAILURE = "FETCH_SCORECARD_FAILURE";

function fetchMiniScorecardFailure(error) {
  return {
    type: FETCH_SCORECARD_FAILURE,
    payload: error.message,
  };
}

function fetchMiniScorecardSuccess(data) {
  return {
    type: FETCH_SCORECARD_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchMiniScorecardData(state, params, query) {
  let apiUrl = "https://nbtstorage.indiatimes.com/nbtappcricwidget.htm?feedtype=sjson";
  // "https://nbtstorage.indiatimes.com/regionalscorecard/scorecard/scorecardnw_widget.json";
  //let apiUrl = process.env.API_ENDPOINT + "/api_miniscorecard";
  //let apiUrl= "https://langdev8002.indiatimes.com/feeds/appcricwidget.cms?feedtype=sjson&source=web&upcache=2";

  return dispatch => {
    dispatch({
      type: FETCH_SCORECARD_REQUEST,
    });
    return fetch(apiUrl)
      .then(
        data => {
          data.matchid = params.matchid ? params.matchid : "";
          return dispatch(fetchMiniScorecardSuccess(data));
        },
        error => dispatch(fetchMiniScorecardFailure(error)),
      )
      .catch(function(error) {
        dispatch(fetchMiniScorecardFailure(error));
      });
  };
}

function shouldFetchMiniScorecardData(state, params, query) {
  return true;
  // if(state.app && (!state.app.minitv || !state.app.minitv.hl)){
  //   return true;
  // } else {
  //   return false;
  // }
}

export function fetchMiniScorecardDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchMiniScorecardData(getState(), params, query)) {
      return dispatch(fetchMiniScorecardData(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}
