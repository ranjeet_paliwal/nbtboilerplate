import { FETCH_SCORECARD_REQUEST, FETCH_SCORECARD_SUCCESS, FETCH_SCORECARD_FAILURE } from "./../actions/scorecard";

function scorecard(
  state = {
    isFetching: false,
    error: false,
  },
  action,
) {
  switch (action.type) {
    case FETCH_SCORECARD_REQUEST:
      state.mini_scorecard = state.mini_scorecard && state.mini_scorecard.Calendar ? { ...state.mini_scorecard } : {};
      return {
        ...state,
        isFetching: true,
        error: false,
      };
    case FETCH_SCORECARD_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCH_SCORECARD_SUCCESS:
      let scorecard_data = action.payload;
      let matchid = action.payload.matchid;
      if (
        scorecard_data &&
        scorecard_data.Calendar &&
        scorecard_data.Calendar.length &&
        scorecard_data.Calendar.length > 0
      ) {
        if (matchid != "") {
          scorecard_data.Calendar = scorecard_data.Calendar.filter(match => match.matchid == matchid);
        } else {
          scorecard_data.Calendar = scorecard_data.Calendar.filter(
            match =>
              //match.live == 0 ||
              (match.live == 1 || match.live == 2 || match.live == 3 || match.live == 4) &&
              (match.teama_short == "IND" ||
                match.teamb_short == "IND" ||
                match.seriesname == "ICC Champions Trophy" ||
                match.seriesname == "Indian Premier League" ||
                match.seriesname == "ICC Cricket World Cup"),
          );
        }

        if (scorecard_data.Calendar.length > 0) {
          return {
            ...state,
            mini_scorecard: scorecard_data,
            isFetching: false,
            error: false,
          };
          // newState = { ...state, mini_scorecard: scorecard_data };
        }
      }
      return {
        ...state,
        isFetching: false,
        error: false,
      };
    default:
      return state;
  }
}

export default scorecard;
