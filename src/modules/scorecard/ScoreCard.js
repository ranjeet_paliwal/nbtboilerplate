import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMiniScorecardDataIfNeeded } from "./actions/scorecard";
import { _isCSR, _getStaticConfig } from "../../utils/util";
import AnchorLink from "../../components/common/AnchorLink";
import styles from "../../components/common/css/MiniScorecard.scss";
import ErrorBoundary from "../../components/lib/errorboundery/ErrorBoundary";
const siteConfig = _getStaticConfig();

export class ScoreCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      miniScorecardInterval: 30000,
    };
    this.config = {
      miniScorecardInterval: "",
    };
  }

  componentDidMount() {
    let { dispatch, query, params, matchid } = this.props;
    params = params || {};
    params.matchid = matchid != undefined ? matchid : "";

    dispatch(fetchMiniScorecardDataIfNeeded(params)).then(() => {
      // this.setState({ miniScorecardInterval: 60000 });
    });

    this.config.miniScorecardInterval = setInterval(() => {
      dispatch(fetchMiniScorecardDataIfNeeded(params));
    }, this.state.miniScorecardInterval);
  }

  componentWillUnmount() {
    // const { dispatch } = this.props;
    const _this = this;
    if (_isCSR()) {
      try {
        clearInterval(_this.config.miniScorecardInterval);
        setSectionDetail();
      } catch (ex) {}
    }
  }

  render() {
    const { scorecard, pageName } = this.props;
    const matches =
      scorecard &&
      scorecard.mini_scorecard &&
      scorecard.mini_scorecard.Calendar &&
      scorecard.mini_scorecard.Calendar.length > 0
        ? scorecard.mini_scorecard.Calendar
        : null;
    const TagName = pageName == "home" ? "li" : React.Fragment;
    return matches ? (
      <ErrorBoundary>
        <TagName className={`scorecard-listview ${matches.length > 1 ? "scroll_content" : ""}`}>
          {matches.map((match, index) => (
            <div className="miniscorecard" key={match.matchid}>
              <AnchorLink
                data-type="scorecard"
                target="_blank"
                style={{ textDecoration: "none" }}
                hrefData={{
                  override: `${siteConfig.mweburl}/sports/cricket/live-score/${match.seoteamname}/${match.matchdate_localnew}/scoreboard/matchid-${match.matchid}.cms`,
                }}
              >
                <div className="liveScore">
                  <strong style={{ float: "right" }}>
                    {match.matchnumber}, {match.seriesname}
                  </strong>
                  <span className="live">
                    <small />
                    LIVE
                  </span>
                  <div className="clear" />
                  <div className="midbox">
                    <span className="dates">
                      {match.matchdd}
                      <small>{match.matchmm}</small>
                    </span>
                    <div className="teama">
                      <span className="teaminfo fl">
                        <img height="20" width="20" src={match.teama_flag_square} />
                        {match.teama_short}
                      </span>
                      <span className="teamscore fl">
                        <span className="teamaScore">{match.teama_score}</span>
                        <span className="teamaOvers">{match.teama_overs != "" ? match.teama_overs : ""}</span>
                      </span>
                    </div>
                    <div className="teamb">
                      <span className="teaminfo fr">
                        <img height="20" width="20" src={match.teamb_flag_square} />
                        {match.teamb_short}
                      </span>
                      <span className="teamscore fr">
                        <span className="teambScore">{match.teamb_score}</span>
                        <span className="teambOvers">{match.teamb_overs != "" ? match.teamb_overs : ""}</span>
                      </span>
                    </div>
                  </div>
                  <div className="mSummery">{match.matchstatus}</div>
                </div>
              </AnchorLink>
            </div>
          ))}
        </TagName>
      </ErrorBoundary>
    ) : null;
  }
}

function mapStateToProps(state) {
  return {
    scorecard: state.scorecard,
  };
}

export default connect(mapStateToProps)(ScoreCard);
