/* eslint-disable no-param-reassign */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable dot-notation */
/* eslint-disable no-continue */
/* eslint-disable block-scoped-var */
/* eslint-disable no-undef */
/* eslint-disable object-shorthand */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-mutable-exports */
/* eslint-disable no-plusplus */
/* eslint-disable prefer-const */
/* eslint-disable one-var */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable radix */
/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable no-inner-declarations */
/* eslint-disable vars-on-top */
/* eslint-disable no-redeclare */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
/* eslint-disable no-var */
// import { relative } from "path";
import { isMobilePlatform } from "../../utils/util";

var svgNS = "https://www.w3.org/2000/svg";

// var myVinyls = {
//   "Classical music": 5,
//   "Alternative rock": 12,
//   Pop: 2,
//   Jazz: 12,
// };

// var drawPieSlice = function(ctx, centerX, centerY, radius, startAngle, endAngle, color) {
//   ctx.fillStyle = color;
//   ctx.beginPath();
//   ctx.moveTo(centerX, centerY);
//   ctx.arc(centerX, centerY, radius, startAngle, endAngle);
//   ctx.closePath();
//   ctx.fill();
// };

// var drawTextTicker = function(path, x, y, radius, startAngle, endAngle, val) {
//   var svgPoint = path.getPointAtLength(path.getTotalLength() / 2);

//   var svgPoint2 = path.getPointAtLength(path.getTotalLength() / 2);
//   var svgText = document.createElementNS(svgNS, "text");

//   var siblingG = path.parentNode.previousSibling;
//   var path2 = document.createElementNS(svgNS, "path");
//   path2.setAttributeNS(null, "stroke", "#000");
//   path2.setAttributeNS(null, "d", describeArc(x, y, 1.35 * radius, startAngle, endAngle));
//   siblingG.append(path2);

//   var svgPoint2 = path2.getPointAtLength(path2.getTotalLength() / 2);

//   svgText.setAttributeNS(null, "fill", "#000");
//   svgText.setAttributeNS(null, "x", svgPoint2.x);
//   svgText.setAttributeNS(null, "y", svgPoint2.y);
//   svgText.setAttributeNS(null, "text-anchor", "middle");
//   svgText.innerHTML = val;
//   path2.after(svgText);

//   // var siblingNext = siblingG.parentNode.nextElementSibling;
//   var line = document.createElementNS(svgNS, "line");
//   line.setAttributeNS(null, "stroke", "#000");
//   line.setAttributeNS(null, "stroke-width", "1");
//   line.setAttributeNS(null, "stroke-dasharray", "25px");
//   line.setAttributeNS(null, "x1", svgPoint.x);
//   line.setAttributeNS(null, "y1", svgPoint.y);
//   line.setAttributeNS(null, "x2", svgPoint2.x);
//   line.setAttributeNS(null, "y2", svgPoint2.y);
//   path2.after(line);
// };

var polarToCartesian = function(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = ((angleInDegrees + 135) * Math.PI) / 180;

  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
};

var describeArc = function(x, y, radius, startAngle, endAngle) {
  var start = polarToCartesian(x, y, radius, endAngle);
  var end = polarToCartesian(x, y, radius, startAngle);

  var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

  var d = ["M", start.x, start.y, "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y].join(" ");

  return d;
};

var animateSector = function(path, x, y, radius, startAngle, endAngle, animationDuration, val, options) {
  if (typeof performance != "undefined" && typeof requestAnimationFrame != "undefined") {
    var startTime = performance.now();

    function doAnimationStep() {
      // Get progress of animation (0 -> 1)
      var progress = Math.min((performance.now() - startTime) / animationDuration, 1.0);
      // Calculate the end angle for this point in the animation
      var angle = startAngle + progress * (endAngle - startAngle);
      // Calculate the sector shape
      var arc = describeArc(x, y, radius, startAngle, angle);
      // Update the path
      path.setAttributeNS(null, "d", arc);
      // If animation is not finished, then ask browser for another animation frame.
      if (progress < 1.0) {
        requestAnimationFrame(doAnimationStep);
      } else if (val > 0) {
        // if(options.showTicker == 'true')drawTextTicker(path, x, y, radius, startAngle, endAngle, val);
      }
    }

    requestAnimationFrame(doAnimationStep);
  } else {
    // Update the path
    path.setAttributeNS(null, "d", describeArc(x, y, radius, startAngle, endAngle));
  }
};

// var Piechart = function(options) {
//   this.options = options;
//   this.canvas = options.canvas;
//   this.svgContainer = options.canvas;
//   // this.ctx = this.canvas.getContext("2d");
//   this.colors = options.colors;
//   this.radius = options.radius || Math.min(this.canvas.width / 2, this.canvas.height / 2);
//   this.totalValue = options.totalValue;

//   this.drawCanvas = function(canvasElem) {
//     /* if(canvasElem) this.canvas = canvasElem;
//         this.canvas.width = this.canvas.height = 400;

//         var total_value = 0;
//         var color_index = 0;
//         for (var categ in this.options.data){
//             var val = this.options.data[categ];
//             total_value += val;
//         }

//         var start_angle = 3/4*Math.PI;
//         for (categ in this.options.data){
//             val = this.options.data[categ];
//             var slice_angle = 3 * Math.PI/2 * val / total_value;

//             drawPieSlice(
//                 this.ctx,
//                 this.canvas.width/2,
//                 this.canvas.height/2,
//                 this.radius,
//                 start_angle,
//                 start_angle+slice_angle,
//                 this.colors[color_index%this.colors.length]
//             );

//             start_angle += slice_angle;
//             color_index++;
//         }

//         //drawing a white circle over the chart
//         //to create the doughnut chart
//         if (this.options.doughnutHoleSize){
//             drawPieSlice(
//                 this.ctx,
//                 this.canvas.width/2,
//                 this.canvas.height/2,
//                 this.options.doughnutHoleSize * this.radius,
//                 3/4*Math.PI - .02,
//                 start_angle + .02,
//                 "#ffffff"
//             );
//         }

//         return false */
//   };

//   this.drawSVG = function(svgElem) {
//     if (svgElem) this.svgContainer = svgElem;
//     var total_value = 0;
//     var color_index = 0;
//     var start_angle = this.options.start_angle || 0;
//     var val = 0;
//     // format data
//     start_angle = parseInt(start_angle);

//     for (var categ in this.options.data) {
//       val = this.options.data[categ];
//       total_value += val;
//     }
//     this.totalValue = this.totalValue ? parseInt(this.totalValue) : total_value;

//     if (this.totalValue > total_value) {
//       this.options.data["_NA"] = parseInt(this.totalValue) - parseInt(total_value);
//       this.colors.push("rgba(128, 128, 128, 0.1)");
//     }

//     var svgElem = document.createElementNS(svgNS, "svg");
//     svgElem.setAttributeNS(null, "viewBox", `0 0 ${this.radius * 3} ${this.radius * 3}`);

//     var svgG2 = document.createElementNS(svgNS, "g");
//     svgG2.setAttributeNS(null, "fill", "none");
//     svgG2.setAttributeNS(null, "stroke-width", "0");
//     svgElem.appendChild(svgG2);

//     var svgG = document.createElementNS(svgNS, "g");
//     svgG.setAttributeNS(null, "fill", "none");
//     svgG.setAttributeNS(null, "stroke-width", this.options.strokeWidth);
//     svgElem.appendChild(svgG);

//     for (categ in this.options.data) {
//       if (this.options.data[categ] < 1) {
//         color_index++;
//         continue;
//       }
//       val = this.options.data[categ];
//       var slice_angle = (this.options.circumference * val) / this.totalValue;
//       if (categ == "_NA") val = 0;

//       var svgPath = document.createElementNS(svgNS, "path");
//       svgPath.setAttributeNS(null, "stroke", this.colors[color_index] || "#f5f5f5");

//       svgG.appendChild(svgPath);
//       animateSector(svgPath, 150, 150, this.radius, start_angle, start_angle + slice_angle - 0.5, 500, val, options);

//       start_angle += slice_angle;
//       color_index++;
//     }

//     this.svgContainer.appendChild(svgElem);
//     return false;
//   };
// };

var getArrData = function(options) {
  let dataArr = [],
    tempArr = [],
    zindex = 150,
    sum = 0;
  let colors = [],
    i = 0;
  for (var key in options.data) {
    if (parseInt(options.data[key]) > -1) {
      tempArr = [];
      tempArr.push(key);
      tempArr.push(parseInt(options.data[key]));
      dataArr.push(tempArr);
      sum += parseInt(options.data[key]);
      colors.push(options.colors[i]);
    }
    i++;
  }
  // tempArr = [];
  // tempArr.push('');
  // tempArr.push(parseInt(options.totalValue) - sum);
  // dataArr.push(tempArr);
  colors.push("#cccccc");
  if (options.totalValue != sum) {
    dataArr.push({
      name: "Other",
      y: parseInt(options.totalValue) - sum,
      dataLabels: {
        enabled: false,
      },
    });
  }
  return {
    dataArr: dataArr,
    colors: colors,
  };
};

var drawPieChart = function(options) {
  // console.log("options-----------", options);

  let dataArr, arrColors;

  let arrObj = getArrData(options);
  dataArr = arrObj.dataArr;
  arrColors = arrObj.colors;

  const configObject = isMobilePlatform()
    ? { ...commonOptions, ...mobileSpecificOptions }
    : { ...commonOptions, ...desktopSpecificOptions };

  configObject.series[0].data = dataArr;
  configObject.colors = arrColors;

  // const configObject = newDesktopOptions;

  // configObject.series[0].data = dataArr;
  // configObject.series[0].colors = arrColors;
  // configObject.series[0].visible = false;

  // configObject.series[1].data = dataArr;
  // configObject.series[1].colors = arrColors;
  // configObject.series[1].point = dataArr;

  try {
    Highcharts.chart(options.canvasid, configObject);
  } catch (err) {
    console.log("err---", err);
  }
};

// var drawPieChart = function (options) {
//     var myDougnutChart = new Piechart(
//         {
//             canvas: document.getElementById(options.canvasid) || document.getElementById("myCanvas"),
//             data: options.data || myVinyls,
//             colors: options.colors || ["#fde23e", "#f16e23", "#57d9ff", "#937e88"],
//             radius: options.radius || 100,
//             doughnutHoleSize: options.doughnutHoleSize || 0.7,
//             totalValue: options.totalValue,
//             circumference: options.circumference || 270,
//             strokeWidth: options.strokeWidth || "35",
//             start_angle: options.start_angle,
//             showTicker: options.showTicker || 'true'
//         }
//     );

//     try {

//         if (myDougnutChart.canvas) {
//             myDougnutChart.canvas.innerHTML = ''
//             myDougnutChart.drawSVG(options.canvasElem);
//             // myDougnutChart.canvas.setAttribute('is-rendered',true);
//         }

//     } catch (err) {
//         throw new Error('Err while piechart rendering' + err.message)
//     }

//     return false
// }

const mobileSpecificOptions = {
  series: [
    {
      type: "pie",
      name: "",
      innerSize: "70%",
      states: {
        hover: {
          enabled: false,
        },
        inactive: {
          opacity: 1,
        },
      },
    },
  ],
};

const desktopSpecificOptions = {
  plotOptions: {
    pie: {
      showInLegend: function() {
        return point.name === "Awaited" ? false : true;
      },
      dataLabels: {
        enabled: true,
        distance: 5,
        format: '<span style="font-size: 1.1rem; font-weight: normal"> {point.y}</span>',
        style: {},
        endOnTick: false,
      },
      startAngle: -135,
      endAngle: 135,
      center: ["50%", "50%"],
      point: {
        events: {
          mouseOver: function() {
            this.graphic.attr({
              r: this.shapeArgs.r + 10,
            });
          },
          mouseOut: function() {
            this.graphic.attr({
              r: this.shapeArgs.r,
            });
          },
        },
      },
    },
    series: {
      events: {
        afterAnimate: function() {
          if (this.index === 0) {
            const series = this.chart.series[0];
            if (!series.data[0]) {
              return;
            }
            series.points[0].graphic.attr({
              r: series.points[0].shapeArgs.r + 10,
            });
          }
        },
      },
    },
  },
  series: [
    {
      type: "pie",
      name: "",
      size: "80%",
      innerSize: "72%",
      states: {
        hover: {
          enabled: true,
          halo: {
            size: 0,
            opacity: 0.5,
          },
          brightness: 0,
          lineWidth: 0,
        },
      },
    },
  ],
  legend: {
    symbolHeight: 1,
    symbolWidth: 1,
    symbolRadius: 0,
    useHTML: true,
    align: "right",
    verticalAlign: "top",
    itemWidth: 100,
    layout: "vertical",
    x: 0,
    y: 20,
    labelFormatter: function() {
      return this.name === "Awaited"
        ? ""
        : '<div data-lengend-index="' +
            this.index +
            '" class="legend" style="background-color:' +
            this.color +
            '"><span class="legend-label">' +
            this.name +
            '<span>:</span></span><span class="legend-count">' +
            this.y +
            "</span></div>";
    },
  },
};

const commonOptions = {
  credits: {
    enabled: false,
  },
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: 0,
    plotShadow: false,
    // height: 315,
    // width: 315,
    style: {
      margin: "auto",
    },
  },
  title: {
    text: "",
    align: "center",
    verticalAlign: "middle",
    y: 60,
  },
  tooltip: {
    enabled: false,
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: 5,
        format: '<span style="font-size: 1.1rem; font-weight: normal"> {point.y}</span>',
        style: {},
        endOnTick: false,
      },
      startAngle: -135,
      endAngle: 135,
      center: ["50%", "50%"],
      size: "70%",
    },
  },
  series: [
    {
      type: "pie",
      name: "",
      innerSize: "70%",
    },
  ],
};

export default drawPieChart;

const newDesktopOptions = {
  chart: {
    events: {
      load: function() {
        this.series.forEach(function(series) {
          series.points.forEach(function(point) {
            point.options.origColor = point.color;
          });
        });
      },
    },
    style: {
      width: "100%",
    },
    type: "pie",
  },
  title: {
    text: "",
  },
  yAxis: {
    title: {
      text: "Total percent market share",
    },
  },
  colors: ["#eee"],
  credits: {
    enabled: false,
  },
  exporting: {
    enabled: false,
  },
  legend: {
    symbolHeight: 1,
    symbolWidth: 1,
    symbolRadius: 0,
    useHTML: true,
    align: "right",
    verticalAlign: "top",
    itemWidth: 100,
    layout: "vertical",
    x: 100,
    y: 100,
    labelFormatter: function() {
      return this.name === "Awaited"
        ? ""
        : '<div data-lengend-index="' +
            this.index +
            '" class="legend" style="background-color:' +
            this.color +
            '"><span class="legend-label">' +
            this.name +
            '<span>:</span></span><span class="legend-count">' +
            this.y +
            "</span></div>";
    },
  },
  plotOptions: {
    series: {
      events: {
        afterAnimate: function() {
          if (this.index === 1) {
            var pointsCount = this.chart.series[1].points.length,
              series = this.chart.series[1];
            this.chart.initR = series.data[0].graphic.attr("r");
            if (!series.data[0]) {
              return;
            }
            series.points[0].update(
              {
                shapeArgsR: 1.1,
              },
              false,
              false,
            );
            // series.points[0].dataLabel.distance *= 1.1;
            this.chart.redraw();
            // series.points[pointsCount - 1].dataLabel && series.points[pointsCount - 1].dataLabel.hide();
            // series.points[pointsCount - 1].connector && series.points[pointsCount - 1].connector.hide();
          }
        },
      },
    },
    pie: {
      showInLegend: function() {
        return point.name === "Awaited" ? false : true;
      },
      dataLabels: {
        formatter: function() {
          if (this.point.name === "Awaited") {
            return "";
          } else {
            return "" + this.point.y;
          }
        },
        connectorColor: "#000",
        distance: 10,
        softConnector: false,
        padding: 0,
        style: {
          color: "#000",
          fontSize: "15px",
        },
      },
      point: {
        events: {
          legendItemClick: function() {
            return false;
          },
          mouseOver: function(e) {
            var that = this;
            if (this.series.chart.series[0].data.indexOf(this) != -1) {
              return;
            }
            if (that.name === "Awaited") {
              this.series.chart.series[1].points.forEach(function(point) {
                point.update(
                  {
                    color:
                      that !== point
                        ? Highcharts.Color(point.color)
                            .setOpacity(0.3)
                            .get()
                        : point.options.origColor,
                    shapeArgsR: 1,
                  },
                  false,
                  false,
                );
              });
              return;
            }

            this.series.chart.series[1].points.forEach(function(point) {
              point.update(
                {
                  color:
                    that !== point
                      ? Highcharts.Color(point.color)
                          .setOpacity(0.3)
                          .get()
                      : point.options.origColor,
                  shapeArgsR: that !== point ? 1 : 1.1,
                },
                false,
                false,
              );
            });
            this.series.chart.redraw();
            that.series.dataLabelsGroup.hide();
            that.connector.show();
            that.dataLabel.show();
          },
          mouseOut: function(e) {
            var pointsCount = this.series.chart.series[1].points.length,
              that = this;
            if (this.series.chart.series[0].data.indexOf(this) != -1) {
              return;
            }
            // if (this.name === 'Awaited') {
            //     return;
            // }
            this.series.chart.series[1].points.forEach(function(point, index) {
              point.update(
                {
                  color: point.options.origColor,
                  shapeArgsR: 1,
                },
                false,
                false,
              );
            });

            // rehighlight first data point
            setTimeout(function() {
              that.series.chart.series[1].points[0].update(
                {
                  shapeArgsR: 1.1,
                },
                false,
                false,
              );

              that.series.chart.redraw();

              // that.series.chart.series[1].points[pointsCount - 1].dataLabel && that.series.chart.series[1].points[pointsCount - 1].dataLabel.hide();
              that.series.chart.series[1].points[pointsCount - 1].connector &&
                that.series.chart.series[1].points[pointsCount - 1].connector.hide();
            });
          },
        },
      },
      startAngle: -135,
      endAngle: 135,
      center: ["50%", "50%"],
      size: "100%",
    },
  },
  tooltip: {
    enabled: false,
  },
  series: [
    {
      cursor: "default",
      states: {
        hover: {
          enabled: false,
        },
      },
      showInLegend: false,
      name: "Election Result",
      enabled: true,
      dataLabels: {
        enabled: false,
      },
      data: "",
      size: "35%",
      innerSize: "85%",
    },
    {
      cursor: "pointer",
      states: {
        hover: {
          enabled: false,
          // enabled: true,
          // halo: {
          //   size: 0,
          //   opacity: 0.5,
          // },
          // brightness: 0,
          // lineWidth: 0,
        },
      },
      name: "Versions",
      dataLabels: {
        enabled: true,
      },
      size: "90%",
      innerSize: "72%",
      zIndex: 2,
    },
  ],
};
// very important js code
// https://toidev.indiatimes.com/electionfeedjs_v1.cms?version=107&amp;minify=0&amp;v=1
// https://timesofindia.indiatimes.com/electionjs.cms?version=31&minify=0
