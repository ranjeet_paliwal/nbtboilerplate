/* eslint-disable no-undef */
/* eslint-disable func-names */
/* eslint-disable object-shorthand */
import { isMobilePlatform } from "../../utils/util";

const getArrData = options => {
  const dataArr = [];
  let tempArr = [];
  let sum = 0;
  const colors = [];

  let i = 0;
  const keyArr = Object.keys(options.data);
  keyArr.forEach(key => {
    if (parseInt(options.data[key], 10) > -1) {
      tempArr = [];
      tempArr.push(key);
      tempArr.push(parseInt(options.data[key], 10));
      dataArr.push(tempArr);
      sum += parseInt(options.data[key], 10);
      colors.push(options.colors[i]);
    }
    i += 1;
  });

  colors.push("#cccccc");
  if (parseInt(options.totalValue, 10) !== sum) {
    dataArr.push({
      name: "Awaited",
      y: parseInt(options.totalValue, 10) - sum,
    });
  }
  return {
    dataArr,
    colors,
  };
};

const drawPieChart = options => {
  const arrObj = getArrData(options);
  const { dataArr, colors } = arrObj;

  const configObject = isMobilePlatform()
    ? { ...commonOptions, ...mobileSpecificOptions }
    : { ...commonOptions, ...desktopSpecificOptions };

  configObject.series[0].data = dataArr;
  configObject.colors = colors;

  // Don't change this code without consulting Kiran
  if (!isMobilePlatform()) {
    if (options.hideLegend) {
      configObject.plotOptions.pie.showInLegend = false;
    } else {
      configObject.plotOptions.pie.showInLegend = showInlegendOption;
    }
  }

  try {
    Highcharts.chart(options.canvasid, configObject, function(chart) {});
  } catch (err) {
    const script = document.querySelector("#highchart-script");
    if (script) {
      script.addEventListener("load", () => {
        setTimeout(() => {
          Highcharts.chart(options.canvasid, configObject);
        }, 2000);
      });
    }
    // eslint-disable-next-line no-console
    console.log("err---", err);
  }
};

const mobileSpecificOptions = {
  series: [
    {
      type: "pie",
      name: "",
      size: "80%",
      innerSize: "70%",
      states: {
        hover: {
          enabled: false,
        },
        inactive: {
          opacity: 1,
        },
      },
    },
  ],
};

const showInlegendOption = {
  showInLegend: function() {
    return point.name !== "Awaited";
  },
};

const desktopSpecificOptions = {
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: 5,
        formatter: function() {
          return this.point.name === "Awaited"
            ? ""
            : `<span style="font-size: 1.1rem; font-weight: normal"> ${this.point.y}</span>`;
        },
        // format: '<span style="font-size: 1.1rem; font-weight: normal"> {point.y}</span>',
        style: {
          textOverflow: "none",
        },
        endOnTick: false,
      },
      startAngle: -135,
      endAngle: 135,
      center: ["50%", "50%"],
      point: {
        events: {
          legendItemClick: function() {
            return false;
          },
          mouseOver: function() {
            const { data } = this.series.chart.series[0];
            if (this !== data[0]) {
              this.graphic.attr({
                r: this.shapeArgs.r + 10,
              });
              data[0].graphic.attr({
                r: data[0].shapeArgs.r,
              });
            }
          },
          mouseOut: function() {
            const { data } = this.series.chart.series[0];
            this.graphic.attr({
              r: this.shapeArgs.r,
            });
            data[0].graphic.attr({
              r: data[0].shapeArgs.r + 10,
            });
          },
        },
      },
    },
    series: {
      events: {
        afterAnimate: function() {
          if (this.index === 0) {
            const series = this.chart.series[0];
            if (!series.data[0]) {
              return;
            }
            series.points[0].graphic.attr({
              r: series.points[0].shapeArgs.r + 10,
            });
            // console.log(this, "after animate");
          }
        },
      },
    },
  },
  series: [
    {
      type: "pie",
      name: "",
      size: "80%",
      innerSize: "72%",
      states: {
        hover: {
          enabled: true,
          halo: {
            size: 0,
            opacity: 0.5,
          },
          brightness: 0,
          lineWidth: 0,
        },
      },
    },
  ],
  legend: {
    symbolHeight: 1,
    symbolWidth: 1,
    symbolRadius: 0,
    useHTML: true,
    align: "right",
    verticalAlign: "top",
    itemWidth: 100,
    layout: "vertical",
    x: 0,
    y: 20,
    labelFormatter: function() {
      return this.name === "Awaited"
        ? ""
        : `<div data-lengend-index="${this.index}" class="legend" style="background-color:${this.color}"><span class="legend-label">${this.name}<span>:</span></span><span class="legend-count">${this.y}</span></div>`;
    },
  },
};

const commonOptions = {
  credits: {
    enabled: false,
  },
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: 0,
    plotShadow: false,
    style: {
      margin: "auto",
    },
  },
  title: {
    text: "",
    align: "center",
    verticalAlign: "middle",
    y: 60,
  },
  tooltip: {
    enabled: false,
  },
  plotOptions: {
    pie: {
      dataLabels: {
        enabled: true,
        distance: 5,
        formatter: function() {
          return this.point.name === "Awaited"
            ? ""
            : `<span style="font-size: 1.1rem; font-weight: normal"> ${this.point.y}</span>`;
        },
        // format: '<span style="font-size: 1.1rem; font-weight: normal"> {point.y}</span>',
        style: {
          textOverflow: "none",
        },
        endOnTick: false,
      },
      startAngle: -135,
      endAngle: 135,
      center: ["50%", "50%"],
      size: "70%",
    },
  },
  series: [
    {
      type: "pie",
      name: "",
      innerSize: "70%",
    },
  ],
  exporting: false,
};

export default drawPieChart;
