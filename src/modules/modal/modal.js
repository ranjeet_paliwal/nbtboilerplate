import React from "react";
import ReactDOM from "react-dom";
import { _isCSR, isMobilePlatform, enableBodyScroll, disableBodyScroll } from "../../utils/util";
import "../../components/common/css/desktop/Modal.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {
    if (prevProps.showModal && !this.props.showModal) {
      //  Remove the scroll disabling class once modal closes.
      enableBodyScroll();
    } else if (!prevProps.showModal && this.props.showModal) {
      //  Add the scroll disabling class once modal opens.
      disableBodyScroll();
    }
  }

  render() {
    return _isCSR() && this.props.showModal
      ? ReactDOM.createPortal(
          <div
            className={`modal-container ${this.props.wrapperClass || ""} ${isMobilePlatform() ? `mobile` : `desktop`} `}
          >
            <div className="overlay">
              <div className="content">
                <button onClick={() => this.props.closeModal()} className="close_icon" />
                {this.props.children}
              </div>
            </div>
          </div>,
          document.getElementById("modal"),
        )
      : null;
  }
}

export default Modal;
