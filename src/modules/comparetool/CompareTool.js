import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchDeviceListDataIfNeeded, fetchSuggestedListDataIfNeeded } from "./actions/comparetool";
//import { fetchCompareDataIfNeeded } from "./../../actions/gadgets/compare/compare";
import { AnalyticsGA } from "../../components/lib/analytics";
import { _getStaticConfig, _getCategory, _isCSR } from "../../utils/util";
import styles from "./../../components/common/css/GadgetNow.scss";
import AdCard from "./../../components/common/AdCard";
import GadgetRelated from "./../../components/common/GadgetRelated";

const siteConfig = _getStaticConfig();

class CompareTool extends Component {
  constructor(props) {
    super(props);
    this.state = { category: "" };
    this.selectDevice = this.selectDevice.bind(this);
    this.addtocompare = this.addtocompare.bind(this);
    this.deviceList = [];
  }
  componentWillMount() {
    let { _category } = this.props;
    let category = _category ? _category : _getCategory(this.props.router.location.pathname, this.props.params);
    if (category) this.setState({ category: category });
  }
  prefillDeviceList() {
    //reset select box is opened already
    // document.getElementById("deviceinput1") ? document.getElementById("deviceinput1").value="" : null;
    // document.getElementById("deviceinput1") ? document.getElementById("deviceinput1").value="" : null;
    // document.getElementById("deviceinput1") ? document.getElementById("deviceinput1").value="" : null;

    if (document.getElementById("deviceinput1") && window.compareDevice[1]) {
      this.deviceList[1] = window.compareDevice[1].keyword;
      document.getElementById("deviceinput1").value = window.compareDevice[1].keylabel;
    }
    if (document.getElementById("deviceinput2") && window.compareDevice[2]) {
      this.deviceList[2] = window.compareDevice[2].keyword;
      document.getElementById("deviceinput2").value = window.compareDevice[2].keylabel;
    }
    if (document.getElementById("deviceinput3") && window.compareDevice[3]) {
      this.deviceList[3] = window.compareDevice[3].keyword;
      document.getElementById("deviceinput3").value = window.compareDevice[3].keylabel;
    }
  }
  componentWillReceiveProps(newProps) {
    let { _category } = this.props;
    let category = _category ? _category : _getCategory(this.props.router.location.pathname, this.props.params);
    if (category) this.setState({ category: category });
  }
  chksuggestedlist() {
    //on page load highlight in suugested list available
    var el = document.querySelectorAll(".add-to-compare");
    for (let i = 0; i < el.length - 1; i++) {
      for (let j = 1; j < 4; j++) {
        if (
          window.compareDevice[j] &&
          window.compareDevice[j].keyword &&
          el[i] &&
          el[i].getAttribute("seo") &&
          el[i].getAttribute("seo") == window.compareDevice[j].keyword
        ) {
          el[i].classList.add("highlighted");
        }
      }
    }
  }
  componentDidMount() {
    if (_isCSR()) {
      !window.compareDevice ? (window.compareDevice = []) : null;
      //device list prefill from window object
      if (window.compareDevice && window.compareDevice.length > 0) {
        this.prefillDeviceList();
      }
    }
    document.body.addEventListener("click", this.bodyClick);
    const { dispatch, params, router } = this.props;
    const { query } = this.props.location;
    let category = this.state.category;
  }
  componentWillUnmount() {
    document.body.removeEventListener("click", this.bodyClick);
  }
  filterDevice(event) {
    const { dispatch, params, router, query } = this.props;
    let category = this.state.category;
    let keyword = event.target.value;
    let index = parseInt(event.target.name.substring(11, 12));
    if (event.target.value && event.target.value != "") {
      document.getElementById("selectArea" + index).style.display = "block";
      CompareTool.fetchDeviceListData({
        dispatch,
        query,
        params,
        router,
        category,
        keyword,
        index,
      }).then(data => { });
    } else {
      document.getElementById("selectArea" + index).style.display = "none";
      let deviceList = null;
      if (_isCSR()) window.compareDevice[index] = deviceList;
      this.deviceList[index] = "";
    }
  }

  bodyClick(e) {
    //alert(e.target);
    console.log(e.target);
    console.log(e.target.type);
    console.log(e.target.parentElement.id);
    if (e.target.parentElement.parentElement.id.indexOf("select") < 0) {
      document.getElementById("selectArea1").style.display = "none";
      document.getElementById("selectArea2").style.display = "none";
      document.getElementById("selectArea3").style.display = "none";
    }
  }
  // closeSelectBox(obj)
  // {
  //     document.getElementById("selectArea1").style.display='none';
  // }
  clearDevice(event) {
    if (_isCSR()) {
      if (event) {
        event.target.parentElement.firstElementChild.value = "";
        let devicenumber = event.target.parentElement.firstElementChild.getAttribute("name").substr(11, 1);
        //reset hightlishgeted buttton if device remove for compare box
        var el = document.querySelectorAll(".highlighted");
        for (i = 0; i < el.length; i++) {
          if (
            window.compareDevice[devicenumber] &&
            window.compareDevice[devicenumber].keyword &&
            el[i].getAttribute("seo") &&
            el[i].getAttribute("seo") == window.compareDevice[devicenumber].keyword
          ) {
            el[i].classList.toggle("highlighted");
          }
        }
        this.deviceList[devicenumber] = null;
        window.compareDevice[devicenumber] = null;
      } else {
        document.getElementById("deviceinput1") && window.compareDevice[1]
          ? (document.getElementById("deviceinput1").value = "")
          : null;
        document.getElementById("deviceinput2") && window.compareDevice[2]
          ? (document.getElementById("deviceinput2").value = "")
          : null;
        document.getElementById("deviceinput3") && window.compareDevice[3]
          ? (document.getElementById("deviceinput3").value = "")
          : null;
        window.compareDevice = [];
        window.compareDevice[0] = null;
        this.deviceList = [];
        var el = document.querySelectorAll(".highlighted");
        for (i = 0; i < el.length; i++) {
          el[i].classList.toggle("highlighted");
        }
      }
      //below code used to reset the suggested list.
      // const { dispatch, params, router, query } = this.props;
      // let category=this.state.category;
      // let keyword="";
      // CompareTool.fetchSuggestedListData({ dispatch, query, params, router,category, keyword }).then((data) => {

      // })
      this.showDeviceCount();
    }
  }
  selectDevice(event) {
    //if user directly land on compare show page, we store device list in validation array when he chnage the device.
    this.prefillDeviceList();
    const { dispatch, params, router, query } = this.props;
    let category = this.state.category;
    let deviceList = this.deviceList;
    let keyword = event.target.getAttribute("seo");
    let selDeviceRegional = event.target.innerText;
    if (this.deviceList.includes(keyword)) {
      alert(siteConfig.locale.tech.devicealreadyinlist);
      return false;
    }

    AnalyticsGA.event({
      category: "Wap_CS",
      action: "GadgetsSearch",
      label: keyword,
    });
    document.getElementById(event.target.parentElement.parentElement.id).style.display = "none";
    document.getElementById(
      event.target.parentElement.parentElement.previousElementSibling.id,
    ).value = selDeviceRegional;

    this.deviceList ? (this.deviceList[this.props.index] = keyword) : null;
    let deviceListobj = { keylabel: selDeviceRegional, keyword: keyword };
    this.addADevice(deviceListobj);
    this.showDeviceCount();
    //this.prefillDeviceList();
    this.chksuggestedlist();
    CompareTool.fetchSuggestedListData({
      dispatch,
      query,
      params,
      router,
      category,
      keyword,
    }).then(data => { });
  }
  showDeviceCount() {
    if (_isCSR()) {
      let filtered = window.compareDevice.filter(function (el) {
        return el != null;
      });
      if (document.getElementById("compare_count") && window.compareDevice.length) {
        let compcount = filtered.length;
        if (compcount > 0) document.getElementById("compare_count").innerHTML = "(" + compcount + ")";
        else document.getElementById("compare_count").innerHTML = "";
      }
    }
  }
  addADevice(deviceListobj) {
    const { dispatch, params, router, query } = this.props;
    let category = this.state.category;
    if (_isCSR()) {
      window.compareDevice[this.props.index] = deviceListobj;
      window.compareDevice[this.props.index].category = category;
    }
  }
  selectedoption(event) {
    let { params, query, router, dispatch } = this.props;
    this.setState({ category: event.target.value });
    this.props.router.push("/tech/compare-" + event.target.value);
    this.clearDevice();
    let category = event.target.value;
    // dispatch(fetchCompareDataIfNeeded(params, query, router, category));
  }

  compareGo(e) {
    let { pagetype, overlayCloseonSameRoute } = this.props;
    if (_isCSR()) {
      let filtered = window.compareDevice.filter(function (el) {
        return el != null;
      });
      let keywords = [];
      if (filtered.length < 2) {
        alert(siteConfig.locale.tech.selectatleast2);
        return false;
      }
      filtered.forEach(function (item) {
        keywords.push(item.keyword);
      });
      let url = "/" + keywords.join("-vs-");
      //this.props.router.push("/tech/"+ filtered[0].category); //for testing purpose;
      this.props.router.push("/tech/compare-" + filtered[0].category + url);
      //AnalyticsGA.event({ category: "Wap_CS", action: "compare", label: this.props.router.location.pathname });

      overlayCloseonSameRoute ? this.props.overlayCloseonSameRoute(e) : null;
    }
  }
  // addtocompare(e)
  // {
  //     let _this=this;
  //     let indexnumber="";
  //     let category=this.state.category;
  //     let keyword=e.target.getAttribute("seo");
  //     let selDeviceRegional=e.target.getAttribute("itemname");
  //     if(_this.deviceList && _this.deviceList.includes(keyword)) {alert(siteConfig.locale.tech.devicealreadyinlist);return false;}

  //     if(document.getElementById('deviceinput1') && document.getElementById('deviceinput1').value=='')
  //     {
  //         indexnumber=1;  document.getElementById('deviceinput1').value=selDeviceRegional
  //     }
  //     else if(document.getElementById('deviceinput2') &&  document.getElementById('deviceinput2').value=='')
  //     {
  //         indexnumber=2;  document.getElementById('deviceinput2').value=selDeviceRegional ;
  //     }
  //     else if(document.getElementById('deviceinput3') && document.getElementById('deviceinput3').value=='')
  //     {
  //         indexnumber=3;  document.getElementById('deviceinput3').value=selDeviceRegional
  //     }
  //     if(indexnumber!="")
  //     {
  //         _this.deviceList ? _this.deviceList[indexnumber] = keyword : null;
  //         let deviceListobj={keylabel: selDeviceRegional ,keyword : keyword,category: category};
  //         window.compareDevice[indexnumber] = deviceListobj;
  //     }

  // }
  addtocompare(e) {
    let obj = e.target;
    let keyword = e.target.getAttribute("seo");
    let selDeviceRegional = e.target.getAttribute("itemname");
    let category = this.state.category;
    //this function required item name, item seo name and category

    obj.classList.toggle("highlighted");

    if (obj.classList.contains("highlighted")) {
      let filtered = window.compareDevice.filter(function (el) {
        return el != null;
      });
      if (filtered.length == 3) {
        alert("You can not add more than 3 item to compare");
        obj.classList.toggle("highlighted");
        return false;
      }
      let deviceListobj = {
        keylabel: selDeviceRegional,
        keyword: keyword,
        category: category,
      };
      //fill blank input for compare
      if (
        document.getElementById("deviceinput1") &&
        (document.getElementById("deviceinput1").value == "" || !window.compareDevice[1])
      ) {
        document.getElementById("deviceinput1").value = selDeviceRegional;
        window.compareDevice[1] = deviceListobj;
      } else if (
        document.getElementById("deviceinput2") &&
        (document.getElementById("deviceinput2").value == "" || !window.compareDevice[2])
      ) {
        document.getElementById("deviceinput2").value = selDeviceRegional;
        window.compareDevice[2] = deviceListobj;
      } else if (
        document.getElementById("deviceinput3") &&
        (document.getElementById("deviceinput3").value == "" || !window.compareDevice[3])
      ) {
        document.getElementById("deviceinput3").value = selDeviceRegional;
        window.compareDevice[3] = deviceListobj;
      }
    } else {
      //remove compare item.
      let array = window.compareDevice;
      for (var i = 1; i < array.length; i++) {
        if (array[i] && array[i].keyword === keyword) {
          //array.splice(i, 1);
          window.compareDevice[i] = null;
          document.getElementById("deviceinput" + i).value = "";
        }
      }
    }
    this.showDeviceCount();
    this.chksuggestedlist();
  }

  render() {
    let pagetype = this.props.pagetype;
    let _this = this;
    let category = siteConfig.locale.tech.category;
    let device1 = this.props.item && this.props.item[1] ? this.props.item[1] : null;
    let device2 = this.props.item && this.props.item[2] ? this.props.item[2] : null;
    let device3 = this.props.item && this.props.item[3] ? this.props.item[3] : null;
    let suggested =
      this.props.suggested && this.props.suggested.gadget && this.props.suggested.gadget.length > 0
        ? this.props.suggested.gadget
        : null;

    return (
      <React.Fragment>
        <div className="wdt_compare_mobiles box-content">
          {typeof _this.props.pagetype != "undefined" &&
            typeof _this.props.pagetype != "null" &&
            _this.props.pagetype == "compare" ? (
              <div className="headingWithFilters">
                <div className="sectionHeading">
                  <h1>
                    <span>
                      {category.map((item, index) => {
                        return this.state.category == item.keyword ? item.keylabel + " " : "";
                      })}
                      {siteConfig.locale.tech.compare}
                    </span>
                  </h1>
                </div>

                <div className="change_device">
                  <label>{siteConfig.locale.tech.change}</label>
                  <select onChange={this.selectedoption.bind(this)}>
                    {category.map((item, index) => {
                      return (
                        <option
                          key={"category1" + index}
                          selected={this.state.category == item.keyword ? "selected" : ""}
                          value={item.keyword}
                        >
                          {item.keylabel}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            ) : null}
          <div className="compare_mobiles">
            <form name="comparetool">
              <div className="input_field">
                <input
                  onChange={_this.filterDevice.bind(_this)}
                  type="text"
                  id="deviceinput1"
                  name="deviceinput1"
                  placeholder={siteConfig.locale.tech.addadevice}
                />
                <div className="auto-suggest selectArea1" id="selectArea1">
                  <ul>
                    {device1
                      ? device1.map((item, index) => {
                        return (
                          <li onClick={_this.selectDevice} key={"deviceList1" + index} seo={item.seoname}>
                            {item.Product_name}
                          </li>
                        );
                      })
                      : null}
                  </ul>
                </div>
                <span className="close_icon" onClick={_this.clearDevice.bind(_this)}></span>
              </div>
              <div className="input_field">
                <input
                  onChange={this.filterDevice.bind(this)}
                  type="text"
                  id="deviceinput2"
                  name="deviceinput2"
                  placeholder={siteConfig.locale.tech.addadevice}
                />
                <div className="auto-suggest selectArea2" id="selectArea2">
                  <ul>
                    {device2
                      ? device2.map((item, index) => {
                        return (
                          <li onClick={_this.selectDevice} key={"deviceList2" + index} seo={item.seoname}>
                            {item.Product_name}
                          </li>
                        );
                      })
                      : null}
                  </ul>
                </div>
                <span className="close_icon" onClick={_this.clearDevice.bind(_this)}></span>
              </div>
              <div className="input_field">
                <input
                  onChange={this.filterDevice.bind(this)}
                  type="text"
                  id="deviceinput3"
                  name="deviceinput3"
                  placeholder={siteConfig.locale.tech.addadevice}
                />
                <div className="auto-suggest selectArea3" id="selectArea3">
                  <ul>
                    {device3
                      ? device3.map((item, index) => {
                        return (
                          <li onClick={_this.selectDevice} key={"deviceList3" + index} seo={item.seoname}>
                            {item.Product_name}
                          </li>
                        );
                      })
                      : null}
                  </ul>
                </div>
                <span className="close_icon" onClick={_this.clearDevice.bind(_this)}></span>
              </div>
              <input type="hidden" name="category" value={this.state.category} />
              <input type="hidden" name="type" value="brand" />
              <input type="hidden" name="tag" value="product_cat" />

              <input
                onClick={e => this.compareGo(e)}
                type="button"
                name="compare"
                value={siteConfig.locale.tech.comparetxt}
                className="more-btn"
              />
            </form>
          </div>
          <div id="suggested">
            {suggested ? (
              <GadgetRelated
                addtocompare={this.addtocompare}
                pagetype={pagetype}
                suggested={suggested}
                anchorlink="true"
              />
            ) : null}
          </div>
          {pagetype && (pagetype == "compare" || pagetype == "gadgetlist" || pagetype == "compareshow") ? (
            <AdCard key="listad_comapre" mstype="ctnbiglist" adtype="ctn" />
          ) : null}
        </div>
        {/* <span>
                        {
                            GadgetCompare({item: compareGadget.compare})
                        }
                    </span> */}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.comparetool,
  };
}

CompareTool.fetchDeviceListData = ({ dispatch, params, query, router, category, keyword, index }) => {
  return dispatch(fetchDeviceListDataIfNeeded(params, query, router, category, keyword, index));
};
CompareTool.fetchSuggestedListData = ({ dispatch, params, query, router, category, keyword, index }) => {
  return dispatch(fetchSuggestedListDataIfNeeded(params, query, router, category, keyword, index));
};

export default connect(mapStateToProps)(CompareTool);
