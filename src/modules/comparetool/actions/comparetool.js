import fetch from "utils/fetch/fetch";
import { _apiBasepointUpdate, _getfeedCategory } from "../../../utils/util";
export const FETCH_DEVICELIST_REQUEST = "FETCH_DEVICELIST_REQUEST";
export const FETCH_DEVICELIST_SUCCESS = "FETCH_DEVICELIST_SUCCESS";
export const FETCH_DEVICELIST_FAILURE = "FETCH_DEVICELIST_FAILURE";
export const FETCH_SUGGESTEDLIST_REQUEST = "FETCH_SUGGESTEDLIST_REQUEST";
export const FETCH_SUGGESTEDLIST_SUCCESS = "FETCH_SUGGESTEDLIST_SUCCESS";
export const FETCH_SUGGESTEDLIST_FAILURE = "FETCH_SUGGESTEDLIST_FAILURE";

//get device list action creator

function fetchDeviceListDataFailure(error) {
  return {
    type: FETCH_DEVICELIST_FAILURE,
    payload: error.message
  };
}
function fetchDeviceListDataSuccess(data, params, category, keyword, index) {
  return {
    type: FETCH_DEVICELIST_SUCCESS,
    payload: data,
    keyword: keyword,
    category: category,
    index: index
  };
}

function fetchDeviceListData(
  state,
  params,
  query,
  router,
  category,
  keyword,
  index
) {
  //send keyword mobile rather than mobile-phones
  category = _getfeedCategory(category);
  let apiUrl =
    _apiBasepointUpdate(router, params.splat) +
    "/autosuggestion.cms?type=brand&tag=product_cat&category=" +
    category +
    "&q=" +
    keyword;

  return dispatch => {
    dispatch({
      type: FETCH_DEVICELIST_REQUEST,
      payload: params
    });
    return fetch(apiUrl)
      .then(
        data => {
          dispatch(
            fetchDeviceListDataSuccess(data, params, category, keyword, index)
          );
        },
        error => dispatch(fetchDeviceListDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchDeviceListDataFailure(error));
      });
  };
}

function shouldFetchDeviceListData(state, params, query) {
  return true;
}

export function fetchDeviceListDataIfNeeded(
  params,
  query,
  router,
  category,
  keyword,
  index
) {
  return (dispatch, getState) => {
    if (shouldFetchDeviceListData(getState(), params, query)) {
      return dispatch(
        fetchDeviceListData(
          getState(),
          params,
          query,
          router,
          category,
          keyword,
          index
        )
      );
    } else {
      return Promise.resolve([]);
    }
  };
}

//get suggested device list action creator

function fetchSuggestedListDataFailure(error) {
  return {
    type: FETCH_SUGGESTEDLIST_FAILURE,
    payload: error.message
  };
}
function fetchSuggestedListDataSuccess(data, params, category, keyword, index) {
  return {
    type: FETCH_SUGGESTEDLIST_SUCCESS,
    payload: data,
    keyword: keyword,
    category: category,
    index: index
  };
}

function fetchSuggestedListData(
  state,
  params,
  query,
  router,
  category,
  keyword
) {
  //send keyword mobile rather than mobile-phones
  category = _getfeedCategory(category);
  let apiUrl =
    _apiBasepointUpdate(router, params.splat) +
    "/wdt_gadgetreleted.cms?feedtype=sjson&type=brand&category=" +
    category +
    "&brandname=" +
    keyword +
    "&productid=" +
    keyword;

  return dispatch => {
    dispatch({
      type: FETCH_SUGGESTEDLIST_REQUEST,
      payload: params
    });
    return fetch(apiUrl)
      .then(
        data => {
          dispatch(
            fetchSuggestedListDataSuccess(data, params, category, keyword)
          );
        },
        error => dispatch(fetchSuggestedListDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchSuggestedListDataFailure(error));
      });
  };
}

function shouldFetchSuggestedListData(state, params, query) {
  return true;
}

export function fetchSuggestedListDataIfNeeded(
  params,
  query,
  router,
  category,
  keyword,
  index
) {
  return (dispatch, getState) => {
    if (shouldFetchSuggestedListData(getState(), params, query)) {
      return dispatch(
        fetchSuggestedListData(
          getState(),
          params,
          query,
          router,
          category,
          keyword,
          index
        )
      );
    } else {
      return Promise.resolve([]);
    }
  };
}
