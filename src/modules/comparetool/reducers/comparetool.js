import {
  FETCH_DEVICELIST_REQUEST,
  FETCH_DEVICELIST_SUCCESS,
  FETCH_DEVICELIST_FAILURE,
  FETCH_SUGGESTEDLIST_REQUEST,
  FETCH_SUGGESTEDLIST_SUCCESS,
  FETCH_SUGGESTEDLIST_FAILURE
} from "./../actions/comparetool";

function comparetool(
  state = {
    isFetching: false,
    error: false,
    item: [],
    head: {},
    index: "",
    category: "",
    msid: "" //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action
) {
  switch (action.type) {
    case FETCH_DEVICELIST_REQUEST:
      state.category = action.category;
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_DEVICELIST_SUCCESS:
      if (action.payload) {
        state.index = action.index;
        state.item[action.index] = action.payload;
        state.keyword = action.keyword;
      }
      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_DEVICELIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_SUGGESTEDLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_SUGGESTEDLIST_SUCCESS:
      if (action.payload) {
        state.suggested = action.payload;
      }
      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_SUGGESTEDLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default comparetool;
