import {
  FETCH_FILTERLIST_REQUEST,
  FETCH_FILTERLIST_SUCCESS,
  FETCH_FILTERLIST_FAILURE,
  FETCH_SUGGESTEDLIST_REQUEST,
  FETCH_SUGGESTEDLIST_SUCCESS,
  FETCH_SUGGESTEDLIST_FAILURE
} from "./../actions/gadgetfiltertool";

function gadgetfiltertool(
  state = {
    isFetching: false,
    error: false,
    head: {},
    index: "",
    techgadgetfilter: [],
    category: "",
    msid: "" //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action
) {
  switch (action.type) {
    case FETCH_FILTERLIST_REQUEST:
      state.category = action.category ? action.category : "";
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_FILTERLIST_SUCCESS:
      if (
        action.payload &&
        (state.category != action.category ||
          (action.payload &&
            action.payload.facets &&
            action.payload.facets.length > 0))
      ) {
        state.techgadgetfilter = action.payload;
      }
      return {
        ...state,
        isFetching: false,
        error: false
      };

    case FETCH_FILTERLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default gadgetfiltertool;
