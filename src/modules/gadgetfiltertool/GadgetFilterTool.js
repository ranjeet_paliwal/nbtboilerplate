import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchFilterListDataIfNeeded } from "./actions/gadgetfiltertool";

import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import styles from "./../../components/common/css/GadgetNow.scss";
import { _getStaticConfig, _isCSR } from "../../utils/util";

const siteConfig = _getStaticConfig();

class GadgetFilterTool extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    const { dispatch, params, router, value, category } = this.props;
    const { query } = this.props.router.location.pathname;

    GadgetFilterTool.fetchFilterListData({
      dispatch,
      query,
      params,
      router,
    }).then(data => {});
  }
  TabClick(obj) {
    let alltabs = obj.target.parentElement.parentElement.children;
    let getIndex = obj.target.parentElement.getAttribute("index");
    //remove active class tab
    for (let i = 0; i < alltabs.length; i++) {
      alltabs[i].classList.remove("active");
    }
    obj.target.parentElement.classList.add("active");
    //show  active class content
    let alltabsContent = document.querySelectorAll("div.tab-values li");
    for (let i = 0; i < alltabsContent.length; i++) {
      if (getIndex == i) alltabsContent[i].classList.add("active");
      else alltabsContent[i].classList.remove("active");
    }
  }
  removeFilter(obj) {
    if (obj.target.getAttribute("class") == "close_icon") {
      document.getElementById(obj.target.parentElement.id).remove();
      let getInputboxId = obj.target.parentElement.id.substring(3);
      eval("document.filtercriteria." + getInputboxId + ".checked=false");
    }
  }
  filterCases(obj) {
    if (obj.target.checked) {
      let closenode = document.createElement("span");
      closenode.className = "close_icon";
      let div = document.createElement("div");
      div.className = "filterkey";
      div.id = "div" + obj.target.name;
      if (obj.target.getAttribute("datavalues") == "rumoured" || obj.target.getAttribute("datavalues") == "upcoming")
        div.innerHTML = obj.target.getAttribute("datavalues");
      else div.innerHTML = obj.target.parentElement.innerText;
      div.appendChild(closenode);
      document.getElementById("filterapplied").appendChild(div);
    } else {
      document.getElementById("div" + obj.target.getAttribute("name")).remove();
    }
  }
  HtmlSearch(obj) {
    let keyword = obj.target.value.toLowerCase();
    if (keyword == "") {
      //show all check box
      let allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        //let inputbox=allcheckbox[i].getElementsByTagName("input");
        allcheckbox[i].classList.remove("hide");
      }
    } else {
      //showcase based on keyword
      let allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        let inputbox = allcheckbox[i].getElementsByTagName("input");

        let inputval = inputbox[0].value.toLowerCase();
        if (inputval.indexOf(keyword) < 0) allcheckbox[i].classList.add("hide");
      }
    }
  }
  render() {
    let _this = this;
    let { techgadgetfilter, category, params } = this.props;
    return (
      <form name="filtercriteria" id="filtercriteria">
        <div className="filters">
          <React.Fragment>
            <ErrorBoundary>
              <div className="filterapplied scroll" id="filterapplied" onClick={this.removeFilter.bind(this)}></div>
              <div className="tabs">
                <ul>
                  <li index="0" id="filterby-sort" onClick={_this.TabClick.bind(this)} className="active filterby-sort">
                    <span>SORT BY</span>
                  </li>
                  {techgadgetfilter && techgadgetfilter.facets && techgadgetfilter.facets.length > 0
                    ? techgadgetfilter.facets.map((item, index) => {
                        return (
                          <li
                            index={index + 1}
                            onClick={_this.TabClick.bind(this)}
                            className={"filterby-" + item.q_param}
                          >
                            <span>{item.d_name}</span>
                          </li>
                        );
                      })
                    : null}
                </ul>
              </div>
              <div className="tab-values">
                <div id="sortData">
                  <ul>
                    <li className="active filterby-sort" id="filterby-sortcontent">
                      {/* <label>
                                <input type="radio" name="sortby" value="recent"/>Recently Reviewed
                            </label> */}
                      <label>
                        <input type="radio" name="sortby" value="latest" selected="selected" />
                        Latest
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="popular" />
                        Popular
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="rating-desc" />
                        Rating
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="price-asc" />
                        Price (Low To High)
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="price-desc" />
                        Price (High To Low)
                      </label>
                    </li>
                    {techgadgetfilter && techgadgetfilter.facets && techgadgetfilter.facets.length > 0
                      ? techgadgetfilter.facets.map((item, index) => {
                          let filtercase = item.q_param;
                          return (
                            <li className="hide" className={"filtercontent-" + item.q_param}>
                              {item.q_param == "brand" || item.q_param == "price-range" || item.q_param == "colour" ? (
                                <p className="search">
                                  <input
                                    onChange={_this.HtmlSearch.bind(this)}
                                    placeholder="Search"
                                    data-search-for={item.q_param}
                                    className="filters-search-box"
                                    type="text"
                                    autoComplete="off"
                                  />
                                </p>
                              ) : null}
                              {item && item.values && item.values.length > 0 ? (
                                item.values.map((item, index) => {
                                  return (
                                    <label>
                                      <input
                                        onClick={_this.filterCases.bind(this)}
                                        index={index}
                                        datavalues={filtercase}
                                        type="checkbox"
                                        name={filtercase.replace("-", "_") + index}
                                        value={item.qname}
                                      />
                                      {item.d_name}
                                    </label>
                                  );
                                })
                              ) : item && item.values && item.values.val ? (
                                <label>
                                  <input
                                    onClick={_this.filterCases.bind(this)}
                                    index="0"
                                    datavalues={filtercase}
                                    type="checkbox"
                                    name={filtercase.replace("-", "_") + "0"}
                                    value={item.values.val.qname}
                                  />
                                  {item.values.val.d_name}
                                </label>
                              ) : null}
                            </li>
                          );
                        })
                      : null}
                  </ul>
                </div>
              </div>
            </ErrorBoundary>
          </React.Fragment>
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.gadgetfiltertool,
  };
}
GadgetFilterTool.fetchFilterListData = ({ dispatch, params, query, router, category, keyword, index }) => {
  return dispatch(fetchFilterListDataIfNeeded(params, query, router, category, keyword, index));
};
export default connect(mapStateToProps)(GadgetFilterTool);
