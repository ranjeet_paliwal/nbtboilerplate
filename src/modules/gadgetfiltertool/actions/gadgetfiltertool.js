import fetch from "utils/fetch/fetch";
import { _apiBasepointUpdate, _getfeedCategory } from "../../../utils/util";
export const FETCH_FILTERLIST_REQUEST = "FETCH_FILTERLIST_REQUEST";
export const FETCH_FILTERLIST_SUCCESS = "FETCH_FILTERLIST_SUCCESS";
export const FETCH_FILTERLIST_FAILURE = "FETCH_FILTERLIST_FAILURE";
export const FETCH_SUGGESTEDLIST_REQUEST = "FETCH_SUGGESTEDLIST_REQUEST";
export const FETCH_SUGGESTEDLIST_SUCCESS = "FETCH_SUGGESTEDLIST_SUCCESS";
export const FETCH_SUGGESTEDLIST_FAILURE = "FETCH_SUGGESTEDLIST_FAILURE";

//get device list action creator

function fetchFilterListDataFailure(error) {
  return {
    type: FETCH_FILTERLIST_FAILURE,
    payload: error.message
  };
}
function fetchFilterListDataSuccess(data, params, category) {
  return {
    type: FETCH_FILTERLIST_SUCCESS,
    payload: data,
    category: category
  };
}

function fetchFilterListData(state, params, query, router, categoryoverride) {
  //send keyword mobile rather than mobile-phones
  let category = categoryoverride
    ? categoryoverride
    : params.category
    ? params.category
    : "mobile-phones";
  category = _getfeedCategory(category);
  let apiUrl =
    _apiBasepointUpdate(router, params.splat) +
    "/pwa_filtercriteria.cms?feedtype=sjson&category=" +
    category;

  return dispatch => {
    dispatch({
      type: FETCH_FILTERLIST_REQUEST,
      payload: params,
      category: category
    });
    return fetch(apiUrl)
      .then(
        data => {
          dispatch(fetchFilterListDataSuccess(data, params, category));
        },
        error => dispatch(fetchFilterListDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchFilterListDataFailure(error));
      });
  };
}

function shouldFetchFilterListData(state, params, query) {
  return true;
}

export function fetchFilterListDataIfNeeded(
  params,
  query,
  router,
  categoryoverride
) {
  return (dispatch, getState) => {
    if (shouldFetchFilterListData(getState(), params, query)) {
      return dispatch(
        fetchFilterListData(getState(), params, query, router, categoryoverride)
      );
    } else {
      return Promise.resolve([]);
    }
  };
}
