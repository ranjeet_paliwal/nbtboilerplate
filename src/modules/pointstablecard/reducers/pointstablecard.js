import {
  FETCH_POINTSTABLECARD_REQUEST,
  FETCH_POINTSTABLECARD_SUCCESS,
  FETCH_POINTSTABLECARD_FAILURE
} from "../actions/pointstablecard";

function pointstablecard(
  state = {
    isFetching: false,
    error: false,
    msid: "" //Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action
) {
  switch (action.type) {
    case FETCH_POINTSTABLECARD_REQUEST:
      state.pointstablecard =
        state.pointstablecard &&
        state.pointstablecard.standings &&
        state.pointstablecard.standings.stage1 &&
        state.pointstablecard.standings.stage1.team
          ? { ...state.pointstablecard }
          : {};
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_POINTSTABLECARD_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_POINTSTABLECARD_SUCCESS:
      let pointstable_data = action.payload;
      if (
        pointstable_data &&
        pointstable_data.standings &&
        pointstable_data.standings.stage1 &&
        pointstable_data.standings.stage1.team &&
        pointstable_data.standings.stage1.team.length > 0
      ) {
        state.pointstablecard = pointstable_data;
      }
      return {
        ...state,
        isFetching: false,
        error: false
      };

    default:
      return state;
  }
}

export default pointstablecard;
