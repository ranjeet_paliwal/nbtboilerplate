import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "utils/util";
const siteConfig = _getStaticConfig();
export const FETCH_POINTSTABLECARD_REQUEST = "FETCH_POINTSTABLECARD_REQUEST";
export const FETCH_POINTSTABLECARD_SUCCESS = "FETCH_POINTSTABLECARD_SUCCESS";
export const FETCH_POINTSTABLECARD_FAILURE = "FETCH_POINTSTABLECARD_FAILURE";

function fetchPointsTableCardFailure(error) {
  return {
    type: FETCH_POINTSTABLECARD_FAILURE,
    payload: error.message
  };
}

function fetchPointsTableCardSuccess(data, params) {
  return {
    type: FETCH_POINTSTABLECARD_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    seriesid: params.seriesid,
    payload: data
  };
}

function fetchPointsTableCardData(state, params, query) {
  let apiUrl = siteConfig.cricket_SIAPI.points_table;
  apiUrl = apiUrl.replace("%seriesid%", params.seriesid);

  return dispatch => {
    dispatch({
      type: FETCH_POINTSTABLECARD_REQUEST
    });
    return fetch(apiUrl)
      .then(
        data => dispatch(fetchPointsTableCardSuccess(data, params)),
        error => dispatch(fetchPointsTableCardFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchPointsTableCardFailure(error));
      });
  };
}

function shouldFetchPointsTableCardData(state, params, query) {
  return true;
  // if(state.app && (!state.app.minitv || !state.app.minitv.hl)){
  //   return true;
  // } else {
  //   return false;
  // }
}

export function fetchPointsTableCardDataIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchPointsTableCardData(getState(), params, query)) {
      return dispatch(fetchPointsTableCardData(getState(), params, query));
    } else {
      return Promise.resolve([]);
    }
  };
}
