import React, { Component } from "react";
import { connect } from "react-redux";
//import { Link } from 'react-router'
// import PropTypes from "prop-types";
import { fetchPointsTableCardDataIfNeeded } from "./actions/pointstablecard";
import "./../../components/common/css/PointsTable.scss";
import AnchorLink from "../../components/common/AnchorLink";
import { _isCSR, _getStaticConfig } from "../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../utils/cricket_util";
import ImageCard from "../../components/common/ImageCard/ImageCard";
// const siteConfig = _getStaticConfig();

class PointsTableCard extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    let _this = this;
    const { dispatch } = _this.props;
    let params = {
      seriesid: this.props.seriesid,
    };
    if (_isCSR() && this.props.seriesid && this.props.seriesid != "") {
      dispatch(fetchPointsTableCardDataIfNeeded(params)); //Load Schedule Widget
    }
  }

  render() {
    let _this = this;
    let pointstable =
      this.props.pointstablecard &&
      this.props.pointstablecard.standings &&
      this.props.pointstablecard.standings.stage1 &&
      this.props.pointstablecard.standings.stage1.team &&
      this.props.pointstablecard.standings.stage1.team.length > 0
        ? this.props.pointstablecard.standings.stage1.team
        : null;
    return (
      <React.Fragment>
        {pointstable && typeof pointstable != "null" ? (
          <div className="box-content">
            <div className="wdt_pointsTable">
              <h2>
                <AnchorLink hrefData={{ override: this.props.pageHeadingLink }}>
                  <span>{this.props.pageHeading}</span>
                </AnchorLink>
              </h2>
              <div className="table_layout">
                <table>
                  <tbody>
                    <tr>
                      <th>TEAM</th>
                      <th>P</th>
                      <th>W</th>
                      <th>L</th>
                      <th>PT</th>
                    </tr>

                    {pointstable.map((item, index) => {
                      return (
                        <tr key={index}>
                          <td>
                            <ImageCard msid={_getTeamLogo(item.id, "square")} size="rectanglethumb" />
                            {item.name}
                          </td>
                          <td>{item.p}</td>
                          <td>{item.w}</td>
                          <td>{item.l}</td>
                          <td>{item.pts}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}
function mapStateToProps(state) {
  return {
    ...state.pointstablecard,
  };
}
export default connect(mapStateToProps)(PointsTableCard);
