/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
export const playOdometer = arg => {
  const { speed, fontSize } = { ...arg };
  // const odometers = document.getElementsByClassName("odometer");
  const odometers = document.getElementById("corona-banner-odometer").getElementsByClassName("odometer");

  const config = {
    speed: speed || 0.5 * 1000, // default delay by half sec each digit
    fontSize: fontSize || 20,
  };
  const defaultDigitNode = document.createElement("div");
  defaultDigitNode.classList.add("digit");

  for (let i = 0; i <= 9; i++) {
    defaultDigitNode.innerHTML += `${i}<br>`;
  }

  const odometerRenderer = odometer => {
    if (typeof odometer === "undefined") return false;
    odometer.innerHTML = "";
    let currentValue = odometer.getAttribute("data-odometer") || 0;
    if (currentValue) {
      currentValue = parseInt(currentValue.replace(/[^0-9]/g, ""));
    }

    const digits = [];

    generateDigits(currentValue.toString().length);
    setValue(currentValue);

    // setInterval(() => {
    //   setValue(Math.floor(Math.random() * 1000000));
    // }, 20000);

    function setValue(number) {
      const s = number
        .toString()
        .split("")
        .reverse()
        .join("");
      const l = s.length;

      if (l > digits.length) {
        generateDigits(l - digits.length);
      }

      for (let i = 0; i < digits.length; i++) {
        setInterval(() => setDigit(i, s[i] || 0), i * config.speed);
      }
    }

    function setDigit(digitIndex, number) {
      digits[digitIndex].style.marginTop = `-${config.fontSize * number}px`;
    }

    function generateDigits(amount) {
      for (let i = 0; i < amount; i++) {
        const d = defaultDigitNode.cloneNode(true);
        odometer.appendChild(d);
        digits.unshift(d);
      }
    }
  };

  [].forEach.call(odometers, odometer => odometerRenderer(odometer));
};
