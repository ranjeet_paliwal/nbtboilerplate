import React, { Component } from "react";
// import { Link, browserHistory } from 'react-router'
// import PropTypes from 'prop-types';
import { connect } from "react-redux";
// import Helmet from 'react-helmet';
import { fetchCheckLiveBlogIfNeeded, fetchLiveBlogIfNeeded } from "./actions/liveblogstrip";
// import ErrorBoundary from './../../components/lib/errorboundery/ErrorBoundary';
import styles from "./../../components/common/css/liveblog-strip.scss";
import AnchorLink from "../../components/common/AnchorLink";
import { SeoSchema } from "../../components/common/PageMeta";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import { _getStaticConfig, _dateFormatter } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;
// import { electionConfig } from '../../campaign/election/utils/config';
// const _electionConfig = electionConfig[process.env.SITE]

export class LiveBlogStrip extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { dispatch, query, params, msid } = this.props;
    params = params ? params : {};
    params.msid = msid ? msid : "";
    LiveBlogStrip.fetchData({ dispatch, query, params });
    // LiveBlogStrip.fetchData({dispatch, query, params}).then((data)=>{
    //   LiveBlogStrip.fetchLBData({dispatch, query, params})
    // });
    // let timer = setInterval(() => {
    //   LiveBlogStrip.fetchData({ dispatch, query, params })
    //   //   if (_this.config.pollingClear) clearInterval(timer);
    // }, 60000);
  }

  render() {
    const { lbdata, widgetType, liveblog_page_url, lbconfig } = this.props;

    if (
      typeof lbdata != "undefined" &&
      lbdata.hasOwnProperty("lbcontent") &&
      lbdata.lbcontent &&
      lbdata.hasOwnProperty("lbcontainer") &&
      lbdata.lbcontainer
    ) {
      return widgetType && widgetType == "multipleposts" ? (
        <div className="live_blog_widget inner_box_widget">
          <div className="h2WithViewMore">
            <h2 className="election_h2">{lbconfig && lbconfig.liveBlogTxt ? lbconfig.liveBlogTxt : "Live Blog"}</h2>
            <AnchorLink
              hrefData={{
                override: lbconfig && lbconfig.url ? lbconfig.url : siteConfig.liveblogbnbewsurl,
              }}
              className="view_more"
            >
              {lbconfig && lbconfig.viewAllTxt ? lbconfig.viewAllTxt : "View All Updates"}
            </AnchorLink>
          </div>
          <ul className="nbt-list">
            <li className="table nbt-listview lead-post">
              <span className="table_row" href="#">
                <span className="table_col img_wrap">
                  <ImageCard msid={lbdata.lbcontainer.containerId} title="" size="bigimage" />
                </span>
                <span className="table_col con_wrap">
                  <span className="text_ellipsis">{lbdata.lbcontainer.msName}</span>
                </span>
              </span>
            </li>
            {lbdata &&
              lbdata.lbcontent.length > 0 &&
              lbdata.lbcontent.map((item, index) => {
                return (
                  index < 5 && (
                    <li key={index} className="table nbt-listview">
                      <div className="times_stamp" itemProp="datePublished" content={item.updateDate}>
                        <span
                          {...SeoSchema({ pagetype: "liveblog" }).attr().modified}
                          content={item.updateDate}
                          className={styles.blog_time + " blog_time"}
                        >
                          {item.shortDateTime},{" "}
                        </span>
                        <span className={styles.blog_date + " blog_date"}>
                          {_dateFormatter(item.artDate).datestring.substring(4, 20)}
                        </span>
                      </div>
                      {/* <span className="times_stamp">1 min Ago</span> */}
                      <span className="table_row" href="#">
                        <span className="table_col con_wrap">
                          <span className="text_ellipsis">
                            {item.hoverridLink != "" &&
                            item.hoverridLink != null &&
                            item.hoverridLink != undefined &&
                            item.hoverridLink != "null" &&
                            item.hoverridLink != "undefined" ? (
                              <AnchorLink
                                hrefData={{ override: item.hoverridLink }}
                                {...SeoSchema({ pagetype: "liveblog" }).attr().entity}
                              >
                                {item.title}
                              </AnchorLink>
                            ) : (
                              item.title
                            )}
                          </span>
                        </span>
                      </span>
                    </li>
                  )
                );
              })}
          </ul>
        </div>
      ) : (
        <div className={styles["lb-strip"] + " lb-strip"}>
          <div className={styles["lb-head-wrapper"] + " lb-head-wrapper"}>
            <span className={styles["lb-heading"] + " lb-heading"}>{locale.live_updates}</span>
            <span className={styles["lb-title"] + " lb-title"}>{lbdata.lbcontainer.msName}</span>
          </div>
          <div className={styles["lb-card"] + " table lb-card"} key={lbdata.lbcontent[0].msid}>
            <AnchorLink hrefData={{ override: siteConfig.liveblogbnbewsurl }} className="table_row">
              {/*
                 lbdata.lbcontent[0].imgUrl ? <span className="table_col img_wrap"> <img  src={'https://'+lbdata.lbcontent[0].imgUrl} /> </span> : null
                 */}
              <span className={styles["con_wrap"] + " table_col con_wrap"}>
                <span
                  className={styles["time-caption"] + " time-caption"}
                  data-time={lbdata.lbcontent[0].artDate ? lbdata.lbcontent[0].artDate : ""}
                >
                  {lbdata.lbcontent[0].artDate ? lbdata.lbcontent[0].artDate : lbdata.lbcontent[0].artDate}
                </span>
                <span className="text_ellipsis">{lbdata.lbcontent[0].title}</span>
              </span>
              <span className="table_col btn_wrap">
                <button type="button" className="button">
                  {locale.read_more_liveblog}
                </button>
              </span>
            </AnchorLink>
          </div>
        </div>
      );
    } else return null;
  }
}

LiveBlogStrip.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchLiveBlogIfNeeded(params, query));
  //return dispatch(fetchCheckLiveBlogIfNeeded(params, query));
};

LiveBlogStrip.fetchLBData = function({ dispatch, query, params }) {
  return dispatch(fetchLiveBlogIfNeeded(params, query));
};

LiveBlogStrip.propTypes = {};

function mapStateToProps(state) {
  return {
    ...state.liveblogstrip,
  };
}

export default connect(mapStateToProps)(LiveBlogStrip);
