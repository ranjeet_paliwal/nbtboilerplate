import {
  FETCH_LIVEBLOGSTRIP_REQUEST,
  FETCH_LIVEBLOGSTRIP_SUCCESS,
  FETCH_LIVEBLOGSTRIP_FAILURE,
  FETCH_CHECK_LIVEBLOG_REQUEST,
  FETCH_CHECK_LIVEBLOG_SUCCESS,
  FETCH_CHECK_LIVEBLOG_FAILURE
} from "./../actions/liveblogstrip";

function liveblogstrip(
  state = {
    isFetching: false,
    error: false,
    //checklb: {},
    lbdata: {}
  },
  action
) {
  switch (action.type) {
    case FETCH_CHECK_LIVEBLOG_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_CHECK_LIVEBLOG_SUCCESS:
      state.checklb = action.payload;
      return {
        ...state,
        isFetching: false
      };

    case FETCH_CHECK_LIVEBLOG_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };
    case FETCH_LIVEBLOGSTRIP_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false
      };
    case FETCH_LIVEBLOGSTRIP_SUCCESS:
      state.lbdata.lbcontainer = action.payload.lbcontainer;
      state.lbdata.lbcontent = [];
      //state.lbdata.lbcontent[0] = action.payload.lbcontent && action.payload.lbcontent[0] ? action.payload.lbcontent[0] : {};
      state.lbdata.lbcontent = action.payload.lbcontent
        ? action.payload.lbcontent
        : {};
      return {
        ...state,
        isFetching: false
      };

    case FETCH_LIVEBLOGSTRIP_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      };

    default:
      return state;
  }
}

export default liveblogstrip;
