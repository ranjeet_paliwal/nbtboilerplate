export const FETCH_CHECK_LIVEBLOG_REQUEST = "FETCH_CHECK_LIVEBLOG_REQUEST";
export const FETCH_CHECK_LIVEBLOG_SUCCESS = "FETCH_CHECK_LIVEBLOG_SUCCESS";
export const FETCH_CHECK_LIVEBLOG_FAILURE = "FETCH_CHECK_LIVEBLOG_FAILURE";
export const FETCH_LIVEBLOGSTRIP_REQUEST = "FETCH_LIVEBLOGSTRIP_REQUEST";
export const FETCH_LIVEBLOGSTRIP_SUCCESS = "FETCH_LIVEBLOGSTRIP_SUCCESS";
export const FETCH_LIVEBLOGSTRIP_FAILURE = "FETCH_LIVEBLOGSTRIP_FAILURE";

import fetch from "utils/fetch/fetch";
import { _getStaticConfig } from "./../../../utils/util";
const siteConfig = _getStaticConfig();

function fetchCheckLiveBlogDataFailure(error) {
  return {
    type: FETCH_CHECK_LIVEBLOG_FAILURE,
    payload: error.message
  };
}

function fetchCheckLiveBlogSuccess(data) {
  return {
    type: FETCH_CHECK_LIVEBLOG_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchLiveBlogDataFailure(error) {
  return {
    type: FETCH_LIVEBLOGSTRIP_FAILURE,
    payload: error.message
  };
}
function fetchLiveBlogSuccess(data) {
  return {
    type: FETCH_LIVEBLOGSTRIP_SUCCESS,
    meta: {
      receivedAt: Date.now()
    },
    payload: data
  };
}

function fetchCheckLiveBlogData(state, params, query) {
  //let lbCheckApi = state.app.urls.live_blog_check_url;
  let lbCheckApi = process.env.API_ENDPOINT + "/checkliveblog";
  return dispatch => {
    /*dispatch({
      type: FETCH_CHECK_LIVEBLOG_REQUEST
    });*/
    return fetch(lbCheckApi)
      .then(
        data => dispatch(fetchCheckLiveBlogSuccess(data)),
        error => dispatch(fetchCheckLiveBlogDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchCheckLiveBlogDataFailure(error));
      });
  };
}

function fetchLiveBlogData(state, params, query) {
  //let lbApi = state.app.urls.live_blog_url;
  // let currentState = state.liveblogstrip;
  // if(typeof(currentState)!='undefined' && typeof(currentState.checklb)!='undefined' && currentState.checklb.hasOwnProperty('liveblog') && currentState.checklb.liveblog.hasOwnProperty('lb') && currentState.checklb.liveblog.lb.hasOwnProperty('msid')){
  //   let msid = currentState.checklb.liveblog.lb.msid;

  //let lbApi = process.env.API_ENDPOINT+"/getliveblog?msid=58166012"
  //let lbApi = process.env.API_ENDPOINT+"/getindialiveblog_hp";
  let lbApi = "";
  if (params && params.msid) {
    lbApi = `${process.env.API_BASEPOINT}/pwafeeds/pwa_livebloghome/${params.msid}.cms?feedtype=sjson`;
  } else {
    lbApi = siteConfig.livebloghpfeed;
  }
  return dispatch => {
    dispatch({
      type: FETCH_LIVEBLOGSTRIP_REQUEST
    });
    return fetch(lbApi)
      .then(
        data => dispatch(fetchLiveBlogSuccess(data)),
        error => dispatch(fetchLiveBlogDataFailure(error))
      )
      .catch(function(error) {
        dispatch(fetchLiveBlogDataFailure(error));
      });
  };
}

function shouldFetchLiveBlogData(state, params, query) {
  if (
    typeof state.liveblogstrip.lbdata == "undefined" ||
    !state.liveblogstrip.lbdata.hasOwnProperty("lbcontent")
  ) {
    return true;
  } else {
    return false;
  }
}

export function fetchLiveBlogIfNeeded(params, query, widgetType) {
  return (dispatch, getState) => {
    if (shouldFetchLiveBlogData(getState(), params, query)) {
      return dispatch(fetchLiveBlogData(getState(), params, query, widgetType));
    }
  };
}

export function fetchCheckLiveBlogIfNeeded(params, query) {
  return (dispatch, getState) => {
    if (shouldFetchLiveBlogData(getState(), params, query)) {
      return dispatch(fetchCheckLiveBlogData(getState(), params, query));
    }
  };
}
