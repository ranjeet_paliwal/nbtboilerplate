import cheerio from "cheerio";

export function appShellConverter(html, pagetype, reqUrl, version) {
  if (!html) return "";

  const $ = cheerio.load(html.html);
  let $head = $.find("head");

  $("script").each((i, elem) => {
    if (
      elem.attribs["id"] &&
      (elem.attribs["id"].includes("process_env_js") ||
        elem.attribs["id"].includes("device_related_js") ||
        elem.attribs["id"].includes("main_js_container") ||
        elem.attribs["id"].includes("initialState"))
    ) {
    } else {
      $(elem).remove();
    }
  });
  $head.append(`<script> var googletag = googletag || {};</script>`);

  //   $("body div").each((i, elem) => {
  //     if (elem.attribs["id"] && elem.attribs["id"].includes("root")) {
  //     } else {
  //       $(elem).remove();
  //     }
  //   });

  $("#parentContainer .mobile_body").children((i, elem) => {
    if (elem.attribs["id"] && elem.attribs["id"].includes("headerContainer")) {
    } else {
      $(elem).remove();
    }
  });

  return $.html();

  let _html = `<meta charSet="utf-8" /> 
            <meta httpEquiv="x-ua-compatible" content="ie=edge,chrome=1" />
            <meta name="viewport" content="width=device-width, height=device-height,initial-scale=1.0,user-scalable=yes,maximum-scale=5"
            />`;
  let styleHtml = "";

  $("style").each((i, elem) => {
    styleHtml += $(elem).html();
  });
  _html += "<style>" + styleHtml + "</style>";

  let headerHtml = $("#headerContainer");
  // .parent()
  // .html();
  _html += headerHtml;

  let footerHTML = $("footer");
  // .parent()
  // .html();
  _html += footerHTML;

  let processJS = $("#process_env_js").html();
  _html += "<script> var googletag = googletag || {};" + processJS + "</script>";

  let deviceJS = $("#device_related_js").html();
  _html += "<script>" + deviceJS + "</script>";

  let mainJs = $("#main_js_container").html();
  _html += "<script>" + mainJs + "</script>";
}
