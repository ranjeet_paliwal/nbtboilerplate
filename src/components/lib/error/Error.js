import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./Error.css";

export class Error extends Component {
  handleClick() {
    document.getElementById("modal3").style.display = "none";
  }
  render() {
    let cssStyle = {
      zIndex: 1003,
      display: "block",
      opacity: 1,
      bottom: "0px"
    };
    return (
      <div id="c_wdt_articleshow_1" className="article-section">
        <div className="error-wrapper">
          <span className="heading-text">{this.props.children}</span>
          <div className="sub-heading">
            It may have expired, or there could be a typo. Maybe you can find
            what you need on our homepage.
          </div>
          <Link className="return-btn" to="/">
            RETURN
          </Link>
        </div>
      </div>
    );
  }
}

Error.propTypes = {
  children: PropTypes.node
};

Error.defaultProps = {
  children: "Error"
};

export default Error;
