// converts DOM object to React Elements
import domToReact from "html-react-parser/lib/dom-to-react";

import React from "react";
import Loadable from "react-loadable";
import AdCard from "./../../common/AdCard";
import SocialShare from "./../../common/SocialShare";
import ListHorizontalCard from "./../../common/ListHorizontalCard";
import ImageCard from "./../../common/ImageCard/ImageCard";
import AnchorLink from "../../common/AnchorLink";
// import ElectionASWidget from "./../../../campaign/election/common/component/ElectionASWidget";

import styles from "./../../common/css/ArticleShow.scss";
import WpBroadCast from "../../common/WpBroadCast";
import { AnalyticsGA } from "../analytics";
import { createConfig } from "../../../modules/videoplayer/utils";

import { _getStaticConfig, _isCSR, isMobilePlatform, LoadingComponent } from "./../../../utils/util";
import { getImageParameters } from "../../common/ImageCard/ImageCardUtils";
import ScoreCard from "../../../modules/scorecard/ScoreCard";
import { SeoSchema } from "../../common/PageMeta";
import globalconfig from "../../../globalconfig";
import { isFeatureURL } from "../../lib/ads/lib/utils";
import ErrorBoundary from "../errorboundery/ErrorBoundary";
import ExitpollASWidget from "../../../campaign/election/components/ExitpollASWidget";
import ResultsASWidget from "../../../campaign/election/components/ResultsASWidget";

const siteConfig = _getStaticConfig();

const VideoPlayer = Loadable({
  loader: () => import("../../../modules/videoplayer/player"),
  LoadingComponent,
});
// const ExitpollASWidget = Loadable({
//   loader: () => import("../../../campaign/election/components/ExitpollASWidget"),
//   LoadingComponent,
// });
// const ResultsASWidget = Loadable({
//   loader: () => import("../../../campaign/election/components/ResultsASWidget"),
//   LoadingComponent,
// });
const ASLeadImage = Loadable({
  loader: () => import("../../common/ASLeadImage"),
  LoadingComponent,
});
const ApnaBazaar = Loadable({
  loader: () => import("../../common/ApnaBazaar"),
  LoadingComponent,
});
const TwitterCard = Loadable({
  loader: () => import("../../common/TwitterCard"),
  LoadingComponent,
});
const PollCard = Loadable({
  loader: () => import("../../common/PollCard"),
  LoadingComponent,
});
const HighlightCard = Loadable({
  loader: () => import("../../common/HighlightCard"),
  LoadingComponent,
});
const InlinePhotoGallery = Loadable({
  loader: () => import("../../common/InlinePhotoGallery"),
  LoadingComponent,
});
const TableCard = Loadable({
  loader: () => import("../../common/TableCard"),
  LoadingComponent,
});
const TableCardUpdated = Loadable({
  loader: () => import("../../common/TableCardUpdated"),
  LoadingComponent,
});

const slikeApiKey = siteConfig.slike.apikey;

let amazonId = 1;

const AdvertorialNews = ({ data, id }) => {
  // data = data.splice(0, 1);
  if (data && Array.isArray(data) && data.length > 0) {
    if (_isCSR() && !window.IdsAdv) {
      window.IdsAdv = [];
    }
    if (data && id) {
      if (id) {
        window.IdsAdv.push(id);
      }

      return (
      <ErrorBoundary>
        <div className="news-advertorial top-article bulletContent">
          <ul>
            {data.map(item => {
              return (
                <li className={item && item.class ? item.class : ""}>
                  <AnchorLink href={item._link}>{item._text}</AnchorLink>
                </li>
              );
            })}
          </ul>
        </div>
      </ErrorBoundary>
      );
    }
    return "";
  }
  return "";
};

const createSlikeConfigLB = (item, slikeid) => {
  let adCode = "others";
  return {
    player: {
      section: adCode,
    },
    video: {
      id: slikeid || "",
      title: item.title || "",
      msid: item.msid || "",
      shareUrl: item.wu + "?" + siteConfig.slikeshareutm || "",
      seopath: item.seolocation ? item.seolocation : item.title,
      template: "liveblog",
      image: item.videoImgUrl || "",
    },
    sdk: {
      apikey: slikeApiKey,
    },
  };
};

const createVideoUrl = (item, msid, type, domNode) => {
  let data = {};
  if (domNode) {
    return {
      slikeid: domNode.attribs.src,
      hl: domNode.children[0].data,
      id: msid,
      // env: "stg",
      // version: "3.5.5",
      controls: {
        ui: "podcast",
      },
      seolocation: domNode.attribs.controls,
    };
  } else if (item.vdo) {
    for (let i = 0; i < item.vdo.length; i++) {
      if (item.vdo[i].id === msid) {
        data = item.vdo[i];
        break;
      }
    }
  }
  //handling for liveblog
  else if (item.msid) {
    data.override =
      item.hoverridLink && item.hoverridLink != "" && item.hoverridLink != "null"
        ? item.hoverridLink
        : `/-/videoshow/${item.msid}.cms`;
  }
  if (type == "data") return data;
  return generateUrl(data);
};

const generateUrl = item => {
  if (item.override) {
    return item.override;
  } else {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return url;
  }
};

const onVideoClickHandler = event => {
  let _this = this;
  // event.currentTarget.previousSibling.innerText
  let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
  if (videourl) {
    try {
      //To maintain previous url
      window.history.pushState({}, "", videourl);
      AnalyticsGA.pageview(location.origin + videourl);
    } catch (ex) {
      console.log("Exception in history: ", ex);
    }
  }
};

const parserOptions = {
  replace: (item, query, parentObj, options = {}, domNode) => {
    // do not replace if element has no attributes
    // console.log("options", options);
    const firstAdvertorial = options.firstAdvertorial;
    const restAdvertorial = options.restAdvertorial;
    const isFeaturedArticle =
      (item && item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff") ||
      isFeatureURL(item.wu);

    //const firstAdvertorial = advertorial.splice(0, 1);
    // console.log("firstAdvertorial", firstAdvertorial, restAdvertorial);
    // if (domNode && domNode.type === "text") {
    //   return <p className="inlinetext">{domNode.data}</p>;
    // } else
    if (!domNode || (domNode && !domNode.attribs)) return null;
    if (domNode.name == "video") {
      if (
        ((domNode.attribs.type == "inhouse" && typeof item.vdo != "undefined") || domNode.attribs.type == "audio") &&
        domNode.attribs.id != ""
      ) {
        let pagetype =
          item.pwa_meta && item.pwa_meta.pagetype && item.pwa_meta.pagetype != ""
            ? item.pwa_meta.pagetype
            : "articleshow";
        const vidData = createVideoUrl(
          item,
          domNode.attribs.id,
          "data",
          domNode.attribs.type === "audio" ? domNode : null,
        );
        let config = createConfig(vidData, item.pwa_meta.parentid, siteConfig, pagetype, item);
        let videoDetailsData = (domNode.children && domNode.children[0] && domNode.children[0].data) || "";
        const videoSchemaImgUrl = vidData && vidData != "" && getImageParameters({ src: "", msid: vidData.id }).ampsrc;
        //removed '//' from embedUrl
        const embedUrl = process.env.WEBSITE_URL.split(".com")[0] + ".com" + vidData.override;

        return (
          <div
            className="videoContainer"
            data-videourl={createVideoUrl(item, domNode.attribs.id)}
            // data-attr-slk={item.vdo[0] && item.vdo[0].id ? item.vdo[0].id : null}
          >
            <div
              {...SeoSchema({
                pagetype: "videoshow",
              }).videoattr().videoObject}
            >
              {SeoSchema({
                pagetype: "videoshow",
              }).metaPosition(0)}
              {SeoSchema({ pagetype: "videoshow" }).mainEntry(vidData.override)}
              {SeoSchema({
                pagetype: "videoshow",
              }).uploadDate(vidData.dlm)}
              {SeoSchema({ pagetype: "videoshow" }).headline(vidData.hl)}
              {SeoSchema({
                pagetype: "videoshow",
              }).dateStatus(vidData.dlm, vidData.dlm)}
              {/* {SeoSchema({ pagetype: source }).keywords("dddd")} //keyword */}
              {SeoSchema({ pagetype: "videoshow" }).name(vidData.hl)}
              {/* there is no such property under videoshow schema */}
              {SeoSchema({
                pagetype: "videoshow",
              }).embedUrl(embedUrl)}
              {SeoSchema().publisherObj()}
              {SeoSchema().metaTag({ name: "author", content: "websitename" })}
              {SeoSchema({ pagetype: "videoshow" }).thumbnail(videoSchemaImgUrl)}
              {SeoSchema({ pagetype: "videoshow" }).duration(vidData.vdu)}
              {vidData.pu && vidData.pu != "" ? SeoSchema({ pagetype: "videoshow" }).contentUrl(vidData.pu) : null}
              {SeoSchema({ pagetype: "videoshow" }).artdesc(vidData.cap)}
              <div>
                <VideoPlayer
                  autoDock
                  key={config.id}
                  inline={true}
                  data-videourl={createVideoUrl(item, domNode.attribs.id)}
                  onVideoClick={onVideoClickHandler.bind(this)}
                  apikey={slikeApiKey}
                  wrapper="masterVideoPlayer"
                  config={config}
                  imageid={domNode.attribs.id}
                  imgsize={vidData ? vidData.imgsize : ""}
                />
              </div>
              {item.vdo &&
                Array.isArray(item.vdo) &&
                item.vdo
                  .filter(items => items.id === domNode.attribs.id)
                  .map(data => <div className="vodtitle">{data && data.cap}</div>)}
              {/* {item.vdo &&
              Array.isArray(item.vdo) &&
              item.vdo
                .filter(items => items.id === domNode.attribs.id)
                .map(data => <div className="vodDec">{data && data.Story}</div>)} */}
              {/* Caption data should come in vdo node only, so commenting below */}
              {/* {videoDetailsData ? videoDetailsData : null} */}
            </div>
          </div>
        );
      } else if (domNode.attribs.type == "youtube") {
        if (isMobilePlatform()) {
          return <iframe data-attr="youtube-embed" data-src={`https://www.youtube.com/embed/${domNode.attribs.id}`} />;
        }

        return (
          <div className="youtube-frame">
            <iframe src={`https://www.youtube.com/embed/${domNode.attribs.id}`} width="600" height="450" />
          </div>
        );
      } else {
        return <span data-exclude="amp" type="invalid-video" />;
      }
    } else if (domNode.name == "amazonwidget") {
      // console.log("domNode.name--", domNode.name, domNode.children[0].data);

      let amzData, category, phrase;
      amzData = domNode.children && domNode.children[0] && domNode.children[0].data;

      if (amzData.indexOf("_") > -1) {
        category = amzData.split("_")[0];
        phrase = amzData.split("_")[1];
      } else {
        category = domNode.children && domNode.children[0] && domNode.children[0].data;
        phrase = item && item.pwa_meta && item.pwa_meta.keyhundred ? item.pwa_meta.keyhundred : null;
      }

      return category && phrase ? (
        _isCSR() ? (
          <iframe
            src={`/amazon_nsa.cms?category=${category}&phrase=${phrase}`}
            id={"amzNSA" + amazonId++}
            height="540"
          />
        ) : null
      ) : // _isCSR() ?
      // 	<div suppressHydrationWarning dangerouslySetInnerHTML={{
      // 		__html: ''
      // 	}} />
      // 	:
      // 	<div className="amazonNSA">
      // 		<script dangerouslySetInnerHTML={{
      // 			__html:
      // 				` 	amzn_assoc_placement = "adunit0";
      // 			amzn_assoc_search_bar = "false";
      // 			amzn_assoc_tracking_id = "nbt0b-21";
      // 			amzn_assoc_search_bar_position = "bottom";
      // 			amzn_assoc_ad_mode = "search";
      // 			amzn_assoc_ad_type = "smart";
      // 			amzn_assoc_marketplace = "amazon";
      // 			amzn_assoc_region = "IN";
      // 			amzn_assoc_title = "Shop Related Products";
      // 			amzn_assoc_default_search_phrase = "${phrase}";
      // 			amzn_assoc_default_category = "${category}";
      // 			amzn_assoc_linkid = "a30ec4c8ea3d4e43b502161e67b0c9ea";
      // 		`
      // 		}} />
      // 		<script src="//z-eu.amazon-adsystem.com/widgets/onejs?MarketPlace=IN"></script>
      // 	</div>
      null;
    } else if (domNode.name == "iframe" && domNode.attribs.class == "amzproduct") {
      let frameSrc = domNode.attribs.src;
      frameSrc += frameSrc.includes("?") ? `&msid=${item.id}` : `?msid=${item.id}`;
      return (
        <iframe
          src={frameSrc}
          className={domNode.attribs.class}
          width="100%"
          scrolling="no"
          title={domNode.attribs.title}
          allowtransparency="true"
        />
      );
    } else if (domNode.name == "facebook" && domNode.children.length > 1 && domNode.children[1].name == "iframe") {
      let src = domNode.children[1].attribs.src.split("href=")[1];
      return <div className={styles["fb-post"] + " fb-post"} data-href={src} />;
    } else if (domNode.name == "twitter" && domNode.text != "") {
      // return TwitterCard({ twitterId: domNode.attribs.id });
      return <TwitterCard twitterId={domNode.attribs.id} />;
    } else if (
      domNode.name == "script" &&
      domNode.attribs.type === "amz_json" &&
      Array.isArray(domNode.children) &&
      domNode.children.length > 0
    ) {
      return <ApnaBazaar item={domNode.children[0].data} />;
    }

    // as quote is not reusable creating html here only
    else if (domNode.name == "quote") {
      return (
        <div className="quote">
          <span className="start-quote">
            <b />
          </span>
          <div className="quote_txt">{domNode.attribs.text}</div>
          <div className="quote_author">{domNode.attribs.author}</div>
          <span className="end-quote">
            <b />
          </span>
        </div>
      );
    }
    // this check is added to replace <small id ="lt"> with less than operator
    else if (domNode.attribs.id === "lt") {
      return <React.Fragment>{`<`}</React.Fragment>;
    }
    // this check is added to replace <small id ="gt"> with greater than operator
    else if (domNode.attribs.id === "gt") {
      return <React.Fragment>{`>`}</React.Fragment>;
    }

    // HighlightCard is getting used for both tophighlight and embed bulletContent
    else if (domNode.name == "highlights" && domNode.attribs.data != "") {
      return <HighlightCard data={domNode.attribs.data} type={"bulletContent"} />;
    } else if (domNode.name == "ul" && domNode.children != "") {
      return <ul className="top-article bulletContent">{domToReact(domNode.children)}</ul>;
    } else if (domNode.name == "slideshow") {
      let params = { msid: domNode.attribs.id };
      return <InlinePhotoGallery params={params} />;
    } else if (domNode.type == "tag" && domNode.name == "table") {
      if (domNode.attribs && domNode.attribs.data && domNode.attribs.data.includes("-|$|-")) {
        return <TableCard data={domNode.attribs.data} type={domNode.attribs.type} />;
      } else {
        domNode.attribs.style = "";
        return (
          <React.Fragment>
            <ErrorBoundary>
              <TableCardUpdated data={domNode.children} />
            </ErrorBoundary>
            {/* <table>{domToReact(domNode.children)}</table> */}
          </React.Fragment>
        );

        // domNode.attribs.className = "tableContainer";
      }
    } else if (domNode.type == "tag" && domNode.name && ["tbody", "th", "tr", "td"].includes(domNode.name)) {
      domNode.attribs.style = "";
    }

    //iterate embedpolls and find with particular pollid , first check embedpolls not empty
    else if (domNode.name == "poll" && Array.isArray(item.embedpolls) && item.embedpolls.length > 0) {
      let polldata = {};
      for (let i = 0; i < item.embedpolls.length; i++) {
        if (item.embedpolls[i].pollid === domNode.attribs.id) {
          polldata = item.embedpolls[i];
          break;
        }
      }
      return <PollCard data={polldata} />;
    }
    //if nic tag , check anchor present as child
    else if (domNode.name == "nic" && Array.isArray(domNode.children) && domNode.children.length > 0) {
      domNode = domNode.children[0];
      return (
        <AnchorLink key={domNode.attribs.href} className="nic" href={domNode.attribs.href}>
          {domToReact(domNode.children)}
        </AnchorLink>
        // <NICCard msid = {domNode.attribs.id.split('_')[1]} text = {domNode.children[0].data}/>
      );
    }

    //iterate embedarticle tag if present and format data as per horizontal listing
    else if (
      domNode.name == "relatedarticle" &&
      Array.isArray(item.embedarticle) &&
      item.embedarticle &&
      item.embedarticle.length > 0
    ) {
      let _item = {
        hl: siteConfig.locale.relatedarticle,
        items: [],
        _class: "relarticle", // passing class name
      };
      let relatedDataStr = domNode.children[0].data;
      for (let i = 0; i < item.embedarticle.length; i++) {
        let data = item.embedarticle[i];
        if (relatedDataStr.indexOf(data.msid) > -1) {
          _item.items.push({
            id: data.msid,
            hl: data.arttitle,
            seolocation: data.seolocation,
            tn: "news",
          });
        }
      }
      return (
        <ul>
          <ListHorizontalCard item={_item} tileWidth={siteConfig.locale.related_articles_horizontal_tile_width} />
        </ul>
      );
    }

    // replace i tag with br , hack for break line
    else if (domNode.name == "i") {
      return <br />;
    }

    //replace em for brandwire , TODO need a special tag for these
    // else if (domNode.name == "em") {
    //   return <div className="brandwire_disclaimer">{domToReact(domNode.children)}</div>;
    // }
    else if (
      domNode.name == "img" &&
      domNode.attribs &&
      domNode.attribs.src &&
      domNode.attribs.src.split("_")[0] == "photo"
    ) {
      let msid = domNode.attribs.src.split("_")[1];

      return (
        <div className="img_wrap">
          <ImageCard
            size="bigimage"
            msid={msid}
            schema
            alt={domNode.attribs.title}
            title={domNode.attribs.title}
            pagetype={item && item.pwa_meta && item.pwa_meta.pagetype}
            adgdisplay={isFeaturedArticle ? "false" : null}
          />
          {domNode.attribs.cap && (
            <div className="img_caption" dangerouslySetInnerHTML={{ __html: domNode.attribs.cap }}></div>
          )}
        </div>
      );
    }

    //for ads check type and create a div with class 'ad1'
    else if (domNode.name == "ad" && domNode.attribs.type != "") {
      if (
        domNode.attribs.type == "special" &&
        domNode.attribs.id != "" &&
        !(item && item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff")
      ) {
        let ad_data = domNode.attribs.id.split(":");
        ad_data = {
          name: ad_data[0],
          mlb: ad_data[1],
          id: ad_data[2],
          size: ad_data[3],
        };
        let bannerData = [];
        if (_isCSR() && window.bannerData) {
          //For Csr banner data is set in window
          bannerData = JSON.parse(decodeURIComponent(window.bannerData));
        } else if (globalconfig.bannerData) {
          //For SSR/AMP banner data is set in globalconfig
          bannerData = globalconfig.bannerData;
        }
        return typeof domNode.attribs.platform == "string" &&
          domNode.attribs.platform != process.env.PLATFORM ? null : (
          <React.Fragment>
            {/*
              <div
              data-adtype={domNode.attribs.type}
              className={`ad1 emptyAdBox ${domNode.attribs.type}`}
              data-name={ad_data.name}
              data-id={ad_data.id}
              //data-mlb={ad_data.mlb}
              data-size={ad_data.size}
              />
            */}
            {isMobilePlatform() && bannerData && bannerData._image && (
              <a className="ads-center" href={bannerData._override} target="_blank" rel="nofollow">
                <img src={bannerData._image} />
              </a>
            )}
            {!isFeaturedArticle && restAdvertorial && restAdvertorial.length > 0 && (
              <AdvertorialNews data={restAdvertorial} id={item && item.id} />
            )}
          </React.Fragment>
        );
      } else if (
        domNode.attribs.type == "campaign" &&
        options &&
        options.campaign &&
        Object.keys(options.campaign).length > 0
      ) {
        let campaignData =
          options &&
          options.campaign &&
          options.campaign[process.env.PLATFORM] &&
          options.campaign[process.env.PLATFORM][process.env.SITE];
        return typeof domNode.attribs.platform == "string" &&
          domNode.attribs.platform != process.env.PLATFORM ? null : (
          <div
            data-adtype={domNode.attribs.type}
            className={`ad1 emptyAdBox ${domNode.attribs.type}`}
            data-name={campaignData && campaignData.name}
            data-id={campaignData && campaignData.id}
            data-size={campaignData && campaignData.size}
          />
        );
      } else if (domNode.attribs.type == "ctn") {
        return typeof domNode.attribs.platform == "string" &&
          domNode.attribs.platform != process.env.PLATFORM ? null : (
          <AdCard ctnstyle="ctninlineshow" mstype={domNode.attribs.id} adtype="ctn" pagetype="articleshow" />
        );
      } else if (domNode.attribs.type == "bccl") {
        if (isMobilePlatform()) {
          return (
            <div className="bccl-ad-wrapper" data-exclude="amp">
              <AdCard className="emptyAdBox" mstype="ctnbccl" adtype="ctn" />
            </div>
          );
        }
      }
      //  else if (domNode.attribs.type == "esi") {
      //   return (
      //     <ErrorBoundary>
      //       <AdCard
      //         ctnstyle="inline"
      //         esiad={true}
      //         content={item}
      //         mstype="midart"
      //         adtype="dfp"
      //         pagetype="articleshow"
      //         query={query}
      //         classtype="emptyAdBox"
      //       />
      //     </ErrorBoundary>
      //   );
      // }
      else {
        // ads & advertorials are off for isFeaturedArticle
        // let leadImageAdded = false;
        let pagetype = item && item.pwa_meta && item.pwa_meta.pagetype;
        //handle parallax
        if (domNode.attribs.type == "dfp" && domNode.attribs.id == "mrec1") {
          if (isMobilePlatform()) {
            return (
              <React.Fragment>
                {pagetype && pagetype.indexOf("topic") > -1 ? null : (
                  <div className="top-img-ad">
                    <ASLeadImage item={item} />
                  </div>
                )}
                {isFeaturedArticle ? null : (
                  <div className="ad1 parallaxDiv">
                    <div className="adinner" data-exclude="amp">
                      <div className="adinner-fxbox">
                        {/*<div className="ad1 mrec prerender" id="div-gpt-ad-7625234525418682-mrec" data-size="[[300, 250], [300, 600]]" 
            data-path={'/7176/NBT_MWeb/Test/NBT_mweb_Test_Parallax'}/>*/}
                        {<AdCard mstype={domNode.attribs.id} adtype="dfp" />}
                      </div>
                    </div>
                  </div>
                )}
                {pagetype && pagetype.indexOf("topic") > -1 ? null : (
                  <React.Fragment>
                    {!isFeaturedArticle && firstAdvertorial && firstAdvertorial.length > 0 && (
                      <AdvertorialNews data={firstAdvertorial} id={item && item.id} />
                    )}
                  </React.Fragment>
                )}
              </React.Fragment>
            );
          }

          if (!isMobilePlatform() && !isFeaturedArticle && firstAdvertorial && firstAdvertorial.length > 0) {
            return pagetype && pagetype.indexOf("topic") > -1 ? null : (
              <React.Fragment>
                <AdvertorialNews data={firstAdvertorial} id={item && item.id} />
              </React.Fragment>
            );
          }
        } else {
          return null;
          // return(<AdCard mstype = {domNode.attribs.type} adtype="dfp" pagetype="articleshow"/>)
        }
      }
    } else if (domNode.name == "a") {
      if (
        domNode.attribs.id &&
        domNode.attribs.href != "" &&
        domNode.attribs.id.split("_")[0] == "tilCustomLink" &&
        domNode.attribs.id.split("_")[1] == "amzbtn"
      ) {
        //console.log(domToReact(domNode.children));
        return (
          <span className={"amz_btn"}>
            <AnchorLink href={domNode.attribs.href}> GET THIS </AnchorLink>
          </span>
        );
      } else if (
        domNode.attribs.href != "" &&
        domToReact(domNode.children) &&
        typeof domToReact(domNode.children) == "string" &&
        domToReact(domNode.children).trim() === "Buy Now On Amazon"
      ) {
        return (
          <span className={"amz_btn"}>
            <AnchorLink href={domNode.attribs.href}> GET THIS </AnchorLink>
          </span>
        );
      } else if (domNode.attribs.id && domNode.attribs.id.split("_")[0] == "inlineshare") {
        let inlineShareObj = {
          title: domToReact(domNode.children),
          url:
            item && item.pwa_meta && item.pwa_meta.canonical != ""
              ? item.pwa_meta.canonical
              : typeof window != "undefined"
              ? location.href
              : null,
        };
        return (
          <span className="inlineshare">
            <span className="inline-txt">{inlineShareObj.title}</span>
            <SocialShare
              sharedata={() => {
                return inlineShareObj;
              }}
              // To ensure that value is true for desktop, false for mobile. ( for styling share icon)
              isInlineShare={!isMobilePlatform()}
            />
          </span>
        );
      } //article interlinking
      else if (domNode.attribs && domNode.attribs.id && domNode.attribs.id == "embedarticle") {
        let utms = isMobilePlatform()
          ? "?utm_source=related_article_amp&utm_medium=referral&utm_campaign=article"
          : "?utm_source=related_article&utm_medium=referral&utm_campaign=article";
        let intUrl =
          domNode.attribs.href && domNode.attribs.href.includes("?")
            ? domNode.attribs.href
            : domNode.attribs.href + utms;
        return item.validtopic ? (
          <span></span>
        ) : (
          <span className="embedarticle">
            <AnchorLink href={intUrl}>{domToReact(domNode.children)}</AnchorLink>
          </span>
        );
      } else if (
        /*else if(domNode.attribs.href && domNode.attribs.href.indexOf('photomazaashow') > 0){
      let params = {msid:domNode.attribs.href.split('/photomazaashow/')[1].split('.cms')[0]}
      return(<InlinePhotoGallery params={params}/>);
    }*/
        domNode.attribs.id &&
        domNode.attribs.href &&
        domNode.attribs.id.split("_")[0] == "topic" &&
        domNode.attribs.href != ""
      ) {
        return domNode.attribs.href.toLocaleLowerCase().indexOf("/topics/elections") > -1 ? (
          <div className="elections-widget">
            {item.pwa_meta.electionwidgettype === "result" ? (
              <ErrorBoundary>
                <ResultsASWidget
                  location={item.pwa_meta.location}
                  constituencyid={item.pwa_meta.constituencyid}
                  electionyear={item.pwa_meta.electionyear}
                />
              </ErrorBoundary>
            ) : item.pwa_meta.electionwidgettype === "exitpoll" ? (
              <ErrorBoundary>
                <ExitpollASWidget location={item.pwa_meta.location} />
              </ErrorBoundary>
            ) : null}
          </div>
        ) : (
          <AnchorLink
            key={domNode.attribs.href}
            className="topic"
            target="_blank"
            hrefData={{ override: domNode.attribs.href }}
            onMouseLeave={event => {
              event.target.classList.remove("selectedTopic");
              if (
                document.querySelector(".topicpopup") &&
                !document.querySelector(".topicpopup").contains(event.relatedTarget)
              ) {
                setTimeout(() => {
                  parentObj.setState({ topicVal: "" });
                }, 1500);
              }
            }}
            onMouseOver={event => {
              event.preventDefault();
              event.target.classList.add("selectedTopic");
              parentObj.setState({ topicVal: domNode.children[0].data });
            }}
          >
            {domToReact(domNode.children)}
          </AnchorLink>
        );
      }
      //Only for EIS and handle leadImage
      // else if (process.env.SITE == 'eisamay' && domNode.attribs.id && domNode.attribs.id == 'Lead_Image') {
      // 	return <SummaryCard item={item} _onlyleadimage={true} pagetype={'articleshow'} />
      // }
      //if href empty dont show as anchor
      else if (domNode.attribs.href == "") {
        return <span>{domToReact(domNode.children)}</span>;
      } else if (
        domNode.attribs.href &&
        (domNode.attribs.id == "tilCustomLink" ||
          domNode.attribs.id == "reflink_" ||
          //  For custom links outside our domain, eg in job pages as well as valid links
          domNode.attribs.href.indexOf("https") > -1 ||
          domNode.attribs.href.indexOf("http") > -1)
      ) {
        return (
          <AnchorLink key={domNode.attribs.href} hrefData={{ override: domNode.attribs.href }}>
            {domToReact(domNode.children)}
          </AnchorLink>
        );
      } else if (domNode.attribs.id && domNode.attribs.id.split("_")[0] == "wappsubscribe") {
        return (
          <WpBroadCast
            pagetype={
              item.pwa_meta && item.pwa_meta.pagetype && item.pwa_meta.pagetype != "" ? item.pwa_meta.pagetype : null
            }
          />
        );
      } else if (domNode.attribs.href == "scorecard") {
        return <ScoreCard matchid={domNode.attribs.id ? domNode.attribs.id : ""} />;
      }
      // else if (domNode.attribs.id == "bennettiframe") {
      //   return (
      //     <iframe
      //       width="100%"
      //       height={isMobilePlatform() ? "835px" : "380px"}
      //       src={siteConfig.bennett}
      //       frameBorder="0"
      //     ></iframe>
      //   );
      // }
    }

    // else if(domNode.type == 'text' && domNode.data && (domNode.data.match(/\n/g)||[]).length > 1){
    // 	debugger;
    // 	this.config.newLineCount += (domNode.data.match(/\n/g)||[]).length ;
    // 	if(this.config.newLineCount == 2){
    // 		return (<span>jgfhfjjjjc</span>)
    // 	}else{
    // 		return (<br></br>)
    // 	}
    // }
  },
};

const parserOptionsLB = {
  replace: (item, domNode) => {
    // do not replace if element has no attributes
    if (!domNode || (domNode && !domNode.attribs)) return null;
    //Moved to liveblogcard
    // else if (domNode.name == "video") {
    //   if (domNode.attribs.rmid == "10") {
    //     return <iframe src={`https://www.youtube.com/embed/${domToReact(domNode.children)}`} />;
    //   }
    // } else if (domNode.name == "embed" && domNode.next.name == "src" && domNode.next.attribs.type == "kaltura") {
    //   //return (<iframe src={`https://m.maharashtratimes.com/slikeiframepage.cms?wapCode=nbt&apikey=nbtmwebtoi54&msid=${item.msid}`}></iframe>)
    //   let config = createSlikeConfigLB(item, domNode.next.attribs.id);
    //   return (
    //     <div data-videourl={createVideoUrl(item, domNode.attribs.id)}>
    //       <VideoPlayer
    //         autoDock
    //         data-videourl={createVideoUrl(item, domNode.attribs.id)}
    //         key={config.id}
    //         onVideoClick={onVideoClickHandler.bind(this)}
    //         apikey={slikeApiKey}
    //         wrapper="masterVideoPlayer"
    //         config={config}
    //         inline={true}
    //         imageid={item.msid}
    //       />
    //     </div>
    //   );
    // }
    //TODO , its not working for kaltura
    else if (domNode.name == "src") {
      return (
        // replace src tag due to amp validation error
        <script
          id={domNode.attribs && domNode.attribs.id ? domNode.attribs.id : ""}
          type={domNode.attribs && domNode.attribs.type ? domNode.attribs.type : ""}
        ></script>
      );
      // return null;
    } else if (item.type == "em" && domNode.name == "div") {
      if (domNode.attribs["data-type"] == "twitter") {
        // return TwitterCard({ twitterId: domNode.attribs["data-id"] });
        return <TwitterCard twitterId={domNode.attribs["data-id"]} />;
      } else if (
        domNode.attribs["data-type"] == "javascript" &&
        domNode.attribs["data-title"] == "Facebook" &&
        domNode.children[0] &&
        domNode.children[0].children[0]
      ) {
        return domNode.children[0].children[0].data ? (
          <div
            className="fb-post"
            dangerouslySetInnerHTML={{
              __html: domNode.children[0].children[0].data,
            }}
          />
        ) : (
          <div className="fb-post">{domToReact(domNode.children[0].children[0])}</div>
        );
      } else if (
        domNode.attribs["data-type"] == "javascript" &&
        domNode.attribs["data-title"] == "Instagram" &&
        domNode.children[0] &&
        domNode.children[0].children[0] &&
        domNode.children[0].children[0].data
      ) {
        return (
          <div
            className="instagram"
            dangerouslySetInnerHTML={{
              __html: domNode.children[0].children[0].data,
            }}
          />
        );
      }
    }
    //Moved to liveblogcard
    // as quote is not reusable creating html here only
    // else if (item.type == "t" && item.subType == "quotes") {
    //   let author = domNode.children[0].children[0].data;
    //   return (
    //     <div className="quote">
    //       <span className="start-quote">
    //         <b />
    //       </span>
    //       <div className="quote_txt">{item.title}</div>
    //       <div className="quote_author">{author}</div>
    //       <span className="end-quote">
    //         <b />
    //       </span>
    //     </div>
    //   );
    // }
    //Moved to liveblogcard
    //iterate embedpolls and find with particular pollid , first check embedpolls not empty
    // else if (item.type == "p" && Array.isArray(item.childs) && item.childs && item.childs.length > 0) {
    //   let polldata = { pollid: item.msid, polltext: item.title };
    //   polldata.polloption = [];
    //   item.childs.map(cItem => {
    //     polldata.polloption.push({
    //       optiontext: cItem.title,
    //       optionno: cItem.contentid,
    //     });
    //   });
    //   return PollCard({ data: polldata });
    // }
  },
};

export { parserOptions, parserOptionsLB };
