// import tpConfig from "../../../../components/common/TimesPoints/TimesPointConfig";
import { _getStaticConfig } from "../../../../utils/util";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;

const DEV_ENV = process.env.DEV_ENV;
const pathTPSDK = `${tpConfig.SDK_DOMAIN[DEV_ENV]}/static/tpsdk/tp-sdk.js`;

export default {
  __html: `(function(g, r, o, w, t, h, rx) {
        g[t] =
          g[t] ||
          function() {
            (g[t].q = g[t].q || []).push(arguments);
          };
        g[t].l = 1 * new Date();
        g[t] = g[t] || {};
        h = r.createElement(o);
        rx = r.getElementsByTagName(o)[0];
        h.async = 1;
        h.src = w;
        rx.parentNode.insertBefore(h, rx);
      })(
        window,
        document,
        'script',
         "${pathTPSDK}",
        'tpsdk',
      );`,
};
