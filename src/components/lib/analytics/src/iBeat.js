import load_iBeat from "./../lib/iBeat";
import { _formatDate } from "./../lib/utils";

const warn_label = "@times-analytics ";

function configure(options) {
  window._pg_startpt = new Date().getTime();
  window._iBeat_url = escape(window.location.href);
  window.groupId = "--groupId=";

  window._page_config = {
    host: options.ibeathost,
    key: options.ibeatkey,
    domain: options.ibeathost,
    channel: options.wapsitename,
    action: 1,
    location: 1,
    articleId: "",
    cat: "",
    subcat: "",
    contentType: getiBeatType(),
    contenttag: "",
    catIds: "",
    articledt: "",
    iBeatField: "isAPP=2" + groupId,
    author: "",
    pubt: "",
  };
}

function getiBeatType(pagetype) {
  switch (pagetype) {
    case "articleshow":
      return 1;
    case "videoshow":
      return 2;
    case "photoshow":
      return 3;
    case "liveblog":
      return 99;
    case "storylist":
      return 20;
    case "home":
      return 20;
    default:
      return 10;
  }
}

export const setIbeatConfigurations = pwaConfig => {
  let pwaObj = JSON.parse(JSON.stringify(pwaConfig));
  let ibeatPageCongig = getiBeatType(pwaObj.pagetype) || "";
  var pagetype = pwaObj.pagetype;
  if (typeof window !== "undefined") {
    const ibeatVal =
      pagetype == "articleshow"
        ? 1
        : pagetype == "videoshow"
          ? 2
          : pagetype == "photoshow"
            ? 3
            : "";

    window._ibeat_track = {
      ct: ibeatVal
    };
  }
  return false;
  window._page_config = window._page_config ? window._page_config : {};

  let obj = JSON.parse(JSON.stringify(pwaConfig));
  let platformIbeatId = "isAPP=" + (process.env.PLATFORM == "desktop" ? 0 : 2);

  if (obj != undefined && obj != "undefined") {
    if (obj.articledt && obj.articledt.indexOf("IST") == -1) {
      obj.articledt = _formatDate(obj.articledt, true);
    }
    if (obj.groupId && obj.groupId != "") {
      obj.iBeatField = platformIbeatId + "--groupId=" + obj.groupId;
    }
    let tempObj = {
      articleId: obj.articleId || "",
      cat: obj.cat || "Others",
      subcat: obj.subcat || "",
      contentType: obj.contentType || getiBeatType(obj.pagetype),
      contenttag: obj.contenttag || "",
      catIds: obj.catIds.replace(" ", "") || "",
      articledt: obj.articledt || "",
      iBeatField: obj.iBeatField || platformIbeatId,
      author: obj.author || "",
      pubt: obj.pubt || "",
      host: obj.ibeathost || _page_config.host,
      key: obj.ibeatkey || _page_config.key,
      domain: obj.ibeathost || _page_config.domain,
      channel: obj.wapsitename || _page_config.channel,
    };
    _page_config = Object.assign(_page_config, tempObj);
    //update _pg_endpt on every hit
    window._pg_endpt = new Date().getTime();
    if (typeof iBeatPgTrend != "undefined") {
      // iBeatPgTrend.init(_page_config);
      _page_config.articleId != "" ? iBeatPgTrend.init(_page_config) : null;
    } else {
      load_iBeat({}, () => {
        // iBeatPgTrend.init(_page_config)
        _page_config.articleId != "" ? iBeatPgTrend.init(_page_config) : null;
      });
    }
  }
};

/**
 * initialize:
 * intializing iBeat method with a unique gaId
 * @param {object} options #required ibeathost, ibeatkey, wapsitename,
 */
export const initialize = options => {
  if (typeof window === "undefined") {
    return false;
  }

  if (!options || (typeof options != "object" && Object.keys(options).length === 0)) {
    console.warn(`${warn_label} options is required | options arg to be an Object | options must not empty`);
    return false;
  }

  if (!options.ibeathost || !options.ibeatkey || !options.wapsitename) {
    console.warn(`${warn_label} options ibeathost, ibeatkey, wapsitename are required`);
    return false;
  }

  configure(options);

  // window.addEventListener('load', () => {
  if (options && !options.gdpr) {
    window._pg_endpt = new Date().getTime();
    load_iBeat(options);
  }
  // }, false);

  return true;
};

export default { initialize, setIbeatConfigurations };
