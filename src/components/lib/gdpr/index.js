import OnDemandLoader from "../../../modules/ondemandloader/index";
import { _getCookie, _getStaticConfig, _setCookie } from "../../../utils/util";
import { customEventPolyfill, isMobilePlatform } from "../../../utils/util";
import LoginControl from "../../common/LoginControl";
import { getSsoChannel } from "../../common/ssoLogin";
const siteConfig = _getStaticConfig();

const GDPR_Module = {
  config: {
    geoapiurl: "https://geoapi.indiatimes.com/?cb=1",
    geoDataCookieName: "geo_data",
    geoDataCookieAge: 1,

    channelCode: "nbt",
    wapsitename: "Navbharat Times",
    mweburl: "https://navbharattimes.indiatimes.com",

    consentCookieName: "ckns_policy",
    updatedConsentCookieName: "ckns_policyV2",

    consentCookieAge: 365,
    consentAcceptText: "I accept all cookie settings",
    consentCookieId: 5,

    optOutCookieName: "optout",
    optOutCookieDefaultVal: 1,
    optOutAcceptCookieVal: 1,
    optOutCookieAge: 365,

    ccpaCookieName: "_col_ccds",
    ccpaCookieDefaultVal: 1,
    ccpaAcceptCookieVal: 1,
    ccpaCookieAge: 365,

    columbiaCoookie: "_col_uuid",
    arrGDPRContinents: ["EU"],
  },

  setupConfig(options) {
    if (options.channelCode) GDPR_Module.config.channelCode = options.channelCode;
    if (options.wapsitename) GDPR_Module.config.wapsitename = options.wapsitename;
    if (options.mweburl) GDPR_Module.config.mweburl = options.mweburl;

    GDPR_Module.config.callback = options.callback;
    // GDPR_Module.config.consentApiurl = `https://etservices2.indiatimes.com/${GDPR_Module.config.channelCode}/consent`;
    GDPR_Module.config.consentApiurl = `https://myt.indiatimes.com/mytimes/profile/update`;
    GDPR_Module.config.disable_ga_property = `ga-disable-${GDPR_Module.config.channelCode}`;
    GDPR_Module.config.pushnotificationUnsubscribeUrl = options.pushnotificationUnsubscribeUrl;
  },

  interceptor(consentGiven) {
    let bool = false;
    // else set flag for is EU-country or not
    // window.geoinfo.notEU = window.geoinfo.Continent.indexOf(window.continent_europe) < 0;
    if (GDPR_Module.config.arrGDPRContinents.indexOf(window.geoinfo.Continent) > -1) {
      bool = true;
    } else if (window.geoinfo.CountryCode === "US" && window.geoinfo.region_code === "CA") {
      bool = true;
      window.isCCPARegion = true;
    }
    window.geoinfo.isGDPRRegion = bool;

    if (!window.isCCPARegion && _getCookie(GDPR_Module.config.optOutCookieName) == undefined) {
      window._setCookie(
        GDPR_Module.config.optOutCookieName,
        GDPR_Module.config.optOutAcceptCookieVal,
        GDPR_Module.config.optOutCookieAge,
        "/",
      );
    }

    if (window.geoinfo.isGDPRRegion) {
      // handle pixel/tracker for EU country is true
      // if (!window.geoinfo.notEU) {
      GDPR_Module.handleGdprRegion_functionalities();
    } else {
      GDPR_Module.handleNonGdprRegion_functionalities();
    }

    // if EU-country activate accepatance form
    const googleBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent);

    if (!consentGiven && window.geoinfo.isGDPRRegion && !googleBot) {
      if (window.isCCPARegion) {
        GDPR_Module.createCCPAconsentForm();
      } else {
        GDPR_Module.createGDPRconsentForm();
      }
      GDPR_Module.onClickAcceptBtn();
      // GDPR_Module.consentPopUp();
    }

    // set cookie for geoData
    if (window._getCookie(GDPR_Module.config.geoDataCookieName) == undefined) {
      window._setCookie(
        GDPR_Module.config.geoDataCookieName,
        JSON.stringify(window.geoinfo),
        GDPR_Module.config.geoDataCookieAge,
        "/",
      );
    }
  },

  showToastMessage() {
    if (
      !window._user &&
      window.geoinfo &&
      window.geoinfo.isGDPRRegion &&
      GDPR_Module.getConsentCookie() == "1" &&
      _getCookie("toastmessage") === undefined
    ) {
      GDPR_Module.handleToastMessage();
    }
  },

  handleToastMessage() {
    const message = `${siteConfig.wapsitename} has updated its privacy policy. We use cookies in order to improve your browsing experience on our Website. Kindly click on <a href="${siteConfig.mweburl}/privacypolicyeu.cms" >Privacy Notice</a> and <a href="${siteConfig.mweburl}/cookiepolicy.cms">Cookie Policy</a> to know more.`;
    const div = document.createElement("div");
    div.setAttribute("id", "toastmessage");
    div.setAttribute("class", "toastmessage");
    const okBtn = document.createElement("Button");
    okBtn.setAttribute("id", "okBtn");
    okBtn.setAttribute("class", "okBtn");
    document.body.appendChild(div);
    document.getElementById("toastmessage").innerHTML = message;
    document.getElementById("toastmessage").appendChild(okBtn);
    document.getElementById("okBtn").innerHTML = "OK";
    document.getElementById("okBtn").addEventListener("click", GDPR_Module.closeToastMessage);
  },

  closeToastMessage() {
    document.getElementById("toastmessage").classList.add("hide");
    _setCookie("toastmessage", "1", 365);
  },

  loadModules(consentGiven) {
    // call interceptor function which will track and handle EU/notEU conditions
    GDPR_Module.interceptor(consentGiven);
    customEventPolyfill();
    const event = new CustomEvent("gdpr.status", {
      detail: { isGDPRRegion: window.geoinfo.isGDPRRegion },
    });
    // Dispatch/Trigger/Fire the event
    setTimeout(() => document.dispatchEvent(event), 500);

    if (typeof GDPR_Module.config.callback === "function") GDPR_Module.config.callback();
  },

  loadGeoApi(consentGiven) {
    // check for geo data , load geoapi and set cookie , on callback initiate all modules
    if (window._getCookie(GDPR_Module.config.geoDataCookieName) == undefined) {
      const ondemandloaderObj = OnDemandLoader({
        geoapiurl: GDPR_Module.config.geoapiurl,
      });
      ondemandloaderObj.geoapiurl.onLoad(() => {
        GDPR_Module.loadModules(consentGiven); // load all modules
      });
      // if geodata cookie present , initiate modules as per GDPR
    } else {
      window.geoinfo = JSON.parse(window._getCookie(GDPR_Module.config.geoDataCookieName));

      GDPR_Module.loadModules(consentGiven);
    }
  },

  handleGdprRegion_functionalities() {
    // first remove all cookies if EU AND OPTOUT
    if (window._getCookie(GDPR_Module.config.optOutCookieName) == undefined) {
      GDPR_Module.removeAllCookies("/", ".indiatimes.com");

      // set optout cookie for EU country
      if (window._getCookie(GDPR_Module.config.optOutCookieName) !== undefined) {
        window._setCookie(
          GDPR_Module.config.optOutCookieName,
          GDPR_Module.config.optOutCookieDefaultVal,
          GDPR_Module.config.optOutCookieAge,
          "/",
        );
      }
      // Unregister Browser Notification Service Worker

      if (GDPR_Module.config.pushnotificationUnsubscribeUrl) {
        const ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", GDPR_Module.config.pushnotificationUnsubscribeUrl);
        ifrm.style.width = "1px";
        ifrm.style.height = "1px";
        ifrm.style.display = "None";
        document.body.appendChild(ifrm);
      }
    }

    if (window._getCookie(GDPR_Module.config.ccpaCookieName) == undefined) {
      GDPR_Module.removeAllCookies("/", ".indiatimes.com");
      // set optout cookie for EU country
      if (window._getCookie(GDPR_Module.config.ccpaCookieName) !== undefined && window.isCCPARegion) {
        window._setCookie(
          GDPR_Module.config.ccpaCookieName,
          GDPR_Module.config.ccpaCookieDefaultVal,
          GDPR_Module.config.ccpaCookieAge,
          "/",
        );
      }
    }

    // remove ckns_policy cookie
    if (window._getCookie(GDPR_Module.config.consentCookieName) != undefined) {
      window._setCookie(GDPR_Module.config.consentCookieName, null, -1, "/");
    }

    window[GDPR_Module.config.disable_ga_property] = true; // disable GA calls
    // ga('set','anonymizeIp',true); // disable user IP - handled in wdt_lib
    window.fireABC = function() {}; // disable ABC calls

    // create style tag which will hide all elements that not to be shown on EU countries
    GDPR_Module.createStyleToHide(true);

    // remove html wrapper
    GDPR_Module.removehtml();

    GDPR_Module.setPersonalizeAds();

    // let optOutCookieVal = window._getCookie(GDPR_Module.config.optOutCookieName);
    // if (optOutCookieVal != undefined && optOutCookieVal == 0) {
    //     let isNonPersonalizeAds = window.geoinfo.notEU ? 0 : 1;
    //     googletag.pubads().setRequestNonPersonalizedAds(isNonPersonalizeAds);
    // }
  },

  handleNonGdprRegion_functionalities() {
    // if not EU country , remove optout cookie if present
    if (window._getCookie(GDPR_Module.config.optOutCookieName) != undefined) {
      window._setCookie(GDPR_Module.config.optOutCookieName, null, -1, "/");
    }
  },

  removeAllCookies(path, domain) {
    const cookies = document.cookie ? document.cookie.split("; ") : [];
    for (let i = 0, l = cookies.length; i < l; i++) {
      const parts = cookies[i].split("=");
      const name = decodeURIComponent(parts.shift());
      domain = (domain || document.location.host).split(":")[0];
      path = path || document.location.pathname;
      window._setCookie(name, null, -1, path, domain);
    }
  },

  removehtml() {
    if (document.querySelector('[data-content = "fbshare"]'))
      document.querySelector('[data-content = "fbshare"]').remove(); // remove app download toast
  },

  /**
   * if bool true hide the GDPR components else show all components
   * When user login show ll gdpr components and logout hide all components
   * @param {*} bool
   */
  createStyleToHide(bool) {
    const styles = document.getElementById("hideGDPRComponents");
    if (styles && bool == false) {
      styles.parentNode.removeChild(styles);
    } else if (!styles && bool == true) {
      const styletag = document.createElement("style");
      styletag.setAttribute("id", "hideGDPRComponents");
      styletag.innerHTML =
        ".app_download , .btn_openinapp , .share_container , .share_icon " +
        ", .comments-n-socail , .comments-wrapper , .btn_rate , .wdt_social_share" +
        " , .comment_wrap , .con_fbapplinks, .con_review_rate, .service_drawer, .footer .sociallink, .inline_app_download" +
        " {display:none !important;} ";

      document.head.appendChild(styletag);
    }
  },

  createRandomUniqueKey() {
    return (
      GDPR_Module.config.channelCode +
      "-wap-xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, c => {
        const r = (Math.random() * 16) | 0;

        const v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      })
    );
  },

  closeCCPAMessage() {
    document.querySelector('[data-attr="top-header-gdpr"]').style.display = "none";
    window._setCookie(GDPR_Module.config.updatedConsentCookieName, 1, GDPR_Module.config.consentCookieAge, "/");
  },

  createCCPAconsentForm() {
    // let CCPA = `We use cookies and other tracking technologies to provide services in line with the preferences you reveal while browsing the Website to show personalize content and targeted ads, analyze site traffic, and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to browse this Website, you consent to the use of these cookies. If you wish to object such processing, please read the instructions described in our Cookie Policy/Privacy Policy`;
    // let EU = `The ${GDPR_Module.config.wapsitename} <a href="${GDPR_Module.config.mweburl}/privacypolicyeu.cms" >privacy policy</a> and <a href="${GDPR_Module.config.mweburl}/cookiepolicy.cms">cookie policy</a>has been updated to align with the new data regulations in European Union, Please review and accept these changes below to continue using the website. You can see our privacy policy & our cookie list below. We use cookies to ensure the best experience for you on our website. If you choose to ignore this message, we”ll assume that you are happy to receive all cookies on ${GDPR_Module.config.wapsitename}.`;
    const textMess = `We use cookies and other tracking technologies to provide services while browsing the Website to show personalise content and targeted ads, analyse site traffic and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to use our Website, you consent to the use of these cookies and accept our Privacy terms. If you wish to see more information about how we process your personal data, please read our <a href="${GDPR_Module.config.mweburl}/privacypolicyeu.cms" >privacy policy</a> and <a href="${GDPR_Module.config.mweburl}/cookiepolicy.cms">cookie policy</a>`;
    const _gdprhtml = `<style type="text/css">
            /*GDPR Top block css*/
            .top-header-gdpr{background:#343434; overflow:hidden; color:#fff;position:relative; font-size:14px; line-height:22px; padding:20px; font-family:arial}
            .top-header-gdpr h2{font-size:18px; font-weight:bold; margin:0 0 10px; padding:0;}
            .top-header-gdpr .btm-button{text-align:left; margin:20px 0 0}
            .top-header-gdpr .btn{border-radius:20px; text-transform:uppercase; font-size:14px; line-height:24px; display:inline-block; padding:6px 0;  border:1px solid #F4B408; cursor:pointer; font-family:arial; text-align:center; color:#F4B408; background:transparent; width:30%; vertical-align:top}
            .top-header-gdpr a{text-decoration:none; color:#F4B408}
            .agreebox{display:inline-flex; vertical-align:top; width:65%; margin:0 10px 0 0; color:#ccc; font-size:14px; line-height:20px; text-align:left}
            .agreebox input{margin:4px 5px 0 0}
            .top-header-gdpr.desktop{width:1000px; box-sizing:border-box; margin:auto; font-size:15px; line-height:25px;}
            .top-header-gdpr.desktop .agreebox{margin-top:10px}
            .top-header-gdpr .close-btn{ background: none; border: 0; font-size: 30px; font-weight: normal;line-height: 1; color: #fff;transform: rotate(-45deg);position: absolute;top:0px;right:0;z-index: 1;font-family: initial;outline: none;cursor: pointer;}
        </style>
        <span>${textMess}</span>
    
        <button type="button" class="close-btn" id="ccpaCloseBtn">+</button>
    `;

    const divtag = document.createElement("div");
    divtag.setAttribute("class", "top-header-gdpr");
    divtag.setAttribute("data-attr", "top-header-gdpr");
    // divtag.setAttribute("style","display:none");
    divtag.innerHTML = _gdprhtml;

    document.body.firstElementChild.before(divtag);
    if (!isMobilePlatform()) {
      document.querySelector('[data-attr="top-header-gdpr"]').classList.add("desktop");
    }
    document.getElementById("ccpaCloseBtn").addEventListener("click", GDPR_Module.closeCCPAMessage);
    // document.getElementById('consentAcceptButton1').addEventListener('click', GDPR_Module.agreeloginpopUp1);
  },

  createGDPRconsentForm() {
    // let CCPA = `We use cookies and other tracking technologies to provide services in line with the preferences you reveal while browsing the Website to show personalize content and targeted ads, analyze site traffic, and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to browse this Website, you consent to the use of these cookies. If you wish to object such processing, please read the instructions described in our Cookie Policy/Privacy Policy`;
    // let EU = `The ${GDPR_Module.config.wapsitename} <a href="${GDPR_Module.config.mweburl}/privacypolicyeu.cms" >privacy policy</a> and <a href="${GDPR_Module.config.mweburl}/cookiepolicy.cms">cookie policy</a>has been updated to align with the new data regulations in European Union, Please review and accept these changes below to continue using the website. You can see our privacy policy & our cookie list below. We use cookies to ensure the best experience for you on our website. If you choose to ignore this message, we”ll assume that you are happy to receive all cookies on ${GDPR_Module.config.wapsitename}.`;
    const textMess = `We use cookies and other tracking technologies to provide services in line with the preferences you reveal while browsing the Website to show personalize content and targeted ads, analyze site traffic, and understand where our audience is coming from in order to improve your browsing experience on our Website. By continuing to browse this Website, you consent to the use of these cookies. If you wish to object such processing, please read the instructions described in our <a href="${GDPR_Module.config.mweburl}/privacypolicyeu.cms" >privacy policy</a>/<a href="${GDPR_Module.config.mweburl}/cookiepolicy.cms">cookie policy</a>.`;
    const _gdprhtml = `<style type="text/css">
            /*GDPR Top block css*/
            .top-header-gdpr{background:#343434; overflow:hidden; color:#fff; font-size:14px; line-height:22px; padding:15px; font-family:arial}
            .top-header-gdpr h2{font-size:18px; font-weight:bold; margin:0 0 10px; padding:0;}
            .top-header-gdpr .btm-button{text-align:left; margin:20px 0 0}
            .top-header-gdpr .btn{border-radius:20px; text-transform:uppercase; font-size:14px; line-height:24px; display:inline-block; padding:6px 0;  border:1px solid #F4B408; cursor:pointer; font-family:arial; text-align:center; color:#F4B408; background:transparent; width:30%; vertical-align:top}
            .top-header-gdpr a{text-decoration:none; color:#F4B408}
            .agreebox{display:inline-flex; vertical-align:top; width:65%; margin:0 10px 0 0; color:#ccc; font-size:14px; line-height:20px; text-align:left}
            .agreebox input{margin:4px 5px 0 0}
            .top-header-gdpr.desktop{width:1000px; box-sizing:border-box; margin:auto; font-size:15px; line-height:25px;}
            .top-header-gdpr.desktop .agreebox{margin-top:10px}
        </style>
        <h2>Accept the updated privacy & cookie policy</h2>
        <span>${textMess}</span>
    
        <div class="btm-button">
            <label class="agreebox">
                <input type="checkbox" id="personalisedCheckbox" />
                I agree to see customized ads that are tailor-made to my preferences
            </label>
            <input type="button" data-attr="accept_gdpr" value="ACCEPT" class="btn" />
        </div>
    `;

    const divtag = document.createElement("div");
    divtag.setAttribute("class", "top-header-gdpr");
    divtag.setAttribute("data-attr", "top-header-gdpr");
    // divtag.setAttribute("style","display:none");
    divtag.innerHTML = _gdprhtml;

    document.body.firstElementChild.before(divtag);
    if (!isMobilePlatform()) {
      document.querySelector('[data-attr="top-header-gdpr"]').classList.add("desktop");
    }
    // document.getElementById('consentAcceptButton1').addEventListener('click', GDPR_Module.agreeloginpopUp1);
  },

  sendConsentDataAcceptance(nodetype) {
    // let ssoid = Cookies.get("ssoid");

    // on accept click , remove the gdpr consent form
    document.querySelector('[data-attr="top-header-gdpr"]').style.display = "none";
    // disable button on policy-form
    if (document.querySelectorAll('[data-attr="accept_gdpr"]')[1]) {
      document.querySelectorAll('[data-attr="accept_gdpr"]')[1].removeAttribute("data-attr");
    }

    window._setCookie(GDPR_Module.config.updatedConsentCookieName, 1, GDPR_Module.config.consentCookieAge, "/");

    // modify text as per consent data provided
    // GDPR_Module.config.consentAcceptText = document.querySelector('[data-attr="top-header-gdpr"] span').innerText;

    // var bodyObj = {
    //     "consent": {
    //         "consents": [
    //             {
    //                 "agree": true,
    //                 "dataPoint": {
    //                     "id": GDPR_Module.config.consentCookieId
    //                 },
    //                 "text": GDPR_Module.config.consentAcceptText,
    //             }
    //         ],
    //         "productCode": GDPR_Module.config.channelCode + '-wap'
    //     }
    // }

    if (window.isCCPARegion) {
      const doNOTSellInfoChecked = document.getElementById("sellinfocheckbox").checked;

      window._setCookie(
        GDPR_Module.config.ccpaCookieName,
        doNOTSellInfoChecked ? 1 : 0,
        GDPR_Module.config.ccpaCookieAge,
        "/",
      );

      return;
    }

    const isPersonalised = document.getElementById("personalisedCheckbox").checked;

    const params = {};
    params[`${siteConfig.gdprID}_S_P`] = true;
    params[`${siteConfig.gdprID}_P_C`] = isPersonalised;

    // AnalyticsGA.event({ category: "GDPR", action: isPersonalised ? "Accept_with_personalized_ads" : "Accept_without_personalized_ads", label: 'GDPR_AC_Mweb' });

    // let params = {
    //     'lang_gdprcookieconsent': true,
    //     'lang_gdprpersonalizedconsent': isPersonalised
    // }

    window._setCookie(
      GDPR_Module.config.optOutCookieName,
      isPersonalised ? 0 : 1,
      GDPR_Module.config.optOutCookieAge,
      "/",
    );

    GDPR_Module.setPersonalizeAds();

    if (_getCookie("ssoid") != undefined) {
      fetch(
        `${GDPR_Module.config.consentApiurl}?propertyKeyValuePairs=${JSON.stringify(params)}&uuId=${_getCookie(
          "ssoid",
        )}`,
        {
          type: "jsonp",
          // ,dataType : "json"
        },
      ).then(resp => {
        // if(resp.status == 'success'){
        if (resp.ok) {
          // set consent cookie on success
          // window._setCookie(GDPR_Module.config.consentCookieName, GDPR_Module.config.consentCookieDefaultVal, GDPR_Module.config.consentCookieAge, '/');
          // window._setCookie(GDPR_Module.config.optOutCookieName, 0, GDPR_Module.config.optOutCookieAge, '/');

          if (nodetype == "anchor") {
            document.location.href = "/";
          }
        }
      });
    }
  },

  /**
   * As per the requirement, we are setting npa param equals to 1 only(0 value need not to be passed)
   * this is kept until user accepted the optout checkbox.
   */

  setPersonalizeAds() {
    // 0 means personalize ads and 1 means non-personalize ads
    const optOutCookieVal = GDPR_Module.getOptOutCookie();
    if (googletag && optOutCookieVal != "0" && window.geoinfo.isGDPRRegion) {
      // let isNonPersonalizeAds = window.geoinfo.notEU ? 0 : 1;
      // let isNonPersonalizeAds = optOutCookieVal == "0" ? 0 : 1;
      googletag.pubads().setRequestNonPersonalizedAds(1);
    }
  },

  onClickAcceptBtn() {
    if (document.querySelector('[data-attr="top-header-gdpr"]')) {
      // show gdpr consent form
      document.querySelector('[data-attr="top-header-gdpr"]').style.display = "block";

      document.body.addEventListener("click", event => {
        if (!event.target.hasAttribute("data-attr")) {
          return false;
        }

        let nodetype = "input";
        if (event.target.nodeName == "A") {
          event.preventDefault();
          nodetype = "anchor";
        }

        const _columbiaCoookieVal = window._getCookie(GDPR_Module.config.columbiaCoookie);
        if (_columbiaCoookieVal) {
          GDPR_Module.config.consentCookieDefaultVal = _columbiaCoookieVal;
        } else {
          GDPR_Module.config.consentCookieDefaultVal = GDPR_Module.createRandomUniqueKey();
        }

        // call for data consent sending api
        GDPR_Module.sendConsentDataAcceptance(nodetype);
      });
    }
  },

  cookiesetup: () => {
    window._getCookie = e => {
      let o;

      let r;

      let i;

      const n = document.cookie.replace("UserName=", "UserName&").split(";");
      for (o = 0; o < n.length; o++)
        if (
          ((r = n[o].substr(0, n[o].indexOf("="))),
          (i = n[o].substr(n[o].indexOf("=") + 1)),
          (r = r.replace(/^\s+|\s+$/g, "")) == e)
        )
          return unescape(i);
    };

    window._setCookie = (name, value, days, path, domain, secure) => {
      // This is done to not set any cookie in case of Europe or GDPR compliant continent
      // if(window.geoinfo && !window.geoinfo.notEU){
      //     return false;
      // }

      let expires = "";
      days = days !== undefined ? days : 30;
      const date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = `; expires=${date.toGMTString()}`;
      domain = (domain || document.location.host).split(":")[0]; // removing port
      path = path || document.location.pathname;
      // Removing file name, fix for IE11
      if (/\/.*\..*/.test(path)) {
        // if path contains file name
        path = path.split("/");
        path.pop();
        path = path.join("/");
      }
      document.cookie = `${name}=${value}${expires}${path ? `;path=${path}` : ""}${
        domain && domain != "localhost" ? `;domain=${domain}` : ""
      }${secure ? ";secure" : ""}`;
    };
  },

  consentPopUp() {
    const div = document.createElement("div");
    div.setAttribute("id", "popuphtml");
    document.body.appendChild(div);
    let popupHtml = "";
    popupHtml += `<div id="wdt_oneLaststep" class="active">
         <div class="box_oneLaststep">
            <div class="head">Welcome Back to The 
             <b>${siteConfig.wapsitename}</b>
             <button type="button" class="close-btn" id="gdprClosebtn">+</button>
            </div>
          <div class="txt_content">
            <ul>
            <li class="txt-info">
            <strong>One Last Step!</strong><p>We tailor your experience and understand how you and other visitors use this website by using cookies and other technologies. This means we are able to keep this site free-of-charge to use.</p><p>Please provide consent for the following scenarios so that we can ensure and enjoyable experience for you on our websites and mobile apps.</p>
            </li>
            <li class="checkbox">
            <p><input name="ageCheckBox"  id="ageCheckBox" type="checkbox"/>
            <label for="ageCheckBox">I warrant that I am at least 18 years old and agree with the <a href="${GDPR_Module.config.mweburl}/termsandcondition.cms" target="_blank" title="" rel="nofollow noindex">Terms &amp; Conditions</a> and <a href="${GDPR_Module.config.mweburl}/privacypolicy.cms" target="_blank" title="" rel="nofollow noindex">Privacy Policy</a> of Times Internet Ltd.
            </label></p>
            </li>
            <li class="checkbox">
            <p><input name="accountShareCheckBox" id="accountShareCheckBox" type="checkbox"/><label for="accountShareCheckBox">I agree to a single sign on for seamless experience across TIL sites</label></p>
            </li>
            <li class="submit"><input type="button" id="consentAcceptButton" class="submit-btn" value="I understand" /></li>
            </ul>
          </div>
        </div>
       </div>
      `;

    document.getElementById("popuphtml").innerHTML = popupHtml;

    GDPR_Module.checkBoxChecked();

    document.getElementById("gdprClosebtn").addEventListener("click", GDPR_Module.gdprLogout);
    document.getElementById("consentAcceptButton").addEventListener("click", GDPR_Module.agreeloginpopUp);
    document.getElementById("ageCheckBox").addEventListener("change", GDPR_Module.checkBoxvalidates);
    document.getElementById("accountShareCheckBox").addEventListener("change", GDPR_Module.checkBoxvalidates);
  },
  checkBoxChecked() {
    const ageCheckBox = document.getElementById("ageCheckBox");
    const accountShareCheckBox = document.getElementById("accountShareCheckBox");

    if (window.geoinfo.isGDPRRegion) {
      accountShareCheckBox.checked = "checked";
      ageCheckBox.checked = "checked";
    } else {
      document.getElementById("consentAcceptButton").disabled = "disabled";
    }
  },

  gdprLogout() {
    document.getElementById("popuphtml").innerHTML = "";
    // logoutuser();
    LoginControl.handleLogoutCall();

    // if (window._getCookie("gdpr") != undefined) {
    //   window._setCookie("gdpr", null, -1, "/");
    // }
  },

  checkBoxvalidates() {
    const ageCheckBox = document.getElementById("ageCheckBox");
    const accountShareCheckBox = document.getElementById("accountShareCheckBox");

    if (ageCheckBox.checked == true && accountShareCheckBox.checked == true) {
      document.getElementById("consentAcceptButton").disabled = false;
    } else {
      document.getElementById("consentAcceptButton").disabled = "disabled";
    }
  },

  agreeloginpopUp() {
    window.JSSO_INSTANCE = new JssoCrosswalk(getSsoChannel(), isMobilePlatform() ? "wap" : "web");
    const tpPolicy = window.geoinfo.isGDPRRegion ? 1 : 0;

    JSSO_INSTANCE.getValidLoggedInUser();
    JSSO_INSTANCE.updateUserPermissions(1, 1, tpPolicy, () => {
      document.getElementById("popuphtml").innerHTML = "";
    });
  },

  /* GDPR code end */

  init(options, callback) {
    if (typeof window === "undefined") return false;
    // console.log('gdfpr')
    if (typeof options === "object") {
      if (typeof callback === "function") options.callback = callback;
      GDPR_Module.setupConfig(options);
    }

    GDPR_Module.cookiesetup();
    window.geoinfo = { Continent: "" };

    // window.continent_europe = "EU"; // for Europe

    // if consent cookie not set , intiate geocheck and activate acceptance form for EU
    if (window._getCookie(GDPR_Module.config.updatedConsentCookieName) == undefined) {
      GDPR_Module.loadGeoApi();
      // else initiate all modules seting "consentGiven" true
    } else {
      GDPR_Module.loadGeoApi(true);

      // consent accepted then disable , consent agree button
      if (document.querySelectorAll('[data-attr="accept_gdpr"]')[1]) {
        document.querySelectorAll('[data-attr="accept_gdpr"]')[1].removeAttribute("data-attr");
      }
    }
  },

  getConsentCookie() {
    return window._getCookie(GDPR_Module.config.updatedConsentCookieName);
  },
  getOptOutCookie() {
    return window._getCookie(GDPR_Module.config.optOutCookieName);
  },
};

export default GDPR_Module;
