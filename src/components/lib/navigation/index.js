import React, { Component } from "react";
import ErrorBoundary from "../errorboundery/ErrorBoundary";
import AnchorLink from "../../common/AnchorLink";
import styles from "./../../common/css/header.scss";

// render HR Nav
export const renderHrNav = (object, parent, msid) => {
  return (
    <ErrorBoundary>
      <ul id="paginationHomePage">
        {// render parent
        parent.weblink ? (
          <li
            itemProp="name"
            className={
              parent.weblink.indexOf(msid) > -1 && msid != "" && msid != "/" ? "hrnavparent active" : "hrnavparent"
            }
            key={"hrNavParent"}
          >
            <AnchorLink
              className={styles["nav_link"] + " nav_link"}
              data-index={0}
              data-label={parent.label}
              hrefData={{ override: parent.weblink }}
            >
              {parent.label}
            </AnchorLink>
          </li>
        ) : null}

        {Object.keys(object).map(function(key, index) {
          return object[key].ignore != "true" &&
            object[key].weblink &&
            object[key].label &&
            object[key].weblink != "" ? (
            <li
              itemProp="name"
              className={object[key].weblink.indexOf(msid) > -1 && msid != "" && msid != "/" ? "active" : ""}
              key={key}
            >
              <AnchorLink
                itemProp="url"
                className={styles["nav_link"] + " nav_link"}
                data-index={index}
                data-label={object[key].label}
                hrefData={{ override: object[key].weblink }}
              >
                {object[key].label}
              </AnchorLink>
            </li>
          ) : null;
        })}
      </ul>
    </ErrorBoundary>
  );
};

// store rendered HR nav
let rendrd;

// primary function to start iteration of header node
export const startItr = (object, parentref, msid, callback) => {
  cycleObject(object, parentref, msid, callback);
  return rendrd;
};

export const cycleObject = (object, parentref, msid, callback) => {
  var searching = true;
  // console.dir(object);
  Object.keys(object).map(function(key, index) {
    // check if mweb url is available
    if (object[key].weblink && msid != "") {
      // check if MSID matches and has child
      // pass current nodes and object reference as return
      if (object[key].msid.indexOf(msid) > -1 && object[key][key + "-1"] && searching) {
        // new object taken
        let o = Object.assign({}, object[key]);
        parentref.label = o.label;
        parentref.weblink = o.weblink;
        delete o.label;
        delete o.weblink;
        delete o.weblink;
        delete o.msid;
        searching = false;
        rendrd = callback(o, parentref, msid);
      }
      // only msid matches and no child
      // In this case we need to show its siblings only
      // In return will pass parentref to showcase its siblings
      else if (object[key].msid.indexOf(msid) > -1 && searching) {
        //let o = Object.assign({},object[key]);
        // parentref.label = o.label;
        // parentref.weblink = o.weblink;
        searching = false;
        rendrd = callback(object, parentref, msid);
      }
      // continue with child if any
      // pass current node as well
      else if (object[key][key + "-1"] && searching) {
        // new object taken
        let o = Object.assign({}, object[key]);
        parentref.label = o.label;
        parentref.weblink = o.weblink;
        delete o.label;
        delete o.weblink;
        delete o.weblink;
        delete o.msid;
        cycleObject(o, parentref, msid, callback);
      }
    }
  });
};
