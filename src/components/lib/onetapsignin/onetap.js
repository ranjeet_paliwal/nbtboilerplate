import loadOneTap from "./lib/loadonetap";
import { _getCookie, _setCookie } from "./lib/utils";
import { AnalyticsGA } from "./../analytics/index";
import fetchJsonP from "fetch-jsonp";

const gaTracking = (gaObject, label) => {
  gaObject.label = label;
  AnalyticsGA.event(gaObject);
  // AnalyticsGA.GTM(gaObject);
};

//clear iframe timer on any error
const clearIframeCheckInterval = () => {
  clearInterval(window.iframeTimer);
};

// const attachPostLoginHTML = (user) => {
//     var loginHTML = '<div class="ontap-background id_postLogin hide">' + '<div class="tap-message">' + '<span class="success"></span>' + '<span class="text">' + '<b class="name">' + user.getFullName() + '</b>' + '<b class="email">' + user.getEmail() + '</b>' + '</span>' + '<span class="close id_onetapclose"></span>' + '</div>' + '<div class="caption">' + mod_oneTapSignin.config.text.oneTapSuccessMsg + '</div>' + '</div>';

//     $('body').append(loginHTML);
//     $('.id_postLogin').slideToggle();
//     $('.id_onetapclose').on('click', function () {
//         $('.id_postLogin').slideToggle();
//     });
// }

const bindEvents = options => {
  window.onGoogleYoloLoad = function(googleyolo) {
    // The 'googleyolo' object is ready for use.
    var checkForIframe = function checkForIframe() {
      let iframes = document.querySelectorAll("iframe");

      for (let i = 0; i < iframes.length; i++) {
        var iframeSrc = iframes[i].getAttribute("src");
        if (iframeSrc && iframeSrc.includes("https://accounts.google.com/gsi/iframe/select")) {
          //ga if iframe present in page , hidden or visible
          if (!options.isViewGaTracked) {
            //Commented as per product LAN-3508
            //gaTracking(options.eventGAObj , 'View');
            options.isViewGaTracked = true;
          }
          if (iframes[i] && !iframes[i].getAttribute("hidden")) {
            //ga if iframe popup in page , visible and clear the timer
            gaTracking(options.eventGAObj, "Popup_Final_View");
            clearIframeCheckInterval();
          }
        }
      }
    };
    window.iframeTimer = setInterval(checkForIframe, options.onetapIframeCheckTimer);

    var tthisConstants = options.constants;

    //Hint method/promise is used to initiate google login
    // supportedAuthMethods is list of types of logins
    //clientID  is where all analytics data will be sent (Should be created by TOI google account) done!!!
    var hintPromise = googleyolo.hint({
      supportedAuthMethods: ["https://accounts.google.com"],
      supportedIdTokenProviders: [
        {
          uri: "https://accounts.google.com",
          clientId: tthisConstants.clientId,
        },
      ],
    });
    //The Hint promise resolves here
    hintPromise.then(
      function(credential) {
        if (credential.idToken) {
          tthisConstants.credential = credential;
          // Send the token to your auth backend.home
          //console.log("Promise" + credential.idToken);
          var googleLongToken = credential.idToken;
          var channel = tthisConstants.channel;

          if (tthisConstants.jssoAPIGetURL && tthisConstants.jssoAPIGetURL) {
            fetchJsonP(tthisConstants.jssoAPIGetURL + "?token=" + googleLongToken + "&channel=" + channel, {
              method: "GET",
              mode: "cors",
              credentials: "include",
            })
              .then(response => {
                return response.json();
              })
              .then(data => {
                //console.log("INSIDE FETCH JSSO google ticket", data);
                gaTracking(options.eventGAObj, "Success_google");
                if (tthisConstants.jssoCallback) {
                  tthisConstants.jssoCallback();
                }
              })
              .catch(err => {
                gaTracking(options.eventGAObj, "Fail_Google");
              });

            // fetch(tthisConstants.jssoAPIGetURL,
            //     {method: 'GET', // or 'PUT'
            //     body: JSON.stringify({
            //         token: googleLongToken,
            //         channel: channel
            //     }), // data can be `string` or {object}!
            //     headers:{
            //         'Content-Type': 'application/json'
            //     }}
            // ).then(
            //     data => {
            //         tthisConstants.jssoCallback();
            //     }
            //     ,
            //     error => {
            //         options.gaTracking('Fail_Google');
            //     }
            // );
            // var request = $.ajax({
            //     url: tthisConstants.jssoAPIGetURL,
            //     method: "GET",
            //     data: {
            //         token: googleLongToken,
            //         channel: channel
            //     },
            //     dataType: "jsonp",
            //     success: tthisConstants.jssoCallback,
            //     error: function error(err) {
            //         //ga track if jsso failed
            //         options.gaTracking('Fail_Google');
            //     }
            // });
          }
          console.log(tthisConstants.credential);
        }
      },
      function(error) {
        console.error("onetap file error:", error.message);
        // console.log("error");
        // console.log(error);
        switch (error.type) {
          //  most common errors are when there are accounts available , google's native API does not  throw error,User closes the popup
          case "userCanceled":
            gaTracking(options.eventGAObj, "Close");
            // The user closed the hint selector. Depending on the desired UX,
            // request manual sign up or do nothing.
            _setCookie(
              tthisConstants.onetapCookie,
              "123",
              tthisConstants.onetapIgnoreDays,
              undefined,
              undefined,
              undefined,
            );
            clearIframeCheckInterval();
            break;
          case "noCredentialsAvailable":
            //Commented as per product LAN-3508
            //gaTracking(options.eventGAObj, 'ErrorNoCredentialsAvailable');
            // attachErrorLoginHTML();
            // No hint available for the session. Depending on the desired UX,
            clearIframeCheckInterval();
            break;
          case "requestFailed":
            // gaTracking(options.eventGAObj, "ErrorTimesout");
            // attachErrorLoginHTML();
            // The request failed, most likely because of a timeout.
            clearIframeCheckInterval();
            break;
          default:
            // gaTracking(options.eventGAObj, "Error");
            clearIframeCheckInterval();
          // mod_oneTapSignin.attachErrorLoginHTML();
        }
      },
    );
  };
};

export const OneTapIntialize = options => {
  if (typeof window == "undefined") {
    return false;
  }

  let isChrome = navigator.userAgent.toLowerCase().match("crios") || navigator.userAgent.toLowerCase().match("chrome");
  let isFirefox = navigator.userAgent.toLowerCase().match("firefox");
  let isSafari = navigator.userAgent.toLowerCase().match("safari");
  let nonSupportedBrowsers =
    navigator.userAgent.toLowerCase().match("kaios") ||
    navigator.userAgent.toLowerCase().match("presto") ||
    navigator.userAgent.toLowerCase().match("kaios") ||
    navigator.userAgent.toLowerCase().match("ucbrowser") ||
    navigator.userAgent.toLowerCase().match("ucmini");

  if (
    !_getCookie("ssoid") &&
    !_getCookie("onetapCookie") &&
    (isChrome || isSafari || isFirefox) &&
    !nonSupportedBrowsers
  ) {
    console.log("load smart lock js");
    bindEvents(options);
    // load js after non logged in user is on page for specified seconds
    setTimeout(loadOneTap(options), 10000); //smartlockJsLoadTimer
  }

  return true;
};

export default { OneTapIntialize };
