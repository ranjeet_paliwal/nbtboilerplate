import fetchJsonP from "fetch-jsonp";
import { AnalyticsGA } from "../analytics/index";
import { fireGRXEvent } from "../analytics/src/ga";
import { _getCookie, _setCookie } from "./lib/utils";

const eventGAObj = { hitType: "event", category: "Login", action: "One_Tap", label: "" };

const gaTracking = (gaObject, label) => {
  // eslint-disable-next-line no-param-reassign
  console.log("Called gaTracking", gaObject);

  gaObject.label = label;
  AnalyticsGA.event(gaObject);
  // AnalyticsGA.GTM(gaObject);
};

/* eslint-disable no-undef */
export const OneTapIntialize = options => {
  if (typeof window === "undefined") {
    return false;
  }

  const isChrome =
    navigator.userAgent.toLowerCase().match("crios") || navigator.userAgent.toLowerCase().match("chrome");
  const isFirefox = navigator.userAgent.toLowerCase().match("firefox");
  const isSafari = navigator.userAgent.toLowerCase().match("safari");
  const nonSupportedBrowsers =
    navigator.userAgent.toLowerCase().match("kaios") ||
    navigator.userAgent.toLowerCase().match("presto") ||
    navigator.userAgent.toLowerCase().match("kaios") ||
    navigator.userAgent.toLowerCase().match("ucbrowser") ||
    navigator.userAgent.toLowerCase().match("ucmini");

  if (
    !_getCookie("ssoid") &&
    !_getCookie("onetapCookie") &&
    (isChrome || isSafari || isFirefox) &&
    !nonSupportedBrowsers
  ) {
    // handling the google is not defined error gracefully
    // as api is now moved to the app.js from html file
    try {
      loadOneTap(options);
    } catch (error) {
      window.onGoogleLibraryLoad = () => {
        loadOneTap(options);
      };
    }
  }

  return true;
};

const loadOneTap = options => {
  try {
    google.accounts.id.initialize({
      client_id: options.constants.clientId,
      cancel_on_tap_outside: false,
      callback: response => handleCredentialResponse(response, options),
    });

    // eslint-disable-next-line no-unused-vars
    google.accounts.id.prompt(notification => gaCallsForOneTap(notification, options));
  } catch (e) {
    console.log("Error loading/initializing onetap", e);
  }
};

const handleCredentialResponse = (response, options) => {
  console.info("credential for the onetap", response);
  const onetapconstants = options.constants;
  if (response.credential) {
    onetapconstants.credential = response;
    // Send the token to your auth backend.home
    const googleLongToken = response.credential;
    const { channel } = onetapconstants;

    if (onetapconstants.jssoAPIGetURL && onetapconstants.jssoAPIGetURL) {
      fetchJsonP(`${onetapconstants.jssoAPIGetURL}?token=${googleLongToken}&channel=${channel}`, {
        method: "GET",
        mode: "cors",
        credentials: "include",
      })
        .then(newresponse => newresponse.json())
        .then(() => {
          console.log("INSIDE FETCH JSSO google ticket");
          try {
            gaTracking(eventGAObj, "email_click");
            window._login_source = "onetap";
          } catch (ex) {
            console.log("Failed to log GA", ex);
          }

          if (onetapconstants.jssoCallback) {
            onetapconstants.jssoCallback();
          }
        })
        .catch(() => {
          gaTracking(eventGAObj, "Fail_Google");
        });
    }
  }
};

const gaCallsForOneTap = (notification, options) => {
  console.info("notification for the onetap", notification);
  const onetapconstants = options.constants;
  if (notification.g === "skipped" && notification.l === "user_cancel") {
    fireGRXEvent("track", "no_login");
    gaTracking(eventGAObj, "Close");
    _setCookie(onetapconstants.onetapCookie, "123", onetapconstants.onetapIgnoreDays, undefined, undefined, undefined);
  } else if (notification.g === "display" && notification.h == true) {
    gaTracking(eventGAObj, "Popup_Final_View");
  }
};
