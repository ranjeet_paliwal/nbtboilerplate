export default function(options) {
  (function(i, s, o, g, r, a, m) {
    (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m);
  })(
    window,
    document,
    "script",
    options && options.src ? options.src : "https://smartlock.google.com/client"
  );
}
