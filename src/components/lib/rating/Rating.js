import React from "react";
import styles from "./Rating.css";

const Rating = props => {
  let rating = parseFloat(props.rate) * 20;
  return (
    <div className={styles.starRatingsSprite}>
      <span
        style={{ width: rating + "%" }}
        className={styles.starRatingsSpriteRating}
      ></span>
    </div>
  );
};

export default Rating;
