import React from "react";
import { withRouter, Link } from "react-router";
import {
  _getStaticConfig,
  isTechSite,
  getCountryCode,
  isInternationalUrl,
  isBusinessSection,
  isGadgetPage,
  getLogoLink,
  getLogoPath,
  getLogoClass,
} from "../../utils/util";
import globalconfig from "../../globalconfig";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import { internationalconfig } from "../../utils/internationalpageConfig";
import AnchorLink from "./AnchorLink";
const siteConfig = _getStaticConfig();

const Logo = props => {
  const { router, config } = props;
  const pagetype = config && config.pagetype;
  const siteName = config && config.siteName;
  const pathname = router.location && router.location.pathname;
  const logoPath = getLogoPath(pagetype, pathname, siteName);
  const countryCode = getCountryCode();
  const logoLink = getLogoLink(pagetype, pathname, countryCode, siteName);
  const logocssClass = getLogoClass(pagetype, pathname);

  return (
    <Logocon router={router}>
      <ErrorBoundary>
        <AnchorLink href={logoLink} title={siteConfig.languagefullName} className={logocssClass}>
          <img alt={siteConfig.languagefullName} title={siteConfig.languagefullName} src={logoPath} />
          {router.location.pathname &&
          (router.location.pathname.endsWith("/tech") ||
            router.location.pathname.indexOf("tech/amp_articlelist") > -1) ? (
            <span className="txt-seo">{siteConfig.locale.tech.techNewsH1}</span>
          ) : null}
        </AnchorLink>
      </ErrorBoundary>
    </Logocon>
  );
};

export const Logocon = ({ children, ...other }) => {
  let element;
  const router = other.router;
  element =
    other.pagetype == "/" || other.pagetype == "/amp_default.cms" ? (
      <h1 title={siteConfig.languagefullName}>{children}</h1>
    ) : globalconfig.pagetype === "home" ? (
      <h1 itemProp="name" title={siteConfig.languagefullName}>
        {children}
      </h1>
    ) : router && router.location && (router.location.pathname.endsWith("/tech") || isInternationalUrl(router)) ? (
      <h1 itemProp="name" title={siteConfig.languagefullNameGN}>
        {children}
      </h1>
    ) : (
      <h2 itemProp="name" title={siteConfig.languagefullName}>
        {children}
      </h2>
    );

  return element;
};

export default withRouter(Logo);
