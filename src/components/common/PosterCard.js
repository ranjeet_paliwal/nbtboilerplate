import React from "react";
import { Link } from "react-router";
import styles from "./css/PosterCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const PosterCard = props => {
  let { section, heading, title, link, image } = props;
  heading = heading ? heading : section && section._heading ? section._heading : "";
  title = title ? title : section && section._title ? section._title : "";
  link = link ? link : section && section._override ? section._override : "";
  image = image ? image : section && section._image ? section._image : "";

  return link && image ? (
    <div className={"poster-content" + (!title ? " custom-poster" : "")}>
      <AnchorLink hrefData={{ override: section._override }} className="">
        <div className="pos_section" data-attr-image={image}>
          <ImageCard type="absoluteImgSrc" src={image} className="poster-bg" />
          <span className="pos_title">{heading}</span>
          {title ? (
            <div className="pos_text">
              <h3>{title}</h3>
              <button className="more-btn pos_more">{siteConfig.locale.aur_jaane}</button>
            </div>
          ) : null}
        </div>
      </AnchorLink>
    </div>
  ) : null;
};

export default PosterCard;
