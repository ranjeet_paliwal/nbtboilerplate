import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import { _isCSR, _deferredDeeplink, isMobilePlatform } from "./../../utils/util";
import styles from "./css/PhotoMazzaShowCard.scss";
import PhotoMazzaShowSubCard from "./PhotoMazzaShowSubCard";
import ErrorBoundary from "./../lib/errorboundery/ErrorBoundary";
import AdCard from "./AdCard";

import { _getStaticConfig } from "./../../utils/util";
import FakePhotoMazzaCard from "./FakeCards/FakePhotoMazzaCard";
const siteConfig = _getStaticConfig();
const adsObj = siteConfig.ads;
// import PaytmWidget from './../../modules/paytmwidget/PaytmWidget';

class PhotoMazzaShowCard extends Component {
  constructor(props) {
    super(props);

    this.config = {
      adposition: adsObj.ctnads.adspositionsPhoto,
    };
  }

  render() {
    let {
      item,
      view,
      msid,
      isInlineContent,
      parentIndex,
      photoshow_app_install,
      showAppInstall,
      openCommentsPopup,
      parentid,
      isAmpPage
    } = this.props;
    let imgMatched = false,
      stopRendering = false;
    let nextItem = item.pwa_meta && item.pwa_meta.nextItem ? item.pwa_meta.nextItem : undefined;
    // subsec1 var for paytm only
    let subsec1 = item.subsec1;
    let var_showAppInstall = item.items && showAppInstall && showAppInstall(item.items, msid) ? true : false;
    let app_install_show_slides =
      photoshow_app_install && photoshow_app_install.show_slides ? photoshow_app_install.show_slides : 0;

    if (item.items && item.items.length > 0) {
      return (
        <React.Fragment>
          <ul
            parent-index={parentIndex}
            className={
              typeof view != "undefined"
                ? styles["photo_story"] + " photo_story " + view
                : styles["photo_story"] + " photo_story"
            }
          >
            {item.items.map((it, index) => {
              if (var_showAppInstall && index > app_install_show_slides) {
                return null;
              }
              // For Bot : if server side rendering ,show in bunch of 5 or less , decided by feed nextItem.msid
              if (!_isCSR() && nextItem && nextItem.msid == it.id) stopRendering = true;
              if (stopRendering) return null;

              //Rendering start after the slide which match msid.
              //Ex: If id is of 4th slide, then we start slideshow with 4th slide not with first.
              if (parentIndex > 0 || msid == it.id) imgMatched = true;
              //
              if (item.it.id != msid && imgMatched == false) return null;

              let _id = it.id;
              let url = it.tn == "html" ? it.wu : "/photoshow/" + _id + ".cms";
              url = it.seolocation ? "/" + it.seolocation + url : url;

              return (
                <ErrorBoundary key={index}>
                  <PhotoMazzaShowSubCard
                    parentid={parentid}
                    photoshow_app_install={photoshow_app_install}
                    showAppInstall={var_showAppInstall}
                    item={it}
                    url={url}
                    key={index}
                    index={index}
                    openCommentsPopup={openCommentsPopup}
                    count={item.pg.cnt}
                    view={view}
                    isInlineContent={isInlineContent}
                    isAmpPage={isAmpPage}
                  />
                  {index == 3 && isMobilePlatform() ? (
                    <li className="inline_app_download">
                      <div className="table">
                        <div className="table_row">
                          <span className="table_col con_wrap">{siteConfig.locale.seemoreimages}</span>
                          <span className="table_col">
                            <a
                              href={_deferredDeeplink("photo", siteConfig.appdeeplink, parentid)}
                              className="btn_appdownload"
                            >
                              {siteConfig.locale.photoshow_app_install.download}
                            </a>
                          </span>
                        </div>
                      </div>
                    </li>
                  ) : null}

                  {
                    /* Print Paytm Widget for Tech Section Only */
                    // index == 1 && !isInlineContent && subsec1 == '19615041' ? <li className="paytm_wrapper"><PaytmWidget pagename="photoshow" subsec1={subsec1} /></li> : null
                  }

                  {!isInlineContent &&
                  this.config.adposition &&
                  this.config.adposition.indexOf("-" + (index + 1) + "-") > -1 ? (
                    index + 1 == 2 ? (
                      <AdCard mstype="mrec1" adtype="dfp" elemtype="li" />
                    ) : (
                      <AdCard mstype="ctnbigphoto" adtype="ctn" elemtype="li" />
                    )
                  ) : null}
                </ErrorBoundary>
              );
            })}
            {this.props.isLoadingSlides && <FakePhotoMazzaCard showImages={true} />}
          </ul>
          {/* next slide url for Google Bot to read */}
          {!_isCSR() && nextItem && nextItem.url ? <Link to={nextItem.url} /> : null}
        </React.Fragment>
      );
    } else {
      return null;
    }
  }
}

PhotoMazzaShowCard.propTypes = {
  item: PropTypes.object,
};

export default PhotoMazzaShowCard;
