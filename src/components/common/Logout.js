import React, { Component } from "react";
import { logOut } from "./ssoLogin";
import Cookies from "react-cookie";
class Logout extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    logOut(Cookies.load("ssoid"));
    window.close();
  }

  render() {
    return <div>Logging out...</div>;
  }
}

export default Logout;
