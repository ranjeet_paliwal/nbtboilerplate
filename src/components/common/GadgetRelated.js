import React, { Component } from "react";
import PropTypes from "prop-types";
import ImageCard from "./ImageCard/ImageCard";
import { AnalyticsGA } from "./../lib/analytics/index";
import AnchorLink from "./AnchorLink";
import { _getStaticConfig, _getUrlOfCategory } from "../../utils/util";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import styles from "./../../components/common/css/GadgetNow.scss";
const siteConfig = _getStaticConfig();

const removespace = title => {
  if (typeof title != "undefined" && typeof title != "object") {
    title = title
      .trim()
      .replace(/\s+/g, "-")
      .toLowerCase();
    return title;
  } else return;
};
const getURL = url => {
  if (url.indexOf("https://www.amazon.in") > -1) {
    return url.replace("https://www.amazon.in", "");
  } else {
    return url;
  }
};
class GadgetRelated extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.chksuggestedlist();
  }
  chksuggestedlist() {
    //on page load highlight in suugested list available
    var el = document.querySelectorAll(".add-to-compare");
    for (let i = 0; i < el.length - 1; i++) {
      for (let j = 1; j < 4; j++) {
        if (
          window.compareDevice[j] &&
          window.compareDevice[j].keyword &&
          el[i] &&
          el[i].getAttribute("seo") &&
          el[i].getAttribute("seo") == window.compareDevice[j].keyword
        ) {
          el[i].classList.add("highlighted");
        }
      }
    }
  }
  render() {
    let _this = this;
    let { suggested, headingoverride, anchorlink } = this.props;
    let category = suggested && suggested.length > 0 ? _getUrlOfCategory(suggested[0].category) : "";
    var filtered = siteConfig.locale.tech.category.filter(function(item) {
      return item.keyword == category;
    });
    return (
      <div className="gadget-amz-scroll">
        <h2>
          <span>
            {headingoverride
              ? siteConfig.locale.tech.relatedgadgets
              : siteConfig.locale.tech.suggested +
                " " +
                filtered[0].keylabel +
                " " +
                siteConfig.locale.tech.ki +
                " " +
                siteConfig.locale.tech.comparetxt}
          </span>
        </h2>
        <ul>
          {suggested && suggested.length > 0
            ? suggested.map((item, index) => {
                let amazoncontent, amazoncontentexact;
                item.affiliate && item.affiliate.related && item.affiliate.related.length > 0
                  ? (amazoncontent = item.affiliate.related.filter(item => {
                      return item.Identifier == "amazon";
                    }))
                  : null;
                item.affiliate && item.affiliate.exact && item.affiliate.exact.length > 0
                  ? (amazoncontentexact = item.affiliate.exact.filter(item => {
                      return item.Identifier == "amazon";
                    }))
                  : null;
                let prodUrl;
                amazoncontentexact && amazoncontentexact[0]
                  ? (prodUrl =
                      "https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=" +
                      getURL(amazoncontentexact[0].url) +
                      "&utm_campaign=nbt_wap_comparisonlanding_suggestionwidget-21&title=" +
                      removespace(amazoncontentexact[0].name) +
                      "&price=" +
                      amazoncontentexact[0].sort_price +
                      "&utm_source=gnwap&utm_medium=Affiliate")
                  : null;
                !prodUrl && amazoncontent && amazoncontent[0]
                  ? (prodUrl =
                      "https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=" +
                      getURL(amazoncontent[0].url) +
                      "&utm_campaign=nbt_wap_comparisonlanding_suggestionwidget-21&title=" +
                      removespace(amazoncontent[0].name) +
                      "&price=" +
                      amazoncontent[0].sort_price +
                      "&utm_source=gnwap&utm_medium=Affiliate")
                  : null;
                let itemname = item.regional && item.regional.name ? item.regional.name : item.name;
                return (
                  <li>
                    {this.props.addtocompare ? (
                      <span
                        className="add-to-compare"
                        onClick={_this.props.addtocompare}
                        itemname={item.name}
                        seo={item.uname}
                      ></span>
                    ) : null}
                    <div className="img_wrap">
                      {anchorlink ? (
                        <AnchorLink href={`/tech/${_getUrlOfCategory(item.category)}/${item.uname}`}>
                          <ImageCard
                            msid={item.imageMsid instanceof Array ? item.imageMsid[0] : item.imageMsid}
                            src={item.imageMsid instanceof Array ? item.imageMsid[0] : item.imageMsid}
                            size="gnthumb"
                            schema
                          />
                        </AnchorLink>
                      ) : (
                        <ImageCard
                          msid={item.imageMsid instanceof Array ? item.imageMsid[0] : item.imageMsid}
                          src={item.imageMsid instanceof Array ? item.imageMsid[0] : item.imageMsid}
                          size="gnthumb"
                          schema
                        />
                      )}
                    </div>
                    <div className="item_name">
                      <span className="text_ellipsis">
                        {anchorlink ? (
                          <AnchorLink href={`/tech/${_getUrlOfCategory(item.category)}/${item.uname}`}>
                            {itemname}
                          </AnchorLink>
                        ) : (
                          itemname
                        )}
                      </span>
                    </div>
                    {prodUrl ? (
                      <a className="btn_purchase" target="_blank" rel="nofollow" href={prodUrl}>
                        <b>Amazon</b> {siteConfig.locale.tech.pe} {siteConfig.locale.tech.purchase}
                      </a>
                    ) : null}
                    {/* {
                                        this.props.addtocompare ? 
                                            <div className="btn_purchase" onClick={_this.props.addtocompare} itemname={itemname} seo={item.uname}> {siteConfig.locale.tech.addtocompare}</div>
                                            : null
                                        } */}
                  </li>
                );
              })
            : null}
        </ul>
      </div>
    );
  }
}

GadgetRelated.propTypes = {
  item: PropTypes.object,
};

export default GadgetRelated;
