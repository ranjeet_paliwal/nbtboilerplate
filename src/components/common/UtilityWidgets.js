import React, { Component, PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { capitalizeFirstLetter, _getStaticConfig, _isCSR } from "../../utils/util";
import PollutionCard from "./PollutionCard";
import FuelCard from "./FuelCard";
import WeatherCard from "./WeatherCard";
import "./css/UtilityWidgets.scss";
import fetch from "../../utils/fetch/fetch";
const siteConfig = _getStaticConfig();
const siteName = process.env.SITE;

class UtilityWidgets extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { cities: [], selectedCity: "" };
    this.config = { citiesJson: [] };
  }

  componentDidMount() {
    const { data } = this.props;
    let city = siteConfig.weather.defaultCity;
    if (data && typeof data.location !== "undefined") {
      city = data.location;
    } else if (data && typeof data.alternatetitle !== "undefined") {
      city = data.alternatetitle;
    }
    this.setState({ selectedCity: capitalizeFirstLetter(city) });
    this.fetchData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { data } = this.props;
    if (prevProps.data.alternatetitle !== data.alternatetitle) {
      //&& prevState.selectedCity != this.state.selectedCity) {
      let city = siteConfig.weather.defaultCity;
      if (
        data.alternatetitle &&
        this.config.citiesJson instanceof Array &&
        this.config.citiesJson.filter(cityobj => {
          return cityobj.city.toLowerCase() == data.alternatetitle.toLowerCase();
        }).length > 0
      ) {
        city = data.alternatetitle;
      }
      city = capitalizeFirstLetter(city);
      if (city != this.state.selectedCity) this.setState({ selectedCity: city });
    }
  }

  fetchData() {
    let _this = this;
    fetch(siteConfig.weather.citiesJson)
      .then(resp => {
        _this.config.citiesJson = resp.cityjson;
        if (
          resp.cityjson instanceof Array &&
          resp.cityjson.filter(cityobj => {
            return cityobj.city == this.state.selectedCity;
          }).length > 0
        ) {
          this.setState({ cities: resp.cityjson });
        } else {
          this.setState({ selectedCity: siteConfig.weather.defaultCity, cities: resp.cityjson });
        }
      })
      .catch(e => {});
  }

  cityChange = e => {
    if (e.target.value) {
      // this.config.shouldUpdate = false;
      this.setState({ selectedCity: e.target.value });
    }
  };

  render() {
    const { cities, selectedCity } = this.state;

    if (!cities || !_isCSR()) {
      return null;
    }
    return (
      <div className="utility-widgets">
        <div className="utl-header">
          <h2>
            <span>{siteConfig.locale.utility}</span>
          </h2>
          <div className="custom-select">
            <select onChange={this.cityChange} value={this.state.selectedCity}>
              {cities.map((item, index) => (
                <option value={item.city} key={`city${index}`}>
                  {item[siteName]}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div>
          <PollutionCard selectedCity={selectedCity} />
          <FuelCard selectedCity={selectedCity} />
          <WeatherCard version="v2" selectedCity={selectedCity} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  weather: state.app.weather,
});

UtilityWidgets.propTypes = {
  weather: PropTypes.string,
};

export default connect(mapStateToProps)(UtilityWidgets);
