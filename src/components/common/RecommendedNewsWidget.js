import React from "react";
import { connect } from "react-redux";

// import AnchorLink from "./AnchorLink";
// import AdCard from "./AdCard";
import { _getStaticConfig } from "../../utils/util";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import GridSectionMaker from "./ListingCards/GridSectionMaker";
import { _isCSR, isMobilePlatform } from "../../utils/util";
// FIXME: Move ads to feed (Varun sir)
const Config = _getStaticConfig();

const RecommendedNewsWidget = ({ articleId, rhsData, sectionType, isFeaturedArticle, index, hidethumbandsection }) => {
  const data = rhsData;
  const artid = articleId;

  let filternewssection =
    data && Array.isArray(data) && data.length > 0
      ? data.filter(fdata => {
          return fdata.ctype === "news" && fdata.id != artid;
        })
      : [];

  let filternonnewssection =
    data && Array.isArray(data) && data.length > 0
      ? data.filter(fdata => {
          return fdata.ctype === "nonnews" && fdata.id != artid;
        })
      : [];

  let platform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";

  let filteradvertorial =
    data && Array.isArray(data) && data.length > 0
      ? data.filter(fdata => {
          return (
            (platform === "mobile" ? fdata.ctype == "adv_wap" : fdata.ctype == "adv") &&
            fdata.wu &&
            fdata.wu.includes(process.env.WEBSITE_URL)
          );
        })
      : [];

  if (filteradvertorial && filteradvertorial.length > 2) {
    filteradvertorial.splice(2);
  }

  filternonnewssection = suffleArticles([...filternonnewssection]);
  filternewssection = suffleArticles([...filternewssection]);

  let filternonnewssectionCopy = filternonnewssection;
  let filternewssectionCopy = filternewssection;

  let dataArray;
  if (sectionType === "non-news") {
    let finalArrayNonNews = [].concat(
      filternonnewssectionCopy.sort(customSorting).splice(0, 8),
      filternewssectionCopy.splice(0, 5),
    );
    dataArray = insertAds(finalArrayNonNews, filteradvertorial);
  } else {
    let finalArrayNews = [].concat(
      filternewssection.splice(0, 8),
      filternonnewssection.sort(customSorting).splice(0, 5),
    );
    dataArray = insertAds(finalArrayNews, filteradvertorial);
  }

  const UTMtype = "article";

  const dataArrayCopy = JSON.parse(JSON.stringify(dataArray));

  const dataArrayReConstructed = dataArrayCopy.map((nData, index) => {
    const newLink = nData.wu
      ? `${nData.wu}?utm_source=recommended&utm_medium=referral&utm_campaign=${UTMtype}${index + 1}`
      : "";
    // const newLink = nData.override;
    const imageid = nData.imageid === "" ? Config.imageconfig.thumbid : nData.imageid;

    nData.override = newLink;
    // nData.override = newLink;
    nData.imageid = imageid;
    return nData;
  });

  return (
    <div
      className="box-item recommended_news"
      data-scroll-id={isMobilePlatform() ? `recommended-news-${articleId}` : null}
      // index comes as false ( 0 ) since 0 evaluates to falsy
      data-row-id={isMobilePlatform() ? `recommended-news${index ? "-perpetual" : ""}` : null}
      data-exclude="amp"
    >
      {data ? (
        <div className="inner-content">
          {!isMobilePlatform() ? (
            <React.Fragment>
              <div className="section">
                <div className="top_section">
                  <h3>
                    <span>{Config.locale.recommendednews}</span>
                  </h3>
                </div>
              </div>
              <ErrorBoundary>
                <ErrorBoundary>
                  <GridSectionMaker
                    type={defaultDesignConfigs.gridHorizontalAL}
                    data={dataArrayReConstructed}
                    isFeaturedArticle={isFeaturedArticle}
                  />
                </ErrorBoundary>
              </ErrorBoundary>
            </React.Fragment>
          ) : (
            <React.Fragment>
              {isMobilePlatform() && _isCSR() ? (
                <React.Fragment>
                  <div className="section">
                    <div className="top_section">
                      <h3>
                        <span>{Config.locale.recommendednews}</span>
                      </h3>
                    </div>
                  </div>
                  <ErrorBoundary>
                    <ErrorBoundary>
                      <GridSectionMaker
                        type={defaultDesignConfigs.gridHorizontalAL}
                        data={dataArrayReConstructed}
                        isFeaturedArticle={isFeaturedArticle}
                        hidethumbandsection={hidethumbandsection}
                      />
                    </ErrorBoundary>
                  </ErrorBoundary>
                </React.Fragment>
              ) : null}
            </React.Fragment>
          )}
        </div>
      ) : null}
    </div>
  );
};

const customSorting = (a, b) => {
  const bandA = a.lu;
  const bandB = b.lu;

  let comparison = 0;
  if (bandA < bandB) {
    comparison = 1;
  } else if (bandA > bandB) {
    comparison = -1;
  }
  return comparison;
};

// Adding ads in 2, 5, 10 , 13 positions
const insertAds = (dataArray, data) => {
  // Handling some editads
  if (data && data.length > 0 && data.length <= 2) {
    dataArray.splice(1, 0, data[0]);
    dataArray.splice(2, 0, {
      type: "ctn",
      mstype: "ctnrecommended",
      tn: "ad",
    });
    if (data && data.length == 1) {
      dataArray.splice(4, 0, {
        type: "ctn",
        mstype: "ctnrecommended",
        tn: "ad",
      });
    } else {
      dataArray.splice(4, 0, data[1]);
      dataArray.splice(5, 0, {
        type: "ctn",
        mstype: "ctnrecommended",
        tn: "ad",
      });
    }
  } else {
    dataArray.splice(1, 0, {
      type: "ctn",
      mstype: "ctnrecommended",
      tn: "ad",
    });
    dataArray.splice(4, 0, {
      type: "ctn",
      mstype: "ctnrecommended",
      tn: "ad",
    });
  }

  dataArray.splice(9, 0, {
    type: "ctn",
    mstype: "ctnrecommended",
    tn: "ad",
  });

  dataArray.splice(12, 0, {
    type: "ctn",
    mstype: "ctnrecommended",
    tn: "ad",
  });

  return dataArray;
};

const suffleArticles = data => {
  let suffledData = data && data.sort(() => Math.random() - 0.5);
  return suffledData;
};

function mapStateToProps(state) {
  return {
    rhsData: state.app.rhsData,
  };
}

export default connect(mapStateToProps)(RecommendedNewsWidget);
