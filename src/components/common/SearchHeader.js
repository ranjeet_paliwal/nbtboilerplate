import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, browserHistory } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/search.scss";
// import {gaEvent} from  './../../utils/util';
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;

class SearchHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.recognition = false;
    this.ignore_onend = null;
    this.start_timestamp = null;
    this.final_transcript = "";
    this.recognizing = false;
  }

  componentDidMount() {
    const _this = this;
    if (typeof window != "undefined") {
      window.setTimeout(() => {
        if (this.props.searchTerm != "") {
          document.getElementById("searchInput").value = this.props.searchTerm;
        }
      }, 500);

      if (!("webkitSpeechRecognition" in window)) {
        document.getElementById("voiceSearch").style.visibility = "hidden";
      } else {
        document.getElementById("voiceSearch").style.visibility = "visible";
        _this.recognition = new webkitSpeechRecognition();
        _this.recognition.continuous = false;
        _this.recognition.interimResults = false;
        _this.recognition.onstart = function() {
          _this.recognizing = true;
          document
            .getElementById("searchImg")
            .setAttribute("src", "/img/mic-anim.gif");
        };
        _this.recognition.onerror = function(event) {
          if (event.error == "no-speech") {
            document
              .getElementById("searchImg")
              .setAttribute("src", "/img/mic.png");
            console.log("No speech was found...Try again");
            _this.ignore_onend = true;
          }
          if (event.error == "audio-capture") {
            document
              .getElementById("searchImg")
              .setAttribute("src", "/img/mic.png");

            _this.ignore_onend = true;
          }
          if (event.error == "not-allowed") {
            _this.ignore_onend = true;
          }
        };
        _this.recognition.onend = function() {
          _this.recognizing = false;
          if (_this.ignore_onend) {
            return;
          }
          document
            .getElementById("searchImg")
            .setAttribute("src", "/img/mic.png");

          if (!_this.final_transcript) {
            return;
          }
        };
        _this.recognition.onresult = function(event) {
          _this.final_transcript = event.results[0][0].transcript;
          document.getElementById("searchInput").value = _this.final_transcript;
          _this.recognition.stop();
          _this.searchClicked();
        };
      }
    }
  }

  voiceSearchClicked(event) {
    if (this.recognizing) {
      this.recognition.stop();
      return;
    }
    this.final_transcript = "";
    this.recognition.lang = "en-US";
    this.recognition.start();
    this.ignore_onend = false;
    document
      .getElementById("searchImg")
      .setAttribute("src", "/img/mic-dis.gif");
  }

  handleInputChange(event) {
    this.setState({ value: event.target.value });
  }

  searchClicked() {
    let term = document.getElementById("searchInput").value;
    browserHistory.push("/topics/" + encodeURI(term));
  }

  onKeyPressHandler(event) {
    if (event.keyCode == 13) {
      let term = document.getElementById("searchInput").value;
      browserHistory.push("/topics/" + encodeURI(term));
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.searchTerm) this.setState({ value: nextProps.searchTerm });
  }

  render() {
    let { searchTerm } = this.props;
    return (
      <div
        className={styles.searchHeader + " searchHeader"}
        id="categoryHeader"
      >
        <table width="100%" cellPadding="0" cellSpacing="0" border="0">
          <tbody>
            <tr>
              <td>
                <Link to="/">
                  <div className={styles.backButton}></div>
                </Link>
              </td>
              <td className={styles.searchIcon}>
                <input
                  id="searchInput"
                  type="text"
                  placeholder={locale.search_placeholder}
                  value={this.state.value}
                  onKeyDown={this.onKeyPressHandler}
                  onChange={this.handleInputChange}
                />
              </td>
              <td width="10%">
                <div
                  className={styles.voiceSearchButton}
                  onClick={this.voiceSearchClicked.bind(this)}
                  id="voiceSearch"
                >
                  <img src="/img/mic.png" id="searchImg" />
                </div>
              </td>
              <td width="10%">
                <div
                  className={styles.searchButton}
                  onClick={this.searchClicked.bind(this)}
                ></div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.config
  };
}

export default connect(mapStateToProps)(SearchHeader);
