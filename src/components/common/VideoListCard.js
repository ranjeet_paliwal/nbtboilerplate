import React, { Component } from "react";

import PropTypes from "prop-types";
import styles from "./css/VideoListCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import { getImageParameters } from "./ImageCard/ImageCardUtils";
import VideoPlayer from "./../../modules/videoplayer/index";
import { SeoSchema } from "./PageMeta";

import { AnalyticsGA } from "./../lib/analytics/index";
import AnchorLink from "./AnchorLink";
import Ads_Module from "../lib/ads";
import { createConfig } from "./../../modules/videoplayer/utils";

import { _getStaticConfig, modifyAdsOnScrollView } from "../../utils/util";
import KeyWordCard from "./KeyWordCard";
const siteConfig = _getStaticConfig();

const slikeApiKey = siteConfig.slike.apikey;
const slikeAdCodes = siteConfig.slikeAdCodes;
const adsObj = siteConfig.ads;

class VideoListCard extends Component {
  constructor(props) {
    super(props);
    this.config = {
      isLandingPage: true,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.item.id != nextProps.item.id) {
      return true;
    }
    return false;
  }

  // If leadpost hide icon in amp (data-exclude='amp')
  showListingIcon(item, leadpost) {
    let icon = item.tn == "video" ? "video_icon" : item.tn == "photo" ? "photo_icon" : "";
    if (icon == "") return;
    return <span className={"type_icon " + icon} data-exclude={leadpost ? "amp" : null}></span>;
  }

  showListNodeLabel(item) {
    let label = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][1] : "";
    return label;
  }

  generateUrl(item) {
    // if(item.override) {
    //   return item.override;
    // } else {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return url;
    // }
  }

  onVideoClickHandler(item, event) {
    let _this = this;
    let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
    if (videourl) {
      try {
        //To maintain previous url
        window.history.pushState({}, "", videourl);
      } catch (ex) {
        console.log("Exception in history: ", ex);
      }
    }

    if (_this.props.onVidClick) {
      _this.props.onVidClick();
      AnalyticsGA.pageview(location.origin + _this.generateUrl(item));
      // trigger recreate of ads
      if (_this.config.isLandingPage) {
        window.wapads = adsObj.dfpads["videoshow"];
        Ads_Module.recreateAds(["fbn"]);
        // after recreation , reset fbn refresh ads
        // modifyAdsOnScrollView(true);

        _this.config.isLandingPage = false;
      } else {
        //Refresh ads
        Ads_Module.refreshAds(["fbn"]);
      }
      // let oldAd = document.querySelector('.ad1.atf') ;
      // if(oldAd && oldAd != null){
      //   oldAd.remove();
      // }
    }
  }

  render() {
    let { item, leadpost, type, source, position, subsecmsid2, noLazyLoad, keyword } = this.props;
    //call for common create config for settig up slike config as per template
    let config = createConfig(item, subsecmsid2, siteConfig, source);
    let posturl = this.generateUrl(item);
    let videourl = this.generateUrl(item);
    let thumbnailURL = getImageParameters({
      msid: item.imageid ? item.imageid : item.id,
      title: item.seotitle ? item.seotitle : "",
      size: type == "video_lead" ? "largewidethumb" : leadpost ? "largewidethumb" : "smallwidethumb",
    }).src;

    return (
      <li
        data-videourl={videourl}
        // data-attr-slk={source == "videoshow" && leadpost && item.id != undefined ? item.id : null}
        className={
          (leadpost
            ? source == "videoshow"
              ? [styles["videoshowview"], styles["lead-post"]].join(" ")
              : [styles["videolistview"], styles["lead-post"]].join(" ")
            : source == "videoshow"
            ? styles["videoshowview"]
            : styles["videolistview"]) +
          " animated fadeIn table nbt-listview " +
          (source == "videoshow" ? " videoshowview " : " videolistview ") +
          (leadpost ? "lead-post" : "") +
          " table"
        }
        key={item.id}
        data-list-type={this.showListNodeLabel(item)}
        {...SeoSchema({ pagetype: source }).listItem()}
        {...(leadpost
          ? source == "videoshow"
            ? { ...SeoSchema({ pagetype: source }).videoattr().videoObject }
            : ""
          : "")}
      >
        <VideoPlayer
          key={item.eid}
          lead={leadpost}
          apikey={slikeApiKey}
          data-videourl={videourl}
          wrapper="masterVideoPlayer"
          config={config}
          onVideoClick={this.onVideoClickHandler.bind(this, item)}
        >
          <a
            className="table_row"
            href={videourl}
            {...(source == "videoshow"
              ? { ...SeoSchema({ pagetype: source }).videoattr().url }
              : { ...SeoSchema({ pagetype: source }).attr().url })}
          >
            {/* for Schema */ SeoSchema({ pagetype: source }).metaPosition(position)}
            {/* for Schema */ SeoSchema({ pagetype: source }).mainEntry(posturl)}
            {/* for Schema */ SeoSchema({ pagetype: source }).uploadDate(item.dlseo ? item.dlseo : "")}
            {/* for Schema */ SeoSchema({ pagetype: source }).duration(item.duv ? item.duv : "")}
            {/* for Schema */ SeoSchema({ pagetype: source }).headline(item.hl)}
            {/* for Schema */ SeoSchema({ pagetype: source }).dateStatus(item.dlseo, item.luseo)}
            {/* for Schema */ SeoSchema({ pagetype: source }).keywords(keyword)}
            {/* for Schema */ SeoSchema({ pagetype: source }).contentUrl(item.pu ? item.pu : "")}
            {/* Publisher Object */ SeoSchema().publisherObj()}
            {SeoSchema().metaTag({ name: "author", content: "websitename" })}
            {item.vw
              ? SeoSchema().metaTag({
                  name: "interactionCount",
                  content: item.vw,
                })
              : null}
            <span className="table_col img_wrap" data-tag={item.tn == "video" ? item.du : null}>
              {/* For Schema */ leadpost
                ? source == "videoshow"
                  ? SeoSchema({ pagetype: source }).thumbnail(thumbnailURL)
                  : ""
                : ""}
              <ImageCard
                noLazyLoad={noLazyLoad}
                msid={item.imageid ? item.imageid : item.id}
                title={item.seotitle ? item.seotitle : ""}
                size={type == "video_lead" ? "largewidethumb" : leadpost ? "largewidethumb" : "smallwidethumb"}
                schema={type == "video_lead" || leadpost ? true : false}
              />
              {this.showListingIcon(item, leadpost)}
            </span>
            <span className={styles["con_wrap"] + " table_col con_wrap"}>
              {source == "videoshow" && leadpost ? (
                <h1
                  className="text_ellipsis"
                  {...SeoSchema({ pagetype: source }).attr().name}
                  {...SeoSchema({ pagetype: source }).videoattr().name}
                >
                  {item.hl}
                </h1>
              ) : (
                <span
                  className="text_ellipsis"
                  {...SeoSchema({ pagetype: source }).attr().name}
                  {...SeoSchema({ pagetype: source }).videoattr().name}
                >
                  {item.hl}
                </span>
              )}
              {source == "videoshow" && leadpost ? (
                <h2 className="description" {...SeoSchema({ pagetype: source }).videoattr().description}>
                  {item.Story}
                </h2>
              ) : null}
              {leadpost ? (
                <span
                  className={styles["time-caption"] + " time-caption"}
                  data-time={item.lu ? item.lu : item.dl ? item.dl : ""}
                >
                  {item.lu ? item.lu : item.dl ? item.dl : ""}
                </span>
              ) : null}
            </span>
          </a>
        </VideoPlayer>

        {keyword ? <div className="keywords_wrap">{KeyWordCard(keyword)}</div> : null}
      </li>
    );
  }
}

VideoListCard.propTypes = {
  item: PropTypes.object,
};

export default VideoListCard;
