import React, { PureComponent } from "react";
import fetch from "../../../utils/fetch/fetch";
import AnchorLink from "../AnchorLink";

class GnSearchBox extends PureComponent {
  state = {
    autoSuggest: {
      userInput: "",
      data: "",
    },
    userCliked: false,
  };

  itemClickedHandler = () => {
    this.setState({
      autoSuggest: {
        userInput: "",
        data: "",
      },
      userCliked: true,
    });
  };

  searchGagdgets = e => {
    const inputVal = e.currentTarget.value;

    this.setState({
      autoSuggest: {
        userInput: inputVal,
      },
    });

    fetch(`${process.env.API_BASEPOINT}/pwa_search.cms?keyword=${inputVal}&tag=product&q=${inputVal}`)
      .then(item => {
        this.setState({
          autoSuggest: {
            data: item,
          },
          userCliked: false,
        });
      })
      .catch(error => {
        // console.log("error", error);
      });
  };

  render() {
    const { autoSuggest, userCliked } = this.state;
    return (
      <div className="tech-search">
        <input
          type="text"
          value={autoSuggest.userInput}
          name="searchbox"
          onChange={this.searchGagdgets}
          placeholder="search gadgets"
          autoComplete="off"
        />
        {autoSuggest.data && autoSuggest.data.length > 0 && !userCliked && (
          <ul className="search-result">
            <AutoSuggestHTML data={autoSuggest.data} clicked={this.itemClickedHandler} />
          </ul>
        )}
      </div>
    );
  }
}

const ProductInfo = ({ data, clicked }) => {
  const _this = this;
  if (data && Array.isArray(data) && data.length > 0) {
    return data.map(item => {
      return (
        <React.Fragment>
          <li onClick={clicked}>
            <AnchorLink title={item.Product_name} href={item.url}>
              {item.Product_name}
            </AnchorLink>

            <a></a>
          </li>
        </React.Fragment>
      );
    });
  }
  return "";
};

const AutoSuggestHTML = ({ data, clicked }) => {
  if (data && Array.isArray(data) && data.length > 0) {
    return data.map(item => {
      if (item && typeof item == "object") {
        for (const key in item) {
          if (Object.prototype.hasOwnProperty.call(item, key)) {
            const product = item[key];
            return (
              <React.Fragment>
                <li>{product.text}</li>
                <ProductInfo data={product.data} clicked={clicked} />
              </React.Fragment>
            );
          }
        }
      }
      return "";
    });
  }
  return "";
};

export default GnSearchBox;
