import React, { Component } from "react";
import styles from "./css/commonComponents.scss";
import AnchorLink from "./AnchorLink";
const Breadcrumb = props => {
  // let {items} = this.props;

  let { items } = props;
  const { data } = props;
  if (data) {
    items = data;
  }
  return (
    <div className="breadcrumb">
      <ul itemType="https://schema.org/BreadcrumbList" itemScope="rmattval">
        {items.li && Array.isArray(items.li) ? (
          items.li &&
          items.li.map((item, index) => {
            if (item.a) {
              return (
                <li key={index} itemType="https://schema.org/ListItem" itemScope="rmattval" itemProp="itemListElement">
                  <meta itemProp="position" content={index}></meta>
                  <AnchorLink itemProp="item" href={item.a["@href"]}>
                    <span itemProp="name">{item.a.span && item.a.span["#text"] ? item.a.span["#text"] : ""}</span>
                  </AnchorLink>
                </li>
              );
            }
            if (item.span["@content"]) {
              return (
                <li key={index} itemType="https://schema.org/ListItem" itemScope="rmattval" itemProp="itemListElement">
                  <meta itemProp="position" content={index}></meta>
                  <span itemProp="item" content={item.span["@content"]}>
                    <span itemProp="name">
                      {item.span && item.span.span && item.span.span["#text"] ? item.span.span["#text"] : null}
                    </span>
                  </span>
                </li>
              );
            }
            return (
              <li key={index} itemType="https://schema.org/ListItem" itemProp="itemListElement" itemScope="rmattval">
                <meta itemProp="position" content={index}></meta>
                <span itemProp="name">
                  {item.span && item.span.span && item.span.span["#text"]
                    ? item.span.span["#text"]
                    : item.span && item.span.span
                      ? item.span.span
                      : null}
                </span>
              </li>
            );
          })
        ) : (
          <li key="1" itemType="https://schema.org/ListItem" itemScope="rmattval" itemProp="itemListElement">
            <meta itemProp="position" content="1"></meta>
            <span
              content={
                items && items.li && items.li.span && items.li.span.span && items.li.span.span["@content"]
                  ? items.li.span.span["@content"]
                  : ""
              }
            >
              <span itemProp="name">
                {items.li && items.li.span && items.li.span.span && items.li.span.span["#text"]
                  ? items.li.span.span["#text"]
                  : null}
              </span>
            </span>
          </li>
        )}
      </ul>
    </div>
  );
};

export default Breadcrumb;
