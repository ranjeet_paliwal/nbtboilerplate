/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React from "react";
import { Helmet } from "react-helmet";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";

import { _getStaticConfig, filterDescription } from "../../utils/util";
const siteConfig = _getStaticConfig();

export const PageMeta = props => {
  // let {meta} = props;
  const meta = props;
  const description = filterDescription(meta && meta.desc);
  const ogDescription = filterDescription(meta && meta.syn);
  return (
    <React.Fragment>
      {meta ? (
        <Helmet>
          {/* common meta tag */}
          <title>{meta.title}</title>
          <meta name="description" content={description} />
          <meta name="keywords" content={meta.key ? meta.key : process.env.SITE} />
          {meta.canonical && <link rel="canonical" href={meta.canonical} />}
          {meta.alt && meta.alt.map(alt => <link rel="alternate" href={alt.url} hrefLang={alt.lang} />)}
          {process.env.SITE === "mly" && (meta.pagetype === "home" || meta.hyp1 === "gulf") && (
            <link rel="alternate" href="https://malayalam.samayam.com" hrefLang="x-default" />
          )}
          {process.env.SITE === "nbt" && (meta.pagetype === "home" || meta.hyp1 === "gulf") && (
            <link rel="alternate" href="https://navbharattimes.indiatimes.com" hrefLang="x-default" />
          )}
          {process.env.SITE === "mt" && (meta.pagetype === "home" || meta.hyp1 === "us_news") && (
            <link rel="alternate" href="https://maharashtratimes.com" hrefLang="x-default" />
          )}
          {process.env.SITE === "eisamay" &&
            (meta.pagetype === "home" || meta.hyp1 === "bangladesh_news" || meta.hyp1 === "united_states_news") && (
              <link rel="alternate" href="https://eisamay.indiatimes.com" hrefLang="x-default" />
            )}
          {/* {process.env.SITE === 'mly' && meta.hyp1 === "gulf" &&
            <link rel="alternate" href="https://malayalam.samayam.com" hrefLang="ml-in" />}
          {process.env.SITE === 'mly' && meta.hyp1 === "gulf" &&
            <link rel="alternate" href="https://malayalam.samayam.com/gulf" hrefLang="ml-ae" />}
          {process.env.SITE === 'mly' && meta.hyp1 === "gulf" &&
            <link rel="alternate" href="https://malayalam.samayam.com" hrefLang="x-default" />} */}
          {meta.pagetype === "articleshow" || meta.pagetype === "videoshow" || meta.pagetype === "photoshow" ? (
            <meta name="robots" content="max-image-preview:large" />
          ) : null}
          {process.env.SITE === "mly" ? <meta name="geo.region" content="AE" /> : null}
          {meta.pagetype === "articleshow" ? <meta name="news_keywords" content={meta.key} /> : null}
          {meta.pagetype === "home" ? <meta content="Last-Modified" content={meta.modified} /> : null}
          {meta.pagetype === "home" ? <meta name="msvalidate.01" content={meta.bingwebmasters} /> : null}
          {/* <link rel="publisher" title={siteConfig.wapsitename} href={siteConfig.gplus} /> */}
          {/* deeplink pattern */}
          {meta.deeplinkios ? <meta property="al:ios:url" content={meta.deeplinkios} /> : null}
          {meta.deeplinkios ? <meta property="al:ios:app_store_id" content={siteConfig.iospackageid} /> : null}
          {meta.deeplinkios ? <meta property="al:ios:app_name" content={siteConfig.iosapp_name} /> : null}
          {meta.deeplinkandroid ? <meta property="al:android:package" content={siteConfig.androidpackage} /> : null}
          {meta.deeplinkandroid ? <link rel="alternate" href={meta.deeplinkandroid} /> : null}
          {/* nofollow noindex */}
          {meta.noindex ? <meta content="NOINDEX,NOFOLLOW" name="robots" /> : null}
          {meta.sponsormeta ? <meta property="cr:sponsored" /> : ""}
          {meta.amphtml && !meta.noindex ? <link data-exclude="amp" rel="amphtml" href={meta.amphtml} /> : null}

          {/* Next and Prev */}
          {/* {meta.nextItem && meta.nextItem.url != "" ? (
            <link rel="next" href={meta.nextItem.url} />
          ) : null}
          {meta.prevItem && meta.prevItem.url != "" ? (
            <link rel="prev" href={meta.prevItem.url} />
          ) : null} */}

          {/* OG Tags */}
          <meta name="og:type" content={meta.ogtype} />
          <meta property="og:title" content={meta.ogtitle} />
          <meta property="og:url" content={meta.redirectUrl ? meta.redirectUrl : meta.canonical} />
          <meta property="og:site_name" content={siteConfig.wapsitename} />
          <meta property="og:image" content={meta.ogimg ? meta.ogimg : ""} />
          <meta property="og:description" content={ogDescription} />
          {meta.publishcontent ? <meta itemProp="datePublished" content={meta.publishcontent} /> : null}
          {meta.modifiedcontent ? <meta itemProp="dateModified" content={meta.modifiedcontent} /> : null}

          {/* robot tag added for videoshow page LAN-7744 */}
          {meta.pagetype == "videoshow" ? <meta name="robots" content="max-video-preview:-1" /> : null}

          {/* Item Prop */}
          <meta itemProp="name" content={meta.title} />
          <meta itemProp="description" content={description} />
          <meta itemProp="image" content={meta.ogimg ? meta.ogimg : ""} />
          <meta itemProp="url" content={meta.redirectUrl ? meta.redirectUrl : meta.canonical} />
          {meta.publishcontent ? <meta itemProp="datePublished" content={meta.publishcontent} /> : null}
          {meta.modifiedcontent ? <meta itemProp="dateModified" content={meta.modifiedcontent} /> : null}
          <meta itemProp="provider" content={siteConfig.wapsitename} />

          {/* Twitter Card */}
          {/* <meta content="summary" name="twitter:card" /> */}
          <meta name="twitter:domain" content={siteConfig.weburl} />
          <meta name="twitter:title" content={meta.ogtitle} />
          <meta name="twitter:description" content={description} />
          <meta name="twitter:image" content={meta.ogimg ? meta.ogimg : ""} />
          <meta name="twitter:url" content={meta.redirectUrl ? meta.redirectUrl : meta.canonical} />
          <meta content="summary_large_image" name="twitter:card" />
          <meta name="twitter:site" content={siteConfig.twitterHdandler} />
          <meta name="twitter:creator" content={siteConfig.twitterHdandler} />

          {/* Cutom Meta Using in AMP page to paas section name in ctn ad call */}
          <meta
            name="active:section"
            content={meta.pagetype == "home" ? "home" : meta.alternatetitle ? meta.alternatetitle : null}
          />

          {/* {meta.pagetype &&
          (meta.pagetype == "articleshow" ||
            meta.pagetype == "moviereview" ||
            meta.pagetype == "liveblog" ||
            meta.pagetype == "photoshow") ? (
            <script
              type="text/javascript"
              src="https://platform.twitter.com/widgets.js"
              rel="dns-prefetch"
              defer="true"
            />
          ) : null} */}

          {/* {meta.pagetype &&
          (meta.pagetype == "articleshow" ||
            meta.pagetype == "moviereview" ||
            meta.pagetype == "liveblog" ||
            meta.pagetype == "photoshow") ? (
            <script
              type="text/javascript"
              src="https://platform.instagram.com/en_US/embeds.js"
              rel="dns-prefetch"
              defer="true"
            />
          ) : null} */}

          {/* To turn off ads in AMP, insert this meta */
          meta.AdServingRules ? (
            <meta name="adsserving" content={meta.AdServingRules == "Turnoff" ? "false" : "true"} />
          ) : null}
        </Helmet>
      ) : null}
    </React.Fragment>
  );
};

export const SeoSchema = prop => {
  const props = prop || {};
  props.pagetype = props.pagetype ? props.pagetype : "";

  props.url = props.url ? props.url : "";
  props.itemidUrl = props.url ? props.url : "";

  // Schema for Listing Pages
  const schema =
    props.pagetype == "articlelist" ||
    props.pagetype == "photolist" ||
    props.pagetype == "topics" ||
    props.pagetype == "videolist" ||
    props.pagetype == "movieshowlist" ||
    props.pagetype == "movielist"
      ? {
          itemList: {
            itemScope: "itemScope",
            itemType: "http://schema.org/ItemList",
          },
          listItem: {
            itemProp: "itemListElement",
            itemScope: "itemscope",
            itemType: "http://schema.org/ListItem",
          },
          url: { itemProp: "url" },
          name: { itemProp: "name" },
          item: { itemProp: "item" },
        }
      : {};

  // for video show page
  const videoSchema =
    props.pagetype == "videoshow"
      ? {
          videoObject: {
            itemProp: "video",
            itemScope: "itemscope",
            itemType: "http://schema.org/VideoObject",
          },
          name: { itemProp: "name" },
          url: { itemProp: "url" },
          description: { itemProp: "description" },
          duration: { itemProp: "duration" },
        }
      : {};

  const articleSchema =
    props.pagetype == "articleshow"
      ? {
          NewsArticle: {
            itemScope: "itemScope",
            itemType: "http://schema.org/NewsArticle",
          },
          Article: {
            itemScope: "itemScope",
            itemType: "http://schema.org/Article",
          },
          Movie: {
            itemScope: "itemScope",
            itemType: "http://schema.org/Movie",
          },
          pageheadline: { itemProp: "headline" },
          author: { itemProp: "author", itemType: "Thing" },
          person: {
            itemProp: "author",
            itemScope: "itemScope",
            itemType: "http://schema.org/Person",
          },
          articleBody: { itemProp: "articleBody" },
          Recipe: {
            itemScope: "itemScope",
            itemType: "http://schema.org/Recipe",
          },
          PrepTime: { itemProp: "prepTime" },
          CookTime: { itemProp: "cookTime" },
          Nutrition: {
            itemScope: "itemScope",
            itemProp: "nutrition",
            itemType: "http://schema.org/NutritionInformation",
          },
          Calories: { itemProp: "calories" },
          Ingredients: { itemProp: "recipeIngredient" },
          Instructions: { itemProp: "recipeInstructions" },
        }
      : {};

  // Common for show pages
  const showSchema =
    props.pagetype == "vidoeshow" || props.pagetype == "articleshow" || props.pagetype == "moviereview"
      ? {
          show: { show: "yes" },
        }
      : {};

  // Live Blog Schema
  const liveSchema =
    props.pagetype == "liveblog"
      ? {
          liveBlog: {
            itemID: props.itemidUrl,
            itemScope: "itemScope",
            itemType: "http://schema.org/LiveBlogPosting",
          },
          pageheadline: { itemProp: "headline" },
          description: { itemProp: "description" },
          blogpost: {
            itemProp: "liveBlogUpdate",
            itemScope: "itemscope",
            itemType: "http://schema.org/BlogPosting",
          },
          articleBody: { itemProp: "articleBody" },
          modified: { itemProp: "dateModified" },
          entity: { itemProp: "mainEntityOfPage" },
        }
      : {};

  // FixMe: for Test only need to make this code dynamic

  const seo = {};

  // Set to 0 where schema not required. Checked on the basis of schema object length
  const flag = Object.keys(schema).length == 0 ? 0 : 1;
  const videoflag = Object.keys(videoSchema).length == 0 ? 0 : 1;
  const showflag = Object.keys(showSchema).length == 0 ? 0 : 1;
  const articlflag = Object.keys(articleSchema).length == 0 ? 0 : 1;
  const liveflag = Object.keys(liveSchema).length == 0 ? 0 : 1;

  seo.commonSchema = obj => {
    const logoschema = siteConfig.logo_schema;

    const commonSchema = `{"@context": "http://schema.org","@graph":[{"@type": "Organization", "name": "${siteConfig.wapsitename}", "url": "${siteConfig.mweburl}", "logo":{"@type":"ImageObject", "url": "${logoschema}", "width":600, "height":60}, "sameAs": ["${siteConfig.fb}","${siteConfig.twitter}", "${siteConfig.gplus}"]}]}`;
    const schemaUrl = `${siteConfig.mweburl}/search?q={search_term_string}`;
    const webSiteSchema = `{"@context": "http://schema.org","@graph":[{"@type": "Website", "name": "${siteConfig.wapsitename}", "url": "${siteConfig.mweburl}"}]}`;
    const webSiteSchemaRegional = `{"@context": "http://schema.org","@graph":[{"@type": "Website", "name": "${siteConfig.wapsiteregionalname}", "url": "${siteConfig.mweburl}"}]}`;
    const websiteSchemaAction = `{"@context": "http://schema.org","@graph":[{"@type": "Website", "name": "${siteConfig.wapsitename}", "url": "${siteConfig.mweburl}", "potentialAction":{"@type":"SearchAction", "target" :{"@type": "EntryPoint", "urlTemplate": "${schemaUrl}"} , "query-input" : {"@type": "PropertyValueSpecification" , "valueRequired" : "http://schema.org/True" , "valueName" : "search_term_string"}}}]}`;
    return (
      <React.Fragment>
        <script type="application/ld+json" data-type="ampElement" dangerouslySetInnerHTML={{ __html: commonSchema }} />
        <script type="application/ld+json" data-type="ampElement" dangerouslySetInnerHTML={{ __html: webSiteSchema }} />
        <script
          type="application/ld+json"
          data-type="ampElement"
          dangerouslySetInnerHTML={{ __html: webSiteSchemaRegional }}
        />
        <script
          type="application/ld+json"
          data-type="ampElement"
          dangerouslySetInnerHTML={{ __html: websiteSchemaAction }}
        />
      </React.Fragment>
    );
  };

  seo.publisherObj = obj => (
    <div itemProp="publisher" itemScope="itemScope" itemType="https://schema.org/Organization">
      <meta itemProp="name" content={siteConfig.wapsitename} />
      <meta itemProp="url" content={siteConfig.weburl} />
      <div itemType="https://schema.org/ImageObject" itemScope="itemScope" itemProp="logo">
        <meta itemProp="url" content={siteConfig.logo_schema} />
        <meta content="600" itemProp="width" />
        <meta content="60" itemProp="height" />
      </div>
    </div>
  );
  // job schema
  seo.job = data => (
    // data = (data);
    <script type="application/ld+json" data-type="ampElement" dangerouslySetInnerHTML={{ __html: data }} />
  );
  // faq schema
  // seo.schema = data => (
  //   //data = JSON.stringify(data);
  //   <script type="application/ld+json" data-type="ampElement" dangerouslySetInnerHTML={{ __html: data }} />
  // );

  seo.factcheck = (data, date, headline, ContainerDescription) => (
    <div itemScope="" itemType="http://schema.org/ClaimReview">
      {date != undefined ? <meta itemProp="datePublished" content={date} /> : ""}
      {data.canonical != undefined ? <meta itemProp="url" content={data.canonical} /> : ""}
      <span itemProp="author" itemScope="1" itemType="http://schema.org/Organization">
        {data.factauthor != undefined ? <meta itemProp="name" content={data.factauthor} /> : ""}
      </span>
      {headline != undefined ? (
        <meta itemProp="claimReviewed" content={ContainerDescription != undefined ? ContainerDescription : headline} />
      ) : (
        ""
      )}
      <span itemProp="reviewRating" itemScope="1" itemType="http://schema.org/Rating">
        {data.ratingvalue != undefined ? <meta itemProp="ratingValue" content={data.ratingvalue} /> : ""}
        <meta itemProp="bestRating" content="5" />
        {data.factrating != undefined ? <meta itemProp="alternateName" content={data.factrating} /> : ""}
      </span>
      <span itemProp="itemReviewed" itemScope="1" itemType="http://schema.org/CreativeWork">
        <span itemProp="author" itemScope="1" itemType="http://schema.org/Person">
          {data.claimer != undefined ? <meta itemProp="name" content={data.claimer} /> : ""}
          <link itemProp="sameAs" href={data.claimerurl} />
        </span>
        {date != undefined ? <meta itemProp="datePublished" content={date} /> : ""}
      </span>
    </div>
  );

  seo.itemList = () => (flag ? { ...schema.itemList } : "");

  seo.listItem = () => (flag ? { ...schema.listItem } : "");

  seo.numberofitems = count => (flag ? <meta itemProp="numberOfItems" content={count} /> : "");

  seo.metaUrl = url => (flag || articlflag || liveflag ? <meta itemProp="url" content={url} /> : "");

  seo.metaPosition = position => (flag && position ? <meta itemProp="position" content={position} /> : "");

  seo.attr = () => (flag ? schema : articlflag ? articleSchema : liveflag ? liveSchema : "");

  seo.videoattr = () => (videoflag ? videoSchema : "");

  seo.mainEntry = url => <meta itemProp="mainEntityOfPage" content={url} />;

  seo.thumbnail = src => (videoflag ? <meta itemProp="thumbnailUrl" content={src} /> : "");
  seo.contentUrl = contenturl =>
    videoflag && contenturl != "" ? <meta itemProp="contenturl" content={contenturl} /> : null;
  seo.embedUrl = embed => (videoflag ? <meta itemProp="embedUrl" content={embed} /> : "");
  seo.name = name => (videoflag ? <meta itemProp="name" content={name} /> : "");

  seo.uploadDate = date => (videoflag ? <meta itemProp="uploadDate" content={date} /> : "");

  seo.duration = duration => (videoflag && duration != "" ? <meta itemProp="duration" content={duration} /> : null);

  seo.dateStatus = (published, modified) =>
    videoflag || showflag ? (
      <React.Fragment>
        {published != undefined ? <meta itemProp="datePublished" content={published} /> : ""}
        {modified != undefined ? <meta itemProp="dateModified" content={modified} /> : ""}
      </React.Fragment>
    ) : (
      ""
    );

  seo.headline = headline => (videoflag ? <meta itemProp="headline" content={headline} /> : "");

  seo.artSection = sec => <meta itemProp="articleSection" content={sec} />;
  seo.disambiguatingDescription = syn => <meta itemProp="disambiguatingDescription" content={syn} />;

  seo.artdesc = syn => <meta itemProp="description" content={syn} />;

  seo.language = () => <meta itemProp="inLanguage" content={siteConfig.languagemeta} />;

  seo.alternativeHeadline = headline => (headline ? <meta itemProp="alternativeHeadline" content={headline} /> : "");
  seo.alternateName = alternatetitle =>
    alternatetitle ? <meta itemProp="alternateName" content={alternatetitle} /> : "";

  seo.keywords = keywords => (keywords ? <meta itemProp="keywords" content={keywords} /> : "");

  seo.metaTag = props => {
    const { name, content } = props;
    let contentval = content ? content.toString() : "";
    contentval =
      contentval.indexOf(".com") > -1
        ? contentval
        : contentval == "websitename"
        ? siteConfig.wapsitename
        : contentval.substring(0, 110);
    return name && content ? <meta itemProp={name} content={contentval} /> : null;
  };

  return seo;
};

export default { PageMeta, SeoSchema };
