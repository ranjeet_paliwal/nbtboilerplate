import React from "react";

import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import styles from "./css/ArticleShow.scss";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
const siteconfig = siteConfig;

const WebTitleCard = (props, pageType = "") => {
  const it = props;
  const getSeodescription = seodescription => {
    if (typeof seodescription === "string") return <div dangerouslySetInnerHTML={{ __html: seodescription }} />;
    if (typeof seodescription === "object") {
      if (Array.isArray(seodescription.a) && Array.isArray(seodescription["#text"])) {
        const seodata = [];
        for (let i = 0; i < seodescription["#text"].length; i++) {
          seodata.push(seodescription["#text"][i]);
          seodata.push(seodescription.a[i]);
        }

        return seodata.map((item, index) =>
          typeof item === "string" ? (
            item
          ) : typeof item === "object" ? (
            <AnchorLink hrefData={{ override: item["@href"] }}>{item["#text"]}</AnchorLink>
          ) : null,
        );
      }
      if (seodescription) {
        for (const key in seodescription) {
          if (key == "a" && typeof seodescription[key] === "object") {
            return (
              <AnchorLink hrefData={{ override: seodescription[key]["@href"] }}>
                {seodescription[key]["#text"]}
              </AnchorLink>
            );
          }
        }
      }
    } else {
      return null;
    }
  };

  return it ? (
    <ErrorBoundary>
      <div className={`${styles.webtitle} webtitle`}>
        {it.webtitle ? (
          <span>
            <b>Web Title : </b>
            {it.webtitle}
            <br />
            <AnchorLink hrefData={{ override: siteconfig.mweburl }}>
              {siteconfig.language} {"News "}
            </AnchorLink>
            from {siteconfig.wapsitename}, TIL Network
          </span>
        ) : null}
        {pageType !== "photoshow" && it.seodescription ? (
          <span>
            <br />
            {getSeodescription(it.seodescription)}
          </span>
        ) : null}
      </div>
    </ErrorBoundary>
  ) : null;
};

export default WebTitleCard;
