import React from "react";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import styles from "./css/ArticleShow.scss";
import AnchorLink from "./AnchorLink";
import ImageCard from "./ImageCard";
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const siteconfig = siteConfig;

const GadgetFeaturecompare = (props, pid, id, rsname) => {
  let item = props;
  let kpid = pid ? pid : 245;
  let unique = id ? id : 46;
  let name = rsname ? rsname : "";
  return (
    <div className="detailed-specification">
      {Object.keys(item).map((keyparent, index) => {
        return (
          <div className="gn_table_layout">
            <input
              className="btn-showhide"
              id={`${keyparent}${kpid}${index}${unique}`}
              type="checkbox"
              style={{ display: "none" }}
              defaultChecked={index < 1 ? true : false}
            ></input>
            <label
              className="spec"
              htmlFor={`${keyparent}${kpid}${index}${unique}`}
            >
              {siteconfig.locale.tech.fullspecification}
            </label>
            <table className="gn_table_layout tableContainer">
              <tbody>
                {Object.keys(item[keyparent]).map((speckey, index) => {
                  return speckey !== "regional" ?
                    (<React.Fragment>
                      <tr>
                        <td>{item[keyparent][speckey].key}</td>
                        <td>
                          {typeof item[keyparent] &&
                            item[keyparent][speckey] != ""
                            ? item[keyparent][speckey].value
                            : "-"}
                        </td>
                      </tr>
                    </React.Fragment>
                    ) : null
                })}
              </tbody>
            </table>
          </div>
        );
      })}
    </div>
  );
};
export default GadgetFeaturecompare;
