import React from "react";
import Loadable from "react-loadable";
import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/ListHorizontalCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";
import { SeoSchema } from "../../components/common/PageMeta"; //For Page SEO/Head Part
import { AnalyticsGA, Analytics_fbpixel } from "./../lib/analytics/index";
import { createConfig } from "./../../modules/videoplayer/utils";

import { _getStaticConfig, LoadingComponent } from "../../utils/util";
const siteConfig = _getStaticConfig();

const slikeApiKey = siteConfig.slike.apikey;
const slikeAdCodes = siteConfig.slikeAdCodes;

const VideoPlayer = Loadable({
  loader: () => import("./../../modules/videoplayer/player"),
  LoadingComponent,
});

function showListingIcon(item) {
  let icon =
    item.tn == "videolist" || item.tn == "video"
      ? "video_icon"
      : item.tn == "photolist" || item.tn == "photo"
      ? "photo_icon"
      : "";
  if (icon == "") return null;
  return <span className={"type_icon " + icon}></span>;
}

// function generateUrl(item){
//   let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
//   let url = '/'+ (item.seolocation ? (item.seolocation) + '/' : "" ) + templateName + '/' + item.id + '.cms';
//   return url;
// }

function generateUrl(item) {
  if (item.override) {
    //Changing domain from absolute to relative
    return item.override.replace(/^.*\/\/[^\/]+/, "");
  } else {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return url;
  }
}

function onVideoClickHandler(item, event) {
  let _this = this;
  // event.currentTarget.previousSibling.innerText
  let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
  if (videourl) {
    try {
      //To maintain previous url
      window.history.pushState({}, "", videourl);
      AnalyticsGA.pageview(location.origin + generateUrl(item));
    } catch (ex) {
      console.log("Exception in history: ", ex);
    }
  }
}

const ListHorizontalCard = props => {
  let { item, type, tileWidth, listCountInc, pagetype, className } = props;
  tileWidth = tileWidth ? tileWidth : siteConfig.locale.default_horizontal_tile_width;
  let sliderWidth =
    typeof item.items != "undefined" && item.items.length > 0
      ? item.items.length * tileWidth + (item.items.length - 1) * 10 + "px"
      : "";
  //let sliderHeading = type == "rlvideo" ? siteConfig.locale.related_videos : item.hl ? item.hl : item.tn == "videolist" ? siteConfig.locale.videos : item.tn == "photolist" ? siteConfig.locale.photo : "";
  let sliderHeading =
    type == "rlvideo"
      ? siteConfig.locale.related_videos
      : item.hl
      ? item.hl
      : item.tn == "videolist"
      ? ""
      : item.tn == "photolist"
      ? ""
      : "";

  return (
    <li
      className={
        (className ? className + " " : "") +
        styles["nbt-horizontalView"] +
        " nbt-horizontalView animated fadeIn " +
        (item.tn == "videolist" || item.tn == "video" ? "video" : item.tn == "photolist" ? "photo" : "") +
        (item._class === "relarticle" ? item._class : "")
      }
      key={item.id}
    >
      {sliderHeading && sliderHeading != "" ? (
        <span className={styles["article-text"] + " article-text"}>{sliderHeading}</span>
      ) : null}
      <div className="scroll_content">
        <ul className="nbt-list" style={{ width: sliderWidth }}>
          {typeof item.items != "undefined" && item.items.length > 0
            ? item.items.map((item, index) => {
                return item.tn != "ad" ? (
                  <li
                    key={index}
                    data-videourl={generateUrl(item)}
                    className="table nbt-listview"
                    {...SeoSchema({ pagetype: pagetype }).listItem()}
                  >
                    {item.tn == "video" ? (
                      <VideoPlayer
                        data-videourl={generateUrl(item)}
                        // onVideoClick={onVideoClickHandler.bind(this, item)}
                        // key={item.eid}
                        // apikey={slikeApiKey}
                        // inline={true}
                        autoDock
                        // wrapper="masterVideoPlayer"
                        config={createConfig(item, "", siteConfig, "home")}
                        imageid={item.imageid}
                        videoMsid={item.id}
                      >
                        <AnchorLink
                          fireClickPixel={item.clkurl ? item.clkurl : null}
                          itemProp="url"
                          href={generateUrl(item)}
                        >
                          <span className="table_col img_wrap" data-tag={item.tn == "video" ? item.du : null}>
                            <ImageCard
                              msid={item.imageid ? item.imageid : item.id}
                              title={item.seotitle ? item.seotitle : ""}
                              imgsize={item.imgsize ? item.imgsize : ""}
                              size={
                                item.tn == "videolist" || item.tn == "video"
                                  ? "smallwidethumb"
                                  : item.tn == "photolist" || item.tn == "photo"
                                  ? "midthumb"
                                  : "smallthumb"
                              }
                            />
                            {showListingIcon(item)}
                          </span>
                          <span className={styles["con_wrap"] + " table_col con_wrap"}>
                            <span className="text_ellipsis" {...SeoSchema({ pagetype: pagetype }).attr().name}>
                              {item.hl ? item.hl : ""}
                            </span>
                            {/* for Schema only */ SeoSchema({
                              pagetype: pagetype,
                            }).metaPosition(listCountInc ? listCountInc() : "")}
                          </span>
                        </AnchorLink>
                      </VideoPlayer>
                    ) : (
                      <AnchorLink
                        itemProp="url"
                        fireClickPixel={item.clkurl ? item.clkurl : null}
                        hrefData={
                          item.override
                            ? { override: item.override }
                            : {
                                seo: item.seolocation,
                                tn: item.tn,
                                id: item.tn == "photo" ? item.imageid : item.id,
                              }
                        }
                        className="table_row"
                        {...SeoSchema({ pagetype: pagetype }).attr().item}
                      >
                        <span className="table_col img_wrap" data-tag={item.tn == "video" ? item.du : null}>
                          <ImageCard
                            msid={item.imageid ? item.imageid : item.id}
                            title={item.seotitle ? item.seotitle : ""}
                            imgsize={item.imgsize ? item.imgsize : ""}
                            size={
                              item.tn == "videolist" || item.tn == "video"
                                ? "smallwidethumb"
                                : item.tn == "photolist" || item.tn == "photo"
                                ? "midthumb"
                                : "smallthumb"
                            }
                          />
                          {showListingIcon(item)}
                        </span>
                        <span className={styles["con_wrap"] + " table_col con_wrap"}>
                          <span className="text_ellipsis" {...SeoSchema({ pagetype: pagetype }).attr().name}>
                            {item.hl ? item.hl : ""}
                          </span>
                          {/* for Schema only */ SeoSchema({
                            pagetype: pagetype,
                          }).metaPosition(listCountInc ? listCountInc() : "")}
                        </span>
                      </AnchorLink>
                    )}
                  </li>
                ) : null;
              })
            : null}
        </ul>
      </div>
    </li>
  );
};

ListHorizontalCard.propTypes = {
  item: PropTypes.object,
};

export default ListHorizontalCard;
