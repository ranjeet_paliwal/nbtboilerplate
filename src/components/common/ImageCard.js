/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import React from "react";

import { _getStaticConfig } from "../../utils/util";

// import IronImage from './IronImage';
// import LazyImage from './LazyImage';
import LazyIntersection from "./LazyIntersection";
const siteConfig = _getStaticConfig();

const setProp = prop => {
  const defaultOptions = {
    msid: siteConfig.imageconfig.thumbid,
    title: siteConfig.wapsitename,
    alt: siteConfig.wapsitename,
    size: "largethumb",
  };

  const Props = prop || {};

  Props.msid = Props.msid ? Props.msid : defaultOptions.msid;
  Props.imgname = defaultOptions.title.toLowerCase().replace(" ", "-");
  Props.title = Props.title ? Props.title : defaultOptions.title.toLowerCase().replace(" ", "-");
  Props.alt = Props.alt ? Props.alt : defaultOptions.alt.toLowerCase();
  Props.size = Props.size ? Props.size : defaultOptions.size;
  Props.imgsize = Props.imgsize ? `imgsize-${Props.imgsize},` : "";
  Props.ampsize =
    Props.pagetype && Props.pagetype == "articleshow"
      ? "amplargethumbarticle"
      : Props.size && (Props.size == "largewidethumb" || Props.size == "largethumb")
      ? "amplargethumb"
      : defaultOptions.size;
  const imgsrc =
    Props.type && Props.src && Props.type == "absoluteImgSrc"
      ? Props.src
      : `${siteConfig.imageconfig.imagedomain + Props.msid},${Props.imgsize}${
          Props.imgver ? `imgsize-${Props.imgver},` : ""
        }${siteConfig.imageconfig[Props.size]}/${Props.imgname}.jpg`;
  const ampimgsrc =
    Props.type && Props.src && Props.type == "absoluteImgSrc"
      ? Props.src
      : `${siteConfig.imageconfig.imagedomain + Props.msid},${siteConfig.imageconfig[Props.ampsize]}/${
          Props.imgname
        }.jpg`;
  const img = {
    src: imgsrc,
    ampsrc: ampimgsrc,
    title: Props.title,
    size: Props.size,
    ampsize: Props.ampsize,
    alt: Props.alt,
  };

  return img;
};

const getplaceholderSrc = (size = "largethumb") => {
  size = typeof size === "string" && size.indexOf("wide") > 0 ? "largewidethumb" : "largethumb";
  const title = siteConfig.wapsitename;
  const msid = siteConfig.imageconfig.thumbid;
  const imgsize = siteConfig.imageconfig[size];

  return `${siteConfig.imageconfig.imagedomain + msid},${imgsize}/${title}.jpg`;
};

const ImageCard = props => {
  const img = setProp(props);
  const imgWidth = props.width || "100%";
  const imgobj = {};
  const placeholderSrc = getplaceholderSrc(props.size);
  const className = props.className ? props.className : null;
  const imgsize = siteConfig.imageconfig[img.size];
  const ampimgsize = siteConfig.imageconfig[img.ampsize];
  const { noLazyLoad } = props;
  // Return image Tag
  (imgobj.img = () => (
    <React.Fragment>
      {noLazyLoad ? (
        <img
          width={imgWidth}
          className={className}
          src={img.src}
          alt={img.alt}
          title={img.title}
          placeholder={placeholderSrc}
        />
      ) : (
        <LazyIntersection
          width={imgWidth}
          className={className}
          src={img.src}
          datasrc={img.src}
          alt={img.alt}
          title={img.title}
          placeholder={placeholderSrc}
        />
      )}
      {/* <LazyImage width="100%" className={className} src={img.src} alt={img.title} placeholder={placeholderSrc} /> */}
      {/* <img width="100%" className={className} data-src={img.src} alt={img.title} placeholder={placeholderSrc} /> */}
      {/* {(props.schema == true) ? <meta itemProp="image" content={img.src}/> : ''} */}
      {props.schema == true ? (
        imgsize.indexOf("height") > -1 ? (
          <span
            itemProp="image"
            itemScope
            itemType={imgsize.indexOf("height") > -1 ? "https://schema.org/ImageObject" : ""}
          >
            <meta itemProp="url" content={img.ampsrc} />
            {/* <meta itemProp="width" content={ampimgsize.substring(6, ampimgsize.indexOf(","))} />
            <meta
              itemProp="height"
              content={ampimgsize.substring(ampimgsize.indexOf(",") + 8, ampimgsize.indexOf(",r"))}
            /> */}
            <meta content="1200" itemProp="width" />
            <meta content="900" itemProp="height" />
          </span>
        ) : (
          ""
        )
      ) : (
        ""
      )}
    </React.Fragment>
  )),
    // Return image URL only for Meta data Purpose
    (imgobj.url = () => img.src);

  return imgobj;
};

export default ImageCard;
