import React, { Component } from "react";
// import { Link } from 'react-router'
import PropTypes from "prop-types";
// import { capitalizeFirstLetter, calculateTime } from './../../utils/util';
import styles from "./css/PhotoMazzaShowCard.scss";
import "./css/desktop/PhotoMazzaShowCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import SocialShare from "./SocialShare";
import { AnalyticsGA } from "../lib/analytics/index";
import { _getStaticConfig, isMobilePlatform } from "../../utils/util";
import { _isCSR, _deferredDeeplink } from "../../utils/util";
import AmazonWidget from "./../../modules/amazonwidget/AmazonWidget";
import FakeNewsListCard from "./FakeCards/FakeNewsListCard";
import FakeHorizontalListCard from "./FakeCards/FakeHorizontalListCard";
const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;

class PhotoMazzaShowSubCard extends Component {
  constructor(props) {
    super(props);
    this.config = {
      photoshowWidth: 670,
    };
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.props.item.id != nextProps.item.id) {
  //     return true;
  //   }
  //   return false;
  // }

  getShareDetail() {
    const { item, openCommentsPopup } = this.props;
    const url = item.wu && item.wu != "" ? item.wu : `/photomazza/photoshow/${item.id}.cms`;
    const short_url = item && item.m ? item.m : null;
    return { title: item.hl, url, short_url, text: item.cap, openCommentsPopup, msid: item.id };
  }

  /* readMore() {
    if (this.caption.classList.contains('more')) {
      this.caption.classList.remove('more');
      //this.read.innerText = locale.read_more;
      this.read.classList.add('expand');
      this.read.classList.remove('collapse');
    } else {
      this.caption.classList.add('more');
      //this.read.innerText = locale.read_less;
      this.read.classList.remove('expand');
      this.read.classList.add('collapse');
    }
  } */

  render() {
    const {
      item,
      index,
      count,
      isInlineContent,
      url,
      picid,
      openCommentsPopup,
      photoshow_app_install,
      showAppInstall,
      parentid,
      isAmpPage,
    } = this.props;
    const imgsrc =
      item.type && item.src && item.type == "absoluteImgSrc"
        ? item.src
        : `${siteConfig.imageconfig.imagedomain + item.id},${siteConfig.imageconfig.bigimage}/${item.imgtitle}.jpg`;

    const calcHeight = isMobilePlatform()
      ? item.imgratio * 100 + "vw"
      : item.imgratio * this.config.photoshowWidth + "px";

    return (
      <li
        itemType="http://schema.org/ImageObject"
        itemProp="associatedMedia"
        itemScope="1"
        data-url={url}
        className={`${styles.photo_card} photo_card card_inview`}
        id={item.id}
        gatracked={index == 0 || picid == item.id ? "true" : null}
        // data-active={picid == item.id ? true : null}
      >
        <div
          className="place_holder"
          // style={{ height: item.imgratio && !isAmpPage && isMobilePlatform() ? item.imgratio * 100 + "vw" : "auto" }}
          style={{ height: item.imgratio && !isAmpPage ? calcHeight : "auto" }}
        >
          {!isInlineContent ? <SocialShare sharedata={this.getShareDetail.bind(this)} /> : null}
          <meta itemProp="thumbnailUrl" content={imgsrc} />
          <meta itemProp="contentUrl" content={imgsrc} />
          <ImageCard
            noLazyLoad={index == 0}
            msid={item.id}
            title={item.imgtitle == "" || item.imgtitle == "-" ? item.hl : item.imgtitle}
            size="bigimage"
            alt={item.hl}
            pagetype="photoshow"
            // style={{ height: item.imgratio && !isAmpPage && isMobilePlatform() ? item.imgratio * 100 + "vw" : "auto" }}
            style={{
              height: item.imgratio && !isAmpPage ? calcHeight : "auto",
            }}
          />
        </div>
        {!isInlineContent ? (
          <div className="photo_des">
            <div className="pagination">
              <span>
                {item.no}
                <b>/{count}</b>
              </span>
            </div>
            <div className="photo_con">
              <h2 itemProp="name">{item.hl}</h2>
              <div className="table">
                <div className="table_row">
                  <div className="table_col">
                    <span
                      itemProp="caption description"
                      className="caption text_ellipsis"
                      ref={div => {
                        this.caption = div;
                      }}
                      dangerouslySetInnerHTML={{ __html: item.cap }}
                    />
                  </div>
                  <div className="table_col">
                    <span className="readmore expand">&nbsp;{/* locale.read_more */}</span>
                  </div>
                </div>
              </div>
            </div>
            {!isMobilePlatform() ? (
              <span className="photo_comments">
                <b onClick={openCommentsPopup} />
              </span>
            ) : (
              ""
            )}
          </div>
        ) : (
          <React.Fragment>
            <h4>
              <span className="text_ellipsis">{item.hl}</span>
            </h4>
            <div className="enable-read-more">
              <div className="first_col">
                <span className="caption text_ellipsis" dangerouslySetInnerHTML={{ __html: item.cap }} />
              </div>
              <div className="second_col">
                <span className="readmore expand">&nbsp;</span>
              </div>
            </div>
          </React.Fragment>
        )}
        {photoshow_app_install &&
        photoshow_app_install.active === true &&
        photoshow_app_install.show_slides == index ? (
          <span
            className="disable-photo-card"
            // onClick={() => {
            //   AnalyticsGA.event(siteConfig.photoshow_app_install.ga_object);
            // }}
          >
            <span className="middle">
              <b>{locale.photoshow_app_install.read_more}</b>
              <a
                target="_blank"
                href={_deferredDeeplink("photoshow", siteConfig.appdeeplink, parentid)}
                className="btn_appdownload"
              >
                {locale.photoshow_app_install.download}
              </a>
            </span>
          </span>
        ) : null}
      </li>
    );
  }
}

PhotoMazzaShowSubCard.propTypes = {
  item: PropTypes.object,
};

export default PhotoMazzaShowSubCard;
