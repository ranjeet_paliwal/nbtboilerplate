import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { _isCSR } from "./../../utils/util";

class Timer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      timer: "",
      setIntervalFn: null
    };
  }

  componentDidMount() {
    let _this = this;
    if (_isCSR()) {
      _this.calculateTime(_this.props.time);
    }
  }

  calculateTime(time) {
    let _this = this;
    _this.state.setIntervalFn = setInterval(function() {
      var t = time;
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      var hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((t % (1000 * 60)) / 1000);
      _this.setState({
        timer: days + "d " + hours + "h " + minutes + "m " + seconds + "s "
      });

      time = time - 1000;

      if (t < 0) {
        clearInterval(_this.state.setIntervalFn);
        //document.getElementById("counter").innerHTML = "EXPIRED";
        _this.setState({ timer: "" });
      }
    }, 1000);
  }

  componentWillUnmount() {
    let _this = this;
    clearInterval(_this.state.setIntervalFn);
  }

  render() {
    return <span>{this.state.timer}</span>;
  }
}

export default Timer;
