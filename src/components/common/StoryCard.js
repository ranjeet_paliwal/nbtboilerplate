import React, { Component } from "react";
import Parser from "html-react-parser";
import { connect } from "react-redux";

import WebTitleCard from "./WebTitleCard";
import SummaryCard from "./SummaryCard";

import KeyWordCard from "./KeyWordCard";
import AdCard from "./AdCard";
import HighlightCard from "./HighlightCard";
import TabContainer from "./TabContainer";

import ImageCard from "../common/ImageCard/ImageCard";

import { parserOptions } from "./../lib/htmlReactParser/index";
import { AnalyticsGA } from "./../../components/lib/analytics/index";

import { SeoSchema } from "./../../components/common/PageMeta";
import AnchorLink from "./AnchorLink";
import TopComment from "../../components/desktop/TopComments";
import CommentBox from "../../components/common/CommentBox";
import SocialSubscribeCard from "../../components/common/SocialMediaChannels/SocialSubscribeCard";

import {
  _set_cookie,
  _deferredDeeplink,
  _isCSR,
  _getUrlOfCategory,
  _getTeamLogo,
  isMobilePlatform,
  isTechSite,
  LoadingComponent,
  shouldTPRender,
  isTechPage,
  filterDescription,
  disableBodyScroll,
  enableBodyScroll,
} from "../../utils/util";

import GadgetFeaturecompare from "./../../components/common/GadgetFeaturecompare";
import GadgetRelated from "./../../components/common/GadgetRelated";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
// import fetch from 'utils/fetch/fetch';
import { _getStaticConfig } from "../../utils/util";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import Slider from "../desktop/Slider/index";
import SocialShare from "./SocialShare";
import YSubscribeCard from "./YSubscribeCard";
// import CommentsSection from "../desktop/CommentsSection";
import GridSectionMaker from "./ListingCards/GridSectionMaker";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import Breadcrumb from "./Breadcrumb";
import TPwidget from "./TimesPoints/TPWidgets/TPWidgets";
import GadgetsVariants from "../common/Gadgets/GadgetVariants";
import fetch from "../../utils/fetch/fetch";
const siteConfig = _getStaticConfig();
import SvgIcon from "./SvgIcon";
import Loadable from "react-loadable";
import AmazonWidget from "../common/AmazonWidget/AmazonWidget";
import ASLeadImage from "./ASLeadImage";

const taswidgetNBTPath = "https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas_new.cms";
const taswidgetMTPath = "https://maharashtratimes.com/pwafeeds/amazon_wdt_tas_new.cms";

const NewsLetter = Loadable({
  loader: () => import("../common/Newsletter"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("../common/Newsletter"),
});

const MoviePoster = Loadable({
  loader: () => import("./MoviePoster"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("./MoviePoster"),
});

const Userrating = Loadable({
  loader: () => import("./MoviePoster").then(comp => comp.Userrating),
  LoadingComponent,
});

class StoryCard extends Component {
  constructor(props) {
    super(props);
    this.config = {
      newLineCount: 0,
    };
    this.data = {};
    this.state = {
      relatedgadgets: {},
      showCommentBox: false,
      isScroll: false,
      hasMounted: false,
    };
  }

  componentDidMount() {
    // this.handleSocialApis();
    let { item } = this.props;
    this.setState({ hasMounted: true });

    // ShouldRenderTP executes every time when render executes. And some times it results, TP widget would not render.
    this.config.shouldRenderTimespoint = typeof shouldTPRender == "function" && shouldTPRender();

    if (item.ismarkreview && item.gadgetinfo && item.brandname) {
      fetch(
        `${process.env.API_BASEPOINT}/sc_relatedgadget.cms?type=${item.gadgetinfo.category}&brandname=${item.brandname}&feedtype=sjson`,
      )
        .then(response => {
          this.setState({ relatedgadgets: response });
        })
        .catch(() => null);
    }

    this.scrollRef = this.handleScroll.bind(this, item);
    if (_isCSR()) {
      window.addEventListener("scroll", this.scrollRef);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { showCommentBox } = this.state;
    if (
      nextProps.advertorialData != this.props.advertorialData ||
      nextProps.advertorialNews != this.props.advertorialNews ||
      nextProps.rhsCSRVideoData != this.props.rhsCSRVideoData ||
      showCommentBox !== nextState.showCommentBox ||
      nextProps.superHitWidgetData != this.props.superHitWidgetData
    ) {
      return true;
    }
    // return a boolean value
    return false;
  }

  handleSocialApis = () => {
    let _this = this;
    if (typeof window != "undefined") {
      window.setTimeout(() => {
        try {
          _this.handleFBembed();
          if (window.instgrm) {
            window.instgrm.Embeds.process();
          }
        } catch (ex) {
          console.log("ERROR PROCESSING EMBEDS", ex);
        }
      }, 2000);
    }
  };

  handleScroll(item) {
    this.handleSocialApis();

    if (!this.state.isScroll) {
      this.setState({ isScroll: true });
    }

    if (item) {
      const iframeEle = document.querySelector(`#wdt_amazon${item.id}`);
      const frameDataSrc = iframeEle && iframeEle.getAttribute("data-src");

      if (frameDataSrc) {
        iframeEle.setAttribute("src", frameDataSrc);
        iframeEle.removeAttribute("data-src");
      }

      this.handleYoutubeEmbeds();
    }

    const twitter = document.createElement("script");
    twitter.src = "https://platform.twitter.com/widgets.js";
    twitter.defer = true;

    document.head.appendChild(twitter);

    this.unbindScroll();
  }

  handleYoutubeEmbeds() {
    const arrElements = document.querySelectorAll('[data-attr="youtube-embed"]');
    arrElements.forEach(ele => {
      const frameSrc = ele.getAttribute("data-src");

      if (frameSrc) {
        ele.setAttribute("src", frameSrc);
        ele.removeAttribute("data-src");
      }
    });
  }

  unbindScroll() {
    window.removeEventListener("scroll", this.scrollRef);
  }

  // renderStory = item => {
  //   return Parser(item.Story, {
  //     replace: parserOptions.replace.bind(this, item, query),
  //   });
  // };

  //Scroll Page to next article
  // #id not working with react virtual dom.
  scrollInView = (e, msid) => {
    e.preventDefault();
    var nextArtPos = document.getElementById(msid) ? document.getElementById(msid).offsetTop : null;
    if (nextArtPos != null) {
      window.scrollTo(0, nextArtPos);
    }
  };

  handleFBembed = () => {
    //fbscript if present collect all fb-post and render
    if (document.getElementById("fbScript") && FB) {
      let emframe = document.querySelectorAll(".fb-post");
      emframe.forEach(function(item, index) {
        item.setAttribute("data-href", decodeURIComponent(item.getAttribute("data-href")));
      });

      window.fbAsyncInit();
      return false;
    }

    let fbScriptStr = `var emframe = document.querySelectorAll('.fb-post');
	    emframe.forEach(function(item,index){
        	item.setAttribute('data-href', decodeURIComponent(item.getAttribute('data-href')))
		});
		
	    window.fbAsyncInit = function() {
            FB.init({
              xfbml      : true,
              version    : 'v2.12'
            });
          };
          (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        `;

    let fbScript = document.createElement("script");
    fbScript.setAttribute("id", "fbScript");
    fbScript.innerHTML = fbScriptStr;

    document.body.appendChild(fbScript);
  };

  closeCommentBox = () => {
    // console.log("close", this.state.showCommentBox);
    //if(!this.state.showCommentBox) return;
    this.setState({ showCommentBox: false });
    enableBodyScroll();
  };

  handleComments = msid => {
    if (this.state.showCommentBox) return;
    this.setState({
      showCommentBox: true,
    });
    disableBodyScroll();
    //let subsection = router.location.pathname.indexOf("/tech") > -1 ? "tech/" : "";
    //let url = `${siteConfig.mweburl}/${subsection}off-url-forward/pwa_comments.cms?frmapp=yes&pagetype=articleshow`;
    //window.open(url);

    // let commentBox = document.getElementById('comment-section');
    // commentBox.style.height = window.innerHeight - document.querySelector('#headerContainer').clientHeight - 30 + "px";
    // commentBox.querySelector('#title').innerHTML = document.title.split('-')[0];
    // commentBox.classList.add('show-sidebox');

    // fetch(url).then(resp=>{
    // 	if(resp.status == 200){
    // 		resp.text().then(data=>{
    // 			commentBox.querySelector('#commentHolder').innerHTML = data.querySelector('comment-section');
    // 		})
    // 	}
    // })
  };
  frmBrandwire() {
    let urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has("brandwire") && urlParams.get("brandwire") == 1) {
      return true;
    }
    return false;
  }

  // Converts a string to its html characters completely.
  encode = str => {
    let buf = [];
    for (var i = str.length - 1; i >= 0; i--) {
      buf.unshift(["&#", str[i].charCodeAt(), ";"].join(""));
    }
    return buf.join("");
  };

  // Converts an html characterSet into its original character.
  decode = str => {
    return str.replace(/&#(\d+);/g, function(match, dec) {
      return String.fromCharCode(dec);
    });
  };

  newStoryStrManipulator = item => {
    let story = item.Story;
    const { ag } = item;
    let showBCCLAd = true;
    if (
      (ag && (ag.toLowerCase() == "mediawire" || ag.toLowerCase() == "brandwire" || ag.toLowerCase() == "Colombia")) ||
      story.indexOf(`type="bccl"`) > -1
    ) {
      showBCCLAd = false;
    }
    // handling only the article and not the photofeature
    // site is iag and mrec1 is not present and fullstop is present
    if (process.env.SITE === "iag" && story.indexOf("mrec1") === -1 && story.indexOf(".") > -1) {
      const linebreaker = "<br/><br/>";
      // let newstory = "<ad platform='desktop' type='dfp' id='mrec1'></ad>" + story;
      let newstory = story;
      // need to add this add after 1 para
      // regex to identify where sentence is completed - not a perfect but quite handy approach
      newstory = newstory.replace(/([^a-z0-9A-Z])\. ([^a-z0-9A-Z])/g, "$1.<linecomplete>$2");
      const newstoryArray = newstory.split("<linecomplete>");
      if (newstoryArray && newstoryArray.length > 1) {
        newstory = "";
        newstoryArray.forEach((line, index) => {
          // assuming every four lines is single paragraph:
          if (index === 7) {
            // inserting ad after 2 paragraphs
            newstory += "<ad platform='desktop' type='dfp' id='mrec1'></ad>" + line;
          } else {
            newstory += (index + 1) % 4 === 0 ? linebreaker + line : " " + line;
          }
        });
      }
      if (newstory.indexOf("<linecomplete>") > -1) {
        // just to make sure if any <linecomplete> tag is added and not processed, it should be cleared
        // using regex for that as it is the efficient and do not require looping
        newstory = newstory.replace(/\<linecomplete\>/g, " ");
      }
      if (showBCCLAd) {
        newstory = addBCCLWrapper(newstory);
      }
      return newstory;
    }
    if (showBCCLAd) {
      story = addBCCLWrapper(story);
    }
    return story;
  };

  // Converts Story node , only for iag , adding <br> nodes after every 4 '.' and <ad>mrec1</ad> on 1st 4 '.'
  storyStrManipulator = str => {
    try {
      if (process.env.SITE === "iag" && str.indexOf("mrec1") === -1 && str.indexOf(".") > -1) {
        let customStr = str;

        customStr = customStr.split(".").reduce((newstr, string, index) => {
          let manipulatedStr =
            index + 1 == 4
              ? "<ad platform='desktop' type='dfp' id='mrec1'></ad>"
              : (index + 1) % 4 == 0
              ? ".<br/><br/>"
              : "";
          return (newstr += string + manipulatedStr);
        });

        return customStr;
      }
    } catch (ex) {}

    return str;
  };

  render() {
    let {
      item,
      isFetching,
      sharedata,
      isLastArticle,
      mostViewedData,
      rhsCSRVideoData,
      superHitWidgetData,
      pagetype,
      index,
      query,
      router,
      nextreview,
      platform,
      openCommentsPopup,
      isFeaturedArticle,
      adconfig,
      advertorialData,
      scroll,
      advertorialNews,
      pwaConfigAlaska,
    } = this.props;
    let { pwa_meta } = item;
    //Object to send config in ParserOption
    const advertorialNewsCopy = JSON.parse(JSON.stringify(advertorialNews));
    // const advertorialNewsCopy = [];
    let Options = {
      campaign: adconfig && adconfig.campaign && { ...adconfig.campaign },
      firstAdvertorial: advertorialNewsCopy.splice(0, 1),
      restAdvertorial: [...advertorialNewsCopy],
    };

    //3333 console.log("pwaConfigAlaska", pwaConfigAlaska.amazondeal);
    // subsec1 var for paytml widget only
    let subsec1 = item.subsec1;

    let esidata = item != undefined ? item : null;

    if (_isCSR()) window.data = Object.assign({}, item);

    //reset pagetype is subpagetype available
    pagetype =
      pwa_meta && pwa_meta.subpagetype && router && router.location.pathname.indexOf("/tech") > -1
        ? pwa_meta.subpagetype
        : pwa_meta.pagetype;

    if (item && item.sponsorsurl && item.sponsorsurl.indexOf("?utm_source=") == -1) {
      item.sponsorsurl = item.sponsorsurl + "?utm_source=" + process.env.SITE;
    }

    // const superHitData =
    //   (item && item.recommended && item.recommended.superhitwidget && item.recommended.superhitwidget[0]) || "";

    let isAmpPage = false;
    // For nbt, amp widget works from MT domain
    let amazonIframeDataSource =
      process.env.SITE === "nbt"
        ? `${taswidgetMTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
            pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
          }&msid=${item && item.id}`
        : `${taswidgetNBTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
            pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
          }&msid=${item && item.id}`;
    if (router && router.location.pathname && router.location.pathname.includes("amp_")) {
      isAmpPage = true;
    }

    if (item != undefined) {
      let schemaBody = item.recipe
        ? SeoSchema({
            pagetype: pagetype,
          }).attr().Instructions
        : SeoSchema({
            pagetype: pagetype,
          }).attr().articleBody;
      const featuredClass = isFeaturedArticle ? "featured-article" : "";
      let isLeadImageAdded = true;
      if (item.Story && item.Story.indexOf("mrec1") === -1) {
        isLeadImageAdded = false;
      }
      const replacedSyn = filterDescription(item && item.syn);
      return (
        <div
          key={index}
          className={"article-section clearfix " + featuredClass}
          gatracked={index == 0 ? "true" : null}
          id={item.id}
          data-shorurl={item && item.m}
          // data-artbox={"article-" + item.id}
        >
          {SeoSchema({
            pagetype: pagetype,
          }).mainEntry(item.pwa_meta.canonical)}
          {SeoSchema({
            pagetype: pagetype,
          }).metaUrl(item.pwa_meta.canonical)}
          {SeoSchema({
            pagetype: pagetype,
          }).dateStatus(item.dlseo, item.luseo)}
          {SeoSchema({ pagetype: pagetype }).language()}
          {item.pwa_meta.alternatetitle ? SeoSchema().alternateName(item.pwa_meta.alternatetitle) : ""}
          {item.pwa_meta.key ? SeoSchema().keywords(item.pwa_meta.key) : ""}
          {pagetype == "articleshow" && !item.recipe ? SeoSchema().artSection(item.sec) : ""}
          {replacedSyn ? SeoSchema().artdesc(replacedSyn) : ""}
          {SeoSchema({
            pagetype: pagetype,
          }).alternativeHeadline(item.pwa_meta.title)}
          {pagetype == "articleshow" ? SeoSchema().disambiguatingDescription(item.webtitle) : ""}
          {SeoSchema().publisherObj(item.pwa_meta.techchannel)}

          {item.pwa_meta.claimer
            ? SeoSchema().factcheck(item.pwa_meta, item.dlseo, item.hl, item.ContainerDescription)
            : ""}
          {/* ATF ad starting of every perpetual story with story partition*/}
          {index > 0 && isMobilePlatform() ? (
            <React.Fragment>
              <div className="story_partition">
                <span>{siteConfig.locale.next_article[pagetype ? pagetype : "articleshow"]}</span>
              </div>
              {!isFeaturedArticle && (
                <div className="story-atf-wrapper">
                  <AdCard
                    mstype="atf"
                    adtype="dfp"
                    pagetype="articleshow"
                    query={query}
                    content={esidata}
                    artilceUrl={item.wu}
                    // esiad={esidata != null ? true : false}
                  />
                </div>
              )}
            </React.Fragment>
          ) : null}

          <div className={"story-article"}>
            {item &&
            item.sponsorsurl != undefined &&
            (this.props.query["brandwire"] == 1 || this.props.query["backto"] == 1) ? (
              <div className="backToBrandwire">
                <a href={item.sponsorsurl}>BACK TO {item.ag.toUpperCase()}</a>
              </div>
            ) : null}
            {//Call components conditionally on the basis of page type
            // "MoviePoster" for Moviereview show page lead image card
            // "SummaryCard" for Articleshow show page lead image card
            pagetype == "moviereview" ? (
              // ? MoviePoster(item, pagetype, sharedata, openCommentsPopup)
              <MoviePoster
                item={item}
                pagetype={pagetype}
                sharedata={sharedata}
                openCommentsPopup={openCommentsPopup}
              />
            ) : (
              SummaryCard({
                item: item,
                pagetype: pagetype,
                sharedata: sharedata,
                openCommentsPopup,
              })
            )}
            {item.ismarkreview && item.gadgetinfo ? (
              <div className="wdt_rating mobile">
                <Userrating
                  userratings={item.gadgetinfo.ratings ? item.gadgetinfo.ratings : null}
                  review={`techreview`}
                  userrating={item.gadgetinfo.ur}
                  id={item.id}
                  topfeature={item.topfeature && item.topfeature != "" ? item.topfeature : null}
                  sharedata={sharedata}
                  cr={item.cr}
                  ur={item.gadgetinfo.ur}
                  pagetype={`techreview`}
                />
              </div>
            ) : null}
            {/* For Recipe */
            // if ingredients available ? print
            item.recipe && item.recipe.ingredients && item.recipe.translation && item.recipe.translation.serving
              ? recipeIngriedient(item.recipe.ingredients, item.recipe.translation)
              : null}
            <TabContainer rel="tabs_">
              <article
                tab="active"
                label={siteConfig.locale.critics_review}
                {...schemaBody}
                className={"story-content" + (pagetype == "moviereview" ? " tab_content" : "")}
              >
                {// For Recipe Staeps Title
                item.recipe ? (
                  <h3 className="recipeStepsTitle">
                    {item.recipe.translation.howtomake ? item.recipe.translation.howtomake : item.hl}
                  </h3>
                ) : null}
                {isMobilePlatform() && item.topfeature && item.topfeature != ""
                  ? HighlightCard({
                      data: item.topfeature,
                      type: "topfeature",
                      msid: item.id,
                    })
                  : null}
                {item.embedhighlights && item.embedhighlights != ""
                  ? HighlightCard({
                      data: item.embedhighlights,
                      type: "tophighlight",
                      briefsectioninfo: item.briefsectioninfo,
                      msid: item.id,
                    })
                  : null}
                {// Add Author and Updated Time for Movie Review Only (MOBILE) to check if this
                // is same for tech review
                (isMobilePlatform() && pagetype == "moviereview") || pagetype == "techreview" ? (
                  <div className="moviereview-sourceCard">
                    {/* Check is author name, id and full name exist */
                    SummaryCard.Byline(item)}
                  </div>
                ) : null}
                {/* Article Text */
                Parser(this.newStoryStrManipulator(item), {
                  replace: parserOptions.replace.bind(this, item, query, this.props.parentObj, Options),
                  //replace: parserOptions.replace.bind(this, item, query),
                })}
                {/*Section Implementaion if AS is created in PG as multiple section via slideshow*/
                // item && item.substory ?
                //   <div
                //     className="photomazzashow-content lft_content"
                //     itemType="https://schema.org/ImageGallery"
                //     itemScope="1"
                //     id={item.pwa_meta.msid}
                //   >
                //     <meta itemProp="headline" content={item.pwa_meta.webtitle} />
                //     <meta itemProp="mainEntityOfPage" content={item.pwa_meta.canonical} />
                //     <meta itemProp="dateModified" content={item.pwa_meta.modified ? item.pwa_meta.modified : item.luseo} />
                //     <meta itemProp="datePublished" content={item.pwa_meta.publishtime ? item.pwa_meta.publishtime : item.dlseo} />
                item.substory &&
                  item.substory instanceof Array &&
                  item.substory.map((_item, index) => {
                    return (
                      <section
                        // itemType="http://schema.org/ImageObject"
                        // itemProp="associatedMedia"
                        // itemScope="1"
                        // data-url={_item.sub_url}
                        // id={item.id}
                        key={"section" + index}
                        sub_url={_item.sub_url}
                        id={"story_" + (index + 1)}
                      >
                        {/* <meta itemProp="thumbnailUrl" content={_item.sub_url} />
                        <meta itemProp="contentUrl" content={_item.sub_url} /> */}
                        {_item.h2.indexOf("href=") > -1 ? (
                          /* to support anchor as string in h2, due to hack for as to pf*/
                          <h2 dangerouslySetInnerHTML={{ __html: this.decode(_item.h2) }} />
                        ) : (
                          /* use decode() to support emojies */
                          <h2>{this.decode(_item.h2)}</h2>
                        )}

                        {/*Here we are creating new Object instance of parent item with combing new attributes from section item  */
                        Parser(_item.Story, {
                          replace: parserOptions.replace.bind(this, Object.assign({}, item, _item), query, {}, Options),
                        })}
                      </section>
                    );
                  })}
                {/* </div> : null */}
                {/* Showing the images slider or image if extra images are given */
                item.image && item.image[0] && item.image[0].id ? (
                  //Now compare with lead , that its not present there
                  item.lead && item.lead.image && item.image[0].id == item.lead.image[0].id ? null : (
                    <div className="img_wrap">
                      <ImageCard
                        title={item.image[0].title}
                        alt={item.image[0].title}
                        size="bigimage"
                        msid={item.image[0].id}
                        pagetype={pagetype}
                        schema="true"
                        adgdisplay={
                          (item &&
                            item.pwa_meta &&
                            item.pwa_meta.AdServingRules &&
                            item.pwa_meta.AdServingRules === "Turnoff") ||
                          isFeaturedArticle
                            ? "false"
                            : null
                        }
                      />
                      {item.image[0] && item.image[0].cap ? (
                        <div className="img_caption" dangerouslySetInnerHTML={{ __html: item.image[0].cap }}></div>
                      ) : null}
                    </div>
                  )
                ) : null}
                {/* product specification */}
                {item && item.gadgetinfo ? (
                  <div className="tbl_articleshow">
                    <div className="top-head">
                      <AnchorLink
                        href={`${process.env.WEBSITE_URL}tech/${_getUrlOfCategory(item.gadgetinfo.category)}/${
                          item.gadgetinfo.seoname
                        }#specification`}
                        target="_blank"
                      >
                        {item.gadgetinfo.name} {siteConfig.locale.tech.specifications}
                      </AnchorLink>
                    </div>

                    {item.gadgetinfo.specs ? GadgetFeaturecompare(item.gadgetinfo.specs) : null}

                    <AnchorLink
                      className="bottom-btn"
                      href={`${process.env.WEBSITE_URL}tech/${_getUrlOfCategory(item.gadgetinfo.category)}/${
                        item.gadgetinfo.seoname
                      }#specification`}
                      target="_blank"
                    >
                      {siteConfig.locale.tech.fullspecification}
                    </AnchorLink>
                  </div>
                ) : null}
                {item && item.gadgetinfo && item.gadgetinfo.variants ? (
                  <GadgetsVariants
                    data={item}
                    gadVarients={item.gadgetinfo.name}
                    gadgetCategory={item.gadgetinfo.category}
                  />
                ) : (
                  ""
                )}
                {!isLeadImageAdded && isMobilePlatform() ? <ASLeadImage item={item} /> : null}
                {/* TODO : Adding slider if more than one image resource is available : Need extra handling in slider */}
                {/* {item.image && item.image.length > 1 && <div>Separate slider</div>} */}
              </article>
              {/* {
								(item.twitter && item.twitter_key) ?
								<div label={siteConfig.locale.twitte_reaction}>{getTwitterTimeLine(item)}</div>

								: null
							} */}
            </TabContainer>

            {/* SuperHitWidget for Mobile */}

            {superHitWidgetData && superHitWidgetData.items && isMobilePlatform() ? (
              <div
                className="row wdt_superhit"
                data-scroll-id={`superhit-widget-${item.id}`}
                data-row-id={`superhit-widget${index == 0 ? "" : "-perpetual"}`}
                data-exclude="amp"
              >
                <div className="section superhitt">
                  <h3>
                    {superHitWidgetData.wu ? (
                      <span>
                        <AnchorLink href={superHitWidgetData.wu}>{superHitWidgetData.secname}</AnchorLink>
                      </span>
                    ) : (
                      <span>{superHitWidgetData.secname}</span>
                    )}
                  </h3>
                </div>
                <GridSectionMaker type={defaultDesignConfigs.topicslisting} data={superHitWidgetData.items} />
              </div>
            ) : null}

            {isAmpPage ? <div className="amp_superwidget"></div> : null}
            {/* video iframe added for 5 days */}
            {/* {isMobilePlatform() ? (
              <div className="vd_pl" data-exclude="amp">
                <iframe
                  src={`${siteConfig.weburl}/pwafeeds/videoplayer_iframe.cms?wapCode=${process.env.SITE}`}
                  height="250"
                  width="300"
                  border="0"
                  marginWidth="0"
                  marginHeight="0"
                  hspace="0"
                  vspace="0"
                  frameBorder="0"
                  scrolling="no"
                  align="center"
                  title=""
                  allowTransparency="true"
                  id="vodplayer"
                />
              </div>
            ) : null} */}

            {pwaConfigAlaska &&
            (pwaConfigAlaska._topdeal == "true" || pwaConfigAlaska._topoffer == "true") &&
            isMobilePlatform() &&
            ((isTechSite() && !isTechPage(router.location.pathname)) || !isTechSite()) ? (
              <div className="wdt_amz pwa-deals">
                {this.state.isScroll || isAmpPage ? (
                  <iframe
                    scrolling="no"
                    frameBorder="no"
                    height={isAmpPage ? "330" : "310"}
                    src={
                      isAmpPage
                        ? amazonIframeDataSource
                        : `${taswidgetNBTPath}?host=${process.env.SITE}&wdttype=${
                            pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                          }&msid=${item && item.id}&category=${item && item.pwa_meta && item.pwa_meta.subsectitle1}`
                    }
                  />
                ) : null}
              </div>
            ) : null}

            {/* Tech product Amazon Widget	 */}

            <AmazonWidget
              item={item}
              type="sponsored"
              isFeaturedArticle={isFeaturedArticle}
              router={router}
              techPageOnly
            />

            {!isTechPage() &&
              isMobilePlatform() &&
              !isFeaturedArticle &&
              pwaConfigAlaska &&
              pwaConfigAlaska._amazondeal == "true" && <AmazonWidget type="amazondeal" />}

            {/* Too Good Widget	 */}
            {(process.env.SITE == "nbt" || process.env.SITE == "vk") && item && item.twogudkey ? (
              <div className="wdt_twogud">
                <iframe
                  id="wdt_twogud"
                  height={isMobilePlatform() ? "300" : "290"}
                  scrolling="no"
                  frameBorder="no"
                  src={
                    (siteConfig.channelCode == "nbt"
                      ? "https://malayalam.samayam.com"
                      : "https://navbharattimes.indiatimes.com/") +
                    "/off-url-forward/amazon_widget.cms?type=2gud&channel=" +
                    siteConfig.channelCode +
                    (item.twogudkey && item.twogudkey != "" ? "&productname=" + item.twogudkey : "") +
                    (item.brandname && item.brandname != "" ? "&brandname=" + item.brandname : "") +
                    `&pagename=${item.ismarkreview ? "reviewshow" : "articleshow"}` +
                    "&platform=" +
                    `${isMobilePlatform() ? (router.location.pathname.indexOf("/amp_") > -1 ? "amp" : "wap") : "web"}` +
                    "&pageviewnum=" +
                    (_isCSR() && window.sessionPageView ? window.sessionPageView : "0")
                  }
                  allowtransparency="true"
                ></iframe>
              </div>
            ) : null}

            <TopComment msid={item.id} />
            {/*  Amazon Widget placed just after body, 	only for mobile */}
            {/* Code for amazon widget is placed at bottom of the page - commented*/}
            {/* App link comes for desktop platform above comment button */}
            {!isMobilePlatform() && (pagetype == "moviereview" || pagetype == "articleshow") ? (
              <span className="news-app">
                <AnchorLink target="_blank" href={siteConfig.appPlayStoreLink}>
                  {siteConfig.newsApp}
                </AnchorLink>
                : {siteConfig.locale.appLinkText}
                {siteConfig.locale && siteConfig.locale.appLinkTextextended ? (
                  <AnchorLink target="_blank" href={siteConfig.appPlayStoreLink}>
                    {siteConfig.locale.appLinkTextextended}
                  </AnchorLink>
                ) : null}
                <div className="btn-like" id={`btnLike${item.id}`}>
                  {siteConfig.locale.fblikeTxt.txt1}
                  <AnchorLink target="_blank" href={"/"}>
                    {siteConfig.locale.fblikeTxt.siteName}
                  </AnchorLink>
                  {siteConfig.locale.fblikeTxt.txt2}
                  {/* <iframe
                    id="btnfblike"
                    allowtransparency="true"
                    frameBorder="0"
                    scrolling="no"
                    width="80"
                    height="20"
                    data-src={siteConfig.locale.social_download.fblikeURL}
                  /> */}
                </div>
              </span>
            ) : null}
            {/* Comment widget for mobile and handling commentwidget on gnreview page
                For desktop, comment button shows up after story.
            */}

            {isMobilePlatform() && this.config.shouldRenderTimespoint && (
              <ErrorBoundary>
                <TPwidget type="AS" router={this.props.router} />
              </ErrorBoundary>
            )}

            {isMobilePlatform() ? (
              <div className="comment_wrap" data-exclude="amp">
                {/*<AnchorLink key={'comment'+item.id} className="btn_comment" target="_blank" hrefData={{override : `https://navbharattimes.indiatimes.com/off-url-forward/pwa_comments.cms?frmapp=yes&pagetype=${pagetype}`}}>कॉमेंट लिखें</AnchorLink>*/}
                <a
                  id={"comment_" + item.id}
                  className="btn_comment"
                  href="javascript:void(0)"
                  onClick={this.handleComments.bind(this, item.id)}
                >
                  {siteConfig.locale.comment_text}
                </a>
                {_isCSR() && this.state.showCommentBox ? (
                  <CommentBox msid={item.id} headline={item.hl} closeCommentsBox={this.closeCommentBox} />
                ) : null}
              </div>
            ) : (
              <div className="comments-n-socail">
                <div className="comments-wrapper" role="button" tabIndex="0">
                  <div className="bottom-comments-btns">
                    <div className="cmtbtn-wrapper">
                      <span className="cmtbtn add" onClick={openCommentsPopup}>
                        <span>{siteConfig.locale.comment_text}</span>
                        <b className="icon" />
                      </span>
                    </div>
                  </div>
                </div>
                <div className="btm-social">
                  <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
                </div>
              </div>
            )}
            {/* Comment widget for mobile ends*/}
            {/* For App Download  - mobile*/}
            {(isMobilePlatform() && this.state.hasMounted) || isAmpPage ? (
              <div className="con_fbapplinks">
                <h3>{siteConfig.locale.social_download.title}</h3>
                <div className="wdt_story_social">
                  <SocialSubscribeCard telegramLink={item.pwa_meta.teleChannel} />
                </div>
              </div>
            ) : null}
            {/* App Download Ends Here  - mobile */}
            {/* Mrec ad after app Download 
						    If pwa_meta contains AdServingRules="Turnoff" MREC won't show
						*/}
            {/* {!isMobilePlatform() ? null : item &&
            item.pwa_meta &&
            item.pwa_meta.AdServingRules &&
            item.pwa_meta.AdServingRules === "Turnoff" ? null : (
              <div className="story-content">
                <AdCard mstype="mrec2" adtype="dfp" pagetype="articleshow" renderall={true} />
              </div>
            )} */}
            {
              /* Print Paytm Widget for Tech Section Only */
              // pagetype == 'articleshow' && subsec1 == '19615041' ? <div className="paytm_wrapper"><PaytmWidget pagename="articleshow" subsec1={subsec1} /></div> : null
            }
            {/* Ctn 4X4 ad at end of story
						   if pwa_meta AdServingRules === "Turnoff" having this property, ctn won't show
						*/}
            {/*
							(item && item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff") ? null :
								<AdCard ctnstyle="ctnshow" adtype="ctn" pagetype="articleshow" content={item} query={query} esiad={true} />
						*/}
            {/* (for lokpriya khabre / superhit widget} desktop and mobile*/}
            {/* {superHitWidgetData ? <SuperHitWidget data={superHitWidgetData} router={router} /> : null} */}

            {/* Tech sites -> nbt and vk For these sites tech pages, widget should not come */}

            {/* SuperHitWidget for Desktop */}
            {superHitWidgetData && superHitWidgetData.items && !isMobilePlatform() ? (
              <div
                className="row wdt_superhit"
                data-scroll-id={`superhit-widget-${item.id}`}
                data-row-id={`superhit-widget${index == 0 ? "" : "-perpetual"}`}
                data-exclude="amp"
              >
                <ErrorBoundary>
                  <div className="superhit-wrapper">
                    <h3>
                      {superHitWidgetData.wu ? (
                        <span>
                          <AnchorLink href={superHitWidgetData.wu}>
                            {`${siteConfig.locale.read_more}: ${superHitWidgetData.secname}`}
                          </AnchorLink>
                        </span>
                      ) : (
                        <span>{`${siteConfig.locale.read_more}: ${superHitWidgetData.secname}`}</span>
                      )}
                    </h3>
                    <Slider
                      type="grid"
                      size="3"
                      movesize="1"
                      sliderData={superHitWidgetData.items}
                      width={isMobilePlatform() ? "280" : "200"}
                      SliderClass="popular_videos"
                      islinkable
                      margin={isMobilePlatform() ? "15" : "10"}
                    />
                  </div>
                </ErrorBoundary>
              </div>
            ) : null}
            {/* For desktop social share comes just below Superhit widget*/}
            {/* {!isMobilePlatform() ? (
              <React.Fragment>
                <div className="btm-social">
                  <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
                </div>
              </React.Fragment>
            ) : (
              ""
            )} */}
            {nextreview ? (
              <div className="next_article" data-attr="next_article">
                {/* //review next articles */}
                <a
                  onClick={e => this.scrollInView(e, nextreview.id)}
                  data-nextart={nextreview.id}
                  href={
                    "/" +
                    nextreview.seolocation +
                    "/" +
                    (pagetype && pagetype != "techreview" ? pagetype : "articleshow") +
                    "/" +
                    nextreview.id +
                    ".cms?utm_source=nextstory&utm_medium=referral&utm_campaign=articleshow"
                  }
                >
                  <span className="nxttitle">
                    <span className="text_ellipsis">{nextreview.hl}</span>
                  </span>
                  <span className="nxtlink">
                    {pagetype
                      ? siteConfig.locale.next_article[pagetype]
                      : siteConfig.locale.next_article["articleshow"]}
                  </span>
                </a>
              </div>
            ) : item.pwa_meta && item.pwa_meta.nextItem && item.pwa_meta.nextItem !== "" ? (
              <div className="next_article" data-attr="next_article">
                <AnchorLink
                  href={
                    "/" +
                    item.pwa_meta.nextItem.seolocation +
                    "/" +
                    (pagetype ? pagetype : "articleshow") +
                    "/" +
                    item.pwa_meta.nextItem.msid +
                    ".cms?utm_source=nextstory&utm_medium=referral&utm_campaign=articleshow"
                  }
                >
                  <span className="nxttitle">
                    <span className="text_ellipsis">{item.pwa_meta.nextItem.title}</span>
                  </span>
                  <span className="nxtlink">
                    {pagetype
                      ? siteConfig.locale.next_article[pagetype]
                      : siteConfig.locale.next_article["articleshow"]}
                  </span>
                </AnchorLink>
              </div>
            ) : null}
            {/*  NewsLetter Widget for mobile ends after next story link*/}
            {/*  Mrec2  only for mobile comes after NewsLetter Widget 
               If pwa_meta contains AdServingRules="Turnoff" MREC won't show
            */}
            {!isFeaturedArticle && isMobilePlatform() ? (
              item &&
              item.pwa_meta &&
              item.pwa_meta.AdServingRules &&
              item.pwa_meta.AdServingRules === "Turnoff" ? null : (
                <AdCard mstype="mrec2" adtype="dfp" pagetype="articleshow" renderall={true} />
              )
            ) : null}
            {/*  NewsLetter Widget for mobile starts after next story link*/}
            {isMobilePlatform() && this.state.hasMounted && <NewsLetter pagetype={"articleshow"} articleid={item.id} />}
            {/*  Mrec2 ends only for mobile comes after NewsLetter Widget */}
            {/* TO ADD - FB LIKE TWITTER YT  ETC */}
            {/*  From around the web starts*/}
            {/* code commented for CTN  widget */}
            {/* If Turnoff rules or featured article, from around the web is not loaded */}
            {!isFeaturedArticle &&
              // This condition is applied with not because in case AdServing rules node is not present
              // we need to show ads
              !(item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff") && (
                <div className="adCANContainer">
                  {/* {isMobilePlatform() ? null : <AdCard adtype="ctn" mstype="ctnshow1" />} */}
                </div>
              )}

            {/*  Popular videos for mobile placed after recommended starts*/}
            {isMobilePlatform() && _isCSR() && rhsCSRVideoData ? (
              <div className="row wdt_popular_videos box-item" data-exclude="amp">
                <ErrorBoundary>
                  <div
                    className="section"
                    data-scroll-id={`popular-videos-${item.id}`}
                    data-row-id={`popular-videos${index == 0 ? "" : "-perpetual"}`}
                  >
                    <div className="top_section">
                      <h3>
                        <span>{rhsCSRVideoData.secname}</span>
                      </h3>
                    </div>
                  </div>

                  <GridSectionMaker
                    type={defaultDesignConfigs.horizontalSlider}
                    data={[].concat(rhsCSRVideoData.items)}
                  />
                </ErrorBoundary>
              </div>
            ) : null}

            {/*  From around the web ends */}
            {/* Related gadgets for tech */}
            {isMobilePlatform() && this.state.relatedgadgets && this.state.relatedgadgets.gadget ? (
              <div className="box-content">
                <GadgetRelated
                  anchorlink="yes"
                  headingoverride="true"
                  pagetype={pagetype}
                  suggested={this.state.relatedgadgets.gadget}
                />{" "}
              </div>
            ) : null}
            {/* Keyword widget after From around the web*/}
            {item.pwa_meta && item.pwa_meta.topicskey && item.pwa_meta.topicskey !== "" ? (
              <div className={`${isMobilePlatform() ? "keywords_wrap" : "trending-bullet"}`}>
                {KeyWordCard(item.pwa_meta.topicskey)}
              </div>
            ) : null}
            {/* Keyword widget ends  */}
            {/* Web title widget starts after keywords*/}
            {WebTitleCard(item)}
            {/* Web title widget ends after keywords*/}
            {/* For desktop Comments Section comes just  after Web title starts*/}

            {/* {!isMobilePlatform() ? (
              <React.Fragment>
                <CommentsSection msid={item.id} showComments={openCommentsPopup} />
              </React.Fragment>
            ) : (
              ""
            )} */}

            {/* For desktop Comments Section comes just  after Web title ends */}
            {/*  For desktop Popular videos widget comes after web title */}
            {!isMobilePlatform() && rhsCSRVideoData && rhsCSRVideoData.items ? (
              <div
                className="wdt_popular_videos"
                data-scroll-id={`popular-videos-${item.id}`}
                data-row-id={`popular-videos${index == 0 ? "" : "-perpetual"}`}
                data-exclude="amp"
              >
                <ErrorBoundary>
                  <Slider
                    type="grid"
                    size="3"
                    movesize="3"
                    sliderData={[].concat(rhsCSRVideoData.items)}
                    width="190"
                    SliderClass="popular_videos"
                    islinkable="true"
                    margin="20"
                    heading={siteConfig.locale.popularVideos}
                  />
                </ErrorBoundary>
              </div>
            ) : null}

            {/*  For desktop Popular videos widget comes after web title */}
            {/* Breadcrumb widget for mobile starts after Web title */}
            {isMobilePlatform() && item.breadcrumb && item.breadcrumb.div ? (
              <Breadcrumb items={item.breadcrumb.div.ul} />
            ) : null}
            {/* Breadcrumb widget for mobile ends after Web title */}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

function mapStateToProps(state) {
  return {
    advertorialData: state && state.app && state.app.rhsData,
    // electionresult:state.electionresult
  };
}

export default connect(mapStateToProps)(StoryCard);

// To create twitter timeline widget
const getTwitterTimeLine = data => {
  let timelineUrl = "https://twitter.com/search?q=" + data.twitter_key;
  return timelineUrl != "" ? (
    <a lang="en" className="twitter-timeline" data-widget-id={data.twitter} href={timelineUrl}>
      Tweets about {data.twitter_key}
    </a>
  ) : null;
};

const recipeIngriedient = (item, translation, hl) => {
  return (
    <div className="recipe_ingriedient">
      {translation != undefined && translation.serving != "" ? (
        <span className="forServing">{translation.serving}</span>
      ) : null}
      {Object.keys(item).map(function(key, index) {
        return key != "etc" ? (
          <div>
            <h4 className="specialingrs">
              {translation[key] ? translation[key] : key.indexOf("_") > -1 ? key.split("_").join(" ") : key}
            </h4>

            {
              <ul className="ingredients_lilsting">
                {//Regex to remove white space
                typeof item[key] == "object" ? (
                  item[key].map(function(key, index) {
                    return (
                      <li {...SeoSchema({ pagetype: "articleshow" }).attr().Ingredients}>{key.replace(/\s+/g, " ")}</li>
                    );
                  })
                ) : (
                  <li {...SeoSchema({ pagetype: "articleshow" }).attr().Ingredients}>
                    {item[key].replace(/\s+/g, " ")}
                  </li>
                )}
              </ul>
            }
          </div>
        ) : null;
      })}
    </div>
  );
};

const addBCCLWrapper = story => {
  try {
    const bcclWrapper = `<ad type="bccl"></ad>`;
    const dfpWrapper = `<ad platform="desktop" type="dfp" id="mrec1"></ad>`;
    const dfpWrapperIndex = story.indexOf(dfpWrapper);
    let remainStry;
    if (dfpWrapperIndex > -1) {
      remainStry = story.split(dfpWrapper)[1];
    } else {
      remainStry = story;
    }

    const secndBrkPosition = strPosition(remainStry, "<i></i>", 2);
    if (secndBrkPosition > -1) {
      remainStry = remainStry.substr(0, secndBrkPosition) + bcclWrapper + remainStry.substr(secndBrkPosition);
    } else {
      remainStry = remainStry + bcclWrapper;
    }
    if (dfpWrapperIndex > -1) {
      story = story.substr(0, dfpWrapperIndex) + dfpWrapper + remainStry;
    } else {
      story = remainStry;
    }
    return story;
  } catch (ex) {
    console.log(ex);
    return story;
  }
};

const strPosition = (str, substr, index) => {
  return str.split(substr, index).join(substr).length;
};
