import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./css/NewsListCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import { _deferredDeeplink, _getUrlOfCategory } from "./../../utils/util";
import { SeoSchema } from "../../components/common/PageMeta"; //For Page SEO/Head Part
import { AnalyticsGA } from "./../lib/analytics/index";
import AnchorLink from "./AnchorLink";
import VideoPlayer from "./../../modules/videoplayer/index";
import { createConfig } from "./../../modules/videoplayer/utils";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const slikeApiKey = siteConfig.slike.apikey;
const slikeAdCodes = siteConfig.slikeAdCodes;

class NewsListCard extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.item.id != nextProps.item.id) {
      return true;
    }
    return false;
  }

  showListingIcon(item) {
    let icon =
      item.tn == "video" || (item.isvdo && item.isvdo == "true")
        ? "video_icon"
        : item.tn == "photo"
          ? "photo_icon"
          : "";
    if (icon == "") return;
    return <span className={"type_icon " + icon}></span>;
  }

  showListNodeLabel(item) {
    let label =
      item.platform && item.platform.toLowerCase() == "apponly"
        ? siteConfig.locale.appexclusive
        : siteConfig.listNodeLabels[item.tn]
          ? siteConfig.listNodeLabels[item.tn][1]
          : "";
    return label;
  }

  // generateUrl(item) {
  //   let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
  //   let url = '/' + (item.seolocation ? (item.seolocation) + '/' : "") + templateName + '/' + (item.tn == 'photo' ? item.imageid : item.id) + '.cms';
  //   return item.override ? item.override : url;
  // }

  generateUrl(item) {
    if (item.override) {
      //Changing domain from absolute to relative
      return item.override.replace(/^.*\/\/[^\/]+/, "");
    } else {
      let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
      let url =
        "/" +
        (item.seolocation ? item.seolocation + "/" : "") +
        templateName +
        "/" +
        (item.tn == "photo" ? item.imageid : item.id) +
        ".cms";
      return url;
    }
  }
  listContentVideo(props) {
    let { item, leadpost, type, pagetype, listCountInc, noLazyLoad, seolistschema, layout } = props;
    let appexclusiveflag = item.platform && item.platform.toLowerCase() == "apponly";
    return (
      <React.Fragment>
        <span
          className={"table_col img_wrap" + (leadpost && item.tn == "photo" ? " photolead_img_warpper" : "")}
          data-tag={item.tn == "video" ? item.du : null}
        >
          <ImageCard
            type={
              item.imageid && typeof item.imageid == "string" && item.imageid.toLowerCase().indexOf("http") > -1
                ? "absoluteImgSrc"
                : null
            }
            src={item.imageid}
            noLazyLoad={noLazyLoad}
            msid={item.imageid ? item.imageid : item.id}
            title={item.seotitle ? item.seotitle : ""}
            size={
              layout && layout != ""
                ? "squarethumb"
                : type == "video_lead"
                  ? "largewidethumb"
                  : leadpost && item.tn == "photo"
                    ? "bigimage"
                    : leadpost
                      ? "largethumb"
                      : "smallthumb"
            }
          />
          {this.showListingIcon(item)}
        </span>
        <span className={styles["con_wrap"] + " table_col con_wrap"}>
          <span
            className="text_ellipsis"
            {...(seolistschema != true ? SeoSchema({ pagetype: pagetype }).attr().name : "")}
          >
            {item.hl}
          </span>

          {/* for Schema only */ seolistschema != true && !appexclusiveflag
            ? SeoSchema({ pagetype: pagetype }).metaPosition(listCountInc ? listCountInc() : "")
            : ""}

          {/* {((item.lu || item.dl) && !item.removedate) ?
              <span className={styles["time-caption"] + " time-caption"} data-time={item.lu ? item.lu : (item.dl ? item.dl : "")}>
                {item.lu ? item.lu : (item.dl ? item.dl : "")}
              </span> : null} */}
        </span>
      </React.Fragment>
    );
  }
  listContent(props, listlink) {
    let { item, leadpost, type, pagetype, listCountInc, noLazyLoad, seolistschema, layout } = props;
    let appexclusiveflag = item.platform && item.platform.toLowerCase() == "apponly";
    let gadgetinfoflag =
      ((item.cr && item.cr != "") || item.GadgetsModelDisplayName || item.GadgetsBrand || item.GadgetsCategory) &&
      siteConfig.locale &&
      siteConfig.locale.tech &&
      siteConfig.locale.tech.criticsrating;
    return (
      <React.Fragment>
        <div className="table_row">
          <span
            className={"table_col img_wrap" + (leadpost && item.tn == "photo" ? " photolead_img_warpper" : "")}
            data-tag={item.tn == "video" ? item.du : null}
          >
            <AnchorLink hrefData={listlink} fireClickPixel={item.clkurl ? item.clkurl : null} className="table_row">
              <ImageCard
                type={
                  item.imageid && typeof item.imageid == "string" && item.imageid.toLowerCase().indexOf("http") > -1
                    ? "absoluteImgSrc"
                    : null
                }
                src={item.imageid}
                noLazyLoad={noLazyLoad}
                msid={item.imageid ? item.imageid : item.id}
                title={item.seotitle ? item.seotitle : ""}
                size={
                  layout && layout != ""
                    ? "squarethumb"
                    : type == "video_lead"
                      ? "largewidethumb"
                      : leadpost && item.tn == "photo"
                        ? "bigimage"
                        : leadpost
                          ? "largethumb"
                          : "smallthumb"
                }
              />
              {this.showListingIcon(item)}
            </AnchorLink>
          </span>

          <span className={styles["con_wrap"] + " table_col con_wrap"}>
            <AnchorLink
              hrefData={listlink}
              fireClickPixel={item.clkurl ? item.clkurl : null}
              className="table_row"
              {...(seolistschema != true ? SeoSchema({ pagetype: pagetype }).attr().url : "")}
            >
              <span
                className="text_ellipsis"
                {...(seolistschema != true ? SeoSchema({ pagetype: pagetype }).attr().name : "")}
              >
                {item.hl}
              </span>

              {/* for Schema only */ seolistschema != true && !appexclusiveflag
                ? SeoSchema({ pagetype: pagetype }).metaPosition(listCountInc ? listCountInc() : "")
                : ""}

              {/* {((item.lu || item.dl) && !item.removedate) ?
                <span className={styles["time-caption"] + " time-caption"} data-time={item.lu ? item.lu : (item.dl ? item.dl : "")}>
                  {item.lu ? item.lu : (item.dl ? item.dl : "")}
                </span> : null} */}
            </AnchorLink>
            {gadgetinfoflag ? (
              <React.Fragment>
                {item.author ? <span className="list-tech-rating">{item.author}</span> : null}
                {item.cr && item.cr ? (
                  <span className="list-tech-rating">
                    {siteConfig.locale.tech.criticsrating}:{" "}
                    <span>
                      <b>{item.cr}</b>/5
                    </span>
                  </span>
                ) : null}
                {item.GadgetsModelDisplayName || item.GadgetsBrand || item.GadgetsCategory ? (
                  <span className="list-tech-brands">
                    {item.GadgetsModelDisplayName ? (
                      <AnchorLink
                        href={`/tech/${_getUrlOfCategory(item.GadgetsCategory)}/${item.GadgetsPrimaryGadget}`}
                      >
                        {item.GadgetsModelDisplayName}
                      </AnchorLink>
                    ) : null}
                    {item.GadgetsBrand || item.GadgetsCategory ? (
                      <React.Fragment>
                        {item.GadgetsBrand ? (
                          <AnchorLink href={`/tech/${_getUrlOfCategory(item.GadgetsCategory)}/${item.GadgetsBrand}`}>
                            {item.GadgetsBrand}
                          </AnchorLink>
                        ) : null}
                        {item.GadgetsCategory ? (
                          <AnchorLink href={`/tech/${_getUrlOfCategory(item.GadgetsCategory)}`}>
                            {item.GadgetsCategory}
                          </AnchorLink>
                        ) : null}
                      </React.Fragment>
                    ) : null}
                  </span>
                ) : null}
              </React.Fragment>
            ) : null}
          </span>
        </div>
        {/* {
          gadgetinfoflag ? 
          <span className="table_row">
            { 
              item.cr && item.cr ? 
              <span className="tech-rating table_col">{siteConfig.locale.tech.criticsrating}: <span><b>{item.cr}</b>/5</span></span>
              : null
            }
          </span>
          : null
        } */}
      </React.Fragment>
    );
  }

  onVideoClickHandler(item, event) {
    let _this = this;
    // event.currentTarget.previousSibling.innerText
    let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
    if (videourl) {
      try {
        //To maintain previous url
        window.history.pushState({}, "", videourl);
        AnalyticsGA.pageview(location.origin + _this.generateUrl(item));
      } catch (ex) {
        console.log("Exception in history: ", ex);
      }
    }
  }

  render() {
    let { item, leadpost, pagetype, seolistschema, className } = this.props;
    let appexclusive = item.platform && item.platform.toLowerCase() == "apponly";
    let deeplinktype =
      pagetype == "home"
        ? item.tn && item.tn != "" && item.tn == "news"
          ? "toparticle"
          : item.tn && item.tn != ""
            ? item.tn
            : "news"
        : item.tn && item.tn != ""
          ? item.tn
          : "news";

    //For Brandwire article handling
    let url = item.brandstry && item.brandstry == 1 ? (item.override ? item.override : this.generateUrl(item)) : null;
    let sectionName =
      item.subsecname && item.subsecname != "" && item.subsecseolocation && item.subsecmsid
        ? " section-name "
        : undefined;

    //let listlink = (appexclusive || (item.platform && item.platform.toLowerCase()=='apponly')) ? {override :  siteConfig.applinks.android.oip_appexclusive + item.id + '&pagetype='+ pagetype + '&type=' + (item.tn && item.tn != '' ? item.tn : 'news') } : (item.override ? {override :  item.override} : {seo: item.seolocation, tn : item.tn, id : (item.tn == 'photo') ? item.id && item.id != '' ? item.id : item.imageid : item.id});
    let listlink =
      appexclusive || (item.platform && item.platform.toLowerCase() == "apponly")
        ? {
          override: _deferredDeeplink(deeplinktype, siteConfig.appdeeplink, item.id),
          url: url,
        }
        : item.override
          ? { override: item.override }
          : {
            seo: item.seolocation,
            app_tn: item.app_tn,
            tn: item.tn,
            id: item.tn == "photo" ? (item.id && item.id != "" ? item.id : item.imageid) : item.id,
            url: url,
          };
    let config = createConfig(item, "", siteConfig, pagetype);
    let videourl = this.generateUrl(item);

    return (
      // in all cases remove class name - animated fadeIn
      appexclusive ? (
        <li
          className={
            (className ? className + " " : "") +
            (leadpost ? [styles["nbt-listview"], styles["lead-post"]].join(" ") : styles["nbt-listview"]) +
            " table nbt-listview " +
            (item.GadgetsBrand || item.GadgetsCategory || item.GadgetsModelDisplayName || (item.cr && item.cr != "")
              ? "li-tech "
              : " ") +
            (leadpost ? "lead-post" : "animated fadeIn") +
            (appexclusive && pagetype !== "tech" ? " app-exclusive" : "")
          }
          key={item.id}
          data-list-type={this.showListNodeLabel(item)}
        // onClick={() => {
        //   AnalyticsGA.event(siteConfig.appexclusive_ga);
        // }}
        >
          {this.listContent(this.props, listlink)}
          <span className="app-txt">{siteConfig.locale.appexclusive}</span>
        </li>
      ) : // eid is slike id
        item.tn == "video" && item.eid ? (
          <li
            data-videourl={videourl}
            className={
              (className ? className + " " : "") +
              (leadpost ? [styles["nbt-listview"], styles["lead-post"]].join(" ") : styles["nbt-listview"]) +
              " table nbt-listview " +
              (leadpost ? "lead-post" : "animated fadeIn")
            }
            key={item.id}
            data-list-type={this.showListNodeLabel(item)}
            {...SeoSchema({ pagetype: pagetype }).listItem()}
          >
            {/* <div className="videoUrl" style={{display:"none"}}><AnchorLink getUrl={true} hrefData={hrefData} /></div> */}
            <VideoPlayer
              key={item.eid}
              data-videourl={videourl}
              apikey={slikeApiKey}
              onVideoClick={this.onVideoClickHandler.bind(this, item)}
              wrapper="masterVideoPlayer"
              config={config}
            >
              <AnchorLink fireClickPixel={item.clkurl ? item.clkurl : null} itemProp="url" href={videourl}>
                {this.listContentVideo(this.props)}
              </AnchorLink>
            </VideoPlayer>
          </li>
        ) : (
            <li
              data-attr="as"
              className={
                (className ? className + " " : "") +
                sectionName +
                (leadpost ? [styles["nbt-listview"], styles["lead-post"]].join(" ") : styles["nbt-listview"]) +
                " table nbt-listview " +
                (item.GadgetsBrand || item.GadgetsCategory || item.GadgetsModelDisplayName || (item.cr && item.cr != "")
                  ? "li-tech "
                  : " ") +
                (leadpost ? "lead-post" : "animated fadeIn")
              }
              key={item.id}
              data-list-type={this.showListNodeLabel(item)}
              {...(seolistschema != true ? SeoSchema({ pagetype: pagetype }).listItem() : "")}
            >
              {item.subsecname && item.subsecname != "" && item.subsecseolocation && item.subsecmsid ? (
                <span className="news_section">
                  <AnchorLink href={`/` + item.subsecseolocation + "/articlelist/" + item.subsecmsid + ".cms"}>
                    {item.subsecname}
                  </AnchorLink>
                </span>
              ) : null}

              {this.listContent(this.props, listlink)}
            </li>
          )
    );
  }
}

NewsListCard.propTypes = {
  item: PropTypes.object,
};

export default NewsListCard;
