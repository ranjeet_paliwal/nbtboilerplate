import React from "react";
import PropTypes from "prop-types";

const SvgIcon = props => {
  const { name, className, color, width, height, center } = props;

  const TagName = center ? `i` : React.Fragment;
  return (
    <TagName className="svgiconcount">
      <svg
        className={`svg-icons ${className || ""}`}
        style={{ color }}
        width={`${width || "1em"}`}
        height={`${height || "1em"}`}
      >
        <use href={`#${name}`}></use>
      </svg>
    </TagName>
  );
};

SvgIcon.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  center: PropTypes.bool,
};

function arePropsEqual(prevProps, nextProps) {
  return (
    prevProps.name === nextProps.name &&
    prevProps.className === nextProps.className &&
    prevProps.color === nextProps.color &&
    prevProps.width === nextProps.width &&
    prevProps.height === nextProps.height &&
    prevProps.center === nextProps.center
  );
}

// React memo is used to stop rerendering of the component if the props remain same
export default React.memo(SvgIcon, arePropsEqual);
