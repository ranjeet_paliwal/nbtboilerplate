import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import { _isCSR, _deferredDeeplink, isMobilePlatform, isTechSite, isTechPage } from "./../../utils/util";
import styles from "./css/PhotoMazzaShowCard.scss";
import PhotoMazzaShowSubCard from "./PhotoMazzaShowSubCard";
import ErrorBoundary from "./../lib/errorboundery/ErrorBoundary";
import AdCard from "./AdCard";
import Ads_module from "./../../components/lib/ads/index";
import { _getStaticConfig } from "../../utils/util";
import FakePhotoMazzaCard from "./FakeCards/FakePhotoMazzaCard";
import AmazonWidget from "../../components/common/AmazonWidget/AmazonWidget";
const siteConfig = _getStaticConfig();

const taswidgetNBTPath = "https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas_new.cms";
const taswidgetMTPath = "https://maharashtratimes.com/pwafeeds/amazon_wdt_tas_new.cms";

class PhotoStoryCard extends Component {
  constructor(props) {
    super(props);

    this.config = {
      adposition: siteConfig.ads.ctnads.adspositionsPhoto,
    };
  }

  componentDidMount() {
    let { picid, parentIndex } = this.props;
    let picElem = document.getElementById(picid);
    //Only scroll for first slideshow
    if (parentIndex == 0) {
      let _timer = setTimeout(() => {
        // picElem ? picElem.scrollIntoView({ behavior: 'smooth', block: 'center'}) : null;
        picElem ? window.scroll({ top: picElem.offsetTop, behavior: "smooth" }) : null;
        clearTimeout(_timer);
      }, 1000);
    }
    Ads_module.render({});
  }

  render() {
    let {
      item,
      view,
      msid,
      isInlineContent,
      parentIndex,
      picid,
      photoshow_app_install,
      showAppInstall,
      openCommentsPopup,
      parentid,
      router,
      pwaConfigAlaska,
    } = this.props;
    let imgMatched = false,
      stopRendering = false;
    let nextItem = item.pwa_meta && item.pwa_meta.nextItem ? item.pwa_meta.nextItem : undefined;
    let pagetype = item.pwa_meta && item.pwa_meta.pagetype ? item.pwa_meta.pagetype : undefined;
    let var_showAppInstall =
      item.items && showAppInstall && showAppInstall(item.items, picid ? picid : msid) ? true : false;
    let app_install_show_slides = photoshow_app_install.show_slides ? photoshow_app_install.show_slides : 0;
    let isAmpPage = false;
    // For nbt, amp widget works from MT domain
    let amazonIframeDataSource =
      process.env.SITE === "nbt"
        ? `${taswidgetMTPath}?host=${process.env.SITE}&tag=amp&pagetype=ps&wdttype=${
            pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
          }&msid=${item && item.id}`
        : `${taswidgetNBTPath}?host=${process.env.SITE}&tag=amp&pagetype=ps&wdttype=${
            pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
          }&msid=${item && item.id}`;
    if (router && router.location.pathname && router.location.pathname.includes("amp_")) {
      isAmpPage = true;
    }

    if (item.items && item.items.length > 0) {
      return (
        <div>
          <ul
            parent-index={parentIndex}
            className={
              typeof view != "undefined"
                ? styles["photo_story"] + " photo_story " + view
                : styles["photo_story"] + " photo_story"
            }
          >
            {item.items.map((it, index) => {
              if (var_showAppInstall && index > app_install_show_slides) {
                return null;
              }

              // For Bot : if server side rendering ,show in bunch of 10 or less , decided by feed nextItem.msid
              if (!_isCSR() && nextItem && nextItem.msid == it.id) stopRendering = true;
              if (stopRendering) return null;

              let _id = it.no === "1" ? msid : `msid-${msid},picid-${it.id}`;
              let url = it.tn == "html" ? it.wu : "/photoshow/" + _id + ".cms";
              url = it.seolocation ? "/" + it.seolocation + url : url;

              return (
                <ErrorBoundary key={index}>
                  {/** Default Amazon Widget */}

                  <PhotoMazzaShowSubCard
                    parentid={parentid}
                    photoshow_app_install={photoshow_app_install}
                    showAppInstall={var_showAppInstall}
                    openCommentsPopup={openCommentsPopup}
                    item={it}
                    url={url}
                    key={index}
                    index={index}
                    count={item.pg.cnt}
                    view={view}
                    isInlineContent={isInlineContent}
                    picid={picid}
                    isAmpPage={isAmpPage}
                  />

                  {index == 3 && ( // After 4th slide, index starts with 0, hence 4th slide comes @ index == 3
                    <li>
                      <AmazonWidget item={it} type="sponsored" router={router} techPageOnly />
                    </li>
                  )}

                  {index == 3 &&
                  pwaConfigAlaska &&
                  (pwaConfigAlaska._topdeal == "true" || pwaConfigAlaska._topoffer == "true") &&
                  ((isTechSite() && !isTechPage(router && router.location && router.location.pathname)) ||
                    !isTechSite()) ? (
                    <li>
                      <div className="wdt_amz pwa-deals">
                        <iframe
                          scrolling="no"
                          frameBorder="no"
                          height={isAmpPage ? "330" : "310"}
                          src={
                            isAmpPage
                              ? amazonIframeDataSource
                              : isMobilePlatform()
                              ? `${taswidgetNBTPath}?host=${process.env.SITE}&pagetype=ps&wdttype=${
                                  pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                                }&msid=${item && item.id}&category=${item &&
                                  item.pwa_meta &&
                                  item.pwa_meta.subsectitle1}`
                              : `${taswidgetNBTPath}?host=${process.env.SITE}&platform=desktop&pagetype=ps&wdttype=${
                                  pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                                }&msid=${item && item.id}&category=${item &&
                                  item.pwa_meta &&
                                  item.pwa_meta.subsectitle1}`
                          }
                        />
                      </div>
                    </li>
                  ) : null}

                  {photoshow_app_install.active === false && index == 3 && isMobilePlatform() ? (
                    <li className="inline_app_download">
                      <div className="table">
                        <div className="table_row">
                          <span className="table_col con_wrap">{siteConfig.locale.seemoreimages}</span>
                          <span className="table_col">
                            <a
                              href={_deferredDeeplink("photo", siteConfig.appdeeplink, parentid)}
                              className="btn_appdownload"
                            >
                              {siteConfig.locale.photoshow_app_install.download}
                            </a>
                          </span>
                        </div>
                      </div>
                    </li>
                  ) : null}
                  {!isInlineContent &&
                  !(item && item.pwa_meta && item.pwa_meta.AdServingRules) &&
                  this.config.adposition &&
                  this.config.adposition.indexOf("-" + (index + 1) + "-") > -1 ? (
                    index + 1 == 2 ? (
                      <div className="ad-wrapper-275">
                        <AdCard mstype="mrec1" adtype="dfp" elemtype="li" />
                      </div>
                    ) : (
                      <div className="ad-wrapper-275">
                        <AdCard mstype="ctnbigphoto" adtype="ctn" elemtype="li" />
                      </div>
                    )
                  ) : null}
                </ErrorBoundary>
              );
            })}
            {this.props.isLoadingSlides && <FakePhotoMazzaCard showImages={true} />}
            {!_isCSR() && nextItem && nextItem.url ? <Link to={nextItem.url} /> : null}
          </ul>

          {process.env.SITE == "nbt" && isMobilePlatform() ? (
            <div
              data-adtype="special"
              className="ad1 special"
              data-id="div-gpt-ad-1564660176863-0"
              data-name={siteConfig.ads.dfpads.gndineoutad}
              data-size="[[320, 50]]"
            />
          ) : null}
        </div>
      );
    } else {
      return null;
    }
  }
}

PhotoStoryCard.propTypes = {
  item: PropTypes.object,
};

export default PhotoStoryCard;
