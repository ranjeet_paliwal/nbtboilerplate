import React, { Component } from "react";
import { Link } from "react-router";
import "./../css/desktop/FakeCard.scss";
class FakeLeadListCard extends Component {
  render() {
    let { showImages } = this.props;
    return (
    <div className="table fake-lead-post">
        <div className="table_row">
            <span className="table_col img_wrap">
                {showImages == "no" ? null : <div className="dummy-img"></div>}
            </span>
        </div>
        <div className="table_row">
            <span className="table_col con_wrap">
                <span className="blur-div"></span>
                <span className="blur-div" style={{ width: "70%" }}></span>
                <span className="blur-div small" style={{ width: "40%" }}></span>
            </span>
        </div>
    </div>
    );
  }
}

export default FakeLeadListCard;
