import React from "react";
import { _isCSR, isMobilePlatform } from "../../../utils/util";

const FakeLoader = props => {
  const { pagetype } = props;
    // Type for eg "page" or "widget"
    // name for eg "widgetname(horizontal/vertical)" or "pagename(home, articleshow, listpage, videoshow, photoshow, videolist, photolist)" 
  return _isCSR() ? (
    // return (
        <div>
            {/* <div className={`trendingfake ${pagetype}`}></div> */}
            <div className={`fakeloading ${pagetype}`}></div>
        </div>
    // )
  ) : null;
};

export default FakeLoader;
