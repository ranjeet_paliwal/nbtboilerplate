/* eslint-disable no-nested-ternary */
import React from "react";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const KeyWordCard = props => {
  const keywords = props;
  let items =
    typeof keywords === "string"
      ? (keywords && keywords.split(",")) || ""
      : typeof keywords === "object" && keywords.items && keywords.items.item
      ? keywords.items.item
      : typeof keywords === "object" && keywords.items
      ? keywords.items
      : undefined;

  if (typeof items === "undefined" && props && props.data && typeof props.data !== "undefined") {
    items = props.data;
  }

  const heading = typeof keywords === "object" ? keywords.secname : undefined;
  if (!items || !Array.isArray(items) || items.length === 0) {
    return null;
  }
  return (
    <React.Fragment>
      {heading !== "" ? (
        heading ? (
          <h3>
            <span>{heading}</span>
          </h3>
        ) : siteConfig.locale.key_words ? (
          <h3>
            <span>{siteConfig.locale.key_words}</span>
          </h3>
        ) : null
      ) : null}
      <div className="scroll_content">
        <div className="nowarp_content">
          {items &&
            Array.isArray(items) &&
            items.map((item, index) => {
              if (typeof item !== "undefined" && typeof item === "string" && item.trim() != "") {
                return (
                  <AnchorLink
                    data-track="keysearch"
                    hrefData={{
                      override: `/topics/${item.trim().replace(/ /g, "-")}`,
                    }}
                    key={index.toString()}
                  >
                    <span>{item.replace(/ /g, " ")}</span>
                  </AnchorLink>
                );
              }
              if (typeof item !== "undefined" && typeof item === "object" && item.tn && item.tn !== "ad") {
                // && item.tn == "news"
                return (
                  <AnchorLink hrefData={item} key={index.toString()}>
                    <span>{item.hl}</span>
                  </AnchorLink>
                );
              }
              return null;
            })}
        </div>
      </div>
    </React.Fragment>
  );
};

// const generateUrl = item => {
//   const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
//   const url = `/${item.seolocation ? `${item.seolocation}/` : ""}${templateName}/${item.id}.cms`;

//   return item.override ? item.override : url;
// };

export default KeyWordCard;
