import React, { PureComponent } from "react";
import Parser from "html-react-parser";
import { isMobilePlatform, _getStaticConfig, _sessionStorage } from "../../utils/util";
const siteConfig = _getStaticConfig();

import KeyWordCard from "./KeyWordCard";
// import AnchorLink from './AnchorLink';
import styles from "./css/Notification-cards.scss";
import { parserOptions } from "./../lib/htmlReactParser/index";

class BreakingNewsCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { showBnews: true };
    this.bnewsScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    const { bnews, trending } = this.props;
    if (bnews || (trending && trending.items)) {
      // setTimeout(() => {
      //   // this.initMove();
      // }, 4000);
      window.addEventListener("scroll", this.bnewsScroll);
    }
  }

  componentDidUpdate() {
    if (isMobilePlatform()) {
      const marqueePlaceholder = document.querySelector(".marquee_content");
      if (marqueePlaceholder) {
        this.calcSpeed(40, marqueePlaceholder);
      }
    }
  }

  // componentDidUpdate() {
  //   const { bnews, trending } = this.props;
  //   if (bnews || (trending && trending.items)) {
  //     clearTimeout(this.timer);
  //     // this.initMove();
  //   }
  // }

  // bindScroll() {
  //   window.addEventListener("scroll", this.bnewsScroll);
  // }
  handleScroll() {
    const bnews = document.querySelector(".card_breaking_news");
    bnews.classList.remove("hideBnews");
    this.unbindScroll();
  }

  unbindScroll() {
    if (isMobilePlatform()) {
      const marqueePlaceholder = document.querySelector(".marquee_content");
      if (marqueePlaceholder) {
        marqueePlaceholder.classList.add("enable_marquee");
      }
    } else {
      this.initMove();
    }
    window.removeEventListener("scroll", this.bnewsScroll);
  }

  calcSpeed = (speed, placeholder) => {
    // Time = Distance/Speed
    var spanLength = placeholder.offsetWidth;
    var timeTaken = spanLength / speed;
    placeholder.style.animationDuration = timeTaken + "s";
  };

  initMove() {
    const marqueePlaceholder = document.querySelector(".marquee_content");
    if (marqueePlaceholder) {
      const position = 0;
      const fullWidth = marqueePlaceholder.offsetWidth;
      // const moving = false;
      this.move(position, fullWidth);
    }
  }

  move(position, fullWidth) {
    const marqueePlaceholder = document.querySelector(".marquee_content");
    if (marqueePlaceholder) {
      marqueePlaceholder.style.left = `${position - 2}px`;
      if (marqueePlaceholder.style.left === `-${fullWidth}px`) {
        position = 100;
      }
      position -= 1;
      this.timer = setTimeout(() => {
        this.move(position, fullWidth);
      }, 20); // 20 milisecond
    }
  }

  closeclick = e => {
    e.preventDefault();
    //Removing parent div having attribute data-id='breakingNews'
    // e.target.closest("[data-id='breakingNews']").remove();

    this.setState({ showBnews: false });
    //set session storage close
    _sessionStorage().set("bnews", "close");
  };

  showTicker = () => {
    // console.log("enter ticker");
    let bnConWidth = document.querySelector('[data-id="bNewsText"]')
      ? document.querySelector('[data-id="bNewsText"]').clientWidth
      : 0;
    let bnScrlWidth = document.querySelector('[data-id="bNewsText"]')
      ? document.querySelector('[data-id="bNewsText"]').scrollWidth
      : 0;
    if (bnScrlWidth > bnConWidth) {
      // console.log("show ticker");
    }
  };

  render() {
    const { bnews, trending } = this.props;

    let cardHeading = "";
    if (bnews) {
      cardHeading = siteConfig.locale.breaking_news;
    } else if (trending) {
      cardHeading = siteConfig.locale.trending;
    }

    if (!bnews && (!trending || !trending.items)) {
      return (
        <div className="breaking_news_card_placeholder">
          <span className="head_wrap"></span>
          <span className="con_wrap"></span>
          <span className="btn_wrap"></span>
        </div>
      );
    }
    // return bnews && bnews.indexOf("href")!== -1 && this.state.showBnews ? (
    return (bnews || (trending && trending.items)) && this.state.showBnews ? (
      <div className="card_breaking_news" data-id="breakingNews">
        <div className="table">
          <span className="table_row">
            {isMobilePlatform() ? (
              <span className="table_col head_wrap wap"></span>
            ) : (
              <span className="table_col head_wrap">{cardHeading}</span>
            )}
            <span className={`table_col con_wrap ${isMobilePlatform() ? "wap" : ""}`}>
              {/* <h3>{siteConfig.locale.breaking_news}</h3> */}
              {/*<span className="text_ellipsis" dangerouslySetInnerHTML={{__html: bnews}}></span>*/}
              <span
                className={`bnew_text marquee_content ${isMobilePlatform() ? "enable_marquee" : ""}`}
                data-id="bNewsText"
              >
                {trending && trending.items && <div className="trending_keywords">{KeyWordCard(trending)}</div>}
                {bnews &&
                  Parser(bnews, {
                    replace: parserOptions.replace.bind(this, bnews),
                  })}
              </span>
            </span>
            <span className="table_col btn_wrap" onClick={this.closeclick}>
              <b className="close_icon" />
            </span>
          </span>
        </div>
        {/* {showTicker()} */}
      </div>
    ) : null;

    {
      /*
    return bnews && bnews.length && bnews.length > 70 ? (
      <div className="bnews-placeholder">
        <span className="left">अभी अभी</span>
        <span className="center">
          <b className="marquee_content" style={style}>
            {bnews}
          </b>
        </span>
        <span className="right">
          <span onClick={this.CloseBNewsHandler} className="close_icon" />
        </span>
      </div>
    ) : null;
    */
    }
  }
}

export default BreakingNewsCard;
