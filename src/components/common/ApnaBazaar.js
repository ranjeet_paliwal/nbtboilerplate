import React, { PureComponent } from "react";
import ImageCard from "./ImageCard/ImageCard";
import "./css/ApnaBazaar.scss";
import AnchorLink from "./AnchorLink";
// import { isFeatureURL } from "../lib/ads/lib/utils";

class ApnaBazaar extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedValue: 0,
    };
    this.config = {
      noLazyLoad: false,
    };
  }

  render() {
    // const data = {
    //   title: "Kent - 11076 New Grand 8-Litres Wall-Mountable RO + UV+ UF + TDS (White) 20 litre/hr Water Purifier",
    //   id: "amazon-prod-123123241",
    //   mrp: "₹ 19,500",
    //   discount: "₹ 6001.00 (31%)",
    //   price: "",
    //   images: {
    //     "0": "https://images-na.ssl-images-amazon.com/images/I/71pP7OvqaRL._SY450_.jpg",
    //     "1": "https://images-na.ssl-images-amazon.com/images/I/41glccnH21L._SY450_.jpg",
    //     "2": "https://images-na.ssl-images-amazon.com/images/I/51xi6GnYYqL._SS40_.jpg",
    //     "3": "https://images-na.ssl-images-amazon.com/images/I/51xi6GnYYqL._SS40_.jpg",
    //     "4": "https://images-na.ssl-images-amazon.com/images/I/51xi6GnYYqL._SS40_.jpg",
    //   },
    //   rating: "4 (3,264 ratings)",
    //   url:
    //     "https://www.amazon.in/dp/B07P5QR6KG/ref=s9_acsd_al_bw_c2_x_0_i?pf_rd_m=A1K21FY43GMZF8&pf_rd_s=merchandised-search-7&pf_rd_r=6T69AVR62RDQGTQAW820&pf_rd_t=101&pf_rd_p=680ae4eb-22a7-4651-9291-838fb1dc2c55&pf_rd_i=17168975031",
    // };

    const { item } = this.props;
    const data = JSON.parse(item);
    const pwa_meta = data && data.pwa_meta;
    let imgObj = data.images ? data.images : {};

    imgObj &&
      Object.keys(imgObj).map(key => {
        let regex = /\.\_\S+_./;
        let val = imgObj[key].replace(regex, "._SY450_.");
        imgObj[key] = val;
      });

    let discntPer = "";
    if (data && data.discount && typeof data.discount === "string") {
      const discountArr = data.discount.split("(");
      if (discountArr && discountArr[1]) {
        discntPer = discountArr[1].replace(")", "");
      }
    }
    const platform = process.env.PLATFORM;

    const { url } = data;
    const isFeaturedArticle = true;

    return (
      <div className="wdt_apnaBazaar">
        <h3 className="abz-head">
          <AnchorLink href={url} className="text_ellipsis">
            {data.title}
          </AnchorLink>
        </h3>
        <div className="abz-slider">
          <ul className="abz-slider-tabs" data-exclude="amp">
            {Object.keys(imgObj).map(key => {
              return (
                <li onClick={() => handleImage(key, this)}>
                  <ImageCard
                    adgdisplay={isFeaturedArticle ? "false" : null}
                    src={imgObj[key]}
                    size="smallthumb"
                    type="absoluteImgSrc"
                  />
                </li>
              );
            })}
          </ul>
          <div className="abz-slider-content">
            <div className="imgwrap">
              {discntPer !== "" ? <span className="discount">{discntPer + " off"}</span> : null}
              <AnchorLink href={url}>
                <ImageCard
                  src={imgObj[this.state.selectedValue]}
                  size="largethumb"
                  type="absoluteImgSrc"
                  noLazyLoad={this.config.noLazyLoad}
                  adgdisplay={isFeaturedArticle ? "false" : null}
                />
              </AnchorLink>
            </div>
          </div>
        </div>
        <div className="abz-description">
          <div className="price-box">
            <div class="price_tag">
              <AnchorLink href={url}>
                {data.price && data.price !== "" ? data.price : data.mrp}
                <b>
                  <span>{data.price && data.price !== "" ? data.mrp : null}</span>
                  {discntPer !== "" ? ` (${discntPer} off)` : null}
                </b>
              </AnchorLink>
            </div>
            <div className="abz-btn">
              <AnchorLink href={url} className="btn">
                {"Get This"}
              </AnchorLink>
              <AnchorLink href={url} className="abz-icon">
                <img src="https://navbharattimes.indiatimes.com/photo/58606011.cms" width="80" />
              </AnchorLink>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const handleImage = (key, self) => {
  self.config.noLazyLoad = true;
  self.setState({
    selectedValue: key,
  });
};

export default ApnaBazaar;
