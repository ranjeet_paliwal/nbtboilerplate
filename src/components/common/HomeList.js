
import React, { PureComponent } from "react";
import { _getStaticConfig } from "../../utils/util";
import fetch from "../../utils/fetch/fetch";
import GridSectionMaker from "./ListingCards/GridSectionMaker";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import SectionHeader from "./SectionHeader/SectionHeader";

const siteConfig = _getStaticConfig();

class HomeList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            listData: null,
        }
    }

    componentDidMount() {
        this.fetchHomeList();
    }

    fetchHomeList() {
        const { config } = this.props;
        let { listData } = this.state;
        const listApiUrl = config._count ? `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${config._sec_id}&type=${config._type}&feedtype=sjson&count=${config._count}`
            : `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${config._sec_id}&type=${config._type}&feedtype=sjson`;
        fetch(listApiUrl).then(
            data => {
                listData = data;
                this.setState({ listData });
            }
        )

    }

    render() {
        const { listData } = this.state;
        return (
            (listData ?
                <div className="row box-item">
                    <SectionHeader sectionhead={siteConfig.locale.top_headlines}
                    />
                    <GridSectionMaker
                        type={defaultDesignConfigs.listGallery}
                        data={listData.items}
                        override={{ istrending: "true" }}
                    />
                </div> : null)
        )
    }

}
export default HomeList;
