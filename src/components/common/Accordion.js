import React from "react";

const Accordion = props => {
  if (!props.heading) {
    return;
  }
  return (
    <li className="accordion active" onClick={e => toggleDescription(e)}>
      {props.heading}
      <div
        className="description"
        dangerouslySetInnerHTML={{
          __html: props.description,
        }}
      />
    </li>
  );
};

const toggleDescription = e => {
  const descriptionEle = e && e.currentTarget && e.currentTarget.querySelector(".description");
  if (descriptionEle.className.indexOf("hide") > 0) {
    descriptionEle.classList.remove("hide");
    e.currentTarget.classList.add("active");
  } else {
    descriptionEle.classList.add("hide");
    e.currentTarget.classList.remove("active");
  }
};

export default Accordion;
