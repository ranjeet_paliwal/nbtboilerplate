import React from "react";
import { isMobilePlatform, _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

export const CriticReview = ({ data, userRating, criticRating }) => {
  let topfeatureArray = data ? (Array.isArray(data) ? data : [data]) : null;
  return isMobilePlatform() ? (
    <span className="image_caption">
      <div className="table rating">
        <meta itemProp="author" />
        <div className="table_row">
          <span className="table_col">
            <span className="txt critic">{siteConfig.locale.tech.criticsrating}</span>
            <span className="count">
              {criticRating == 0 ||
              criticRating == undefined ||
              criticRating == "NaN" ||
              // Check if parsing criticRating gives value > 0
              !Boolean(parseInt(criticRating)) ? (
                <span>
                  NA
                  <meta content="1" />
                </span>
              ) : (
                <span>
                  <b>{criticRating}</b>/5
                </span>
              )}
            </span>
          </span>
          {userRating && (
            <span className="table_col">
              <span className="txt users">{siteConfig.locale.tech.userrating}</span>
              <span className="count">
                {userRating && userRating != "NaN" && userRating != "" ? (
                  <span>
                    <b>{userRating == "NaN" ? "NA" : userRating}</b>/5
                  </span>
                ) : (
                  "NA"
                )}
              </span>
            </span>
          )}
        </div>
      </div>
    </span>
  ) : (
    <span className="image_caption">
      <span className="lft">
        <span className="head">
          <b className="icon_star one" />
          {siteConfig.locale.critics_rating}
        </span>
        {/* https://timesgroup.jira.com/browse/NBT-12227 Below change should work for both 0 and Non numbers */}
        {Number.parseInt(criticRating) ? (
          <span className="rate_txt">
            {Number.parseFloat(criticRating).toFixed(1)}
            <sub>/5</sub>
          </span>
        ) : (
          "NA"
        )}
      </span>
      {topfeatureArray ? (
        <span className="rgt">
          <span className="head">{siteConfig.locale.tech.topfeature}</span>
          <ul className="features_txt">
            {topfeatureArray.map(topFeatData => (
              <li key={topFeatData}>{topFeatData}</li>
            ))}
          </ul>
        </span>
      ) : (
        ""
      )}
    </span>
    //   </div>
    //   <span className="review_text">{data.artsyn}</span>
    //   <Link to={data.url} className="more-btn">
    //     {Config.Locale.readCompleteReview}
    //   </Link>
    // </div>
  );
};
