import React from "react";
import AnchorLink from "../AnchorLink";
import { _getStaticConfig, _isCSR } from "../../../utils/util";
import SvgIcon from "../SvgIcon";
const siteConfig = _getStaticConfig();

const SocialIcons = ({ icons }) => (
  <React.Fragment>
    {icons && icons.indexOf("fb") > -1 && (
      <a href={siteConfig.fb} className="facebook" target="_blank" title="facebook" rel="nofollow noopener">
        <SvgIcon name="facebook" center="center" />
      </a>
    )}
    {icons && icons.indexOf("tw") > -1 && (
      <a href={siteConfig.twitter} className="twitter" target="_blank" title="twitter" rel="nofollow noopener">
        <SvgIcon name="twitter" center="center" />
      </a>
    )}
    {icons && icons.indexOf("yt") > -1 && (
      <a className="youtube" href={siteConfig.youtubeUrl} target="_blank" title="youtube" rel="nofollow noopener">
        <SvgIcon name="youtube-new" center="center" />
      </a>
    )}
    {icons && icons.indexOf("telegram") > -1 && (
      <AnchorLink href={siteConfig.telegramFollowLink} className="telegram" title="telegram">
        <SvgIcon name="telegram" className="telegram-icon" center="center" />
      </AnchorLink>
    )}
  </React.Fragment>
);

const SocialMediaChannels = React.memo(props => {
  const {
    parentClass,
    heading,
    icons,
    dropdown,
  } = props;
  
  const TagName = dropdown ? "span" : React.Fragment;
  const TagNameDrop = dropdown ? "div" : React.Fragment;
  const iconsArr = icons && icons.split(",");

  return (
    <div className={parentClass}>
      {heading ? <span className="heading">FOLLOW US ON</span> : ""}
      <span className="icons">
        {icons && icons.indexOf("app") > -1 && (
          <a href={`${siteConfig.weburl}${siteConfig.applist}`} className="app_icon" target="_blank" title="applist">
            <SvgIcon name="mobile-phone" center="center" />
          </a>
        )}
        {icons && icons.indexOf("rss") > -1 && (
          <a href={`${siteConfig.weburl}${siteConfig.rss}`} className="rssdef" target="_blank" title="rss">
            <SvgIcon name="rssfeed" center="center" />
          </a>
        )}
        <TagName className="social_icons">
          {dropdown && <span className="shareico" title="social share"><SvgIcon name="share" center="center" /></span>}
          <TagNameDrop className="social_drop">
            {iconsArr.map((icons, index) => (
              <SocialIcons icons={icons} />
            ))}
          </TagNameDrop>
        </TagName>
      </span>
    </div>
  );
});



export default SocialMediaChannels;
