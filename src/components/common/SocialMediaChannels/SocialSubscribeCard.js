/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-nested-ternary */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { _getStaticConfig, getMobileOs, _deferredDeeplink, filterAlaskaData } from "../../../utils/util";
import AnchorLink from "../AnchorLink";
import SvgIcon from "../SvgIcon";
const siteConfig = _getStaticConfig();

const SocialMediaChannels = props => {
  const { fb, twitter, youtubeUrl, telegramFollowLink, kooUrl } = siteConfig;
  const { telegramLink, header } = props;
  const fbSocialData = filterAlaskaData(header.alaskaData, ["pwaconfig", "Facebook"], "label");
  const twitterSocialData = filterAlaskaData(header.alaskaData, ["pwaconfig", "Twitter"], "label");
  const tgSocialData = filterAlaskaData(header.alaskaData, ["pwaconfig", "Telegram"], "label");
  const ytSocialData = filterAlaskaData(header.alaskaData, ["pwaconfig", "Youtube"], "label");
  const kooSocialData = filterAlaskaData(header.alaskaData, ["pwaconfig", "Koo"], "label");
  const fbData = fbSocialData && fbSocialData._socialcount ? fbSocialData._socialcount : undefined;
  const twitterData = twitterSocialData && twitterSocialData._socialcount ? twitterSocialData._socialcount : undefined;
  const telegramData = tgSocialData && tgSocialData._socialcount ? tgSocialData._socialcount : undefined;
  const ytData = ytSocialData && ytSocialData._socialcount ? ytSocialData._socialcount : undefined;
  const kooData = kooSocialData && kooSocialData._socialcount ? kooSocialData._socialcount : undefined;

  return (
    <ul>
      <li>
        <a data-link="appdownload" href={_deferredDeeplink("article_end", siteConfig.appdeeplink)}>
          <span
            className={`iconcircle app-install ${
              getMobileOs() === "android" ? "android" : getMobileOs() === "ios" ? "ios" : "btn"
            }`}
          >
            {getMobileOs() === "android" ? (
              <SvgIcon name="android" />
            ) : getMobileOs() === "ios" ? (
              <SvgIcon name="apple" />
            ) : (
              <SvgIcon name="mobile-phone" className="mobile" />
            )}
          </span>
          <label>Install</label>
        </a>
      </li>
      <li>
        <AnchorLink href={fb} title="facebook">
          <span className="iconcircle fb-like">
            <SvgIcon name="facebook" />
          </span>
          <label>
            <GenericCountComponent data={fbData} dataTxt="Likes" fallbackTxt="Like Us" />
          </label>
        </AnchorLink>
      </li>
      <li>
        <AnchorLink href={twitter} title="twitter">
          <span className="iconcircle twitter">
            <SvgIcon name="twitter" />
          </span>
          <label>
            <GenericCountComponent data={twitterData} dataTxt="Followers" fallbackTxt="Follow Us" />
          </label>
        </AnchorLink>
      </li>
      <li>
        <AnchorLink href={youtubeUrl} title="youtube">
          <span className="iconcircle youtube">
            <SvgIcon name="youtube" />
          </span>
          <label>
            <GenericCountComponent data={ytData} dataTxt="Subscribers" fallbackTxt="Subscribe" />
          </label>
        </AnchorLink>
      </li>
      <li>
        <AnchorLink href={telegramLink || telegramFollowLink}>
          <span className="iconcircle telegram">
            <SvgIcon name="telegram" />
          </span>
          <label>
            <GenericCountComponent data={telegramData} dataTxt="Followers" fallbackTxt="Follow Us" />
          </label>
        </AnchorLink>
      </li>
      {kooUrl && (
        <li>
          <AnchorLink href={kooUrl}>
            <span className="iconcircle koo">
              <SvgIcon name="kooicon" />
            </span>
            <label>
              <GenericCountComponent data={kooData} dataTxt="Followers" fallbackTxt="Follow Us" />
            </label>
          </AnchorLink>
        </li>
      )}
    </ul>
  );
};

function arePropsEqual(prevProps, nextProps) {
  // check any case to rerender otherwise never rerender
  return true;
}

SocialMediaChannels.propTypes = {
  telegramLink: PropTypes.string,
  header: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    header: state && state.header,
  };
}

const GenericCountComponent = props => {
  const { data, dataTxt, fallbackTxt } = props;
  return data ? (
    <React.Fragment>
      <b>{data}</b>
      {dataTxt}
    </React.Fragment>
  ) : (
    <React.Fragment>{fallbackTxt}</React.Fragment>
  );
};

GenericCountComponent.propTypes = {
  data: PropTypes.string,
  dataTxt: PropTypes.string,
  fallbackTxt: PropTypes.string,
};

// React memo is used to stop rerendering of the component if the props remain same
export default connect(mapStateToProps)(React.memo(SocialMediaChannels, arePropsEqual));
