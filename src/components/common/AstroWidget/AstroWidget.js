import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import fetch from "../../../utils/fetch/fetch";
// import ImageCard from "../ImageCard";
// import AnchorLink from "../AnchorLink";
// import TabContainer from "../TabContainer";
import FakeNewsListCard from "../FakeCards/FakeNewsListCard";
import Slider from "../../desktop/Slider/index";
// import fetchDataIfNeeded from '../../../actions/home/astrowidget/astrowidget';
import "./AstroWidget.scss";

import { _getStaticConfig, generateUrl } from "./../../../utils/util";
const siteConfig = _getStaticConfig();

class AstroWidget extends PureComponent {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     AstroData: "",
  //   };
  // }

  // componentDidMount() {
  //   const _this = this;
  //   fetch(
  //     `https://navbharattimes.indiatimes.com/pwafeeds/web_astro.cms?datatype=signs_data&name=aries&feedtype=sjson`,
  //   ).then(function(response) {
  //     // console.log("sss", response);
  //     _this.setState({
  //       AstroData: response,
  //     });
  //   });
  // }

  // AstroClickHandler(event) {
  //   // const { dispatch } = this.props;
  //   const _this = this;
  //   const api1 = `https://navbharattimes.indiatimes.com/pwafeeds/web_astro.cms?datatype=signs_data&name=${event.target.id.trim()}&feedtype=sjson`;
  //   fetch(api1)
  //     .then(response => {
  //       // dispatch(updateSignData());
  //       _this.setState({
  //         AstroData: response,
  //       });
  //     })
  //     .catch(() => {});
  // }

  render() {
    // const { AstroData } = this.state;
    const { item } = this.props;
    const astroLink = generateUrl(item);

    return (
      <li className="astro_slider">
        <Slider
          type="zodiac_sign"
          size="5"
          sliderData={siteConfig.ZodiacSigns.data}
          clicked={event => this.AstroClickHandler(event)}
          width="60"
          islinkable="false"
          link={astroLink}
          margin="5"
        />
      </li>
    );
  }
}

AstroWidget.propTypes = {
  value: PropTypes.array,
  children: PropTypes.node,
  dispatch: PropTypes.any,
  params: PropTypes.any,
};

export default AstroWidget;
