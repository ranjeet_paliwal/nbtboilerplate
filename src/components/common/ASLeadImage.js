import React from "react";
import { checkIsAmpPage, generateUrl, isMobilePlatform, _getStaticConfig, _isCSR } from "../../utils/util";
import AdCard from "./AdCard";
import { CriticReview } from "./CriticReview";
import ImageCard from "./ImageCard/ImageCard";
import { SeoSchema } from "./PageMeta";
import VideoPlayer from "../../modules/videoplayer/player";
import YSubscribeCard from "./YSubscribeCard";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import GridSectionMaker from "./ListingCards/GridSectionMaker";
import { createConfig } from "../../modules/videoplayer/utils";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import { isFeatureURL } from "../lib/ads/lib/utils";

const siteConfig = _getStaticConfig();

const ASLeadImage = ({ item }) => {
  const { ag, lead, pwa_meta, image } = item;
  const isFeaturedArticle = isFeatureURL(item.wu);
  const isAmpPage = checkIsAmpPage(item.wu);
  const largethumbRatio = 0.75; // height/width

  //for BrandWire and Colombia return null
  if (ag && (ag.toLowerCase() == "mediawire" || ag.toLowerCase() == "brandwire" || ag.toLowerCase() == "colombia")) {
    return null;
  } else if (lead && lead.image && lead.image[0]) {
    return (
      <React.Fragment>
        <div className={item.ismarkreview ? "thumb_img_reviews" : "img_wrap"}>
          <ImageCard
            noLazyLoad={true}
            msid={lead.image[0].id}
            size={!isMobilePlatform() ? "gallerythumb" : "largethumb"}
            className="asleadimg"
            pagetype="articleshow"
            title={lead.image[0].title}
            alt={lead.image[0].title}
            imgsize={lead.image[0].imgsize && lead.image[0].imgsize != "" ? lead.image[0].imgsize : ""}
            schema="true"
            adgdisplay={
              (pwa_meta && pwa_meta.AdServingRules && pwa_meta.AdServingRules === "Turnoff") || isFeaturedArticle
                ? "false"
                : null
            }
            style={{
              height: largethumbRatio && !isAmpPage && isMobilePlatform() ? largethumbRatio * 92 + "vw" : "auto", // 92 --> 4% padding on parent container on both sides(left-right)
            }}
          />
          {item.ismarkreview ? (
            <CriticReview
              data={item.topfeature}
              userRating={item.gadgetinfo && item.gadgetinfo.ur}
              criticRating={item.cr}
            />
          ) : lead.image[0].cap ? (
            <div className="img_caption" dangerouslySetInnerHTML={{ __html: lead.image[0].cap }}></div>
          ) : null}
        </div>
        {/* {banner && <Banner data={banner} divTag />} */}
        {/* BCCL CTN */}
        {/* {isMobilePlatform() && (
          <div className="bccl-ad-wrapper" data-exclude="amp">
            <AdCard className="emptyAdBox" mstype="ctnbccl" adtype="ctn" />
          </div>
        )} */}
      </React.Fragment>
    );
  } else if (lead && lead.vdo) {
    //call for common create config for settig up slike config as per template
    let relatedVideos = [];
    let config = createConfig(lead.vdo[0], pwa_meta.subsecmsid2, siteConfig, "articleshow", item);
    if (item && item.recommended && item.recommended.relatedvideos && item.recommended.relatedvideos.item) {
      const videoItems = item.recommended.relatedvideos.item;
      if (Array.isArray(videoItems) && videoItems.length > 0) {
        relatedVideos = videoItems.slice(0, 3);
      } else {
        relatedVideos.push(videoItems);
      }
    }

    return (
      <div
        // data-attr-slk={lead.vdo[0].id ? lead.vdo[0].id : null}
        data-videourl={generateUrl(lead.vdo[0])}
      >
        <span {...SeoSchema({ pagetype: "articleshow" }).videoattr().videoObject}>
          {SeoSchema({ pagetype: "articleshow" }).name(lead.vdo[0].hl)}
          {SeoSchema({ pagetype: "articleshow" }).duration(lead.vdo[0].vdu)}
          {SeoSchema({ pagetype: "articleshow" }).uploadDate(lead.vdo[0].dlm)}
          {SeoSchema({ pagetype: "articleshow" }).artdesc(lead.vdo[0].Story)}
          {SeoSchema({ pagetype: "articleshow" }).thumbnail(lead.vdo[0].img)}
          {SeoSchema({ pagetype: "articleshow" }).contentUrl(lead.vdo[0].pu)}
          {SeoSchema({ pagetype: "articleshow" }).embedUrl(lead.vdo[0].embed)}
        </span>
        <div>
          <VideoPlayer
            key={lead.vdo[0].eid}
            data-videourl={generateUrl(lead.vdo[0])}
            autoDock
            autoplay
            // onVideoClick={onVideoClickHandler.bind(this)}
            // apikey={slikeApiKey}
            // wrapper="masterVideoPlayer"
            // inline={true}
            config={config}
            imageid={lead.vdo[0].imageid}
            videoMsid={lead.vdo[0].id}
            lead={true}
            schema={true}
            imgsize={lead.vdo[0].imgsize}
          />
        </div>
        {lead.vdo[0] && lead.vdo[0].cap && <div className="vdo-caption"> {lead.vdo[0].cap}</div>}
        {!isAmpPage ? <YSubscribeCard /> : null}
        <div className="popular-inline-video">
          {/*  Trending videos for articleshow */}
          {!isMobilePlatform() && lead && lead.vdo && relatedVideos.length > 0 ? (
            <ErrorBoundary>
              <h3>{siteConfig.locale.most_viewed_videos}</h3>
              <GridSectionMaker type={defaultDesignConfigs.gridHorizontalAL} data={relatedVideos} />
            </ErrorBoundary>
          ) : null}
        </div>
        {/* {banner && <Banner data={banner} divTag />} */}
        {/* BCCL CTN */}
        {/* {isMobilePlatform() && (
          <div className="bccl-ad-wrapper" data-exclude="amp">
            <AdCard className="emptyAdBox" mstype="ctnbccl" adtype="ctn" />
          </div>
        )} */}
      </div>
    );
  } else if (image && image.length > 0) {
    return (
      <div className="img_wrap">
        <ImageCard
          title={image[0].title}
          alt={image[0].title}
          noLazyLoad={!_isCSR()}
          msid={image[0].id}
          size="largethumb"
          schema
          adgdisplay={
            (pwa_meta && pwa_meta.AdServingRules && pwa_meta.AdServingRules === "Turnoff") || isFeaturedArticle
              ? "false"
              : null
          }
        />
        {image[0].cap && <div className="img_caption" dangerouslySetInnerHTML={{ __html: image[0].cap }}></div>}
      </div>
    );
  } // To return a default thumb in case no image of article exists
  else {
    return (
      <div className="img_wrap">
        <ImageCard
          noLazyLoad={!_isCSR()}
          pagetype="articleshow"
          size="largethumb"
          schema
          adgdisplay={
            (pwa_meta && pwa_meta.AdServingRules && pwa_meta.AdServingRules === "Turnoff") || isFeaturedArticle
              ? "false"
              : null
          }
        />
      </div>
    );
  }
};

export default ASLeadImage;
