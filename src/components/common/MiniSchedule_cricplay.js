import React from "react";
//import { Link } from 'react-router'
import PropTypes from "prop-types";
import "./css/MiniSchedule.scss";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const MiniSchedule_cricplay = props => {
  let schedule =
    props.schedule &&
    props.schedule.schedule &&
    props.schedule.schedule.length > 0
      ? props.schedule.schedule
      : null;
  return schedule && typeof schedule != "null"
    ? schedule.map((item, index) => {
        return (
          <div className="wdt_schedule">
            <div className="match_date_venue">
              {item.venue}{" "}
              <span>
                {item.matchdate} - {item.matchtime}
              </span>
            </div>
            <div className="liveScore table">
              <div className="teama table_col">
                <span className="teaminfo">
                  <img height="50" width="50" src={item.teamalogo}></img>
                  <b className="country">{item.teama}</b>
                </span>
              </div>
              <div className="versus table_col">
                <span>VS</span>
              </div>
              <div className="teamb table_col">
                <span className="teaminfo">
                  <img height="50" width="50" src={item.teamblogo}></img>
                  <b className="country">{item.teamb}</b>
                </span>
              </div>
            </div>
          </div>
        );
      })
    : null;
};

MiniSchedule_cricplay.propTypes = {
  item: PropTypes.object
};

export default MiniSchedule_cricplay;
