import React from "react";
import PropTypes from "prop-types";
import { _loadJS, _isCSR } from "./../../utils/util";

const GRecaptch = props => {
  if (_isCSR() && typeof grecaptcha == "undefined") {
    _loadJS({ scriptpath: "https://www.google.com/recaptcha/api.js" });
  }

  return (
    <div
      className={props.styles + " g-recaptcha"}
      data-sitekey={props.gRecaptchaKey}
      id={props.id}
    ></div>
  );
};

export default GRecaptch;
