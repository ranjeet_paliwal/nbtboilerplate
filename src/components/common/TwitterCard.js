import React, { Component } from "react";
import { withRouter } from "react-router";
import { _isCSR, checkIsFbPage, checkIsAmpPage } from "../../utils/util";

class TwitterCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scroll: false,
    };

    this.scrollRef = this.handleScroll.bind(this);
    if (_isCSR()) {
      window.addEventListener("scroll", this.scrollRef);
    }
  }

  handleScroll() {
    this.setState({ scroll: true });
    // this.unbindScroll();
  }

  unbindScroll() {
    window.removeEventListener("scroll", this.scrollRef);
  }

  createTwitterWidget(twitterId) {
    if (typeof window !== "undefined") {
      setTimeout(() => {
        const eleContainer = document.getElementById(`container_${twitterId}`);
        if (typeof twttr !== "undefined" && eleContainer && !eleContainer.getAttribute("data-render")) {
          eleContainer.setAttribute("data-render", true);
          twttr.widgets.createTweet(twitterId, eleContainer);
        }
      }, 500);
    }
  }

  render() {
    const { twitterId, router } = this.props;

    return this.state.scroll || checkIsAmpPage(router.location.pathname) || checkIsFbPage(router.location.pathname) ? (
      <div>
        <div className="twitter_widget" id={`container_${twitterId}`}></div>
        {this.createTwitterWidget(twitterId)}
      </div>
    ) : null;
  }
}

export default withRouter(TwitterCard);
