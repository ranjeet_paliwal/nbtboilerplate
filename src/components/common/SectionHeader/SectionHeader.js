import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import AnchorLink from "../AnchorLink";
import { filterAlaskaData, getAlaskaSections, isMobilePlatform, _getStaticConfig } from "../../../utils/util";
import ImageCard from "../ImageCard/ImageCard";
import SelectCity from "../SelectCity/SelectCity";
const siteConfig = _getStaticConfig();
/**
 * @data : Alaska data Object;
 * @sectionId : section id for filtering subsection from Alaska Data;
 * @sectionhead : custom lable for section heading;
 * @weblink : link;
 * @headingTag : html tag for heading;
 * @count : subsections count that will be visible in first view;
 */

class SectionHeader extends PureComponent {
  componentDidUpdate() {
    const { sectionId } = this.props;
    if (typeof document !== "undefined") {
      const elmUL = document.querySelector(`#sub-list-${sectionId} ul`);
      if (elmUL && elmUL.clientHeight <= 35) {
        const dottedElm = document.querySelector(`#sub-list-${sectionId} .more-sublist`);
        if (dottedElm) {
          dottedElm.classList.remove("more-sublist");
        }
      }
    }
  }

  showAllSection = sectionId => {
    // const { sectionId } = this.props;
    const elm = document.getElementById(`sub-list-${sectionId}`);
    if (elm) {
      elm.classList.remove("hide-extra");
    }
  };

  hideAllSection = sectionId => {
    // const { sectionId } = this.props;
    const elm = document.getElementById(`sub-list-${sectionId}`);
    if (elm) {
      elm.classList.add("hide-extra");
    }
  };

  render() {
    const {
      data,
      sectionId,
      sectionhead,
      weblink,
      headingTag,
      datalabel,
      sectionTag,
      morelink,
      headerClass,
      headerLinkParams,
      sectionIdArr,
      pageH1Value,
      imageId,
      parentSecId,
      cityStateId,
    } = this.props;
    // send data blank in case sectionhead is there
    // const [isVisible, updateStatus] = useState(false);
    const morelinkVisible = morelink === undefined ? true : morelink;
    let subsections = [];
    let sectionData = "";
    let cityStateSectionData = "";
    // let subsecFirstView = "";
    // let subsecMoreView = "";
    // const Header = headingTag || "h3"; // h3 is reserved for sub headlines for SEO purpose
    /* As Per SEO comments Page Heading should be H1, Sub sections should be marked as H2
       For sections under sub-sections we will mark it as h3. Pass 'H1' and 'H3' from your
        page as a "headingTag" props 
    */
    const Header = headingTag || "h2";
    const hideExtraClass = typeof isMobilePlatform === "function" && !isMobilePlatform() ? " hide-extra" : "";
    let parentSec_Id = parentSecId && parentSecId != '' && parentSecId != undefined  ? parentSecId : sectionId;
    let showCityDropDown = siteConfig.pages.stateSection && siteConfig.pages.stateSection.includes(parentSec_Id);

    if (data && typeof data === "object" && data.constructor === Object && (sectionId || sectionIdArr)) {
      // sectionId = sectionId == "74078287" ?  "17127056": sectionId;
      if (sectionIdArr) {
        sectionData = sectionIdArr.map(secId => filterAlaskaData(data, secId, "searchAll"));
        subsections = sectionData;
      } else {
        sectionData = filterAlaskaData(data, sectionId);
        subsections = getAlaskaSections(sectionData);
      }
      // subsecFirstView = subsections ? subsections.splice(0, count) : "";
      // subsecMoreView = subsections;
      if (cityStateId && cityStateId != undefined) {
        cityStateSectionData = filterAlaskaData(data, cityStateId, "searchAll");
      }
    }

    if (sectionData) {
      return (
        <div
          key={sectionId}
          className={`section ${headerClass}`}
          onMouseLeave={this.hideAllSection.bind(this, sectionId)}
        >
          <LabelSection
            link={
              headerLinkParams
                ? sectionData.weblink + headerLinkParams
                : cityStateSectionData != ""
                ? cityStateSectionData.weblink
                : sectionData.weblink
            }
            label={
              cityStateSectionData != ""
                ? cityStateSectionData.label
                : pageH1Value
                ? pageH1Value
                : sectionData.label == "HomePageWidgets"
                ? sectionData._customheading
                  ? sectionData._customheading
                  : sectionhead
                : sectionData.label
                ? sectionData.label
                : sectionhead
            }
            Header={Header}
            sectionTag={sectionTag}
            morelinkVisible={morelinkVisible}
            hardNav={headerLinkParams}
            parentSecId={parentSec_Id}
            showCityDropDown={showCityDropDown}
            data={data}
          />
          {subsections && subsections.length > 0 && sectionData.label !== "HomePageWidgets" && (
            <div className={`sub-section${hideExtraClass}`} id={`sub-list-${sectionId}`}>
              <ul className="sub-list">
                {subsections.map((item, index) => (
                  <li key={`${index.toString()}${datalabel}${item.msid}`}>
                    <AnchorLink href={item.weblink}>{item.label}</AnchorLink>
                  </li>
                ))}
              </ul>

              {typeof isMobilePlatform === "function" && !isMobilePlatform() ? (
                <span
                  className="more-sublist"
                  onMouseOver={this.showAllSection.bind(this, sectionId)}
                  onFocus={this.showAllSection.bind(this, sectionId)}
                >
                  <b />
                </span>
              ) : (
                ""
              )}
            </div>
          )}

          {isMobilePlatform() && showCityDropDown && (
            <div className="selectCity_wrapper">
              <SelectCity sectionId={process.env.SITE == "tlg" ? parentSec_Id : siteConfig.pages.stateSection} />
            </div>
          )}
        </div>
      );
    }

    if (sectionhead && weblink) {
      return (
        <div key={sectionhead} className={`section ${headerClass}`}>
          <LabelSection
            link={headerLinkParams ? weblink + headerLinkParams : weblink}
            label={pageH1Value || sectionhead}
            Header={Header}
            sectionTag={sectionTag}
            morelinkVisible={morelinkVisible}
            hardNav={headerLinkParams}
            parentSecId={parentSec_Id}
            showCityDropDown={showCityDropDown}
          />
        </div>
      );
    }
    if (sectionhead) {
      return (
        <div key={sectionhead} className={`section ${headerClass}`}>
          <div
            className={
              !isMobilePlatform() &&
              siteConfig.pages.stateSection &&
              siteConfig.pages.stateSection.includes(parentSecId)
                ? "top_section city_section"
                : "top_section"
            }
          >
            <Header>
              {imageId ? (
                // ImageID support for Event Banner
                <ImageCard msid={imageId} title="" size="smallthumb" />
              ) : (
                <React.Fragment>
                  <span>{pageH1Value || sectionhead}</span>
                  {!isMobilePlatform() && showCityDropDown && (
                      <div className="selectCity_wrapper">
                        <SelectCity
                          sectionId={process.env.SITE == "tlg" ? parentSecId : siteConfig.pages.stateSection}
                        />
                      </div>
                    )}
                </React.Fragment>
              )}
            </Header>
          </div>
          {isMobilePlatform() && showCityDropDown && (
            <div className="selectCity_wrapper">
              <SelectCity sectionId={process.env.SITE == "tlg" ? parentSecId : siteConfig.pages.stateSection} />
              </div>
            )}
        </div>
      );
    }

    return "";
  }
}

SectionHeader.propTypes = {
  sectionId: PropTypes.string,
  data: PropTypes.object,
  sectionhead: PropTypes.string,
  weblink: PropTypes.string,
  headingTag: PropTypes.string,
  datalabel: PropTypes.string,
  sectionTag: PropTypes.string,
  morelink: PropTypes.bool,
};

// eslint-disable-next-line arrow-body-style
const LabelSection = ({ link, label, Header, sectionTag, morelinkVisible, hardNav, parentSecId, showCityDropDown, data }) => {

  return link || label ? (
    <div
      className={
        !isMobilePlatform() && showCityDropDown
          ? "top_section city_section"
          : "top_section"
      }
    >
      <Header>
        {label && (
          <span>{sectionTag !== "ibeatmostread" && link ? <AnchorLink href={link}>{label}</AnchorLink> : label}</span>
        )}
        {!isMobilePlatform() && showCityDropDown && (
          <div className="selectCity_wrapper">
            <SelectCity sectionId={process.env.SITE == "tlg" ? parentSecId : siteConfig.pages.stateSection} />
          </div>
        )}
      </Header>
      {sectionTag !== "ibeatmostread" && link && morelinkVisible && (
        <AnchorLink className="read_more" href={link} hardNav={hardNav}>
          {siteConfig.locale.readmore}
        </AnchorLink>
      )}
    </div>
  ) : (
    ""
  );
};

LabelSection.propTypes = {
  link: PropTypes.string,
  label: PropTypes.string,
  Header: PropTypes.any,
  sectionTag: PropTypes.string,
  morelinkVisible: PropTypes.bool,
};

export default SectionHeader;
