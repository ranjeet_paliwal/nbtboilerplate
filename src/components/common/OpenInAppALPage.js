import React from "react";
import ReactDOM from "react-dom";
import { getPageType, _isCSR, isMobilePlatform, _getStaticConfig, _deferredLinkAtListPage } from "../../utils/util";
const siteConfig = _getStaticConfig();

class OpenInAppAtALPage extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {}

    render() {
        const { compType, msid, isAmpPage, secName } = this.props;
        return (
            <React.Fragment>
                {/* Open in App Button Floating at ListPage*/
                    isMobilePlatform() && msid != "" && compType === 'articlelist' ? (
                        _isCSR() ? (
                            ReactDOM.createPortal(
                                <a
                                    className="btn_openinapp"
                                    data-attr="btn_openinapp"
                                    style={!isAmpPage ? { display: "none" } : null}
                                    href={_deferredLinkAtListPage(
                                        siteConfig.appdeeplink,
                                        msid,
                                        secName,
                                    )}
                                >
                                    {siteConfig.locale.openinapp}
                                </a>,
                                document.getElementById("share-container-wrapper"),
                            )
                        ) : (
                                <a
                                    className="btn_openinapp"
                                    data-attr="btn_openinapp"
                                    style={!isAmpPage ? { display: "none" } : null}
                                    href={_deferredLinkAtListPage(
                                        siteConfig.appdeeplink,
                                        msid,
                                        secName,
                                    )}
                                >
                                    {siteConfig.locale.openinapp}
                                </a>
                            )
                    ) : null}
            </React.Fragment>
        )
    }      
}

export default OpenInAppAtALPage;
