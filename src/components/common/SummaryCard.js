import React from "react";
import ImageCard from "./ImageCard/ImageCard";

import styles from "./css/SummaryCard.scss";
import { SeoSchema } from "./PageMeta";
import { _isCSR, isMobilePlatform } from "../../utils/util";
import { AnalyticsGA } from "../lib/analytics";
import RattingCard from "./RattingCard";

import { _getStaticConfig } from "../../utils/util";
import AnchorLink from "./AnchorLink";
import SocialShare from "./SocialShare";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
// import { getTpConfig } from "../common/TimesPoints/TimesPointConfig";
import TPicon from "./TimesPoints/TPWidgets/TPicon";
import SvgIcon from "./SvgIcon";
import ASLeadImage from "./ASLeadImage";
import YSubscribeCard from "../common/YSubscribeCard";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;
const slikeApiKey = siteConfig.slike.apikey;
const slikeAdCodes = siteConfig.slikeAdCodes;

const SummaryCard = props => {
  let { item, pagetype, _onlyleadimage, sharedata, openCommentsPopup, router } = props;
  let title = item.hl,
    timecaption = item.lu,
    agency = item.ag,
    { image, brandstory, lead, pwa_meta, banner } = item;
  const generateUrl = item => {
    // if(item.override) {
    //   return item.override;
    // } else {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return url;
    // }
  };

  const onVideoClickHandler = event => {
    let _this = this;
    // event.currentTarget.previousSibling.innerText
    let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
    if (videourl) {
      try {
        //To maintain previous url
        window.history.pushState({}, "", videourl);
        AnalyticsGA.pageview(location.origin + videourl);
      } catch (ex) {
        console.log("Exception in history: ", ex);
      }
    }
  };

  return (
    <React.Fragment>
      {// show lead image with h1 and sysnopsys
      !_onlyleadimage ? (
        <div className="news_card" data-attr="news_card">
          <h1 {...SeoSchema({ pagetype: pagetype }).attr().pageheadline}>
            {title}

            {item.disclaimer_image ? (
              <img
                valign="middle"
                alt="fake-news"
                className="smileyicon"
                src={`${siteConfig.mweburl}/photo/${item.disclaimer_image}.cms`}
              />
            ) : null}

            {/* For Recipe Cousing Type veg or non veg */}
            {item.recipe
              ? SeoSchema({
                  pagetype: "articleshow",
                }).name(title)
              : null}
            <meta itemProp="name" content={item.pwa_meta.editornameseo} />
            {item.recipe && item.recipe.recipeinfo && item.recipe.recipeinfo.cuisinetype ? (
              <span className="vegnonveg">
                <span className={item.recipe.recipeinfo.cuisinetype.replace(/\s+/g, " ")} />
              </span>
            ) : null}
          </h1>
          {//disclaimer
          item.disclaimer ? (
            <div className="txtdisclaim">
              <strong>{siteConfig.locale.disclaimer}:</strong>
              {item.disclaimer}
            </div>
          ) : null}
          <div className="source_sharing">
            {isMobilePlatform() && (
              <ErrorBoundary>
                <TPicon basePath={siteConfig.mweburl} pageType={"articleshow"} />
              </ErrorBoundary>
            )}
            <div className="news_card_source">
              {item.pwa_meta && item.pwa_meta.brandstory && item.pwa_meta.brandstory !== "" ? (
                <div className="brandstories">{item.pwa_meta.brandstory}</div>
              ) : null}
              {SummaryCard.Byline(item)}
              {/* magicbricks */}
              {item.ag && item.ag != "" && item.ag == "Magicbricks" ? (
                <div className="adsForThirdparty">
                  <AnchorLink target="_blank" href={"https://www.magicbricks.com/"}>
                    <ImageCard
                      noLazyLoad={!_isCSR()}
                      type="absoluteImgSrc"
                      src="https://navbharattimes.indiatimes.com/thumb/msid-72272822,width-182,height-25,resizemode-4/pic.jpg"
                    />
                  </AnchorLink>
                </div>
              ) : null}
            </div>
            {!isMobilePlatform() ? (
              <div className="only-web">
                <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} router={router} />
                <div id={`widget-two-${item && item.id}`} className="wdt_timespoints" />
                <div className="btn-subscribe">
                  <YSubscribeCard />
                </div>
                <div className="clear"></div>
              </div>
            ) : (
              ""
            )}
          </div>

          {//toast summary story
          //(item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff") && item.syn ?
          item.syn && !item.recipe ? (
            <div className="enable-read-more">
              <div className="first_col">
                <h2 className="caption text_ellipsis">{item.syn}</h2>
              </div>
              <div className="second_col" data-exclude="amp">
                <span className="readmore expand">&nbsp;</span>
              </div>
            </div>
          ) : null}
          {(item.pwa_meta && item.pwa_meta.AdServingRules && item.pwa_meta.AdServingRules === "Turnoff") ||
          item.ag != "Brandwire"
            ? !isMobilePlatform() && <ASLeadImage item={item} openCommentsPopup={openCommentsPopup} />
            : null}

          {/* For Recipe Card */}
          {item.recipe && item.recipe.recipeinfo
            ? SummaryCard.RecipeShortDetails(item.recipe.recipeinfo, item.id, item.ur && item.ur != "" ? item.ur : null)
            : null}
          {/* For Recipe Card Synopsis */
          item.recipe && item.syn ? (
            <div className="enable-read-more">
              <div className="first_col">
                <h2 className="caption text_ellipsis">{item.syn}</h2>
              </div>
              <div className="second_col" data-exclude="amp">
                <span className="readmore expand">&nbsp;</span>
              </div>
            </div>
          ) : null}
        </div> // Show only lead image
      ) : _onlyleadimage && item.ag != "Brandwire" && !(item.pwa_meta && item.pwa_meta.AdServingRules) ? (
        !isMobilePlatform() && <ASLeadImage openCommentsPopup={openCommentsPopup} />
      ) : null}
    </React.Fragment>
  );
};

SummaryCard.Byline = item => {
  const editorName = (item && item.audetails && item.audetails.primary && item.audetails.primary.name) || "";
  const editorlabel = (item && item.audetails && item.audetails.primary && item.audetails.primary.label) || "";
  const editorNameSec = (item && item.audetails && item.audetails.secondary && item.audetails.secondary.name) || "";
  const editorSeclabel = (item && item.audetails && item.audetails.secondary && item.audetails.secondary.label) || "";
  const editorLink = (item && item.audetails && item.audetails.primary && item.audetails.primary.link) || "";
  const editorSecLink = (item && item.audetails && item.audetails.secondary && item.audetails.secondary.link) || "";
  const editorCD = (item && item.audetails && item.audetails.cd) || "";
  return (
    <div className="source">
      {item.brandstory ? <div>{item.brandstory}</div> : null}
      {editorLink != "" ? (
        <React.Fragment>
          <span className="txt">{editorlabel}</span>
          <a href={editorLink} {...SeoSchema({ pagetype: "articleshow" }).attr().person} className="txt">
            <meta itemProp="name" content={editorName} />
            {editorName}
            <span> | </span>
            {SeoSchema({ pagetype: "articleshow" }).name(editorCD)}
          </a>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span>{editorlabel} </span>
          <span itemProp="author" itemScope="1" itemType={"http://schema.org/Person"}>
            <meta itemProp="name" content={editorCD} />
            <span> {editorName}</span>
          </span>
          {editorName != "" ? <span> | </span> : null}
          {SeoSchema({ pagetype: "articleshow" }).name(editorCD)}
        </React.Fragment>
      )}
      {/* secondry Authour */}
      {editorSecLink != "" ? (
        <React.Fragment>
          <span className="txt">{editorSeclabel} </span>
          <a href={editorSecLink} className="txt">
            {editorNameSec}
          </a>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span>{editorSeclabel} </span>
          <span> {editorNameSec}</span>
        </React.Fragment>
      )}
      {editorNameSec != "" ? <span> | </span> : null}
      {item.ag ? (
        !editorName || editorName == "No-Author" ? (
          <React.Fragment>
            <span itemProp="author" itemScope="1" itemType={"http://schema.org/Thing"}>
              {item.ag}
              <meta itemProp="name" content={item.ag} />
            </span>
            <span className="sep"> | </span>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {item.ag}
            {item.pwa_meta && item.pwa_meta.sectionid != siteConfig.pages.giftsection ? (
              <span className="sep"> | </span>
            ) : null}
          </React.Fragment>
        )
      ) : item.audetails && item.audetails.agency ? (
        !editorName || editorName == "No-Author" ? (
          <React.Fragment>
            <span itemProp="author" itemScope="1" itemType={"http://schema.org/Thing"}>
              {item.audetails.agency}
              <meta itemProp="name" content={item.audetails.agency} />
            </span>
            <span className="sep"> | </span>
          </React.Fragment>
        ) : (
          <React.Fragment>
            {item.audetails.agency}
            <span className="sep"> | </span>
          </React.Fragment>
        )
      ) : (
        ""
      )}

      {item.sec && item.sec == "citizen-reporter" ? (
        <div>{item.dl}</div>
      ) : item.pwa_meta && item.pwa_meta.sectionid == siteConfig.pages.giftsection ? (
        <span></span>
      ) : (
        <span className="time">Updated: {item.lu}</span>
      )}
    </div>
  );
};

SummaryCard.RecipeShortDetails = (item, id, ur) => {
  return (
    <div>
      {/* <RattingCard id={id} ratingOnly={true} type={"recipe"} ur={ur} /> */}

      <div className="time_calor">
        <ul>
          {item.cookingtimeinminutes ? (
            <li>
              <SvgIcon name="totaltime" />
              <div className="txt_innner">
                <span
                  {...SeoSchema({ pagetype: "articleshow" }).attr().CookTime}
                  content={"PT" + item.cookingtimeinminutes.replace(/\s+/g, "") + "M"}
                >
                  {item.totaltimeinminutes}m
                </span>
                Total Time
              </div>
            </li>
          ) : null}

          {item.preparationtimeinminutes ? (
            <li>
              <SvgIcon name="preptime" />
              <div className="txt_innner">
                <span
                  {...SeoSchema({ pagetype: "articleshow" }).attr().PrepTime}
                  content={"PT" + item.preparationtimeinminutes.replace(/\s+/g, "") + "M"}
                >
                  {item.preparationtimeinminutes}m
                </span>
                Prep Time
              </div>
            </li>
          ) : null}
          {item.calories ? (
            <li>
              <SvgIcon name="calories" />
              <div className="txt_innner" {...SeoSchema({ pagetype: "articleshow" }).attr().Nutrition}>
                <span {...SeoSchema({ pagetype: "articleshow" }).attr().Calories}>{item.calories}</span>
                Calories
              </div>
            </li>
          ) : null}
        </ul>

        {SeoSchema().metaTag({
          name: "recipeCuisine",
          content: item.cuisine.replace(/\s+/g, ""),
        })}
        {SeoSchema().metaTag({
          name: "recipeCategory",
          content: item.course.replace(/\s+/g, ""),
        })}
      </div>
    </div>
  );
};

export default SummaryCard;
