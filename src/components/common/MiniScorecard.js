import React from "react";
//import { Link } from 'react-router'
import PropTypes from "prop-types";
import styles from "./css/MiniScorecard.scss";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const MiniScorecard = props => {
  let match =
    props.match && props.match.Calendar && props.match.Calendar.length
      ? props.match.Calendar
      : {};

  return match ? (
    <li className="scorecard-listview scroll_content">
      {match.map((match, index) => {
        return (
          <div className="miniscorecard">
            <AnchorLink
              data-type="scorecard"
              target="_blank"
              style={{ textDecoration: "none" }}
              hrefData={{
                override:
                  siteConfig.mweburl +
                  "/sports/cricket/live-score/" +
                  match.seoteamname +
                  "/" +
                  match.matchdate_localnew +
                  "/scoreboard/matchid-" +
                  match.matchid +
                  ".cms"
              }}
            >
              <div className="liveScore">
                <strong style={{ float: "right" }}>
                  {match.matchnumber}, {match.seriesname}
                </strong>
                <span className="live">
                  <small></small>LIVE
                </span>
                <div className="clear"></div>
                <div className="midbox">
                  <span className="dates">
                    {match.matchdd}
                    <small>{match.matchmm}</small>
                  </span>
                  <div className="teama">
                    <span className="teaminfo fl">
                      <img
                        height="20"
                        width="20"
                        src={match.teama_flag_square}
                      ></img>
                      {match.teama_short}
                    </span>
                    <span className="teamscore fl">
                      <span className="teamaScore">{match.teama_score}</span>
                      <span className="teamaOvers">
                        {match.teama_overs != "" ? match.teama_overs : ""}
                      </span>
                    </span>
                  </div>
                  <div className="teamb">
                    <span className="teaminfo fr">
                      <img
                        height="20"
                        width="20"
                        src={match.teamb_flag_square}
                      ></img>
                      {match.teamb_short}
                    </span>
                    <span className="teamscore fr">
                      <span className="teambScore">{match.teamb_score}</span>
                      <span className="teambOvers">
                        {match.teamb_overs != "" ? match.teamb_overs : ""}
                      </span>
                    </span>
                  </div>
                </div>
                <div className="mSummery">{match.matchstatus}</div>
              </div>
            </AnchorLink>
          </div>
        );
      })}
    </li>
  ) : (
    ""
  );
};

MiniScorecard.propTypes = {
  item: PropTypes.object
};

export default MiniScorecard;
