import React from "react";
import { _isCSR } from "./../../utils/util";
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();

const esiAd = prop => {
  let { index, adunit } = prop;
  let ctnslot = adunit && siteConfig.ads.ctnslots[adunit] != undefined ? siteConfig.ads.ctnslots[adunit] : "";
  let pos = index && index != undefined ? index : 1;

  let esi = {
    config: {
      fetch: `<!--esi <esi:eval src="/fetch_native_content.cms?fpc=$url_encode($(HTTP_COOKIE{'_col_uuid'}))&x1=$url_encode($(HTTP_COOKIE{'c_ut'}))&x2=$url_encode($(HTTP_COOKIE{'us_ch'}))&ab=$url_encode($(HTTP_COOKIE{'${siteConfig.ads.ctnads.ctncookievalue}'}))&id=$url_encode('#combination#')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> -->`,
      col_script: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'_col_script'}))">$(native_content{'_col_script'})<script>  <esi:text> var isesi = 1;</esi:text> </script> </esi:when></esi:choose>-->`,
      col_ab: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'_col_ab_call'}))">$(native_content{'_col_ab_call'})</esi:when></esi:choose> -->`,
      col_ui: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'_col_uuid'}))">$add_header('Set-Cookie', $(native_content{'_col_uuid'}))</esi:when></esi:choose>-->`,

      slot: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'#combination#'}))">$(native_content{'#combination#'})</esi:when></esi:choose> -->`,

      // track: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'_col_up'})) && (#combination#)"><script>var e_x = '#combination2#';</script></esi:when><esi:when test="!$exists($(native_content{'_col_up'}))"><script>ga('send', 'event', 'e_usfl', 'yes', 'e_usfl', 1, {'nonInteraction': 1});</script></esi:when></esi:choose> -->`,
      track: `<!--esi <esi:choose><esi:when test="$exists($(native_content{'_col_up'})) && (#combination#)"><script>var e_x = '#combination2#';</script></esi:when><esi:when test="!$exists($(native_content{'_col_up'}))"><script></script></esi:when></esi:choose> -->`,
    },
  };

  esi.adComment = prop => {
    return <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config[prop.type] }} />;
  };

  esi.ssr = prop => {
    let slots = prop.slots.split(",");
    let esiadunits = "";
    // Ad Unit and Position Conbination
    slots != undefined
      ? slots.forEach((element, index) => {
          if (siteConfig.ads.ctnslots[element]) {
            esiadunits += `${siteConfig.ads.ctnslots[element]}~${index}~0`;
            if (index != slots.length - 1) {
              esiadunits += ",";
            }
          }
        })
      : null;
    for (var property in esi.config) {
      if (esi.config.hasOwnProperty(property)) {
        //Add Combination comma seperated in fetch call
        if (property == "fetch") {
          esi.config[property] = esi.config[property].replace("#combination#", esiadunits);
        }
        //Pass combination in two spererate methods
        else if (property == "track") {
          let slotComb = esiadunits.split(",");
          let trackstr = "";
          let trackstr2 = "";

          slotComb.forEach((element, index) => {
            trackstr += `$exists($(native_content{'${element}'}))`;

            let adunitStr = element.split("~");
            adunitStr.pop();
            adunitStr = adunitStr.join("-");

            trackstr2 += `${adunitStr} = $exists($(native_content{'${element}'}))`;

            if (index != slotComb.length - 1) {
              trackstr += "||";
              trackstr2 += ",";
            }
          });

          esi.config[property] = esi.config[property].replace("#combination#", trackstr);
          esi.config[property] = esi.config[property].replace("#combination2#", trackstr2);
        } else if (property == "slot") {
          let slotComb = esiadunits.split(",");
          slotComb.forEach((element, index) => {
            if (element.indexOf(ctnslot) > -1) {
              esi.config["slot"] = esi.config["slot"].split("#combination#").join(ctnslot + "~" + index + "~0");
            }
          });
        }
      }
    }

    return prop.type == "slot" ? (
      <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["slot"] }} />
    ) : (
      <div>
        {/* fetch Native Call */}
        {/* for Head Part Should be single time on page */}
        <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["fetch"] }} />
        <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["col_ui"] }} />
        <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["col_script"] }} />
        <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["col_ab"] }} />
        <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: esi.config["track"] }} />
      </div>
    );
  };

  return esi;
};

export default esiAd;
