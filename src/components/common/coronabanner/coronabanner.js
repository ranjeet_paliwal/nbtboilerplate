import React from "react";
// import PropTypes from "prop-types";
import fetch from "../../../utils/fetch/fetch";
import { playOdometer } from "../../../modules/odometer";
import odometerCSS from "../../../modules/odometer/odometer.scss";
import { isMobilePlatform } from "../../../utils/util";

// import "../../components/common/css/desktop/DockPortal.scss";

class CoronaBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { response: {}, type: "India" };
    this.types = { India: "Total", Total: "India" };
    this.config = { prevUrl: "" };
  }

  componentDidMount() {
    fetch(`https://popper.ai/ncov19/summary_data_toi_lite.json`).then(response => {
      // console.log(this, response, response.Total, response.India);
      this.setState({ response });

      playOdometer();

      setInterval(() => {
        this.changeType();
      }, 10000);
    });
  }

  changeType = () => {
    this.setState({ type: this.types[this.state.type] }, () => {
      playOdometer();
    });
  };

  render() {
    return (
      <div
        id="corona-banner-odometer"
        className={`corona-numbers ${isMobilePlatform() ? "mobile" : "desktop"} `}
        data-exclude="amp"
      >
        <div className="inline-values">
          <div className="heading">
            <h4>
              COVID-19 <b>cases</b>
            </h4>
            <span>{this.state.type === "Total" ? "World" : "India"}</span>
          </div>
          <div className="confirmed">
            <span className="name">Confirmed</span>
            <div
              className="odometer"
              data-odometer={
                this.state.response[this.state.type] ? this.state.response[this.state.type].Confirmed : "000"
              }
            >
              0000
            </div>
          </div>
          <div className="recovered">
            <span className="name">Recovered</span>
            <div
              className="odometer"
              data-odometer={
                this.state.response[this.state.type] ? this.state.response[this.state.type].Recovered : "000"
              }
            >
              0000
            </div>
          </div>
          <div className="deaths">
            <span className="name">Death</span>
            <div
              className="odometer"
              data-odometer={this.state.response[this.state.type] ? this.state.response[this.state.type].Deaths : "000"}
            >
              00
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// CoronaBanner.propTypes = {};

export default CoronaBanner;
