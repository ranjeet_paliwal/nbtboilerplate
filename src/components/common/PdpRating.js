import React from "react";
import ImageCard from "./ImageCard";

import styles from "./css/MovieReviewShowCard.scss";
import fetch from "../../utils/fetch/fetch";
import { _isCSR, _checkUserStatus } from "../../utils/util";
import { openLoginPopup } from "./ssoLogin";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const locale = siteConfig.locale;

const RatingPoster = props => {
  // Adding other details of Moview Review (language,gener, etc)
  let mr_otherinfo = [];
  props.lang ? mr_otherinfo.push(props.lang) : null; //add Movie language
  props.gn ? mr_otherinfo.push(props.gn.split(",").join(", ")) : null;

  let ratings =
    props.gadgetinfo && props.gadgetinfo.rating
      ? props.gadgetinfo.rating
      : null;
  let criticRating =
    props.gadgetinfo &&
    props.gadgetinfo.criticRating != "NaN" &&
    parseInt(props.gadgetinfo.criticRating)
      ? parseInt(props.gadgetinfo.criticRating)
      : null;
  let uRating = props.ur && props.ur != "NaN" ? props.ur : null;

  //{console.log('criticRating',criticRating)}
  //{console.log('criticRating before Cal', props.gadgetinfo.criticRating)}
  return (
    <div className="animated fadeIn moviereview-summaryCard">
      {/*userrating line*/}
      <Userrating userratings={ratings} userRating={uRating} id={props.id} />
      {/* {MovieReviewSocialReactions !='' ? <a lang="en" className="twitter-timeline" data-widget-id={props.twitter} href={MovieReviewSocialReactions}>Tweets about {props.twitter_key}</a> : null} */}
    </div>
  );
};

export const Userrating = props => {
  let arrRatings = [];
  if (props.userratings && props.userratings.ratings) {
    arrRatings.push(props.userratings.ratings);
  } else if (props.userratings) {
    arrRatings = [...props.userratings];
  }

  // arrRatings = [...props.userratings];
  let criticRating = props.userRating;
  let userRatingnew =
    props.userRating && props.userRating != "NaN" ? props.userRating : "";
  let userrating = [],
    pos,
    arrSpan = [],
    max = 0,
    rateStyle = {};
  userrating.length = 5;

  arrRatings.map((item, index) => {
    pos = item.rating / 2 - 1;
    userrating[pos] = item;
    max = parseInt(item.count) > max ? item.count : max;
  });

  for (let i = 4; i >= 0; i--) {
    rateStyle = {
      width: userrating[i]
        ? (parseInt(userrating[i].count) / parseInt(max)) * 100 + "%"
        : "0%"
    };
    arrSpan.push(
      <li>
        <span className="star_txt">{i + 1 + " ★ "}</span>
        <span className="star_bg">
          <span className={"star" + (i + 1)} style={rateStyle}>
            {userrating[i] ? userrating[i].count : "0"}
          </span>
        </span>
      </li>
    );
  }

  return (
    <React.Fragment>
      <div className="con_review_rate">
        <div className="rating-values">
          {userRatingnew ? (
            <div className="user_review">
              <div className="rating">
                <span>
                  {userRatingnew}
                  <sub>/5</sub>
                </span>
                <b>Avg rating</b>
              </div>
            </div>
          ) : null}
          {userrating ? (
            <div className="user_star">
              <ul>{arrSpan}</ul>
            </div>
          ) : null}
        </div>
        <div className="tabs_circle">
          <ul className="tabs_circle_list">
            <li onClick={excuteReview.bind(this, props.id)}>
              {locale.review_movie}
            </li>
            <li
              id={"rating_" + props.id}
              onClick={e => {
                if (!window._user) {
                  openLoginPopup(excuteRating.bind(this, props.id));
                } else {
                  excuteRating(props.id);
                }
              }}
            >
              {locale.rate_movie}
              <input className="hide" type="checkbox"></input>
            </li>
          </ul>
          {_isCSR() ? checkRating(props.id) : null}
        </div>

        <div
          className="rateBox"
          style={{ "max-height": 500 }}
          id={"ratingbox_" + props.id}
        >
          <div className="rate_con">
            <h3>{locale.slide_to_rate_movie}</h3>
            <div className="star-and-value">
              <span className="rating-stars">
                <span className="empty-stars"></span>
                <span
                  className="filled-stars ratestar w50"
                  id={"ratestar_" + props.id}
                ></span>
              </span>
              <span className="ratespan" id={"ratespan_" + props.id}>
                2.5
              </span>
            </div>
            <input
              className="rangeslider"
              type="range"
              min="0"
              max="5"
              step=".5"
              id={"rateslider_" + props.id}
            ></input>
            <label className="custom-checkbox">
              <input
                className="rangesubmit"
                type="checkbox"
                name="submit"
                value="submit"
                id={"ratesubmit_" + props.id}
              ></input>
              <span className="checkmark"></span>
            </label>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

// Return Actors with microdata schema

const excuteReview = (id, e) => {
  let commentBox = document.getElementById("comment_" + id);
  if (commentBox) commentBox.click();
};

const excuteRating = id => {
  let rateButton = document.getElementById("rating_" + id);
  let hidden_checkbox = rateButton.querySelector(".hide");
  let rateBox = document.getElementById("ratingbox_" + id);
  let rateSlider = document.getElementById("rateslider_" + id);
  let rateStar = document.getElementById("ratestar_" + id);
  let rateSpan = document.getElementById("ratespan_" + id);
  let rateSubmit = document.getElementById("ratesubmit_" + id);
  let _rating = 0;
  let ratingurl =
    siteConfig.mweburl +
    "/rate_moviereview.cms?msid=" +
    id +
    "&getuserrating=1&criticrating=" +
    "&vote=";

  if (!rateBox || !rateSlider || !rateSubmit) return false;
  hidden_checkbox.checked = !hidden_checkbox.checked;
  hidden_checkbox.checked
    ? rateBox.classList.add("visible")
    : rateBox.classList.remove("visible");
  hidden_checkbox.checked
    ? rateButton.classList.add("active")
    : rateButton.classList.remove("active");

  rateSlider.oninput = e => {
    _rating = parseFloat(e.target.value);
    e.target.style.background = `linear-gradient(to right, #00b3ff 0%,#00b3ff ${_rating *
      2 *
      10}%,#fff ${_rating * 2 * 10}%, #fff 100%)`;
    rateStar.classList.value = "";
    rateStar.classList.add("w" + _rating * 2 * 10);
    rateStar.classList.add("filled-stars");
    rateStar.classList.add("ratestar");
    rateSpan.innerHTML = _rating;
  };

  rateSubmit.onchange = e => {
    if (e.target.checked && window._user && _rating > 0) {
      // checkbox submit, disable when rated and color change
      e.target.classList.add("rate-submit");
      e.target.setAttribute("disabled", true);
      e.target.style.pointerEvents = "none";
      rateSlider.style.pointerEvents = "none";
      ratingurl = ratingurl + _rating * 2;
      fetch(ratingurl, { type: "jsonp" })
        .then(
          () => {
            setTimeout(() => {
              document.getElementById(
                "rating_" + id
              ).innerHTML = `<span><b>${_rating}</b>/5</span>`;
              rateBox.classList.remove("visible");
              rateButton.style.pointerEvents = "none";
              rateButton.classList.add("btn-rated");
            }, 500);
          },
          () => {
            rateBox.innerHTML = "ERROR";
          }
        )
        .catch(() => {
          rateBox.innerHTML = "ERROR";
        });

      mytActivityLogging("Rated", id, "", _rating * 2, "");
    }
  };
};

const checkRating = id => {
  _checkUserStatus(user => {
    let ratingSpan;
    if (user.detail) {
      let alreadyRatedURL = `${siteConfig.sso["MYTIMES_ALREADY_RATED"]}userId=${user.detail._id}&baseEntityId=0&uniqueAppKey=${id}`;
      fetch(alreadyRatedURL).then(text => {
        if (text && text != "") {
          ratingSpan = document.getElementById("rating_" + id);
          ratingSpan.innerHTML = `<span><b>${parseInt(text) / 2}</b>/5</span>`;
          ratingSpan.style.pointerEvents = "none";
          ratingSpan.classList.add("btn-rated");
        }
        // else{
        //   ratingSpan.addEventListener('click',excuteRating.bind(this,id));
        // }
      });
      // .catch((err)=> ratingSpan.addEventListener('click', excuteRating.bind(this,id)))
    } else {
      // ratingSpan.addEventListener('click',openLoginPopup(excuteRating.bind(this,id)));
    }
  });
};

const mytActivityLogging = (
  activityType,
  _m_msid,
  _m_msg,
  rateval,
  exCommentTxt
) => {
  if (_m_msid != undefined && _m_msid != null) {
    let src = siteConfig.sso["MYTIMES_ADD_ACTIVITY"];
    src = src + "activityType=" + activityType;
    src = src + "&uniqueAppID=" + _m_msid;
    src = src + "&baseEntityType=MOVIEW_REVIEW";
    src = src + "&objectType=B";
    src = src + "&url=" + window.location.href;
    if (
      rateval != null &&
      rateval != undefined &&
      rateval != "" &&
      rateval != "0"
    ) {
      src = src + "&userrating=" + rateval;
    }
    if (
      exCommentTxt != null &&
      exCommentTxt != undefined &&
      exCommentTxt != ""
    ) {
      src = src + "&exCommentTxt=" + exCommentTxt;
    }
    if (_m_msg != null && _m_msg != undefined && _m_msg != "") {
      src = src + "&via=" + _m_msg;
    }
    var img = new Image(1, 1);
    img.src = src;
  }
};

export default RatingPoster;
