import React, { Component } from "react";

import { _getStaticConfig, _isCSR } from "../../utils/util";
const siteConfig = _getStaticConfig();

import PushNotificationCard from "./PushNotificationCard";
import { _deferredDeeplink } from "./../../utils/util";
import styles from "../../components/common/css/ServiceDrawer.scss";
import FacebookLikeCard from "./FacebookLikeCard";
import AnchorLink from "./AnchorLink";
import SvgIcon from "./SvgIcon";

class ServiceDrawer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    //commented this code as We have moved to Izooto.
    //Todo: we can remove this code in future
    //pn_flag variable using to enable disable pushnotification card
    //let pn_flag = ['mly', 'tml', 'tlg']
    return (
      <div className="scroll_content service_drawer">
        {/*chrome alert*/}
        {/* {
                pn_flag.indexOf(process.env.SITE) > -1 ?
                  <PushNotificationCard key="pushnotify" />
                : null
              } */}

        {/*download app alert*/}
        <div className="con_appinstall item_drawer">
          <div className="label">
            <h4
              dangerouslySetInnerHTML={{
                __html: siteConfig.locale.appdownloadcardtext
              }}
            ></h4>
          </div>
          {/* href={siteConfig.applinks.android.serviceDrawer} */}
          <AnchorLink
            target="_blank"
            href={_deferredDeeplink("serviceDrawer", siteConfig.appdeeplink)}
            className="btn-draweralert"
          >
            {siteConfig.locale.installapp}
          </AnchorLink>
          <span className="bg_circle">
            <SvgIcon name="gadgets" className="phone_icon" center="center" />
            {/* <b className="phone_icon"></b> */}
          </span>
        </div>

        {/*facebook like*/}
        <FacebookLikeCard />
        {/*
              <div className="con_facebooklike item_drawer">
                <div className="label">
                  <h4>एनबीटी फेसबुक पेज को <br/> लाइक करे</h4>
                </div>
                <a target="_blank" href="#" className="btn-draweralert">LIKE</a>
                <span className="bg_circle"><b className="nbt_icon"></b></span>
              </div>
              */}

        {/*newsletter*/}
        {/*
              <div className="con_newsletter item_drawer">
                <div className="label">
                  <h4>न्यूजलेटर सबस्क्राइब करेे</h4>
                </div>
                <div className="text_email"><input type="text" placeholder="Email" /></div>
                <a className="btn-draweralert" href="#" target="_blank">जॉइन करे</a>
                <span className="bg_circle"><SvgIcon name="close-envelope" className="newsletter_icon" /></span>
              </div>
              */}
        {/* <SvgIcon name="bell" className="bell_icon" /> */}
      </div>
    );
  }
}

export default ServiceDrawer;
