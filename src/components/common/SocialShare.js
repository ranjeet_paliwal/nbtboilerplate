import React, { Component } from "react";
import ReactDOM from "react-dom";
import { withRouter } from "react-router";
// import { withRouter } from "react-router-dom";

import {
  getMobileOperatingSystem,
  getPageType,
  isMobilePlatform,
  _getStaticConfig,
  _isCSR,
  shouldTPRender,
} from "../../utils/util";
// import { shareHandlingForTimesPoints } from "../common/TimesPoints/timespoints.util";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import styles from "./css/socialShare.scss";

import SvgIcon from "./SvgIcon";
import { fireActivity } from "./TimesPoints/timespoints.util";
const siteConfig = _getStaticConfig();

const locale = siteConfig.locale;

class SocialShare extends Component {
  constructor(props) {
    super(props);
    this.state = { isVisible: false };
    this.openShareList = this.openShareList.bind(this);
  }

  componentDidMount() {
    const _this = this;
    if (typeof window !== "undefined") {
      window.addEventListener(
        "click",
        event => {
          const modal = document.getElementById("shareModal");
          if (event.target == modal) {
            _this.closeShareModal();
          }
        },
        false,
      );
    }
  }

  setShareData() {
    const retrnObj = {
      title: document.title,
      url: location.href,
    };

    return retrnObj;
  }

  openShareList() {
    let sharedata;
    let defaultUrl = document.location.href;
    const _this = this;

    if (_this) {
      if (this.props !== undefined && this.props.sharedata) {
        sharedata = this.props.sharedata;
      } else {
        sharedata = this.setShareData;
      }
    }

    const canonicalElement = document.querySelector("link[rel=canonical]");
    if (canonicalElement !== null) {
      defaultUrl = canonicalElement.href;
    }

    if (navigator.share && isMobilePlatform()) {
      const native_title = sharedata().title || "";
      const native_text = `${siteConfig.locale.social_download.extrasharetxt} `;
      const native_storyurl = sharedata().short_url
        ? `${sharedata().short_url}/a36${siteConfig.shortutm}`
        : sharedata().url
        ? `${sharedata().url}?utm_source=native_sharing${siteConfig.fullutm}`
        : `${defaultUrl}?utm_source=native_sharing${siteConfig.fullutm}/n/n\n\n`;
      // Added onelink to end of merge text and replaced native_url with story url so
      const native_mergetext = `${sharedata().title || ""} ${native_storyurl}${native_text} ${
        siteConfig.applinks.android.social
      }`;
      // const native_url = siteConfig.applinks.android.social;
      navigator
        .share({ title: native_title, text: native_mergetext, url: native_storyurl })
        .then(() => {})
        .catch(error => {});
    } else {
      this.setState({ isVisible: true });
    }
  }

  /* All social share function have short micron url availability then used it for social sharing. */
  shareOnFacebook = () => {
    let u;
    let t;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        u = shareData.short_url;
        u += `/m${siteConfig.shortutm}`;
      } else {
        u = shareData.url;
        u += `?utm_source=fb${siteConfig.fullutm}`;
      }
    } else {
      u = siteConfig.mweburl + location.pathname;
      u += `?utm_source=fb${siteConfig.fullutm}`;
    }
    // console.log("fbshare", u, t);
    try {
      const articleId = location.pathname
        .split(".cms")[0]
        .split("/")
        .pop();

      import("./TimesPoints/timespoints.util").then(comp => {
        if (typeof comp.shareHandlingForTimesPoints === "function" && articleId) {
          comp.shareHandlingForTimesPoints("facebook", articleId);
        }
      });
    } catch (ex) {
      console.log("TP Points Error FB", ex);
    }

    window.open(
      `https://www.facebook.com/sharer.php?u=${encodeURIComponent(u)}`,
      "sharer",
      "toolbar=0,status=0,width=626,height=436",
    );
    // gaEvent('Share','ArticleShare','FB');

    return false;
  };

  shareOnTwitter = () => {
    let u;
    let t;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        u = shareData.short_url;
        u += `/o${siteConfig.shortutm}`;
        t = shareData.title;
        // remove english title for twiiter limitation
        // if (t.indexOf(" - ")) {
        //   t = t.substring(t.indexOf(" - ") + 3);
        // }
        if (t.length > 150) t = `${t.substring(0, 150)}... `;
        t = encodeURIComponent(t);
        t += ` ${siteConfig.locale.social_download.extrasharetxt}&${siteConfig.applinks.android.social}`;
      } else {
        u = shareData.url;
        u += `?utm_source=twitter${siteConfig.fullutm}`;
        t = encodeURIComponent(shareData.title);
      }
    } else {
      u = siteConfig.mweburl + location.pathname;
      u += `?utm_source=twitter${siteConfig.fullutm}`;
      t = encodeURIComponent(document.title);
    }

    try {
      const articleId = location.pathname
        .split(".cms")[0]
        .split("/")
        .pop();
      import("./TimesPoints/timespoints.util").then(comp => {
        if (typeof comp.shareHandlingForTimesPoints === "function" && articleId) {
          comp.shareHandlingForTimesPoints("twitter", articleId);
        }
      });
    } catch (ex) {
      console.log("TP Points Error Twitter", ex);
    }

    window.open(
      `https://twitter.com/intent/tweet?url=${encodeURIComponent(u)}&text=${t.replace(
        "|",
        "",
      )}&via=${siteConfig.twitterHdandler.replace("@", "")}`,
      "",
      "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600",
    );
    // gaEvent('Share','ArticleShare','T');

    return false;
  };

  shareOnGPlus = () => {
    let u = "https://plus.google.com/share?url=";
    let title;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        u += `${shareData.short_url}/a25${siteConfig.shortutm}`;
      } else {
        u += `${shareData.url}?utm_source=gplus${siteConfig.fullutm}`;
      }
      title = shareData.title;
    } else {
      u = siteConfig.mweburl + location.pathname;
      u += `?utm_source=gplus${siteConfig.fullutm}`;
      title = document.title;
    }

    window.open(u, title, "height=500,width=400");
    // gaEvent('Share','ArticleShare','G+');
    return false;
  };

  shareOnEmail = () => {
    const shareData = this.props.sharedata();
    const emailUrl = `mailto:?subject=${shareData.title}`;
    let body;
    if (this.props.sharedata) {
      if (shareData.short_url != "" && shareData.short_url != null) {
        body = `body=${encodeURIComponent(
          `${shareData.short_url}/a28${siteConfig.shortutm}${siteConfig.locale.social_download.extrasharetxt}  ${siteConfig.applinks.android.social}`,
        )}`;
      } else {
        body = `body=${encodeURIComponent(
          `${shareData.url}?utm_source=email${siteConfig.fullutm}${siteConfig.locale.social_download.extrasharetxt} ${siteConfig.applinks.android.social}`,
        )}`;
      }
    } else {
      body = `body=${encodeURIComponent(`${location.href}?utm_source=email${document.title}`)}`;
    }

    window.location.href = `${emailUrl}&${body}`;
    // gaEvent('Share','ArticleShare','Mail');
    return false;
  };

  shareOnWhatsapp = () => {
    let info;
    let title;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        info = shareData.short_url;
        info += `/a31${siteConfig.shortutm}`;
        info += `${siteConfig.locale.social_download.extrasharetxt} ${siteConfig.applinks.android.social}`;
      } else {
        info = shareData.url;
        info += `?utm_source=whatsapp${siteConfig.fullutm}`;
        info += `${siteConfig.locale.social_download.extrasharetxt}  ${siteConfig.applinks.android.social}`;
      }
      title = shareData.title;
    } else {
      info = siteConfig.mweburl + location.pathname;
      info += `?utm_source=whatsapp${siteConfig.fullutm}`;
      info += `${siteConfig.locale.social_download.extrasharetxt} ${siteConfig.applinks.android.social}`;
      title = document.title;
    }

    // Firing whatsapp activity
    try {
      if (shouldTPRender() && this.props.sharedata && this.props.sharedata().msid) {
        fireActivity("WHATSAPP_SHARE", `ws_share_${this.props.sharedata().msid}`);
      }
    } catch (e) {}

    //  Added sharing for desktop
    if (!isMobilePlatform()) {
      const shareText = `${siteConfig.locale.whatsapp_share} ${title} ${info}`;
      window.open(`https://api.whatsapp.com//send?text=${encodeURIComponent(shareText)}`);
    } else {
      const whatsappurl = `whatsapp://send?text=${encodeURIComponent(title)} - ${encodeURIComponent(info)}`;
      window.location.href = whatsappurl;
    }

    // gaEvent('Share','ArticleShare','W');
    return false;
  };

  shareOnSms = () => {
    const os = getMobileOperatingSystem();
    let smsurl = "";
    let smsbody = `${encodeURIComponent(document.title)} - `;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        smsbody += `${shareData.short_url}/a29${siteConfig.shortutm}`;
      } else {
        smsbody += `${shareData.url}?utm_source=sms${siteConfig.fullutm}`;
      }
    } else {
      smsbody += `${document.href}?utm_source=sms${siteConfig.fullutm}`;
    }

    if (os == "android") {
      smsurl = `sms:?body=${smsbody}`;
    }
    if (os == "ios") {
      // alert("OS version is: "+iosVersion)
      smsurl = `sms:&body=${smsbody}`;
    }

    window.open(smsurl, "_self");
    // gaEvent('Share','ArticleShare','SMS');
    return false;
  };

  shareOnKoo = () => {
    let u = "https://www.kooapp.com/create?link=#";
    let title;
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        u += `${shareData.short_url}/a25${siteConfig.shortutm}`;
      } else {
        u += `${shareData.url}?utm_source=koo${siteConfig.fullutm}`;
      }
      title = shareData.title && shareData.title.replaceAll("'", "");
    } else {
      u = siteConfig.mweburl + location.pathname;
      u += `?utm_source=koo${siteConfig.fullutm}`;
      title = document.title && document.title.replaceAll("'", "");
    }
    let kooShareData = `${u}&title=#${title}&language=#${siteConfig.language}&handle=#${siteConfig.siteName}`;
    window.open(kooShareData, "_blank");
    // gaEvent('Share','ArticleShare','G+');
    return false;
  };

  shareonTgm = () => {
    const utm_source = !isMobilePlatform()
      ? "telegram_desktop_sharing"
      : window.location.href.indexOf("/amp_") > -1
      ? "telegram_native_sharing_amp"
      : "telegram_native_sharing_mobile";
    let u = "";
    const shortCode = !isMobilePlatform() ? "pma7" : "qma7";
    if (this.props.sharedata) {
      const shareData = this.props.sharedata();
      if (shareData.short_url != "" && shareData.short_url != null) {
        u = shareData.short_url;
        //u = `${u}/${shortCode}`;
      } else {
        u = window.location.href;
      }
    }
    u = u + "/" + shortCode;
    window.open(`https://telegram.me/share/url?url=${u}`, "_blank");
  };

  showComments = () => {
    if (this.props.sharedata && this.props.openCommentsPopup) {
      if (typeof this.props.openCommentsPopup === "function") {
        this.props.openCommentsPopup();
      }
    } else if (this.props.openCommentsPopup) {
      if (typeof this.props.openCommentsPopup() === "function") {
        this.props.openCommentsPopup();
      }
    }

    return false;
  };

  closeShareModal() {
    this.setState({ isVisible: false });
  }

  render() {
    let { networkStatus, sharedata, location, icons, isInlineShare, router } = this.props; // icons:   comma separated value of social icons (for Eg: comment,email,whatsapp,fb,twitter,sms)

    const isVisible = this.state.isVisible;
    // FIXME: If this is hardcoded, condition below is not needed
    networkStatus = true; // change this
    const pageType = getPageType(location.pathname);
    if (!isMobilePlatform() && !isInlineShare && pageType !== "photoshow" && pageType !== "newsbrief") {
      return (
        <div className="wdt_social_share">
          {(pageType !== "liveblog" && !icons) ||
          (pageType !== "liveblog" && icons && icons.indexOf("comment") !== -1) ? (
            <ShareIconWrapper
              type="comments"
              clickFn={this.showComments}
              anchorClass="comments social-cell"
              shareText=""
            />
          ) : (
            ""
          )}

          {!icons || (icons && icons.indexOf("whatsapp") !== -1) ? (
            <ShareIconWrapper
              type="whatsapp"
              clickFn={this.shareOnWhatsapp}
              anchorClass="whatsapp social-cell"
              shareText=""
            />
          ) : (
            ""
          )}

          <ShareIconWrapper type="telegram" clickFn={this.shareonTgm} anchorClass="telegram social-cell" shareText="" />
          {/* 
          {!icons || (icons && icons.indexOf("email") !== -1) ? (
            <ShareIconWrapper
              type="close-envelope"
              clickFn={this.shareOnEmail}
              anchorClass="email social-cell"
              shareText=""
            />
          ) : (
            ""
          )} */}

          {!icons || (icons && icons.indexOf("fb") !== -1) ? (
            <ShareIconWrapper
              type="facebook"
              clickFn={this.shareOnFacebook}
              anchorClass="fb social-cell"
              shareText=""
            />
          ) : (
            ""
          )}

          {!icons || (icons && icons.indexOf("twitter") !== -1) ? (
            <ShareIconWrapper
              type="twitter"
              clickFn={this.shareOnTwitter}
              anchorClass="twitter social-cell"
              shareText=""
            />
          ) : (
            ""
          )}

          {!icons || (icons && icons.indexOf("sms") !== -1) ? (
            <ShareIconWrapper type="sms" clickFn={this.shareOnSms} anchorClass="sms social-cell" shareText="" />
          ) : (
            ""
          )}
        </div>
      );
    }
    if (typeof networkStatus !== "undefined" && networkStatus == false) {
      return null;
    }
    return (
      <ErrorBoundary>
        {isVisible == true ? (
          isMobilePlatform() ? (
            ReactDOM.createPortal(
              <div id="shareModal" className={`${styles.shareModalContainer} shareModalContainer`}>
                <div className={`${styles.shareContent} shareContent`}>
                  <div className={`${styles.shareHeader} shareHeader`}>
                    <span className="close_icon close_position" onClick={this.closeShareModal.bind(this)} />
                    <h3>{locale.share_popup_header}</h3>
                  </div>
                  <div className="shareBody">
                    <div className="social-table">
                      {!isMobilePlatform() && (
                        <ShareIconWrapper
                          type="whatsapp"
                          clickFn={this.shareOnWhatsapp}
                          anchorClass="whatsapp social-cell"
                          shareText="Whatsapp"
                        />
                      )}

                      <ShareIconWrapper
                        type="facebook"
                        clickFn={this.shareOnFacebook}
                        anchorClass="fb social-cell"
                        shareText="Facebook"
                      />
                      <ShareIconWrapper
                        type="telegram"
                        clickFn={this.shareonTgm}
                        anchorClass="telegram social-cell"
                        shareText="Telegram"
                      />
                    </div>
                    <div className="social-table">
                      <ShareIconWrapper
                        type="twitter"
                        clickFn={this.shareOnTwitter}
                        anchorClass="twitter social-cell"
                        shareText="Twitter"
                      />
                      {pageType !== "newsbrief" && (
                        <ShareIconWrapper
                          type="email"
                          clickFn={this.shareOnEmail}
                          anchorClass="email social-cell"
                          shareText="Email"
                        />
                      )}

                      {pageType !== "newsbrief" && (
                        <ShareIconWrapper
                          type="sms"
                          clickFn={this.shareOnSms}
                          anchorClass="sms social-cell"
                          shareText="SMS"
                        />
                      )}
                      {pageType !== "newsbrief" && process.env.SITE == "vk" && (
                        <ShareIconWrapper
                          type="kooicon"
                          clickFn={this.shareOnKoo}
                          anchorClass="koo social-cell"
                          shareText="KOO"
                        />
                      )}
                      {pageType !== "newsbrief" && !isMobilePlatform() ? (
                        <ShareIconWrapper
                          type="comments"
                          clickFn={this.showComments}
                          anchorClass="comments social-cell"
                          shareText="comment"
                        />
                      ) : (
                        ""
                      )}
                      {/* <ShareIconWrapper
                      type="share"
                      clickFn={this.openShareList}
                      anchorClass=""
                      shareText=""
                    /> */}
                    </div>
                  </div>
                  <div className="shareFooter">
                    <h3 />
                  </div>
                </div>
              </div>,
              document.getElementById("socialshare-parent-container"),
            )
          ) : (
            <div id="shareModal" className={`${styles.shareModalContainer} shareModalContainer`}>
              <div className={`${styles.shareContent} shareContent`}>
                <div className={`${styles.shareHeader} shareHeader`}>
                  <span className="close_icon close_position" onClick={this.closeShareModal.bind(this)} />
                  <h3>{locale.share_popup_header}</h3>
                </div>
                <div className="shareBody">
                  <div className="social-table">
                    {!isMobilePlatform() && (
                      <ShareIconWrapper
                        type="whatsapp"
                        clickFn={this.shareOnWhatsapp}
                        anchorClass="whatsapp social-cell"
                        shareText="Whatsapp"
                      />
                    )}

                    <ShareIconWrapper
                      type="facebook"
                      clickFn={this.shareOnFacebook}
                      anchorClass="fb social-cell"
                      shareText="Facebook"
                    />
                  </div>
                  <div className="social-table">
                    <ShareIconWrapper
                      type="telegram"
                      clickFn={this.shareonTgm}
                      anchorClass="telegram social-cell"
                      shareText="Telegram"
                    />
                    <ShareIconWrapper
                      type="twitter"
                      clickFn={this.shareOnTwitter}
                      anchorClass="twitter social-cell"
                      shareText="Twitter"
                    />
                    {pageType !== "newsbrief" && (
                      <ShareIconWrapper
                        type="email"
                        clickFn={this.shareOnEmail}
                        anchorClass="email social-cell"
                        shareText="Email"
                      />
                    )}

                    {pageType !== "newsbrief" && (
                      <ShareIconWrapper
                        type="sms"
                        clickFn={this.shareOnSms}
                        anchorClass="sms social-cell"
                        shareText="SMS"
                      />
                    )}
                    {pageType !== "newsbrief" && !isMobilePlatform() ? (
                      <ShareIconWrapper
                        type="comments"
                        clickFn={this.showComments}
                        anchorClass="comments social-cell"
                        shareText="comment"
                      />
                    ) : (
                      ""
                    )}
                    {/* <ShareIconWrapper
                type="share"
                clickFn={this.openShareList}
                anchorClass=""
                shareText=""
              /> */}
                  </div>
                </div>
                <div className="shareFooter">
                  <h3 />
                </div>
              </div>
            </div>
          )
        ) : null}

        <div className="share_icon">
          <ShareIconWrapper type="share" clickFn={this.openShareList} anchorClass="" shareText="" />
        </div>
      </ErrorBoundary>
    );
  }
}

const ShareIconWrapper = ({ type, clickFn, anchorClass, shareText }) => (
  <span onClick={clickFn} className={anchorClass}>
    <div className="iconsquare">
      <SvgIcon name={type} />
    </div>
    {shareText ? <div className="socialText">{shareText}</div> : ""}
  </span>
);

export default withRouter(SocialShare);
