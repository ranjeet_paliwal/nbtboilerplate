import React from "react";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import { AnalyticsGA } from "../lib/analytics";

const ElectionExitPollWidget = props => {
  let { isFrmapp } = props;
  let _link = "/elections/assembly-elections/exit-polls";
  _link = _link + (isFrmapp ? "?frmapp=yes" : "");
  let arrData = props.exitpolldata ? props.exitpolldata.data : null;
  arrData = arrData
    ? Array.from(Object.keys(arrData), key => {
        let _data = arrData[key];
        return _data;
      })
    : [];
  return (
    <div className="election_result_widget">
      <div className="election_head">
        <h2>
          <a href={_link}>
            <b>
              <i>Election</i>
              <i>Exit Poll 2018</i>
            </b>
          </a>
        </h2>
        {isFrmapp ? null : (
          <a
            className="actionbtn"
            onClick={() => {
              // AnalyticsGA.event({
              //   category: "app_download",
              //   action: "click",
              //   label: "election_widget"
              // });
              getAppDowld();
            }}
          >
            App Download
          </a>
        )}
      </div>
      {arrData.length > 0 ? (
        <ErrorBoundary>
          <div className="election_table">
            <a href={_link}>
              <table cellPadding="0" cellSpacing="0" border="0" data-attr="eticker-table">
                <tbody>
                  {arrData.map((item, index) => {
                    return <tr>{tdItems(item)}</tr>;
                  })}
                </tbody>
              </table>
            </a>
          </div>
        </ErrorBoundary>
      ) : null}
      {props.exitpolldata.disclaimer ? (
        <div className="disclaimer">
          <div>{props.exitpolldata.disclaimer.split("|")[0]}</div>
          <div>{props.exitpolldata.disclaimer.split("|")[1]}</div>
        </div>
      ) : null}
    </div>
  );
};

const getAppDowld = () => {
  window.open(
    "https://navbharattimes.indiatimes.com/off-url-forward/appdownload_mweb.cms?wapcode=nbt&applink=https://navbharattimes.onelink.me/cMxT/406fd538",
  );
};

const tdItems = item => {
  let tdArr = [];
  for (var key in item) {
    tdArr.push(
      key == "src" || key == "seats" ? null : key == "name" ? (
        <td className="state-name">
          {item[key]}
          <span>
            {item["seats"]}
            <sup>{item["src"]}</sup>
          </span>
        </td>
      ) : item[key] != "" ? (
        <td className="party">
          <div className="seats">{item[key]}</div>
          <span>{key.replace("-", "+")}</span>
        </td>
      ) : (
        <td></td>
      ),
    );
  }

  return tdArr;
};

export default ElectionExitPollWidget;
