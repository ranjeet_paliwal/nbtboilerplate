import React from "react";
import { connect } from "react-redux";

import GridSectionMaker from "./ListingCards/GridSectionMaker";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import { isMobilePlatform } from "../../utils/util";

/* 
    Top Story Widget 
    used in desktop 
    and mobile
     articleshow
*/

const TopStoryWidget = ({ home, numItems = 5 }) => {
  return home && home.value && home.value[0] && Array.isArray(home.value[0].items) ? (
    <div className="row box-item top_stories">
      <div className="section">
        <div className="top_section">
          <h3>
            <span>Top Stories</span>
          </h3>
        </div>
      </div>
      <GridSectionMaker
        type={isMobilePlatform() ? defaultDesignConfigs.horizontalSlider : defaultDesignConfigs.topicslisting}
        data={[].concat(home.value[0].items.slice(0, numItems + 1))}
      />
    </div>
  ) : null;
};

function mapStateToProps({ home }) {
  return { home };
}

export default connect(mapStateToProps)(TopStoryWidget);
