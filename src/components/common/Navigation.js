import React, { Component } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import { _getStaticConfig } from "../../utils/util";
import { renderHrNav, startItr } from "../lib/navigation/index";
import { fetchHeaderDataIfNeeded, fetchHoverDataIfNeeded } from "../../actions/header/header";

const siteConfig = _getStaticConfig();

class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hoverShown: false,
    };
    this.config = {
      isFrmapp: false,
    };
  }

  componentDidMount() {
    const { dispatch } = this.props;
    let _this = this;
    dispatch(fetchHeaderDataIfNeeded());
  }

  renderHoverSection = msid => {
    dispatch(fetchHoverDataIfNeeded({ msid }));
  };

  render() {
    const { header, location } = this.props;
    let path = this.props.location.pathname;
    let newlist = Object.assign({}, header);

    let msid =
      this.props.config.pagetype === "articleshow" ||
      this.props.config.pagetype === "videoshow" ||
      this.props.config.pagetype === "photoshow"
        ? this.props.config.parentId
        : this.props.params.msid;

    return (
      <React.Fragment>
        {path.indexOf("/newsbrief") > -1
          ? null
          : path === "/" ||
            path === "/pwa_home.cms" ||
            path === "/default.cms" ||
            path === "/amp_default.cms" ||
            path === "/newsbrief"
          ? renderHrNav(newlist, {}, "/")
          : path.indexOf("19615041") > -1
          ? startItr(newlist, {}, "19615041", (childnodes, parentref, msid) => {
              return renderHrNav(childnodes, parentref, msid);
            })
          : startItr(newlist, {}, msid, (childnodes, parentref, msid) => {
              return renderHrNav(childnodes, parentref, msid);
            })}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.app,
    header: state.header,
    config: state.config,
  };
}

export default connect(mapStateToProps)(Navigation);
