import React, { Component } from "react";
import PropTypes from "prop-types";
import { _isCSR } from "../../utils/util";

const fadeIn = `
  @keyframes gracefulimage {
    0%   { opacity: 0.25; }
    50%  { opacity: 0.5; }
    100% { opacity: 1; }
  }
`;

class LazyIntersection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
    };
  }

  /*
    Creating a stylesheet to hold the fading animation
  */
  addAnimationStyles() {
    const exists = document.head.querySelectorAll("[data-gracefulimage]");

    if (!exists.length) {
      const styleElement = document.createElement("style");
      styleElement.setAttribute("data-gracefulimage", "exists");
      document.head.appendChild(styleElement);
      styleElement.sheet.insertRule(fadeIn, styleElement.sheet.cssRules.length);
    }
  }

  /*
    Attempts to download an image, and tracks its success / failure
  */
  loadImage() {
    const image = new Image();
    image.onload = () => {
      this.setState({ loaded: true });
    };
    image.onerror = () => {
      this.setState({ loaded: false });
    };
    image.src = this.props.src;
  }

  /*
    Attempts to load an image src passed via props
    and utilises image events to track sccess / failure of the loading
  */
  componentDidMount() {
    this.addAnimationStyles();
    let _this = this;
    // if user wants to lazy load
    if (!_this.props.noLazyLoad && _isCSR() && _this.ImageEle) {
      var elem = _this.ImageEle;
      // continue if IntersectionObserver API support available
      if ("IntersectionObserver" in window) {
        _this.elemObserver = new IntersectionObserver(
          function(entries, observer) {
            entries.forEach(function(entry) {
              if (entry.isIntersecting) {
                let tthis = entry.target;
                _this.loadImage();
                // stop watching this element
                _this.elemObserver.unobserve(tthis);
              }
            });
          },
          { rootMargin: "0px 0px 100px 0px" },
        ); //Root Margin given to load images just before coming to viewport
        _this.elemObserver.observe(elem);
      } else {
        // fall back
        //console.log('!IntersectionObserver');
        _this.loadImage();
      }
    }
  }

  /*
    clear any existing event listeners
  */
  componentWillUnmount() {
    let _this = this;
    if (_this.ImageEle) {
      _this.elemObserver.unobserve(_this.ImageEle);
    }
  }

  shouldComponentUpdate(nextProps) {
    return this.state.loaded === false;
  }

  /*
    - If image hasn't yet loaded AND user didn't want a placeholder then don't render anything
    - Else if image has loaded then render the image
    - Else render the placeholder
  */
  render() {
    if (!this.state.loaded && this.props.noPlaceholder) return null;

    const src = this.state.loaded ? this.props.src : this.props.placeholder;
    const style = !this.state.loaded ? { backgroundImage: "url(" + this.props.placeholder + ")" } : "";

    return (
      <img
        src={src}
        data-src={this.props.datasrc}
        className={this.props.className}
        width={this.props.width}
        height={this.props.height}
        data-adgdisplay={this.props.adgdisplay}
        style={{
          ...style,
          ...this.props.style,
        }}
        alt={this.props.alt}
        title={this.props.title}
        ref={this.state.loaded ? null : ref => (this.ImageEle = ref)}
      />
    );
  }
}

LazyIntersection.defaultProps = {
  src: null,
  className: null,
  width: null,
  height: null,
  alt: "NBT",
  style: {},
  placeholder: null,
  noPlaceholder: false,
  noLazyLoad: false,
};

LazyIntersection.propTypes = {
  src: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  alt: PropTypes.string,
  style: PropTypes.object,
  noPlaceholder: PropTypes.bool,
  noLazyLoad: PropTypes.bool,
};

export default LazyIntersection;
