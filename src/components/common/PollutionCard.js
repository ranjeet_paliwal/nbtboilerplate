import React, { PureComponent } from "react";
import { connect } from "react-redux";
import "./css/UtilityWidgets.scss";
import SvgIcon from "./SvgIcon";
import fetch from "../../utils/fetch/fetch";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";

class PollutionCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pm10: "",
      pm25: "",
    };
  }

  componentDidMount() {
    if (this.props.selectedCity) {
      this.fetchData(this.props.selectedCity);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.selectedCity !== this.props.selectedCity) {
      this.fetchData(this.props.selectedCity);
    }
  }

  fetchData(city) {
    fetch(`https://livepollutiondata.loki.ai/cities/${city}.json`)
      .then(data => {
        this.setState({ pm25: data.pm25, pm10: data.pm10 });
      })
      .catch(e => {
        this.setState({ pm25: "", pm10: "" });
      });
  }

  getStatusColor = pm25 => {
    if (pm25 <= 50) {
      return "green";
    }
    if (pm25 <= 100) {
      return "yellow";
    }
    if (pm25 <= 150) {
      return "orange";
    }
    if (pm25 <= 200) {
      return "red";
    }
    if (pm25 <= 300) {
      return "purple";
    }
    return "maroon";
  };

  render() {
    const { pm25, pm10 } = this.state;
    const statusColor = this.getStatusColor(parseInt(pm25));
    return (
      <ErrorBoundary>
        <div className="utility-widget">
          <div className="table">
            <div className="table_row">
              <div className="table_col iconcol">
                <SvgIcon name="polution" width="31" height="44" />
              </div>
              {/*
              Green	    Good	                          0 to 50
              Yellow	  Moderate	                      51 to 100
              Orange	  Unhealthy for Sensitive Groups	101 to 150
              Red	      Unhealthy	                      151 to 200
              Purple	  Very Unhealthy	                201 to 300
              Maroon	  Hazardous	                      301 and higher
              */}
              <div className="table_col">
                <div className="polutiondetails">
                  PM 2.5 - {pm25 ? <span className={`value color-${statusColor}`}>{pm25}</span> : "-"}
                </div>
              </div>
              <div className="table_col lastcol">
                <div className="polutiondetails">
                  PM 10 - {pm10 ? <span className={`value color-${statusColor}`}>{pm10}</span> : "-"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </ErrorBoundary>
    );
  }
}
const mapStateToProps = state => ({
  fuel: state.app.fuel,
});
export default connect(mapStateToProps)(PollutionCard);
