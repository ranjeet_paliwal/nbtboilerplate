import React, { PureComponent } from "react";
import PropTypes from "prop-types";
// import { createConfig } from '../../../modules/videoplayer/utils';
import "./css/desktop/GadgetNow.scss";
import Slider from "../desktop/Slider/Gadget";
import { _getStaticConfig, isMobilePlatform } from "../../utils/util";
import AnchorLink from "./AnchorLink";
import CategoryList from "../common/Gadgets/CategoryList/CategoryList";
import CategoryListGadgets from "../common/Gadgets/CategoryList/CategoryListGadgets";

import gadgetConfig from "../../utils/gadgetsConfig";
const siteConfig = _getStaticConfig();

class GadgetSlider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tab: "mobile",
      toggle: "latest",
      tabFeed: props.gadgetsData,
      isFetching: true,
    };
    this.toggleConfig = {
      latest: "latest",
      popular: "popular",
    };
    this.inFocusLinkConfig = {
      firstLink: "firstLink",
      secondLink: "secondLink",
    };
    this.perPageCount = isMobilePlatform() === true ? 5 : 10;
  }

  componentDidMount() {
    const { tab, toggle } = this.state;
    this.fetchSLiderData(tab, toggle);
  }

  tabClicked = obj => {
    obj.preventDefault();
    let { tab, toggle } = this.state;

    tab = obj.currentTarget.id;
    //  tab = currentTab;
    toggle = this.toggleConfig.latest;
    this.setState({ tab, toggle });
    this.fetchSLiderData(tab, toggle);
    return false;
  };

  /*   tabClicked = currentTab => e => {
    let { tab, toggle } = this.state;
    tab = currentTab;
    toggle = this.toggleConfig.latest;
    this.setState({ tab, toggle });
    this.fetchSLiderData(currentTab, toggle);
    return false;
  }; */

  toggleClicked = currentToggle => e => {
    let { tab, toggle } = this.state;
    this.preventDefaultAndPropagation(e);
    toggle = currentToggle;
    this.setState({ toggle });
    this.fetchSLiderData(tab, currentToggle);
  };

  preventDefaultAndPropagation = e => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    if (e.preventDefault) {
      e.preventDefault();
    }
  };

  inFocusClickHandler = click => e => {
    const { tabFeed } = this.state;
    if (e.preventDefault) {
      e.preventDefault();
    }
    if (click === this.inFocusLinkConfig.firstLink) {
      window.location.href = tabFeed && tabFeed.gadgetstrend.length > 0 && tabFeed.gadgetstrend[0].override;
    } else if (click === this.inFocusLinkConfig.secondLink) {
      window.location.href = tabFeed && tabFeed.gadgetstrend.length > 0 && tabFeed.gadgetstrend[1].override;
    }
  };

  submitRating = item => e => {
    this.preventDefaultAndPropagation(e);
    const { tab } = this.state;
    window.open(`${siteConfig.mweburl}/tech/${gadgetConfig.gadgetCategories[tab]}/${item.lcuname}#rate`);
  };

  removespace = title => {
    if (typeof title !== "undefined" && typeof title !== "object") {
      title = title
        .trim()
        .replace(/\s+/g, "-")
        .toLowerCase();
      return title;
    }
  };

  fetchSLiderData(tab, toggle) {
    let { tabFeed, isFetching } = this.state;
    isFetching = true;
    this.setState({ isFetching });
    fetch(
      `${process.env.API_BASEPOINT}/pwafeeds/sc_gadgetlist.cms?category=${tab}&feedtype=sjson&pagetype=widget&perpage=${this.perPageCount}&sort=${toggle}`,
    )
      .then(res => res.json())
      .then(response => {
        tabFeed = response;
        isFetching = false;
        this.setState({ tabFeed, isFetching });
      });
  }

  clickLink = event => {
    event.preventDefault();
  };

  render() {
    const { tabFeed, tab } = this.state;
    const { source } = this.props;

    return (
      <div className="col12 wdt-inline-gadgets">
        {!isMobilePlatform() && tabFeed && tabFeed.gadgetstrend && tabFeed.gadgetstrend.length > 0 && (
          <div className="seo-gadget-tags">
            <span onClick={this.inFocusClickHandler(this.inFocusLinkConfig.firstLink)}>
              {tabFeed.gadgetstrend && tabFeed.gadgetstrend.length > 0 && tabFeed.gadgetstrend[0].hl}
            </span>
            <span onClick={this.inFocusClickHandler(this.inFocusLinkConfig.secondLink)}>
              {tabFeed.gadgetstrend && tabFeed.gadgetstrend.length > 0 && tabFeed.gadgetstrend[1].hl}
            </span>
          </div>
        )}

        <CategoryListGadgets clicked={this.tabClicked.bind(this)} activeGadget={tab} slider />

        {/* tabs */}
        <ul className="gn-tabs">
          <li
            className={this.state.toggle == this.toggleConfig.latest ? "active" : ""}
            onClick={this.toggleClicked(this.toggleConfig.latest)}
          >
            <AnchorLink
              href={`${process.env.WEBSITE_URL}tech/mobile-phones/filters/sort=latest`}
              onClick={this.clickLink}
            >
              {siteConfig.locale.tech.latest}
            </AnchorLink>
          </li>
          <li
            className={this.state.toggle == this.toggleConfig.popular ? "active" : ""}
            onClick={this.toggleClicked(this.toggleConfig.popular)}
          >
            <AnchorLink href={`${process.env.WEBSITE_URL}tech/mobile-phones`} onClick={this.clickLink}>
              {siteConfig.locale.tech.popular}
            </AnchorLink>
          </li>
        </ul>

        {/* Gadgets content */}

        <div className="inline-gadgets-content">
          {tabFeed ? (
            <Slider
              type="inlineGadgets"
              size="3"
              sliderData={tabFeed.gadgets}
              width={isMobilePlatform() === true ? "" : "195"}
              // parentMsid="60309788"
              // noSeo={true}
              sliderClass="trendingslider"
              videoIntensive={false}
              toggle={
                this.state.toggle === this.toggleConfig.latest
                  ? siteConfig.locale.tech.latest
                  : siteConfig.locale.tech.popular
              }
              submitRating={this.submitRating}
              //gadgetItemClick={this.onGadgetItemClick}
              // amazonBuyClick={this.onAmazonBuyButtonClick}
              // islinkable="false"
              // link={astroLink}
              margin={isMobilePlatform() === true ? "" : "10"}
              gadgetCategory={tab}
            />
          ) : (
            <div className="wdt_loading">
              <span className="spinner_circle" />
            </div>
          )}
        </div>
      </div>
    );
  }
}

GadgetSlider.propTypes = {
  alaskaData: PropTypes.object,
};

export default GadgetSlider;
