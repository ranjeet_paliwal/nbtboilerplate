import React, { PureComponent } from "react";
import PropTypes from "prop-types";
// import { createConfig } from '../../../modules/videoplayer/utils';
import "./css/desktop/GadgetNow.scss";
import FakeNewsListCard from "./FakeCards/FakeNewsListCard";
import Slider from "../desktop/Slider/Gadget";
import { isMobilePlatform, _getStaticConfig } from "../../utils/util";
import gadgetsConfig from "../../utils/gadgetsConfig";
import AnchorLink from "./AnchorLink";

const siteConfig = _getStaticConfig();

const gadgetSliderConfig = siteConfig.locale.tech.gadgetSliderConfig;
class BrandSlider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      tab: "mobile",
      tabFeed: null,
      isFetching: true,
    };
    this.displayMap = {
      mobile: gadgetSliderConfig.mobile,
      laptop: gadgetSliderConfig.laptop,
      tablet: gadgetSliderConfig.tablet,
      camera: gadgetSliderConfig.camera,
      smartwatch: gadgetSliderConfig.smartwatch,
      fitnessband: gadgetSliderConfig.fitnessband,
    };
    this.brandUrlMapping = gadgetsConfig.gadgetCategories;
  }

  componentDidMount() {
    const { tab } = this.state;
    this.fetchSLiderData(tab);
  }

  tabClicked = currentTab => e => {
    let selectedTab = currentTab;
    if (!currentTab) {
      selectedTab = e.target.value;
    }
    let { tab } = this.state;
    tab = selectedTab;
    this.setState({ tab });
    this.fetchSLiderData(selectedTab);
  };

  brandClicked = item => e => {
    if (e.preventDefault) {
      e.preventDefault();
    }
    window.location.href = `${process.env.API_BASEPOINT}/tech/${this.brandUrlMapping[this.state.tab]}/${item.lcuname}`;
  };

  fetchSLiderData(tab) {
    let { tabFeed, isFetching } = this.state;
    isFetching = true;
    this.setState({ isFetching });
    fetch(
      // `https://langdev8352.indiatimes.com/feeds/sc_brandlist.cms?feedtype=sjson&category=${tab}`,
      `${process.env.API_BASEPOINT}/pwafeeds/sc_brandlist.cms?feedtype=sjson&category=${tab}`,
    )
      .then(res => res.json())
      .then(response => {
        tabFeed = response;
        isFetching = false;
        this.setState({ tabFeed, isFetching });
      });
  }

  render() {
    const { tabFeed, tab, isFetching } = this.state;

    const gadgetCategories =
      siteConfig && siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.category;
    const category = this.brandUrlMapping[tab];
    return (
      <div className="col12 brands_slider">
        {isMobilePlatform() ? (
          <div className="change_device">
            <label>{this.displayMap[this.state.tab]}</label>
            <select onChange={this.tabClicked()}>
              {gadgetCategories.map(item => {
                const activeClass = tab == item.keyword ? "active" : false;
                return (
                  <option className={activeClass} value={item.keyword}>
                    {item.keylabel}
                  </option>
                );
              })}
            </select>
          </div>
        ) : (
          <ul className="tabs">
            {gadgetCategories.map(item => {
              const activeClass = tab == item.keyword ? "active" : false;
              return (
                <li className={activeClass} onClick={this.tabClicked(item.keyword)}>
                  {item.keylabel}
                </li>
              );
            })}
          </ul>
        )}
        {tabFeed ? (
          <div className="brands_content">
            {!isFetching ? (
              <DisplayBrands data={tabFeed.brands} category={category} />
            ) : (
              <div className="wdt_loading">
                <span className="spinner_circle" />
              </div>
            )}
          </div>
        ) : null}
      </div>
    );
  }
}

const DisplayBrands = ({ data, category }) => {
  if (Array.isArray(data)) {
    data = isMobilePlatform() ? data.slice(0, 4) : data.slice(0, 16);
  }

  return Array.isArray(data) ? (
    <ul>
      {data.map((card, index) => (
        <li key={index}>
          <AnchorLink href={`${siteConfig.mweburl}/tech/${category}/${card.lcuname}`}>
            <div className="slide">
              <img src={`${siteConfig.mweburl}/photo/${card.imageMsid}.cms`} alt={card.name} title={card.name} />
            </div>
          </AnchorLink>
        </li>
      ))}
    </ul>
  ) : null;
};

BrandSlider.propTypes = {
  alaskaData: PropTypes.object,
};

export default BrandSlider;
