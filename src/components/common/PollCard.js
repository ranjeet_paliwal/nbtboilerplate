import React from "react";
// import PropTypes from 'prop-types'
import styles from "./css/PollCard.scss";
import GRecaptch from "./GRecaptch";
import { AnalyticsGA } from "../lib/analytics/index";

import { _isCSR, _checkUserStatus, _getCookie } from "../../utils/util";
import { _getStaticConfig } from "../../utils/util";
import { openLoginPopup } from "./ssoLogin";

const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;
const gRecaptchaKey = siteConfig.gRecaptchaKey;

const PollCard = props => {
  const { data } = props;
  const pollid = data.pollid.toString();
  let widgetId;
  const pollcontainerclass = props.widget == "true" ? "pollContainer wdt" : "pollContainer";

  const asca = () => {
    widgetId = grecaptcha.render("example1", {
      sitekey: "your_site_key",
    });
  };

  const moveProgressBar = () => {
    const animationLength = 1000;
    const childs = document.querySelectorAll(".progress-child");
    childs.forEach((ele, i) => {
      ele.style.width = `${parseInt(ele.getAttribute("data-progress-percent"))}%`;
    });
  };
  const fireloginwindow = e => {
    openLoginPopup();
    //document.querySelector(".user-controls .bottom").click();
  };
  const validatePoll = e => {
    e.preventDefault();

    window.recaptchaElems = window.recaptchaElems ? window.recaptchaElems : document.querySelectorAll(".g-recaptcha");

    let elem = e.target;
    let recaptchaIndex = 0;

    while (elem.nodeName != "FORM") {
      elem = elem.parentElement;
    }

    if (recaptchaElems.length > 1) {
      recaptchaElems.forEach((ele, index) => {
        if (ele.hasAttribute("id")) {
          recaptchaIndex = ele.getAttribute("id").indexOf(pollid) > -1 ? index : recaptchaIndex;
        }
      });
    }

    if (grecaptcha && grecaptcha.getResponse(recaptchaIndex).length > 0) {
      if (elem.PRadio.value == null || elem.PRadio.value == undefined || elem.PRadio.value == "") {
        alert("Please select an Option");
        return false;
      }
      const formData = Array.from(new FormData(elem), e => e.map(encodeURIComponent).join("=")).join("&");
      let apiUrl = "";
      if (["development", "stg1", "stg2"].includes(process.env.DEV_ENV)) {
        // apiUrl = 'https://langdev9350.indiatimes.com/pwafeeds/wdt_polllistresult.cms';
        apiUrl = `${process.env.API_BASEPOINT}/off-url-forward/wdt_polllistresult.cms`;
      } else {
        apiUrl = `${siteConfig.mweburl}/off-url-forward/wdt_polllistresult.cms`;
      }
      fetch(apiUrl, {
        method: "post",
        body: formData,
        headers: {
          Accept: "application/json, application/xml, text/play, text/html, *.*",
          "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
        },
      })
        .then(response => response.text())
        .then(html => {
          document.getElementById(`pollform_${pollid}`).innerHTML = html;
          AnalyticsGA.event(siteConfig.poll_ga.poll_submit);
          moveProgressBar();
        })
        .catch(err => {});
      // check login status to show result of Poll or not
      const userLoggedStatus = !!window._user;
      if (!window._user) {
        document.getElementById(`polldivLoginLayer_${pollid}`).classList.remove("hide");
        document.getElementById(`pollform_${pollid}`).classList.add("hide");
        const pollvotestatus = _getCookie(`POLLID${pollid}`);
        if (pollvotestatus != null && pollvotestatus != undefined)
          document.getElementById(`votemsg_${pollid}`).innerHTML = siteConfig.locale.votealready;
      }
      _checkUserStatus(user => {
        if (user.detail) {
          document.getElementById(`polldivLoginLayer_${pollid}`).classList.add("hide");
          document.getElementById(`pollform_${pollid}`).classList.remove("hide");
          if (userLoggedStatus) AnalyticsGA.event(siteConfig.poll_ga.poll_result_loggedIn);
          else AnalyticsGA.event(siteConfig.poll_ga.poll_result_nonloggedIn);
        }
      });
    } else {
      alert("Please validate captcha");
      return false;
    }
  };

  return (
    <div id="polldiv" data-attr="polldiv" data-exclude="amp" className={pollcontainerclass}>
      <form method="post" name={`pollform_${pollid}`} id={`pollform_${pollid}`}>
        <h4>{data.polltext}</h4>

        <div className="poll_content">
          <ul>
            {data.polloption &&
              data.polloption.map((item, index) => (
                <li key={index}>
                  <label>
                    {item.optiontext}
                    <input
                      className="form-input"
                      type="radio"
                      valign="center"
                      name="PRadio"
                      value={item.optionno}
                      required="required"
                    />
                  </label>
                </li>
              ))}
            <input type="hidden" name="txtPolliD" value={pollid} />
            <input type="hidden" name="clname" value="VSNL" />
          </ul>
          <div className="captcha-box">
            <input
              type="submit"
              className={`${styles.btnsubmit} btnsubmit`}
              onClick={validatePoll.bind(this)}
              id={`submit_${pollid}`}
              value={siteConfig.locale.submit_vote}
            />
            {/* <div className = {styles["g-recaptcha"]+" g-recaptcha"} data-sitekey={gRecaptchaKey} ></div> */}

            {GRecaptch({
              styles: styles["g-recaptcha"],
              gRecaptchaKey,
              id: `grecaptcha_${pollid}`,
            })}
          </div>
        </div>
      </form>
      <div className="hide login-before-result" id={`polldivLoginLayer_${pollid}`}>
        <span className="check_icon" />
        <h5>
          <span id={`votemsg_${pollid}`}>{siteConfig.locale.votesuccessfully}</span>
          {siteConfig.locale.thanks}
        </h5>
        <span onClick={fireloginwindow.bind(this)} className="btn-login">
          Login to View Poll Results
        </span>
      </div>
    </div>
  );
};

export default PollCard;
