import React from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
import { defaultDesignConfigs } from "../../../containers/defaultDesignConfigs";
import fetch from "../../../utils/fetch/fetch";
import { isMobilePlatform, _getStaticConfig } from "../../../utils/util";
import Slider from "../../desktop/Slider/Slider";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import AdCard from "../AdCard";
import AnchorLink from "../AnchorLink";
import GridSectionMaker from "../ListingCards/GridSectionMaker";
import LiveBlog from "../LiveBlog/LiveBlog";

const siteConfig = _getStaticConfig();

class BudgetWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      budgetFeedData: "",
      sliderFeedData: "",
      isFetching: true,
    };
  }
  componentDidMount() {
    const { app } = this.props;
    const { budget } = app;
    let { budgetFeedData, sliderFeedData, isFetching } = this.state;
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;

    fetch(
      `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${budgetLbConfig.msid}&type=list&feedtype=sjson&ctnadtype=homedefault&layouttype=hpui2`,
    ).then(response => {
      budgetFeedData = response;
      isFetching = false;
      this.setState({ budgetFeedData, isFetching });
    });
    fetch(
      `${process.env.API_BASEPOINT}/sc_homelist.cms?msid=${budgetLbConfig.sliderMsid}&type=list&feedtype=sjson&ctnadtype=homedefault&layouttype=hpui2`,
    ).then(response => {
      sliderFeedData = response;
      this.setState({ sliderFeedData });
    });
  }
  render() {
    const { alaskaData, app } = this.props;
    const { budgetFeedData, sliderFeedData, isFetching } = this.state;
    const { budget } = app;
    let filtertedSliderFeedData = "";
    let filterBudgetFeedData = "";
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    filtertedSliderFeedData =
      sliderFeedData &&
      sliderFeedData.items &&
      sliderFeedData.items.filter(item => {
        return item.tn !== "ad";
      });
    filterBudgetFeedData =
      budgetFeedData &&
      budgetFeedData.items &&
      budgetFeedData.items.filter(item => {
        return item.tn !== "ad";
      });
    return budgetFeedData && !isFetching ? (
      <div className="wdt_budget" data-exclude="amp">
        {isMobilePlatform() && budgetLbConfig.liveBlogStatus === "true" ? (
          <div className="row">
            <div className="col12">
              <ErrorBoundary key="liveblog_section">
                <LiveBlog type="liveblog" dataSource={budgetLbConfig.feedPath} cssClass="wdt_lb_home" />
              </ErrorBoundary>
            </div>
          </div>
        ) : null}
        <div className="budget_head">
          <h3>
            <AnchorLink
              href={`${siteConfig.mweburl}/${budgetFeedData.seolocation}/articlelist/${budgetFeedData.id}.cms`}
            >
              {budgetFeedData.secname}
            </AnchorLink>
          </h3>
        </div>
        <div>
          <div className="row bdt_top">
            <div className={isMobilePlatform() ? "col12" : "col4"}>
              {filterBudgetFeedData && (
                <div className="row">
                  <GridSectionMaker
                    type={isMobilePlatform() ? "topicslisting" : "listWithOnlyLeadImage"}
                    data={filterBudgetFeedData && filterBudgetFeedData.slice(0, 4)}
                  />
                </div>
              )}
            </div>
            {!isMobilePlatform() ? (
              <ErrorBoundary>
                <div className="col4 pd0">
                  {budgetLbConfig.liveBlogStatus === "true" ? (
                    <ErrorBoundary key="liveblog_section">
                      <LiveBlog type="liveblog" dataSource={budgetLbConfig.feedPath} cssClass="wdt_lb_home" />
                    </ErrorBoundary>
                  ) : (
                    <GridSectionMaker
                      type={"listWithOnlyLeadImage"}
                      data={filterBudgetFeedData && filterBudgetFeedData.slice(3, 6)}
                    />
                  )}
                </div>
                <div className="col4">
                  <AdCard mstype="mrec1" adtype="dfp" className="box-item ad-top" pagetype="home" />
                </div>
              </ErrorBoundary>
            ) : null}
          </div>
          <div className="row cheaper_dearer">
            {filtertedSliderFeedData &&
              (isMobilePlatform() ? (
                <GridSectionMaker
                  type={defaultDesignConfigs.horizontalSlider}
                  data={filtertedSliderFeedData && filtertedSliderFeedData}
                  imgsize={"bigimage"}
                />
              ) : (
                <Slider
                  type="budgetSlider"
                  size="3"
                  width="306"
                  margin="20"
                  sliderData={filtertedSliderFeedData && filtertedSliderFeedData}
                />
              ))}
          </div>
        </div>
      </div>
    ) : null;
  }
}
function mapStateToProps(state) {
  return {
    isFetching: state.home.isFetching,
    sections: state.home.sections,
    app: state.app,
    alaskaData: state.header.alaskaData,
  };
}

export default connect(mapStateToProps)(BudgetWidget);
