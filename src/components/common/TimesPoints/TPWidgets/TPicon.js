import React from "react";
import AnchorLink from "../../AnchorLink";
import { _getStaticConfig } from "../../../../utils/util";
import { AnalyticsGA } from "../../../lib/analytics";

// import tpConfig from "../TimesPointConfig";

const siteConfig = _getStaticConfig();

const tpConfig = siteConfig.TimesPoint;
const siteName = process.env.SITE;
const TPicon = ({ basePath, pageType }) => {
  const gaLabelMap = { articleshow: "AS_Crown_Click", photoshow: "PS_Crown_Click", videoshow: "VS_Crown_Click" };

  return tpConfig && tpConfig.channels && tpConfig.channels.includes(siteName) ? (
    <div
      className="tp_icondiv"
      data-exclude="amp"
      onClick={() => {
        AnalyticsGA.event({
          category: "TimesPoints Page",
          action: "TP_Page",
          label: gaLabelMap[pageType],
          overrideEvent: "tp_page_activity",
        });
      }}
    >
      <AnchorLink href={`${basePath}${tpConfig && tpConfig.tpPage && `${tpConfig.tpPage}?section=howtoearn`}`}>
        <span className="tp-sprite tp_points"></span>
      </AnchorLink>
    </div>
  ) : (
    ""
  );
};

export default TPicon;
