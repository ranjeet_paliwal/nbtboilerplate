/* Home widget for mobile tp */
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import { getUserProfile } from "../../../../actions/timespoints/sdkactions";
import AnchorLink from "../../AnchorLink";
// import tpConfig from "../TimesPointConfig";
import { _isCSR, _getStaticConfig, isMobilePlatform } from "./../../../../utils/util";
import { AnalyticsGA } from "../../../lib/analytics";
import { openLoginPopup } from "../../ssoLogin";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;

class TPWidget extends PureComponent {
  constructor(props) {
    super(props);
    this.hpWidgetGTMtrigger = false;
    this.asWidgetGTMtrigger = false;
  }

  componentDidMount() {
    const { type } = this.props;
    this.props.getUserProfile();
  }

  componentDidUpdate(prevProps) {
    // Fetch user points data again if login changes.
    if (prevProps.authentication.loggedIn !== this.props.authentication.loggedIn) {
      this.props.getUserProfile();
    }
  }

  // Currently only works on mobile
  // As homepage widget is only for mobile
  redirectOrOpenLogin = event => {
    const { authentication, userProfile } = this.props;
    const { loggedIn } = authentication;
    const arePointsZero = Boolean(userProfile && (userProfile.redeemablePoints == 0 || userProfile.totalPoints == 0));
    event.preventDefault();
    if (!loggedIn) {
      AnalyticsGA.event({
        category: "TP Widget",
        action: "HP_Widget",
        label: "Login_button",
        overrideEvent: "tp_widget_activity",
      });
      openLoginPopup("", "tp_hp_widget");
    } else {
      // Change gtm event label on same branch
      AnalyticsGA.event({
        category: "TP Widget",
        action: "HP_Widget",
        label: `${arePointsZero ? "Know_more" : "Redeem_Button"}`,
        overrideEvent: "tp_widget_activity",
      });
      // Redirect to different pages based on points
      this.props.router.push(`${tpConfig.tpPage}?section=${arePointsZero ? "howtoearn" : "offers"}`);
    }
  };

  render() {
    const { authentication, userProfile, type, translations } = this.props;

    if (type == "AS") {
      return <ASWidget authentication={authentication} userProfile={userProfile} type={type} />;
    }
    if (type == "HOME") {
      return (
        <HomeWidget
          userProfile={userProfile}
          clickBtn={this.redirectOrOpenLogin}
          translations={translations}
          authentication={authentication}
          type={type}
        />
      );
    }
  }
}
const gtmTrigger = type => {
  TPWidget.hpWidgetGTMtrigger = true;
  setTimeout(() => hpWidgetObserver(type), 0);
};

const gtmTriggerAS = type => {
  TPWidget.asWidgetGTMtrigger = true;
  setTimeout(() => asWidgetObserver(type), 0);
};

const hpWidgetObserver = type => {
  let _this = this;
  // Firing GA call only when it is coming in viewport therefore using interaction observer.
  if (_isCSR() && "IntersectionObserver" in window) {
    _this.tpWidgetObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let tthis = entry.target;
          if (type == "HOME" && _isCSR()) {
            if (TPWidget.hpWidgetGTMtrigger === true) {
              console.log("home page widget GTM trigger in interaction observer");
              AnalyticsGA.event({
                category: "TP Widget",
                action: "HP_Widget",
                label: "HP_Widget_Display",
                overrideEvent: "tp_widget_activity",
              });
              TPWidget.hpWidgetGTMtrigger = false;
            }
          }
          _this.tpWidgetObserver.unobserve(tthis);
        }
      });
    });
    document.querySelector("#tpWidget") && _this.tpWidgetObserver.observe(document.querySelector("#tpWidget"));
  } else {
    console.log("no observer");
  }
};

const asWidgetObserver = type => {
  let _this = this;
  // Firing GA call only when it is coming in viewport therefore using interaction observer.
  if (_isCSR() && "IntersectionObserver" in window) {
    _this.tpWidgetObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let tthis = entry.target;
          if (type == "AS" && _isCSR()) {
            if (TPWidget.asWidgetGTMtrigger === true) {
              AnalyticsGA.event({
                category: "TP Widget",
                action: "AS_Widget",
                label: "AS_Widget_Display",
                overrideEvent: "tp_widget_activity",
              });
              TPWidget.asWidgetGTMtrigger = false;
            }
          }
          _this.tpWidgetObserver.unobserve(tthis);
        }
      });
    });
    document.querySelector("#tpWidgetAs") && _this.tpWidgetObserver.observe(document.querySelector("#tpWidgetAs"));
  } else {
    console.log("no observer");
  }
};

const ASWidget = ({ userProfile, type, authentication }) => {
  return userProfile && userProfile.totalPoints > 0 ? (
    <div className="tp_as_widget" id="tpWidgetAs" ref={gtmTriggerAS(type)}>
      <div className="tp-sprite tpicon"></div>
      <div className="tptxt">
        <span>You have</span>
        {userProfile.totalPoints} TIMESPOINTS
      </div>
      {authentication.loggedIn ? (
        <span
          onClick={() => {
            AnalyticsGA.event({
              category: "TP Widget",
              action: "AS_Widget",
              label: "Redeem_button",
              overrideEvent: "tp_widget_activity",
            });
          }}
        >
          <AnchorLink href={`${tpConfig.tpPage}?section=offers`} className="tpbtn">
            REDEEM NOW
          </AnchorLink>
        </span>
      ) : (
        <span
          onClick={() => {
            AnalyticsGA.event({
              category: "TP Widget",
              action: "AS_Widget",
              label: "Login_button",
              overrideEvent: "tp_widget_activity",
            });

            openLoginPopup("", "tp_as_widget");
          }}
          className="tpbtn"
        >
          LOGIN
        </span>
      )}
    </div>
  ) : (
    ""
  );
};

const HomeWidget = ({ authentication, userProfile, clickBtn, translations, type }) => {
  let heading = "";
  let subText = "";
  let text = "";
  const arePointsZero = Boolean(userProfile && (userProfile.redeemablePoints == 0 || userProfile.totalPoints == 0));

  if (authentication && !authentication.loggedIn) {
    heading = "Read & earn rewards with TimesPoints!";
    text = "Now visit daily on our site & earn exciting rewards";
    subText = "Login and Redeem";
  } else if (authentication.loggedIn) {
    // userProfile && (arePointsZero)
    // heading = translations && translations.read_earn_rp;
    // text = translations && translations.visit_nd_earn;
    // subText = translations && translations.know_more;
    heading = `${
      arePointsZero
        ? "Read & earn rewards with TimesPoints!"
        : `You have ${userProfile && userProfile.redeemablePoints} points`
    }`;
    text = `${
      arePointsZero
        ? "Read articles or watch videos to start earning Timespoints"
        : `Redeem points to get exclusive gift coupons`
    }`;
    subText = `${arePointsZero ? "Know more" : `Redeem Now`}`;
  }

  return subText ? (
    <li className="tp_home_widget" id="tpWidget" ref={gtmTrigger(type)} data-exclude="amp">
      {/* FIXME: Image path hardcoded for tamil */}
      <img src="https://tamil.samayam.com/photo/78493117.cms" className="tp-bg" />
      <div className="tp-txt">
        <h4>{heading}</h4>
        <span className="desc">{text}</span>
        <span className="btn" onClick={clickBtn}>
          {subText}
        </span>
      </div>
    </li>
  ) : (
    ""
  );
};

const mapDispatchToProps = dispatch => ({
  getUserProfile: () => {
    dispatch(getUserProfile());
  },
});

const mapStateToProps = state => ({
  userProfile: state.timespoints.userProfile,
  authentication: state.authentication,
  translations: state.timespoints.translations,
});

export default connect(mapStateToProps, mapDispatchToProps)(TPWidget);
