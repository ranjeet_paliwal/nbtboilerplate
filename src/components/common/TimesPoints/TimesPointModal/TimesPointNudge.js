/* eslint-disable no-unused-expressions */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { loadDailyCheckInDetails, getUserProfile } from "../../../../actions/timespoints/sdkactions";
import { getCheckinDaysLeft } from "../timespoints.util";

// import { getTpConfig } from "../TimesPointConfig";
import { tpShowModalEmitter, OFFERS_TAB } from "./TimesPointModal";
import { _getCookie, _setCookie, _isCSR, _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import { AnalyticsGA } from "../../../lib/analytics";
import { openLoginPopup } from "../../ssoLogin";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;
const campaignDays = tpConfig.campaignDays;

class TimesPointNudge extends React.Component {
  constructor() {
    super();
    this.state = {
      showNudge: false,
    };
    this.popUpInitialised = false;
    this.moduleName = "timespointPopUp";
    this.TimesPointNudgeMod = null;
    this.bonusClassMap = {
      "-1": "upcoming",
      "0": "bonus-lost",
      "1": "bonus-earned",
    };
  }

  componentDidMount() {
    if (_isCSR()) {
      AnalyticsGA.event({
        overrideEvent: "tp_widget_activity",
        category: "TP Widget",
        action: "DailyCheckin_widget",
        label: "DC_Widget_Display",
      });
    }
    //FIXME: Change this if needed
    // if (window.TimesApps && window.TimesApps.PopUpManager) {
    //   this.registerWithPopupManager();
    // } else {
    //   window.addEventListener(POPUP_MANAGER_LOADED, this.registerWithPopupManager);
    // }

    // Set cookie showing nudge as false

    this.loadTpNudge();
  }

  componentWillUnmount() {
    //FIXME: Change this if needed
    // window.removeEventListener(POPUP_MANAGER_LOADED, this.registerWithPopupManager);
    // Widget frequency driven from Alaska
    // const { frequency } = this.props;
    // let numDays = 1;
    // if (frequency && parseInt(frequency)) {
    //   numDays = parseInt(frequency);
    // }
    // _setCookie("show_nudge", "0", numDays);
  }

  componentDidUpdate(prevProps) {
    // if (
    //   !this.popUpCancelled &&
    //   prevProps.userData &&
    //   this.props.userData &&
    //   ((typeof prevProps.userData.isLoggedIn === "undefined" &&
    //     // eslint-disable-next-line react/prop-types
    //     this.props.userData.isLoggedIn !== "undefined") ||
    //     (this.props.userData.isLoggedIn !== "undefined" && !this.popUpInitialised))
    // ) {
    //   window.clearTimeout(this.timeoutVal);
    //   this.TimesPointNudgeMod && this.TimesPointNudgeMod.onReadyCallback(1);
    //   this.popUpInitialised = true;
    // }

    if (
      this.state.showNudge &&
      // eslint-disable-next-line react/prop-types
      this.props.loggedIn !== prevProps.loggedIn
    ) {
      this.loadTpNudgeData();
    }
  }

  loadTpNudgeData = () => {
    const { dispatch } = this.props;
    this.props.loadDailyCheckInDetails();
    this.props.getUserProfile();
  };

  loadTpNudge = () => {
    this.loadTpNudgeData();
    this.setState({ showNudge: true });
  };

  closeNudge = () => {
    // Widget frequency driven from Alaska
    const { frequency } = this.props;
    let numDays = 1;
    if (frequency && parseInt(frequency)) {
      numDays = parseInt(frequency);
    }
    this.setState({ showNudge: false });
    _setCookie("show_nudge", "0", numDays);
    // this.props.fireGAEvent("Daily_Check_in_pop_up", "close");
  };

  getCheckinDaysLeftFromProps = () => {
    const { dailyCheckInDetails, allActivity } = this.props;

    return getCheckinDaysLeft(dailyCheckInDetails, allActivity);
  };

  registerWithPopupManager = () => {};

  getListItem = item => {
    const currentDate = new Date(item.timestamp);
    const html = (
      <li className={`${this.bonusClassMap[item.status]} ${item.istoday ? "active" : ""}`}>
        <span className="daily-status"></span>
        <p>
          {currentDate.getDate()} {this.props.getMonth(currentDate.getMonth(), true)}
        </p>
      </li>
    );

    return html;
  };

  getListHtml = () => {
    let html = null;
    const { dailyCheckInDetails } = this.props;
    let dailyCheckInData = [];
    if (dailyCheckInDetails && dailyCheckInDetails.length > 0) {
      dailyCheckInData = dailyCheckInDetails.slice(3, 8);
      html = (
        <React.Fragment>
          {dailyCheckInData &&
            dailyCheckInData.map((data, index) => (
              <React.Fragment key={index}>{this.getListItem(data)}</React.Fragment>
            ))}
        </React.Fragment>
      );
    }

    return html;
  };

  // redirectToTpPage = () => {
  //   window.open(
  //     `https://www.timespoints.com/products?utm_source=${siteConfig.channelCode}&utm_medium=timespoint&utm_campaign=daily_checkin`,
  //     "_blank",
  //   );
  // };

  //FIXME: Can be same for  nudge and other components
  redeemPointsHandler = () => {
    if (this.props.loggedIn) {
      if (_isCSR()) {
        AnalyticsGA.event({
          category: "TP Widget",
          action: "DailyCheckin_widget",
          label: "Redeem_button",
          overrideEvent: "tp_page_activity",
        });
      }
      if (isMobilePlatform()) {
        this.props.router.push(`${tpConfig.tpPage}?section=offers`);
        this.closeNudge();
      } else {
        tpShowModalEmitter(OFFERS_TAB);
      }
    } else {
      if (_isCSR()) {
        AnalyticsGA.event({
          category: "TP Widget",
          action: "DailyCheckin_widget",
          label: "Login_button",
          overrideEvent: "tp_page_activity",
        });
      }
      // this.redirectToTpPage();
      openLoginPopup("", "tp_dc_widget");
    }
  };

  render() {
    const { translations, dailyCheckInDetails, userData, userProfile } = this.props;

    if (!this.state.showNudge || (dailyCheckInDetails && dailyCheckInDetails.length == 0)) {
      return null;
    }

    // {
    //   userData &&
    //     userData &&
    //     userProfile &&
    //     typeof userProfile.redeemablePoints === "number" &&
    //     userProfile.redeemablePoints > 0 && (
    //       <div className="redeem-points-sticky tp-sprite">
    //         <button className="tp-btn" onClick={() => this.redeemPointsHandler(true)}>
    //           {`REDEEM ${userProfile.redeemablePoints} POINTS`}
    //         </button>
    //       </div>
    //     );
    // }
    // {
    //   !userData && userProfile.currentPoints > 0 && (
    //     <div className="redeem-points-sticky tp-sprite">
    //       <button className="tp-btn" onClick={() => this.redeemPointsHandler(true)}>
    //         {`LOGIN & REDEEM`}
    //       </button>
    //     </div>
    //   );
    // }

    let tpButtonText = "Login and Redeem";

    if (userData) {
      tpButtonText =
        userProfile && typeof userProfile.redeemablePoints === "number" && userProfile.redeemablePoints > 0
          ? `REDEEM ${userProfile.redeemablePoints} POINTS`
          : `VIEW REWARDS`;
    }

    const checkInDaysObj = this.getCheckinDaysLeftFromProps();
    return (
      <div
        className={`timespoint-popup ${isMobilePlatform() ? "mobile" : ""} `}
        id="timespoint-popup"
        style={{ display: "block" }}
      >
        <button type="button" className="close-btn" onClick={() => this.closeNudge()}>
          +
        </button>
        <div className="wrapper">
          <div className="top">
            <div className="column heading">
              <h2>
                <i className="icon tp-sprite" />

                {/* <b className="text_ellipsis">{translations && translations.visit_nd_earn}</b> */}

                <b className="text_ellipsis">Visit and Read daily to earn TimesPoints</b>

                {checkInDaysObj.checkInDays !== -1 &&
                  checkInDaysObj.checkInDays !== campaignDays &&
                  checkInDaysObj.bonusPoints !== -1 && (
                    <span className="check-in-str-popup">
                      {`You are ${campaignDays - checkInDaysObj.checkInDays} day${
                        campaignDays - checkInDaysObj.checkInDays > 1 ? "s" : ""
                      } away from ${checkInDaysObj.bonusPoints} bonus points`}
                    </span>
                  )}
              </h2>
            </div>
            {!isMobilePlatform() ? (
              <div className="column">
                <button
                  type="button"
                  className="tp-btn"
                  onClick={() => {
                    this.redeemPointsHandler(true);
                  }}
                >
                  {tpButtonText}
                </button>
              </div>
            ) : null}
          </div>
          <div className="column">
            <ul className="daily-points">{this.getListHtml()}</ul>
          </div>
          {isMobilePlatform() ? (
            <div className="column center">
              <button
                type="button"
                className="tp-btn"
                onClick={() => {
                  this.redeemPointsHandler(true);
                }}
              >
                {tpButtonText}
              </button>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

TimesPointNudge.propTypes = {
  loadDailyCheckInDetails: PropTypes.func.isRequired,
  dailyCheckInDetails: PropTypes.arrayOf(PropTypes.shape({})),
  userData: PropTypes.shape({}),
  activitiesMapList: PropTypes.shape({}),
  allActivity: PropTypes.arrayOf(PropTypes.shape({})),
  getMonth: PropTypes.func.isRequired,
  userProfile: PropTypes.shape({}),
};

TimesPointNudge.defaultProps = {
  dailyCheckInDetails: [],
  userData: {},
  activitiesMapList: {},
};

const mapStateToProps = state => ({
  //FIXME: Change this to add event listener
  // userData: _get(state, "login.userData", {})
  userData: state.authentication.userData,
  loggedIn: state.authentication.loggedIn,
  dailyCheckInDetails: state.timespoints.dailyCheckInDetails,
  allActivity: state.timespoints.allActivity,
  activitiesMapList: state.timespoints.activitiesMapList,
  translations: state.timespoints.translations,
  userProfile: state.timespoints.userProfile,
});

const mapDispatchToProps = dispatch => ({
  loadDailyCheckInDetails: () => {
    dispatch(loadDailyCheckInDetails());
  },
  getUserProfile: () => {
    dispatch(getUserProfile());
  },

  // FIXME: Moved to central component, check if needed here.
  // getAllActivities: () => {
  //   dispatch(getAllActivities());
  // },
});

export default connect(mapStateToProps, mapDispatchToProps)(TimesPointNudge);
