import React from "react";
import PropTypes from "prop-types";
// import { getTpConfig } from "../TimesPointConfig";
import { _isCSR, _getStaticConfig } from "../../../../utils/util";
import { AnalyticsGA } from "../../../lib/analytics";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;

/* Recommended logic for UI -
   20 offers in the sequence: Exclusive(All) > Top > Away > All
    (Remaining to complete 20)
    As per TML-2120 
    Changed to display 4 offers
    */
const fireGTMevent = () => {
  if (_isCSR()) {
    AnalyticsGA.event({
      category: "TimesPoints Page",
      action: "TP_Page",
      label: "Offer_Click",
      overrideEvent: "tp_page_activity",
    });
  }
};

const Offers = ({ data }) => {
  return Array.isArray(data) && data.length > 0 ? (
    <div className="tab-content offers">
      {/* loader start, needs to be in condition, will hide once data load */}
      {/* <div className="wdt_loading">
          <span className="spinner_circle" />
        </div> */}
      {/* Loader end */}
      <ul>
        {data.slice(0, 20).map(offer => (
          <li key={`${offer.productId}_${offer.partnerId}`}>
            <span className="brands">
              <img src={offer.imageurl} alt={offer.productId} />
            </span>
            <span className="offer">
              <span className="text_ellipsis">{offer.title}</span>
            </span>
            <span className="price">
              {offer.point} <i className="tp-sprite icon"></i>
            </span>
            {/* FIXME: Have to replace this with correct values (device/ticketId) */}
            <a href={offer.targetUrl} target="_blank" className="btn-offers" onClick={fireGTMevent}>
              Redeem
            </a>
          </li>
        ))}
      </ul>
      {data && data.length > 20 && (
        <div className="view-all-offers text-center">
          <a className="more-btn" href={tpConfig && tpConfig.tpRedeemLink} target="_blank">
            View All
          </a>
        </div>
      )}
    </div>
  ) : (
    <div className="tab-content offers">
      <div className="wdt_loading">
        <span className="spinner_circle" />
      </div>
    </div>
  );
};

Offers.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
};

Offers.defaultProps = {
  data: [],
};

export default Offers;
