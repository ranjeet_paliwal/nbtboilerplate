/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react";
import PropTypes from "prop-types";

class UserActivity extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  getActivityHtml = (code, activity) => {
    let returnHtml = "";
    const { activitiesMapList, translations } = this.props;
    const activityMapObj = activitiesMapList[code];
    if (!activityMapObj) {
      return null;
    }

    returnHtml = (
      <li className={"flexbox justify-between"}>
        <div className={"column"}>
          <h5>{activityMapObj.label}</h5>
          {activityMapObj.type === "checkin" &&
          activity.listBonusDetails &&
          activity.listBonusDetails[0] &&
          !activity.listBonusDetails[0].campaignAchieved ? (
            <p className={"red-text"}>
              {translations && translations.day_left_for_bonus && typeof translations.day_left_for_bonus === "string"
                ? translations.day_left_for_bonus.replace("{days}", activity.listBonusDetails[0].days_left)
                : ""}
            </p>
          ) : (
            <React.Fragment>{activity.dailyTxt && <p>{activity.dailyTxt}</p>}</React.Fragment>
          )}
        </div>
        <div className={"column act-count"}>
          <p>{activity.points > 0 ? `+${activity.points}` : 0}</p>
          <p className={"tp-sprite green-text"}>
            {activityMapObj.actDoneTxt.replace("$1", activity.count).replace("$2", activity.count > 1 ? "s" : "")}
          </p>
        </div>
      </li>
    );

    return returnHtml;
  };

  getNthDate = val => {
    if (typeof val !== "number") {
      return "";
    }

    const num = val % 10;
    let nthStr = val;
    if (val === 11 || val === 12 || val === 13) {
      nthStr += "th";
      return nthStr;
    }
    switch (num) {
      case 1:
        nthStr += "st";
        break;
      case 2:
        nthStr += "nd";
        break;
      case 3:
        nthStr += "rd";
        break;
      default:
        nthStr += "th";
        break;
    }

    return nthStr;
  };

  getCurrentDate = () => {
    const date = new Date();
    const dateStr = `${this.getNthDate(date.getDate())} ${this.props.getMonth(date.getMonth())} ${date.getFullYear()}`;

    return dateStr;
  };

  render() {
    const { userProfile, userData, dailyActivity, translations } = this.props;
    const activityKeys = Object.keys(dailyActivity || {});
    const isUserLoggedIn = !!userData;
    let name = "User";
    //FIXME: Change to our domain image
    let originalImg = "https://static.toiimg.com/photo/63379366.cms";
    if (isUserLoggedIn && userData && typeof userData.F_N === "string") {
      name = userData.F_N || name;
    }

    //FIXME: Can't find user image, correct this
    if (userData) {
      originalImg = userData.thumb || originalImg;
    }

    // Showing loader in case of this
    if (!userProfile) {
      return (
        <div className="tab-content activity">
          <div className="wdt_loading">
            <span className="spinner_circle" />
          </div>
        </div>
      );
    }

    return (
      <div className="tab-content activity">
        <div className="profile">
          <div className="inner">
            <div className="profile-header flexbox justify-between align-center">
              <figure>
                <img src={originalImg} alt="user" />
              </figure>
              <div className="details">
                <h3>{`Hello ${name}`}</h3>
              </div>
            </div>
            <div className="tp-details">
              <ul className="flexbox">
                <li>
                  <p>{translations && translations.lifetime}</p>
                  <h3>{userProfile.totalPoints || 0}</h3>
                </li>
                <li>
                  <p>{translations && translations.expired}</p>
                  <h3>{userProfile.expiredPoints || 0}</h3>
                </li>
                <li>
                  <p>{translations && translations.redeemed}</p>
                  <h3>{userProfile.redeemedPoints || 0}</h3>
                </li>
              </ul>
            </div>
            <div className="tp-footer">
              <p>{translations && translations.total_redeem_points}</p>
              <div className="flexbox justify-between align-center">
                <div className="points">
                  <i className="tp-sprite icon" />
                  <span>{(isUserLoggedIn ? userProfile.redeemablePoints : userProfile.totalPoints) || 0}</span>
                </div>
                {userProfile && typeof userProfile.redeemablePoints === "number" && userProfile.redeemablePoints > 0 && (
                  <a
                    href="javascript:void(0)"
                    className={"redeem-btn"}
                    onClick={() => this.props.redeemPointsHandler()}
                  >
                    {translations && translations.redeem}
                  </a>
                )}
                {userProfile &&
                  typeof userProfile.redeemablePoints === "number" &&
                  userProfile.redeemablePoints === 0 &&
                  !userData && (
                    <a
                      href="javascript:void(0)"
                      className={"redeem-btn"}
                      onClick={() => this.props.redeemPointsHandler()}
                    >
                      {translations && translations.redeem}
                    </a>
                  )}
              </div>
            </div>
          </div>
          <p className="token-expiry">* {translations && translations.tp_disclaimer}</p>
        </div>
        <div className="tp-box2 flexbox justify-between align-center">
          <h4>{translations && translations.today_activity}</h4>
          <p>{this.getCurrentDate()}</p>
        </div>
        <ul className={`tp-list ${activityKeys && activityKeys.length > 0 ? "" : "tp-loading"}`}>
          {activityKeys &&
            activityKeys.length > 0 &&
            activityKeys.map(data => (
              <React.Fragment key={data}>{this.getActivityHtml(data, dailyActivity[data])}</React.Fragment>
            ))}
        </ul>
      </div>
    );
  }
}

UserActivity.propTypes = {
  userProfile: PropTypes.shape({}),
  userData: PropTypes.shape({}),
  activitiesMapList: PropTypes.shape({}),
  dailyActivity: PropTypes.shape({}),
  getMonth: PropTypes.func.isRequired,
  redeemPointsHandler: PropTypes.func.isRequired,
};

UserActivity.defaultProps = {
  userProfile: {},
  userData: {},
  activitiesMapList: {},
  dailyActivity: {},
};

export default UserActivity;
