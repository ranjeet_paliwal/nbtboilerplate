/* eslint-disable react/no-danger */
import React from "react";
import PropTypes from "prop-types";

class FAQs extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return this.props.data ? (
      <div
        className="tab-content faq"
        dangerouslySetInnerHTML={{
          __html: this.props.data || "",
        }}
      />
    ) : (
      <div className="tab-content faq">
        <div className="wdt_loading">
          <span className="spinner_circle" />
        </div>
      </div>
    );
  }
}

FAQs.propTypes = {
  data: PropTypes.string,
};

FAQs.defaultProps = {
  data: "",
};

export default FAQs;
