import React from "react";
import PropTypes from "prop-types";
// import { getTpConfig } from "../TimesPointConfig";
import Slider from "../../../desktop/Slider";
import { isMobilePlatform, _isCSR, _getStaticConfig } from "../../../../utils/util";
import { AnalyticsGA } from "../../../lib/analytics";

const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;
const campaignDays = tpConfig.campaignDays;

class HowToEarn extends React.Component {
  constructor() {
    super();
    this.state = {
      showMoreList: false,
    };
    this.bonusClassMap = {
      "-1": "upcoming",
      "0": "bonus-lost",
      "1": "bonus-earned",
    };
  }

  componentDidMount() {}
  componentDidUpdate() {
    if (this.props.isMobilePlatform) {
      setTimeout(() => {
        this.swipeDailyCheckInSlider();
      }, 1000);
    }
  }
  getActivityHtml = (data, activityInfo) => {
    let returnHtml = null;
    if (!data || !data.label) {
      return null;
    }
    // not showing in new Layout, hence passing disablePrimeFlow true
    if (data.id === "free_trial" && this.props.disablePrimeFlow) {
      return null;
    }
    if (data.link) {
      const dynamicAttributes = data.attrObj ? data.attrObj : {};
      returnHtml = (
        <li
          {...dynamicAttributes}
          className={data.className}
          onClick={e => {
            this.itemClickHandler(e);
          }}
        >
          <a href={data.link} className="tp-sprite">
            <i className="tp-sprite" />
            <h5>{data.label}</h5>
            <p>
              {data.desc
                .replace(/&amp;/g, "&")
                .replace("$1", activityInfo.assign_points)
                .replace("$2", activityInfo.assign_points > 1 ? "s" : "")}
            </p>
          </a>
        </li>
      );
    } else {
      returnHtml = (
        <li className={data.className}>
          <i className="tp-sprite" />
          <h5>{data.label}</h5>
          <p>
            {data.desc
              .replace(/&amp;/g, "&")
              .replace("$1", activityInfo.assign_points)
              .replace("$2", activityInfo.assign_points > 1 ? "s" : "")}
          </p>
        </li>
      );
    }

    return returnHtml;
  };

  itemClickHandler = event => {
    const { closeModal, isMobilePlatform } = this.props;
    if (_isCSR()) {
      AnalyticsGA.event({
        category: "TimesPoints Page",
        action: "TP_Page",
        label: "Offer_Click",
        overrideEvent: "tp_page_activity",
      });
    }
    if (event.currentTarget.getAttribute("data-prime") === "free-trial") {
      event.preventDefault();
      const utmsFinal = {
        utm_source: "TOI",
        utm_medium: "timespoint_activity_list",
      };
      utmsFinal.utm_campaign = isMobilePlatform ? "Referral_Wap_TOI" : "Referral_Web_TOI";
      if (typeof closeModal === "function" && !isMobilePlatform) {
        closeModal();
      }
      // primePaymentflowStartEmitter({
      //   formType: "verifyMobileScreen",
      //   //callback: this.loadAdFreeNudge,
      //   utmsFinal,
      // });
    }
  };

  showMore = () => {
    this.setState({ showMoreList: true });
    // this.props.fireGAEvent(this.props.category, "view_all");
  };

  getDateStr = dateVal => `${dateVal.getDate()} ${this.props.getMonth(dateVal.getMonth(), true)}`;

  swipeDailyCheckInSlider = () => {
    const ele = document.querySelector(".checkin-parent");
    if (ele) {
      const liEle = ele.getElementsByTagName("li");
      if (liEle && liEle[0]) {
        const liWidth = liEle[0].offsetWidth;
        const swipeWidth = 6 * liWidth;
        ele.scrollLeft = swipeWidth;
      }
    }
  };

  mapDailyCheckins = dailyCheckIns => {
    const mappedDailyCheckins = Array.isArray(dailyCheckIns)
      ? dailyCheckIns.map(checkInData => ({
          ...checkInData,
          bonusClass: this.bonusClassMap[checkInData.status],
          dateString: checkInData.istoday ? "Today" : this.getDateStr(new Date(checkInData.timestamp)),
        }))
      : [];
    return mappedDailyCheckins;
  };

  gtmTrigger = () => {
    if (_isCSR()) {
      AnalyticsGA.event({
        category: "TimesPoints Page",
        action: "TP_Page",
        label: "Offer_Click",
        overrideEvent: "tp_page_activity",
      });
    }
  };

  render() {
    const { userRewards, dailyCheckInDetails, allActivity, activitiesMapList, translations, loggedIn } = this.props;

    const checkInDaysObj = this.props.getCheckinDaysLeft();
    const allActivityLoaded = allActivity && allActivity.length > 0;
    const mappedDailyCheckins = this.mapDailyCheckins(dailyCheckInDetails);

    /* Logic as per TML-2120 */
    let rewardsToShow = [];
    if (userRewards && Array.isArray(userRewards.exclusiveUserRewards)) {
      rewardsToShow = rewardsToShow.concat(userRewards.exclusiveUserRewards);
    }
    if (userRewards && Array.isArray(userRewards.topUserRewards)) {
      rewardsToShow = rewardsToShow.concat(userRewards.topUserRewards);
    }
    if (userRewards && Array.isArray(userRewards.awayUserRewards)) {
      rewardsToShow = rewardsToShow.concat(userRewards.awayUserRewards);
    }
    if (userRewards && Array.isArray(userRewards.allUserRewards)) {
      rewardsToShow = rewardsToShow.concat(userRewards.allUserRewards);
    }

    let checkInPointsText = "";
    if (!loggedIn) {
      checkInPointsText = "You must login to keep earning daily check-in points";
    } else {
      if (checkInDaysObj.checkInDays !== -1 && checkInDaysObj.bonusPoints !== -1) {
        // Split into cases where user is still checking in and recieving points, vs when he has recieved bonus points
        checkInPointsText =
          checkInDaysObj.checkInDays < campaignDays
            ? `You are ${campaignDays - checkInDaysObj.checkInDays} day${
                campaignDays - checkInDaysObj.checkInDays > 1 ? "s" : ""
              } away from ${checkInDaysObj.bonusPoints} bonus points`
            : `You have checked in for ${campaignDays} days and have recieved ${checkInDaysObj.bonusPoints} bonus points! `;
      }
    }

    return (
      <div className="tab-content how-to">
        <div className="tp-list-wrapper">
          <ul
            className={`tp-list ${allActivityLoaded ? "" : "tp-loading"} ${this.state.showMoreList ? "showall" : ""}`}
          >
            {allActivity &&
              allActivity.length > 0 &&
              allActivity.map(data => (
                <React.Fragment key={data.code}>
                  {this.getActivityHtml(activitiesMapList[data.code], data)}
                </React.Fragment>
              ))}
          </ul>
          {!this.state.showMoreList && (
            <div
              className="text-center"
              onClick={() => {
                this.showMore();
              }}
            >
              <button type="button" className="more-btn">
                more
              </button>
            </div>
          )}
        </div>
        <div className="tp-box2 withbtn">
          <h4>
            {`${translations && translations.daily_chekins ? translations.daily_chekins : ""} : `}
            <strong>{`${
              checkInDaysObj.checkInDays !== -1 ? checkInDaysObj.checkInDays : 0
            }/${campaignDays} ${translations && translations.completed}`}</strong>
          </h4>
        </div>
        <div className="checkin-list">
          <div
            className={`checkin-parent ${dailyCheckInDetails && dailyCheckInDetails.length > 0 ? "" : "tp-loading"}`}
          >
            {dailyCheckInDetails && dailyCheckInDetails.length > 0 && !this.props.isMobilePlatform && (
              <Slider
                type="timespointcheckin"
                size="5"
                movesize="4"
                startFromIndex={1}
                sliderData={[].concat(mappedDailyCheckins)}
                width={isMobilePlatform() ? "60" : "60"}
                sliderClass="tp_slider"
                islinkable
                margin={isMobilePlatform() ? "7" : "7"}
              />
            )}
            {dailyCheckInDetails && dailyCheckInDetails.length > 0 && this.props.isMobilePlatform && (
              <ul id="tpDailycheckin" className="daily-points">
                {dailyCheckInDetails.map(data => (
                  <li key={data.date} className={`checkin ${this.bonusClassMap[data.status]}`}>
                    <i className="tp-sprite icon" />
                    <h5>{data.bonusearned}</h5>
                    <span className="date">{data.istoday ? "Today" : this.getDateStr(new Date(data.timestamp))}</span>
                  </li>
                ))}
              </ul>
            )}
          </div>
        </div>

        <div className="days_away">{checkInPointsText}</div>

        <div className="tp-box2 flexbox justify-between align-center">
          <h4>{translations && translations.redeem_tp}</h4>
          <a
            href="https://www.timespoints.com/products?utm_source=TOI&utm_medium=timespoint&utm_campaign=daily_checkin"
            className="view-all-btn"
            target="_blank"
            rel="noopener nofollow noreferrer"
            // onClick={() => this.props.fireGAEvent(this.props.category, "view_all")}
          >
            VIEW ALL
          </a>
        </div>
        <div className="gift-cards">
          <ul
            className={
              userRewards.exclusiveUserRewards && userRewards.exclusiveUserRewards.length > 0 ? "" : "tp-loading"
            }
          >
            {rewardsToShow &&
              rewardsToShow.length > 0 &&
              rewardsToShow.slice(0, 4).map((data, index) => (
                <li key={index}>
                  <a href={data.targetUrl} target="_blank" onClick={this.gtmTrigger}>
                    <span>
                      {data.awaypoints === 0
                        ? "Click To Redeem this offer"
                        : `You are ${data.awaypoints} Point(s) away to redeem`}
                    </span>
                    <div className="details flexbox justify-between">
                      <div className="partner-logo">
                        <img src={data.imageurl} alt="" />
                      </div>
                      <div className="card-info">
                        <p>
                          <span className="text_ellipsis" title={data.title}>
                            {data.title}
                          </span>
                        </p>
                        <h3>
                          {`${data.point}`} <i className="icon tp-sprite" />
                        </h3>
                      </div>
                    </div>
                  </a>
                </li>
              ))}
          </ul>
        </div>
      </div>
    );
  }
}

HowToEarn.propTypes = {
  userRewards: PropTypes.shape({}),
  getCheckinDaysLeft: PropTypes.func.isRequired,
  category: PropTypes.string.isRequired,
  isMobilePlatform: PropTypes.bool,
  closeModal: PropTypes.func.isRequired,
  primeFlow: PropTypes.bool.isRequired,
};

HowToEarn.defaultProps = {
  isMobilePlatform: false,
  userRewards: {},
};

export default HowToEarn;
