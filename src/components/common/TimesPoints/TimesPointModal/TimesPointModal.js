/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-expressions */
/* eslint-disable func-names */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
//FIXME: Not needed
// import _get from "lodash.get";

import { _getCookie, isMobilePlatform, _getStaticConfig } from "../../../../utils/util";
// TODO: Added this in common modal component ( seems like a good function )
// import { enableBodyScroll, disableBodyScroll } from "utils/common";

// TODO: Check and change this functionality
// import { loginEventEmitter } from "components/Login/Login";

// TODO: Correct / modify these  components as per need
// import HowToEarn from "./HowToEarn";

// import TimesPointNudge from "./TimesPointNudge";
import Modal from "../../../../modules/modal/modal";

import "./TimesPointModal.scss";

// Importing sdk actions for tp data
import {
  loadUserRewards,
  getUserProfile,
  loadDailyCheckInDetails,
  loadDailyActivity,
} from "../../../../actions/timespoints/sdkactions";

// Importing actions fetched from newspoint / feed
import { getFAQData } from "../../../../actions/timespoints/timespoints";
import { getCheckinDaysLeft } from "../timespoints.util";
// import { getTpConfig } from "../TimesPointConfig";

import TimesPointSections from "../TimesPointSections";

const siteConfig = _getStaticConfig();

// const PCODE = tpConfig.pcode;
const tpConfig = siteConfig.TimesPoint;
const SHOW_TP_MODAL = "show_tp_modal";

export const HOW_TO_EARN_TAB = "HOW_TO_EARN_TAB";
export const OFFERS_TAB = "OFFERS_TAB";
export const ACTIVITY_TAB = "ACTIVITY_TAB";
export const FAQS_TAB = "FAQS_TAB";

export function tpShowModalEmitter(divId) {
  try {
    const logoutEvent = new CustomEvent(SHOW_TP_MODAL, {
      detail: { elementId: divId },
    });
    window.dispatchEvent(logoutEvent);
  } catch (e) {
    // console.log(e);
  }
}
class TimesPointModal extends React.Component {
  constructor() {
    super();
    this.state = {
      activeTab: "",
      showModal: false,
    };
  }

  componentDidMount() {
    // Check if mobile / web , and adds escape key event
    if (!isMobilePlatform()) {
      window.addEventListener(SHOW_TP_MODAL, this.showTpModal);
      document.body.addEventListener("keyup", this.closeModalOnEscapeKeyPress);
      setTimeout(() => {
        const elem = document.getElementById("times-point-modal");
        if (elem) {
          elem.addEventListener("click", this.closeTPModalOnOutsideClick);
        }
      }, 0);
    } else {
      this.showTpModal();
    }
    document.body.addEventListener("keyup", this.closeModalOnEscapeKeyPress);
    setTimeout(() => {
      const elem = document.getElementById("times-point-modal");
      if (elem) {
        elem.addEventListener("click", this.closeTPModalOnOutsideClick);
      }
    }, 0);
  }

  componentWillUnmount() {
    //FIXME: Change this to isMobilePlatform or remove if not needed
    if (!isMobilePlatform()) {
      window.removeEventListener(SHOW_TP_MODAL, this.showTpModal);
    }
  }

  closeTPModalOnOutsideClick = e => {
    if (e.srcElement && e.srcElement.id === "times-point-modal") {
      this.closeModal();
    }
  };

  closeModalOnEscapeKeyPress = e => {
    if (this.state.showModal) {
      const keyCode = e.keyCode || e.which;
      if (keyCode === 27) {
        this.closeModal();
      }
    }
  };

  setTabActive = tab => {
    this.setState({ activeTab: tab });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  showTpModal = e => {
    console.log(e);
    if (!this.state.showModal) {
      this.setState({ showModal: true });
    }

    const tabMap = {
      HOW_TO_EARN_TAB: "howtoearn",
      ACTIVITY_TAB: "myactivity",
      OFFERS_TAB: "offers",
      FAQS_TAB: "faqs",
    };

    if (e && e.detail && e.detail.elementId && e.detail.elementId in tabMap) {
      this.setTabActive(tabMap[e.detail.elementId]);
    }
  };

  render() {
    const { wrapperClass, translations } = this.props;
    const { activeTab } = this.state;

    return (
      <React.Fragment>
        <Modal showModal={this.state.showModal} closeModal={this.closeModal} wrapperClass={wrapperClass}>
          <div id="times-point-modal" className={`${"timespoint-wrapper"} ${this.state.showModal ? "active" : ""}`}>
            <div className="times-point-modal">
              {isMobilePlatform() && <div className="back-to-site">{translations && translations.text_timespoint}</div>}
              {!isMobilePlatform() && (
                <React.Fragment>
                  <button className="close-btn" type="button" onClick={() => this.closeModal()}>
                    +
                  </button>
                  <h2>
                    <i className="tp-sprite icon" />
                    {translations && translations.text_timespoint}
                  </h2>
                </React.Fragment>
              )}
              <TimesPointSections activeTab={activeTab} />
            </div>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

TimesPointModal.propTypes = {
  userRewards: PropTypes.shape({}),
  dailyCheckInDetails: PropTypes.arrayOf(PropTypes.shape({})),
  allActivity: PropTypes.arrayOf(PropTypes.shape({})),
  userProfile: PropTypes.shape({}),
  userData: PropTypes.shape({}),
  activitiesMapList: PropTypes.shape({}),
  dailyActivity: PropTypes.shape({}),
};

TimesPointModal.defaultProps = {
  currentCountry: "",
  userRewards: {},
  dailyCheckInDetails: [],
  userProfile: {},
  userData: {},
  faq: "",
  activitiesMapList: {},
  allActivity: [],
  dailyActivity: {},
  isWapView: false,
  disablePrimeFlow: false,
};

const mapStateToProps = state => ({
  // currentCountry: state.geo.currentCountry,
  userRewards: state.timespoints.userRewards,
  userProfile: state.timespoints.userProfile,
  faq: state.timespoints.faq,
  authentication: state.authentication,
  dailyCheckInDetails: state.timespoints.dailyCheckInDetails,
  allActivity: state.timespoints.allActivity,
  activitiesMapList: state.timespoints.activitiesMapList,
  dailyActivity: state.timespoints.dailyActivity,
  translations: state.timespoints.translations,
});

const mapDispatchToProps = dispatch => ({
  loadUserRewards: () => {
    dispatch(loadUserRewards());
  },
  getFAQData: () => {
    dispatch(getFAQData());
  },
  getUserProfile: () => {
    dispatch(getUserProfile());
  },
  loadDailyCheckInDetails: () => {
    dispatch(loadDailyCheckInDetails());
  },
  //  FIXME: Added to central component, check if needed
  // getAllActivities: isWapView => {
  //   dispatch(getAllActivities(isWapView));
  // },
  loadDailyActivity: () => {
    dispatch(loadDailyActivity());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TimesPointModal);
