import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getCheckinDaysLeft, getCombinedOffers, getMonth } from "./timespoints.util";
import {
  loadDailyActivity,
  loadDailyCheckInDetails,
  getUserProfile,
  loadUserRewards,
} from "../../../actions/timespoints/sdkactions";
import { getFAQData } from "../../../actions/timespoints/timespoints";
import { isMobilePlatform, _getStaticConfig, _isCSR } from "../../../utils/util";
import { openLoginPopup } from "../../common/ssoLogin";
import HowToEarn from "./TimesPointModal/HowToEarn";
import UserActivity from "./TimesPointModal/UserActivity";
import FAQs from "./TimesPointModal/FAQs";
import Offers from "./TimesPointModal/Offers";
import "./TimesPointModal/TimesPointModal.scss";
import { AnalyticsGA } from "../../lib/analytics";
import SvgIcon from "../SvgIcon";

const siteConfig = _getStaticConfig();

class TimesPointSections extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "howtoearn",
    };

    // For ga
    this.tabIdToCategoryMap = {
      tab1: "Earn_TimesPoint",
      tab2: "TimesPoints_Activity",
      tab3: "TimesPoints_FAQ",
      tab4: "TimesPoints_Offers",
    };
  }

  componentDidMount() {
    if (_isCSR()) {
      AnalyticsGA.event({
        category: "TimesPoints Page",
        action: "TP_Page",
        label: "TP_PageOpen",
        overrideEvent: "tp_page_activity",
      });
    }

    // To set and parse tabs from query string
    if (isMobilePlatform()) {
      if (this.props.location.query) {
        if (this.props.location.query.section) {
          this.setTabActive(this.props.location.query.section);
        } else {
          this.setTabActive("howtoearn");
        }
      }
    } else {
      if (!isMobilePlatform() && this.props.activeTab) {
        this.setTabActive(this.props.activeTab);
      }
    }
    // Load data for the sections
    this.loadTpData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.authentication.loggedIn !== this.props.authentication.loggedIn) {
      this.loadTpData();
    }
  }

  loadTpData = () => {
    this.props.loadUserRewards();
    this.props.getFAQData();
    this.props.getUserProfile();
    this.props.loadDailyCheckInDetails();
    this.props.loadDailyActivity();
  };

  setTabActive = tab => {
    // To set and parse tabs from query string
    if (isMobilePlatform()) {
      try {
        //To maintain previous url
        window.history.replaceState({}, "", `${window.location.pathname}?section=${tab}`);
      } catch (ex) {
        console.log("Exception in history: ", ex);
      }
    }

    this.setState({ activeTab: tab });
  };

  getCheckinDaysLeftFromProps = () => {
    const { dailyCheckInDetails, allActivity } = this.props;

    return getCheckinDaysLeft(dailyCheckInDetails, allActivity);
  };

  //FIXME: Can be moved to utilities as invoked from multiple places (Nudge and here)
  redeemPointsHandler = () => {
    if (this.props.authentication && this.props.authentication.loggedIn) {
      if (_isCSR()) {
        AnalyticsGA.event({
          category: "TimesPoints Page",
          action: "TP_Page",
          label: "Redeem_button",
          overrideEvent: "tp_page_activity",
        });
      }
      this.redirectToTpPage();
    } else {
      if (_isCSR()) {
        AnalyticsGA.event({
          category: "TimesPoints Page",
          action: "TP_Page",
          label: "Login_button",
          overrideEvent: "tp_page_activity",
        });
      }
      // this.redirectToTpPage();
      openLoginPopup("", "tp_page");
    }
  };

  goToLastPage = () => {
    this.props.router && typeof this.props.router.goBack == "function" && this.props.router.goBack();
  };

  redirectToTpPage = () => {
    window.open(
      `https://www.timespoints.com/products?utm_source=${siteConfig.channelCode}&utm_medium=timespoint&utm_campaign=daily_checkin`,
      "_blank",
    );
  };

  renderTPSections = () => {
    const { userProfile, authentication, translations } = this.props;
    const { userData, loggedIn } = authentication;
    return (
      <div className="inner-container">
        <div className="box tab-container">
          <input
            type="radio"
            id="howtoearn"
            className="tab"
            checked={this.state.activeTab === "howtoearn"}
            onChange={() => this.setTabActive("howtoearn")}
          />
          {/* All locales are in english, therefore dont need to keep in config */}
          <label htmlFor="howtoearn">{"How To Earn"}</label>
          <input
            type="radio"
            id="myactivity"
            className="tab"
            checked={this.state.activeTab === "myactivity"}
            onChange={() => this.setTabActive("myactivity")}
          />
          <label htmlFor="myactivity">{"My Activity"}</label>
          <input
            type="radio"
            id="offers"
            className="tab"
            checked={this.state.activeTab === "offers"}
            onChange={() => this.setTabActive("offers")}
          />
          <label htmlFor="offers">{"Offers"}</label>
          <input
            type="radio"
            id="faqs"
            className="tab"
            checked={this.state.activeTab === "faqs"}
            onChange={() => this.setTabActive("faqs")}
          />
          <label htmlFor="faqs">{"FAQs"}</label>
          <HowToEarn
            userRewards={this.props.userRewards}
            dailyCheckInDetails={this.props.dailyCheckInDetails}
            allActivity={this.props.allActivity}
            loggedIn={loggedIn}
            activitiesMapList={this.props.activitiesMapList}
            translations={translations}
            getMonth={getMonth}
            getCheckinDaysLeft={this.getCheckinDaysLeftFromProps}
            category={this.tabIdToCategoryMap.tab1}
            fireGAEvent={this.fireGAEvent}
            isMobilePlatform={isMobilePlatform()}
            closeModal={this.closeModal}
            disablePrimeFlow={this.props.disablePrimeFlow}
          />
          <UserActivity
            userProfile={this.props.userProfile}
            userData={userData}
            dailyActivity={this.props.dailyActivity}
            activitiesMapList={this.props.activitiesMapList}
            getMonth={getMonth}
            redeemPointsHandler={this.redeemPointsHandler}
            translations={translations}
          />
          <FAQs data={this.props.faq} />
          <Offers data={getCombinedOffers(this.props.userRewards)} />
        </div>
        {userData &&
          userData &&
          userProfile &&
          typeof userProfile.redeemablePoints === "number" &&
          userProfile.redeemablePoints > 0 && (
            <div className="redeem-points-sticky">
              <button className="tp-btn" onClick={() => this.redeemPointsHandler(true)}>
                {`REDEEM ${userProfile.redeemablePoints} POINTS`}
              </button>
            </div>
          )}
        {!userData && (
          <div className="redeem-points-sticky">
            <button className="tp-btn" onClick={() => this.redeemPointsHandler(true)}>
              {`LOGIN AND REDEEM`}
            </button>
          </div>
        )}
      </div>
    );
  };

  render() {
    const { translations } = this.props;
    if (!isMobilePlatform()) {
      return this.renderTPSections();
    }

    // Wrap with appropriate classes in case of mobile
    return (
      <div id="times-point-modal" className="tp-mobile">
        <div className="times-point-modal">
          <div className="back-to-site">
            <span className="back_icon" onClick={this.goToLastPage}>
              <SvgIcon name="back" />
            </span>
            {translations && translations.text_timespoint}
          </div>
          {/* start, a component by Abhinav */}
          {this.renderTPSections()}
          {/* end, a component by Abhinav */}
        </div>
      </div>
    );
  }
}

TimesPointSections.propTypes = {
  loadUserRewards: PropTypes.func.isRequired,
  getFAQData: PropTypes.func.isRequired,
  getUserProfile: PropTypes.func.isRequired,
  loadDailyCheckInDetails: PropTypes.func.isRequired,
  // getAllActivities: PropTypes.func.isRequired,
  loadDailyActivity: PropTypes.func.isRequired,
  userRewards: PropTypes.arrayOf(PropTypes.shape({})),
  dailyCheckInDetails: PropTypes.arrayOf(PropTypes.shape({})),
  allActivity: PropTypes.arrayOf(PropTypes.shape({})),
  userProfile: PropTypes.shape({}),
  userData: PropTypes.shape({}),
  activitiesMapList: PropTypes.shape({}),
  dailyActivity: PropTypes.shape({}),
  faq: PropTypes.string,
  disablePrimeFlow: PropTypes.bool,
  currentCountry: PropTypes.string,
};

TimesPointSections.defaultProps = {
  currentCountry: "",
  userRewards: [],
  dailyCheckInDetails: [],
  userProfile: {},
  userData: {},
  faq: "",
  activitiesMapList: {},
  allActivity: [],
  dailyActivity: {},
  disablePrimeFlow: false,
};

const mapStateToProps = state => ({
  // currentCountry: state.geo.currentCountry,
  userRewards: state.timespoints.userRewards,
  userProfile: state.timespoints.userProfile,
  faq: state.timespoints.faq,
  authentication: state.authentication,
  dailyCheckInDetails: state.timespoints.dailyCheckInDetails,
  allActivity: state.timespoints.allActivity,
  activitiesMapList: state.timespoints.activitiesMapList,
  dailyActivity: state.timespoints.dailyActivity,
  translations: state.timespoints.translations,
});

const mapDispatchToProps = dispatch => ({
  loadUserRewards: () => {
    dispatch(loadUserRewards());
  },
  getFAQData: () => {
    dispatch(getFAQData());
  },
  getUserProfile: () => {
    dispatch(getUserProfile());
  },
  loadDailyCheckInDetails: () => {
    dispatch(loadDailyCheckInDetails());
  },
  loadDailyActivity: () => {
    dispatch(loadDailyActivity());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TimesPointSections);
