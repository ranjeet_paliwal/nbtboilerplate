// // export const campaingHistoryNameWEB = "campaign233904"; // __PROD__ ? "campaign7514741"
// // export const campaingHistoryNameWAP = "campaign8133461"; // __PROD__ ? "campaign4261891" :
// // const siteConfig = _getStaticConfig();

// const tpConfig = {
//   //** Moved to common config */
//   channels: ["tml"],
//   // status: true,
//   // pcode: "channel8951747",
//   // scode: "channel8951747",
//   // stg: {
//   //   pcode: "channel1130049",
//   //   scode: "channel1130049",
//   //   campaingHistoryName: {
//   //     desktop: "campaign1650529",
//   //     mobile: "campaign1741855",
//   //   },
//   //   activities: {
//   //     DAILY_CHECKINS: "act1000165",
//   //     DAILY_CHECKINS_MOBILE: "act1448351",
//   //     READ_ARTICLE: "read",
//   //     WATCH_VIDEOS: "watch_video",
//   //     SH_FB: "sh_fb",
//   //     SH_TW: "sh_tw",
//   //     LOGIN: "act1341701",
//   //     MOVIE_RATE: "act1594658",
//   //     MOVIE_REVIEW: "act1643595",
//   //     VIEW_PHOTO: "view_photo",
//   //     LIVE_BLOG: "act1798195",
//   //     SUB_NL: "act1849353",
//   //     REGISTER: "act3473944",
//   //     COMMENT: "cmnt",
//   //     SUB_NOTIFICATION: "act3529439",
//   //     CHECKIN_BONUS_DESKTOP: "act3921411",
//   //     CHECKIN_BONUS_MOBILE: "act3944615",
//   //     WHATSAPP_SHARE: "act3814272",
//   //   },
//   // },

//   // TP_FAQ_MSID: "76003641",
//   //** Moved to common config */
//   // staging activity code
//   // activities: {
//   //   DAILY_CHECKINS: "act1000165",
//   //   DAILY_CHECKINS_MOBILE: "act1448351",
//   //   READ_ARTICLE: "read",
//   //   WATCH_VIDEOS: "watch_video",
//   //   SH_FB: "sh_fb",
//   //   SH_TW: "sh_tw",
//   //   LOGIN: "act1341701",
//   //   MOVIE_RATE: "act1594658",
//   //   MOVIE_REVIEW: "act1643595",
//   //   VIEW_PHOTO: "view_photo",
//   //   LIVE_BLOG: "act1798195",
//   //   SUB_NL: "act1849353",
//   //   REGISTER: "act3473944",
//   //   COMMENT: "cmnt",
//   //   SUB_NOTIFICATION: "act3529439",
//   //   CHECKIN_BONUS_DESKTOP: "act3921411",
//   //   CHECKIN_BONUS_MOBILE: "act3944615",
//   //   WHATSAPP_SHARE: "act3814272",
//   // },

//   //** Moved to common config */
//   // Production activity codes
//   // activities: {
//   //   DAILY_CHECKINS: "act3432947",
//   //   DAILY_CHECKINS_MOBILE: "act3682055",
//   //   READ_ARTICLE: "act4757750",
//   //   WATCH_VIDEOS: "watch_video",
//   //   SH_FB: "sh_fb",
//   //   SH_TW: "sh_tw",
//   //   LOGIN: "act3460671",
//   //   MOVIE_RATE: "act2995299",
//   //   MOVIE_REVIEW: "act2927403",
//   //   VIEW_PHOTO: "view_photo",
//   //   LIVE_BLOG: "act3503227",
//   //   SUB_NL: "act3528954",
//   //   REGISTER: "act3559146",
//   //   COMMENT: "cmnt",
//   //   SUB_NOTIFICATION: "act7176795",
//   //   CHECKIN_BONUS_DESKTOP: "act3921411",
//   //   CHECKIN_BONUS_MOBILE: "act3944615",
//   //   WHATSAPP_SHARE: "act5434425",
//   // },
//   //** Moved to common config */

//   NP_DOMAIN: {
//     production: "https://npcoins.indiatimes.com",
//     // development: "https://npcoins.indiatimes.com",
//     // stg1: "https://npcoins.indiatimes.com",
//     // stg2: "https://npcoins.indiatimes.com",
//     development: "https://nprelease.indiatimes.com",
//     stg1: "https://nprelease.indiatimes.com",
//     stg2: "https://nprelease.indiatimes.com",
//   },

//   TP_DOMAIN: {
//     production: "https://tpapi.timespoints.com",
//     // development: "https://tpapi.timespoints.com",
//     // stg1: "https://tpapi.timespoints.com",
//     // stg2: "https://tpapi.timespoints.com",
//     development: "https://test.timespoints.com/tpapi",
//     stg1: "https://test.timespoints.com/tpapi",
//     stg2: "https://test.timespoints.com/tpapi",
//   },

//   SDK_DOMAIN: {
//     production: "https://image.timespoints.iimg.in",
//     // development: "https://image.timespoints.iimg.in",
//     // stg1: "https://image.timespoints.iimg.in",
//     // stg2: "https://image.timespoints.iimg.in",
//     development: "https://test-img.timespoints.com",
//     stg1: "https://test-img.timespoints.com",
//     stg2: "https://test-img.timespoints.com",
//   },

//   tpwidgetJS: {
//     production: "/tpwidgets/dist/js/tpwidget.js",
//     // development: "/tpwidgets/dist/js/tpwidget.js",
//     // stg1: "/tpwidgets/dist/js/tpwidget.js",
//     // stg2: "/tpwidgets/dist/js/tpwidget.js",
//     development: "/static/tpwidgets/static/dist/js/tpwidget.js",
//     stg1: "/static/tpwidgets/static/dist/js/tpwidget.js",
//     stg2: "/static/tpwidgets/static/dist/js/tpwidget.js",
//   },

//   //** Moved to common config */
//   // campaingHistoryName: {
//   //   desktop: "campaign4654430",
//   //   mobile: "campaign4848729",
//   // },

//   // campaignDays: 5,
//   //** Moved to common config */
//   tpPage: "/timespoints.cms",
//   tpRedeemLink: "https://www.timespoints.com/products",
//   //checkInDaysDesktopActivity: "act3921411",
//   // checkInDaysMobileActivity: "act3944615",
// };

// //  Merge common config and site specific config, return
// export const getTpConfig = () => ({ ...tpConfig, ...siteConfig.Timespoint });

// export default tpConfig;
