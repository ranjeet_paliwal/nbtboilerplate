import React from "react";
// import PropTypes from 'prop-types';
import { connect } from "react-redux";

import {
  loadLoginTimesPoints,
  setInititializeTPFlag,
  getAllActivities,
} from "../../../actions/timespoints/timespoints";
import { isMobilePlatform, _getStaticConfig, isProdEnv } from "../../../utils/util";
import {
  getEmail,
  getSsoid,
  preLoginActivity,
  postLoginActions,
  fireActivity,
  resetDailyCheckInEntries,
} from "./timespoints.util";

import { tpShowModalEmitter } from "./TimesPointModal/TimesPointModal";
import { AnalyticsGA } from "../../lib/analytics/index";

const siteConfig = _getStaticConfig();

// import tpConfig from "./TimesPointConfig";
const tpConfig = siteConfig.TimesPoint;
const PCODE = isProdEnv() ? tpConfig.pcode : tpConfig.stg.pcode;

const pfm = isMobilePlatform() ? "msite" : "web";
const DEV_ENV = process.env.DEV_ENV;

// c1f2ab70c2a8406c96adbdd65aa54a82

class TimesPoints extends React.Component {
  static defaultProps = {
    userData: undefined,
    userPointsData: undefined,
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.sdkLoaded = false;
    // Subscribe notification seems to be hit twice, this is a workaround to prevent it from being hit again
    this.subscribedToNotifications = false;
    // this.isLoggedIn = false;
  }

  browserNotificationCallback = data => {
    console.log("browserNotificationCallback", data);

    if (
      data &&
      data.detail &&
      (data.detail.statuscode == 1 || data.detail.statuscode == 9) &&
      !this.subscribedToNotifications
    ) {
      this.subscribedToNotifications = true;
      fireActivity("SUB_NOTIFICATION", "pushnotification");
    }
  };

  // userLogoutCallback = data => {
  //   // alert("LogOut!!");
  //   // Logout Case
  //   //  this.isLoggedIn = false;

  //   if (window.TPWidget) {
  //     window.TPWidget.logoutAction();
  //   }

  //   // reinitialize with blank ticketId for anonymous case

  //   window.tpsdk("init", {
  //     platform: pfm,
  //     channelCode: PCODE,
  //     ticketId: "",
  //   });

  //   // ToDO : make data Blank getLoginTimesPointData
  // };

  // userLoginCallback = data => {
  //   // console.log("sss", this);
  //   const userInfo = data.detail;
  //   if (userInfo && userInfo.uid) {
  //     console.log("userInfo", userInfo);
  //     const ssoId = userInfo.uid;
  //     const ticketId = userInfo.ticketId;
  //     this.isLoggedIn = true;
  //     this.props.getLoginTimesPointData(ssoId);
  //     postLoginActions();
  //     resetDailyCheckInEntries();

  //     if (window.tpsdk) {
  //       window.tpsdk("init", {
  //         platform: pfm,
  //         channelCode: PCODE,
  //         ticketId: ticketId,
  //       });
  //     }
  //   }

  // window.tpsdk("init", {
  //   platform: "web",
  //   channelCode: "channel1130049",
  //   ticketId: "rSL7540IYY8xhisIZBQMn9t2nf5ZsONTnxdJcEYjiuzjUZ5aERT9bFw2n-62t-8N",
  // });
  // };

  componentDidMount() {
    const { authentication, pageType, router } = this.props;
    const msid = router && router.params && router.params.msid;
    const ticketId = (authentication && authentication.userData && authentication.userData.ticketId) || "";
    this.loadSDK();
    //  this.props.getLoginTimesPointData();
    this.setConfigObj();
    // document.addEventListener("user.status", this.userLoginCallback, false);
    //document.addEventListener("userLogout", this.userLogoutCallback, false);
    if (window.tpsdk) {
      window.tpsdk("init", {
        platform: pfm,
        channelCode: PCODE,
        ticketId: ticketId,
      });
    }

    document.body.addEventListener("tpwidget-ready", () => {
      window.loaded = "tploaded";
      this.initialiseTimesPoint(pageType, "all", msid);
    });

    this.props.getAllActivities(isMobilePlatform());
    const dailyChekin = isMobilePlatform() ? "DAILY_CHECKINS_MOBILE" : "DAILY_CHECKINS";
    fireActivity(dailyChekin, Math.round(new Date().getTime()));

    document.addEventListener("browserNotification", this.browserNotificationCallback);
  }

  componentDidUpdate(prevProps) {
    const _this = this;
    const { router } = _this.props;
    const { authentication, loc, pageType } = this.props;

    // console.log("prevProps", prevProps, authentication);
    const msid = router && router.params && router.params.msid;
    const isloggedIn = authentication && authentication.loggedIn;
    const userData = authentication && authentication.userData;
    const userAction = authentication && authentication.userAction;
    const ticketId = userData && userData.ticketId;
    if (loc && prevProps.loc && prevProps.loc.pathname != loc.pathname) {
      setTimeout(() => {
        const elmId =
          _this.TPconfig &&
          _this.TPconfig.widgettypes &&
          _this.TPconfig.widgettypes[pageType] &&
          _this.TPconfig.widgettypes[pageType].ele;
        if (document.querySelector(`#${elmId}-${msid}`)) {
          this.initialiseTimesPoint(pageType, "", msid);
        }
      }, 3000);

      if (window.tpsdk) {
        window.tpsdk("init", {
          platform: pfm,
          channelCode: PCODE,
          ticketId: ticketId,
        });
      }
    }

    if (isloggedIn) {
      const ssoId = userData.uid;

      if (ssoId) {
        this.props.getLoginTimesPointData(ssoId);
      }

      if (prevProps.authentication && prevProps.authentication.loggedIn !== isloggedIn) {
        postLoginActions(ticketId);
        this.initialiseTimesPoint(pageType, "all", msid);
        //  resetDailyCheckInEntries(); // we can enable this if will be required. commented because of multiple daily checkin calls
      }

      if (window.tpsdk) {
        window.tpsdk("init", {
          platform: pfm,
          channelCode: PCODE,
          ticketId: ticketId,
        });
      }
      if (
        userAction == "login" &&
        prevProps &&
        prevProps.authentication &&
        prevProps.authentication.userAction != userAction
      ) {
        const timestamp = new Date().getTime();
        fireActivity("LOGIN", `login_${timestamp}`);
        this.initialiseTimesPoint(pageType, "all", msid);
      }
    } else if (userAction == "logout") {
      if (window.TPWidget) {
        window.TPWidget.logoutAction();
      }

      window.tpsdk("logout");
      window.tpsdk("init", {
        platform: pfm,
        channelCode: PCODE,
        ticketId: "",
      });
    }
  }

  /* componentDidUpdate(prevProps) {
    if (
      this.state.sdkLoaded &&
      ((!(prevProps.userData && prevProps.userData.isLoggedIn) &&
        this.props.userData &&
        // eslint-disable-next-line react/prop-types
        this.props.userData.isLoggedIn) ||
        (this.props.userData.isLoggedIn && !this.isLoggedIn))
    ) {
      this.props.getLoginTimesPointData(getSsoid());
      // this.initialiseTimesPoint();
      postLoginActions();
      this.isLoggedIn = true;
      //fireActivity("visit", Math.round(new Date().getTime()));
      // fireActivity("register", Math.round(new Date().getTime()));
    }
    if (
      this.state.sdkLoaded &&
      prevProps.userData &&
      prevProps.userData.isLoggedIn &&
      this.props.userData.isLoggedIn === false
    ) {
      if (window.TPWidget) window.TPWidget.logoutAction();
      this.isLoggedIn = false;
    }

    // Reset daily checkin if user is logged in
    if (
      this.state.sdkLoaded &&
      prevProps.userData &&
      prevProps.userData.isLoggedIn === false &&
      this.props.userData &&
      this.props.userData.isLoggedIn === true
    ) {
      resetDailyCheckInEntries();
      if (typeof window !== "undefined" && window.TimesGDPR && window.TimesGDPR.common.consentModule.gdprCallback) {
        window.TimesGDPR.common.consentModule.gdprCallback(dataObj => {
          if (!dataObj.isEUuser) {
            fireActivity("dailycheckin", Math.round(new Date().getTime()));
          }
        });
      }
    }
  } */

  setConfigObj() {
    this.TPconfig = {
      widgettypes: {
        head: { ele: "widget-head", widgetType: "tpwidget-one" },
        articleshow: { ele: "widget-two", widgetType: "tpwidget-two" },
        videoshow: { ele: "widget-two-vs", widgetType: "tpwidget-two" },
        photoshow: { ele: "widget-two-ps", widgetType: "tpwidget-two" },
      },
    };
  }

  loadSDK() {
    // const sdkPrefix = !isProdEnv() ? "/static" : "";
    const sdkPrefix = "/static";
    const sdkPath = `${tpConfig["SDK_DOMAIN"][DEV_ENV]}${sdkPrefix}/tpwidgetV2/static/dist/js/main.js`;
    window.tp_js = (function attachScript(d, myElement, id) {
      const fjs = d.getElementsByTagName(myElement)[0];
      const t = window.tp_js || {};
      if (d.getElementById(id)) return t;
      const js = d.createElement(myElement);
      js.id = id;
      js.src = sdkPath;
      js.onload = function() {
        //  _this.setState({ sdkLoaded: true });
      };
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function ready(f) {
        t._e.push(f);
      };

      return t;
    })(document, "script", "tp-wjs");
  }

  initialiseTimesPoint(pageType, type, msid) {
    const { authentication } = this.props;
    const ticketId = (authentication && authentication.userData && authentication.userData.ticketId) || "";
    // console.log("authenticationticketId", authentication);
    const _this = this;
    const widgettypes = [];

    const gaLabelMap = { articleshow: "AS_Crown_Click", photoshow: "PS_Crown_Click", videoshow: "VS_Crown_Click" };

    if (!isMobilePlatform()) {
      if (type == "all") {
        widgettypes.push({
          ele: this.TPconfig.widgettypes.head.ele,
          widgetType: this.TPconfig.widgettypes.head.widgetType,
          platform: pfm,
          channel: PCODE,
          callback() {
            AnalyticsGA.event({
              category: "TimesPoints Page",
              action: "TP_Page",
              label: "TP_HNav_Click",
              overrideEvent: "tp_page_activity",
            });
            tpShowModalEmitter(_this.TPconfig.widgettypes.head.ele);
          },
        });
      }
      if (this.TPconfig.widgettypes[pageType]) {
        const widgetInfo = this.TPconfig.widgettypes[pageType];
        widgettypes.push({
          ele: `${widgetInfo.ele}-${msid}`,
          widgetType: this.TPconfig.widgettypes[pageType].widgetType,
          platform: pfm,
          channel: PCODE,
          callback() {
            AnalyticsGA.event({
              category: "TimesPoints Page",
              action: "TP_Page",
              label: gaLabelMap[pageType],
              overrideEvent: "tp_page_activity",
            });
            tpShowModalEmitter(_this.TPconfig.widgettypes[pageType].ele);
          },
        });
      }
    }

    if (typeof window.TPWidget !== "undefined" && typeof window.TPWidget.init === "function") {
      if (getSsoid() && getEmail() && ticketId) {
        window.TPWidget.init({
          widgets: widgettypes,
          userLoginInfo: {
            ticketId: ticketId, // sso ticket id
          },
        });
      } else {
        // preLoginActivity("DAILY_CHECKINS"); //commnted this for TP2 implemntaion suggested by TP  Team
        window.TPWidget.init({
          widgets: widgettypes,
        });
      }

      this.props.setInititializeTPFlag();
    }
  }
  render() {
    const { userPointsData } = this.props;

    // TODO: Ask doubt Why
    if (userPointsData) {
      if (isMobilePlatform()) {
        return <div />;
      }
      return null;
    }
    return null;
  }
}

const mapStateToProps = state => ({
  // userPointsData: state && state.timesPoint && state.timesPoint.userData,
  // faq: state && state.timespoints.faq,
  // userData: get(state, "login.userData", {}),
  authentication: state.authentication,
});

const mapDispatchToProps = dispatch => ({
  getLoginTimesPointData: ssoId => {
    dispatch(loadLoginTimesPoints(ssoId));
  },
  setInititializeTPFlag: () => {
    dispatch(setInititializeTPFlag());
  },
  getAllActivities: isWapView => {
    dispatch(getAllActivities(isWapView));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TimesPoints);

/* _id: 1268178
uid: "607y8gs4xnmzwot6snz5uklh5"
twitter: "timestest01"
sso: "607y8gs4xnmzwot6snz5uklh5@indiatimes.com"
facebook: "3173382689358254"
GID: 45046904
F_N: "Ranjeet"
L_N: "Singh"
F_C: 20
FE_C: 46
FE_C_D: (5) [9053627, 6793941, 4679503, 23982599, 6278593]
DOB: 445199400000
NC_B_ES: -1
NC_B_ET: -1
NC_B_NGS: -1
GN_C: 9
NC_QNAZ: -1
NC_B_ST: -1
NC_B: -1
NC_B_itimes: -1
LN: 1062022292
LN_B_ET: 55741556
LN_B: 56590949
P_SI: "true"
LI_T_M: "true"
G: "M"
EMAIL: "ranjeet.paliwal@yahoo.com"
P_WS: "false"
I_U_A: "false"
I_I_L: "27715322"
p_d: "null"
P_R_A_E: "true"
P_WIF: "true"
U_G: ""
M_N: "9582003439"
P_IR: "true"
L_U_DT: "1588942227399"
S_UP: "2"
U_CD: "2013-01-24 10:36:06.0"
original: "https://mytimes.indiatimes.com/image/original/1/1268178"
P_TF: "true"
profile: "https://mytimes.indiatimes.com/image/profile/1/1268178"
W_ID: "2475742"
P_SIC: "true"
v_w: "true"
P_PV: "true"
P_PU: "true"
P_FM: "true"
U_RD: "2013-01-24 10:36:06.0"
CITY: "Unknown"
FL_N: "Ranjeet Singh"
tiny: "https://mytimes.indiatimes.com/image/tiny/1/1268178"
P_R_M_W: "true"
P_V_C: "true"
P_V_LS: "true"
P_V_E: "true"
F_P: {}
thumb: "https://mytimes.indiatimes.com/image/thumb/1/1268178" */

/* window.TPWidget.init({
  widgets: [
    {
      ele: "widget-two",
      widgetType: "tpwidget-two",
    },
    {
      ele: "widget-head",
      widgetType: "tpwidget-one",
    },
  ],

  userLoginInfo: TPData(userData),
}); */
