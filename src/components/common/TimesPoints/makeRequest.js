import axios from "axios";
import _merge from "lodash/merge";

function get(url) {
  return axios.get(url);
}

function post(url, request, config) {
  request = JSON.parse(request);
  const options = {
    method: "post",
    url: url,
    data: request,
  };
  _merge(options, config);
  return axios(options);
}

function postData(url, request, config) {
  const options = {
    method: "post",
    url: url,
    data: request,
  };
  _merge(options, config);
  return axios(options);
}

const makeRequest = {
  get,
  post,
  postData,
};

export default makeRequest;
