import makeRequest from "./makeRequest";

import { _setCookie, _getCookie, isMobilePlatform, isProdEnv, _getStaticConfig } from "../../../utils/util";

// import tpConfig from "./TimesPointConfig";

const siteConfig = _getStaticConfig();

const tpConfig = siteConfig.TimesPoint;
const campaignDays = tpConfig.campaignDays;

const PCODE = isProdEnv() ? tpConfig.pcode : tpConfig.stg.pcode;
const devEnv = process.env.DEV_ENV;
const SITE = process.env.SITE;

const ACTIVITY_MAXCAP_MAP = "allActivityConfig";
const ACTIVITY_ACCRUE_MAP = "achievedActConfig";

// const allActivities = process.env.NODE_ENV == "production" ? tpConfig.activities : tpConfig.stg.activities;
const allActivities = isProdEnv() ? tpConfig.activities : tpConfig.stg.activities;

export function SaveActivityToLocalStorage(acode, msid) {
  if (typeof window.localStorage === "undefined") {
    return;
  }

  try {
    let accrueObj = localStorage.getItem(ACTIVITY_ACCRUE_MAP);

    if (!accrueObj) {
      accrueObj = {};
    } else {
      accrueObj = JSON.parse(accrueObj);
    }
    if (accrueObj[acode]) {
      accrueObj[acode].push(msid);
    } else {
      accrueObj[acode] = [msid];
    }
    window.localStorage.setItem(ACTIVITY_ACCRUE_MAP, JSON.stringify(accrueObj));
  } catch (e) {
    // console.log(e);
  }
}
export function sendActivityRequest(request, cb) {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  makeRequest
    .post(
      `${tpConfig.NP_DOMAIN[devEnv]}/timespoint/submit/activity/${process.env.SITE}`,
      JSON.stringify(request),
      config,
    )
    .then(response => {
      // if (response.data.status === '"FAILURE"') {
      //   // console.log(data.message);
      // } else {
      //   // console.log(
      //   //   'TPActivity Post Login Activity URL -https://tpapi.timespoints.com/v1/activity/logact' +
      //   //     request,
      //   // );
      // }
      if (response && response.data && response.data.success == true) {
        SaveActivityToLocalStorage(request.aname, request.txnId);
        cb(response);
      } else {
        console.log("response submit activity", response);
      }
    })
    .catch(() => {
      //console.log(err);
    });
}

export function resetDailyCheckInEntries() {
  if (typeof window.localStorage !== "undefined") {
    let achievedActConfig = {};
    try {
      achievedActConfig = localStorage.getItem(ACTIVITY_ACCRUE_MAP);
      const dailyCheckInAName = (tpConfig && tpConfig.activities.DAILY_CHECKINS) || "";
      if (!achievedActConfig || !dailyCheckInAName) {
        return;
      }
      achievedActConfig = JSON.parse(achievedActConfig);
      if (
        achievedActConfig &&
        achievedActConfig[dailyCheckInAName] &&
        achievedActConfig[dailyCheckInAName].length > 0
      ) {
        achievedActConfig[dailyCheckInAName] = [];
        window.localStorage.setItem(ACTIVITY_ACCRUE_MAP, JSON.stringify(achievedActConfig));
      }
    } catch (e) {
      // console.log(e);
    }
  }
}

export function checkAccrualStatusActivity(msid, acode) {
  if (typeof window.localStorage === "undefined") {
    return true;
  }

  let activityMap = {};
  let achievedActConfig = {};
  let isValidActivity = false;
  try {
    activityMap = window.localStorage.getItem(ACTIVITY_MAXCAP_MAP);
    achievedActConfig = localStorage.getItem(ACTIVITY_ACCRUE_MAP);
    if (!activityMap) {
      return true;
    }

    activityMap = JSON.parse(activityMap);
    achievedActConfig = JSON.parse(achievedActConfig);
    if (!activityMap || activityMap[acode] == undefined) {
      isValidActivity = true;
    } else {
      let dateVal = achievedActConfig && achievedActConfig.date ? achievedActConfig.date : "";
      const currDate = new Date();
      const curDate = [currDate.getDate(), currDate.getMonth(), currDate.getFullYear()];
      const curDateStr = curDate.join("-");
      if (curDateStr !== dateVal) {
        dateVal = curDateStr;
        isValidActivity = true;
        achievedActConfig = {};
        achievedActConfig.date = dateVal;
        window.localStorage.setItem(ACTIVITY_ACCRUE_MAP, JSON.stringify(achievedActConfig));
      } else if (
        !achievedActConfig ||
        !achievedActConfig[acode] ||
        (achievedActConfig[acode].length < activityMap[acode] && achievedActConfig[acode].indexOf(msid) === -1)
      ) {
        isValidActivity = true;
      }
    }
  } catch (e) {
    isValidActivity = true;
    // console.log(e);
  }

  return isValidActivity;
}

/* Can be consolidated to one function with all user details */
export function getEmail() {
  const email = _getCookie("MSCSAuthDetails") ? _getCookie("MSCSAuthDetails").split("=")[1] : "";
  return email;
}

/* Can be consolidated to one function with all user details */
export function getSsoid() {
  const ssoid = _getCookie("ssoid");
  return ssoid;
}

/**
 * Get details of a user
 * @author Abhinav Mishra
 * @returns value {Object | null} of user data, null if not logged in.
 * @description Helper function to get userData after login, returns null if not logged in.
 */
export function getUserData() {
  // Email is in MSCSAuthDetails (with the s) seems to be same value in MSCSAuthDetail , picked from there
  // const email = _getCookie("MSCSAuthDetails") ? _getCookie("MSCSAuthDetails").split("=")[1] : "";
  let userData = null;
  if (typeof window !== "undefined") {
    const ssoid = _getCookie("ssoid");

    // To return data if user is logged in , else null
    if (ssoid) {
      // Contains name, DOB, GENDER ,CITY etc in MSCSAuthDetail (without the s)
      const userDetails = _getCookie("MSCSAuthDetail") ? _getCookie("MSCSAuthDetail").split("~") : "";
      if (Array.isArray(userDetails)) {
        userData = userDetails.reduce((acc, usInfo) => {
          const info = usInfo.split("=");
          acc[info[0].toLowerCase()] = info[1];
          return acc;
        }, {});
      }
      userData.ssoid = ssoid;
    }
  }
  return userData;
}

export function handleActivity(activity, txnId, userTypeId, cb) {
  const newTPconfig = { pcode: PCODE, scode: PCODE, uid: getSsoid() };
  const pfm = isMobilePlatform() ? "msite" : "web";
  const activities = isProdEnv() ? tpConfig.activities : tpConfig.stg.activities;
  const request = {
    pcode: `${newTPconfig.pcode}`,
    scode: `${newTPconfig.scode}`,
    aname: `${activities[activity] || ""}`,
    // aname: `${activity}`,
    txnId: `${txnId}`,
    apiVersion: `23`,
    clientId: PCODE,
    platform: `${pfm}`,
  };

  if (!request.aname) {
    return;
  }

  const innerFn = data => {
    // eslint-disable-next-line default-case

    switch (userTypeId) {
      case "deviceid":
        request.deviceId = data;
        break;
      case "ssoid":
        request.uid = data;
        break;
    }
    if (checkAccrualStatusActivity(txnId, allActivities[activity])) {
      sendActivityRequest(request, cb);
    }
  };

  if (userTypeId === "deviceid") {
    if (typeof window.tpsdk !== "undefined") {
      // window.tpsdk = function() {
      //   (window.tpsdk.q = window.tpsdk.q || []).push(arguments);
      // };
      // window.tpsdk.l = 1 * new Date();
      // window.tpsdk = window.tpsdk || {};

      // window.tpsdk("init", {
      //   platform: pfm,
      //   channelCode: PCODE,
      // });
      window.tpsdk("getDeviceId", innerFn);
    }
  } else {
    innerFn(`${getSsoid()}`);
  }
}
export function fireActivity(activity, txnId, cb) {
  if (tpConfig && tpConfig.channels.includes(SITE)) {
    if (typeof getSsoid() !== "string") {
      preLoginActivity(activity, txnId);
      handleActivity(activity, txnId, "deviceid", cb);
    } else {
      handleActivity(activity, txnId, "ssoid", cb);
    }
  }
}

export function shareHandlingForTimesPoints(type, msid) {
  if (typeof type === "string" && type.indexOf("facebook") >= 0) {
    fireActivity("SH_FB", `fb_${msid}`);
  }
  if (typeof type === "string" && type.indexOf("twitter") >= 0) {
    fireActivity("SH_TW", `tw_${msid}`);
  }
}

export function preLoginActivity(activity, txnId) {
  // if (window.TPWidget && checkAccrualStatusActivity(txnId, allActivities[activity])) {
  //   const acode = tpConfig.activities[activity];
  //   window.TPWidget.addPreLoginActivity(acode);
  // }
}

export function postLoginActions(ticketId) {
  const newTPconfig = {
    pcode: PCODE,
    scode: PCODE,
    uid: getSsoid(),
    email: getEmail(),
  };

  const mergeTpDataForLoggedInUser = deviceId => {
    if (!deviceId) {
      return;
    }
    const request = {
      uid: getSsoid(),
      pcode: PCODE,
      platform: "web",
      clientId: PCODE,
      deviceId,
    };

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    makeRequest.post(`${tpConfig.NP_DOMAIN[devEnv]}/timespoint/init/tml`, JSON.stringify(request), config);
  };

  if (typeof window.tpsdk === "function") {
    window.tpsdk("getDeviceId", mergeTpDataForLoggedInUser);
  }
  return (
    window.TPWidget &&
    window.TPWidget.PostLoginActions({
      ticketId: ticketId, // sso ticket id
    })
  );

  // return (
  //   window.TPWidget &&
  //   window.TPWidget.PostLoginActions({
  //     host: newTPconfig.pcode,
  //     channel: newTPconfig.scode,
  //     URL: escape(document.location.href),
  //     userId: newTPconfig.uid,
  //     oid: "",
  //     email: newTPconfig.email,
  //   })
  // );
}

export function valiDateLocalStorageConfigData() {
  if (typeof localStorage === "object") {
    const acheivedActConfig = JSON.parse(localStorage.getItem("acheivedActConfig"));
    if (acheivedActConfig instanceof Object) {
      const date = new Date();
      const configDate = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`;
      // eslint-disable-next-line radix
      if (acheivedActConfig.date !== configDate) {
        localStorage.removeItem("acheivedActConfig");
      }
    }
  }
}

/**
 * @author Abhinav Mishra
 * @param {index,shortVal} Number, boolean
 * @description Utility function to get Month value in string
 * @returns {String} Month value in string if exists, "" otherwise
 * */

export const getMonth = (index, shortVal) => {
  if (typeof index !== "number" || index > 11) {
    return "";
  }

  let monStr = "";
  const monArr = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  monStr = monArr[index];
  if (shortVal) {
    monStr = monStr.substring(0, 3);
  }

  return monStr;
};

/** Helper function to calculate checkin days left
 * @author Abhinav Mishra
 * @param {dailyCheckInDetails,allActivity} dailyCheckInDetails , allActivity object
 * @description Utility function to return checkin days left for activities
 * @returns {Object} Object containing checkin details
 * */
export const getCheckinDaysLeft = (dailyCheckInDetails, allActivity) => {
  // const { dailyCheckInDetails, allActivity } = this.props;
  const checkInObj = {
    checkInDays: -1,
    bonusPoints: -1,
  };
  let i = 7;
  let checkInDays = -1;
  let bonusPoints = -1;

  if (dailyCheckInDetails && dailyCheckInDetails.length > 0 && allActivity && allActivity.length > 0) {
    checkInDays = 0;
    for (; i >= 3; i -= 1) {
      if (dailyCheckInDetails[i].status !== 1 || (i !== 7 && dailyCheckInDetails[i].campaignAchieved)) {
        break;
      }

      checkInDays += 1;

      if (checkInDays === campaignDays) {
        break;
      }
    }

    i = 0;
    const activities = isProdEnv() ? tpConfig.activities : tpConfig.stg.activities;
    // FIXME: Change and check this hardcoded activities.
    const bonusActCode = isMobilePlatform() ? activities["CHECKIN_BONUS_MOBILE"] : activities["CHECKIN_BONUS_DESKTOP"];

    for (; i < allActivity.length; i += 1) {
      if (allActivity[i].code === bonusActCode) {
        bonusPoints = allActivity[i].assign_points;
        break;
      }
    }
  }

  checkInObj.checkInDays = checkInDays;
  checkInObj.bonusPoints = bonusPoints;
  return checkInObj;
};

/** Helper function to get offers list
 * @author Abhinav Mishra
 * @param {userRewards} userRewards , Object containing various offers
 * @description Utility function to calculate and prioritize offers
 * @returns {Array} Array containing offers According to priority
 * */

export const getCombinedOffers = (userRewards = {}) => {
  /* Recommended logic for UI -
   20 offers in the sequence: Exclusive(All) > Top > Away > All
    (Remaining to complete 20) */

  let { awayUserRewards, topUserRewards, allUserRewards, exclusiveUserRewards } = userRewards;

  // Check for undefined
  awayUserRewards = awayUserRewards || [];
  topUserRewards = topUserRewards || [];
  allUserRewards = allUserRewards || [];
  exclusiveUserRewards = exclusiveUserRewards || [];

  // Spread array in order to combine them according to priority
  const combinedOffers = [...exclusiveUserRewards, ...topUserRewards, ...awayUserRewards, ...allUserRewards];
  // console.log("combinedOffers", combinedOffers);
  // Creating offers Map to keep unique values only, and preserve order
  // Assuming productId and partnerId combined as unique key
  const offersMap = combinedOffers.reduce((acc, offer) => {
    acc.set(`${offer.productId}_${offer.partnerId}`, offer);
    return acc;
  }, new Map());

  // Convert back to array from iterable object
  return Array.from(offersMap.values());
};
