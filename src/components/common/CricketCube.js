import React, { Component } from "react";
import "./css/electionCube.scss";
import AnchorLink from "./AnchorLink";
import { _getStaticConfig, _isCSR, _deferredDeeplink, _sessionStorage, _getTeamLogo } from "../../utils/util";
import { subscribe } from "../../utils/pubsub";
const siteConfig = _getStaticConfig();
import { electionConfig } from "../../campaign/election/utils/config";
import Ads_module from "./../../components/lib/ads/index";
import { AnalyticsGA } from "./../../components/lib/analytics/index";
import { coustomizeFeedData } from "../../campaign/election/utils/util";
import { constants } from "../../campaign/election/utils/constants";
import ImageCard from "./../../components/common/ImageCard/ImageCard";
import "./css/MiniSchedule.scss";

const timeInterval = 6000;
const _electionConfig = electionConfig[process.env.SITE];

export const RotateCube = props => {
  if (_isCSR()) {
    let cube = document.querySelector(".flipbox-box.flipbox-horizontal");

    if (cube && !cube.getAttribute("isRotateAttached")) {
      // setInterval(() => {
      //     Ads_module.refreshAds(['special1']);
      // }, timeInterval * 4);
      // setInterval(() => {
      //     Ads_module.refreshAds(['special2']);
      // }, timeInterval * 5);

      Ads_module.render({});

      let currentVal = 0;
      setInterval(() => {
        currentVal -= 90;
        cube.style.transform = "rotateY(" + currentVal + "deg)";
      }, timeInterval);

      cube.setAttribute("isRotateAttached", true);
    }

    if (cube && props && !cube.getAttribute("isGAFired")) {
      CriketCubeGA(props);
      cube.setAttribute("isGAFired", true);
    }
  }
};
const getMatchID = miniScoreCard => {
  let id;

  if (miniScoreCard.Calendar) {
    miniScoreCard.Calendar.forEach(function(item) {
      if (item.live == 1) {
        id = item.matchid;
      }
    });
  }
  return id;
};

const CriketCubeGA = e => {
  let id;
  id = getMatchID(e);

  // Ads_module.refreshAds(['special1', 'special2', 'headAd1', 'headAd2', 'headAd3', 'headAd4']);
  // AnalyticsGA.pageview('/elections/lok-sabha-elections/result/69253509.cms?cube=result');
  AnalyticsGA.pageview("/scoreboard/matchid-" + id + ".cms");
};

// const teamScore = (team, item) => {
//     // let inning1 = item && item.inn_team_1 != null && item.inn_team_1 != undefined ? parseInt(item.inn_team_1) : null;
//     // let inning2 = item && item.inn_team_2 != null && item.inn_team_2 != undefined ? parseInt(item.inn_team_2) : null;
//     if (team == 'A' && inning1 == parseInt(item.teama_Id)) {
//         return teamScoreFill(1, item);
//     }
//     else if (team == 'B' && inning1 == parseInt(item.teamb_Id)) {
//         return teamScoreFill(1, item);
//     }

// }

const teamScoreFill = (teamId, item) => {
  if (item["team" + teamId + "_score"] && item["team" + teamId + "_score"] != "") {
    return (
      <React.Fragment>
        <span className="score">{item["team" + teamId + "_score"].split(" (")[0]}</span>
        <span className="over">{item["team" + teamId + "_score"].split(" (")[1].split(")")[0]}</span>
      </React.Fragment>
    );
  } else {
    return null;
  }
};

const getBatsmanDetail = (scoredetails, item) => {
  let batsmanDetail, status, tourname;
  if (scoredetails && scoredetails.Innings) {
    batsmanDetail = getPlayersOnCrease(scoredetails);
    status = geMatchStatus(scoredetails);
    tourname = scoredetails.Matchdetail.Series.Name
      ? scoredetails.Matchdetail.Series.Name
      : "ICC Cricket World Cup, 2019";

    return (
      <div className="dem">
        <AnchorLink
          data-type="scorecard"
          style={{ textDecoration: "none" }}
          hrefData={{
            override:
              siteConfig.mweburl +
              "/sports/cricket/live-score/" +
              item.teama_short.toLowerCase() +
              "-vs-" +
              item.teamb_short.toLowerCase() +
              "/" +
              item.matchdate_ist.replace(/\//g, "-") +
              "/scoreboard/matchid-" +
              item.matchfile +
              ".cms",
          }}
        >
          <div className="cube_player_details">
            <div className="series-title">{tourname}</div>
            {batsmanDetail ? (
              <React.Fragment>
                {batsmanDetail[0] && batsmanDetail[0].playerName ? (
                  <div className="row_player">
                    {batsmanDetail[0].playerName}
                    <span className="strike">{batsmanDetail[0].isOnStrike ? "*" : null}</span>
                    <span className="runs">
                      {batsmanDetail[0].Runs ? batsmanDetail[0].Runs : "-"}{" "}
                      {batsmanDetail[0].Balls ? "(" + batsmanDetail[0].Balls + ")" : "-"}
                    </span>
                    <span className="balls"></span>
                  </div>
                ) : null}
                {batsmanDetail[1] && batsmanDetail[1].playerName ? (
                  <div className="row_player">
                    {batsmanDetail[1].playerName}
                    <span className="strike">{batsmanDetail[1].isOnStrike ? "*" : null}</span>
                    <span className="runs">
                      {batsmanDetail[1].Runs ? batsmanDetail[1].Runs : "-"}
                      {batsmanDetail[1].Balls ? "(" + batsmanDetail[1].Balls + ")" : "-"}
                    </span>
                    <span className="balls"></span>
                  </div>
                ) : null}
              </React.Fragment>
            ) : null}
            <div className="match_final_status">
              <span className="text_ellipsis">{status}</span>
            </div>
          </div>
        </AnchorLink>
        <a
          href={_deferredDeeplink(
            "campaign",
            siteConfig.appdeeplink,
            null,
            siteConfig.mweburl + "/scoreboard/matchid-" + item.matchfile + ".cms?frmapp=yes",
          )}
          target="_blank"
          className="open-app-with-ad"
        >
          <button className="open-in-app">Open in App</button>
        </a>
      </div>
    );
  }
};

const getPlayersOnCrease = scoreDetails => {
  let inning;
  inning = scoreDetails.Innings.length > 1 ? scoreDetails.Innings[1] : scoreDetails.Innings[0];
  let batsmanArr = inning.Batsmen;
  let playerIdOnStrike, playerIdOnNonStrike, pStrikeInd, pNonstrikeInd;
  let playerNameOnStrike, playerNameOnNonStrike;
  let teamID, indx;
  let retrnObj = [];

  if (Object.keys(scoreDetails.Innings).length > 1) {
    teamID = scoreDetails.Innings[1]["Battingteam"];
  } else {
    teamID = scoreDetails.Innings[0]["Battingteam"];
  }
  // if (Object.keys(scoreDetails.Teams).length > 1) {
  //     teamID = scoreDetails.Teams[];
  // } else {
  //     teamID = Object.keys(scoreDetails.Teams)[0];
  // }

  batsmanArr.forEach(function(item, index) {
    if (item.Isbatting && item.Isbatting == true) {
      if (item.Isonstrike && item.Isonstrike == true) {
        playerIdOnStrike = item.Batsman;
        // pStrikeInd = index;
        playerNameOnStrike = getPlayerName(scoreDetails.Teams[teamID], playerIdOnStrike);

        retrnObj.push({
          playerName: playerNameOnStrike,
          isOnStrike: true,
          Runs: item.Runs,
          Balls: item.Balls,
        });
      } else {
        playerIdOnNonStrike = item.Batsman;
        // pNonstrikeInd = index;
        playerNameOnNonStrike = getPlayerName(scoreDetails.Teams[teamID], playerIdOnNonStrike);
        retrnObj.push({
          playerName: playerNameOnNonStrike,
          isOnStrike: false,
          Runs: item.Runs,
          Balls: item.Balls,
        });
      }
    }
  });

  return retrnObj;
};

const getPlayerName = (team, playerID) => {
  let players, playerName;
  players = team.Players;
  playerName = players[playerID]["Name_Full"];
  return playerName;
};

const geMatchStatus = scoredetail => {
  let matchDetail,
    retrnStatus = "";

  if (scoredetail && scoredetail.Matchdetail) {
    matchDetail = scoredetail.Matchdetail;
    retrnStatus = matchDetail.Status == "Match Ended" ? matchDetail.Result : matchDetail.Status;
  }
  return retrnStatus;
};

const scorecardWidget = (item, scoredetail) => {
  let status, tourname;
  if (scoredetail) {
    status = geMatchStatus(scoredetail);
    tourname = scoredetail.Matchdetail.Series.Name
      ? scoredetail.Matchdetail.Series.Name
      : "ICC Cricket World Cup, 2019";
  }

  return (
    <div className="dem">
      <AnchorLink
        data-type="scorecard"
        style={{ textDecoration: "none" }}
        hrefData={{
          override:
            siteConfig.mweburl +
            "/sports/cricket/live-score/" +
            item.teama_short.toLowerCase() +
            "-vs-" +
            item.teamb_short.toLowerCase() +
            "/" +
            item.matchdate_ist.replace(/\//g, "-") +
            "/scoreboard/matchid-" +
            item.matchfile +
            ".cms",
        }}
      >
        <div className="wdt_schedule floating_cube">
          <div className="match_date_venue">{tourname}</div>
          <div className="liveScore table">
            <div className="table">
              <div className="teama table_col">
                <b className="country">{item.teama_short}</b>
                <span className="teaminfo">
                  <ImageCard msid={_getTeamLogo(item.teama_Id, "square")} size="rectanglethumb" />
                  <span className="con_wrap">
                    {item.matchtype.toLowerCase() != "test" ? teamScoreFill("a", item) : null}
                  </span>
                </span>
              </div>
              <div className="versus table_col">
                <span>VS</span>
              </div>
              <div className="teamb table_col">
                <b className="country">{item.teamb_short}</b>
                <span className="teaminfo">
                  <ImageCard msid={_getTeamLogo(item.teamb_Id, "square")} size="rectanglethumb" />
                  <span className="con_wrap">
                    {item.matchtype.toLowerCase() != "test" ? teamScoreFill("b", item) : null}
                  </span>
                </span>
              </div>
            </div>
            <div className="match_final_status">
              <span className="text_ellipsis">{status}</span>
              {item.live == "1" && item.live == "2" && item.live == "3" && item.live == "4" ? (
                <span className="status_live">Live</span>
              ) : null}
            </div>
          </div>
        </div>
      </AnchorLink>
      <a
        href={_deferredDeeplink(
          "campaign",
          siteConfig.appdeeplink,
          null,
          siteConfig.mweburl + "/scoreboard/matchid-" + item.matchfile + ".cms?frmapp=yes",
        )}
        target="_blank"
        className="open-app-with-ad"
      >
        <button className="open-in-app">Open in App</button>
      </a>
    </div>
  );
};

export const CricketCube = props => {
  // let { election } = props;
  let { scorecard, scoredetail } = props;
  // let {scorecard} = props;
  // let {scoredetail} = props;
  let pos, schedule;

  scoredetail = scoredetail.data;

  const hideCube = () => {
    if (document.querySelector(".election_cube")) {
      document.querySelector(".election_cube").className += " hide";
      _sessionStorage().set("CricketCube", false);
    }
  };

  if (scorecard && scorecard.Calendar) {
    schedule = scorecard.Calendar;
    schedule.forEach(function(item, index) {
      if (item.live == 1 && (item.teama == "India" || item.teamb == "India")) {
        pos = index;
      }
    });
  }

  return (
    <div className="election_cube">
      <div id="content">
        <span className="close_icon" onClick={hideCube}></span>
        <div id="box1" className="box-cube flipbox-wrapper">
          <div className="flipbox-box flipbox-horizontal">
            <div className="flipbox-side flipbox-front">
              {schedule ? scorecardWidget(schedule[pos], scoredetail) : null}
            </div>

            <div className="flipbox-side flipbox-back" style={{ transform: "translateZ(-195px) rotateY(180deg)" }}>
              {schedule ? scorecardWidget(schedule[pos], scoredetail) : null}
            </div>
            <div className="flipbox-side flipbox-left" style={{ transform: "rotateY(270deg) translateX(-195px)" }}>
              {scoredetail ? getBatsmanDetail(scoredetail, schedule[pos]) : null}
              {/* <a href={_deferredDeeplink("campaign", siteConfig.appdeeplink, null, siteConfig.mweburl + '/elections/lok-sabha-elections/results/' + _electionConfig.loksabhaResultsMSID + '.cms?frmapp=yes')} target="_blank" className="open-app-with-ad">
                                <button className="open-in-app">Open in App</button>
                            </a> */}
            </div>
            <div className="flipbox-side flipbox-right" style={{ transform: "rotateY(-270deg) translateX(195px)" }}>
              {scoredetail ? getBatsmanDetail(scoredetail, schedule[pos]) : null}
              {/* <a href={_deferredDeeplink("campaign", siteConfig.appdeeplink, null, siteConfig.mweburl + '/elections/lok-sabha-elections/results/' + _electionConfig.loksabhaResultsMSID + '.cms?frmapp=yes')} target="_blank" className="open-app-with-ad">
                                <button className="open-in-app">Open in App</button>
                            </a> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default { CricketCube, RotateCube };
