import React, { PureComponent } from "react";
import fetch from "../../../utils/fetch/fetch";
import {
  filterAlaskaData,
  getAlaskaSections,
  _getStaticConfig,
  generateUrl,
  _isCSR,
  isTechPage,
  isTechSite,
  getCountryCode,
} from "../../../utils/util";
import { connect } from "react-redux";
import { fetchHoverDataIfNeeded } from "../../../actions/header/header";
import ImageCard from "../../common/ImageCard/ImageCard";
import AnchorLink from "../AnchorLink";
import GnSearchBox from "../GnSearchBox/GnSearchBox";
import SvgIcon from "../../common/SvgIcon";
import { internationalconfig } from "../../../utils/internationalpageConfig";

const siteConfig = _getStaticConfig();

const siteName = process.env.SITE;

const configUtmCampaign = {
  news: "article",
  slideshow: "photo",
};

class NavBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showHoverSection: false,
      serchBoxVisible: false,
      isHoverVisible: false,
      isHoverVisibleL2: false,
      sectionData: {
        id: "",
        data: [],
      },
      sectionIdNews: "",
      hoverItem: "",
    };

    this.navItemCount =
      (siteName && ["tml"].includes(siteName)) || (siteName && ["mly"].includes(siteName))
        ? 8
        : siteName && ["nbt"].includes(siteName)
        ? 15
        : siteName && ["mt"].includes(siteName)
        ? 11
        : siteName && ["iag"].includes(siteName)
        ? 12
        : 10;
    this.secondLevelCount =
      siteName && ["nbt", "mt"].includes(siteName) ? 14 : siteName && ["mly"].includes(siteName) ? 8 : 9;
  }

  toggleSearchBox = status => {
    this.setState({ serchBoxVisible: !this.state.serchBoxVisible });
  };

  mouseLeaveHandler = () => {
    this.setState({
      isHoverVisible: false,
      sectionData: {
        id: "",
        data: [],
      },
    });
  };

  mouseLeaveHandlerL2 = type => {
    if (type == "parentbar") {
      this.setState({
        isHoverVisibleL2: false,
        hoverItem: "",
      });
    }
  };

  getUTMLink(item, type) {
    let webURL = item.weblink;
    //if (webURL && !webURL.includes("utm_source")) {
    // webURL = `${webURL}?utm_source=navigation`;
    //}
    return webURL;
  }

  getTn(weblink) {
    let tn = "";
    if (weblink && weblink.includes("/videolist/")) {
      tn = "video";
    } else if (weblink.includes("/photolist/")) {
      tn = "photo";
    } else {
      tn = "list";
    }
    return tn;
  }

  fetchMsid(item) {
    let msid = "";

    if (typeof item._sec_id != "undefined" && item._sec_id == "") {
      return msid;
    }

    if (item && item.weblink && !item.weblink.includes("/articleshow/")) {
      const weblink = item.weblink;
      // fetch msid in case msid in Alaska feed
      // doesn't match with weblink id
      if (item._sec_id) {
        msid = item._sec_id;
      } else if (item.msid && weblink.includes(item.msid)) {
        msid = item.msid;
      } else if (weblink && weblink.includes(".cms")) {
        const regexFetchLast = /\/(\w+)+\.cms/;
        const isNum = /^\d+$/;
        const resultArr = regexFetchLast.exec(weblink);
        const lastParam = resultArr && resultArr[1];
        if (isNum.test(lastParam)) {
          msid = lastParam;
        }
      } else {
        // if weblink doesn't include.cms
        msid = item.msid;
      }
    }
    return msid;
  }

  renderHoverSection = (msid, type, tn) => {
    const { dispatch, header, pagetype } = this.props;
    const { sectionData } = this.state;
    const _this = this;

    //console.log("renderHoverSection", msid, type, tn);

    if (!tn) {
      tn = "list";
    }

    if (!msid) {
      /*  this.setState({
        isHoverVisible: false,
        sectionData: {
          id: "",
          data: [],
        },
        hoverItem: "",
        isHoverVisibleL2: false,
      }); */
      return;
    }

    if (type == "parentbar") {
      if (msid == "navhover") {
        this.setState({
          isHoverVisible: false,
          isHoverVisibleL2: false,
          sectionData: {
            id: "navhover",
            data: [],
          },
          sectionIdNews: msid,
          hoverItem: "parentbar",
        });
      } else {
        dispatch(fetchHoverDataIfNeeded({ msid: msid, tn: tn })).then(() => {
          this.setState({
            sectionIdNews: msid,
            hoverItem: "parentbar",
            isHoverVisibleL2: true,
          });
        });
      }
    } else if (type == "subsection") {
      // for Home Page Sub Sections Mouse Hover
      if (msid) {
        dispatch(fetchHoverDataIfNeeded({ msid: msid, tn: tn }));
        this.setState({
          isHoverVisible: true,
          sectionIdNews: msid,
          hoverItem: "",
          isHoverVisibleL2: false,
        });
      } else {
        this.setState({
          isHoverVisible: true,
          sectionIdNews: "",
          hoverItem: "",
          isHoverVisibleL2: false,
        });
      }
    } else if (msid == "navhover") {
      // for More Sections(...) Mouse Hover
      this.setState({
        isHoverVisible: true,
        sectionData: {
          id: "navhover",
          data: [],
        },
        sectionIdNews: msid,
        hoverItem: "",
        isHoverVisibleL2: false,
      });
    } else {
      //console.log("msiddd", msid);
      if (msid && msid.trim()) {
        // if section have any section id
        const alaskaData = header && header.alaskaData;
        const objSectionInfo = filterAlaskaData(alaskaData, msid);
        const listdata = getAlaskaSections(objSectionInfo);
        // if there is no subsection will pick msid

        const sectionId = listdata.length > 0 ? _this.fetchMsid(listdata[0]) : msid;

        if (listdata.length > 0) {
          tn = this.getTn(listdata && listdata[0] && listdata[0].weblink);
        }

        if (sectionId && sectionId.trim()) {
          this.setState({
            isHoverVisible: true,
            sectionData: {
              id: msid,
              data: listdata,
            },
            sectionIdNews: sectionId,
            hoverItem: "",
            isHoverVisibleL2: false,
          });
          dispatch(fetchHoverDataIfNeeded({ msid: sectionId, tn: tn })); // returns news listings
        } else {
          this.setState({
            isHoverVisible: false,
            sectionData: {
              id: "",
              data: [],
            },
            hoverItem: "",
            isHoverVisibleL2: false,
          });
        }
      } else {
        // if section doesn't have any section id like newsbrief
        this.setState({
          isHoverVisible: false,
          sectionData: {
            id: "",
            data: [],
          },
          hoverItem: "",
          isHoverVisibleL2: false,
        });
      }
    }
  };

  componentDidUpdate(prevProps, prevstate) {
    // console.log("Nav Updated", this.state, prevstate);
  }

  render() {
    const { rootSections, parentSections, header, parentId, subsec1, secId, pagetype } = this.props;
    // console.log("Nav render", this.props);
    const { sectionData, isHoverVisible, isHoverVisibleL2, sectionIdNews, hoverItem } = this.state;

    const rootSectionsFiltered = rootSections.filter(item => item && item.class !== "desktophide");

    // const dataCopy = data ? [...data] : [];

    const hoverSectionNews = header && header.navHoverData && sectionIdNews ? header.navHoverData[sectionIdNews] : "";

    // console.log("navData112", hoverSectionNews);
    const hoverData = {
      sectionItems: { ...sectionData },
      newsItems: { ...hoverSectionNews },
    };

    // console.log("navData1121", hoverData, hoverItem);
    const countryCode = getCountryCode();
    return (
      <React.Fragment>
        <div
          id="fixedMenu"
          className="first-level-menu"
          onMouseLeave={this.mouseLeaveHandler}
          itemType="http://www.schema.org/SiteNavigationElement"
          itemScope="1"
        >
          {/* We are not passing header data fetched in SSR to CSR, header data is kept blank first in CSR
              and then one call retrieves header data again because of which there is unexpected behaviour in DOM arise  */}
          {rootSections && rootSections.length > 0 ? (
            <React.Fragment>
              <h3 className="home active" itemProp="name">
                <AnchorLink
                  href={
                    countryCode ? internationalconfig[siteName].internationalPagesUrl[countryCode] : siteConfig.weburl
                  }
                >
                  {/* {siteConfig && siteConfig.wapsitename} */}
                  <SvgIcon name="home" center="center" />
                </AnchorLink>
              </h3>
              <NavHTML
                data={rootSectionsFiltered}
                renderHoverSection={this.renderHoverSection}
                mouseLeaveHandlerL2={this.mouseLeaveHandlerL2}
                hoverData={hoverData}
                isHoverVisible={isHoverVisible}
                sectionIdNews={sectionIdNews}
                parentId={parentId}
                subsec1={subsec1}
                pagetype={pagetype}
                className="items"
                trimTo={this.navItemCount} // this is for root level home page
                getTn={this.getTn}
                fetchMsid={this.fetchMsid}
                getUTMLink={this.getUTMLink}
              />

              <div className={this.state.serchBoxVisible ? "search_in_header active" : "search_in_header"}>
                <b onClick={this.toggleSearchBox}>
                  <SvgIcon name="search" />
                </b>
                {this.state.serchBoxVisible && (
                  <div className="searchbox">
                    {isTechSite() && isTechPage() ? (
                      <GnSearchBox />
                    ) : (
                      <React.Fragment>
                        <iframe
                          taget="_parent"
                          scrolling="no"
                          // src={siteConfig.searchIframeLink}
                          src={`${process.env.WEBSITE_URL}${siteConfig.searchIframeLink}?layout=tmlnew`}
                          height="60"
                          width="100%"
                          frameBorder="0"
                        />
                      </React.Fragment>
                    )}

                    <span className="close_icon" onClick={this.toggleSearchBox} />
                  </div>
                )}
              </div>
            </React.Fragment>
          ) : null}
        </div>
        <NavHTML
          data={parentSections}
          renderHoverSection={this.renderHoverSection}
          mouseLeaveHandlerL2={this.mouseLeaveHandlerL2}
          parentId={parentId}
          subsec1={subsec1}
          secId={secId}
          htmlType="parentbar"
          trimTo={this.secondLevelCount}
          className="second-level-menu"
          getTn={this.getTn}
          fetchMsid={this.fetchMsid}
          getUTMLink={this.getUTMLink}
          hoverItem={hoverItem}
          isHoverVisibleL2={isHoverVisibleL2}
          hoverData={hoverData}
          pagetype={pagetype}
        />
      </React.Fragment>
    );
  }
}

const NavHTML = ({
  data,
  renderHoverSection,
  mouseLeaveHandlerL2,
  hoverData,
  isHoverVisible,
  sectionIdNews,
  parentId,
  subsec1,
  secId,
  htmlType,
  trimTo,
  pagetype,
  className,
  getTn,
  getUTMLink,
  fetchMsid,
  hoverItem,
  isHoverVisibleL2,
}) => {
  if (data && data.length > 0) {
    if (htmlType == "parentbar") {
      //parentbar is the second bar, first is root bar.
      return (
        <GetListItem
          data={data}
          isHoverVisibleL2={isHoverVisibleL2}
          parentId={parentId}
          secId={secId}
          className={className}
          trimTo={trimTo}
          fetchMsid={fetchMsid}
          renderHoverSection={renderHoverSection}
          mouseLeaveHandlerL2={mouseLeaveHandlerL2}
          type="parentbar"
          getTn={getTn}
          getUTMLink={getUTMLink}
          hoverItem={hoverItem}
          isHoverVisibleL2={isHoverVisibleL2}
          hoverData={hoverData}
          sectionIdNews={sectionIdNews}
          pagetype={pagetype}
        />
      );
    }

    //  console.log("hoverData", hoverData);

    let sectionItems = "";
    let newsItems = "";
    let sectionItemsData = "";
    if (hoverData && hoverData.sectionItems) {
      sectionItems = JSON.parse(JSON.stringify(hoverData.sectionItems));
      sectionItemsData = JSON.parse(JSON.stringify(sectionItems.data));
    }
    if (hoverData && hoverData.newsItems) {
      newsItems = JSON.parse(JSON.stringify(hoverData.newsItems));
    }

    //  console.log("hoverData11", newsItems, hoverItem, isHoverVisibleL2);

    // const sectionItems = (hoverData && hoverData.sectionItems) || "";
    //  const newsItems = (hoverData && hoverData.newsItems) || "";
    const entityClass = (newsItems && newsItems.data && newsItems.data.tn) || "";

    return (
      <React.Fragment>
        <GetListItem
          data={data}
          subsec1={subsec1}
          renderHoverSection={renderHoverSection}
          mouseLeaveHandlerL2={mouseLeaveHandlerL2}
          sectionItems={sectionItems}
          isHoverVisible={isHoverVisible}
          className={className}
          trimTo={trimTo}
          fetchMsid={fetchMsid}
          getTn={getTn}
          getUTMLink={getUTMLink}
        />

        {/*  Hover Sections for Home Page */}

        {isHoverVisible && sectionItemsData && sectionIdNews != "navhover" ? (
          <div className="hover_nav_content">
            <ul className="con_primary">
              {sectionItemsData && Array.isArray(sectionItemsData)
                ? sectionItemsData.map((item, index) => {
                    const tn = getTn(item.weblink);
                    const msid = fetchMsid(item);
                    //const utms = `?utm_source=navigation`;
                    return (
                      <li
                        key={index + msid}
                        onMouseOver={() => renderHoverSection(msid, "subsection", tn)}
                        className={sectionIdNews && sectionIdNews == msid ? "active" : ""}
                      >
                        <AnchorLink href={item.weblink}>{item.label}</AnchorLink>
                      </li>
                    );
                  })
                : ""}
            </ul>
            <HoverNewsItemsHtml sectionIdNews={sectionIdNews} newsItems={newsItems} entityClass={entityClass} />
          </div>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
  if (!(pagetype === "home" || pagetype === "liveblog")) {
    return <div className="second-level-menu"></div>;
  }

  return "";
};

const GetListItem = ({
  data,
  renderHoverSection,
  mouseLeaveHandlerL2,
  sectionItems,
  parentId,
  subsec1,
  secId,
  className,
  trimTo,
  fetchMsid,
  type,
  getTn,
  getUTMLink,
  hoverItem,
  isHoverVisibleL2,
  hoverData,
  sectionIdNews,
  pagetype,
}) => {
  const dataCopy = [...data];
  const firstViewItems = dataCopy.splice(0, trimTo);
  const moreViewItems = dataCopy;
  // console.log("firstViewItems11", type, hoverItem, isHoverVisibleL2, hoverData);

  const hoverDataCopy = hoverData && typeof hoverData == "object" && JSON.parse(JSON.stringify(hoverData));
  const newsItems = hoverDataCopy && hoverDataCopy.newsItems;

  const iscmpPage = ["comparelist", "compareshow"].includes(pagetype);
  const isGadgetPage = ["gadgetlist", "gadgetshow"].includes(pagetype);

  return (
    <div className={className} onMouseLeave={() => mouseLeaveHandlerL2 && mouseLeaveHandlerL2(type)}>
      <ul>
        {firstViewItems
          ? firstViewItems.map((item, index) => {
              let className = "";
              let classNames = "";
              if (
                (sectionItems && sectionItems.id && sectionItems.id == item.msid) ||
                // (item.msid && parentId && item.msid == parentId) ||
                (item.msid && secId && item.msid == secId) ||
                (item._sec_id && item._sec_id == secId) ||
                (item.msid && subsec1 && item.msid == subsec1) // this case is for making root section active and rest above are for parent section
              ) {
                if (!iscmpPage && !isGadgetPage) {
                  className = "active";
                }
              }

              if ((iscmpPage && item._pagetype == "compare") || (isGadgetPage && item._pagetype == "gadget")) {
                classNames = "active";
              }

              if (className && item.class) {
                classNames = `${className} ${item.class}`;
              } else if (className) {
                classNames = className;
              } else if (item.status) {
                classNames = item.status;
              } else if (item.class) {
                classNames = item.class;
              }

              if (item._classDesktop) {
                classNames += ` ${item._classDesktop}`;
              }

              const msid = fetchMsid(item);
              const tn = getTn(item.weblink);
              const webURL = getUTMLink(item, "section");

              return (
                <React.Fragment key={index.toString()}>
                  <li
                    onMouseOver={() => renderHoverSection && renderHoverSection(msid, type, tn)}
                    className={classNames}
                    itemProp="name"
                  >
                    <AnchorLink href={webURL} itemProp="url" key={index.toString()}>
                      {item.label}
                    </AnchorLink>
                  </li>
                </React.Fragment>
              );
            })
          : ""}
      </ul>
      {moreViewItems && moreViewItems.length > 0 ? (
        <div className="nav-more" onMouseOver={() => renderHoverSection && renderHoverSection("navhover", type)}>
          <span className="nav-more-icon">
            <SvgIcon name="ellipsis" />
          </span>
          <div className="hover_nav_content">
            <ul>
              {moreViewItems.map((item, index) => {
                const webURL = getUTMLink(item, "section");
                let classNames = "";
                if (item._classDesktop) {
                  classNames += ` ${item._classDesktop}`;
                }
                return (
                  <li key={index.toString()} className={classNames}>
                    <AnchorLink href={webURL}>
                      <span>{item.label}</span>
                    </AnchorLink>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      ) : (
        ""
      )}
      {isHoverVisibleL2 && hoverItem == "parentbar" && newsItems && (
        <div className="hover_nav_content" id="parentbar-hover-news">
          <HoverNewsItemsHtml
            sectionIdNews={sectionIdNews}
            newsItems={newsItems}
            entityClass={newsItems.data && newsItems.data.tn}
          />
        </div>
      )}
    </div>
  );
};

const HoverNewsItemsHtml = ({ sectionIdNews, newsItems, entityClass }) => {
  let items = "";
  if (newsItems && newsItems.data && newsItems.data.items && Array.isArray(newsItems.data.items)) {
    items = JSON.parse(JSON.stringify(newsItems.data.items));
  }
  //  console.log("newsItems111", newsItems);
  const secname = newsItems && newsItems.data && newsItems.data.secname;
  const tn = items && items[0] && items[0].tn;

  if (!items && newsItems.isFetching == "done") {
    <li className="no-hover-data">No Data Found</li>;
  }

  const utmC = configUtmCampaign[tn] ? configUtmCampaign[tn] : tn;
  //console.log("itemsitems", items);
  return (
    <ul className={`con_secondary  ${entityClass}`}>
      {sectionIdNews != "navhover" && items ? (
        items.map(item => {
          let itemLink = generateUrl(item);
          //itemLink += `?utm_source=navigation-hover&utm_campaign=${utmC}`;
          return item && item.hl ? (
            <li key={itemLink}>
              <AnchorLink href={itemLink}>
                <span className="img_wrap">
                  {item.imageid && item.imageid.includes("https") ? (
                    <img src={item.imageid} />
                  ) : (
                    <ImageCard
                      alt={item.imgalt || ""}
                      title={item.imgalt || ""}
                      msid={item.imageid}
                      size="smallthumb"
                      className="candidate_img"
                    />
                  )}
                </span>
                <span className="con_wrap">
                  <span className="text_ellipsis">{item.hl}</span>
                </span>
              </AnchorLink>
            </li>
          ) : (
            ""
          );
        })
      ) : (
        <div className="wdt_loading">
          <span className="spinner_circle" />
        </div>
      )}
    </ul>
  );
};

export default NavBar;
