/* eslint-disable react/react-in-jsx-scope */
import React, { PureComponent } from "react";

class AmazonWrapper extends PureComponent {
  componentDidMount() {}

  render() {
    return (
      <div className="prd_itm-cnt">
        <div className="prd_img-wrp">
          <a
            className="prd_ttl for_mob"
            target="_blank"
            href="https://pricee.com/api/redirect/n.php?itemid=1-topfsuczmqanfugf"
            rel="nofollow"
          >
            {"TOKYO TALKIES Casual Short Sleeve Printed Women Yellow Top"}
          </a>
          <div className="swiper-container gallery-top swiper-container-initialized swiper-container-horizontal">
            <div className="swiper-wrapper">
              <div className="swiper-slide swiper-slide-active" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugx5rfhy8zx.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugx5rfhy8zx.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
              <div className="swiper-slide swiper-slide-next" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxbu54nmmw.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxbu54nmmw.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
              <div className="swiper-slide" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxzaj7zbg8.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxzaj7zbg8.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
              <div className="swiper-slide" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugxzb4fznrp.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugxzb4fznrp.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
              <div className="swiper-slide" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugx8xqzqfrf.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugx8xqzqfrf.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
              <div className="swiper-slide" style={{ width: "218px" }}>
                <img
                  alt=""
                  className="lazyload"
                  data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugxtm88aqry.jpeg?q=70"
                  src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugxtm88aqry.jpeg?q=70"
                  data-loaded="true"
                />
              </div>
            </div>
            <span className="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
          </div>
          <div className="prd_thm-wrp">
            <div
              className="swiper-button-next swiper-button-white"
              tabIndex="0"
              role="button"
              aria-label="Next slide"
              aria-disabled="false"
            ></div>
            <div className="swiper-container gallery-thumbs swiper-container-initialized swiper-container-free-mode swiper-container-thumbs swiper-container-horizontal">
              <div className="swiper-wrapper">
                <div
                  className="swiper-slide swiper-slide-visible swiper-slide-active swiper-slide-thumb-active"
                  style={{ width: "55px" }}
                >
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugx5rfhy8zx.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugx5rfhy8zx.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
                <div className="swiper-slide swiper-slide-visible swiper-slide-next" style={{ width: "55px" }}>
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxbu54nmmw.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxbu54nmmw.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
                <div className="swiper-slide swiper-slide-visible" style={{ width: "55px" }}>
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxzaj7zbg8.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/a/z/4/m-tttp003696-tokyo-talkies-original-imafsugxzaj7zbg8.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
                <div className="swiper-slide" style={{ width: "55px" }}>
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugxzb4fznrp.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/s-tttp003696-tokyo-talkies-original-imafsugxzb4fznrp.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
                <div className="swiper-slide" style={{ width: "55px" }}>
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugx8xqzqfrf.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugx8xqzqfrf.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
                <div className="swiper-slide" style={{ width: "55px" }}>
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugxtm88aqry.jpeg?q=70"
                    src="https://rukminim1.flixcart.com/image/600/600/kbi9h8w0/top/u/g/f/xl-tttp003696-tokyo-talkies-original-imafsugxtm88aqry.jpeg?q=70"
                    data-loaded="true"
                  />
                </div>
              </div>
              <span className="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
            <div
              className="swiper-button-prev swiper-button-white swiper-button-disabled"
              tabIndex="0"
              role="button"
              aria-label="Previous slide"
              aria-disabled="true"
            ></div>
          </div>
        </div>
        <div className="prd_cnt-wrp">
          <div className="prd_ttl-wrp">
            <a
              className="prd_ttl for_dsk"
              target="_blank"
              href="https://pricee.com/api/redirect/n.php?itemid=1-topfsuczmqanfugf"
              rel="nofollow"
            >
              TOKYO TALKIES Casual Short Sleeve Printed Women Yellow Top
            </a>
            <div className="prd_rte-wrp">
              <div className="prd_rte-str" title="4.4">
                <span style={{ width: "86%" }} className="_pdsale-actv">
                  <i></i>
                  <i></i>
                  <i></i>
                  <i></i>
                  <i></i>
                </span>
                <span>
                  <i></i>
                  <i></i>
                  <i></i>
                  <i></i>
                  <i></i>
                </span>
              </div>
              <span className="prd_rte-txt">(14 ratings &amp; 122 reviews)</span>
            </div>
          </div>
          <div className="prd_prc-box">
            <div className="prd_prc-wrp">
              <div className="prd_prc-dtl">
                <div className="prd_act-prc">
                  <span className="prd_prc-rs">₹</span>770
                </div>
                <div className="prd_prc-dis">
                  <div className="prd_prc-cst">
                    <span className="prd_prc-rs">₹</span>2,749
                  </div>
                  <div className="prd_prc-off">(71% OFF)</div>
                </div>
              </div>
            </div>
            <div className="prd_btn-wrp">
              <a target="_blank" href="https://pricee.com/api/redirect/n.php?itemid=1-topfsuczmqanfugf" rel="nofollow">
                <span className="prd_btn-icn">
                  <img
                    alt=""
                    className="lazyload"
                    data-src="https://cdn.gadgets360.com/pricee/assets/store/1518083376_1487933307_flipkart.png"
                    src="https://cdn.gadgets360.com/pricee/assets/store/1518083376_1487933307_flipkart.png"
                    data-loaded="true"
                  />
                </span>
                <span className="prd_btn-txt">Buy Now</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AmazonWrapper;
