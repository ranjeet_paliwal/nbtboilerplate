/* eslint-disable jsx-a11y/iframe-has-title */
import React, { PureComponent } from "react";
import {
  isMobilePlatform,
  _getStaticConfig,
  _isCSR,
  elementInView,
  isTechPage,
  isTechSite,
  getPageTypeUpdated,
} from "../../../utils/util";

const siteConfig = _getStaticConfig();

class AmazonWidget extends PureComponent {
  componentDidMount() {
    // Event handler is bind in the same component as this component is called in
    // multiple pages, mobile, desktop
    // need to remove its handler from AS Page
    if (_isCSR()) {
      document.addEventListener("scroll", this.setFrameSrc);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("scroll", this.setFrameSrc);
  }

  setFrameSrc = () => {
    document.querySelectorAll(".amazon-frame").forEach(item => {
      const elmId = document.querySelector(`#${item.id}`);
      if (elmId && elementInView(elmId)) {
        const dataSrc = item.getAttribute("data-src");
        if (dataSrc) {
          item.setAttribute("src", dataSrc);
          item.removeAttribute("data-src");
        }
      }
    });
  };

  render() {
    const { item, router, type, techPageOnly } = this.props;
    const pathName = router && router.location && router.location.pathname;
    // if (_isCSR() && item && item.pwa_meta && item.pwa_meta.pagetype == "articleshow") {
    //   pathName = item.wu; // this is hack for articleshow in perpectual
    // }

    if (techPageOnly && (!isTechSite() || !isTechPage(pathName))) {
      return "";
    }

    let channel = siteConfig.channelCode;
    let platform = "web";

    if (channel == "eisamay") {
      channel = "eis";
    } else if (channel == "ml") {
      channel = "mly";
    }

    if (isMobilePlatform()) {
      if (pathName && pathName.indexOf("/amp_") > -1) {
        platform = "amp";
      } else {
        platform = "wap";
      }
    }

    let pageType = getPageTypeUpdated(pathName);

    if (pathName == "/tech/reviews.cms" || pageType == "amp_reviews") {
      pageType = "reviewlist";
    }
    if (pageType && pageType.startsWith("amp_")) {
      pageType = pageType.split("amp_")[1];
    }
    // https://navbharattimes.indiatimes.com/sc_affiliate_amazon.cms?tag=nbt_web_articleshow_sponsoredwidget-21&utm_campaign=nbt_web_articleshow_sponsoredwidget-21&title=Redmi-Note-9-(Aqua-Green,-4GB-RAM,-64GB-Storage)---48MP-Quad-Camera-and-Full-HD+-Display&price=11999&utm_source=articleshow&utm_medium=Affiliate&url=/dp/B08695YMYC"

    const pageViewNum = (_isCSR() && window.sessionPageView) || 0;

    // let frameSrc = "https://navbharattimes.indiatimes.com/off-url-forward/amazon_widget_new.cms";
    let frameSrc =
      channel === "nbt"
        ? "https://malayalam.samayam.com/pwafeeds/amazon_widget_new.cms"
        : "https://navbharattimes.indiatimes.com/pwafeeds/amazon_widget_new.cms";

    const productId = item && item.amazonkey ? `&productid=${item.amazonkey}` : "";
    if (type == "sponsored") {
      // const pageName = item.ismarkreview ? "reviewshow" : "articleshow";
      const tag = `${channel}_${platform}_${pageType}_sponsoredwidget-21`;
      frameSrc += `?type=gn&tag=${tag}&channel=${channel}${productId}&pagename=${pageType}&platform=${platform}&pageviewnum=${pageViewNum}`;
    } else if (type == "amazondeal") {
      frameSrc += `?type=amazondeal&pagename=articleshow&channel=${channel}&platform=${platform}&pageviewnum=${pageViewNum}`;
    }
    frameSrc = `${frameSrc}&msid=${item && item.id}`;
    const frameHeight = isMobilePlatform() || type == "amazondeal" ? "280" : "285";
    return (
      <div className="wdt_amz">
        <iframe
          className="amazon-frame"
          id={`wdt_amazon_${item && item.id}`}
          height={frameHeight}
          scrolling="no"
          frameBorder="no"
          data-src={frameSrc}
          allowTransparency="true"
        ></iframe>
      </div>
    );
  }
}

export default AmazonWidget;
