/* eslint-disable indent */
import React from "react";
import { _isCSR, _getStaticConfig, isMobilePlatform } from "../../utils/util";

const siteConfig = _getStaticConfig();
const iFrameUrl = process.env.SITE === "nbt" ? "https://maharashtratimes.com" : "https://navbharattimes.indiatimes.com";
const YSubscribeCard = () => (
  // _isCSR() ? (
  //   // eslint-disable-next-line react/no-danger
  //   <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: `` }} />
  // ) :
  <div className="youtube-iframe">
    <span className="txt">Subscribe </span>
    <iframe
      src={`${iFrameUrl}/youtubesubscribe.cms?channelid=${siteConfig.youtubeUrl.split("/channel/")[1]}`}
      height="45"
      width="130"
      border="0"
      marginWidth="0"
      marginHeight="0"
      hspace="0"
      vspace="0"
      frameBorder="0"
      scrolling="no"
      align="center"
      title="Subscribe Us On"
      allowTransparency="true"
      id="youtubeBtn"
    />
  </div>
);
// className="yContainer"
// style={{ maxWidth: isMobilePlatform() ? "initial" : "initial" }}
// data-exclude="amp"
// // eslint-disable-next-line react/no-danger
// dangerouslySetInnerHTML={{
//   __html: `
//       <span>Subscribe Us On</span>
//           <div class="g-ytsubscribe" style="width:160px;" data-channelid=${
//             siteConfig.youtubeUrl.split("/channel/")[1]
//           } data-layout="default" data-count="default"></div>
//   `,
// }}

export default YSubscribeCard;
