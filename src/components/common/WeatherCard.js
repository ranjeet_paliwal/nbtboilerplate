import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styles from "./css/WeatherCard.scss";
import "./css/UtilityWidgets.scss";
import { fetchWeatherDataIfNeeded } from "../../actions/app/app";

import { _getStaticConfig, getCountryCode } from "../../utils/util";
const siteConfig = _getStaticConfig();
const countryCode = getCountryCode();

const siteName = process.env.SITE;

class WeatherCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedCity: siteConfig.weather.defaultCity };
  }

  componentDidMount() {
    let { dispatch } = this.props;
    let { selectedCity } = this.state
    selectedCity = siteConfig.weather.defaultCity;
    this.setState({ selectedCity });

    if (dispatch) {
      const params = {};
      params.cityName = this.state.selectedCity;
      dispatch(fetchWeatherDataIfNeeded(params)); // Load Weather Widget
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { dispatch } = this.props;
    if (prevProps.selectedCity !== this.props.selectedCity) {
      const params = {};
      params.cityName = this.props.selectedCity;
      dispatch(fetchWeatherDataIfNeeded(params));
    }
  }

  cityChange = e => {
    const { dispatch } = this.props;
    if (dispatch) {
      if (e.target.value) {
        this.setState({ selectedCity: e.target.value });
      }
      const params = {};
      params.cityName = e.target.value;
      dispatch(fetchWeatherDataIfNeeded(params));
    }
  };

  renderV1(weatherData, citiesJson, weatherLabel) {
    return (
      <div className="wdt-weather">
        <h3>{siteConfig.weather.heading}</h3>
        <div className="wth-details table">
          <div className="table_row">
            <span className="forecast table_col">
              <img alt="" src={weatherData.weather_icon[weatherLabel]} />
            </span>
            <span className="temp table_col">
              <b>{weatherData.weather_info.observation.metric.temp}</b> C
            </span>
            <div className="loaction table_col">
              <div className="custom-select">
                <select onChange={this.cityChange} value={this.state.selectedCity}>
                  {citiesJson.map((item, index) => {
                    return (
                      <option value={item.city} key={"city" + index}>
                        {item[siteName]}
                      </option>
                    );
                  })}
                </select>
              </div>
              <span className="hight-low-temp">
                {`H ${weatherData.weather_info.observation.metric.temp_max_24hour} / L ${weatherData.weather_info.observation.metric.temp_min_24hour}`}
              </span>
              <span className="rain">{weatherData.weather_info.observation.sky_cover}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderV2(weatherData, weatherLabel) {
    return (
      <div className="utility-widget">
        <div className="table">
          {weatherData && weatherData.weather_info && weatherData.weather_info.observation ? (
            <div className="table_row">
              <div className="table_col iconcol">
                <img alt="" src={weatherData.weather_icon[weatherLabel]} />
              </div>
              <div className="table_col temp">
                <b>{weatherData.weather_info.observation.metric.temp}</b> C
              </div>
              <div className="table_col location lastcol">
                <span className="hight-low-temp">
                  {`H ${weatherData.weather_info.observation.metric.temp_max_24hour} / L ${weatherData.weather_info.observation.metric.temp_min_24hour}`}
                </span>
                <span className="rain">{weatherData.weather_info.observation.sky_cover}</span>
              </div>
            </div>
          ) : (
            <span
              style={{
                textAlign: "center",
                display: "block",
              }}
            >
              NA
            </span>
          )}
        </div>
      </div>
    );
  }

  render() {
    const { version } = this.props;
    const { weatherData, citiesJson } = this.props.weather ? this.props.weather : {};
    const weatherLabel =
      weatherData &&
        weatherData.weather_info &&
        weatherData.weather_info.observation &&
        weatherData.weather_info.observation.sky_cover
        ? weatherData.weather_info.observation.sky_cover.replace(/ +/g, "").toLowerCase()
        : null;
    if (!weatherData && !(version && version === "v2")) {
      return null;
    }
    return version && version === "v2"
      ? this.renderV2(weatherData, weatherLabel)
      : this.renderV1(weatherData, citiesJson, weatherLabel);
  }
}

WeatherCard.propTypes = {
  weather: PropTypes.object,
  version: PropTypes.string,
};
function mapStateToProps(state) {
  return {
    weather: state.app.weather,
  };
}
export default connect(mapStateToProps)(WeatherCard);
