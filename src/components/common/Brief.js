import React, { Component } from "react";
// import { Link } from 'react-router'
import PropTypes from "prop-types";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import "./css/Brief.scss";
import ImageCard from "./ImageCard/ImageCard";
// import FakeListing from './FakeCards/FakeListing';
// import FakeHorizontalListCard from './FakeCards/FakeHorizontalListCard';
import { AnalyticsGA, GTM } from "./../../components/lib/analytics/index";
// import Adcard from './AdCard';
import Ads_module from "./../../components/lib/ads/index";
import AnchorLink from "./AnchorLink";
import SocialShare from "./SocialShare";
import {
  _getStaticConfig,
  _deferredDeeplink,
  debounce,
  elementInView,
  isMobilePlatform,
  throttle,
} from "../../utils/util";
import { _isCSR } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const appdeeplink = siteConfig.appdeeplink;
import AdCard from "./AdCard";
import RhsWidget from "../desktop/RhsWidget";

let highestScrollEncountered = 0;

class NewsBrief extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focuscard: false,
      sectionInViewIndex: 0,
      _scrollEventBinded: false,
    };
    this.config = {
      maxCardLength: this.props.briefcardno ? this.props.briefcardno - 1 : 4,
      apponlyvisibility: this.props.briefvisiblepwa,
      cardIndex: 0,
      //brief card start with 0 index
    };
    // debounce removed as of now
    this.scrollWithThrottle = throttle(this.handleScroll.bind(this));
  }

  handleScroll() {
    let _this = this;
    trackersOnScroll(_this);
  }
  componentDidUpdate(_, prevState) {
    if (
      isMobilePlatform() &&
      prevState.sectionInViewIndex &&
      prevState.sectionInViewIndex > 0 &&
      this.state.sectionInViewIndex == 0
    ) {
      Ads_module.refreshAds(["atf"]);
    }
  }

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;
    let activeCardLink = window.location.href;
    let short_url = "";
    let sharedata = {
      title: document.title,
      url: activeCardLink,
      short_url: short_url ? short_url : null,
    };
    //for watsapp sharing feature along with GA
    AnalyticsGA.event({ category: "Briefs", action: "Click", label: "Share", briefURL: activeCardLink });

    if (!isShareAll) return sharedata;
  }
  fireGA(e, href) {
    AnalyticsGA.event({
      category: "Briefs",
      action: "Click",
      label: "Aur Padhe",
      briefURL: href,
    });
    // AnalyticsGA.GTM({ category: "Briefs", action: "Click", label: "Aur Padhe", event: "tvc_briefs_click" });
  }
  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms?utm_source=briefs";
    return item.override ? item.override : url;
  }

  componentDidMount() {
    let _this = this;
    //_this.handleCardSlider(_this);
    //tooltip showcase
    if (_isCSR()) {
      window.addEventListener("scroll", this.scrollWithThrottle);
    }
    setTimeout(function() {
      let tooltipobj = document.getElementById("wdt_download_apptooltip");
      if (tooltipobj) {
        let tooltipcontent = tooltipobj.style;
        if (tooltipcontent) {
          tooltipcontent.opacity = 1;
          (function fade() {
            (tooltipcontent.opacity -= 0.1) < 0 ? (tooltipcontent.display = "none") : setTimeout(fade, 120);
          })();
        }
      }
    }, 4000);

    if (window && window.location) {
      const url = window.location.pathname;
      var msid = url.substring(url.lastIndexOf(",") + 1, url.lastIndexOf("."));
      let u = "";
      let dom = null;
      let finaldiv;
      if (msid.includes("msid-")) {
        let h = msid.match(/[0-9]+/);
        if (h && h[0]) {
          u = h[0];
          dom = document.getElementsByClassName("article-section");
          for (let i in dom) {
            if (typeof dom[i] === "object") {
              if (dom[i].getAttribute("data-artmsid") === u) {
                finaldiv = dom[i];
                let position = 0;
                u && finaldiv && (position = finaldiv.offsetTop),
                  window.scrollTo({
                    top: position,
                    behavior: "smooth",
                  });
              }
            }
          }
        }
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.scrollWithThrottle);
  }

  render() {
    let _this = this;
    const brefsecId = this.props.sectionid || "";

    return (
      <div className="briefbox row">
        <div className={isMobilePlatform() ? "col12 visible-height" : "col8 visible-height"}>
          {/* swipperContainerDiv */}
          <ul className="slide_list swipperParentDiv">
            {this.props.item
              ? this.props.item.map((item, index) => {
                  let briefsyn =
                    item.briefsyn && item.briefsyn != "" && item.briefsyn.indexOf("--|$|--") > -1 ? (
                      <ul>
                        {item.briefsyn.split("--|$|--").map(function(item, index) {
                          return <li key={`briefsyn-${index}`}>{item}</li>;
                        })}
                      </ul>
                    ) : (
                      item.briefsyn
                    );
                  let href = _this.generateUrl(item);
                  let mrecMsType = "mrecinf";

                  if(index === 5){
                    mrecMsType = isMobilePlatform() ? "mrec1" : "mrec2";
                  }else{
                    mrecMsType = isMobilePlatform() && index === 5 * 2 ? "mrec2" : "mrecinf" 
                  }

                  return index < _this.props.briefcardno ? (
                    <ErrorBoundary key={item.id}>
                      {index % 5 === 0 && index !== 0 ? <AdCard mstype={mrecMsType} adtype="dfp" elemtype="li" /> : null}
                      {item && !item.type ? (
                        <li
                          key={`briefli${item.id}`}
                          data-m={item.m}
                          data-href={href}
                          data-msid={`article-${item.id}`}
                          data-artmsid={item.id}
                          data-index={index}
                          id={"carouseldot_" + index}
                          className={`article-section slide ${index == "0" ? "active" : ""}`}
                          data-secid={brefsecId}
                          data-seourl={item.seolocation}
                        >
                          <div className="top_content">
                            <div className="counter">
                              {index + 1}/{_this.props.briefcardno}
                            </div>
                            <AnchorLink onClick={e => this.fireGA(e, href)} href={href} className="table_row">
                              <span className="table_col img_wrap">
                                <ImageCard
                                  noLazyLoad={true}
                                  msid={item.imageid ? item.imageid : item.id}
                                  title={item.seotitle ? item.seotitle : ""}
                                  size={isMobilePlatform() ? "largewidethumb" : "largethumb"}
                                />
                              </span>
                            </AnchorLink>
                          </div>
                          <div className="bottom_content">
                            <AnchorLink onClick={e => this.fireGA(e)} href={href} className="table_row">
                              <span className="text_ellipsis">{item.hl} </span>
                              <span className="time-caption" data-time={item.lu && item.lu != "" ? item.lu : item.dl}>
                                {item.lu && item.lu != "" ? item.lu : item.dl}
                              </span>
                              <div className="briefsyn">{briefsyn}</div>
                            </AnchorLink>
                            <div className="actionbar">
                              {/* {index == 0 ? (
                              <div id="wdt_download_apptooltip" className="wdt_download_apptooltip">
                                <span className="btn_app_downloadtooltip">
                                  {siteConfig.locale.brief && siteConfig.locale.brief.tooltip
                                    ? siteConfig.locale.brief.tooltip
                                    : null}
                                </span>
                              </div>
                            ) : null} */}
                              <div>
                                {isMobilePlatform() ? (
                                  <a
                                    href={_deferredDeeplink(
                                      "section",
                                      appdeeplink,
                                      item.id,
                                      null,
                                      _this.props.briefsectioninfo,
                                    )}
                                    className="btn_app_download"
                                  >
                                    {siteConfig.locale.see_in_app}
                                  </a>
                                ) : null}
                                <div className="share_iconbox">
                                  <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                                </div>
                              </div>
                              <AnchorLink onClick={e => this.fireGA(e)} href={href} className="more">
                                {siteConfig.locale.read_more_liveblog}
                              </AnchorLink>
                            </div>
                          </div>
                        </li>
                      ) : null}
                    </ErrorBoundary>
                  ) : null;
                })
              : null}
          </ul>
        </div>
        {!isMobilePlatform() ? (
          <div className="briefrhs">
            {/* Without passing articleSecId 
            undefined == undefined for some site occurs 
            for apnabazaar condition check in component
            
            */}
            <div>
              <RhsWidget articleSecId="-1" pagetype="brief" />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
NewsBrief.propTypes = {
  section: PropTypes.object,
};

export function trackersOnScroll(_this) {
  try {
    //const { item } = _this.props;
    // loop on items length
    let briefSection = _this.props && _this.props.briefsction && _this.props.briefsction.redirectUrl;
    briefSection = briefSection && briefSection.includes(".com") ? briefSection.split(".com")[1] : null;
    let item = document.querySelectorAll(`.slide`);

    const header = document.querySelector("header");

    const firstSlide = item ? item[0] : null;
    //const redirectURL = item && item.
    // IF currently above first slide or header, reset page name
    if (header && firstSlide) {
      if (window.scrollY < header.getBoundingClientRect().bottom) {
        window.history.replaceState({}, document.title, briefSection);
        return;
      }
    }

    const currentScroll = window.scrollY;
    const rhsWidget = document.querySelector(".briefrhs");
    const rhsad = document.querySelector(".briefad");
    const contentStartElement = document.querySelector(".briefbox");
    let topOffset = 0;

    if (rhsWidget && rhsad) {
      const rhsWidgetBot = rhsWidget && rhsWidget ? rhsWidget.offsetTop + rhsWidget.offsetHeight : 700;
      topOffset = contentStartElement.offsetTop;
      if (currentScroll >= rhsWidgetBot && rhsad && !rhsad.classList.contains("sticky-rhs")) {
        rhsad.classList.add("sticky-rhs");
      }
      if (currentScroll < rhsWidgetBot && rhsad && rhsad.classList.contains("sticky-rhs")) {
        rhsad.classList.remove("sticky-rhs");
      }
    }

    let lastviewedID = "";
    let sectionInView = null;
    let sectionInViewIndex = 0;
    // document.querySelectorAll(`.slide`).removeClass("active");
    for (let i = 0; i < item.length; i++) {
      const article = item[i];
      const _index = i;

      //const article = document.querySelector(`[data-msid="article-${itemNo.id}"]`);
      const secId = article.getAttribute("data-secid");
      let artmsid = article.getAttribute("data-artmsid");
      // checking box id view on window height half
      // && lastviewedID !== artmsid
      if (article && !elementInView(article, true, 50)) {
        article.classList.remove("active");
      }
      if (article && elementInView(article, true, 50)) {
        sectionInView = article;
        sectionInViewIndex = i;
        lastviewedID = artmsid;
        const _article = article;
        let pathname = location.href;
        let msid = _article.getAttribute("data-artmsid");
        let seoURL = _article.getAttribute("data-seourl");
        let currentUrl = "";
        currentUrl =
          window.location.origin +
          "/newsbrief/" +
          seoURL +
          "/newsbrief/" +
          "articleid-" +
          secId +
          ",msid-" +
          msid +
          ".cms";

        const triggerUrlRelatedChanges = currentUrl => {
          window.history.replaceState({}, document.title, currentUrl);
          if (isMobilePlatform()) {
            Ads_module.refreshAds(["fbn"]);
          } else {
            Ads_module.refreshAds(["atf", "fbn"]);
          }

          article.classList.add("active");
        };

        // Desktop url changes triggered here
        if (pathname != currentUrl && !isMobilePlatform()) {
          triggerUrlRelatedChanges(currentUrl);
        }

        if (_index === 0) {
          sectionInView.setAttribute("comscorepageview", true);
        }
        if (sectionInView && !sectionInView.getAttribute("gatracked")) {
          sectionInView.setAttribute("gatracked", true);
          // Fire pageview for perpetual
          // AnalyticsGA.pageview(window.location.pathname);
          // Fire pageview
          AnalyticsGA.pageview(window.location.pathname, {
            setEditorName: true,
            pageCountInc: false,
            comscorepageview: sectionInView.getAttribute("comscorepageview"),
          });

          // added for pageview_candidate call
          sectionInView.setAttribute("comscorepageview", true);
          if (!isMobilePlatform()) {
            break;
          }
        }

        // /\ Dont remove
        if (!isMobilePlatform()) {
          break;
        }

        // Doesn't seem needed
        // trigger page view on page load
        // if (!_article.getAttribute("gatracked")) {
        //   // Hit ga
        //   AnalyticsGA.pageview(window.location.pathname, {
        //     setEditorName: false,
        //     pageCountInc: true,
        //     comscorepageview: _article.getAttribute("comscorepageview"),
        //   });
        //   _article.setAttribute("gatracked", true);
        //   _article.setAttribute("comscorepageview", true);
        //   break;
        // }
        // Mobile url changes triggered here
        if (isMobilePlatform()) {
          const currentSection = item[_this.state.sectionInViewIndex];
          if (
            sectionInViewIndex == 0 &&
            window.scrollY <=
              (item[sectionInViewIndex].getBoundingClientRect().top +
                item[sectionInViewIndex].getBoundingClientRect().height) /
                5
          ) {
            _this.setState({
              sectionInViewIndex,
            });
            currentUrl =
              window.location.origin +
              "/newsbrief/" +
              seoURL +
              "/newsbrief/" +
              "articleid-" +
              secId +
              ",msid-" +
              msid +
              ".cms";

            triggerUrlRelatedChanges(currentUrl);

            break;
          }

          // Greater or not in viewport
          else if (
            sectionInViewIndex > _this.state.sectionInViewIndex ||
            (!elementInView(currentSection, true) && _this.state.sectionInViewIndex == sectionInViewIndex + 1)
          ) {
            _this.setState({
              sectionInViewIndex,
            });
            currentUrl =
              window.location.origin +
              "/newsbrief/" +
              seoURL +
              "/newsbrief/" +
              "articleid-" +
              secId +
              ",msid-" +
              msid +
              ".cms";

            triggerUrlRelatedChanges(currentUrl);
          }
        }
      } else if (article) {
        article.removeAttribute("gatracked");
      }
    }
  } catch (e) {
    console.log(`trackersOnScroll error${e.message}`);
  }
}

export default NewsBrief;
