import React, { useState } from "react";
import { _isCSR, _getStaticConfig, isBusinessSection, isGadgetPage } from "../../utils/util";
import AnchorLink from "../common/AnchorLink";
import gadgetsConfig from "../../utils/gadgetsConfig";
const siteConfig = _getStaticConfig();
const footerConfig = _getStaticConfig("footer");

const LangDropDown = React.memo(({ location, pagetype, clicked }) => {
  let linkItems = footerConfig && footerConfig.languages;
  /*  if (isGadgetPage({ pagetype })) {
    linkItems = gadgetsConfig.languages;
  } */

  const [status, setStatus] = useState(false);

  return !isBusinessSection(location && location.pathname) ? (
    <div className="other-sites">
      <span className="head" onClick={() => setStatus(!status)}>
        <a title="" rel="" href="#">
          {`${siteConfig.language}`}
        </a>
      </span>
      <ul className={`restLang${status ? " active" : ""}`}>
        {linkItems.map(item => {
          return (
            <li>
              <AnchorLink key={item.name} href={item.link}>
                {item.regName || item.name}
              </AnchorLink>
            </li>
          );
        })}
      </ul>
    </div>
  ) : (
    ""
  );
});

export default LangDropDown;
