import React from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import PopupCard from "./PopupCard";
import fetch from "../../utils/fetch/fetch";

const createNICWidget = msid => {
  if (typeof window !== "undefined") {
    const apiUrl = `${process.env.API_BASEPOINT}/pwa_articleshow.cms?feedtype=sjson&version=v9&tag=news&msid=${msid}&pwa=true`;

    let popupContainer = document.getElementById("popupContainer");

    if (!popupContainer) {
      popupContainer = document.createElement("div");
      popupContainer.setAttribute("id", "popupContainer");
      document.body.append(popupContainer);

      popupContainer = document.getElementById("popupContainer");
    }

    fetch(apiUrl)
      // .then(resp => resp.json())
      .then(data => {
        popupContainer.innerHTML = (
          <PopupCard hideOnOverlayClicked isVisible>
            Hello, I dont have any callback.
          </PopupCard>
        );
      })
      .catch(err => {});
  }
};

const NICCard = props => {
  const { msid, text } = props;
  const _this = this;

  return (
    <Link to={`/articleshow/${msid}.cms?type=NIC`}>{text}</Link>
    /* <div>
			<span className = "nic-card" onClick = {createNICWidget.bind(this,msid)} > +{text}+ </span>
		</div> */
  );
};

export default NICCard;
