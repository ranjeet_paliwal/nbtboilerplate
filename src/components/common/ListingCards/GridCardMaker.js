/* eslint-disable prettier/prettier */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable indent */
/* eslint-disable no-nested-ternary */
import React from "react";
import ImageCard from "../ImageCard/ImageCard";
import AnchorLink from "../AnchorLink";
import VideoItem from "../VideoItem/VideoItem";
import { SeoSchema } from "../PageMeta"; // For Page SEO/Head Part

import {
  _getStaticConfig,
  _deferredDeeplink,
  isMobilePlatform,
  generateUrl,
  _getUrlOfCategory,
} from "../../../utils/util";

import styles from "../css/NewsListCard.scss";
// eslint-disable-next-line no-unused-vars
import stylesD from "../css/commonComponents.scss";
// eslint-disable-next-line import/extensions
import globalconfig from "../../../globalconfig.js";

if (!globalconfig.position) {
  globalconfig.position = 1;
}

const siteConfig = _getStaticConfig();

// cardtypes could be of 5types - lead, only-info, horizontal, vertical, horizontal-lead - heandled via css
function GridCardMaker(props) {
  const {
    card,
    columns,
    cardType,
    className,
    seolistschema,
    layout,
    type,
    keyName,
    imgsize,
    width,
    margin,
    configImgsize,
    listCountInc,
    index,
    compType,
    parentMsid,
    noSeo,
    sliderClick,
    videoIntensive,
    noLazyLoad,
    hidethumbandsection,
  } = props;

  const appexclusive = typeof card.platform === "string" && card.platform.toLowerCase() === "apponly";
  const { pagetype } = globalconfig;
  const deeplinktype =
    pagetype === "home"
      ? card.app_tn && card.app_tn !== "" && card.app_tn
      : card.tn && card.tn !== "" && card.tn === "news"
      ? "toparticle"
      : card.tn && card.tn !== ""
      ? card.tn
      : "news";

  const listType = {
    photo: "photolist",
    video: "videolist",
    news: "articlelist",
    moviereview: "articlelist",
    movieshow: "articlelist",
    slideshow: "photolist",
  };

  // For Brandwire article handling
  const url = card.brandstry && card.brandstry === 1 ? (card.override ? card.override : generateUrl(card)) : null;

  // let listlink = (appexclusive || (card.platform && card.platform.toLowerCase()=='apponly')) ? {override :  siteConfig.applinks.android.oip_appexclusive + card.id + '&pagetype='+ pagetype + '&type=' + (card.tn && card.tn != '' ? card.tn : 'news') } : (card.override ? {override :  card.override} : {seo: card.seolocation, tn : card.tn, id : (card.tn == 'photo') ? card.id && card.id != '' ? card.id : card.imageid : card.id});

  // deeplink will form only in case pagetype == home

  let listlink = "";
  if (isMobilePlatform() && appexclusive && typeof _deferredDeeplink === "function") {
    listlink = {
      override: _deferredDeeplink(deeplinktype, siteConfig.appdeeplink, card.id),
      url,
    };
  } else if (card.override) {
    listlink = { override: card.override };
  } else {
    listlink = {
      site: card.site,
      seo: card.seolocation,
      app_tn: card.app_tn,
      tn: card.tn,
      id: card.tn === "photo" ? (card.id && card.id !== "" ? card.id : card.imageid) : card.id,
      url,
    };
  }

  const imgsizeprop = configImgsize
    ? configImgsize
    : imgsize
    ? imgsize
    : layout && layout !== ""
    ? "squarethumb"
    : card.tn === "video" || type === "video_lead"
    ? "largewidethumb"
    : // : leadpost && card.tn == "photo"
    cardType === "lead" && (card.tn === "photo" || card.tn === "news")
    ? "largethumb"
    : cardType === "lead"
    ? "largewidethumb"
    : cardType === "webstories"
    ? "webstoriesthumb"
    : card.tn === "photo" || card.tn === "slideshow" || card.tn === "news"
    ? "smallthumb"
    : "smallwidethumb";

  const gadgetRating = card.tn !== "moviereview" && card.tn !== "movieshow" && card.cr && card.cr != "";

  return (
    <li
      data-attr="as"
      // For Items having 'pl' and 'seo' attribute, will not append schema in those items
      itemProp={
        (card && (card.pl === "1" || card.noseo === "1")) || pagetype == "articleshow" || noSeo
          ? undefined
          : "itemListElement"
      }
      itemScope="1"
      title={card.hl}
      itemType={
        (card && (card.pl === "1" || card.noseo === "1")) || pagetype == "articleshow" || noSeo
          ? undefined
          : card.definitionTerm
          ? "https://schema.org/DefinedTerm"
          : "http://schema.org/ListItem"
      }
      className={`${className || ""} news-card ${cardType} col${columns} ${
        card.tn === "slideshow"
          ? "photo"
          : card.seolocation && card.seolocation.includes("web-stories")
          ? "webstories"
          : card.seolocation && card.seolocation.includes("apna-bazaar")
          ? "apanaBazaar"
          : card.tn
      } ${imgsize} ${
        isMobilePlatform() && pagetype !== "others" && pagetype !== "tech" && appexclusive ? " app-exclusive" : ""
      } ${compType === "videolist" ? "big" : ""} ${card.isvdo && card.tn !== "video" ? "video" : ""}`}
      style={className && className.indexOf("slide") > -1 ? { width: `${width}px`, paddingRight: `${margin}px` } : null}
      key={card.id + keyName}
      // data-list-type={this.showListNodeLabel(item)}

      // Below schema is commented because we are including schema above on the basis of item
      // from where it belongs if item belongs to top section we will not mark schema for that
      // {...(seolistschema != true
      //   ? SeoSchema({ pagetype: pagetype }).listItem()
      //   : "")}
    >
      {/* Below snippet works in case list item is video */}
      {card.tn === "video" && !hidethumbandsection && (
        <VideoItem
          videoIntensive={videoIntensive || (compType && compType === "videolist") || !card.eid}
          hideDescription
          parentMsid={parentMsid}
          type={
            card.imageid && typeof card.imageid === "string" && card.imageid.toLowerCase().indexOf("http") > -1
              ? "absoluteImgSrc"
              : null
          }
          item={card}
          src={card.imageid}
          alt={card.seotitle ? card.seotitle : ""}
          msid={card.imageid ? card.imageid : card.id}
          title={card.seotitle ? card.seotitle : card.hl || ""}
          istrending={props.istrending}
          index={index}
          listlink={listlink}
          imgsize={imgsizeprop}
          noSeo={noSeo}
          sliderClick={sliderClick}
          lead={cardType && cardType === "lead" && index === 0}
        />
      )}

      {/* This snippet is used to show image for list item and create link for the same */}
      {card.tn !== "video" && !hidethumbandsection && (
        <span
          // data-tag={card.tn == "video" ? card.du : null}
          className="img_wrap"
        >
          <AnchorLink
            key={card.id + keyName}
            hrefData={listlink}
            fireClickPixel={card.clkurl ? card.clkurl : null}
            istrending={props.istrending}
          >
            {cardType !== "only-info" ? (
              <ImageCard
                type={
                  card.imageid && typeof card.imageid === "string" && card.imageid.toLowerCase().indexOf("http") > -1
                    ? "absoluteImgSrc"
                    : null
                }
                src={card.imageid}
                alt={card.seotitle ? card.seotitle : ""}
                msid={card.imageid ? card.imageid : card.id}
                title={card.seotitle ? card.seotitle : card.hl || ""}
                size={card.seolocation && card.seolocation.includes("web-stories") ? "webstoriesthumb" : imgsizeprop}
                imgver={card.imgsize}
                noLazyLoad={noLazyLoad || (cardType && cardType.indexOf("lead") > -1 && index === 0)}
              />
            ) : null}
          </AnchorLink>
        </span>
      )}

      {card.tn !== "video" && (
        <span className="con_wrap">
          {(card.subsecoverride ||
            (card.subsecname &&
              card.subsecname !== "" &&
              card.subsecseolocation &&
              card.subsecmsid &&
              card.tn !== "" &&
              card.tn !== "lb")) &&
          !hidethumbandsection ? (
            // Below part is used to show section name for each listing item
            <AnchorLink
              key="section_name"
              className="section_name"
              href={
                card.subsecoverride
                  ? card.subsecoverride
                  : `/${card.subsecseolocation}/${listType[card.tn]}/${card.subsecmsid}.cms`
              }
            >
              {card.subsecname}
            </AnchorLink>
          ) : null}
          {/* below Snippet is used to show headline for list item or ratings for movie list item  */}
          <AnchorLink
            key="section_data"
            hrefData={listlink}
            itemProp={!(card && (card.pl === "1" || card.noseo === "1")) ? "url" : ""}
            fireClickPixel={card.clkurl ? card.clkurl : null}
            className="table_row"
            {...(seolistschema !== true ? SeoSchema({ pagetype }).attr().url : "")}
            istrending={props.istrending}
          >
            {!noSeo && !(card && (card.pl === "1" || card.noseo === "1" || card.definitionTerm)) ? (
              <meta itemProp="position" content={globalconfig.position++} />
            ) : null}
            {(card.tn === "moviereview" || card.tn === "movieshow") && !card.showcard ? ( // showcard node is there in headline feed
              <MovieCard card={card} pagetype={pagetype} listCountInc={listCountInc} />
            ) : (
              <span className="text_ellipsis" {...(seolistschema !== true ? SeoSchema({ pagetype }).attr().name : "")}>
                {card.tn === "lb" && <span className="liveblink_newscard">Live</span>} {card.hl}
              </span>
            )}
            {props.children}
          </AnchorLink>

          {card && card.authorName && card.authorBlog ? (
            <span className="auname">
              <a key="author_data" href={card.authorBlog}>
                {" "}
                - {card.authorName}
              </a>
            </span>
          ) : null}

          {card && card.authorName && !card.authorBlog ? <span className="auname"> - {card.authorName}</span> : null}
          {/* NBT Citision reporter address */}
          {pagetype === "articlelist" && card && card.authorLoc ? (
            <span className="auloc">
              {card.authorLoc}
              {/* {card && card.authorCity} */}
            </span>
          ) : null}

          {gadgetRating && <GadgetReviewScore card={card} />}
        </span>
      )}

      {isMobilePlatform() && appexclusive ? <span className="app-txt">App Exclusive</span> : null}
    </li>
  );
}

export default GridCardMaker;

const GadgetReviewScore = props => {
  const { card } = props;
  return (
    <React.Fragment>
      {card.GadgetsModelDisplayName || card.GadgetsBrand || card.GadgetsCategory ? (
        <span className="more_brands">
          {card.GadgetsModelDisplayName && card.GadgetsCategory ? (
            <AnchorLink
              href={`/tech/${_getUrlOfCategory(card.GadgetsCategory)}/${card.GadgetsPrimaryGadget &&
                card.GadgetsPrimaryGadget.toLowerCase()}`}
            >
              {card.GadgetsModelDisplayName}
            </AnchorLink>
          ) : null}
          {card.GadgetsBrand || card.GadgetsCategory ? (
            <React.Fragment>
              {card.GadgetsBrand && card.GadgetsCategory ? (
                <AnchorLink
                  href={`/tech/${_getUrlOfCategory(card.GadgetsCategory)}/${card.GadgetsBrand &&
                    card.GadgetsBrand.toLowerCase()}`}
                >
                  {card.GadgetsBrand}
                </AnchorLink>
              ) : null}
              {card.GadgetsCategory ? (
                <AnchorLink href={`/tech/${_getUrlOfCategory(card.GadgetsCategory)}`}>
                  {card.GadgetsCategory}
                </AnchorLink>
              ) : null}
            </React.Fragment>
          ) : null}
        </span>
      ) : null}
      {card.cr && card.cr ? (
        <span className="rating_txt">
          {siteConfig.locale.tech && siteConfig.locale.tech.criticsrating}:{" "}
          <b>
            <small>{card.cr}</small>/5
          </b>
        </span>
      ) : null}
    </React.Fragment>
  );
};

const MovieCard = props => {
  const { card, pagetype, listCountInc } = props;
  return card ? (
    <span className={`${styles.con_wrap} table_col con_wrap`}>
      <span className={`${styles.title} title text_ellipsis`} {...SeoSchema({ pagetype }).attr().name}>
        <b>{card.hl}</b>
      </span>
      {card.syn && process.env.PLATFORM === "desktop" ? (
        <span className={`${styles.desc} desc text_ellipsis`}>{card.syn}</span>
      ) : null}
      {/* for Schema only */ SeoSchema({
        pagetype,
      }).metaPosition(listCountInc ? listCountInc() : "")}

      {card.cr && (
        <span className="rating">
          <span className="rating-stars">
            <span className="empty-stars" />
            <span style={{ width: `${parseInt(card.cr * 20)}%` }} className="filled-stars critic" />
          </span>
          <span className="rate_val">
            {parseFloat(card.cr) > 0 ? parseFloat(card.cr).toFixed(1) : card.cr}
            <small>/5</small>
          </span>
          <b>{siteConfig.locale.critics_rating}</b>
        </span>
      )}
      {card.ur && (
        <span className="rating">
          <span className="rating-stars">
            <span className="empty-stars" />
            <span style={{ width: `${parseInt(card.ur * 20)}%` }} className="filled-stars user" />
          </span>
          <span className="rate_val">
            {parseFloat(card.ur) > 0 ? parseFloat(card.ur).toFixed(1) : card.ur}
            <small>/5</small>
          </span>
          <b>{siteConfig.locale.user_rating}</b>
        </span>
      )}

      {card.ct ? (
        <span className={`${styles.cast} cast text_ellipsis`}>
          <small>{siteConfig.locale.cast}</small>
          {card.ct}
        </span>
      ) : (
        ""
      )}
      {card.gn && process.env.PLATFORM !== "desktop" ? <span className={`${styles.des} des`}>{card.gn}</span> : null}

      {card.cr || card.ur ? (
        <span
          className={`${styles["time-caption"]} time-caption`}
          data-time={card.lu ? card.lu : card.dl ? card.dl : ""}
        >
          {card.lu ? card.lu : card.dl ? card.dl : ""}
        </span>
      ) : (
        ""
      )}
    </span>
  ) : null;
};
