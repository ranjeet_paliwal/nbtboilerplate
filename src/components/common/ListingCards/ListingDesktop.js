import React, { Component } from "react";
// import { Link } from "react-router";
import PropTypes from "prop-types";
import NewsListCard from "./NewsListCardDesktop";
import MovieReviewTile from "../../common/MovieReviewTile";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import styles from "./../../common/css/commonComponents.scss";
import FakeListing from "../../common/FakeCards/FakeListing";
import FakeHorizontalListCard from "../../common/FakeCards/FakeHorizontalListCard";
// import Adcard from "../../common/AdCard";
// import ListHorizontalCard from "../../common/ListHorizontalCard";

// import { SeoSchema } from "../../common/PageMeta";
import AnchorLink from "../../common/AnchorLink";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { _getStaticConfig } from "../../../utils/util";
import CustomListingCard from "./CustomListingCard";

const siteConfig = _getStaticConfig();

class Listing extends Component {
  constructor(props) {
    super(props);
  }

  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn]
      ? siteConfig.listNodeLabels[item.tn][0]
      : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      item.id +
      ".cms";

    return item.override ? item.override : url;
  }

  render() {
    //widgetType can be headlisting, articlelist, videolist, photolist
    let {
      section,
      widgetType,
      widgetSubType,
      pagetype,
      listCountInc,
      subsec1,
      key
    } = this.props;
    let {
      cardViewType,
      listViewType,
      _listLength,
      listHeading,
      _showMoreButton
    } = this.props;
    let scrollItems;
    widgetType = widgetType ? widgetType : section.tn;

    let listLength = _listLength
      ? _listLength
      : widgetType == "photolist" || widgetType == "videolist"
      ? 1
      : section.items && section.items.length
      ? section.items.length
      : 0;
    if (
      (_listLength || widgetType == "photolist" || widgetType == "videolist") &&
      section.items
    ) {
      scrollItems = { ...section };
      scrollItems.items =
        section.items.constructor === Array
          ? section.items.filter((item, index) => index > (listLength || 0))
          : []; //FIX: when only single item is there it comes as an object
    }

    let listingLink = this.generateUrl(section);
    let widgetHeading = listHeading
      ? listHeading
      : widgetType == "videolist"
      ? section.secname
        ? section.secname
        : siteConfig.locale.videos
      : widgetType == "photolist"
      ? section.secname
        ? section.secname
        : siteConfig.locale.photo_maza
      : section.secname;
    let showWidgetHeading = widgetType == "headlisting" ? false : true;
    let showMoreButton =
      typeof _showMoreButton == "boolean"
        ? _showMoreButton
        : widgetType == "headlisting"
        ? false
        : widgetHeading
        ? true
        : false;
    let moreButtonText = widgetHeading + siteConfig.locale.read_more_listing;

    /*FIX for case when only single item is there in section items */
    if (
      section &&
      typeof section.items != "undefined" &&
      section.items.constructor !== Array
    ) {
      let tempSec = Object.assign({}, section.items);
      section.items = [tempSec];
    }
    return (
      <div
        data-rlvideoid={section._rlvideoid ? section._rlvideoid : null}
        className={
          styles["box-content"] +
          " box-content " +
          (widgetType == "videolist" && widgetSubType == "homepageVideo"
            ? "videoview"
            : "")
        }
        data-exclude={this.props.amp && this.props.amp == "no" ? "amp" : null}
      >
        {showWidgetHeading ? (
          <h2>
            {widgetHeading ? (
              <AnchorLink hrefData={{ override: listingLink }}>
                <span>{widgetHeading} </span>
              </AnchorLink>
            ) : null}
          </h2>
        ) : null}

        <ul
          className={`nbt-list ${
            this.props.layout && this.props.layout != ""
              ? this.props.layout
              : ""
          }`}
        >
          {/* For SEO Schema */}
          {/* <meta itemProp="numberOfItems" content={section && typeof (section.items) != 'undefined' && section.items.length > 0 ? section.items.length : 0} /> */}
          {section &&
          typeof section.items != "undefined" &&
          section.items.length > 0 ? (
            section.items.map((item, index) => {
              if (item.tn == "moviereview" || item.tn == "movieshow") {
                return (
                  <ErrorBoundary key={index}>
                    <MovieReviewTile
                      pagetype={pagetype}
                      listCountInc={listCountInc ? listCountInc : ""}
                      key={index}
                      item={item}
                      leadpost={index == 0 ? true : false}
                      position={index + 1}
                    />
                  </ErrorBoundary>
                );
              } else if (item.tn == "ad") {
                return null;
                /*_listLength ?
                      null
                      :
                      <Adcard key={`listingad_${index}`} ctnstyle="inline" mstype={item.mstype} adtype={item.type} elemtype="li" />
                    */
                //return(<li ctn-style={item.mstype} data-plugin="ctn" ></li> )
              } else if (index <= listLength) {
                return (
                  <ErrorBoundary key={index}>
                    <NewsListCard
                      layout={
                        this.props.layout && this.props.layout != ""
                          ? this.props.layout
                          : ""
                      }
                      pagetype={pagetype}
                      listCountInc={listCountInc ? listCountInc : ""}
                      type={widgetType == "videolist" ? "video_lead" : ""}
                      item={item}
                      seolistschema={section.items[0].pl == 1 ? true : false}
                      leadpost={
                        listViewType != 0 &&
                        (index <= _listLength || index == 0)
                          ? true
                          : false
                      }
                      cardViewType={cardViewType}
                      listViewType={listViewType}
                    />

                    {/* Print Paytm Widget for Tech Section Only */}
                    {/* {index == 6 ? <li className="nbt-listview"><PaytmWidget pagename="articlelist" subsec1={subsec1} /></li> :null} */}
                  </ErrorBoundary>
                );
              } else {
                return null;
              }
            })
          ) : (
            <li>
              <FakeListing showImages={true} />
            </li>
          )}
          {scrollItems &&
          typeof scrollItems.items != "undefined" &&
          scrollItems.items.length > 0 ? (
            <ErrorBoundary>
              <CustomListingCard
                pagetype={pagetype}
                listCountInc={listCountInc ? listCountInc : ""}
                item={scrollItems}
                cardViewType={0}
                listViewType={listViewType}
                className={
                  widgetType == "videolist" ? "nbt-horizontalView" : ""
                }
              />
            </ErrorBoundary>
          ) : null}
          {section &&
          typeof section.items != "undefined" &&
          section.items.length > 0 &&
          section.rlvideo ? (
            <ErrorBoundary>
              <CustomListingCard
                pagetype={pagetype}
                listCountInc={listCountInc ? listCountInc : ""}
                type="rlvideo"
                item={section.rlvideo}
              />
            </ErrorBoundary>
          ) : section && section.isfetchingrlvideo ? (
            <FakeHorizontalListCard />
          ) : null}
        </ul>

        {showMoreButton &&
        section &&
        typeof section.items != "undefined" &&
        section.items.length > 0 ? (
          <AnchorLink
            hrefData={{ override: listingLink }}
            className={styles["more-btn"] + " more-btn"}
          >
            {moreButtonText}
          </AnchorLink>
        ) : null}
      </div>
    );
  }
}

Listing.propTypes = {
  section: PropTypes.object
};

export default Listing;
