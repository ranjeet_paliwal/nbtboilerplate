import React from "react";
import GridCardMaker from "./GridCardMaker";
import AdCard from "../AdCard";
import DataDrawer from "../../desktop/DataDrawer";
import { isMobilePlatform, _isCSR, _getStaticConfig } from "../../../utils/util";
import SelectCity from "../SelectCity/SelectCity";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";

const siteConfig = _getStaticConfig();

function GridListMaker(props) {
  const {
    list,
    datalist,
    keyName,
    className,
    imgsize,
    compType,
    dispatch,
    noSeo,
    noLazyLoad,
    isFeaturedArticle,
    isTopSection,
    hidethumbandsection,
    mediaWireData,
  } = props;

  let columns = list && list.noOfColumns ? parseInt(12 / list.noOfColumns, 10) : "";
  if (columns === 5) columns = 24;
  const configImgsize = list.imgsize ? list.imgsize : undefined;

  return (
    <React.Fragment>
      <ul className={className}>
        {datalist instanceof Array &&
          datalist.map((card, index) => {
            card = addAttrToCard(list, card);
            if (card.tn == "ad" && !list.offads) {
              // for handling platforms i.e. if we want to show ad only in mobile or desktop
              return typeof card.platform === "string" && card.platform != process.env.PLATFORM ? null : (
                <li
                  key={index}
                  className={`news news-card ${list.type} col${list.adcols ? list.adcols : columns} con_ads`}
                >
                  {!isFeaturedArticle ? (
                    <AdCard
                      mstype={card.mstype}
                      adtype={card.type}
                      ctnstyle={card.mstype}
                      rendertype={card.mstype && card.mstype === "ctnhometop" ? "prerender" : ""}
                    />
                  ) : null}
                  {/* <div className={`ad1 ${card.mstype}`} /> */}
                </li>
              );
            }
            if (card.type == "data_drawer" && !list.offDrawer) {
              return (
                <li key={index} className={`news-card wdt_data_drawer ${list.type} col${columns}`}>
                  {<DataDrawer dispatch={dispatch} />}
                </li>
              );
            }

            if (card.itemtype === 'mediawire' && card.platform === process.env.PLATFORM) {
              return (
                <MediaWireCard 
                  mediaWireData={mediaWireData}
                  mediaKey={`${keyName}${index}${card.id}`}
                  keyName={`${keyName}${index}`}
                  cardType={list.type}
                  columns={columns}
                  configImgsize={configImgsize}
                  imgsize={imgsize}
                  istrending={list.istrending}
                  index={index}
                  compType={compType}
                  noSeo={noSeo}
                  noLazyLoad={noLazyLoad || (isTopSection && index < 4)}
                  adcols={list.adcols ? list.adcols : columns}
                />
              );
            }

            if (card.type == "ddl_city") {
              return (
                <li key={index} className={`news-card city_drawer ${list.type}`}>
                  <div className="selectCity_wrapper">
                    {<SelectCity sectionId={siteConfig.pages.stateSection} extraContent="true" />}
                  </div>
                </li>
              );
            }
            // else if (card.tn == "banner") {
            //   return <Banner data={card} />;
            // }
            if (card.itemtype == "advertorial" && isMobilePlatform(true) == card.platform) {
              return (
                <GridCardMaker
                  card={card}
                  key={`${keyName}${index}${card.id}`}
                  keyName={`${keyName}${index}`}
                  cardType={list.type}
                  columns={columns}
                  configImgsize={configImgsize}
                  imgsize={imgsize}
                  istrending={list.istrending}
                  index={index}
                  compType={compType}
                  noSeo={noSeo}
                  noLazyLoad={noLazyLoad || (isTopSection && index < 4)}
                />
              );
            }
            if (
              (card.tn == "news" ||
                card.tn == "video" ||
                card.tn == "photo" ||
                card.tn == "slideshow" ||
                card.tn == "moviereview" ||
                card.tn == "movieshow" ||
                card.tn == "lb" ||
                card.tn == "videoshow" ||
                card.tn == "") &&
              card.itemtype !== "advertorial"
            ) {
              return (
                <GridCardMaker
                  card={card}
                  key={`${keyName}${index}${card.id}`}
                  keyName={`${keyName}${index}`}
                  cardType={list.type}
                  columns={columns}
                  configImgsize={configImgsize}
                  imgsize={imgsize}
                  istrending={list.istrending}
                  index={index}
                  compType={compType}
                  noSeo={noSeo}
                  noLazyLoad={noLazyLoad || (isTopSection && index < 4)}
                  hidethumbandsection={hidethumbandsection}
                />
              );
            }
            return null;
          })}
      </ul>
    </React.Fragment>
  );
}

function addAttrToCard(list, card) {
  if (list.definitionTerm) {
    card["definitionTerm"] = list.definitionTerm;
  }
  return card;
}

const MediaWireCard = props => {
  const { mediaWireData, mediaKey, keyName, cardType, columns, configImgsize, imgsize, istrending, index, compType, noSeo, noLazyLoad, adcols } = props
  return (
    <ErrorBoundary>
    {mediaWireData && mediaWireData.tn == 'ad' ?
    <li key={index}
        className={`news news-card ${cardType} col${adcols} con_ads`}
    >
    <AdCard
          mstype={mediaWireData.mstype}
          adtype={mediaWireData.type}
          ctnstyle={mediaWireData.mstype}
          rendertype={mediaWireData.mstype && mediaWireData.mstype === "ctnhometop" ? "prerender" : ""}
    /></li>:
     <GridCardMaker
          card={mediaWireData}
          key={mediaKey}
          keyName={keyName}
          cardType={cardType}
          columns={columns}
          configImgsize={configImgsize}
          imgsize={imgsize}
          istrending={istrending}
          index={index}
          compType={compType}
          noSeo={noSeo}
          noLazyLoad={noLazyLoad || (isTopSection && index < 4)}
    />} 
  </ErrorBoundary>
  )}
export default GridListMaker;
