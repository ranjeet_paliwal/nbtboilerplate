import React from "react";
import Loadable from "react-loadable";
import GridListMaker from "./GridListMaker";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import { isMobilePlatform, LoadingComponent } from "../../../utils/util";
import { designConfigs } from "../../../containers/desktop/home/designConfigs";
import { defaultDesignConfigs } from "../../../containers/defaultDesignConfigs";
import SectionHeader from "../SectionHeader/SectionHeader";

const PointsTableCard = Loadable({
  loader: () => import("../../../campaign/cricket/components/PointsTableCard"),
  LoadingComponent,
});

const compMap = {
  pointstablecard: PointsTableCard,
};

function dataListCutter(list, startIndex, noOfElements) {
  if (!list) list = [];
  let dataArrayList = [...list];
  const filteredDataList = dataArrayList.filter(item => {
    return !item.tn || item.tn !== "ad" || !item.platform || item.platform === process.env.PLATFORM;
  });

if (noOfElements > 0) {
    dataArrayList = filteredDataList.splice(startIndex, noOfElements);
  } else {
    dataArrayList = filteredDataList.splice(startIndex);
  }
  return dataArrayList;
}

function GridSectionMaker(props) {
  const {
    data,
    className,
    override,
    imgsize,
    compType,
    noSeo,
    noLazyLoad,
    ptMsid,
    isFeaturedArticle,
    enableHeader,
    isTopSection,
    hidethumbandsection,
    mediaWireData,
  } = props;
  let { type } = props;
  if (!type) {
    return "";
  }
  if (typeof type === "string") {
    if (isMobilePlatform()) {
      type = defaultDesignConfigs[type];
    } else {
      type = designConfigs[type];
    }
  }
  if (type.sections) {
    return (
      <React.Fragment>
        {type.sections instanceof Array &&
          type.sections.map((section, index) => (
            <div key={index.toString()} className={section.className}>
              <GridSectionMaker
                type={section.type}
                data={data}
                imgsize={imgsize}
                compType={compType}
                noSeo={noSeo}
                noLazyLoad={noLazyLoad}
                isFeaturedArticle={isFeaturedArticle}
                enableHeader={enableHeader}
                mediaWireData={mediaWireData}
              />
            </div>
          ))}
      </React.Fragment>
    );
  }

  return (
    <ErrorBoundary>
      {type.map((list, index) => {
        const ul_index = data && data[0] ? `${index}_${data[0].id}` : "keyName";
        // Flexibility provided if we want to override so default design Configs
        let _list = {};
        if (!list.immutable && typeof override === "object") {
          _list = Object.assign({}, list, override);
        } else {
          _list = list;
        }
        if (list.sections) {
          // Check for nesting Sections
          return (
            <GridSectionMaker key={ul_index} type={_list} data={data} compType={compType} noSeo={noSeo} mediaWireData={mediaWireData} noLazyLoad={noLazyLoad} />
          );
        }
        if (_list.type && _list.type === "component") {
          const CustomTag = compMap[_list.compName];

          return (
            <div className={_list.className}>
              <CustomTag {..._list.props} ptMsid={ptMsid} />
            </div>
          );
        }
        return (
          <React.Fragment>
            {enableHeader ? (
              <SectionHeader sectionhead={data[0].secname} weblink={data[0].override || data[0].wu} />
            ) : null}
            <GridListMaker
              key={ul_index}
              keyName={ul_index}
              list={_list}
              datalist={dataListCutter(data, _list.startIndex, _list.noOfElements)}
              className={_list.className}
              imgsize={imgsize || ""}
              compType={compType}
              noSeo={noSeo}
              noLazyLoad={noLazyLoad}
              isFeaturedArticle={isFeaturedArticle}
              isTopSection={isTopSection}
              hidethumbandsection={hidethumbandsection}
              mediaWireData={mediaWireData}
            />
          </React.Fragment>
        );
      })}
    </ErrorBoundary>
  );
}

export default GridSectionMaker;
