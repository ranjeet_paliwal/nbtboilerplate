import React, { Component } from "react";
import fetchJsonP from "fetch-jsonp";
import AnchorLink from "./AnchorLink";
import { _getStaticConfig, _isCSR, _deferredDeeplink, _sessionStorage } from "../../utils/util";
import { fireActivity } from "../common/TimesPoints/timespoints.util";
import { _getCookie } from "../../utils/util";
import styles from "./css/NewsLetterCard.scss";
import { AnalyticsGA } from "../lib/analytics/index";
const siteConfig = _getStaticConfig();
const SSO = siteConfig.sso;
import SvgIcon from "./SvgIcon";

class NewsLetter extends Component {
  constructor(props) {
    super(props);
    this.config = {
      value: "",
      // emailRegex: /\S+@\S+\.\S+/,
      emailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      param: {
        EM: "",
        LS: "2254",
        APK: "d5025f0d-9117-4179-937f-3107468c3270",
        ticketid: false,
      },
      pagetype: "",
      articleid: "",
    };
  }

  componentDidMount() {
    this.init();
  }

  handleValue(e) {
    this.config.value = e.target.value;
  }

  handleNewsLetter = () => {
    let mailId;
    mailId = this.config.value;
    this.onSubmissionTrigger();
    const label =
      this.config.pagetype == "home" ? "HP_Newsletter" : this.config.pagetype == "articleshow" ? "HP_AS" : "";

    AnalyticsGA.event({
      category: "Newsletter",
      action: "Click",
      label,
    });
  };

  // Auto fill email ID for Logedin case
  autofillEmail() {
    const textboxes = document.querySelectorAll('[data-type="txtSubsEmail"]');
    // event.subscribe("user.status", function (user) {
    // if (user != null) {
    if (_getCookie("ssoid") && window._user != null) {
      textboxes.forEach(tbox => {
        tbox.value = _user.EMAIL;
        tbox.disabled = true;
      });
      this.config.param.EM = _user.EMAIL;
      this.checkStatus("stscheck", "loggedin");
    }
    // else {
    //     this.config.param.ticketid = false;
    //     this.config.param.EM = "";

    //     if ($('[data-type="unsubscriptionform"]').length > 0) {
    //         //for Unsubscription case
    //         $('[data-type="txtSubsEmail"]').val(this.getUrlParameter('email')).prop('disabled', true);;
    //         this.checkStatus("stscheck", "loggedout");
    //     }
    //     else {
    //         $('.chkboxList [type="checkbox"]').prop("checked", false);
    //         $('[data-type="txtSubsEmail"]').val("").prop('disabled', false);
    //     }
    // }
    // });
  }

  // On Submition
  onSubmissionTrigger() {
    const self = this;

    let article;
    let emailTxtbox;
    let errorsummary;

    article = document.querySelector(`[data-newsletter="articleNewsletter-${this.config.articleid}"]`);
    if (article) {
      emailTxtbox = article.querySelector('[data-type="txtSubsEmail"]');
      errorsummary = article.querySelector('[data-type="errorsummary"]');
    } else {
      emailTxtbox = document.querySelector('[data-type="txtSubsEmail"]');
      errorsummary = document.querySelector('[data-type="errorsummary"]');
    }
    // let chkboxList = document.querySelector('.chkboxList');
    //  $('[data-type="btnSubscribe"]').click(function () {
    if (emailTxtbox.value != "" && this.config.emailRegex.test(emailTxtbox.value)) {
      // errorsummary.html("");
      // emailTxtbox.removeClass("error");
      // chkboxList.find($('input[type="checkbox"]').prop("checked")).prop("disabled", true);
      // if (document.querySelector(".chkboxList [type='checkbox']:checked").length > 0) {

      // errorsummary.html("");

      // For Logedin Case
      // if (login.getUser()) {
      if (window._user) {
        self.getticket("subscribe", "loggedin");
      } else {
        self.config.param.ticketid = false;
        self.checkStatus("subscribe", "loggedout", "false");
      }
    } else if (emailTxtbox.value == "") {
      errorsummary.innerHTML = siteConfig.locale.newsLetter.blankemail;
      if (!emailTxtbox.classList.contains("error")) emailTxtbox.classList += "error";
    } else {
      errorsummary.innerHTML = siteConfig.locale.newsLetter.validemail;
      if (!emailTxtbox.classList.contains("error")) emailTxtbox.classList += "error";
    }
  }

  // On Submition
  unSubscribTrigger() {
    // $('[data-type="btnUnSubscribe"]').click(function () {
    //     if ($(".chkboxList [type='checkbox']:checked").length == 0) {
    //         $('[data-type="errorsummary"]').html(msg.unchecked);
    //     }
    //     else {
    //         $('[data-type="errorsummary"]').html("");
    //         this.unsubscribe(this.getUrlParameter('email'), this.getUrlParameter('hashcode'));
    //     }
    // })
  }

  init() {
    setTimeout(() => {
      this.autofillEmail();
    }, 15000);
    // this.onSubmissionTrigger();
    // this.unSubscribTrigger();

    // if ($('[data-type="confirmationform"]').length > 0) {
    //     this.verify(this.getUrlParameter('email'), this.getUrlParameter('hashcode'));
    // }
  }

  // Check Subscription Status
  getticket(req, sts) {
    // let url = SSO_URL_NewsTicket;
    fetchJsonP(SSO.SSO_URL_NewsTicket, {
      method: "GET",
      mode: "cors",
      credentials: "include",
    })
      .then(response => response.json())
      .then(data => {
        this.config.param.ticketid = !!(data.ticketId && data.ticketId != "");
        this.checkStatus("subscribe", "loggedin", data.ticketId);
      })
      .catch(err => {
        console.log("INSIDE FETCH get ticket error", err);
      });
  }

  // Check Subscription Status
  checkStatus(req, sts, tck) {
    let reqheader;

    const self = this;

    let article;
    let emailTxtbox;
    let errorsummary;

    article = document.querySelector(`[data-newsletter="articleNewsletter-${this.config.articleid}"]`);
    if (article) {
      emailTxtbox = article.querySelector('[data-type="txtSubsEmail"]');
      errorsummary = article.querySelector('[data-type="errorsummary"]');
    } else {
      emailTxtbox = document.querySelector('[data-type="txtSubsEmail"]');
      errorsummary = document.querySelector('[data-type="errorsummary"]');
    }

    const label = "";

    if (sts === "loggedin") {
      reqheader = {
        LS: siteConfig.locale.newsLetter.LS,
        APK: this.config.param.APK,
        TCK: tck,
        EM: emailTxtbox.value,
      };
    } else {
      reqheader = {
        LS: siteConfig.locale.newsLetter.LS,
        APK: this.config.param.APK,
        EM: emailTxtbox.value,
      };
    }
    fetch(siteConfig.sso.SSO_URL_SubStatus, {
      dataType: "json",
      headers: reqheader,
    })
      .then(response => response.json())
      .then(data => {
        // Status Check on page Load to set check box property
        if (
          req === "stscheck" &&
          (data.code === 36 || data.code === 42 || data.code === 43 || data.code === 44) &&
          (data.status === "VERIFIED_USER" || data.status === "VERIFIED_SSO_USER")
        ) {
          const newsletters = document.querySelectorAll(".wdt_newsLetter");
          if (newsletters) {
            newsletters.forEach(nletter => {
              nletter.style.display = "none";
            });
          }

          // if (data.nlids.length > 0) {
          //     for (i = 0; i < data.nlids.length; i++) {
          //         $('.chkboxList').find('input[value="' + data.nlids[i] + '"]').prop('checked', true).prop('disabled', true);
          //     }
          // }
          // if ($('[data-type="unsubscriptionform"]').length > 0) {
          //     $('.chkboxList input[type="checkbox"]').prop('disabled', false);
          // }
        }

        // Code 34 ("New User, verification email sent")
        else if (data.code === 34 && req == "subscribe" && data.status === "NEW_USER") {
          self.initiateSubscribe(tck, sts, data.code);
        }

        // Code 35 ("User not verified, email not verified")
        else if (data.code === 35 && req == "subscribe" && data.status === "NON_VERIFIED_USER") {
          self.success("Earlier we sent email to you, please check your mailbox and verify");
        }

        // Code 36 or code 44 ("verified user or verified sso user, check for already subscribed or not")
        else if (
          (data.code === 36 || data.code === 42 || data.code === 43 || data.code === 44) &&
          req == "subscribe" &&
          (data.status === "VERIFIED_USER" ||
            data.status === "VERIFIED_SSO_USER" ||
            data.status === "VERIFIED_NON_SUBSCRIBE_USER" ||
            data.status === "VERIFIED_NON_SUBSCRIBE_SSOUSER")
        ) {
          const result = 0;
          // for (i = 0; i < data.nlids.length; i++) {
          //     if ($('.chkboxList').find('input[value="' + data.nlids[i] + '"]').prop("checked")) {
          //         result = result + 1;
          //     }
          // }
          if (data.nlids.length >= 1 && (data.code === 36 || data.code === 42 || data.code === 44)) {
            // Already Subscribe
            self.success(siteConfig.locale.newsLetter.already);
          } else if (data.code === 43) {
            self.success(siteConfig.locale.newsLetter.login);
          } else {
            self.initiateSubscribe(tck, sts, data.code);
          }
        }

        // Code 44 ("VERIFIED_SSO_USER")
        else if (data.code === 44 && req == "subscribe" && data.status === "VERIFIED_SSO_USER") {
          if (sts != "loggedin") {
            errorsummary.innerHTML = siteConfig.locale.newsLetter.login;
            if (!emailTxtbox.classList.contains("error")) emailTxtbox.classList += "error";

            // var login = require('tiljs/login');
          } else {
            self.initiateSubscribe(tck, sts, data.code);
          }
        }
      });
  }

  // For Subscribe NewsLetter
  initiateSubscribe(tck, sts, code) {
    let reqheaders;
    let article;
    const self = this;
    let emailTxtbox;

    article = document.querySelector(`[data-newsletter="articleNewsletter-${this.config.articleid}"]`);
    if (article) {
      emailTxtbox = article.querySelector('[data-type="txtSubsEmail"]');
    } else {
      emailTxtbox = document.querySelector('[data-type="txtSubsEmail"]');
    }

    if (sts === "loggedin") {
      reqheaders = {
        LS: siteConfig.locale.newsLetter.LS,
        APK: this.config.param.APK,
        TCK: tck,
        EM: emailTxtbox.value,
      };
    } else {
      reqheaders = {
        LS: siteConfig.locale.newsLetter.LS,
        APK: this.config.param.APK,
        EM: emailTxtbox.value,
      };
    }

    // Get all check checkboxes
    // $(".chkboxList [type='checkbox']:checked").each(function () {
    //     if ($(this).prop('checked')) {
    //         nlids += this.value + '|';
    //     }
    // });
    // nlids = nlids.slice(0, -1); //removes last pipe sign.

    fetch(siteConfig.sso.SSO_URL_subscribe, {
      type: "OPTIONS",
      dataType: "json",
      headers: reqheaders,
    })
      .then(response => response.json())
      .then(data => {
        if (data.status === "SUCCESS" && data.code === 28 && tck == "false" && code === 34) {
          self.success(siteConfig.locale.newsLetter.verification);
        } else if (data.status === "SUCCESS" && data.code === 28 && (code === 44 || code === 34 || code === 43)) {
          self.success(siteConfig.locale.newsLetter.Subscribed);
          self.timesPointsActivity();
          // ga('send', 'event', 'newsletter', 'subscribed', 'newsletter');
        }
      });
  }

  // For Unsubscription
  unsubscribe(email, hash) {
    const reqheaders = {
      LS: mod_newsletter.config.param.LS,
      APK: mod_newsletter.config.param.APK,
      EM: email,
      HC: hash,
    };
    let nlids = "";
    // Get all check checkboxes
    $(".chkboxList [type='checkbox']:checked").each(function() {
      if ($(this).prop("checked")) {
        nlids += `${this.value}|`;
      }
    });
    nlids = nlids.slice(0, -1); // removes last pipe sign.
    $.ajax({
      dataType: "json",
      url: `${newsletterfeeds.unsubscribe}&nlid=${nlids}`,
      headers: reqheaders,
      success(data) {
        if (data.code === 28 && data.message === "Already Unsubscribed") {
          mod_newsletter.success(mod_newsletter.config.msg.alreadyunsubscribed);
        } else if (data.code === 28 && data.status == "SUCCESS") {
          // Object {message: "Unsubscribe Successful", status: "SUCCESS", code: 28}
          mod_newsletter.success(mod_newsletter.config.msg.unsubscribed);
        } else if (data.status == "ERROR") {
          mod_newsletter.success(mod_newsletter.config.msg.error);
        }
      },
    });
  }

  // Trigger on success subscription
  success(msg) {
    let article;

    let pnlsuccess;
    let successDiv;

    article = document.querySelector(`[data-newsletter="articleNewsletter-${this.config.articleid}"]`);
    if (article) {
      pnlsuccess = article.querySelector(".pnlsuccess");
      successDiv = article.querySelector('[data-type="successmsg"]');
      // Pass mod_newsletter.config.msg as a parameter
      article.querySelector('[data-type="subscriptionform"]').style.display = "none";
    } else {
      pnlsuccess = document.querySelector(".pnlsuccess");
      successDiv = document.querySelector('[data-type="successmsg"]');
      // Pass mod_newsletter.config.msg as a parameter
      document.querySelector('[data-type="subscriptionform"]').style.display = "none";
    }
    successDiv.innerHTML = msg;
    pnlsuccess.style.display = "block";

    // $('[data-type="subscriptionform"], [data-type="unsubscriptionform"]').hide();
    // $('[data-type="successmsg"]').html(mod_newsletter.config.msg).show();
  }

  // TO Verify Email
  verify(email, hash) {
    const self = this;
    const reqheaders = {
      LS: mod_newsletter.config.param.LS,
      APK: mod_newsletter.config.param.APK,
      EM: email,
      HC: hash,
    };
    $.ajax({
      dataType: "json",
      url: newsletterfeeds.confirm,
      headers: reqheaders,
      success(data) {
        if (data.code === 39 && data.message === "Thanks for verification") {
          mod_newsletter.success(mod_newsletter.config.msg.Subscribed);
          self.timesPointsActivity();
          AnalyticsGA.event({
            category: "newsletter",
            action: "subscribed",
            label: "newsletter",
          });
          // ga("send", "mobilenewsletter", "newsletter", "subscribed", "newsletter");
        } else if (data.code === 38 && data.message === "User Already Verified") {
          mod_newsletter.success(mod_newsletter.config.msg.verified);
        }
      },
    });
  }

  getUrlParameter(sParam) {
    const sPageURL = decodeURIComponent(window.location.search.substring(1));

    const sURLVariables = sPageURL.split("&");

    let sParameterName;

    let i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split("=");

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  }

  timesPointsActivity() {
    if (typeof fireActivity == "function") {
      fireActivity("SUB_NL", "newsletter");
    }
  }

  render() {
    let headerText;
    let joinIn;
    let footerText;
    let thankyou;

    headerText =
      siteConfig.locale.newsLetter && siteConfig.locale.newsLetter.headerText
        ? siteConfig.locale.newsLetter.headerText
        : "";
    joinIn =
      siteConfig.locale.newsLetter && siteConfig.locale.newsLetter.joinIn ? siteConfig.locale.newsLetter.joinIn : "";
    footerText =
      siteConfig.locale.newsLetter && siteConfig.locale.newsLetter.footerText
        ? siteConfig.locale.newsLetter.footerText
        : "";
    thankyou =
      siteConfig.locale.newsLetter && siteConfig.locale.newsLetter.thankyou
        ? siteConfig.locale.newsLetter.thankyou
        : "";

    const { pagetype, articleid } = this.props;
    this.config.pagetype = pagetype;
    this.config.articleid = articleid;

    return (
      <div className="wdt_newsLetter" data-exclude="amp" data-newsletter={`articleNewsletter-${this.config.articleid}`}>
        <div data-type="subscriptionform" className="subscriptionform">
          <div className="head">
            <SvgIcon name="email" className="mail_icon" />
            {<h3>{headerText}</h3>}
          </div>
          <div className="formElements">
            <div className="input_txt">
              <input
                type="text"
                name="email"
                size="35"
                data-type="txtSubsEmail"
                onChange={event => this.handleValue(event)}
                placeholder="example@domain.com"
              />
              <input type="submit" value={joinIn} onClick={this.handleNewsLetter} />
            </div>
            <span data-type="errorsummary" className="error_msg" />
          </div>
          {/* Removed as per Vasu and Minakshi */}
          {/* <div className="bottom_caption">{footerText}</div> */}
        </div>
        <div className="pnlsuccess">
          <SvgIcon name="email" className="mail_icon" />
          <h4>{thankyou}</h4>
          <span data-type="successmsg" className="message_txt" />
        </div>
      </div>
    );
  }
}

export default NewsLetter;
