import React, { Component } from "react";
import "./css/Cube.scss";
import AnchorLink from "./AnchorLink";
import globalconfig from "../../globalconfig";
import {
  _getStaticConfig,
  _isCSR,
  _deferredDeeplink,
  _sessionStorage,
  isMobilePlatform,
  hasValue,
} from "../../utils/util";
const siteConfig = _getStaticConfig();
import Ads_module from "./../../components/lib/ads/index";
import { electionConfig } from "../../campaign/election/utils/config";
import { AnalyticsGA } from "./../../components/lib/analytics/index";
import ImageCard from "./../../components/common/ImageCard/ImageCard";
import { fetchTopListDataIfNeeded } from "./../../actions/home/home";
import { fetchMiniScheduleDataIfNeeded } from "../../modules/minischedule/actions/minischedule";
import { connect } from "react-redux";
import { _getTeamLogo } from "../../utils/cricket_util";
import { teamScore } from "../../campaign/cricket/components/schedulecard";
import { extractMiniSchedule } from "../../campaign/cricket/service";
import fetch from "../../utils/fetch/fetch";
// import './css/MiniSchedule.scss';

const _electionConfig = electionConfig[process.env.SITE];

class Cube extends Component {
  constructor(props) {
    super(props);
    this.config = {
      count: 0,
      rotationInterval: null,
      refreshInterval: null,
      href: undefined,
      scorecardInterval: 30000,
      timeInterval: 5000,
    };

    this.state = {
      results: null,
    };

    window.addEventListener("hideCube", e => {
      if (e.detail && e.detail.routeChange) this.hideCube(e.detail.routeChange);
    });

    window.addEventListener("rotateCube", e => {
      if (e && e.detail && e.detail.cubeProp) this.RotateCube(e.detail.cubeProp);
    });
  }

  componentDidMount() {
    let { cubeProp, dispatch, params, query, router } = this.props;
    let self = this;
    this.RotateCube(cubeProp);
    let iplMsid;

    // Cube auto close if value is set in configure part
    if (cubeProp.autoClose && cubeProp.autoClose != "") {
      let timeToClose = parseInt(cubeProp.autoClose);
      if (timeToClose > 0) {
        setTimeout(function() {
          self.hideCube();
        }, timeToClose);
      }
    }

    this.electionData();

    // iplMsid = siteConfig.pages.iplHomePage;
    // params.msid = siteConfig.pages.iplHomePage;
    // dispatch(fetchTopListDataIfNeeded(params, query, router));
    //dispatch(fetchMiniScheduleDataIfNeeded(params, query, iplMsid));
    //this.config.refreshInterval = setInterval(() => {
    // self.refreshCube();
    // self.CubeGA();
    //dispatch(fetchMiniScheduleDataIfNeeded(params, query, iplMsid));
    // dispatch(fetchTopListDataIfNeeded(params, query, router));
    //}, this.config.timeInterval * 4);

    // if (!globalconfig.scorecardInterval) {
    // globalconfig.scorecardInterval = setInterval(() => {
    //   // dispatch(fetchMiniScheduleDataIfNeeded(params, query));
    // }, this.config.scorecardInterval);
    // }

    if (window) {
      this.config.href = window.location.href;
    }

    document
      .querySelector(".flipbox-box.flipbox-horizontal")
      .addEventListener("click", el => this.fireFaceEvent(el), false);
  }

  electionData() {
    const { election } = this.props;
    const resultURL = election && election.result && election.result.feedurl ? election.result.feedurl : null;
    if (resultURL) {
      this.fetchElectionResults(resultURL);
      this.config.refreshInterval = setInterval(() => {
        this.refreshCube();
        this.fetchElectionResults(resultURL);
      }, this.config.timeInterval * 4);
    }
  }

  fetchElectionResults(feedURL) {
    fetch(feedURL).then(data => {
      this.setState({ results: data });
    });
  }

  /**
   * Cube GA fired only once URL changes for Cube. Not on every full rotation
   * @param {*} prevProps
   */
  componentDidUpdate(prevProps) {
    /** when route change fire GA */
    let cube = document.querySelector(".Cube");
    if (window && this.config.href != window.location.href && cube && !cube.classList.contains("hide")) {
      // this.CubeGA();
      this.refreshCube();
      this.config.href = window.location.href;
    }
  }

  /**
   * This is used to rotate cube after set of interval
   * @param {*} cubeProp
   */
  RotateCube(cubeProp) {
    let self = this;
    const timeInterval = cubeProp.turntime;
    let { dispatch, params, query } = this.props;

    if (_isCSR()) {
      let cube = document.querySelector(".flipbox-box.flipbox-horizontal");

      if (cube && !cube.getAttribute("isRotateAttached")) {
        Ads_module.render({});

        let currentVal = 0;
        this.config.rotationInterval = setInterval(() => {
          currentVal -= 90;
          cube.style.transform = "rotateY(" + currentVal + "deg)";
        }, timeInterval);

        cube.setAttribute("isRotateAttached", true);
      }
    }
  }

  /**
   * This is used to refresh the ads and fetch top list news
   */
  refreshCube() {
    // For fetching top list news

    let arrAds = ["special1", "special2", "headAd1", "headAd2", "headAd3", "headAd4"];
    let arrAdsId = [];

    for (let item of arrAds) {
      let adsObj = document.querySelector("." + item);
      let id = adsObj.getAttribute("data-id");
      arrAdsId.push(id);
    }

    Ads_module.refreshAds([...arrAdsId]);
    // Ads_module.refreshAds(['special1', 'special2', 'headAd1', 'headAd2', 'headAd3', 'headAd4']);
  }

  /**
   * This method is used to fetch top 2 article stories from home PL list
   * @param {*} homeData
   */
  getHomeFeeds(homeData) {
    let arrItems = [];
    for (let item of homeData) {
      if (arrItems.length < 2) {
        if (item.mstype == "2") {
          arrItems.push(item);
        }
      } else {
        return arrItems;
      }
    }
  }

  teamScoreFill(teamId, item) {
    if (item["team" + teamId + "_score"] && item["team" + teamId + "_score"] != "") {
      return (
        <React.Fragment>
          <span className="score">{item["team" + teamId + "_score"].split(" (")[0]}</span>
          <span className="over">{item["team" + teamId + "_score"].split(" (")[1].split(")")[0]}</span>
        </React.Fragment>
      );
    } else {
      return null;
    }
  }

  getPlayerName(team, playerID) {
    let players, playerName;
    players = team.Players;
    playerName = players[playerID]["Name_Full"];
    return playerName;
  }

  scorecardWidget(item) {
    let status, tourname;
    if (item) {
      status = item.matchresult;
      tourname = item.series_short_display_name || "IPL 2021";

      return (
        <div className="dem">
          <AnchorLink
            data-type="scorecard"
            style={{ textDecoration: "none" }}
            hrefData={{
              override:
                siteConfig.mweburl +
                "/sports/cricket/live-score/" +
                item.teama_short.toLowerCase() +
                "-vs-" +
                item.teamb_short.toLowerCase() +
                "/" +
                item.matchdate_ist.replace(/\//g, "-") +
                "/scoreboard/matchid-" +
                item.matchfile +
                ".cms",
            }}
          >
            <div className="wdt_schedule floating_cube">
              {/* <div className="match_date_venue">{tourname}</div> */}
              <div className="liveScore table">
                <div className="table">
                  <div className="teama table_col">
                    <b className="country">{item.teama_short}</b>
                    <span className="teaminfo">
                      <ImageCard msid={_getTeamLogo(item.teama_Id, "square")} size="squarethumb" />
                      <span className="con_wrap">
                        {item.matchtype.toLowerCase() != "test" ? teamScore("A", item) : null}
                      </span>
                    </span>
                  </div>
                  <div className="versus table_col">
                    <span>VS</span>
                  </div>
                  <div className="teamb table_col">
                    <b className="country">{item.teamb_short}</b>
                    <span className="teaminfo">
                      <ImageCard msid={_getTeamLogo(item.teamb_Id, "square")} size="squarethumb" />
                      <span className="con_wrap">
                        {item.matchtype.toLowerCase() != "test" ? teamScore("B", item) : null}
                      </span>
                    </span>
                  </div>
                </div>
                <div className="match_final_status">
                  {item.live == "1" || item.live == "2" || item.live == "3" || item.live == "4" ? (
                    <span className="status_live">Live</span>
                  ) : status && status != "" ? (
                    <span className="text_ellipsis">{status}</span>
                  ) : null}
                </div>
              </div>
            </div>
          </AnchorLink>
          {/* {isMobilePlatform() ? (
            <a
              href={_deferredDeeplink(
                "campaign",
                siteConfig.appdeeplink,
                null,
                siteConfig.mweburl + "/scoreboard/matchid-" + item.matchfile + ".cms?frmapp=yes",
              )}
              target="_blank"
              className="open-app-with-ad"
            >
              <button className="open-in-app">Open in App</button>
            </a>
          ) : null} */}
        </div>
      );
    }
  }

  /**
   * For firing Cube GA events
   * @param {*} e
   */
  // CubeGA(e) {
  //     let cube = document.querySelector('.Cube');
  //     if (!cube.classList.contains('hide')) {
  //         AnalyticsGA.pageview('/cube.cms' + (++this.config.count));
  //     }
  // }

  /**
   * This is used externally also for hiding the cube and clear rotational intervals.
   * @param {*} onRouteChange
   */
  hideCube(onRouteChange) {
    let cube = document.querySelector(".Cube");
    let date = new Date();
    let time = date.getTime();
    let flipBox = document.querySelector(".flipbox-box.flipbox-horizontal");
    if (cube) {
      if (!cube.classList.contains("hide")) {
        cube.className += " hide";
      }

      clearInterval(this.config.rotationInterval);
      clearInterval(this.config.refreshInterval);
      cube.removeAttribute("isRotateAttached");

      if (flipBox) {
        flipBox.removeAttribute("isRotateAttached");
        flipBox.removeAttribute("style");
      }

      if (!onRouteChange) {
        _sessionStorage().set("closeTime", time);
        AnalyticsGA.event({ category: "Cube", action: "Tap", label: "Close" });
      }
    }
  }

  /**
   * This is used to fire aan event when any of the face clicked
   * @param {*} e
   */
  fireFaceEvent(e) {
    let faceValue;
    faceValue = e.target && e.target.id ? e.target.id : 1;
    // AnalyticsGA.event({
    //   category: "Cube",
    //   action: "Tap",
    //   label: "Face#" + faceValue,
    // });
  }

  render() {
    // ----------- For Home Feed data-----
    // let { home } = this.props;
    // let homeData =
    //   hasValue(home, "value.0.items") && this.props.home.value[0].items.length > 0
    //     ? this.props.home.value[0].items
    //     : null;

    // let arrHomeFeed = homeData && this.getHomeFeeds(homeData);
    // -----------Ends Home feed data

    // For cricket cube---------
    // let { minischedule } = this.props;

    // let tmpSchedule = minischedule && extractMiniSchedule(minischedule);
    // tmpSchedule["schedule"] = tmpSchedule.schedule && tmpSchedule.schedule.slice(0, 2);

    //-----------cricket cube End

    // ----------- Election Data starts -------
    let { election } = this.props;
    const { results } = this.state;

    // let { data } = props.data;
    // let allianceData = coustomizeFeedData(data);

    // allianceData._ttl_win_lead = 0;
    // allianceData._oth_win_lead = 0;

    // let partyData = electionresult && electionresult.data ? electionresult.data["Maharashtra"] : null;

    // let mhData = electionresult && electionresult.data ? electionresult.data["Maharashtra"] : null;
    // let hrData = electionresult && electionresult.data ? electionresult.data["Haryana"] : null;
    let totalWBSeat = 0;
    let totalTNSeat = 0;

    let allianceListWB = ["CPM+", "BJP", "TMC", "OTH"];
    let allianceListTN = ["DMK+", "AIADMK+", "AMMK+", "MNM"];
    // let partyList = ["AITC", "INC", "CPM", "BJP"];
    let elecData = results ? results["West_Bengal"] : null;
    let tmlElecData = results ? results["Tamil_Nadu"] : null;
    this.config.mapMSID = _electionConfig.mapResultMSID;

    // -------------- Election data ends-------------

    return (
      <div className="Cube hide">
        <div id="content">
          <span className="close_icon" onClick={() => this.hideCube()} />
          <div id="box1" className="box-cube flipbox-wrapper">
            <div className="flipbox-box flipbox-horizontal">
              <div className="flipbox-side flipbox-front" id="1">
                <div className="cube_top_ad">
                  <div
                    data-adtype="headAd3"
                    className="ad1 headAd3"
                    data-id="div-gpt-ad-1540206148700-cube-22"
                    data-name={siteConfig.ads.dfpads.cubeheadad3}
                    data-size="[[195, 30]]"
                  />
                </div>

                {/* ----------score card cube---- */}
                {/* {tmpSchedule && tmpSchedule.schedule ? this.scorecardWidget(tmpSchedule.schedule[0]) : null} */}
                {/* ---- End score card---------- */}

                {/* ---------- show home feed data-------- */}
                {/* {arrHomeFeed && arrHomeFeed.length >= 2 ? (
                  <h4 className="news-txt">
                    <AnchorLink
                      className="text_ellipsis"
                      target="_blank"
                      href={
                        "/" +
                        arrHomeFeed[0].seolocation +
                        "/articleshow/" +
                        arrHomeFeed[0].id +
                        ".cms" +
                        "?utm_source=cube&utm_medium=referral&utm_campaign=article1"
                      }
                    >
                      {arrHomeFeed[0].hl}
                    </AnchorLink>
                  </h4>
                ) : null} */}
                {/* ------------ home feed data end ------------ */}

                {/* -------- Election Data starts here------------ */}

                {/* <AnchorLink href={`/elections/assembly-elections/bihar/results/${this.config.mapMSID["bihar"]}.cms`}> */}
                <div className="resultTable-cube">
                  <div className="election_result_table">
                    <div className="ele_caption clearfix">
                      <h3>Live</h3>
                      <h6>West Bengal</h6>
                    </div>
                    <div className="result_table">
                      <ul>
                        {elecData &&
                          elecData.pg_rslt &&
                          elecData.pg_rslt[0] &&
                          elecData.pg_rslt[0].length &&
                          elecData.pg_rslt[0].map((item, i) => {
                            //calculate total win+lead
                            if (allianceListWB.indexOf(item.an) > -1) {
                              totalWBSeat += parseInt(item.ws) + parseInt(item.ls);
                              let _style = { backgroundColor: item.cc };
                              return (
                                <li style={_style}>
                                  {item.an} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                                </li>
                              );
                            }
                          })}
                      </ul>
                      {elecData && elecData.ttl_seat ? (
                        <div className="bottomLbl">
                          <ul className="table_tot">
                            <li>{`${totalWBSeat} / ${elecData.ttl_seat}`}</li>
                            {election && election.result && election.result.counttext && (
                              <li>{election.result.counttext}</li>
                            )}
                          </ul>
                          <p>
                            {_electionConfig.majorityMark}{" "}
                            <strong>: {Math.floor(parseInt(elecData.ttl_seat) / 2) + 1}</strong>
                            <span>Source: C-Voter</span>
                          </p>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                {/* </AnchorLink> */}

                {/* -------------- Election Data Ends--------- */}
              </div>

              <div
                className="flipbox-side flipbox-back"
                style={{ transform: "translateZ(-195px) rotateY(180deg)" }}
                id="3"
              >
                <div className="cube_top_ad">
                  <div
                    data-adtype="headAd4"
                    className="ad1 headAd4"
                    data-id="div-gpt-ad-1540206148700-cube-23"
                    data-name={siteConfig.ads.dfpads.cubeheadad4}
                    // //data-mlb="[[195, 30]]"
                    data-size="[[195, 30]]"
                  />
                </div>
                {/* ----------score card cube---- */}
                {/* {tmpSchedule && tmpSchedule.schedule ? this.scorecardWidget(tmpSchedule.schedule[0]) : null} */}
                {/* ---- End score card---------- */}

                {/* ---------- show home feed data-------- */}
                {/* {arrHomeFeed && arrHomeFeed.length >= 2 ? (
                  <h4 className="news-txt">
                    <AnchorLink
                      className="text_ellipsis"
                      target="_blank"
                      href={
                        "/" +
                        arrHomeFeed[1].seolocation +
                        "/articleshow/" +
                        arrHomeFeed[1].id +
                        ".cms" +
                        "?utm_source=cube&utm_medium=referral&utm_campaign=article2"
                      }
                    >
                      {arrHomeFeed[1].hl}
                    </AnchorLink>
                  </h4>
                ) : null} */}
                {/* ------------ home feed data end ------------ */}
                {/* -------- Election Data starts here------------ */}

                {/* <AnchorLink href={`/elections/assembly-elections/bihar/results/${this.config.mapMSID["bihar"]}.cms`}> */}
                <div className="resultTable-cube">
                  <div className="election_result_table">
                    <div className="ele_caption clearfix">
                      <h3>Live</h3>
                      <h6>Tamil Nadu</h6>
                    </div>
                    <div className="result_table">
                      <ul>
                        {tmlElecData &&
                          tmlElecData.pg_rslt &&
                          tmlElecData.pg_rslt[0] &&
                          tmlElecData.pg_rslt[0].length &&
                          tmlElecData.pg_rslt[0].map((item, i) => {
                            //calculate total win+lead
                            if (allianceListTN.indexOf(item.an) > -1) {
                              totalTNSeat += parseInt(item.ws) + parseInt(item.ls);
                              let _style = { backgroundColor: item.cc };
                              return (
                                <li style={_style}>
                                  {item.an} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                                </li>
                              );
                            }
                          })}
                      </ul>
                      {tmlElecData && tmlElecData.ttl_seat ? (
                        <div className="bottomLbl">
                          <ul className="table_tot">
                            <li>{`${totalTNSeat} / ${tmlElecData.ttl_seat}`}</li>
                            {election && election.result && election.result.counttext && (
                              <li>{election.result.counttext}</li>
                            )}
                          </ul>
                          <p>
                            {_electionConfig.majorityMark}{" "}
                            <strong>: {Math.floor(parseInt(tmlElecData.ttl_seat) / 2) + 1}</strong>
                            <span>Source: C-Voter</span>
                          </p>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>
                {/* </AnchorLink> */}

                {/* -------------- Election Data Ends--------- */}
              </div>
              <div
                className="flipbox-side flipbox-left"
                style={{ transform: "rotateY(270deg) translateX(-195px)" }}
                id="4"
              >
                <div className="cube_top_ad">
                  <div
                    data-adtype="headAd1"
                    className="ad1 headAd1"
                    data-id="div-gpt-ad-1540206148700-cube-20"
                    data-name={siteConfig.ads.dfpads.cubeheadad1}
                    data-size="[[195, 30]]"
                  />
                </div>
                <div
                  data-adtype="special1"
                  className="ad1 special1"
                  data-id="div-gpt-ad-1540206148700-cube-12"
                  data-name={siteConfig.ads.dfpads.cubead2}
                  data-size="[[195, 85]]"
                />

                {/* ---------- show home feed data-------- */}
                {/* {arrHomeFeed && arrHomeFeed.length >= 2 ? (
                  <h4 className="news-txt">
                    <AnchorLink
                      className="text_ellipsis"
                      target="_blank"
                      href={
                        "/" +
                        arrHomeFeed[0].seolocation +
                        "/articleshow/" +
                        arrHomeFeed[0].id +
                        ".cms" +
                        "?utm_source=cube&utm_medium=referral&utm_campaign=article1"
                      }
                    >
                      {arrHomeFeed[0].hl}
                    </AnchorLink>
                  </h4>
                ) : null} */}
                {/* ------------ home feed data end ------------ */}
              </div>
              <div
                className="flipbox-side flipbox-right"
                style={{ transform: "rotateY(-270deg) translateX(195px)" }}
                id="2"
              >
                <div className="cube_top_ad">
                  <div
                    data-adtype="headAd2"
                    className="ad1 headAd2"
                    data-id="div-gpt-ad-1540206148700-cube-21"
                    data-name={siteConfig.ads.dfpads.cubeheadad2}
                    data-size="[[195, 30]]"
                  />
                </div>
                <div
                  data-adtype="special2"
                  className="ad1 special2"
                  data-id="div-gpt-ad-1540206148700-cube-11"
                  data-name={siteConfig.ads.dfpads.cubead1}
                  // //data-mlb="[[320, 100], [320, 50]]"
                  data-size="[[195, 85]]"
                />

                {/* ---------- show home feed data-------- */}
                {/* {arrHomeFeed && arrHomeFeed.length >= 2 ? (
                  <h4 className="news-txt">
                    <AnchorLink
                      className="text_ellipsis"
                      target="_blank"
                      href={
                        "/" +
                        arrHomeFeed[1].seolocation +
                        "/articleshow/" +
                        arrHomeFeed[1].id +
                        ".cms" +
                        "?utm_source=cube&utm_medium=referral&utm_campaign=article2"
                      }
                    >
                      {arrHomeFeed[1].hl}
                    </AnchorLink>
                  </h4>
                ) : null} */}
                {/* ------------ home feed data end ------------ */}

                {/* {isMobilePlatform() ? (
                  <a
                    href={_deferredDeeplink(
                      "campaign",
                      siteConfig.appdeeplink,
                      null,
                      siteConfig.mweburl +
                        "/elections/assembly-elections/results/" +
                        _electionConfig.assemblyResultsMSID +
                        ".cms?frmapp=yes",
                    )}
                    target="_blank"
                    className="open-app-with-ad"
                  >
                    <button className="open-in-app">Open in App</button>
                  </a>
                ) : null} */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    minischedule: state.minischedule,
    results: state.results,
    home: state.home,
    election: state.app && state.app.election,
  };
}

export default connect(mapStateToProps)(Cube);
