import React from "react";
// import PropTypes from "prop-types";
import SvgIcon from "./SvgIcon";
import AnchorLink from "./AnchorLink";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const FacebookLikeCard = () => {
  const { fb } = siteConfig;
  return (
    <div className="con_facebooklike item_drawer" data-exclude="amp">
      <div className="label">
        <h4
          dangerouslySetInnerHTML={{
            __html: siteConfig.locale.fblikecardtext,
          }}
        ></h4>
      </div>
      <AnchorLink href={fb} title="facebook" className="fb-like">
        <span>
          <SvgIcon name="facebook" />
          Like
        </span>
      </AnchorLink>
      {/* <a target="_blank" href="#" className="btn-draweralert">LIKE</a> */}
      <span className="bg_circle">
        <img className="portal_icon" alt="Hindi News" title="Hindi News" src={siteConfig.logo_id} />
      </span>
    </div>
  );
};

FacebookLikeCard.propTypes = {};

function arePropsEqual(prevProps, nextProps) {
  return true;
}

// React memo is used to stop rerendering of the component if the props remain same
export default React.memo(FacebookLikeCard, arePropsEqual);
