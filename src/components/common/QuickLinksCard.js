import React, { Component } from "react";
import { connect } from "react-redux";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

import { _isCSR } from "./../../utils/util";
import styles from "./css/footer.scss";
import { fetchQuickLinksDataIfNeeded } from "../../actions/footer/footer";
import AnchorLink from "./AnchorLink";

class QuickLinksCard extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, pwaMeta } = this.props;
    let params = {};
    let pathname = this.props.routing.locationBeforeTransitions.pathname;
    //console.log('pwaMeta in quickLinks component',pwaMeta.qlinks);
    // params.msid =
    //   pathname && pathname != "/" ? pathname.slice(pathname.lastIndexOf("/") + 1, pathname.indexOf(".cms")) : "";
    //msid should be valid id(number)
    // params.msid = parseInt(params.msid);
    params.msid = pwaMeta && pwaMeta.qlinks;
    if (params.msid) dispatch(fetchQuickLinksDataIfNeeded(params));
  }

  // componentWillReceiveProps(nextProps) {
  //   let currentpathname = this.props.routing.locationBeforeTransitions.pathname;
  //   let nextpathname = nextProps.routing.locationBeforeTransitions.pathname;
  //   if (currentpathname != nextpathname) {
  //     const { dispatch } = nextProps;
  //     let params = {};
  //     params.msid =
  //       nextpathname && nextpathname != "/"
  //         ? nextpathname.slice(nextpathname.lastIndexOf("/") + 1, nextpathname.indexOf(".cms"))
  //         : "";
  //     //msid should be valid id(number)
  //     params.msid = parseInt(params.msid);
  //     if (params.msid) dispatch(fetchQuickLinksDataIfNeeded(params));
  //   }
  // }

  render() {
    let { quick_links } = this.props.footer;
    return quick_links && quick_links.items && quick_links.items.length > 0 ? (
      <React.Fragment>
        <h3 className="head">
          <span>{quick_links.title ? quick_links.title : "Quick Links"}</span>
        </h3>
        <div className="scroll_content">
          {quick_links.items.map((item, index) => {
            return (
              <AnchorLink key={index} hrefData={{ override: item.override }}>
                {item.hl}
              </AnchorLink>
            );
          })}
        </div>
      </React.Fragment>
    ) : null;
  }
}

function mapStateToProps(state) {
  return {
    footer: state.footer,
    routing: state.routing,
  };
}

export default connect(mapStateToProps)(QuickLinksCard);
