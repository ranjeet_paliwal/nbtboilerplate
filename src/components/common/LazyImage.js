import React, { Component } from "react";
import PropTypes from "prop-types";
import { throttle } from "./../../utils/util";

function registerListener(event, fn) {
  if (window.addEventListener) {
    window.addEventListener(event, fn);
  }
}

function isInViewport(el) {
  if (!el) return false;
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

const fadeIn = `
  @keyframes gracefulimage {
    0%   { opacity: 0.25; }
    50%  { opacity: 0.5; }
    100% { opacity: 1; }
  }
`;

class LazyImage extends Component {
  constructor(props) {
    super(props);
    // store a reference to the throttled function
    this.throttledFunction = throttle(this.lazyLoad, 150, this);

    this.state = {
      loaded: false,
    };
  }

  /*
    Creating a stylesheet to hold the fading animation
  */
  addAnimationStyles() {
    const exists = document.head.querySelectorAll("[data-gracefulimage]");

    if (!exists.length) {
      const styleElement = document.createElement("style");
      styleElement.setAttribute("data-gracefulimage", "exists");
      document.head.appendChild(styleElement);
      styleElement.sheet.insertRule(fadeIn, styleElement.sheet.cssRules.length);
    }
  }

  /*
    Attempts to download an image, and tracks its success / failure
  */
  loadImage() {
    const image = new Image();
    image.onload = () => {
      this.setState({ loaded: true });
    };
    image.onerror = () => {
      this.setState({ loaded: false });
    };
    image.src = this.props.src;
  }

  /*
    If placeholder is currently within the viewport then load the actual image
    and remove all event listeners associated with it
  */
  lazyLoad = () => {
    if (isInViewport(this.placeholderImage)) {
      this.clearEventListeners();
      this.loadImage();
    }
  };

  /*
    Attempts to load an image src passed via props
    and utilises image events to track sccess / failure of the loading
  */
  componentDidMount() {
    this.addAnimationStyles();

    // if user wants to lazy load
    if (!this.props.noLazyLoad) {
      // check if already within viewport to avoid attaching listeners
      if (isInViewport(this.placeholderImage)) {
        this.loadImage();
      } else {
        //registerListener("load", this.throttledFunction);
        registerListener("scroll", this.throttledFunction);
      }
    } else {
      this.loadImage();
    }
  }

  clearEventListeners() {
    //window.removeEventListener("load", this.throttledFunction);
    window.removeEventListener("scroll", this.throttledFunction);
  }

  /*
    clear any existing event listeners
  */
  componentWillUnmount() {
    this.clearEventListeners();
  }

  /*
    - If image hasn't yet loaded AND user didn't want a placeholder then don't render anything
    - Else if image has loaded then render the image
    - Else render the placeholder
  */
  render() {
    if (!this.state.loaded && this.props.noPlaceholder) return null;

    const src = this.state.loaded ? this.props.src : this.props.placeholder;
    const style = !this.state.loaded ? { backgroundImage: "url(" + this.props.placeholder + ")" } : "";

    return (
      <img
        src={src}
        className={this.props.className}
        width={this.props.width}
        height={this.props.height}
        style={{
          ...style,
          ...this.props.style,
        }}
        alt={this.props.alt}
        ref={this.state.loaded ? null : ref => (this.placeholderImage = ref)}
      />
    );
  }
}

LazyImage.defaultProps = {
  src: null,
  className: null,
  width: null,
  height: null,
  alt: "NBT",
  style: {},
  placeholder: null,
  noPlaceholder: false,
  noLazyLoad: false,
};

LazyImage.propTypes = {
  src: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  alt: PropTypes.string,
  style: PropTypes.object,
  noPlaceholder: PropTypes.bool,
  noLazyLoad: PropTypes.bool,
};

export default LazyImage;
