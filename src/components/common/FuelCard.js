import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./css/UtilityWidgets.scss";

import { _isCSR } from "../../utils/util";
import { fetchFuelDataIfNeeded } from "../../actions/app/app";
import SvgIcon from "./SvgIcon";

class FuelCard extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(fetchFuelDataIfNeeded());
    }
  }

  shouldComponentUpdate(nextProps) {
    return nextProps !== this.props;
  }

  render() {
    const { fuel, selectedCity } = this.props;
    let fuelPrice = {};
    if (typeof fuel !== "undefined" && fuel.data && selectedCity) {
      fuelPrice = fuel.data.filter(cityObj => cityObj.city.toUpperCase() === selectedCity.toUpperCase());
    }

    if (!_isCSR()) {
      return null;
    }
    return (
      <div className="utility-widget">
        {/* <h3>{siteConfig.fuel.heading}</h3> */}
        <div className="table">
          <div className="table_row">
            <div className="table_col iconcol">
              <SvgIcon name="fuel" width="31" height="37" />
            </div>
            <div className="table_col fueldetails">
              <div className="text">Petrol</div>
              <div className="value">
                <span>{fuelPrice && fuelPrice[0] && fuelPrice[0].Petrol ? fuelPrice[0].Petrol : "-"}</span> Rs/L
              </div>
            </div>
            <div className="table_col fueldetails lastcol">
              <div className="text">Diesel</div>
              <div className="value">
                <span>{fuelPrice && fuelPrice[0] && fuelPrice[0].Diesel ? fuelPrice[0].Diesel : "-"}</span> Rs/L
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  fuel: state.app.fuel,
});

FuelCard.propTypes = {
  fuel: PropTypes.object,
};

export default connect(mapStateToProps)(FuelCard);
