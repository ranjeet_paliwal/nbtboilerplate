import React from "react";
// import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
// import styles from "./css/ArticleShow.scss";
import AnchorLink from "./AnchorLink";
import ImageCard from "./ImageCard/ImageCard";
import { _getStaticConfig, _isCSR, _getUrlOfCategory } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const siteconfig = siteConfig;

const GadgetCompare = props => {
  let GadgetCompare = props;
  let category = GadgetCompare && GadgetCompare.length > 0 ? _getUrlOfCategory(GadgetCompare[0].category) : "";
  var filtered = siteConfig.locale.tech.category.filter(function(item) {
    return item.keyword == category;
  });
  return (
    <div className="comparison_list">
      <h2>
        <span>
          {" "}
          {siteConfig.locale.tech.popular +
            " " +
            filtered[0].keylabel +
            " " +
            siteConfig.locale.tech.ki +
            " " +
            siteConfig.locale.tech.comparetxt}{" "}
        </span>
      </h2>
      <ul>
        {GadgetCompare.map((item, index) => {
          return (
            <li>
              {
                <AnchorLink href={`/tech/compare-${category}/${item._id}`}>
                  <div className="single-gadget">
                    <span className="img_wrap">
                      <ImageCard noLazyLoad={!_isCSR()} msid={item.imageMsid[0]} size="gnthumb" />
                    </span>
                    <span className="txt">
                      <span className="text_ellipsis">{item.product_name[0]}</span>{" "}
                    </span>
                  </div>
                  <div className="single-gadget">
                    <span className="img_wrap">
                      <ImageCard noLazyLoad={!_isCSR()} msid={item.imageMsid[1]} size="gnthumb" />
                    </span>
                    <span className="txt">
                      <span className="text_ellipsis">{item.product_name[1]}</span>
                    </span>
                  </div>
                  {item.imageMsid[2] && item.product_name[2] ? (
                    <div className="single-gadget">
                      <span className="img_wrap">
                        <ImageCard noLazyLoad={!_isCSR()} msid={item.imageMsid[2]} size="gnthumb" />
                      </span>
                      <span className="txt">
                        <span className="text_ellipsis">{item.product_name[2]}</span>
                      </span>
                    </div>
                  ) : null}
                </AnchorLink>
              }
            </li>
          );
        })}
      </ul>
      <a className="more-btn" href={`${siteConfig.mweburl}/tech/compare-${category}/popular-comparisons`}>
        {siteConfig.locale.tech.seemore}
      </a>
    </div>
  );
};

export default GadgetCompare;
