import React from "react";
import styles from "./css/commonComponents.scss";

const TabContainer = props => {
  let tabs = props;
  let isExtraEle =
    tabs.children[tabs.children.length - 1] == null ? true : false;
  return !isExtraEle && tabs.children.length > 1 ? (
    <div>{getTabs(tabs.children, tabs.rel)}</div>
  ) : (
    <div>{tabs.children}</div>
  );
};

export default TabContainer;

const getTabs = (data, rel) => {
  return (
    <div className={styles["tabs_box"] + " tabs_box"}>
      <ul className={styles["tabs_box_list"] + " tabs_box_list"}>
        {data.map((item, index) => {
          return (
            <li
              className={item.props.tab == "active" ? "active" : ""}
              onClick={e => tabclick(rel + "" + index, rel)}
              key={index}
              rel={rel + "" + index}
            >
              {item.props.label}
            </li>
          );
        })}
      </ul>
      <div data-attr={rel + "tabCon"}>
        {data.map((item, index) => {
          return (
            <div
              className={
                rel +
                "container" +
                (item.props.tab == "active" ? " active" : "")
              }
              key={index + "_con"}
              id={rel + "" + index}
            >
              {item}
            </div>
          );
        })}
      </div>
    </div>
  );
};

const tabclick = (ele, rel) => {
  let con_tabs = document.querySelectorAll("." + rel + "container");
  let list_tabs = document.querySelectorAll(".tabs_box_list li");
  for (let i = 0; i < con_tabs.length; i++) {
    con_tabs[i].classList.remove("active"); // Hide all Containers
    list_tabs[i].classList.remove("active"); // Remove Active class form all tabs
  }

  document.querySelector('[rel="' + ele + '"]').classList.add("active"); //Add active class for current tabs
  document
    .querySelector('[data-attr = "' + rel + 'tabCon"]' + " #" + ele)
    .classList.add("active"); //Add active class for current Container
};
