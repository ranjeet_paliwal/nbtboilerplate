import React, { Component } from "react";
import fetch from "utils/fetch/fetch";
import PropTypes from "prop-types";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";

import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import FakeListing from "./../../components/common/FakeCards/FakeListing";
import styles from "./../../components/common/css/GadgetNow.scss";
import { _getStaticConfig, _isCSR, _getCategory } from "../../utils/util";
import { fetchBrandListDataIfNeeded } from "./../../actions/articlelist/articlelist";

const siteConfig = _getStaticConfig();

class GadgetBrandWidget extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    let { router, pagetype, _category } = this.props;
    let category =
      this.props.category == ""
        ? _category || _getCategory(this.props.router.location.pathname, this.props.params)
        : this.props.category;
    //if(category)this.setState({ category: category });
  }

  setCategory(obj) {
    // const { dispatch, params, query, router } = this.props;
    // let category=obj.currentTarget.id;
    // if(category)this.setState({ category: category });
    //return dispatch(fetchGadgetListDataIfNeeded(params, query,router,category));
  }
  selectedoption(event) {
    let { params, query, router, dispatch } = this.props;

    let category = event.target.value;
    //this.setState({ category: category });
    return dispatch(fetchBrandListDataIfNeeded(params, query, router, category));
  }
  render() {
    let _this = this;
    let category = siteConfig.locale.tech.category;
    let statecategory = this.props.category;

    let { brand } = this.props;
    return !this.props.isFetchingBrand ? (
      <div className="wdt_brand_list box-content" key="bradnwidgetkey">
        <div className="headingWithFilters">
          <div className="sectionHeading">
            <h2>
              <span>{this.props.category + " " + siteConfig.locale.tech.brands}</span>
            </h2>
          </div>
          <div className="change_device">
            <label>{siteConfig.locale.tech.change}</label>
            <select onChange={this.selectedoption.bind(this)}>
              {category.map((item, index) => {
                return (
                  <option
                    key={"categorybrand" + index}
                    selected={this.props.category == item.keyword ? "selected" : ""}
                    value={item.keyword}
                  >
                    {item.keylabel}
                  </option>
                );
              })}
            </select>
          </div>
        </div>

        <div className="gadget-amz-scroll">
          <ErrorBoundary>
            {brand && brand instanceof Array ? (
              <ul key="brandlistul">
                {brand.map((item, index) => {
                  return (
                    <li key={"barndlist" + index}>
                      <AnchorLink href={`/tech/${_this.props.categoryseo}/${item.uname}`}>
                        <div className="img_wrap">
                          <ImageCard noLazyLoad={!_isCSR()} msid={item.imageMsid} size="gnthumb" />
                        </div>
                        <div className="item_name">
                          <span className="text_ellipsis">{item.name}</span>
                        </div>
                      </AnchorLink>
                    </li>
                  );
                })}
              </ul>
            ) : (
              <FakeListing showImages={true} />
            )}
          </ErrorBoundary>
        </div>
      </div>
    ) : (
      <FakeListing showImages={true} />
    );
  }
}

GadgetBrandWidget.propTypes = {
  item: PropTypes.object,
};

export default GadgetBrandWidget;
