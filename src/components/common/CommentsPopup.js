import React, { Component } from "react";
import AdCard from "./AdCard";
import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();
class CommentsPopup extends Component {
  componentDidUpdate(prevProps) {
    const { showCommentsPopup } = this.props;
    const bodyElement = document.querySelector("body");
    // Add and remove class when popup opens/closes
    if (!prevProps.showCommentsPopup && showCommentsPopup) {
      bodyElement.classList.add("disable-scroll");
    } else if (prevProps.showCommentsPopup && !showCommentsPopup) {
      bodyElement.classList.remove("disable-scroll");
    }

    if (document.getElementById("comment-frame")) {
      document.getElementById("comment-frame").src += "";
    }
  }

  render() {
    const { showCommentsPopup, closeComments, msid, loggedIn, closeOnOverLay, isNotEu } = this.props;
    // const ifrmSrc = `https://langdev9350.indiatimes.com/comments_slider_react_v1.cms?${
    const ifrmSrc = `${siteConfig.weburl}/comments_slider_react_v1.cms?${
      msid ? `msid=${msid}` : ""
    }${`&isloggedin=${loggedIn}`}${isNotEu ? `&isnoteu=${isNotEu}` : ""}`;

    return showCommentsPopup ? (
      <div className="popup_comments">
        <div className="body_overlay" onClick={closeOnOverLay ? closeComments : () => null}></div>
        <div style={{ top: "5%", left: "25%", position: "fixed", padding: "5px", background: "#c5c5c5", zIndex: 9999 }}>
          <AdCard mstype="mrec1" adtype="dfp" />
        </div>
        <div style={{ right: 0 }} id="comments__body" className="comments-body">
          <span className="close_icon" onClick={closeComments} />
          <iframe
            src={ifrmSrc}
            height="100%"
            width="550"
            border="0"
            marginWidth="0"
            marginHeight="0"
            hspace="0"
            vspace="0"
            frameBorder="0"
            scrolling="no"
            align="center"
            title="iplwidget"
            allowTransparency="true"
            id="comment-frame"
          />
        </div>
      </div>
    ) : (
      ""
    );
  }
}

export default CommentsPopup;
