import React from "react";
import "./css/electionCube.scss";
import { _getStaticConfig, _isCSR, _deferredDeeplink, _sessionStorage } from "../../utils/util";
import { subscribe } from "../../utils/pubsub";
import { electionConfig } from "../../campaign/election/utils/config";
import Ads_module from "../lib/ads/index";
import { AnalyticsGA } from "../lib/analytics/index";
import { coustomizeFeedData } from "../../campaign/election/utils/util";

const siteConfig = _getStaticConfig();
const timeInterval = 6000;
const _electionConfig = electionConfig[process.env.SITE];

export const RotateCube = props => {
  if (_isCSR()) {
    const cube = document.querySelector(".flipbox-box.flipbox-horizontal");

    if (cube && !cube.getAttribute("isRotateAttached")) {
      // setInterval(() => {
      //     Ads_module.refreshAds(['special1']);
      // }, timeInterval * 4);
      // setInterval(() => {
      //     Ads_module.refreshAds(['special2']);
      // }, timeInterval * 5);

      Ads_module.render({});

      let currentVal = 0;
      setInterval(() => {
        cube.style.transform = `rotateY(${currentVal}deg)`;
        currentVal -= 90;
      }, timeInterval);

      cube.setAttribute("isRotateAttached", true);
    }
  }
};

subscribe("ElectionCubeGA", () => {
  if (
    _isCSR() &&
    election.result &&
    election.result.cube &&
    election.result.cube.show == "true" &&
    _sessionStorage().get("ResultPollCube") &&
    _sessionStorage().get("ResultPollCube") != "false"
  ) {
    Ads_module.refreshAds(["special1", "special2", "headAd1", "headAd2", "headAd3", "headAd4"]);
    AnalyticsGA.pageview("/elections/lok-sabha-elections/result/69253509.cms?cube=result");
  }
});

export const ElectionCube = props => {
  const { election } = props;
  const { data } = props.data;
  const allianceData = coustomizeFeedData(data);

  allianceData._ttl_win_lead = 0;
  allianceData._oth_win_lead = 0;

  const partyData = coustomizeFeedData(data, "partywise");

  partyData._ttl_win_lead = 0;
  partyData._oth_win_lead = 0;

  const hideCube = () => {
    if (document.querySelector(".election_cube")) {
      document.querySelector(".election_cube").className += " hide";
      _sessionStorage().set("ResultPollCube", false);
    }
  };

  return (
    <div className="election_cube">
      <div id="content">
        <span className="close_icon" onClick={hideCube}></span>
        <div id="box1" className="box-cube flipbox-wrapper">
          <div className="flipbox-box flipbox-horizontal">
            <div className="flipbox-side flipbox-front">
              <div className="top_ad">
                {/* <span className="txt">Sponsored by</span>
                            <span className="ad"></span> */}
                <div
                  data-adtype="headAd1"
                  className="ad1 special3"
                  data-id="div-gpt-ad-1540206148700-20"
                  data-name={siteConfig.ads.dfpads.cubeheadad}
                  // //data-mlb="[[195, 30]]"
                  data-size="[[195, 30]]"
                />
              </div>
              <a
                href={_deferredDeeplink(
                  "campaign",
                  siteConfig.appdeeplink,
                  null,
                  `${siteConfig.mweburl}/elections/lok-sabha-elections/results/${_electionConfig.loksabhaResultsMSID}.cms?frmapp=yes`,
                )}
                target="_blank"
                className="bottom_cube_content"
              >
                <div className="resultTable-cube">
                  <div className="election_result_table">
                    <div className="ele_caption clearfix">
                      <h3>Live</h3>
                      <h6>Lok Sabha {allianceData && allianceData.items ? allianceData.year : 2013}</h6>
                    </div>
                    <div className="result_table">
                      <ul>
                        {allianceData &&
                          allianceData.items &&
                          allianceData.items.length &&
                          allianceData.items.map((item, i) => {
                            // calculate total win+lead
                            allianceData._ttl_win_lead += parseInt(item.ws) + parseInt(item.ls);
                            const _style = {
                              backgroundColor: allianceData.master[allianceData.master_index[item.nm]].cc,
                            };
                            return (
                              <li style={_style}>
                                {item.nm} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                              </li>
                            );
                          })}
                      </ul>
                      <ul className="table_tot">
                        <li>
                          {allianceData && allianceData.items ? allianceData._ttl_win_lead : 0}
                          /543
                        </li>
                        {election && election.result.counttext && <li>{election.result.counttext}</li>}
                      </ul>
                      <p>
                        {_electionConfig.majorityMark}
                        <strong>: {Math.floor(543 / 2) + 1}</strong>
                      </p>
                      <button className="open-in-app">Open in App</button>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="flipbox-side flipbox-back" style={{ transform: "translateZ(-195px) rotateY(180deg)" }}>
              <div className="top_ad">
                {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                <div
                  data-adtype="headAd2"
                  className="ad1 special3"
                  data-id="div-gpt-ad-1540206148700-21"
                  data-name={siteConfig.ads.dfpads.cubeheadad}
                  // //data-mlb="[[195, 30]]"
                  data-size="[[195, 30]]"
                />
              </div>
              <a
                href={_deferredDeeplink(
                  "campaign",
                  siteConfig.appdeeplink,
                  null,
                  `${siteConfig.mweburl}/elections/lok-sabha-elections/results/${_electionConfig.loksabhaResultsMSID}.cms?frmapp=yes`,
                )}
                target="_blank"
                className="bottom_cube_content"
              >
                <div className="resultTable-cube">
                  <div className="election_result_table">
                    <div className="ele_caption clearfix">
                      <h3>Live</h3>
                      <h6>Lok Sabha 2019</h6>
                    </div>
                    <div className="result_table">
                      <ul>
                        {partyData &&
                          partyData.items &&
                          partyData.items.length &&
                          partyData.items.map((item, i) => {
                            // calculate total win+lead
                            partyData._ttl_win_lead += parseInt(item.ws) + parseInt(item.ls);
                            const _style = {
                              backgroundColor: partyData.master[partyData.master_index[item.nm]].cc,
                            };
                            return (
                              <li style={_style}>
                                {item.nm} <strong>{parseInt(item.ws) + parseInt(item.ls)}</strong>
                              </li>
                            );
                          })}
                      </ul>
                      <ul className="table_tot">
                        <li>
                          {partyData && partyData.items ? partyData._ttl_win_lead : 0}
                          /543
                        </li>
                        {election && election.result.counttext && <li>{election.result.counttext}</li>}
                      </ul>
                      <p>
                        {_electionConfig.majorityMark} <strong>: {Math.floor(543 / 2) + 1}</strong>
                      </p>
                      <button className="open-in-app">Open in App</button>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="flipbox-side flipbox-left" style={{ transform: "rotateY(270deg) translateX(-195px)" }}>
              <div className="top_ad">
                {/* <span className="txt">Sponsored by</span>
                                    <span className="ad"></span> */}
                <div
                  data-adtype="headAd3"
                  className="ad1 special3"
                  data-id="div-gpt-ad-1540206148700-22"
                  data-name={siteConfig.ads.dfpads.cubeheadad}
                  // //data-mlb="[[195, 30]]"
                  data-size="[[195, 30]]"
                />
              </div>
              <div
                data-adtype="special1"
                className="ad1 special"
                data-id="div-gpt-ad-1540206148700-10"
                data-name={siteConfig.ads.dfpads.cubead}
                // //data-mlb="[[320, 100], [320, 50]]"
                data-size="[[195, 85]]"
              />

              <a
                href={_deferredDeeplink(
                  "campaign",
                  siteConfig.appdeeplink,
                  null,
                  `${siteConfig.mweburl}/elections/lok-sabha-elections/results/${_electionConfig.loksabhaResultsMSID}.cms?frmapp=yes`,
                )}
                target="_blank"
                className="open-app-with-ad"
              >
                <button className="open-in-app">Open in App</button>
              </a>
            </div>
            <div className="flipbox-side flipbox-right" style={{ transform: "rotateY(-270deg) translateX(195px)" }}>
              <div className="top_ad">
                <div
                  data-adtype="headAd4"
                  className="ad1 special3"
                  data-id="div-gpt-ad-1540206148700-23"
                  data-name={siteConfig.ads.dfpads.cubeheadad}
                  // //data-mlb="[[195, 30]]"
                  data-size="[[195, 30]]"
                />
              </div>
              <div
                data-adtype="special2"
                className="ad1 special"
                data-id="div-gpt-ad-1540206148700-11"
                data-name={siteConfig.ads.dfpads.cubead}
                // //data-mlb="[[320, 100], [320, 50]]"
                data-size="[[195, 85]]"
              />

              <a
                href={_deferredDeeplink(
                  "campaign",
                  siteConfig.appdeeplink,
                  null,
                  `${siteConfig.mweburl}/elections/lok-sabha-elections/results/${_electionConfig.loksabhaResultsMSID}.cms?frmapp=yes`,
                )}
                target="_blank"
                className="open-app-with-ad"
              >
                <button className="open-in-app">Open in App</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default { ElectionCube, RotateCube };
