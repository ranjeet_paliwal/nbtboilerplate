import React, { Component } from "react";
import style from "./css/networkstatus.css";
class NetworkMessage extends Component {
  render() {
    let msg = "Currently you are in offline mode";
    if (window) {
      if (window.location.href.indexOf("allvideolist.cms") >= 0) {
        msg += ". This feature is not available in offline mode";
      }
    }
    return <div className={style.networkStatus}>{msg}</div>;
  }
}

export default NetworkMessage;
