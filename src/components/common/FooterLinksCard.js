import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchLinksDataIfNeeded } from "../../actions/footerlinks/footerlinks";
import AnchorLink from "./AnchorLink";
class FooterLinksCard extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, fotterData } = this.props;
    let params = {};
    params.msid = fotterData;
    if (params.msid) dispatch(fetchLinksDataIfNeeded(params));
  }

  render() {
    let { footer_links } = this.props.footerlinks;
    return footer_links && footer_links.items && footer_links.items.length > 0 ? (
      <React.Fragment>
        <div className="scroll_content">
          {footer_links.items.map((item, index) => {
            return (
              <AnchorLink key={index} hrefData={{ override: item.override }}>
                {item.hl}
              </AnchorLink>
            );
          })}
        </div>
      </React.Fragment>
    ) : null;
  }
}

function mapStateToProps(state) {
  return {
    footerlinks: state.footerlinks,
    routing: state.routing,
  };
}

export default connect(mapStateToProps)(FooterLinksCard);
