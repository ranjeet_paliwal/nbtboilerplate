import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/PhotoMazzaCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";
import { SeoSchema } from "./PageMeta";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

class PhotoMazzaCard extends Component {
  constructor(props) {
    super(props);
  }

  showListingIcon(item) {
    let icon = item.tn == "video" ? "video_icon" : item.tn == "photo" ? "photo_icon" : "";
    if (icon == "") return;
    return <span className={"type_icon " + icon}></span>;
  }

  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return item.override ? item.override : url;
  }

  getAnchorLink(url) {
    //returning only url
    return <AnchorLink getUrl={true} hrefData={url} />;
  }

  render() {
    //When Single Column layout required add class "singleCol"
    let { item, leadpost, position, singleCol } = this.props;
    let postUrl = item.override
      ? { override: item.override }
      : {
          seo: item.seolocation,
          tn: item.tn,
          id: item.tn == "photo" ? (item.id && item.id != "" ? item.id : item.imageid) : item.imageid,
        };

    return (
      <li
        className={
          (leadpost ? [styles["photolistview"], styles["lead-post"]].join(" ") : styles["photolistview"]) +
          " animated fadeIn table nbt-listview photolistview " +
          (leadpost ? "lead-post" : "") +
          (singleCol ? " singleCol" : "")
        }
        key={item.id}
        {...SeoSchema({ pagetype: "photolist" }).attr().listItem}
      >
        <AnchorLink hrefData={postUrl} className="table_row">
          {/* <Link to={this.generateUrl(item)} className="table_row" itemProp="item"> */}
          <span className={"table_col img_wrap " + (leadpost ? "photolead_img_warpper" : null)}>
            <ImageCard
              msid={item.imageid ? item.imageid : item.id}
              title={item.seotitle ? item.seotitle : ""}
              size={leadpost ? "bigimage" : "midthumb"}
            />
            {this.showListingIcon(item)}
          </span>
          {/* <span>{this.getAnchorLink(postUrl)}</span> */}
          <span className={styles["con_wrap"] + " table_col con_wrap"}>
            <span className="text_ellipsis" itemProp="name">
              {item.hl}
            </span>
            {/* for Schema only */ SeoSchema().metaTag({
              name: "position",
              content: position,
            })}
            {/* for Schema */ SeoSchema().metaTag({
              name: "url",
              content: this.getAnchorLink(postUrl),
            })}
            {/*
              <span className={styles["time-caption"] + " time-caption"} data-time={item.lu ? item.lu : (item.dl ? item.dl : "")}>
                {item.lu ? item.lu : (item.dl ? item.dl : "")}
              </span>
              */}
          </span>
        </AnchorLink>
      </li>
    );
  }
}

PhotoMazzaCard.propTypes = {
  item: PropTypes.object,
};

export default PhotoMazzaCard;
