import React from "react";

import Parser from "html-react-parser";
import { parserOptionsLB } from "./../lib/htmlReactParser/index";
import ImageCard from "./ImageCard/ImageCard";
import { getImageParameters } from "./ImageCard/ImageCardUtils";
import AdCard from "./AdCard";
import { _dateFormatter, isMobilePlatform } from "./../../utils/util";
import VideoPlayer from "../../modules/videoplayer/player";

import "./../../components/common/css/liveblog.scss";
import commonstyles from "./../../components/common/css/commonComponents.scss";
import { SeoSchema } from "../../components/common/PageMeta";

import { _getStaticConfig, _isOPPO, _isVivo, _isSamsung } from "./../../utils/util";
const siteConfig = _getStaticConfig();
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import AnchorLink from "./AnchorLink";
import PollCard from "./PollCard";
import { onVideoClickHandler, createVideoUrl, createSlikeConfigLB } from "../../containers/liveblog/liveblog_utils";
const adsObj = siteConfig.ads;
const slikeApiKey = siteConfig.slike.apikey;

const LiveblogCard = props => {
  let maxItems = 40;
  let adposition = adsObj.ctnads.adspositionsLiveblog;
  let { lbcontent, type, pwa_meta, query } = props;
  let canonical = props.canonical;
  let _this = this;
  let esidata = props;
  let isOPPO = query && query.utm_source && query.utm_source == "oppo" ? true : false;
  let isVivo = query && query.utm_source && query.utm_source == "vivo" ? true : false;
  let isSamsung = query && query.utm_source && query.utm_source == "samsung" ? true : false;
  let UTM = `?utm_source=liveblog&utm_medium=${isMobilePlatform() ? "mweb" : "web"}&utm_campaign=hyperlink`;
  return (
    <ul className="livepost">
      {lbcontent.map((item, index) => {
        return (
          <ErrorBoundary key={`liveblog_${index}`}>
            {item["@type"] == "overend" ? (
              <li className="list_over_end">
                <div className="over_end">
                  <span>{item.title}</span>
                  {item.subtitle}
                </div>
              </li>
            ) : (
              <li
                key={item.msid}
                className={
                  item && item.Score
                    ? `livelist ${item.type === "t" && item.subType == "quotes" ? "quote" : ""}`
                    : `livelist only-txt ${item.type === "t" && item.subType == "quotes" ? "quote" : ""}`
                }
                {...SeoSchema({ pagetype: "liveblog" }).attr().blogpost}
              >
                <meta itemProp="url" content={pwa_meta ? pwa_meta.canonical + "#" + item.msid : ""} />
                <div className="blog_timestamp" itemProp="datePublished" content={item.updateDate}>
                  <span
                    {...SeoSchema({ pagetype: "liveblog" }).attr().modified}
                    content={item.updateDate}
                    className="blog_time"
                  >
                    {item.shortDateTime},{" "}
                  </span>
                  <span className="blog_date">{_dateFormatter(item.artDate).datestring.substring(4, 20)}</span>
                </div>
                {/* cricket commentary layer */}
                <div className="blog_txt">
                  {item.Overs && item.Overs != "" ? (
                    <div className="score-with-url">
                      <span className={"cls_" + item.Flag}>{item.Flag}</span>
                      {item.Score} <b>{item.Overs}</b>
                    </div>
                  ) : null}
                  {item.type == "p" || item.subType == "quotes" ? (
                    <React.Fragment>
                      {SeoSchema().metaTag({
                        name: "headline",
                        content: item.title && item.title.length > 100 ? item.title.substr(0, 100) : item.title,
                      })}
                      {SeoSchema().metaTag({
                        name: "mainEntityOfPage",
                        content: pwa_meta ? pwa_meta.canonical : "",
                      })}
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      {item.overridelink != "" &&
                      item.overridelink != null &&
                      item.overridelink != undefined &&
                      item.overridelink != "null" &&
                      item.overridelink != "undefined" ? (
                        <AnchorLink
                          hrefData={{ override: item.overridelink + UTM }}
                          {...SeoSchema({ pagetype: "liveblog" }).attr().entity}
                        >
                          {item.title}
                        </AnchorLink>
                      ) : (
                        <TitleWithSchema item={item} pwa_meta={pwa_meta} />
                      )}
                      {SeoSchema().metaTag({
                        name: "headline",
                        content: item.title && item.title.length > 100 ? item.title.substr(0, 100) : item.title,
                      })}
                    </React.Fragment>
                  )}
                  {item.keyevent === "0" && typeof item.detailmsg == "string" && item.detailmsg != "" ? (
                    <div {...SeoSchema({ pagetype: "liveblog" }).attr().articleBody}>
                      {Parser(item.detailmsg, {
                        replace: parserOptionsLB.replace.bind(this, item),
                      })}
                    </div>
                  ) : (
                    SeoSchema().metaTag({
                      name: "articleBody",
                      content: item.title,
                    })
                  )}

                  {(() => {
                    {
                      /*Create Image/Video on the basis of mstype/type ('m' for Image and)*/
                      /* Do not print media in case of keyevent===1 (highlights) */
                    }
                    switch (item.type) {
                      case "m":
                        if (item.keyevent === "0") {
                          return <ImageCard size="bigimage" msid={item.msid} schema />;
                        } else {
                          return true;
                        }

                      case "v":
                        if (item.detailmsg) {
                          if (item.yt == "true") {
                            return (
                              <iframe
                                className="youtubeembed"
                                src={`https://www.youtube.com/embed/${item.detailmsg}`}
                              />
                            );
                          }
                          let config = createSlikeConfigLB(item, item.detailmsg);
                          return (
                            <div data-videourl={createVideoUrl(item, item.detailmsg)}>
                              <VideoPlayer
                                autoDock
                                data-videourl={createVideoUrl(item, item.detailmsg)}
                                key={config.id}
                                onVideoClick={onVideoClickHandler.bind(this)}
                                apikey={slikeApiKey}
                                wrapper="masterVideoPlayer"
                                config={config}
                                inline={true}
                                imageid={item.msid}
                              />
                            </div>
                          );
                        } else {
                          return null;
                        }

                      //   return null;
                      // } else if (domNode.name == "embed" && domNode.next.name == "src" && domNode.next.attribs.type == "kaltura") {
                      //   //return (<iframe src={`https://m.maharashtratimes.com/slikeiframepage.cms?wapCode=nbt&apikey=nbtmwebtoi54&msid=${item.msid}`}></iframe>)
                      //   let config = createSlikeConfigLB(item, domNode.next.attribs.id);
                      //   return (
                      //     <div data-videourl={createVideoUrl(item, domNode.attribs.id)}>
                      //       <VideoPlayer
                      //         autoDock
                      //         data-videourl={createVideoUrl(item, domNode.attribs.id)}
                      //         key={config.id}
                      //         onVideoClick={onVideoClickHandler.bind(this)}
                      //         apikey={slikeApiKey}
                      //         wrapper="masterVideoPlayer"
                      //         config={config}
                      //         inline={true}
                      //         imageid={item.msid}
                      //       />
                      //     </div>
                      //   );

                      case "t":
                        // let author = domNode.children[0].children[0].data;
                        if (item.subtype == "quotes") {
                          return (
                            <div className="quote">
                              <span className="start-quote">
                                <b />
                              </span>
                              <div className="quote_txt">{item.title}</div>

                              <div className="quote_author">{item.author || ""}</div>
                              <span className="end-quote">
                                <b />
                              </span>
                            </div>
                          );
                          //FIXME: Check - removed from feed
                          // } else if (
                          //   item.keyevent === "0" &&
                          //   item.subType === "quotes" &&
                          //   typeof item.lbmdescrptionXML == "string" &&
                          //   item.lbmdescrptionXML != "" &&
                          //   item.lbmdescrptionXML != "null"
                          // ) {
                          //   return Parser(item.lbmdescrptionXML, {
                          //     replace: parserOptionsLB.replace.bind(_this, item),
                          //   });
                          // } else if (item.keyevent === 0 && item.subType === "quotes") {
                          //   return <span>{item.title}</span>;
                        } else {
                          return true;
                        }
                      // FIXME: DOUBT - This or above parser
                      case "p":
                        //FIXME: Old poll removed
                        // if (item.keyevent === "0") {
                        //   return Parser(item.lbhmdescrptionXML, {
                        //     replace: parserOptionsLB.replace.bind(_this, item),
                        //   });
                        // }
                        if (Array.isArray(item.childs) && item.childs && item.childs.length > 0) {
                          let polldata = { pollid: item.msid, polltext: item.title };
                          polldata.polloption = [];
                          item.childs.map(cItem => {
                            polldata.polloption.push({
                              optiontext: cItem.title,
                              optionno: cItem.contentid,
                            });
                          });
                          return PollCard({ data: polldata });
                        } else {
                          return true;
                        }
                      default:
                        if (item.keyevent == 0 && typeof item.embedXML == "string" && item.embedXML != "") {
                          return Parser(item.embedXML, {
                            replace: parserOptionsLB.replace.bind(_this, item),
                          });
                        } else {
                          return true;
                        }
                    }
                  })()}

                  {SeoSchema().publisherObj()}
                  {SeoSchema().metaTag({
                    name: "author",
                    content: "websitename",
                  })}
                  {/* if post image not available */ SeoSchema().metaTag({
                    name: "image",
                    content: getImageParameters({ size: "bigimage" }).src,
                  })}
                </div>
              </li>
            )}

            {adposition && isMobilePlatform() && adposition.indexOf("-" + (index + 1) + "-") > -1 ? (
              index + 1 == 5 ? (
                <React.Fragment>
                  <div className="ad1 parallaxDiv">
                    <div className="adinner" data-exclude="amp">
                      <div className="adinner-fxbox">
                        {
                          <AdCard
                            key={`adliveblog_${index}`}
                            mstype={isOPPO ? "mrec1" : isVivo ? "mrec1" : isSamsung ? "mrec11" : "mrec1"}
                            adtype="dfp"
                            elemtype="li"
                            // esiad={esidata != undefined ? true : false}
                            pagetype="liveblog"
                            // content={esidata}
                            query={query}
                          />
                        }
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <AdCard
                  key={`adliveblog_${index}`}
                  mstype={isOPPO || isVivo || isSamsung ? "mrec21" : "mrecinf"}
                  adtype="dfp"
                  elemtype="li"
                  pagetype="liveblog"
                />
              )
            ) : null}
          </ErrorBoundary>
        );
      })}
    </ul>
  );
};

// const createSlikeConfigLB = (item, slikeid) => {
//   let adCode = "others";
//   return {
//     player: {
//       section: adCode,
//     },
//     video: {
//       id: slikeid || "",
//       title: item.title || "",
//       msid: item.msid || "",
//       shareUrl: item.wu + "?" + siteConfig.slikeshareutm || "",
//       seopath: item.seolocation ? item.seolocation : item.title,
//       template: "liveblog",
//       image: item.videoImgUrl || "",
//     },
//     sdk: {
//       apikey: slikeApiKey,
//     },
//   };
// };

// const createVideoUrl = (item, msid, type, domNode) => {
//   let data = {};
//   if (domNode) {
//     return {
//       slikeid: domNode.attribs.src,
//       hl: domNode.children[0].data,
//       id: msid,
//       // env: "stg",
//       // version: "3.5.5",
//       controls: {
//         ui: "podcast",
//       },
//       seolocation: domNode.attribs.controls,
//     };
//   } else if (item.vdo) {
//     for (let i = 0; i < item.vdo.length; i++) {
//       if (item.vdo[i].id === msid) {
//         data = item.vdo[i];
//         break;
//       }
//     }
//   }
//   //handling for liveblog
//   else if (item.msid) {
//     data.override =
//       item.hoverridLink && item.hoverridLink != "" && item.hoverridLink != "null"
//         ? item.hoverridLink
//         : `/-/videoshow/${item.msid}.cms`;
//   }
//   if (type == "data") return data;
//   return generateUrl(data);
// };

// const generateUrl = item => {
//   if (item.override) {
//     return item.override;
//   } else {
//     let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
//     let url =
//       "/" +
//       (item.seolocation ? item.seolocation + "/" : "") +
//       templateName +
//       "/" +
//       (item.tn == "photo" ? item.imageid : item.id) +
//       ".cms";
//     return url;
//   }
// };

// const onVideoClickHandler = event => {
//   let _this = this;
//   // event.currentTarget.previousSibling.innerText
//   let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
//   if (videourl) {
//     try {
//       //To maintain previous url
//       window.history.pushState({}, "", videourl);
//       AnalyticsGA.pageview(location.origin + videourl);
//     } catch (ex) {
//       console.log("Exception in history: ", ex);
//     }
//   }
// };

const TitleWithSchema = ({ item, pwa_meta }) => {
  return item.subtype != "quotes" ? (
    <React.Fragment>
      <p>{item.title}</p>
      {SeoSchema().metaTag({
        name: "mainEntityOfPage",
        content: pwa_meta ? pwa_meta.canonical : "",
      })}
    </React.Fragment>
  ) : null;
};
export default LiveblogCard;
