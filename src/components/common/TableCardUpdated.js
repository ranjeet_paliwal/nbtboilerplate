import React from "react";
import PropTypes from "prop-types";
import styles from "./css/ArticleShow.scss";
import AnchorLink from "./AnchorLink";
import ImageCard from "./ImageCard/ImageCard";

/* table row  = '-$|$-'  && table col = '-|$|-'*/
const TableCardUpdated = props => {
  let { data, type } = props;

  // if (data && data != "") {
  //   data = data && data.split("-$|$-");
  // }
  // return null;
  return data && data.length > 0 ? (
    <div className="tableContainer">
      <table className={type != "" ? type : null}>
        <tbody>
          {data &&
            data[0] &&
            data[0].children &&
            data[0].children.map((item, index) => {
              let itemstds = item && item.children;
              return (
                <tr key={"tr" + index}>
                  {itemstds &&
                    itemstds.map((item, index) => {
                      const tbdata =
                        item &&
                        item.children &&
                        item.children[0] &&
                        item.children[0].children &&
                        item.children[0].children[0];
                      const itemdata = item && item.children && item.children[0];
                      const tddata1 = tbdata && item.children[0].children[0].data;
                      const itemchild = itemdata && item.children[0].children && item.children[0].children[0];
                      let tddata2 = tbdata && itemchild.children && itemchild.children[0] && itemchild.children[0].data;
                      let tddata = (itemdata && item.children[0].data) || tddata1 || tddata2;
                      return (
                        <td key={"td" + index}>
                          {itemdata && item.children[0].name == "a" && item.children[0].type == "tag" ? (
                            <AnchorLink href={item.children[0].attribs.href} target="_blank">
                              {itemchild.data}
                            </AnchorLink>
                          ) : itemdata &&
                            itemchild &&
                            itemchild.name &&
                            itemchild.name == "a" &&
                            itemchild.type == "tag" ? (
                            <AnchorLink href={itemchild.attribs.href} target="_blank">
                              {itemchild && itemchild.children[0].data}
                            </AnchorLink>
                          ) : itemdata &&
                            itemchild &&
                            itemchild.name &&
                            itemchild.name == "img" &&
                            itemchild.type == "tag" ? (
                            <img src={itemchild.attribs.src} target="_blank" />
                          ) : (
                            tddata
                          )}
                        </td>
                      );
                    })}
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  ) : null;
};

export default TableCardUpdated;
