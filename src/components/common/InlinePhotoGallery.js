import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { fetchNextPhotoMazzaShowDataIfNeeded } from "../../actions/photomazzashow/photomazzashow";

import ErrorBoundary from "./../lib/errorboundery/ErrorBoundary";
import ImageCard from "./ImageCard/ImageCard";
import PhotoMazzaShowCard from "./PhotoMazzaShowCard";
import FakePhotoMazzaCard from "./FakeCards/FakePhotoMazzaCard";
import { handleReadMore, isMobilePlatform } from "../../utils/util";
import CommonSlideWidget from "./CommonSlideWidget";
import Slider from "../desktop/Slider/index";

const getCurrentItem = _this => {
  let item;
  if (_this.props.value[0] && _this.props.value[0].length > 0) {
    _this.props.value[0].map((data, index) => {
      if ((data.it && data.it.id) == _this.props.params.msid) item = data;
    });
  }
  return item;
};

class InlinePhotoGallery extends Component {
  constructor(props) {
    super(props);
  }

  loadPhotoGallery() {
    const { dispatch, params } = this.props;
    InlinePhotoGallery.fetchNextData({ dispatch, params }).then(() => {
      handleReadMore();
    });
  }

  // componentDidMount() {
  //   // if (!isMobilePlatform()) {
  //   //   this.loadPhotoGallery();
  //   // }
  // }

  renderSlider = data => {
    const sliderData =
      data ||
      (this.props.value[0] && this.props.value[0].length > 0 && this.props.value[0][0].items
        ? this.props.value[0][0]
        : null);

    if (!sliderData) {
      return null;
    }

    return (
      <div className="wdt_embedslider">
        <div style={{ padding: "3px 15px" }}>{sliderData.secname}</div>
        <Slider
          type="embedslider"
          movesize="1"
          size="1"
          sliderData={sliderData && sliderData.items}
          width="630"
          height="472"
          islinkable={false}
          link={sliderData && sliderData.pwa_meta && sliderData.pwa_meta.redirectUrl}
        />
      </div>
    );
  };

  render() {
    let { params, isFetching } = this.props;
    let item = getCurrentItem(this);
    let view = "horizontal";

    return (
      <ErrorBoundary>
        {typeof item == "undefined" ? (
          !isFetching ? (
            <span data-slideshow={params.msid} onClick={this.loadPhotoGallery.bind(this)}>
              <ImageCard msid={params.msid} size="bigimage" />
            </span>
          ) : (
            <FakePhotoMazzaCard showImages={true} view={view} />
          )
        ) : isMobilePlatform() ? (
          <PhotoMazzaShowCard key={params.msid} item={item} msid={params.msid} isInlineContent={true} view={view} />
        ) : (
          this.renderSlider(item)
        )}
      </ErrorBoundary>
    );
  }
}

InlinePhotoGallery.fetchNextData = ({ dispatch, params }) => {
  return dispatch(fetchNextPhotoMazzaShowDataIfNeeded(params));
};

function mapStateToProps(state) {
  return {
    ...state.photomazzashow,
  };
}

export default connect(mapStateToProps)(InlinePhotoGallery);
