import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/MovieReviewListCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

import { SeoSchema } from "../../components/common/PageMeta"; //For Page SEO/Head Part

class MovieReviewTile extends Component {
  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return item.override ? item.override : url;
  }

  render() {
    let { item, leadpost, listCountInc, pagetype } = this.props;
    return (
      <li
        className={
          (leadpost
            ? [styles["moviereview-listview"], styles["lead-post"]].join(" ")
            : styles["moviereview-listview"]) +
          " animated fadeIn table moviereview-listview " +
          (leadpost ? "lead-post" : "")
        }
        key={item.id}
        {...SeoSchema({ pagetype: pagetype }).listItem()}
      >
        <AnchorLink
          hrefData={
            item.override
              ? { override: item.override }
              : {
                  seo: item.seolocation,
                  tn: item.tn,
                  id: item.tn == "photo" ? item.imageid : item.id,
                }
          }
          className="table_row"
          {...SeoSchema({ pagetype: pagetype }).attr().url}
        >
          {/* <Link to={this.generateUrl(item)} className="table_row"> */}
          {leadpost && (
            <ImageCard type="absoluteImgSrc" src={siteConfig.locale.movie_review_tile_bg} className="poster-bg" />
          )}
          <span className={styles["img_wrap"] + " table_col img_wrap"}>
            <ImageCard
              msid={item.imageid ? item.imageid : item.id}
              title={item.seotitle ? item.seotitle : ""}
              size="posterthumb"
              imgsize={item.imgsize ? item.imgsize : ""}
            />
          </span>
          <span className={styles["con_wrap"] + " table_col con_wrap"}>
            {/* {leadpost ? <span className={styles["caption"] + " caption"}>{siteConfig.locale.movie_review}</span> : ""} */}
            <span
              className={styles["title"] + " title text_ellipsis"}
              {...SeoSchema({ pagetype: pagetype }).attr().name}
            >
              {item.hl}
            </span>
            {/* for Schema only */ SeoSchema({
              pagetype: pagetype,
            }).metaPosition(listCountInc ? listCountInc() : "")}
            {!leadpost ? <span className={styles["cast"] + " cast text_ellipsis"}>{item.ct}</span> : ""}

            <span className="rating">
              <b>{siteConfig.locale.critics_rating}</b>
              <span className="rating-stars">
                <span className="empty-stars"></span>
                <span className={"filled-stars critic " + "w" + parseInt(item.cr) * 20}></span>
              </span>
            </span>
            {leadpost ? (
              <span className="lang">
                <b>{item.cg}</b>
              </span>
            ) : (
              ""
            )}
            <span className={styles["des"] + " des"}>{item.gn}</span>
            {!leadpost ? (
              <span
                className={styles["time-caption"] + " time-caption"}
                data-time={item.lu ? item.lu : item.dl ? item.dl : ""}
              >
                {item.lu ? item.lu : item.dl ? item.dl : ""}
              </span>
            ) : (
              ""
            )}
          </span>
        </AnchorLink>
      </li>
    );
  }
}

MovieReviewTile.propTypes = {
  item: PropTypes.object,
};

export default MovieReviewTile;
