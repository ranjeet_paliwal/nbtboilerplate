import React, { Component } from "react";
// import { Link } from 'react-router'
// import PropTypes from 'prop-types'
import Cookies from "react-cookie";
import fetchJsonP from "fetch-jsonp";
import styles from "./css/header.scss";
import { openLoginPopup, logOut, getSsoChannel } from "./ssoLogin";
import AnchorLink from "./AnchorLink";
import { OneTapIntialize } from "../lib/onetapsignin/newonetap";
import { AnalyticsGA } from "../lib/analytics/index";
import {
  _getStaticConfig,
  isMobilePlatform,
  _setCookie,
  _getCookie,
  isProdEnv,
  shouldTPRender,
  _isCSR,
  getURLParams,
  filterAlaskaData,
} from "../../utils/util";

import { loadJS } from "../lib/ads/lib/utils";
import GDPR_Module from "../lib/gdpr/index";
// import { getTpConfig } from "../common/TimesPoints/TimesPointConfig";
// import tpConfig from "../common/TimesPoints/TimesPointConfig";
import SvgIcon from "./SvgIcon";
import { fireGRXEvent } from "../lib/analytics/src/ga";
const siteConfig = _getStaticConfig();
const SSO = siteConfig.sso;
const SSOAPI = isProdEnv() ? siteConfig.sso : siteConfig.sso.stg;
const oneTapSignInObj = siteConfig.oneTapSignIn;
const tpConfig = siteConfig.TimesPoint;

class LoginControl extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.handleTruecallerClick = this.handleTruecallerClick.bind(this);
    this.state = { isLoggedIn: false, userName: "", img: "" };
    this.handleLoginCallback = this.loginCallback.bind(this);
    this.handleLogoutCallback = this.logoutCallback.bind(this);
    this.triggerGTMcalls = false;
    // this.loginButtonClicked = false;
  }

  componentDidMount() {
    const { dispatch, params, query } = this.props;
    const _this = this;
    if (typeof window !== "undefined") {
      oneTapSignInObj.constants.jssoCallback = _this.init.bind(this);
      // OneTapIntialize(oneTapSignInObj); // Initialize OneTapSignIn
      window.addEventListener("scroll", this.initializeOnHalfScroll.bind(this));
      if (
        window.location.href.indexOf("login.cms") != -1 &&
        (window.location.href.indexOf("status=ssosigninsuccess") != -1 ||
          window.location.href.indexOf("status=MappingUpdated") != -1)
      ) {
        try {
          const source = getURLParams(window.location.href, "source");
          window.opener.postMessage({ type: "login_success", source }, document.location.origin);
        } catch (ex) {}
        window.close();
      }

      try {
        _this.init(_this, user => {
          // console.log("user---------", user);
          GDPR_Module.showToastMessage();
        });
      } catch (ex) {
        console.log("Error in Login @ line _this.init", ex);
      }

      window.addEventListener("page_count", e => {
        if (e && e.detail && e.detail.pageview > SSO.SSO_TRUECALLER_visibility) {
          _this.handleTruecallerClick();
        }
      });
      window.addEventListener("message", e => {
        if (event.origin != document.location.origin) {
          return;
        }
        if (e.data.type === "login_success") {
          window.loginButtonClicked = true;
          this.triggerGTMcalls = true;

          try {
            const source = e.data.source;
            _this.init(
              _this,
              user => {
                // console.log("user---------", user);
                GDPR_Module.showToastMessage();
                if (user && typeof user === "object") {
                  fireGRXEvent("profile", "login", {
                    email: user.EMAIL || "",
                    userid: user.sso || "",
                    mobile: user.M_N || "",
                    login_source: window._login_source || "",
                  });
                }
              },
              { source },
            );
          } catch (ex) {
            console.log("Error in Login @ line _this.init", ex);
          }
        }
      });
    }
  }

  initializeOnHalfScroll() {
    if (window.scrollY > window.innerHeight / 2) {
      if (!window.initailizedOneTap) {
        window.initailizedOneTap = true;
        loadJS("https://accounts.google.com/gsi/client", () => {
          OneTapIntialize(oneTapSignInObj);
        });

        window.removeEventListener("scroll", this.initializeOnHalfScroll.bind(this));
      }
    }
  }

  handleLoginClick() {
    openLoginPopup(LoginControl.getUsersInfo.bind(this));
  }

  handleTruecallerClick() {
    const _this = this;
    const curdateTime = Math.floor(Date.now() / 1000); // return seconds
    const OneTapTrueCallerActTime = localStorage.getItem("OneTapTrueCallerActTime")
      ? parseInt(localStorage.getItem("OneTapTrueCallerActTime"))
      : localStorage.getItem("OneTapTrueCallerActTime");
    // check True caller window existance
    if (OneTapTrueCallerActTime != null && OneTapTrueCallerActTime != undefined) {
      console.log("True Caller Status: localstorage is active");
      // + 86400 * 5  (1day in unixtimestamp * days - in seconds value)
      const OneTapTrueCallerExpiry = OneTapTrueCallerActTime + 86400 * 2; // set for 2 days
      if (curdateTime > OneTapTrueCallerExpiry) {
        localStorage.removeItem("OneTapTrueCallerActTime");
        // Below Event is for debugging purpose only, need to remove post debugging
        AnalyticsGA.event({
          category: "True Caller",
          action: "Click",
          label: "Cookie Removed",
        });
      }
      // console.log(curdateTime > OneTapTrueCallerExpiry);
    }

    if (typeof JssoCrosswalk !== "null" && typeof JssoCrosswalk !== "undefined") {
      console.log("True Caller Status: JssoCrosswalk available");
      if (JssoCrosswalk && !window._user && (OneTapTrueCallerActTime == null || OneTapTrueCallerActTime == undefined)) {
        console.log("True Caller Status: ready to invoke popup");
        // AnalyticsGA.event(siteConfig.sso_ga.truecaller_requestscreen);
        const TrueCallerCrosswalk = new JssoCrosswalk(getSsoChannel(), "wap");
        // check true caller visibility focus
        const trueCallerFocusVisibility = !!(ua.indexOf("fban") == -1 && ua.indexOf("fbav") == -1);
        TrueCallerCrosswalk.truecallerOneTapLogin(trueCallerFocusVisibility, data => {
          const truecallermaxRetry = 6;
          let truecallercounter = 0;
          if (data && data.data && data.data.requestId && data && data.data.requestId.length > 1) {
            console.log("True Caller Status: Popup is Visible");
            // check True caller window intitate
            AnalyticsGA.event(siteConfig.sso_ga.truecaller_screen);
            // AnalyticsGA.GTM(siteConfig.sso_ga.truecaller_screen);
            const truecallerTimer = setInterval(() => {
              TrueCallerCrosswalk.truecallerVerify(data.data.requestId, resultData => {
                // check True caller window verifictaion when user action intitate
                // document.getElementById('headerContainer').append("|Verify status:" + resultData.data.code);
                // check user allowed with TrueCaller or not
                if (resultData && resultData.data && resultData.data.code) {
                  if (resultData.data.code == 200) {
                    // AnalyticsGA.event(siteConfig.sso_ga.truecaller_screencontinue);
                    // True caller success login
                    console.log("True Caller Status: Login Successful");
                    truecallercounter += 1;
                    clearInterval(truecallerTimer);
                    AnalyticsGA.event(siteConfig.sso_ga.truecaller_loginsuccess);
                    AnalyticsGA.event({
                      category: "Login New",
                      action: "Login_Screen",
                      label: "Login_Successful_Truecaller",
                      overrideEvent: "tp_login_new",
                    });
                    window._login_source = "truecaller";
                    // AnalyticsGA.GTM(siteConfig.sso_ga.truecaller_loginsuccess);
                    _this.init(_this, () => {});
                    // LoginControl.getTicketSuccess(resultData.data, () => { });
                    // localStorage.setItem('OneTapTrueCallerActTime', curdateTime);
                  } else if (resultData.data.code == 202) {
                    // AnalyticsGA.event(siteConfig.sso_ga.truecaller_skip);
                    // True caller skip button
                    console.log("True Caller Status: Skip Button");
                    localStorage.setItem("OneTapTrueCallerActTime", curdateTime);
                    truecallercounter += 1;
                    clearInterval(truecallerTimer);
                  } else if (resultData.data.code == 201 && truecallermaxRetry < truecallercounter) {
                    truecallercounter += 1;
                  } else {
                    console.log("True Caller Status: resultData :", resultData);
                  }
                } else {
                  if (resultData && resultData.code == "400") {
                    // TRANSACTION_ERROR
                    console.log("True Caller Status: local storage set", resultData);
                    localStorage.setItem("OneTapTrueCallerActTime", curdateTime);
                  }
                  console.log("True Caller Status: resultData.data :", resultData);
                }
                truecallercounter += 1;
                if (truecallermaxRetry < truecallercounter) {
                  // localStorage.setItem('OneTapTrueCallerActTime', curdateTime);
                  clearInterval(truecallerTimer);
                }
              });
            }, 4000);
          } else if (data && data.code == "505") {
            // TRUECALLER_LOGIN_FAILURE
            AnalyticsGA.event({
              category: "True Caller",
              action: "Click",
              label: "Login Failure",
            });
          }
        });
      }
    }
  }

  handleLogoutClick() {
    AnalyticsGA.event({
      category: "Login New",
      action: "Login_Screen",
      label: "Logout",
      overrideEvent: "tp_login_new",
    });
    logOut(Cookies.load("ssoid"));
    if (window.geoinfo.isGDPRRegion) {
      GDPR_Module.createStyleToHide(true);
    }
    LoginControl.context.handleLogoutCallback();
    LoginControl.statusChange(null);
    // LoginControl.logout();
  }

  loginCallback(data, ticketId) {
    this.setState({ isLoggedIn: true, userName: data.F_N || "", img: data.profile });
    // this.state.userName = data.F_N ? data.F_N : "";
    //  this.state.img = data.profile;
    try {
      if (data && data.F_N && typeof window && window._login_source == "onetap") {
        AnalyticsGA.event({ hitType: "event", category: "Login", action: "One_Tap", label: "Success_google" });
      }
    } catch (ex) {}

    this.forceUpdate();
    LoginControl.statusChange(data, ticketId);
  }

  logoutCallback() {
    this.setState({ isLoggedIn: false });
    this.state.userName = "";
    this.state.img = "";
    this.forceUpdate();
    const event = new CustomEvent("userLogout", { detail: "" });
    // Dispatch/Trigger/Fire the event
    document.dispatchEvent(event);
  }

  init(_this, callback, params) {
    LoginControl.context = _this || this;
    let ticketAPI = SSOAPI.SSO_URL_GETTICKET;
    if (!isMobilePlatform() && ticketAPI) {
      ticketAPI = ticketAPI.replace("platform=wap", "platform=web");
    }

    const url = ticketAPI + getSsoChannel();
    fetchJsonP(url, { method: "GET", mode: "cors", credentials: "include", timeout: 10000 })
      .then(response => response.json())
      .then(data => {
        if (data && data.ticketId) {
          if (this.triggerGTMcalls === true) {
            LoginControl.fetchLoginSource(data.ticketId, true, params);
            this.triggerGTMcalls = false;
          } else {
            LoginControl.fetchLoginSource(data.ticketId, false, params);
          }
        }
        LoginControl.getTicketSuccess(data, callback);
      })
      .catch(err => {
        LoginControl.apiError(err);
      });

    LoginControl.onStatusChange(_user => {});
  }

  fireTPGTM = () => {
    if (_isCSR()) {
      AnalyticsGA.event({
        category: "TimesPoints Page",
        action: "TP_Page",
        label: "TP_PageOpen",
        overrideEvent: "tp_page_activity",
      });
    }
  };

  render() {
    const user = this.state;
    const { timespoints, btnType } = this.props;
    // const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");

    let button = null;
    if (btnType == "logout") {
      if (user.isLoggedIn) {
        return <LogoutButton onClick={this.handleLogoutClick} />;
      }
      return "";
    }

    if (user.isLoggedIn) {
      button = <LogoutButton onClick={this.handleLogoutClick} />;
    } else {
      button = <LoginButton onClick={this.handleLoginClick} />;
    }

    let heading = "";
    let buttonText = "";
    let link = "";
    if (isMobilePlatform() && typeof shouldTPRender === "function" && shouldTPRender()) {
      if (timespoints && timespoints.userProfile) {
        const tpUserProfile = timespoints.userProfile;
        if (tpUserProfile.redeemablePoints == 0 || tpUserProfile.totalPoints == 0) {
          heading = "Read & earn rewards with TimesPoints!";
          buttonText = "Know How";
          link = tpConfig && `${tpConfig.tpPage}?section=howtoearn`;
        } else {
          heading = `Claim and earn your ${timespoints.userProfile && timespoints.userProfile.redeemablePoints} Points`;
          buttonText = "Redeem";
          link = tpConfig && tpConfig.tpRedeemLink;
        }
      }
    }
    // FIXME: IF POINTS 0, redirect to timespoint page

    return !isMobilePlatform() ? (
      <div className="wdt_login">
        <div className="user-controls">
          {user.img ? <img src={user.img} width="25" height="25" /> : ""}
          <Greeting user={user} />

          {button}
        </div>
      </div>
    ) : (
      <div className="login-block user-controls" data-exclude="amp">
        <div className="top">
          <div className="user-img">
            <span className="user-icon">
              {user.img ? <img src={user.img} /> : <SvgIcon name="userdefault" className="default-user-icon" />}
            </span>
          </div>
          <div className="user-info">
            <Greeting user={user} />

            <div className="wdt-tp-redeem">
              <p>{heading}</p>
              {buttonText ? (
                <AnchorLink href={link} className="btn tp-redeem" onClick={this.fireTPGTM}>
                  {buttonText}
                </AnchorLink>
              ) : null}
            </div>
            <div className="bottom">
              {button}
              <span className="hide" onClick={this.handleTruecallerClick} id="truecallerlogin">
                Login with truecaller
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginControl.getTicketSuccess = (data, callback) => {
  if (data.ticketId != null && data.ticketId != undefined && data.ticketId.length > 0) {
    // let validateticketurl = process.env.API_ENDPOINT + "/validateticket?ticketId=" + data.ticketId + "&channel=" + getSsoChannel();

    const validateticketurl = `${SSOAPI.SOCIAL_API_INTEGRATOR}/v1validateTicket?ticketId=${
      data.ticketId
    }&channel=${getSsoChannel()}`;

    const crossdomainvalidateticketurl = `${SSOAPI.BASE_URL}/v1validateTicket?ticketId=${
      data.ticketId
    }&channel=${getSsoChannel()}`;

    fetchJsonP(validateticketurl, {
      method: "GET",
      mode: "cors",
      credentials: "include",
      timeout: 10000,
    })
      .then(response => response.json())
      .then(jsonData => {
        if (jsonData && jsonData.msg == "success" && jsonData.code == "200") {
          if (
            process.env.SITE == "mt" ||
            process.env.SITE == "vk" ||
            process.env.SITE == "tlg" ||
            process.env.SITE == "tml" ||
            process.env.SITE == "mly"
          ) {
            fetchJsonP(crossdomainvalidateticketurl, {
              method: "GET",
              mode: "cors",
              credentials: "include",
              timeout: 10000,
            })
              .then(response => response.json())
              .then(json => {
                LoginControl.validateTicketUrlSuccess(data, callback);
              })
              .catch(error => {
                LoginControl.apiError(error);
                // This is done for staging only as stgaing APIs were getting failed in a valid case
                if (!isProdEnv() || ["mt", "iag"].includes(process.env.SITE)) {
                  LoginControl.validateTicketUrlSuccess(data, callback);
                }
              });
          } else {
            LoginControl.validateTicketUrlSuccess(data, callback);
          }
        }
      })
      .catch(error => {
        LoginControl.apiError(error);
      });
  } else {
    LoginControl.statusChange(null);
    if (callback) {
      callback(null);
    }
  }
};

LoginControl.apiError = err => {
  console.log("api error :", err);
};

LoginControl.fetchLoginSource = (ticketId, fireGTM, params) => {
  try {
    const crossdomainvalidateticketurl = `${
      SSOAPI.SSO_BASE_URL
    }/v1validateTicket?sourcereq=yes&ticketId=${ticketId}&channel=${getSsoChannel()}`;
    fetchJsonP(crossdomainvalidateticketurl, {
      method: "GET",
      mode: "cors",
      credentials: "include",
      timeout: 10000,
    })
      .then(response => response.json())
      .then(json => {
        if (json && fireGTM) {
          LoginControl.fireGTMevents(json, params);
        }
        LoginControl.userData = json;
        // we are setting ssoid cookie for iag,
        // as JCMS JAR has some issues while setting cookie
        // for rest of non indiatimes domain it is setting by JCMS JAR
        // that we need to change and should set by our own code
        if (process.env.SITE == "iag" && json.userId) {
          _setCookie("ssoid", json.userId, 30, ".iamgujarat.com");
        } else if (json.userId && typeof document !== "undefined" && document.domain.includes(".gadgetsnow.com")) {
          _setCookie("ssoid", json.userId, 30, ".gadgetsnow.com");
        }
      })
      .catch(error => {
        LoginControl.apiError(error);
      });
  } catch (e) {
    LoginControl.apiError(e);
  }
};

LoginControl.fireGTMevents = (json, params) => {
  if (json) {
    let loginSource = json.lastLoginType || "unknown";
    let source = "";
    if (params.source) {
      source = `_${params.source}`;
    }
    if (json.lastLoginType === "googleplus") {
      AnalyticsGA.event({
        category: "Login New",
        action: "Login_Screen",
        label: `Login_Successful_Google${source}`,
        overrideEvent: "tp_login_new",
      });
    } else if (json.lastLoginType === "facebook") {
      AnalyticsGA.event({
        category: "Login New",
        action: "Login_Screen",
        label: `Login_Successful_FB${source}`,
        overrideEvent: "tp_login_new",
      });
    } else if (json.lastLoginType === "sso") {
      if (json.login_source === "mobile") {
        AnalyticsGA.event({
          category: "Login New",
          action: "Login_Screen",
          label: `Login_Successful_Mobile${source}`,
          overrideEvent: "tp_login_new",
        });
      } else if (json.login_source === "email") {
        AnalyticsGA.event({
          category: "Login New",
          action: "Login_Screen",
          label: `Login_Successful_Email${source}`,
          overrideEvent: "tp_login_new",
        });
        loginSource = json.login_source;
      }
    }
    window._login_source = loginSource;
    // if (_isCSR() && window.grx) {
    //   window.grx("track", "login", { source: loginSource });
    // }
  }
};

LoginControl.validateTicketUrlSuccess = (data, callback) => {
  const ssoid = Cookies.load("ssoid");
  const ticketId = data && data.ticketId;
  // console.log("validateTicketUrlSuccess", data);
  // loadJS("https://jssocdnstg.indiatimes.com/crosswalk/jsso_crosswalk_0.5.3.min.js");
  if (typeof JssoCrosswalk === "undefined") {
    loadJS(SSOAPI.SSO_CROSSWALK);
  }

  // if (_getCookie("gdpr") === undefined) {
  //   _setCookie("gdpr", "n#n#n", 365);
  // }

  LoginControl.getUsersInfo({ ssoid, ticketId }, user => {
    if (callback) {
      callback(user[0] || user);
    }
  });
};

LoginControl.getUsersInfo = (_params, callback) => {
  const ssoId = _params.ssoid;
  const url = SSOAPI.MYTIMES_URL_USER + ssoId;
  fetchJsonP(url, { method: "GET", mode: "cors", credentials: "include", timeout: 10000 })
    .then(response => response.json())
    .then(json => {
      if (window.geoinfo && window.geoinfo.isGDPRRegion) {
        LoginControl.handleGDPR(json);
      }
      LoginControl.context.handleLoginCallback(json, _params.ticketId);
      if (typeof callback === "function") {
        callback(json);
      }
    })
    .catch(err => {
      console.log("GET USERS INFO ERROR", err);
    });
};

LoginControl.handleLogoutCall = () => {
  LoginControl.context.handleLogoutClick();
};

/**
 * Consent pop up need to be shown for user or not
 * @param {*} userData
 */
LoginControl.checkConsentPopup = userData => {
  // "termsAccepted":"1","shareDataAllowed":"1","timespointsPolicy":"1"
  let showConsentPopup = false;
  if (userData && userData.termsAccepted && userData.termsAccepted != "1") {
    showConsentPopup = true;
  } else if (userData && userData.shareDataAllowed && userData.shareDataAllowed != "1") {
    showConsentPopup = true;
  }
  return showConsentPopup;
};

LoginControl.handleGDPR = json => {
  // console.log("json-------", json, window.geoinfo.isGDPRRegion, window._user);

  const { userData } = LoginControl;

  let showConsentPopup = false;
  if ((_getCookie("gdpr") === "" || _getCookie("gdpr") === undefined) && userData) {
    showConsentPopup = LoginControl.checkConsentPopup(userData);
  }

  let logIn_SP;

  let logIn_PC;

  let cknsCookieVal;

  let optOutCookieVal;

  const params = {};
  logIn_PC = json && json.hasOwnProperty(`${siteConfig.gdprID}_P_C`) ? json[`${siteConfig.gdprID}_P_C`] : null; // json[siteConfig.gdprID + "_P_C"];
  logIn_SP = json && json.hasOwnProperty(`${siteConfig.gdprID}_S_P`) ? json[`${siteConfig.gdprID}_S_P`] : null;

  cknsCookieVal = window._getCookie("ckns_policyV2");
  optOutCookieVal = window._getCookie("optout");

  params[`${siteConfig.gdprID}_S_P`] = true;
  params[`${siteConfig.gdprID}_P_C`] = optOutCookieVal == 0;

  if (window.geoinfo.isGDPRRegion) {
    GDPR_Module.createStyleToHide(false);
  }

  // Creating/Updating gdpr cookies if consent already given and values are not present for logged same user

  if (
    typeof _getCookie === "function" &&
    (showConsentPopup || _getCookie("gdpr") === "0#0#0" || _getCookie("gdpr") === "n#n#n")
  ) {
    // consentPopUp();
    GDPR_Module.consentPopUp();
    // let event = new Event("gdprcustomEvent");
    // document.dispatchEvent(event);
  }

  if (!logIn_PC && !logIn_SP && window.geoinfo && window.geoinfo.isGDPRRegion) {
    fetch(
      "https://myt.indiatimes.com/mytimes/profile/update" +
        `?propertyKeyValuePairs=${JSON.stringify(params)}&uuId=${_getCookie("ssoid")}`,
      {
        type: "jsonp",
        // ,dataType : "json"
      },
    ).then(resp => {});
  }
};

LoginControl.OneTapTrueCallerInit = () => {
  if (typeof JssoCrosswalk === "undefined" && ua.indexOf("iphone") == "-1" && ua.indexOf("firefox") == "-1") {
    const OneTapTrueCaller = document.createElement("script");
    // OneTapTrueCaller.src = "https://jssocdn.indiatimes.com/crosswalk/jsso_crosswalk_legacy_0.4.4.min.js";
    OneTapTrueCaller.src = SSOAPI.SSO_CROSSWALK;
    OneTapTrueCaller.async = true;
    OneTapTrueCaller.defer = true;
    OneTapTrueCaller.type = "text/javascript";
    document.body.appendChild(OneTapTrueCaller);
  }
};

LoginControl.statusChange = (user, ticketId) => {
  console.log(`User Status:${user ? user.uid : null}`);
  // create a window object
  if (user && typeof user === "object" && ticketId) {
    user.ticketIdTemp = ticketId;
    user.ticketId = _getCookie("TicketId");
  }

  window._user = user;

  // Create the event
  const event = new CustomEvent("user.status", { detail: user || null });
  // Dispatch/Trigger/Fire the event
  document.dispatchEvent(event);
  // intitate OneTap TrueCaller
  if (!window._user && isMobilePlatform()) LoginControl.OneTapTrueCallerInit();
};

LoginControl.onStatusChange = function(callback) {
  document.addEventListener("user.status", callback, false);
};

function UserGreeting(props) {
  return (
    <span className="user-name">
      <b>Hi {props.user.userName}</b>
      <i className="icon_down " />
    </span>
  );
}

function GuestGreeting(props) {
  return isMobilePlatform() ? (
    <span className="user-name">
      Hi <b>User</b>
    </span>
  ) : (
    ""
  );
}

function Greeting(props) {
  const isLoggedIn = props.user.isLoggedIn;
  if (isLoggedIn && props.user) {
    return <UserGreeting user={props.user} />;
  }
  return <GuestGreeting />;
}

function LoginButton(props) {
  return isMobilePlatform() ? (
    <span onClick={props.onClick} style={{ color: "#fff", textDecoration: "underline" }}>
      Login
    </span>
  ) : (
    <span className="btn no-bg" onClick={props.onClick}>
      Login
      {/* {isMobilePlatform() ? siteConfig.locale.login : "Login"} */}
    </span>
  );
}

function LogoutButton(props) {
  return isMobilePlatform() ? (
    <span onClick={props.onClick} style={{ color: "#fff", textDecoration: "underline" }}>
      Logout
    </span>
  ) : (
    <div className="dropdown">
      <ul>
        {!(window.isCCPARegion && window.isCCPARegion === true) ? (
          <li>
            <AnchorLink href={siteConfig.sso.MYTIMES_URL_SETTINGS}>Edit Profile</AnchorLink>
          </li>
        ) : null}
        <li onClick={props.onClick}>Logout</li>
      </ul>
    </div>
  );
}

export default LoginControl;
