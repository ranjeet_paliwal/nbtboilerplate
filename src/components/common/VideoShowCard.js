import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/NewsListCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

class VideoShowCard extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.item.id != nextProps.item.id) {
      return true;
    }
    return false;
  }

  showListingIcon(item) {
    let icon = item.tn == "video" ? "video_icon" : item.tn == "photo" ? "photo_icon" : "";
    if (icon == "") return;
    return <span className={"type_icon " + icon}></span>;
  }

  showListNodeLabel(item) {
    let label = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][1] : "";
    return label;
  }

  generateUrl(item) {
    if (item.override) {
      return item.override;
    } else {
      let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
      let url =
        "/" +
        (item.seolocation ? item.seolocation + "/" : "") +
        templateName +
        "/" +
        (item.tn == "photo" ? item.imageid : item.id) +
        ".cms";
      return url;
    }
  }

  render() {
    let { item, leadpost, type } = this.props;
    return (
      <li
        className={styles["nbt-listview"] + " animated fadeIn nbt-listview " + (leadpost ? "lead-post" : "")}
        key={item.id}
        data-list-type={this.showListNodeLabel(item)}
      >
        <AnchorLink
          hrefData={{ override: this.generateUrl(item) }}
          target={item.tn == "html" ? "_blank" : ""}
          className=""
        >
          <span className="img_wrap">
            <ImageCard
              msid={item.imageid ? item.imageid : item.id}
              title={item.hl}
              size={type == "video_lead" ? "largewidethumb" : leadpost ? "largewidethumb" : "smallthumb"}
            />
            {this.showListingIcon(item)}
          </span>
          <span className={styles["con_wrap"] + " con_wrap"}>
            <span className="text_ellipsis">{item.hl}</span>
            <span
              className={styles["time-caption"] + " time-caption"}
              data-time={item.lu ? item.lu : item.dl ? item.dl : ""}
            >
              {item.lu ? item.lu : item.dl ? item.dl : ""}
            </span>
          </span>
        </AnchorLink>
      </li>
    );
  }
}

VideoShowCard.propTypes = {
  item: PropTypes.object,
};

export default VideoShowCard;
