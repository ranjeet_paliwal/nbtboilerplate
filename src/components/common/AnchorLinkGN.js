import React, { PureComponent } from "react";
import { Link } from "react-router";
import { _getStaticConfig } from "../../utils/util";

const siteConfig = _getStaticConfig();
const domainName = siteConfig.gadgetdomain;

class AnchorLinkGN extends PureComponent {
  render() {
    const { href, clicked, preventDefault } = this.props;
    let finalUrl = `${domainName}${href}`;
    if (href.includes(domainName)) {
      finalUrl = href;
    }

    return (
      <Link to={finalUrl.toLowerCase()} {...this.props}>
        {this.props.children}
      </Link>
    );
  }
}

export default AnchorLinkGN;
