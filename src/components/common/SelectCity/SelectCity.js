import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import AnchorLink from "../AnchorLink";
import {
  _isCSR,
  _filterALJSON,
  isMobilePlatform,
  filterAlaskaData,
  getAlaskaSections,
  _getStaticConfig,
  getPageType,
  _getCookie,
  _setCookie,
} from "../../../utils/util";
import { withRouter } from "react-router";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";

import SvgIcon from "../SvgIcon";
import { fetchHomeCityWidgetDataIfNeeded } from "../../../actions/home/home";

const siteConfig = _getStaticConfig();

class SelectCity extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showPopup: false,
    };
  }

  openPopUp = () => {
    this.setState({ showPopup: true });
  };

  closePopUp = () => {
    this.setState({ showPopup: false });
  };

  homePageDataChange = (item, pagetype = "home") => {
    const { dispatch, sectionId, homeWidgetInfo } = this.props;
    this.closePopUp();

    let secId = sectionId && sectionId.split(",")[0];
    let widgetInfo = isMobilePlatform()
      ? homeWidgetInfo.filter(widgetInfo => widgetInfo.id == secId)
      : homeWidgetInfo.filter(widgetInfo => widgetInfo._sec_id == secId);
    let params = isMobilePlatform() ? widgetInfo && widgetInfo[0] && widgetInfo[0].params : widgetInfo && widgetInfo[0];

    let cityId =
      item &&
      item.weblink &&
      item.weblink
        .split("/")
        .pop()
        .split(".cms")[0];

    let citySecId = cityId ? cityId : item && item.msid;
    if (params && params != undefined && citySecId != "" && citySecId != undefined) {
      params.citySecId = citySecId;
      _setCookie("selected_cityId", citySecId, 30);
      dispatch(fetchHomeCityWidgetDataIfNeeded(params, pagetype, widgetInfo));
    }
  };

  render() {
    const { alaskaData = {}, sectionId, location, extraContent, parentComponent } = this.props;
    let pagetype = getPageType(location && location.pathname);
    let secId;
    if (extraContent && process.env.SITE === "tlg" 
        && location && location.pathname 
        && location.pathname.includes('/telangana/')){
      secId = sectionId && sectionId.split(",")[1];
    } else {
      secId = sectionId && sectionId.split(",")[0];
    }
    let objSectionInfo = filterAlaskaData(alaskaData, secId);
    let selectCityData = getAlaskaSections(objSectionInfo);
    return (
      <ErrorBoundary>
        {selectCityData && selectCityData.length > 0 ? (
          <React.Fragment>
            <div
              className={parentComponent || `wdt_select_city`}
              data-exclude="amp"
              onMouseEnter={() => this.openPopUp()}
              onMouseLeave={() => this.closePopUp()}
            >
              <div className={parentComponent || "sel-city-btn"} onClick={() => this.openPopUp()}>
                {extraContent && <h4>{siteConfig.locale.choseStateCity.extraCityContent}</h4>}
                <span>
                  <SvgIcon name="location-new" />{" "}
                  {parentComponent === "headericon" ? null : siteConfig.locale.choseStateCity.dropDownText}
                </span>
              </div>
              {_isCSR() && isMobilePlatform() ? (
                ReactDOM.createPortal(
                  <div className={`sel-city-content ${this.state.showPopup ? "show" : ""}`}>
                    <span className="close_icon" onClick={() => this.closePopUp()}>
                      close
                    </span>
                    <div className="inner-content">
                      <ParentSection
                        selectCityData={selectCityData}
                        pagetype={pagetype}
                        alaskaData={alaskaData}
                        homePageDataChange={this.homePageDataChange}
                        parentComponent={parentComponent}
                        parentClosePopUp={this.closePopUp}
                      />
                    </div>
                  </div>,
                  document.getElementById("position-fixed-floater"),
                )
              ) : (
                <div className={`sel-city-content ${this.state.showPopup ? "show" : ""}`}>
                  <ParentSection
                    selectCityData={selectCityData}
                    pagetype={pagetype}
                    alaskaData={alaskaData}
                    homePageDataChange={this.homePageDataChange}
                    parentComponent={parentComponent}
                  />
                </div>
              )}
            </div>
          </React.Fragment>
        ) : null}
      </ErrorBoundary>
    );
  }
}

const ParentSection = ({
  selectCityData = [],
  pagetype,
  alaskaData,
  homePageDataChange,
  parentComponent,
  parentClosePopUp,
}) => {
  let utm_campaign = (parentComponent === "headericon") & _isCSR() ? location.pathname : "articlelist";
  //console.log("parentComponent", parentComponent);
  return (
    <ul>
      {selectCityData.map((item, index) => (
        <React.Fragment>
          {(parentComponent === "headericon" || pagetype && pagetype != "home") && item && !item._citydropdown ? (
            <li className="head" key={`${`city_dropdown`}${index.toString()}`} onClick={() => parentClosePopUp()}>
              <AnchorLink
                href={`${item.weblink}?utm_source=citydropdown&utm_medium=${parentComponent ||
                  "referral"}&utm_campaign=${utm_campaign}`}
              >
                {item.label}
              </AnchorLink>
            </li>
          ) : (
            item &&
            !item._citydropdown && (
              <li
                className="head"
                onClick={() => homePageDataChange(item)}
                key={`${`city_dropdown`}${index.toString()}`}
              >
                <b>{item.label}</b>
              </li>
            )
          )}
          {item && !item._citydropdown && (
            <ChildSection
              childData={item}
              alaskaData={alaskaData}
              pagetype={pagetype}
              homePageDataChange={homePageDataChange}
              parentComponent={parentComponent}
              parentClosePopUp={parentClosePopUp}
            />
          )}
        </React.Fragment>
      ))}
    </ul>
  );
};

// child component
const ChildSection = ({ childData, alaskaData, pagetype, homePageDataChange, parentComponent, parentClosePopUp }) => {
  let childSectionInfo = filterAlaskaData(alaskaData, childData.msid, "searchAll");
  let childSectionData = getAlaskaSections(childSectionInfo);
  let utm_campaign = (parentComponent === "headericon") & _isCSR() ? location.pathname : "articlelist";

  return childSectionData && childSectionData.length > 0 ? (
    <React.Fragment>
      {childSectionData.map((childItem, index) => (
        <React.Fragment>
          {(parentComponent === "headericon" || pagetype != "home") && childItem && !childItem._citydropdown ? (
            <li key={`${`child_city_drop_down`}${index.toString()}`} onClick={() => parentClosePopUp()}>
              <AnchorLink
                href={`${childItem.weblink}?utm_source=citydropdown&utm_medium=${parentComponent ||
                  "referral"}&utm_campaign=${utm_campaign}`}
              >
                {childItem.label}
              </AnchorLink>
            </li>
          ) : (
              childItem && !childItem._citydropdown &&
            <li key={`${`child_city_drop_down`}${index.toString()}`} onClick={() => homePageDataChange(childItem)}>
              <b>{childItem.label}</b>
            </li>
          )}
        </React.Fragment>
      ))}
    </React.Fragment>
  ) : (
    ""
  );
};

ChildSection.propTypes = {
  label: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    alaskaData: state.header && state.header.alaskaData,
    homeWidgetInfo: state.home && state.home.value && state.home.value[1],
  };
}

export default withRouter(connect(mapStateToProps)(SelectCity));
