import React, { Component } from "react";
import { findDOMNode } from "react-dom";

const IMAGE_FADE_IN_CLASS = `iron-image-fade-in`;

class IronImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLoadStatus: false,
      imageLoadFinishedClass: "",
      placeholderStyle: { backgroundImage: `url(${this.props.placeholder})` }
    };
  }

  componentDidMount() {
    const img = findDOMNode(this).querySelector("img");
    if (img.complete) {
      this.imageLoadHandler();
    }
  }
  imageLoadHandler() {
    if (!this.state.imageLoadStatus) {
      this.setState({
        imageLoadFinishedClass: IMAGE_FADE_IN_CLASS,
        imageLoadStatus: true
      });
      if (typeof this.props.onLoaded != "undefined") this.props.onLoaded();
    }
  }

  imageErrorHandler() {
    this.setState({
      imageLoadFinishedClass: IMAGE_FADE_IN_CLASS,
      imageLoadStatus: false
    });
  }

  render() {
    return (
      <span
        className="iron-image-container"
        style={this.state.placeholderStyle}
      >
        <img
          className={`iron-image ${this.state.imageLoadFinishedClass}`}
          alt={this.props.alt}
          title={this.props.alt}
          width={this.props.width}
          height={this.props.height}
          src={this.props.src}
          onLoad={this.imageLoadHandler.bind(this)}
          onError={this.imageErrorHandler.bind(this)}
        />
      </span>
    );
  }
}

export default IronImage;
