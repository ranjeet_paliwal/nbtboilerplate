/* eslint-disable no-nested-ternary */
/* eslint-disable indent */
import React from "react";
// import { Link } from "react-router";
import PropTypes from "prop-types";
import styles from "./css/ListHorizontalCard.scss";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";
import { _getStaticConfig, _deferredDeeplink } from "../../utils/util";
const siteConfig = _getStaticConfig();

// class ListSlideshowCard extends Component {

function generateUrl(item) {
  const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
  const url = `/${item.seolocation ? `${item.seolocation}/` : ""}${templateName}/${item.id}.cms`;

  return item.override ? item.override : url;
}

const ListSlideshowCard = props => {
  const { item, type, className } = props;
  const firstSlideItem = item && item.items ? item.items[0] : {};
  const sliderWidth =
    typeof item.items !== "undefined" && item.items.length > 0
      ? `${item.items.length * siteConfig.locale.default_slideshow_tile_width + (item.items.length - 1) * 10}px`
      : "";
  // let listlink = {override :  siteConfig.applinks.android.oip_appexclusive + item.id + '&pagetype=home'+ '&type=' + (item.tn && item.tn == "photostrip" ? "extphoto" : (firstSlideItem.tn && firstSlideItem.tn != '' ? firstSlideItem.tn : 'news')) }
  // If the click was from recommended page
  const campaignName = item.clkurl ? "recommended" : item.tn && item.tn == "photostrip" ? "extphoto" : "";
  /* const listlink = {
    override: _deferredDeeplink(
      item.tn && item.tn == "photostrip"
        ? "extphoto"
        : firstSlideItem.tn && firstSlideItem.tn != ""
        ? firstSlideItem.tn
        : "news",
      siteConfig.appdeeplink,
      item.id,
      "",
      "",
      campaignName,
    ),
  }; */
  const listlink = {
    override: `https://${siteConfig.photogallerydomain}/${item.seolocation}/articleshow/${item.id}.cms`,
  };
  // {firstSlideItem.override ? {override :  firstSlideItem.override} : {seo: firstSlideItem.seolocation, tn : firstSlideItem.tn, id : firstSlideItem.id}}
  return (
    <li
      className={`${(className ? `${className} ` : "") +
        styles["nbt-horizontalView"]} nbt-horizontalView animated fadeIn app-exclusive`}
      key={item.id}
      data-list-type={siteConfig.locale.appexclusive}
    >
      <AnchorLink hrefData={listlink} fireClickPixel={item.clkurl ? item.clkurl : null} className="table_row">
        {/* <Link to={generateUrl(item.items[0])}> */}
        <span className="article-text">{item.hl}</span>
        <div className="scroll_content">
          <ul className="nbt-list" style={{ width: sliderWidth }}>
            {typeof item.items !== "undefined" && item.items.length > 0
              ? item.items.map((item, index) => (
                  <li key={index} className="table nbt-listview">
                    <span className="table_col img_wrap" data-tag={item.tn == "videolist" ? item.du : ""}>
                      <ImageCard
                        msid={item.imageid}
                        title={item.seotitle ? item.seotitle : ""}
                        size="smallslideshowthumb"
                        imgsize={item.imgsize ? item.imgsize : ""}
                      />
                    </span>
                  </li>
                ))
              : null}
          </ul>
        </div>
        {/* {type && (type == "photostrip" || type == "photopreview") ? (
          <span className="app-txt">{siteConfig.locale.appexclusive}</span>
        ) : null} */}
      </AnchorLink>
    </li>
  );
};

ListSlideshowCard.propTypes = {
  item: PropTypes.object,
};

export default ListSlideshowCard;
