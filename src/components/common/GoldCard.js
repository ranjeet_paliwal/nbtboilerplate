import React, { PureComponent } from "react";
import styles from "./css/PollutionCard.scss";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

class GoldCard extends PureComponent {
  componentDidMount() {}

  render() {
    return (
      <div
        className="data-drawer"
        style={{ width: "315px", overflow: "hidden", position: "relative", margin: "5px 0" }}
      >
        <div className="wdt-weather">
          <div className="wth-details table">
            <div className="table_row">
              <span>&#x25B2;Gold: 55551</span>
              <span>284 (+0.59%)</span>
            </div>
            <div className="table_row">
              <span>Silver: 55551</span>
              <span>&#x25BC;284 (+0.59%)</span>
            </div>
            <div className="table_row">
              <span className="forecast table_col">
                <img src="https://navbharattimes.indiatimes.com/photo/54583262.cms" />
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default GoldCard;
