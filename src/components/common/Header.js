import React, { PureComponent } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";

import styles from "./css/header.scss";
import LoginControl from "./LoginControl";

import {
  _getStaticConfig,
  _isCSR,
  getAlaskaSections,
  filterAlaskaData,
  isTechSite,
  isMobilePlatform,
  getMobileOs,
  checkIsAmpPage,
  isTechHomePage,
  _getCookie,
  _setCookie,
  isInternationalUrl,
  getCountryCode,
  isBusinessSection,
  getLogoLink,
  getLogoPath,
  getLogoClass,
  isGadgetPage,
} from "./../../utils/util";
import globalconfig from "../../globalconfig.js";

const siteConfig = _getStaticConfig();
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import { throttle, _deferredDeeplink } from "./../../utils/util";
import { AnalyticsGA } from "./../../components/lib/analytics/index";
import AnchorLink from "./AnchorLink";
import Ads_Module from "./../../components/lib/ads/index";
import { fetchTechHeaderDataIfNeeded, fetchNavDataSuccess } from "./../../actions/header/header";

import GnSearchBox from "../common/GnSearchBox/GnSearchBox";
import SvgIcon from "./SvgIcon";
import SelectCity from "./SelectCity/SelectCity";
import { fireGRXEvent } from "../lib/analytics/src/ga";
import { internationalconfig } from "../../utils/internationalpageConfig";
import gadgetsConfig from "../../utils/gadgetsConfig";
import LangDropDown from "./LangDropDown";
import { navConfig } from "../common/Gadgets/GnNavBar";

const ASSET_PATH = process.env.ASSET_PATH || "/";
const logo = siteConfig.logo;
const appdeeplink = siteConfig.appdeeplink;
const countryCode = getCountryCode();
const siteName = process.env.SITE;

// Need to remove this once data is inserted in alaska for gadget page

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      subNavigations: {},
      navHistory: [],
      navOffsetTop: "",
      dockHeader: false,
      hasMounted: false,
    };
    this.isGulf = false;
    this.currentNav = {};
  }

  componentDidMount() {
    const { dispatch, params, query, config } = this.props;
    const _this = this;

    this.setState({ hasMounted: true });
    if (typeof window != "undefined") {
      window.setTimeout(() => {
        window.addEventListener("scroll", throttle(_this.scrollHandler.bind(_this), 100));
      }, 1000);
      if (window && window.location && window.location.href.indexOf("/gulf") > -1) {
        this.isGulf = true;
      } else {
        this.isGulf = false;
      }
    }

    //Header.fetchData({dispatch, params, query});
    /*if(typeof(window)!='undefined'){
      window.setTimeout(()=>{
        _this.setupNavigationClick();
      },100);
    }*/
    this.updateLogoLink();
  }

  componentDidUpdate() {
    const activeLI = document.querySelector("#hrNavScroll li.active");
    if (activeLI) {
      document.querySelector("#hrNavScroll").scrollLeft = activeLI.offsetLeft;
    }

    let bodyPadding = 0;
    if (globalconfig.topatfEnabled && globalconfig.alaskaMastHeadEnabled) {
      bodyPadding = 196;
    } else if (globalconfig.topatfEnabled || globalconfig.alaskaMastHeadEnabled) {
      bodyPadding = 146;
    } else {
      bodyPadding = 96;
    }
    if (this.props.config.pagetype === "videoshow") {
      bodyPadding -= 46;
    }
    if (this.props.isTimesPointPage) {
      bodyPadding -= 100;
    }
    let topAtfEle = document.querySelector(".ad1.topatf");
    if (topAtfEle && globalconfig.topatfEnabled) {
      topAtfEle.style.minHeight = `50px`;
      topAtfEle.style.height = `50px`;
    } else {
      topAtfEle.style.minHeight = `0px`;
      topAtfEle.style.height = `0px`;
    }
    // if (this.props.config.pagetype === "videoshow") {
    //   bodyPadding = -50;
    // }
    let bodyEle = document.querySelector("body");
    bodyEle.style.paddingTop = `${bodyPadding}px`;
    // let top_drawer = document.querySelector("#topdrawer_content");
    // if (top_drawer && top_drawer.classList.value.includes("visible")) {
    //   document.querySelector(".topdrawer_icon").click();
    // }
  }

  componentWillReceiveProps(nextProps) {
    // FIXME:Commenting all These as we dont need to handle Header and dock in Videoshow
    /*const _this = this;
    if (_this.props.dockHeader !== nextProps.dockHeader) {
      _this.setState({ dockHeader: nextProps.dockHeader });
    }*/
    /*if (_this.props.config !== nextProps.config) {
      let rootEle = document.querySelector("#root");
      let headerEle = document.querySelector(".header");
      if (nextProps.config && nextProps.config.pagetype == "videoshow") {
        headerEle.classList.add("top_fixed");
        rootEle.style.paddingTop = headerEle.offsetHeight + "px";
      } else {
        headerEle.classList.remove("top_fixed");
        rootEle.style.paddingTop = "0px";
      }
    }*/
  }
  scrollHandler() {
    const _this = this;
    let navEle = document.querySelector(".navbar");
    let rootEle = document.querySelector("#root");
    let navOffsetTop = rootEle ? rootEle.offsetTop : "";
    let fixed_bar = document.querySelector(".fixed_bar_with_app");
    let openappStrip = document.querySelector(".open_app_strip");

    //let headerContainerHeight = document.querySelector('#headerContainer').offsetHeight;
    let headerEle = document.querySelector(".header");
    let windowScroll = window.scrollY;
    // let atfElem = document.querySelector('.ad1.atf');
    // if (_this.props.config && _this.props.config.pagetype == "videoshow") {
    //   console.log("videoshow");
    //   headerEle.classList.add('top_fixed');
    //  // rootEle.style.paddingTop = headerEle.offsetHeight + "px";
    // }
    let bodyPadding = 0;
    if (globalconfig.topatfEnabled && globalconfig.alaskaMastHeadEnabled) {
      bodyPadding = 150;
    } else if (globalconfig.topatfEnabled || globalconfig.alaskaMastHeadEnabled) {
      bodyPadding = 100;
    } else {
      bodyPadding = 50;
    }
    if (_this.props.config.pagetype === "videoshow") {
      bodyPadding -= 50;
    }
    const topFixedClassName = `top_fixed${bodyPadding}`;
    if (_this.props.config) {
      if (navEle && !navEle.classList.contains(topFixedClassName) && windowScroll > navOffsetTop / 2) {
        _this.setState({ navOffsetTop: navOffsetTop });
        navEle.classList.add(topFixedClassName);
        if (openappStrip) {
          openappStrip.classList.remove("mobilehide");
        }
        //Start atfElem fixed
        // if(atfElem && atfElem.parentNode && atfElem.parentNode.parentNode){atfElem.parentNode.parentNode.classList.add('fixedatf');}
        //End
        // rootEle.style.paddingTop = navEle.offsetHeight + "px";
      }
      //scrolling up
      else if (navEle.classList.contains(topFixedClassName) && windowScroll < navOffsetTop / 2) {
        _this.setState({ navOffsetTop: navOffsetTop });
        for (let i = navEle.classList.length - 1; i >= 0; i--) {
          const className = navEle.classList[i];
          if (className.startsWith("top_fixed")) {
            navEle.classList.remove(className);
          }
        }
        // navEle.classList.remove(topFixedClassName);
        if (openappStrip) {
          openappStrip.classList.add("mobilehide");
        }
        // if(atfElem && atfElem.parentNode && atfElem.parentNode.parentNode){atfElem.parentNode.parentNode.classList.remove('fixedatf');}
        // rootEle.style.paddingTop = "0px";
      }
    }
  }

  searchToggle(e) {
    e.preventDefault();
    document.getElementById("search-chk").checked = !document.getElementById("search-chk").checked;
  }
  selectGadget(e) {
    this.searchToggle();
    document.getElementById(e.target.parentElement.parentElement.parentElement.id).style.display = "none";
  }
  searchGadgets(e) {
    let { dispatch, router, params, query } = this.props;
    e.preventDefault();
    //e.stopPropagation();
    let _this = this;
    let querytext = _this.refs.searchStringInput.value;
    querytext = querytext.replace(/\s+/g, " ").trim();
    querytext = querytext.replace(/[`~!#%()+\=?;:'",.<>\{\}\[\]\\\/]/gi, "");
    if (!querytext) {
      _this.refs.searchStringInput.value = "";
      return false;
    } else {
      return dispatch(fetchTechHeaderDataIfNeeded(params, query, router, querytext));
      //console.log(querytext);
      //populate gadget list
    }
  }
  submitSearch(e) {
    e.preventDefault();
    document.getElementById("search-chk").checked = !document.getElementById("search-chk").checked;
    //e.stopPropagation();
    let _this = this;
    let querytext = _this.refs.searchStringInput.value;
    querytext = querytext.replace(/\s+/g, " ").trim();
    querytext = querytext.replace(/[`~!#%()+\=?;:'",.<>\{\}\[\]\\\/]/gi, "");
    if (!querytext) {
      _this.refs.searchStringInput.value = "";
      return false;
    } else {
      window.location.href = siteConfig.mweburl + "/topics/" + _this.refs.searchStringInput.value;
    }
  }

  dockPlayer() {
    // FIXME:Commenting all these as we dont need to handle dock in Videoshow
    /*let event = new CustomEvent("dockMasterPlayer", { detail: "button" });
    document.dispatchEvent(event);
    this.setState({ dockHeader: false });*/
  }

  opentopdrawer = event => {
    event.preventDefault();
    if (document.getElementById("topdrawer_content").classList.contains("visible")) {
      document.getElementById("topdrawer_content").classList.remove("visible");
      document.querySelector(".top_drawer").classList.remove("active");
      document.querySelector(".topdrawer_icon").classList.add("rotate");
    } else {
      document.getElementById("topdrawer_content").classList.add("visible");
      document.querySelector(".top_drawer").classList.add("active");
      document.querySelector(".topdrawer_icon").classList.remove("rotate");
    }
  };

  openNavigation() {
    this.currentNav = {};
    document.getElementById("mySidenav").style.transform = "translateX(0px)";
    document.querySelector(".greyLayer").style.display = "block";
    document.getElementById("mainNavigation").style.display = "block";
    document.getElementById("subNavigation").style.display = "none";
    document.body.style.overflow = "hidden";
  }

  openHomeNavigation() {
    this.currentNav = {};
    this.setState({ navHistory: [] });
    this.openNavigation();
  }

  closeNavigation() {
    if (document.getElementById("mySidenav"))
      document.getElementById("mySidenav").style.transform = "translateX(-1000px)";
    if (document.querySelector(".greyLayer")) document.querySelector(".greyLayer").style.display = "none";

    document.body.style.overflow = "scroll";
  }

  closeBanner() {
    let banner = document.querySelector(".open_app_strip");

    if (banner) {
      banner.classList.add("mobilehide");
      _setCookie("closeAppbanner", true, 1);
    }
  }

  renderNavElements(value, index, keyobj, level, gadgetPage) {
    let isSubNavAvailable = Object.keys(value).length > 4 && !gadgetPage ? true : false;
    let key = null;
    if (value.hasOwnProperty("key")) {
      key = value.key;
    } else {
      key = keyobj;
    }
    if (value.ignore != "true" && value.weblink && value.weblink != "" && value.class !== "mobilehide") {
      return (
        <li key={index} className={`${value._classMobile ? value._classMobile : ""}`}>
          {isSubNavAvailable && level == 1 ? <input type="checkbox" id={key ? "chk-" + key : ""} /> : ""}
          <AnchorLink className={isSubNavAvailable ? "subnav" : ""} hrefData={{ override: value.weblink }}>
            <span>{value.label}</span>
          </AnchorLink>
          {isSubNavAvailable ? (
            <label
              data-level={"l" + level}
              data-bc={value.breadcrumb == "1" ? "1" : "0"}
              data-index={index}
              data-key={key ? key : ""}
              data-label={value.label}
              className="linearrow dir-right"
              htmlFor={key ? "chk-" + key : ""}
              onClick={
                isSubNavAvailable && level == 0
                  ? this.loadNavigation.bind(this)
                  : isSubNavAvailable && level == 1
                  ? this.activeNavChild.bind(this)
                  : null
              }
            />
          ) : (
            ""
          )}
          {isSubNavAvailable && level == 1 ? (
            <React.Fragment>
              <ul className="sub_navigation">
                {Object.keys(value).map((key, index) => {
                  return key != "label" &&
                    key != "weblink" &&
                    key != "mweb" &&
                    key != "error" &&
                    key != "isFetching" &&
                    key != "name" &&
                    key != "msid" ? (
                    <li key={"l-0-" + index}>
                      <AnchorLink data-level={"l" + level} hrefData={{ override: value[key].weblink }}>
                        {value[key].label}
                      </AnchorLink>
                    </li>
                  ) : (
                    ""
                  );
                })}
              </ul>
            </React.Fragment>
          ) : (
            ""
          )}
        </li>
      );
    } else return null;
  }
  mapObject(object, callback) {
    let level = 0;
    if (Object.keys(object).length === 4) {
      return Object.keys(object).map(function(key, index) {
        return callback(key, object[key], index, level);
      });
    } else {
      return Object.keys(object).map(function(key, index) {
        if (
          key != "label" &&
          key != "weblink" &&
          key != "mweb" &&
          key != "error" &&
          key != "isFetching" &&
          key != "name"
        ) {
          level = key.split("").filter(ch => ch == "-").length;
          return callback(key, object[key], index, level);
        }
      });
    }
  }

  /*
   * get Top Drawer from alaska
   * */
  drawerMenuFromAlaska = drawerData => {
    try {
      let drawerMenu = [];
      // const iconclass = ["one", "two", "three", "four", "five", "six"];
      let indexClass = 0;
      if (drawerData && typeof drawerData === "object") {
        Object.keys(drawerData).map((key, index) => {
          if (drawerData[key].label != undefined && drawerData[key].label != null && indexClass < 5) {
            let tempObj = {};
            tempObj["secname"] = drawerData[key]._icontext;
            tempObj["link"] = drawerData[key]._destionationurl;
            // tempObj["classname"] = iconclass[indexClass];
            tempObj["iconname"] = drawerData[key]._icon;
            drawerMenu.push(tempObj);
            indexClass++;
          }
        });
      }
      return drawerMenu;
    } catch (ex) {
      console.log("Error in alaskaTopDrawer:", ex);
    }
  };

  loadNavigation(event) {
    let _this = this;
    let targetElem = event.currentTarget;
    //let subNavigations = JSON.parse(targetElem.getAttribute("data-sectionurl"));
    let key = targetElem.getAttribute("data-key");
    let subNavigations = {};

    _this.currentNav =
      Object.keys(_this.currentNav).length === 0 && _this.currentNav.constructor === Object
        ? _this.props.navigations
        : _this.currentNav;
    for (let k in _this.currentNav) {
      if (k === key) {
        subNavigations = _this.currentNav[k];
        _this.currentNav = subNavigations;
        break;
      }
    }
    if (Object.keys(subNavigations).length > 4) {
      let bElem = {};
      bElem["label"] = targetElem.getAttribute("data-label");
      bElem["key"] = targetElem.getAttribute("data-key");
      bElem["breadcrumb"] = "1";
      let navHistory = this.state.navHistory;
      if (targetElem.getAttribute("data-bc") == "1") {
        let dataIndex = targetElem.getAttribute("data-index");
        navHistory = navHistory.slice(0, dataIndex + 1);
      } else {
        navHistory.push(bElem);
      }

      this.setState({ navHistory: navHistory });

      event.preventDefault();
      if (subNavigations) {
        _this.setState({ subNavigations: subNavigations });
        document.getElementById("subNavigation").style.display = "block";
        document.getElementById("mainNavigation").style.display = "none";

        // Append Active Level1 Section Name in Nav Top Bar
        targetElem.getAttribute("data-level") == "l0"
          ? (document.querySelector(".active_secName b").innerHTML = targetElem.getAttribute("data-label"))
          : null;
      }
    } else {
      _this.currentNav = {};
    }
  }

  activeNavChild(ele) {
    let target = ele.currentTarget;
    var classlist = target.classList;
    if (classlist.contains("active")) {
      target.classList.remove("active");
    } else {
      target.classList.add("active");
    }
  }

  updateLogoLink() {
    const logoSelector = document.querySelector(".logo a");
    if (countryCode && logoSelector) {
      const logoUpdatedLink = siteConfig.weburl + internationalconfig[siteName].internationalPagesUrl[countryCode];
      logoSelector.setAttribute("href", logoUpdatedLink);
    }
  }

  render() {
    let _this = this;
    let { navigations, config, dockHeader, viralBranding, query, router, navData, timespoints, location } = this.props;
    // console.log("configconfig", config);
    let path = this.props.location.pathname;
    let alaskaMastHead = filterAlaskaData(navigations, ["pwaconfig", "masthead", "mobile"], "label");
    const isTimesPointsPage = router.location.pathname.indexOf("timespoints.cms") > -1;
    let isAmpPage = checkIsAmpPage(path);

    let videoHeader = config.pagetype == "videoshow" ? true : false;
    let showTopDrawer = false;
    //config && config.pagetype == "home" ? true : false;
    // if (typeof window != "undefined") {
    //   if (window.scrollY == 0) {
    //     showTopDrawer = true;
    //   } else {
    //     showTopDrawer = false;
    //   }
    // }

    let navdata = "";
    const pathName = location && location.pathname;
    const URLlocation = router && router.location && router.location.pathname;

    if (
      (config.pagetype != "home" || isTechHomePage(pathName)) &&
      config.pagetype != "topics" &&
      config.pagetype != "liveblog"
    ) {
      let parentId = config.parentId;
      let secId = config.secId;
      let childSections = "";

      // for parent Section/Id kitchen-corner(48909483) in Tamil
      // will use navigation of Life Style(subsec1)
      // we can also drive this from feed

      // Also for photoshow there is exceptional use case as we use picid/slideid
      // apart from slideshow id, hence will be using subsecid for navigation in mobile fot

      if (config.parentId == "48909483" || config.pagetype == "photoshow") {
        parentId = config.subsec1;
      }

      // Logic for showing nav data
      // if current section has its child, children will be shown else siblings will be shown

      if (secId) {
        const childSectionsObj = filterAlaskaData(navigations, secId);
        childSections = getAlaskaSections(childSectionsObj, true);
        if (childSections.length > 0) {
          navdata = JSON.parse(JSON.stringify(childSections));
        } else if (secId && parentId && secId == parentId) {
          // Root Level
          navdata = getAlaskaSections(navigations);
        } else if (parentId) {
          const objSectionInfo = filterAlaskaData(navigations, parentId);
          navdata = getAlaskaSections(objSectionInfo, true);
        }
      }
    } else {
      navdata = getAlaskaSections(navigations);
    }

    let isNavEmpty =
      navdata == undefined || (Array.isArray(navdata) && (navdata.length == 0 || navdata[0].msid == undefined));
    if (this.props.navData && isNavEmpty) {
      navdata = this.props.navData;
    }

    let alaskaMainDrawer = filterAlaskaData(navigations, ["pwaconfig", "snackbar", "default_bar"], "label");
    let alaskaTechDrawer = filterAlaskaData(navigations, ["pwaconfig", "snackbar", "tech_bar"], "label");

    let mainDrawer = alaskaMainDrawer ? this.drawerMenuFromAlaska(alaskaMainDrawer) : null;
    let techDrawer = alaskaTechDrawer ? this.drawerMenuFromAlaska(alaskaTechDrawer) : null;

    let drawerMenu =
      this.props.router &&
      this.props.router.location &&
      this.props.router.location.pathname &&
      this.props.router.location.pathname.indexOf("/tech") > -1 &&
      techDrawer
        ? techDrawer
        : mainDrawer
        ? mainDrawer
        : null;

    let isAppBannerClosed;
    if (_isCSR()) {
      isAppBannerClosed = !_getCookie("closeAppbanner");
    }

    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    const dropdownconfig = internationalconfig[siteName] && internationalconfig[siteName].dropdownArray;
    let intPathName = router && router.location && router.location.pathname;
    intPathName = config && config.pagetype != "home" ? "/" : intPathName;
    let isFrmapp = false;
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      isFrmapp = true;
    }

    const logoPath = getLogoPath(config.pagetype, pathName, config.siteName);
    const logoLink = getLogoLink(config.pagetype, pathName, countryCode, config.siteName);
    const logocssClass = getLogoClass(config.pagetype, pathName);
    let homeIconLink = siteConfig.mweburl;

    if (countryCode) {
      homeIconLink = internationalconfig[siteName].internationalPagesUrl[countryCode];
    } else if (viralBranding && siteConfig.viralBrandingLink) {
      homeIconLink = siteConfig.viralBrandingLink;
    } else if (isGadgetPage({ pagetype: config.pagetype }) || config.siteName == gadgetsConfig.gnMetaVal) {
      homeIconLink = siteConfig.gadgetdomain;
    }

    if (isTimesPointsPage) {
      return (
        <div id="headerContainer" className="navbar">
          {!isFrmapp ? (
            <ErrorBoundary>
              <div
                data-adtype="topatf"
                className="ad1 topatf"
                data-id="div-gpt-ad-1540206148700-007"
                data-name={siteConfig.ads.dfpads.topatf}
                data-size="[[320, 100], [320, 50]]"
              />
            </ErrorBoundary>
          ) : null}
        </div>
      );
    }

    let gadgetPage = false;
    if (isGadgetPage(config) || config.siteName == gadgetsConfig.gnMetaVal) {
      gadgetPage = true;
      navdata = navConfig[process.env.SITE];
      navdata = JSON.parse(JSON.stringify(navdata));
      navdata = navdata.map(item => {
        item.weblink = siteConfig.gadgetdomain + item.weblink;
        return item;
      });
    }

    return (
      <React.Fragment>
        <div id="headerContainer" className="navbar">
          {!isFrmapp ? (
            <ErrorBoundary>
              <div
                data-adtype="topatf"
                className="ad1 topatf"
                data-id="div-gpt-ad-1540206148700-007"
                data-name={siteConfig.ads.dfpads.topatf}
                data-size="[[320, 100], [320, 50]]"
              />
            </ErrorBoundary>
          ) : null}
          {/* AlaskaMastHead banner - doesn't come for TP page */}
          {alaskaMastHead && alaskaMastHead._status == "active" && alaskaMastHead._banner != "" ? (
            <div className="alaskamasthead" data-exclude="amp">
              <AnchorLink href={alaskaMastHead._destinationurl}>
                <img src={alaskaMastHead._banner} />
              </AnchorLink>
            </div>
          ) : null}
          {/* AlaskaMastHead banner End */}
          <header className={styles["header"] + " header"}>
            <div
              className={styles["hamburger"] + " hamburger"}
              onClick={this.openNavigation.bind(this)}
              data-exclude="amp"
            >
              {/*<span className={"nav_icon"}></span>*/}
              <i />
              <i />
              <i />
            </div>
            <div className={`logo ${logocssClass}`}>
              <Logocon pagetype={path} router={router}>
                <Link to={logoLink} title={siteConfig.languagefullName}>
                  <img
                    id="logo-img"
                    alt={siteConfig.languagefullName}
                    title={siteConfig.languagefullName}
                    src={logoPath}
                  />
                  {router.location.pathname.endsWith("/tech") ||
                  router.location.pathname.indexOf("tech/amp_articlelist") > -1 ? (
                    <span className="txt-seo">{siteConfig.locale.tech.techNewsH1}</span>
                  ) : null}
                </Link>
                {/*<div className={styles["mbh_notify_popup_close"]+" "+styles["mbh_notify_sprit"]+" mbh_notify_popup_close mbh_notify_sprit"}> <span className={styles["mbh_notify_sprit"]}></span> </div>*/}
              </Logocon>
            </div>
            {/* {videoHeader && !this.state.dockHeader ? null : (videoHeader && this.state.dockHeader ? <div className="dock_icon" onClick={this.dockPlayer.bind(this)}><span></span></div> : */}
            {videoHeader ? null : (
              <React.Fragment>
                <div className="icons search" data-exclude="amp">
                  {isTechSite() && router && router.location.pathname.indexOf("/tech") > -1 ? (
                    <form onSubmit={this.submitSearch.bind(this)} name="srchFrm">
                      <label
                        className="search_icon"
                        htmlFor="search-chk"
                        onClick={() => {
                          this.refs.searchStringInput.focus();
                        }}
                      >
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                          <g fill="none" fill-rule="evenodd">
                            <path
                              d="M15.5 14h-.79l-.28-.27a6.51 6.51 0 10-.7.7l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0a4.5 4.5 0 110-9 4.5 4.5 0 010 9z"
                              fill="#000"
                              fill-rule="nonzero"
                            />
                            <path d="M0 0h24v24H0z" />
                          </g>
                        </svg>
                      </label>
                      <input id="search-chk" type="checkbox" />
                      <div className="table searchbox" id="srchFrm">
                        <div className="table_row">
                          <div className="table_col txtbox">
                            <label className="search_box _hide" htmlFor="searchBox">
                              {siteConfig.locale.search_gadgets}
                            </label>
                            {/* <input
                              ref="searchStringInput"
                              id="searchBox"
                              onChange={this.searchGadgets.bind(this)}
                              type="text"
                              placeholder={siteConfig.locale.search_gadgets}
                              name="query"
                            /> */}
                            <GnSearchBox />
                          </div>

                          <div className="table_col search">
                            <input
                              type="submit"
                              onClick={this.submitSearch.bind(this)}
                              value={siteConfig.locale.search}
                            />
                          </div>
                        </div>
                      </div>
                    </form>
                  ) : (
                    <form onSubmit={this.submitSearch.bind(this)} name="srchFrm">
                      <label
                        className="search_icon"
                        htmlFor="search-chk"
                        onClick={() => {
                          this.refs.searchStringInput.focus();
                        }}
                      >
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                          <g fill="none" fill-rule="evenodd">
                            <path
                              d="M15.5 14h-.79l-.28-.27a6.51 6.51 0 10-.7.7l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0a4.5 4.5 0 110-9 4.5 4.5 0 010 9z"
                              fill="#000"
                              fill-rule="nonzero"
                            />
                            <path d="M0 0h24v24H0z" />
                          </g>
                        </svg>
                      </label>
                      <input id="search-chk" type="checkbox" />
                      <div className="table searchbox" id="srchFrm">
                        <div className="table_row">
                          <div className="table_col txtbox">
                            <label className="search_box _hide" htmlFor="searchBox">
                              {siteConfig.locale.search_placeholder}
                            </label>
                            <input
                              ref="searchStringInput"
                              id="searchBox"
                              type="text"
                              placeholder={siteConfig.locale.search_placeholder}
                              name="query"
                            />
                          </div>
                          <div className="table_col search">
                            <input
                              type="submit"
                              onClick={this.submitSearch.bind(this)}
                              value={siteConfig.locale.search}
                            />
                          </div>
                        </div>
                      </div>
                    </form>
                  )}
                </div>

                <div className="icons app_download">
                  {/* App Download Button siteConfig.applinks.android.hrnav*/}
                  <a
                    className="appdownload_icon"
                    rel="nofollow"
                    title="download app"
                    href={_deferredDeeplink("hrnav", appdeeplink)}
                    // onClick={e =>
                    //   AnalyticsGA.event({
                    //     category: "app_download",
                    //     action: "click",
                    //     label: "hrnav",
                    //   })
                    // }
                  >
                    {getMobileOs() == "android" ? (
                      <SvgIcon name="android" color="#92AF00" className="android" />
                    ) : getMobileOs() == "ios" ? (
                      <SvgIcon name="apple" color="#000000" className="ios" />
                    ) : null}
                    {isAmpPage || getMobileOs() == "unknown" ? (
                      <svg viewBox="0 0 50 23" xmlns="http://www.w3.org/2000/svg">
                        <g fill="#000" fill-rule="nonzero">
                          <path
                            d="M45.2 14.75h-7.4c-.17 0-.3.22-.3.5s.13.5.3.5h7.4c.17 0 .3-.22.3-.5s-.13-.5-.3-.5z"
                            stroke="#000"
                            stroke-width=".5"
                          />
                          <path
                            d="M44.43 9.86a.28.28 0 00-.41-.02l-2.23 2.09V6.05a.29.29 0 10-.58 0v5.87l-2.23-2.08a.28.28 0 00-.4.02c-.11.13-.1.32.02.43l2.42 2.27c.27.25.7.25.96 0l2.42-2.27c.12-.11.13-.3.03-.43h0z"
                            stroke="#000"
                            stroke-width=".6"
                          />
                          <rect
                            x=".75"
                            y=".75"
                            width="47"
                            height="20"
                            rx="3"
                            fill="none"
                            fill-opacity="0"
                            stroke="#000"
                            stroke-width="1.5"
                            transform="translate(1 1)"
                          />
                          <text className={siteConfig.languagemeta + " appicotext"}>
                            <tspan x="0" y="8">
                              {siteConfig.locale.appIconText ? siteConfig.locale.appIconText : "App"}
                            </tspan>
                          </text>
                        </g>
                      </svg>
                    ) : null}
                  </a>
                </div>

                {/* bazaar */}
                {/* {process.env.SITE == "nbt" ? (
                  <div className="apna_bazaar_header">
                    <a href="/apna-bazaar/articlelist/71186319.cms" title="apna bazaar" />
                  </div>
                ) : null} */}
                <div
                  className="icons dropDown_city"
                  onClick={e => {
                    if (e.target.tagName === "use" || e.target.tagName === "svg") {
                      fireGRXEvent("track", "click", { cta: "location_icon" });
                    }
                  }}
                  data-exclude="amp"
                >
                  {this.state.hasMounted ? (
                    <SelectCity sectionId={siteConfig.pages.stateSection} parentComponent="headericon" />
                  ) : null}
                </div>
                {/* Gold Nav */}
                {process.env.SITE == "eisamay" || process.env.SITE == "nbt" ? (
                  <div className="icons goldnav">
                    <AnchorLink href={siteConfig.gold.redirectUrl} target="_blank" title="Gold" rel="">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                        <defs>
                          <linearGradient x1="49.8%" y1=".3%" x2="50.2%" y2="99.7%" id="a">
                            <stop stop-color="#F0B320" offset="0%" />
                            <stop stop-color="#DB9027" offset="99.9%" />
                          </linearGradient>
                        </defs>
                        <g fill-rule="nonzero" fill="none">
                          <path
                            d="M15.294-.004a15.313 15.313 0 01.122 30.626C6.959 30.656.076 23.827.042 15.37.008 6.913 6.837.03 15.294-.004z"
                            fill="url(#a)"
                            transform="translate(.012 .104)"
                          />
                          <path
                            d="M19.012 8.885a4.964 4.964 0 00-3.422-1.277c-2.622 0-4.137 2.231-4.125 4.718.012 3.153 1.509 5.194 4.773 5.194a7.7 7.7 0 001.253-.116 6.327 6.327 0 00.165-1.754l-.006-.837a4.353 4.353 0 00-.379-2.231h2.271a4.289 4.289 0 00-.361 2.231l.006 1.7c.006.495.047.99.122 1.479a23.931 23.931 0 01-3.605.275c-3.569 0-5.977-1.583-5.995-5.414-.023-4.112 2.947-5.988 6.742-5.988.843 0 1.693.122 2.554.189l.007 1.831z"
                            fill="#FFF"
                          />
                          <path
                            d="M27.113 18.04l-.136-.116.3-1.228c.84-3.42-.03-7.034-2.334-9.696a12.455 12.455 0 00-9.551-4.327A12.358 12.358 0 005.874 7a10.763 10.763 0 00-2.261 9.7l.317 1.228-.136.116a1.048 1.048 0 00-.369 1.234l1.5 3.825c.182.445.62.732 1.1.721l1.309.061a2.025 2.025 0 002.391.868 1.8 1.8 0 001.16-2.353l-2.43-6.2a2.025 2.025 0 00-2.507-1.094 1.807 1.807 0 00-1.225 2.1l-.143.128-.207-.819A10.087 10.087 0 016.49 7.449a11.544 11.544 0 018.9-4.045 11.661 11.661 0 018.935 4.045 10.134 10.134 0 012.19 9.075l-.2.819-.143-.128a1.808 1.808 0 00-1.244-2.1 2.019 2.019 0 00-2.5 1.094l-2.378 6.2a1.814 1.814 0 001.179 2.353 2 2 0 002.384-.868l1.309-.061c.481.013.92-.275 1.1-.721l1.471-3.825a1.062 1.062 0 00-.38-1.247h0zM6.224 15.809a1.254 1.254 0 011.5.654l2.43 6.2a1.029 1.029 0 01-.693 1.412 1.232 1.232 0 01-1.51-.654l-2.423-6.2a1.082 1.082 0 01.696-1.412zM4.312 18.6l.693-.611 2.022 5.158-.985-.047a.394.394 0 01-.382-.244l-1.5-3.825a.374.374 0 01.152-.431zM23 23.417a1.219 1.219 0 01-1.5.654 1.027 1.027 0 01-.706-1.412l2.378-6.2a1.238 1.238 0 011.5-.654 1.094 1.094 0 01.706 1.412L23 23.417zm3.758-4.388l-1.471 3.825a.41.41 0 01-.376.244l-.978.049 1.983-5.158.7.611c.141.093.2.271.141.43l.001-.001z"
                            stroke="#FFF"
                            stroke-width=".5"
                            fill="#FFF"
                          />
                        </g>
                      </svg>
                    </AnchorLink>
                  </div>
                ) : null}

                {/* Native Add to Homescreen button */}
                {/* <button id="add_to_home" className="app_download" style={{display : "none"}}>App</button> */}
              </React.Fragment>
            )}
          </header>

          <div className="fixed_bar_with_app">
            {!videoHeader ? (
              <div
                className="hrnavigation"
                id="hrNav"
                itemType="https://www.schema.org/SiteNavigationElement"
                itemScope="1"
              >
                <Logocon pagetype={"h3"} router={router}>
                  <Link itemProp="url" to={homeIconLink} title={siteConfig.languagefullName}>
                    <span className="home_icon" title={siteConfig.wapsitename}>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd">
                          <path fill="currentColor" fill-rule="nonzero" d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
                          <path d="M0 0h24v24H0z" />
                        </g>
                      </svg>
                    </span>
                  </Link>
                </Logocon>

                <div className="nav_scroll" id="hrNavScroll">
                  <ErrorBoundary>
                    <NavBarMobile
                      navdata={navdata}
                      dispatch={this.props.dispatch}
                      // header={header}
                      pagetype={config.pagetype}
                      parentId={config.parentId}
                      subsec1={config.subsec1}
                      secId={config.secId}
                      processNavData={processNavData}
                    />
                  </ErrorBoundary>
                </div>
              </div>
            ) : null}

            <div className="bannerParent">
              {isAppBannerClosed &&
              !globalconfig.isAppInstalled &&
              (config.pagetype == "home" || config.pagetype == "articlelist") ? (
                <div className="open_app_strip mobilehide" data-exclude="amp">
                  <div className="app_txt">
                    <b>{siteConfig.locale.appDownload}</b>
                    An App designed for readers
                  </div>
                  <a
                    className="btn_appdownload"
                    rel="nofollow"
                    title="download app"
                    href={_deferredDeeplink("topStrip", appdeeplink)}
                  >
                    Open APP
                  </a>
                  <button type="button" className="close_icon" onClick={() => this.closeBanner()}></button>
                </div>
              ) : null}
            </div>
          </div>

          <div
            className={styles["greyLayer"] + " greyLayer"}
            onClick={this.closeNavigation.bind(this)}
            data-exclude="amp"
          />
          {this.state.hasMounted ? (
            <div id="mySidenav" className="sidenav">
              <div className="closebtn" onClick={this.closeNavigation.bind(this)}>
                <span className="close_icon" />
              </div>

              <div className="clearfix" />
              <LoginControl timespoints={timespoints} />
              {/* Internation Pages Dropdown */}
              {process.env.SITE == "nbt" ||
              process.env.SITE == "mt" ||
              process.env.SITE == "mly" ||
              process.env.SITE == "eisamay" ? (
                <div className="other-sites">
                  <span className="head">
                    <span>Edition</span>
                    <i title="" rel="">
                      {_isCSR() && (
                        <SvgIcon
                          name={
                            isInternationalUrl(router)
                              ? internationalconfig[siteName].urldropdowm[router.location.pathname].iconName
                              : "flag-india"
                          }
                        />
                      )}
                      {isInternationalUrl(router)
                        ? internationalconfig[siteName].urldropdowm[router.location.pathname].countryName
                        : "IND"}
                    </i>
                  </span>
                  <ul className="restLang">
                    {dropdownconfig &&
                      dropdownconfig.length > 0 &&
                      dropdownconfig.map(item => {
                        if (item.urlPath !== intPathName) {
                          return (
                            <li key={item.countryName + item.iconName}>
                              <AnchorLink key={item.countryName} href={item.urlPath}>
                                <SvgIcon name={item.iconName} />
                                {siteName == "mly" && item.countryName == "UAE" ? "GULF" : item.countryName}
                              </AnchorLink>
                            </li>
                          );
                        }
                      })}
                  </ul>
                </div>
              ) : null}

              {drawerMenu && !gadgetPage ? (
                <div
                  id="topdrawer_content"
                  className={showTopDrawer ? "topdrawer_content visible" : "topdrawer_content visible"}
                  data-exclude="amp"
                >
                  <div className="inner">
                    <ul>
                      {drawerMenu.map((item, index) => (
                        <li key={"key" + index}>
                          <AnchorLink
                            hrefData={{ override: item.link }}
                            onClick={
                              e => fireGRXEvent("track", "click", { cta: "snackbar", sec_name: item.iconname })
                              /*AnalyticsGA.event({
                              category: item.secname,
                              action: "On Click",
                              label: "masthead_mobile",
                              overrideEvent: "snackbar",
                            })*/
                            }
                          >
                            <span className="td-icons">
                              <SvgIcon name={`drawer-${item.iconname}`} />
                              {/* <b className={"td-" + item.classname} /> */}
                            </span>
                            <span className="txt">{item.secname}</span>
                          </AnchorLink>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              ) : null}

              <div className="child_Nav_Con" data-attr="child_nav_con" />
              <div id="subNavigation">
                <span className="active_secName" onClick={this.openHomeNavigation.bind(this)}>
                  <SvgIcon name="back" className="backicon" />
                  <b />
                </span>
                <ul>
                  {Object.keys(this.state.subNavigations).length > 0
                    ? this.mapObject(this.state.subNavigations, (key, value, index, level) => {
                        return this.renderNavElements(value, index, key, level);
                      })
                    : null}
                </ul>
              </div>

              <ul id="mainNavigation">
                {navigations && Object.keys(navigations).length > 0
                  ? this.mapObject(navigations, (key, value, index, level) => {
                      return this.renderNavElements(value, index, key, level, gadgetPage);
                    })
                  : null}
              </ul>
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

/*Header.fetchData = function({dispatch, params, query}){
  return dispatch(fetchHeaderDataIfNeeded(params, query));
}*/

Header.propTypes = {};

function mapStateToProps(state) {
  return {
    // ...state.header
    articleshow: state.articleshow,
    liveblog: state.liveblog,
    photomazzashow: state.photomazzashow,
    videoshow: state.videoshow,
    videolist: state.videolist,
    timespoints: state.timespoints,
  };
}

export default connect(mapStateToProps)(Header);
// export default Header;

const Logocon = ({ children, ...other }) => {
  const router = other.router;
  let element;
  element =
    other.pagetype == "/" || other.pagetype == "/amp_default.cms" ? (
      <h1 title={siteConfig.languagefullName}>{children}</h1>
    ) : other.pagetype == "h3" && other.pagetype !== "/amp_default.cms" ? (
      <h3 itemProp="name" title={siteConfig.languagefullName}>
        {children}
      </h3>
    ) : router &&
      (router.location.pathname.endsWith("/tech") || router.location.pathname.indexOf("tech/amp_articlelist") > -1) ? (
      <h1 itemProp="name" title={siteConfig.languagefullNameGN}>
        {children}
      </h1>
    ) : isInternationalUrl(router) ? (
      <h1 itemProp="name" title={siteConfig.languagefullName}>
        {children}
      </h1>
    ) : (
      <h2 itemProp="name" title={siteConfig.languagefullName}>
        {children}
      </h2>
    );

  return element;
};

/**
 * In this function nested objects are removed only parent level properties are reserved.
 * This is done so that we can minimize the initial state.
 * @param {*} data
 */
function processNavData(data) {
  const navdata = JSON.parse(JSON.stringify(data));
  navdata &&
    navdata.map(item => {
      return Object.keys(item).forEach(key => {
        if (typeof item[key] === "object") {
          delete item[key];
        }
      });
    });

  return navdata;
}

const NavBarMobile = ({ navdata, dispatch, pagetype, parentId, subsec1, secId, processNavData }) => {
  if (!_isCSR() && navdata) {
    navdata = processNavData(navdata);
    dispatch(fetchNavDataSuccess(navdata));
  }

  return (
    <ul id="paginationHomePage" className="hrnavicons">
      {navdata &&
        Array.isArray(navdata) &&
        navdata.map((item, index) => {
          //  initilizing className=false, does'nt set blank class atrribute
          //  if no class is there;
          const iscmpPage = ["comparelist", "compareshow"].includes(pagetype);
          const isGadgetPage = ["gadgetlist", "gadgetshow"].includes(pagetype);

          let classNames = "";
          if ((item.msid && secId && item.msid == secId) || (item._sec_id && item._sec_id == secId)) {
            if (!iscmpPage && !isGadgetPage) {
              classNames = "active";
            }
          }

          if ((iscmpPage && item._pagetype == "compare") || (isGadgetPage && item._pagetype == "gadget")) {
            classNames = "active";
          } else if (item.class && item.class == "mobilehide") {
            classNames = "mobilehide";
          }

          if (item._classMobile) {
            classNames += ` ${item._classMobile}`;
          }

          let webURL = item.weblink;
          //if (webURL && !webURL.includes("utm_source")) {
          // webURL = `${item.weblink}?utm_source=navigation`;
          //}

          return (
            <li className={`${classNames} ${item._icon ? "isicon" : ""}`} itemProp="name" key={index}>
              {
                <AnchorLink href={webURL} className="nav_link" itemProp="url">
                  {_isCSR() && item._icon ? <SvgIcon name={`hrnav-${item._icon}`} /> : item.label}
                </AnchorLink>
              }
            </li>
          );
        })}
    </ul>
  );
};
