import React, { PureComponent } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { _getStaticConfig, getURL, isMobilePlatform, isGadgetPage } from "../../utils/util";
import gadgetsConfig from "../../utils/gadgetsConfig";
const siteConfig = _getStaticConfig();

const siteName = process.env.SITE;

class AnchorLink extends PureComponent {
  constructor(props) {
    super(props);
  }

  generateUrl(item, isrelative) {
    let urlDomain = isrelative ? "" : siteConfig.mweburl;
    if (item.site == gadgetsConfig.gnMetaVal) {
      urlDomain = siteConfig.gadgetdomain;
    }
    try {
      // if(item.override && (item.override=="javascript:void(0)" || item.override=="javascript:void()")) return item.override;
      // Check if item contains override link
      if (item.override) {
        let url = item.override;
        // Check if url contains any domain
        // If url contains internal web domain and route exist then change url to mweb domain
        // If url contains external domain return as it is
        // If url does not any domain(relative) and route exist then append mweb domain else append web domain
        if (url.indexOf("www.") > -1 || url.indexOf("http") > -1) {
          url =
            url.startsWith && url.startsWith("http:") && url.indexOf(siteConfig.webdomain) && this.routeExists(url)
              ? url.replace("http:", "https:")
              : url;
          if (this.checkWebsiteLink(url) && url.indexOf(siteConfig.mwebdomain) == -1 && this.routeExists(url)) {
            url = url.replace(siteConfig.webdomain, siteConfig.mwebdomain);
          }
        } else {
          url =
            (this.routeExists(url) ? urlDomain : urlDomain) + (url.startsWith && url.startsWith("/") ? url : `/${url}`);
        }
        return url;
      }
      if (item.seolocation) {
        // if item has seolocation, generate id using seolocation field and template
        const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
        let url = `${item.seolocation}/${templateName}/${item.id}.cms`;
        url = url.startsWith && url.startsWith("/") ? url : `/${url}`;
        if (!isrelative) {
          url = urlDomain + url;
        }
        return url;
      }
      // if item does not contain override link
      // Create Absolute link with web or mweb domain base on route
      const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
      let url = `${(item.seo ? `${item.seo}/` : "") + templateName}/${item.id}.cms`;
      // url = (this.routeExists(url) ? siteConfig.mweburl : siteConfig.weburl) + (url.startsWith('/') ? url : ("/" + url));
      url = urlDomain + (url.startsWith && url.startsWith("/") ? url : `/${url}`);
      return url;
    } catch (ex) {}
  }

  routeExists(url) {
    // if (siteName == 'nbt' && ((url.indexOf('/tech') < 0 && url.indexOf('/photoarticlelist/') < 0 && url.indexOf('/photoshow/') < 0 && url.indexOf('/videolist/') < 0 && url.indexOf('/videoshow/') < 0) || (url.indexOf('/astro/') > -1 && url.indexOf('/videoshow/') < 0 && url.indexOf('/videolist/') < 0))) return false; remove for tech open internally
    if (
      url == "/" ||
      url == siteConfig.mweburl ||
      url == `${siteConfig.mweburl}/` ||
      url == siteConfig.weburl ||
      url == `${siteConfig.weburl}/` ||
      url == `${siteConfig.weburl}/tech` ||
      url == `${siteConfig.mweburl}/tech` ||
      (url.includes(siteConfig.weburl) && url.includes("/tech/"))
    )
      return true;
    return siteConfig.routes.filter(route => url.indexOf(route) > -1).length > 0;
  }

  // Check desktop, mobile, photgallery to set Target blank & set no-follow.
  checkWebsiteLink(url, item) {
    // if (url && url.indexOf("liveblog") > -1) {
    // this was done as liveblog is not in react
    // return false;
    // }
    if (url.includes(siteConfig.gadgetdomain)) {
      //item && item.site == gadgetsConfig.gnMetaVal &&
      return true;
    }

    return url.indexOf("tamilsamayam.onelink.me") > -1 ||
      url.indexOf("malayalamsamayam.onelink.me") > -1 ||
      url.indexOf("go.onelink.me") > -1
      ? true
      : !!(
          (url.indexOf(siteConfig.webdomain) > -1 ||
            url.indexOf(siteConfig.mwebdomain) > -1 ||
            url.indexOf(siteConfig.weburl) > -1) &&
          url.indexOf(siteConfig.photogallerydomain) == -1
        );
  }

  // app exclusive onelink domain to remove Target blank & set no-follow.
  checkAppExclusiveLink(url) {
    return !!(
      url.indexOf("tamilsamayam.onelink.me") > -1 ||
      url.indexOf("malayalamsamayam.onelink.me") > -1 ||
      url.indexOf("go.onelink.me") > -1
    );
  }

  // Append frmapp=yes if we are hitting the link with frmapp=yes
  appendfrmAppIfNeeded(url) {
    const query =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    // if(query && query.frmapp && query.frmapp.toLowerCase() == "yes" && url && url.toLowerCase().indexOf("frmapp=yes") == -1){
    if (
      query &&
      query.toLowerCase().indexOf("frmapp=yes") > -1 &&
      url &&
      url.toLowerCase().indexOf("frmapp=yes") == -1
    ) {
      return `${url + (url.indexOf("?") > -1 ? "&" : "?")}frmapp=yes`;
    }
    return url;
  }

  // Remove frmApp if it mistakenly comes from feed via override etc. This is only needed if we are hitting the link with frmapp=yes
  removefrmApp(url) {
    // return url && (url.indexOf('?frmapp=yes&') > -1 ? url.replace("frmapp=yes&", "") : url.indexOf('?frmapp=yes') > -1 ? url.replace("frmapp=yes", "") : url.indexOf('&frmapp=yes') > -1 ? url.replace("&frmapp=yes", "") : url);
    return (
      url &&
      url
        .replace("&frmapp=yes", "")
        .replace("frmapp=yes&", "")
        .replace("frmapp=yes", "")
    );
  }

  fireDeepLink(e) {
    if (!isMobilePlatform() || (ua && ua.match("firefox/"))) {
      return;
    }

    if (ua.indexOf("iphone") == "-1" && ua.indexOf("macintosh") == "-1") {
      const contentid =
        e.currentTarget.getAttribute("data-id") != null && e.currentTarget.getAttribute("data-id") != undefined
          ? e.currentTarget.getAttribute("data-id")
          : null;
      const contenttn =
        e.currentTarget.getAttribute("data-tn") != null && e.currentTarget.getAttribute("data-tn") != undefined
          ? e.currentTarget.getAttribute("data-tn")
          : null;
      const contentdomain = contenttn != null && contenttn == "photostrip" ? "p" : "t";

      // URL For HTML View Only
      let url =
        e.currentTarget.getAttribute("data-url") != null && e.currentTarget.getAttribute("data-url") != undefined
          ? e.currentTarget.getAttribute("data-url")
          : e.currentTarget.getAttribute("href")
          ? e.currentTarget.getAttribute("href")
          : null;
      // To make URL Absolute
      if (url != undefined && url.startsWith("/")) {
        url = siteConfig.mweburl + url;
      }
      // Add frmapp=yes for webview
      if (url != undefined && !(url.indexOf("frmapp=yes") > -1)) {
        if (url.indexOf("?") > -1) {
          url += "&frmapp=yes";
        } else {
          url += "?frmapp=yes";
        }
      }

      if (
        typeof window !== "undefined" &&
        contentid != null &&
        contenttn != null &&
        (contenttn == "html" ||
          contenttn == "news" ||
          contenttn == "photo" ||
          contenttn == "video" ||
          contenttn == "photostrip" ||
          contenttn == "photofeature" ||
          contenttn == "photoFeature" ||
          contenttn == "recipe")
      ) {
        window.location.href = `${siteConfig.deeplinkappid}://open-$|$-${siteConfig.deeplinkpubid}-$|$-type=${
          contenttn != null ? contenttn : "html"
        }-$|$-domain=${contentdomain}-$|$-id=${
          (contenttn == "html" || contenttn == null) && url != null ? url : contentid
        }`;
      }
    }
  }

  // Used to fire CTN click call when displaying personalized stories
  fireClickPixel(e, link) {
    if (!link) return;
    const head = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
    if (head) {
      const img = document.createElement("img");
      img.setAttribute("class", "ctnclickpixel");
      img.src = link;
      // console.log("Firing Pixel");
      head.appendChild(img);
      head.removeChild(img);
    }
  }

  // Provide Data in hrefData(override, seo, tn, id)
  // if getUrl passed than only url will be returned
  // Will give preference to overridelink
  render() {
    const props = this.props;

    const hrefObject = props.href ? { override: props.href } : props.hrefData;
    let url = hrefObject ? this.generateUrl(hrefObject, props.isrelative) : "";
    const _isfrmapp = typeof window !== "undefined" && window.location.href.indexOf("frmapp=yes") > -1;
    if (_isfrmapp) {
      url = this.appendfrmAppIfNeeded(url);
    } else {
      url = this.removefrmApp(url);
    }
    // below line is used to append utm source for trending widget
    url = props.istrending ? getURL(url) : url;
    url = url && url.trim();
    const parsedurl = url && url.replace(/  +/g, " ").replace(/ /g, "-");
    // url = url && url.toLowerCase();
    const deeplinkbind = !!(
      hrefObject &&
      hrefObject.id != null &&
      (hrefObject.app_tn == "html" ||
        hrefObject.tn == "news" ||
        hrefObject.tn == "photo" ||
        hrefObject.tn == "video" ||
        hrefObject.tn == "photostrip" ||
        (hrefObject && hrefObject.app_tn && hrefObject.app_tn.toLowerCase() == "photofeature") ||
        hrefObject.tn == "recipe")
    );
    const {
      href,
      hrefData,
      children,
      dispatch,
      routing,
      fireClickPixel,
      preventDefault,
      istrending,
      hardNav,
      config,
      ...eleAttributes
    } = props;
    // Only return URL
    if (props.getUrl) {
      return url;
    }
    let intermediatepageUrl;
    let isAmazonUrl = false;
    if (url.indexOf("www.amazon.in") > -1) {
      isAmazonUrl = true;
      if (url.indexOf("tag=") == -1) {
        url += `${url.indexOf("?") > -1 ? "&" : "?"}tag=${process.env.SITE}_${
          process.env.PLATFORM == "desktop" ? "web" : "wap"
        }_apnawidget-21`;
      }
      intermediatepageUrl = `${siteConfig.mweburl}/pwafeeds/affiliate_amazon.cms?url=${encodeURIComponent(url)}`;
    } else {
      isAmazonUrl = false;
    }

    let parsedurlUpdated = parsedurl;

    if (props && props.shouldUppercase !== "true") {
      parsedurlUpdated = parsedurl && parsedurl.toLowerCase();
    }

    delete eleAttributes.shouldUppercase;

    const gadgetPage = isGadgetPage(config);

    if (gadgetPage && url && !url.includes(siteConfig.gadgetdomain)) {
      return (
        <a
          href={parsedurlUpdated}
          {...eleAttributes}
          rel="nofollow"
          onClick={e => (preventDefault ? e.preventDefault : false)}
          target="_blank"
        >
          {props.children}
        </a>
      );
    }

    return this.routeExists(url) &&
      this.checkWebsiteLink(url, hrefObject) &&
      url &&
      !url.includes("web-stories") &&
      !url.includes("/astro/") &&
      !hardNav ? (
      <Link
        onClick={
          deeplinkbind
            ? e => {
                if (preventDefault) {
                  e.preventDefault();
                }
                {
                  !_isfrmapp && this.fireDeepLink(e);
                }
                this.fireClickPixel(e, fireClickPixel);
              }
            : fireClickPixel
            ? e => {
                if (preventDefault) {
                  e.preventDefault();
                }
                this.fireClickPixel(e, fireClickPixel);
              }
            : preventDefault
            ? e => e.preventDefault()
            : null
        }
        data-tn={
          hrefObject.app_tn
            ? hrefObject.app_tn
            : hrefObject.tn && url && !url.includes("web-stories")
            ? hrefObject.tn
            : "tn"
        }
        data-url={hrefObject.url && hrefObject.url != "" ? hrefObject.url : null}
        data-id={hrefObject.id}
        to={parsedurlUpdated}
        key="anchorlink" // + Math.random(10)}
        {...eleAttributes}
      >
        {props.children}
      </Link>
    ) : this.checkAppExclusiveLink(url) ? (
      <a
        href={url}
        rel="noopener nofollow"
        onClick={
          fireClickPixel
            ? e => {
                if (preventDefault) {
                  e.preventDefault();
                }
                this.fireClickPixel(e, fireClickPixel);
              }
            : preventDefault
            ? e => e.preventDefault()
            : null
        }
        {...eleAttributes}
      >
        {props.children}
      </a>
    ) : this.checkWebsiteLink(url) ? (
      <a
        href={parsedurl && parsedurl.toLowerCase()}
        onClick={
          fireClickPixel
            ? e => {
                if (preventDefault) {
                  e.preventDefault();
                }
                this.fireClickPixel(e, fireClickPixel);
              }
            : preventDefault
            ? e => e.preventDefault()
            : null
        }
        {...eleAttributes}
      >
        {props.children}
      </a>
    ) : (
      <a
        href={isAmazonUrl ? intermediatepageUrl : url}
        rel={url && !url.includes("liveblog") ? "noopener nofollow" : ""}
        target="_blank"
        onClick={preventDefault ? e => e.preventDefault() : null}
        {...eleAttributes}
      >
        {props.children}
      </a>
    );
  }
}

function mapStateToProps(state) {
  return {
    routing: state.routing,
    config: state.config,
  };
}

export default connect(mapStateToProps)(AnchorLink);
