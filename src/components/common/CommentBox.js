import React, { Component } from "react";
import ReactDOM from "react-dom";
import fetchJsonP from "fetch-jsonp";
import AdCard from "./AdCard";
import { _getStaticConfig, _getCookie, _setCookie, _isCSR } from "../../utils/util";
import { connect } from "react-redux";
import "./css/commentBox.scss";
import ImageCard from "../common/ImageCard/ImageCard";
import fetch from "../../utils/fetch/fetch";
import { openLoginPopup } from "./ssoLogin";
import makeRequest from "../common/TimesPoints/makeRequest";
import SvgIcon from "./SvgIcon";

const siteConfig = _getStaticConfig();
const itemPerPage = 10;
class CommentBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      IsExpanded: false,
      commentsData: null,
      totalComments: [],
      msid: props.msid,
      pageno: 1,
      loggedIn: false,
      userInfo: {},
      commentposted: false,
      isRepliedCliked: false,
    };
  }

  fetchComments = (msid, pageno) => {
    const objToQueryStr = obj => {
      if (obj && typeof obj === "object" && obj.constructor === Object) {
        return Object.keys(obj)
          .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
          .join("&");
      }
      return "";
    };

    const commentsParams = Object.assign({}, siteConfig.Comments.allComments);
    commentsParams.size = itemPerPage;
    commentsParams.pagenum = pageno;
    //const loggedIn = _getCookie("lgc_ssoid") ? true : false; // changes lgc_ssoid to ssoid post development

    fetch(`${process.env.API_BASEPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}&msid=${msid}`)
      .then(commentsData => {
        if (commentsData && commentsData.length == 0) {
          this.setState({ totalComments: "no comment" });
        } else {
          let commentsArr = [...commentsData];
          commentsArr.shift();
          const AllComments = [];
          const totalComments = this.getAllComments(commentsArr, AllComments);
          // console.log("allComments", totalComments);

          // this.setState({ commentsData, pageno, totalComments });
          this.setState(previousState => ({
            commentsData,
            pageno,
            totalComments: [...previousState.totalComments, ...totalComments],
          }));
        }
      })
      .catch(() => null);
  };

  userLoginSuccess(e) {
    console.log("dddddd11", e.detail);
    this.setState({ userInfo: e.detail });
  }
  componentDidMount() {
    const { msid } = this.props;
    const { pageno } = this.state;

    if (typeof document) {
      document.addEventListener("user_comment", this.userLoginSuccess);
    }

    this.fetchComments(msid, pageno);
  }

  componentDidUpdate(prevProps) {
    const { showCommentsPopup } = this.props;
    const bodyElement = document.querySelector("body");
    if (!prevProps.showCommentsPopup && showCommentsPopup) {
      bodyElement.classList.add("disable-scroll");
    } else if (prevProps.showCommentsPopup && !showCommentsPopup) {
      bodyElement.classList.remove("disable-scroll");
    }
  }

  onExpand = () => {
    if (!this.state.IsExpanded) {
      // outerHeight.current = containerRef.current.scrollHeight;
      this.setState({
        IsExpanded: true,
      });
    }
  };

  onChange = (commentId, event) => {
    //  console.log("onchange", event);
    if (event.target.value && event.target.value.trim()) {
      if (commentId) {
        domSelector(commentId, ".status-message").innerText = "";
      } else {
        document.querySelector("#parent-form-msg").innerText = "";
      }
    }
    // setCommentValue(e.target.value);
  };

  postComment({ userComment, userInfo, parentId, msid }) {
    const commentsInfo = {
      fromname: userInfo.FL_N,
      fromaddress: userInfo.EMAIL,
      userid: userInfo.uid,
      loggedstatus: 1,
      message: userComment,
      roaltdetails: 1,
      ArticleID: msid,
      article_id: msid,
      msid: msid,
      rchid: siteConfig.siteid,
      langid: 2,
      medium: "WAP",
      imageurl: userInfo.thumb,
      parentid: parentId || 0,
      rootid: 0,
      pcode: siteConfig.sso.SSO_CHANNEL,
      rotype: 3,
    };

    const config = {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
      },
    };

    makeRequest
      .postData(`${process.env.WEBSITE_URL}/pwafeeds/postro.cms`, encodeForm(commentsInfo), config)
      .then(response => {
        this.setState({ commentposted: true });
        if (parentId) {
          domSelector(parentId, ".status-message").innerHTML = "Your comment has been successfully submitted";
          domSelector(parentId, ".status-message").style.display = "block";
          domSelector(parentId, ".status-message").classList.add("sucussmsg");

          this.closeCommentBox(parentId);
        } else {
          document.querySelector("#comment").value = "";
        }
      })
      .catch(() => {
        //console.log(err);
      });
  }

  formSubmitHandler = e => {
    e.preventDefault();
    console.log("thisss", this.props);
    // const { userInfo } = this.state;
    const { msid, authentication } = this.props;
    const userInfo = authentication && authentication.userData;
    let userComment = e.target.comment.value;
    userComment = userComment && userComment.trim();
    if (userComment) {
      if (userInfo) {
        document.querySelector("#parent-form-msg").innerText = "";
        this.postComment({ userComment, userInfo, msid });
      } else {
        //openLoginPopup();
        this.loginClick();
      }
    } else {
      document.querySelector("#parent-form-msg").innerText = "Please enter your comment";
    }
  };

  replyFormSubmitHandler = (parentId, event) => {
    event.preventDefault();
    const { authentication, msid } = this.props;
    const userInfo = authentication && authentication.userData;
    let userComment = event.target.comment.value;
    userComment = userComment && userComment.trim();
    domSelector(parentId, ".status-message").classList.remove("sucussmsg");
    if (userComment) {
      if (userInfo) {
        domSelector(parentId, ".status-message").innerText = "";
        this.postComment({ userComment, userInfo, parentId, msid });
      } else {
        this.loginClick();
      }
    } else {
      domSelector(parentId, ".status-message").innerText = "Please enter your comment";
    }
  };

  closeCommentBox = commentId => {
    domSelector(commentId, ".form-area").style.display = "none";
    domSelector(commentId, ".reply").style.display = "block";
    // domSelector(commentId, ".status-message").innerHTML = "";
  };

  userActionOnOffensiveForm = (commentId, actionType, userId) => {
    const { authentication, msid } = this.props;
    const userInfo = authentication && authentication.userData;

    if (actionType == "close") {
      domSelector(commentId, ".flag_comment_box").style.display = "none";
      return;
    } else if (actionType == "other") {
      domSelector(commentId, ".offensive_popup_reason").style.display = "block";
      return;
    } else if (actionType == "submit" && userInfo && userInfo.uid == userId) {
      domSelector(commentId, ".error").innerText = "You can't mark your own comment offensive";
      return;
    }

    if (!userInfo) {
      this.loginClick();
      return;
    }

    const radioButton = domSelector(commentId, ".flag_comment_box input[name='radiotype']:checked");

    let radioButtonVal = radioButton && radioButton.value;

    if (radioButtonVal == "Others") {
      radioButtonVal = domSelector(commentId, ".offensive_popup_reason").value;
      radioButtonVal = radioButtonVal.trim();
    }

    if (!radioButtonVal) {
      domSelector(commentId, ".error").innerText = "Please select reason";
      return;
    }

    const offensivePayload = {
      ofusername: userInfo.FL_N,
      ofreason: radioButtonVal,
      ofcommenteroid: commentId,
      ofcommenthostid: siteConfig.hostid,
      ofcommentchannelid: siteConfig.siteid,
      ofcommentid: msid,
      ofuserisloggedin: 1,
      ofuserssoid: userInfo.EMAIL,
      ofuseremail: userInfo.EMAIL,
      medium: "WAP",
    };

    makeRequest.get(`${process.env.WEBSITE_URL}ratecomment_new.cms?rateid=0&opinionid=${commentId}&typeid=103`);

    makeRequest.get(`${process.env.WEBSITE_URL}offensive/mark?${encodeForm(offensivePayload)}`).then(response => {
      // console.log("res", response);
      _setCookie("votec_" + commentId, commentId + ":" + "flagged", 30);
      domSelector(commentId, ".error").innerText = "";
      domSelector(commentId, ".flag_comment_box .reasons").style.display = "none";
      domSelector(commentId, ".flag_comment_box .submit").style.display = "none";
      domSelector(commentId, ".flag_comment_box h6").style.display = "none";
      domSelector(commentId, ".success-message").innerText = "Your objection has been sent to moderator"; //siteConfig.locale.comment.offensiveSuccess;
    });
  };

  validateAction(commentId, type) {
    var cookieval = _getCookie("votec_" + commentId);
    if (cookieval) {
      cookieval = cookieval.split(":")[1];
      const divStatusMsg = domSelector(commentId, ".status-message");
      if (cookieval == "agree" && type == "agree") {
        divStatusMsg.innerText = "You have already agreed this";
      } else if (cookieval == "disagree" && type == "disagree") {
        divStatusMsg.innerText = "You have already disagreed this";
      } else if ((cookieval == "agree" && type == "disagree") || (cookieval == "disagree" && type == "agree")) {
        divStatusMsg.innerText = "You Can't mark the same review agree and disagree";
      } else if (cookieval == "agree" && type == "offensive") {
        divStatusMsg.innerText = "You Can't mark the same review agree and then offensive";
      } else if (cookieval == "flagged") {
        divStatusMsg.innerText = "You have already Flagged this";
      } else {
        return true;
      }
      return false;
    }

    return true;
  }

  userActionOnComment = (commentId, type, userId) => {
    const { authentication, msid } = this.props;
    let userInfo = authentication && authentication.userData;

    if (userInfo && userInfo.uid == userId) {
      domSelector(commentId, ".status-message").innerText = "You Can't react on your own comment";
      return;
    }
    if (type != "reply" && type != "offensive" && !userInfo) {
      this.loginClick();
      return;
    }
    if (type != "reply") {
      const isValid = this.validateAction(commentId, type);
      if (!isValid) {
        return;
      }
    }

    if (type == "reply") {
      domSelector(commentId, ".form-area").style.display = "block";
      domSelector(commentId, ".reply").style.display = "none";
    } else if (type == "agree" || type == "disagree") {
      const aType = type == "agree" ? "Agreed" : "Disagreed";
      const mytConfig = {
        appKey: siteConfig.Comments.appkey,
        parentCommentId: commentId,
        activityType: aType,
        baseEntityType: "ARTICLE",
        objectType: "A",
        url: `${process.env.WEBSITE_URL.slice(0, -1)}${document.location.pathname}`,
      };

      const typeId = type == "agree" ? "100" : "101";
      if (type == "agree") {
        let rateContainer = domSelector(commentId, ".comment-agree");
        let rateCount = parseInt(rateContainer.innerText);
        rateContainer.innerText = rateCount + 1;
      } else if (type == "disagree") {
        let rateContainer = domSelector(commentId, ".comment-disagree");
        let rateCount = parseInt(rateContainer.innerText);
        rateContainer.innerText = rateCount + 1;
      }

      // Fire rating Call

      makeRequest.get(`${process.env.WEBSITE_URL}ratecomment_new.cms?rateid=0&opinionid=${commentId}&typeid=${typeId}`);
      // Fire My Times Call
      const apiRateComment = `https://myt.indiatimes.com/mytimes/addActivity?${encodeForm(mytConfig)}`;
      fetchJsonP(apiRateComment)
        .then(response => response.json())
        .then(() => {
          _setCookie("votec_" + commentId, commentId + ":" + type, 30);
        })
        .catch(err => {
          console.log("error while voting", err);
        });
    } else if (type == "offensive") {
      let disAgreeContainer = domSelector(commentId, ".comment-disagree");
      let agreeCount = parseInt(disAgreeContainer.innerText);
      disAgreeContainer.innerText = agreeCount + 1;
      domSelector(commentId, ".flag_comment_box").style.display = "block";
    }
  };

  loadMoreComments = msid => {
    const { pageno } = this.state;
    console.log("Load more..");
    this.fetchComments(msid, pageno + 1);
  };

  loginClick = () => {
    openLoginPopup("", "wap_comment");
  };

  getAllComments(commentsArr, AllComments) {
    if (Array.isArray(commentsArr) && commentsArr.length > 0) {
      commentsArr.forEach(item => {
        AllComments.push(item);
        if (item.CHILD && Array.isArray(item.CHILD)) {
          this.getAllComments(item.CHILD, AllComments);
        }
      });
      return AllComments;
    }
  }

  render() {
    const { commentsData, pageno, commentposted, isRepliedCliked, totalComments } = this.state;
    const { closeCommentsBox, msid, authentication, headline } = this.props;
    const loggedIn = authentication && authentication.loggedIn;
    const userName = authentication && authentication.userData && authentication.userData.F_N;
    const userImage = authentication && authentication.userData && authentication.userData.thumb;

    let statusMessage = "";
    let msgClass = "";
    if (commentposted) {
      statusMessage = "Your comment has been successfully submitted";
      msgClass = "sucussmsg";
    }

    let commentCount = 0;

    if (commentsData && Array.isArray(commentsData) && commentsData.length > 0) {
      commentCount = parseInt(commentsData[0].totalcount);
    }

    const loadmore = commentCount > pageno * itemPerPage;

    return _isCSR()
      ? ReactDOM.createPortal(
          <div className="commentsWap">
            <div className="comments-body">
              <div className="container">
                <div className="bottom-comments-btns">
                  <div class="cmntheading">
                    <span className="closebutton" onClick={closeCommentsBox}></span>
                    <div class="headofcmnt">
                      {commentCount != 0 ? (
                        <h3>
                          <b>Comments({commentCount})</b>
                        </h3>
                      ) : (
                        ""
                      )}
                      <h2 className="text_ellipsis">{headline}</h2>
                    </div>
                  </div>
                </div>
                <span className={msgClass} id="parent-form-msg">
                  {statusMessage}
                </span>
                {userName && (
                  <div className="user-info">
                    <img width="22" height="22" src={userImage} title="userName" />
                    <span className="name"> {userName}</span>
                  </div>
                )}

                <form onSubmit={this.formSubmitHandler}>
                  <textarea
                    className="commentbox"
                    placeholder="Write Comment"
                    name="comment"
                    id="comment"
                    onChange={this.onChange.bind(this, "")}
                  />
                  <div className="actions">
                    <button type="submit">{loggedIn ? "Submit" : "Login to Comment"}</button>
                  </div>
                </form>
              </div>

              {totalComments == "no comment" && <div className="no-comment-text">Be the first one to review</div>}

              <div>
                <AdCard mstype="mrec1" adtype="dfp" />
              </div>

              <div className="comments-wrapper" role="button" tabIndex={0}>
                {totalComments && Array.isArray(totalComments) && totalComments.length > 0 ? (
                  <div className="bottom-comments-popup">
                    {totalComments.map(item => {
                      let className = [];
                      if (item.C_R_EROID != null) {
                        className.push("color-gray");
                      }
                      if (item && item.C_A_ID) {
                        className.push("authorComment");
                      }

                      const userId = item.user_detail && item.user_detail.uid;

                      return (
                        <div className={`comment-box ${className.join(" ")}`} key={item.C_T}>
                          <div className="user-thumbnail">
                            <ImageCard
                              type="absoluteImgSrc"
                              src={
                                (item.user_detail && item.user_detail.thumb) ||
                                `${process.env.WEBSITE_URL}/photo/11834507.cms`
                              }
                              className="bg_poster"
                            />
                            <a
                              to={item.A_ID ? `${siteConfig.sso.PROFILE_URL}/${item.A_ID}` : ""}
                              className="name"
                              target="_blank"
                            >
                              {item.user_detail && item.user_detail.FL_N ? item.user_detail.FL_N : item.A_D_N}
                            </a>
                            <span className="commented-at">{item.C_D}</span>
                          </div>
                          <p className="short_comment" dangerouslySetInnerHTML={{ __html: item.C_T }} />
                          <div className="footbar clearfix" id={`comment_block_${item._id}`}>
                            <div className="status-message"></div>

                            <div className="cmt_actionbar">
                              <div
                                id={`comment_form_${item._id}`}
                                style={{ display: "none" }}
                                className="form-area replybox"
                              >
                                <form
                                  onSubmit={this.replyFormSubmitHandler.bind(this, item._id)}
                                  name={`comment_form_${item._id}`}
                                >
                                  <textarea
                                    className="commentbox"
                                    placeholder="Write Comment"
                                    name="comment"
                                    onChange={this.onChange.bind(this, item._id)}
                                  />
                                  <div className="actions">
                                    {loggedIn ? (
                                      <button type="submit">Submit</button>
                                    ) : (
                                      <button type="submit">Login to Comment</button>
                                    )}
                                  </div>
                                </form>
                                <span className="closebutton" onClick={() => this.closeCommentBox(item._id)}></span>
                              </div>
                              <span
                                commentsdata-action="comment-reply"
                                onClick={() => this.userActionOnComment(item._id, "reply", userId)}
                                className="reply"
                              >
                                reply{/* {siteConfig.locale.comment.reply} */}
                              </span>
                              <span
                                onClick={() => this.userActionOnComment(item._id, "agree", userId)}
                                className="comment-agree"
                                title="Agreed"
                              >
                                <SvgIcon name="thumbsup" className="thumbsup" /> {item.AC_A_C}
                              </span>
                              <span
                                onClick={() => this.userActionOnComment(item._id, "disagree", userId)}
                                className="comment-disagree"
                                title="DisAgreed"
                              >
                                <SvgIcon name="thumbsup" className="thumbsdown" /> {item.AC_D_C}
                              </span>
                              <span
                                onClick={() => this.userActionOnComment(item._id, "offensive", userId)}
                                className="offensive comment-offensive"
                                title="Offensive"
                              >
                                {/* {siteConfig.locale.comment.offensive} */}
                                offensive
                              </span>
                              <FormOffensive
                                commentId={item._id}
                                userActionOnOffensiveForm={this.userActionOnOffensiveForm}
                                closeOffensivePopup={this.userActionOnOffensiveForm}
                                offensiveOtherClick={this.userActionOnOffensiveForm}
                                userId={userId}
                              />
                            </div>
                          </div>
                        </div>
                      );
                    })}

                    {loadmore && (
                      <div className="loadmore">
                        <button className="button" onClick={() => this.loadMoreComments(msid)}>
                          More Comments
                        </button>
                      </div>
                    )}
                  </div>
                ) : totalComments != "no comment" ? (
                  <div className="loadinggif">
                    <img src={`${process.env.IMG_URL}/photo/29439462.cms`} />
                  </div>
                ) : (
                  ""
                )}
              </div>
              {/* <CommentsSection
            msid={msid}
            commentsData={totalComments}
            loadmore={loadmore}
            replyFormSubmitHandler={this.replyFormSubmitHandler}
            loadMoreComments={() => this.loadMoreComments(msid)}
            userActionOnComment={this.userActionOnComment}
            closeCommentBox={this.closeCommentBox}
            isRepliedCliked={isRepliedCliked}
            loggedIn={loggedIn}
          /> */}
            </div>
          </div>,
          document.getElementById("position-fixed-floater"),
        )
      : "";
  }
}

const removeSpcChar = str => {
  const removed = str.replace(/<\/?[^>]+(>|$)/g, "");
  return removed;
};

const domSelector = (commentId, selector) => {
  return document.querySelector(`#comment_block_${commentId} ${selector}`);
};

const encodeForm = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
};

const mapStateToProps = state => ({
  authentication: state.authentication,
});

const FormOffensive = ({ commentId, userActionOnOffensiveForm, userId }) => {
  return (
    <div className="popup_badge popup_above flag_comment_box" style={{ display: "none" }}>
      {/* <div className="heading-block">
        <h4></h4>
      </div> */}
      <div className="content clearfix flagcontainer">
        <span className="success-message"></span>

        <h6>{siteConfig.locale.comment.offensiveReason || "Reason for reporting"}</h6>

        <ul className="reasons" data-action="option_list">
          <li>
            <label data-val="fl">
              <input name="radiotype" value="Foul language" type="radio" />
              <b>{siteConfig.locale.comment.offLang || "Foul language"}</b>
            </label>
          </li>
          <li>
            <label data-val="dfame">
              <input name="radiotype" value="Defamatory" type="radio" />
              <b>{siteConfig.locale.comment.defame || "Defamatory"}</b>
            </label>
          </li>
          <li>
            <label data-val="community">
              <input name="radiotype" value="Inciting hatred against a certain community" type="radio" />
              <b>{siteConfig.locale.comment.community || "Inciting hatred against a certain community"}</b>
            </label>
          </li>
          <li>
            <label data-val="other">
              <input
                name="radiotype"
                value="Others"
                type="radio"
                onClick={() => userActionOnOffensiveForm(commentId, "other")}
              />
              <b>Others</b>
            </label>
          </li>
          <li>
            <textarea className="offensive_popup_reason commentbox" style={{ display: "none" }}></textarea>
          </li>
        </ul>
        <div className="error"></div>
        <div className="buttons">
          <button className="button submit" onClick={() => userActionOnOffensiveForm(commentId, "submit", userId)}>
            {siteConfig.locale.comment.objection || "Report this!"}
          </button>
          <button className="button close_popup" onClick={() => userActionOnOffensiveForm(commentId, "close")}>
            {siteConfig.locale.comment.close || "Close"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(CommentBox);

//     fromname: Ranjeet Singh
// fromaddress: ranjeet.paliwal@gmail.com
// userid: ranjeet.paliwal@gmail.com
// location: null
// imageurl: https://mytimes.indiatimes.com/image/thumb/1/987504
// loggedstatus: 1
// message: fffffff
// roaltdetails: 1
// ArticleID: 81639876
// msid: 81639876
// parentid: 0
// rootid: 0
// medium: WAP
// url: https://m.timesofindia.com/city/mumbai/mumbainia-seizes-printer-used-to-generate-ambani-threat-letter-from-arrested-cops-residence/articleshow_comments/81639876.cms
// configid: 41083278
// pcode: toi-wap

/*     fromname: Ranjeet Singh
fromaddress: ranjeet.paliwal@gmail.com
userid: ranjeet.paliwal@gmail.com
location: null
imageurl: https://mytimes.indiatimes.com/image/thumb/1/987504
loggedstatus: 1
message: sadasd
roaltdetails: 1
ArticleID: 81640321
msid: 81640321
parentid: 2603712218
rootid: 2603712218
medium: WAP
url: https://m.timesofindia.com/business/india-business/tata-spicejet-promoter-shortlisted-for-ai-bids/articleshow_comments/81640321.cms
configid: 41083278
pcode: toi-wap */

/* fromname: Ranjeet Singh
fromaddress: ranjeet.paliwal@gmail.com
userid: 7bgap622hwb8dj2bf8hrwhh1a
location: null
imageurl: https://mytimes.indiatimes.com/image/thumb/1/987504
loggedstatus: 1
message: Yes of courese
roaltdetails: 1
ArticleID: 81667602
msid: 81667602
parentid: 0
rootid: 0
article_id: 81667602
url: https://navbharattimes.indiatimes.com/off-url-forward/pwa_comments.cms?
configid: 41083278
urs:
rotype: 3
pcode: nbt-wap
medium: WAP */

// agree

// https://navbharattimes.indiatimes.com/ratecomment_new.cms?opinionid=2603347305&typeid=100&rateid=0
//disagree

// https://navbharattimes.indiatimes.com/ratecomment_new.cms?opinionid=2603345535&typeid=101&rateid=0

// offensive

// https://navbharattimes.indiatimes.com/ratecomment_new.cms?opinionid=2603345535&typeid=103&rateid=0&ofreason=Foul+language

//https://navbharattimes.indiatimes.com/offensive/mark?ofusername=Ranjeet+Singh&ofreason=NONE&ofcommenteroid=2603345535&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=&ofuserisloggedin=1&ofuserssoid=ranjeet.paliwal%40gmail.com&ofuseremail=ranjeet.paliwal%40gmail.com

// Desktop wala

//https://navbharattimes.indiatimes.com/offensive/mark?ofusername=Ranjeet+Singh&ofreason=Foul+language&clname=VSNL&ofcommenteroid=2603462735&ofcommenthostid=53&ofcommentchannelid=696089404&ofcommentid=81572126&ofuserisloggedin=1&ofuserssoid=ranjeet.paliwal%40gmail.com&ofuseremail=ranjeet.paliwal%40gmail.com
