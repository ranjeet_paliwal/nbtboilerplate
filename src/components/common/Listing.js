import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import NewsListCard from "./NewsListCard";
import MovieReviewTile from "./MovieReviewTile";
import ErrorBoundary from "./../lib/errorboundery/ErrorBoundary";
import styles from "./css/commonComponents.scss";
import FakeListing from "./FakeCards/FakeListing";
import FakeHorizontalListCard from "./FakeCards/FakeHorizontalListCard";
import Adcard from "./AdCard";
import ListHorizontalCard from "./../../components/common/ListHorizontalCard";
// import AmazonWidget from "./../../modules/amazonwidget/AmazonWidget";

import { SeoSchema } from "./../../components/common/PageMeta";
import AnchorLink from "./AnchorLink";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';

import { _getStaticConfig, _isCSR } from "../../utils/util";
import YSubscribeCard from "./YSubscribeCard";
const siteConfig = _getStaticConfig();

class Listing extends Component {
  constructor(props) {
    super(props);
  }

  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn]
      ? siteConfig.listNodeLabels[item.tn][0]
      : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      item.id +
      ".cms";

    return item.override ? item.override : url;
  }

  render() {
    //widgetType can be headlisting, articlelist, videolist, photolist
    let {
      section,
      widgetType,
      widgetSubType,
      pagetype,
      listCountInc,
      subsec1,
      key,
      router
    } = this.props;

    let scrollItems;
    widgetType = widgetType ? widgetType : section.tn;
    if (
      (widgetType == "photolist" || widgetType == "videolist") &&
      section.items
    ) {
      scrollItems = { ...section };
      scrollItems.items =
        section.items.constructor === Array
          ? section.items.filter((item, index) => index > 0)
          : []; //FIX: when only single item is there it comes as an object
    }
    let listLength =
      widgetType == "photolist" || widgetType == "videolist"
        ? 1
        : section.items && section.items.length
          ? section.items.length
          : 0;

    let listingLink = this.generateUrl(section);
    let widgetHeading =
      widgetType == "videolist"
        ? section.secname
          ? section.secname
          : siteConfig.locale.videos
        : widgetType == "photolist"
          ? section.secname
            ? section.secname
            : siteConfig.locale.photo_maza
          : section.secname;
    let showWidgetHeading = widgetType != "headlisting" ? true : false;
    let showMoreButton =
      widgetType == "headlisting" ? false : widgetHeading ? true : false;
    let moreButtonText = widgetHeading + siteConfig.locale.read_more_listing;

    /*FIX for case when only single item is there in section items */
    if (
      section &&
      typeof section.items != "undefined" &&
      section.items.constructor !== Array
    ) {
      let tempSec = Object.assign({}, section.items);
      section.items = [tempSec];
    }

    return (
      <div
        data-rlvideoid={section._rlvideoid ? section._rlvideoid : null}
        className={
          styles["box-content"] +
          " box-content " +
          (widgetType == "videolist" && widgetSubType == "homepageVideo"
            ? "videoview"
            : "")
        }
        data-exclude={this.props.amp && this.props.amp == "no" ? "amp" : null}
      >
        {showWidgetHeading ? (
          <h2>
            {widgetHeading ? (
              <AnchorLink hrefData={{ override: listingLink }}>
                <span>{widgetHeading} </span>
              </AnchorLink>
            ) : null}
          </h2>
        ) : null}
        <ul className="sub-list">
          <li>
            <a href="#">sublist1</a>
          </li>
          <li>
            <a href="#">sublist2</a>
          </li>
          <li>
            <a href="#">sublist3</a>
          </li>
          <li>
            <a href="#">sublist4</a>
          </li>
          <li className="more_sublist">
            <a href="#">sublist5</a>
          </li>
        </ul>
        <YSubscribeCard />

        <ul
          className={`nbt-list ${
            this.props.layout && this.props.layout != ""
              ? this.props.layout
              : ""
          }`}
        >
          {/* For SEO Schema */}
          {/* <meta itemProp="numberOfItems" content={section && typeof (section.items) != 'undefined' && section.items.length > 0 ? section.items.length : 0} /> */}
          {section &&
          typeof section.items != "undefined" &&
          section.items.length > 0 ? (
            section.items.map((item, index) => {
              if (
                (index == 13 || (index > 13 && index % 13 == 0)) &&
                router &&
                router.location.pathname.indexOf("/tech") > -1
              ) {
                return (
                  // <li className="nbt-listview">
                  //   <AmazonWidget router={router} pagename={"articlelist"} />
                  // </li>
                );
              }
              if (item.tn == "moviereview" || item.tn == "movieshow") {
                return (
                  <ErrorBoundary key={index}>
                    <MovieReviewTile
                      pagetype={pagetype}
                      listCountInc={listCountInc ? listCountInc : ""}
                      key={index}
                      item={item}
                      leadpost={index == 0 ? true : false}
                      position={index + 1}
                    />
                  </ErrorBoundary>
                );
              } else if (item.tn == "ad") {
                return (
                  <Adcard
                    key={`listingad_${index}`}
                    ctnstyle="inline"
                    mstype={item.mstype}
                    adtype={item.type}
                    elemtype="li"
                  />
                );
                //return(<li ctn-style={item.mstype} data-plugin="ctn" ></li> )
              } else if (index < listLength) {
                return (
                  <ErrorBoundary key={index}>
                    <NewsListCard
                      layout={
                        this.props.layout && this.props.layout != ""
                          ? this.props.layout
                          : ""
                      }
                      pagetype={pagetype}
                      listCountInc={listCountInc ? listCountInc : ""}
                      type={widgetType == "videolist" ? "video_lead" : ""}
                      item={item}
                      seolistschema={section.items[0].pl == 1 ? true : false}
                      leadpost={index == 0 ? true : false}
                    />

                    {/* Print Paytm Widget for Tech Section Only */}
                    {/* {index == 6 ? <li className="nbt-listview"><PaytmWidget pagename="articlelist" subsec1={subsec1} /></li> :null} */}
                  </ErrorBoundary>
                );
              }
            })
          ) : (
            <li>
              <FakeListing showImages={true} />
            </li>
          )}
          {scrollItems &&
          typeof scrollItems.items != "undefined" &&
          scrollItems.items.length > 0 ? (
            <ErrorBoundary>
              <ListHorizontalCard
                pagetype={pagetype}
                listCountInc={listCountInc ? listCountInc : ""}
                item={scrollItems}
              />
            </ErrorBoundary>
          ) : null}
          {section &&
          typeof section.items != "undefined" &&
          section.items.length > 0 &&
          section.rlvideo ? (
            <ErrorBoundary>
              <ListHorizontalCard
                pagetype={pagetype}
                listCountInc={listCountInc ? listCountInc : ""}
                type="rlvideo"
                item={section.rlvideo}
              />
            </ErrorBoundary>
          ) : section && section.isfetchingrlvideo ? (
            <FakeHorizontalListCard />
          ) : null}
        </ul>
        {showMoreButton &&
        section &&
        typeof section.items != "undefined" &&
        section.items.length > 0 ? (
          <AnchorLink
            hrefData={{ override: listingLink }}
            className={styles["more-btn"] + " more-btn"}
          >
            {moreButtonText}
          </AnchorLink>
        ) : null}
      </div>
    );
  }
}

Listing.propTypes = {
  section: PropTypes.object
};

export default Listing;
