import React from "react";
import PropTypes from "prop-types";
import styles from "./css/ArticleShow.scss";
import AnchorLink from "./AnchorLink";

/* table row  = '-$|$-'  && table col = '-|$|-'*/
const TableCard = props => {
  let { data, type } = props;

  if (data && data != "") {
    data = data.split("-$|$-");
  }

  return data.length > 0 ? (
    <div className="tableContainer">
      <table className={type != "" ? type : null}>
        <tbody>
          {data.map((item, index) => {
            let itemstds = item.split("-|$|-");
            return (
              <tr key={"tr" + index}>
                {itemstds.map((item, index) => {
                  return (
                    <td key={"td" + index} className={item.trim().length <= 4 ? item.trim() : null}>
                      {/* In case a link exists within table */}
                      {item.includes("gov.in") || item.includes("co.in") || item.includes(".com") ? (
                        <AnchorLink href={item} target="_blank">
                          {item.replace("https://", "")}
                        </AnchorLink>
                      ) : (
                        item
                      )}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  ) : null;
};

export default TableCard;
