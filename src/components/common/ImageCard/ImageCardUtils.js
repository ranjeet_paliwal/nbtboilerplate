/* eslint-disable indent */
import { _getStaticConfig } from "../../../utils/util";

const siteConfig = _getStaticConfig();

const defaultOptions = {
  msid: siteConfig.imageconfig.thumbid,
  title: siteConfig.wapsitename,
  alt: "",
  size: "largethumb",
};

const getAmpSize = props => {
  if (props.pagetype && props.pagetype === "articleshow") {
    return "amplargethumbarticle";
  }
  if (props.size && (props.size === "largewidethumb" || props.size === "largethumb")) {
    return "amplargethumb";
  }
  return defaultOptions.size;
};

const getAmpImgSrc = props => {
  const ampsize = getAmpSize(props);
  const ampimgsrc =
    props.type && props.src && props.type === "absoluteImgSrc"
      ? props.src
      : `${siteConfig.imageconfig.imagedomain + props.msid},${siteConfig.imageconfig[ampsize]}/${props.imgname}.jpg`;
  return ampimgsrc;
};

export const getImageSrc = props => {
  // console.log("getImageSrc", props.size, siteConfig.imageconfig);
  // let imageSource = "";
  // if (props.type && props.src && props.type === "absoluteImgSrc") {
  //   imageSource = props.src;
  // } else {
  //   const imgver = props.imgver ? `,imgver-${props.imgver},` : "";
  //   const imgWidth = siteConfig.imageconfig[props.size] ? `${siteConfig.imageconfig[props.size]},` : "";
  //   const imgName = `${props.imgname || "pic"} `;
  //   imageSource = `${siteConfig.imageconfig.imagedomain}${props.msid}${imgver}${imgWidth}${imgName}.jpg`;
  // }
  // return imageSource;

  let { imgname } = props;
  if (!imgname) {
    imgname = siteConfig.wapsitename.toLowerCase().replace(" ", "-");
  }
  const imageSource =
    props.type && props.src && props.type === "absoluteImgSrc"
      ? props.src
      : `${siteConfig.imageconfig.imagedomain + props.msid},${props.imgsize}${
          props.imgver ? `imgsize-${props.imgver},` : ""
        }${siteConfig.imageconfig[props.size]}/${imgname}.jpg`;
  return imageSource;
  // https://static.langimg.com/thumb/msid-75199581,imgver-67413,undefined/ei-samay.jpgccc
};

export const getImageParameters = imageData => {
  const props = Object.assign({}, imageData);

  props.msid = props.msid ? props.msid : defaultOptions.msid;
  props.imgname = defaultOptions.title.toLowerCase().replace(" ", "-");
  props.title = props.title || "";
  props.alt = props.alt || "";
  props.size = props.size ? props.size : defaultOptions.size;
  props.imgsize = props.imgsize ? `imgsize-${props.imgsize},` : "";

  const img = {
    src: getImageSrc(props),
    ampsrc: getAmpImgSrc(props),
    title: props.title,
    size: props.size,
    ampsize: getAmpSize(props),
    alt: props.alt,
  };

  return img;
};
