import React from "react";
import PropTypes from "prop-types";
import { _getStaticConfig } from "../../../utils/util";
import LazyIntersection from "../LazyIntersection";
import { getImageParameters } from "./ImageCardUtils";

const siteConfig = _getStaticConfig();

const getplaceholderSrc = (size = "largethumb") => {
  const newSize = typeof size === "string" && size.indexOf("wide") > 0 ? "largewidethumb" : "largethumb";
  const title = siteConfig.wapsitename;
  const msid = siteConfig.imageconfig.thumbid;
  const imgsize = siteConfig.imageconfig[newSize];

  return `${siteConfig.imageconfig.imagedomain + msid},${imgsize}/${title}.jpg`;
};

const ImageCard = props => {
  const img = getImageParameters(props);
  const imgWidth = props.width || "100%";
  const placeholderSrc = getplaceholderSrc(props.size);
  const className = props.className ? props.className : null;
  const imgsize = siteConfig.imageconfig[img.size];
  const ampimgsize = siteConfig.imageconfig[img.ampsize];
  const adgdisplay =
    props && props.adgdisplay !== "false" && (props.pagetype == "articleshow" || props.pagetype == "photoshow")
      ? null
      : "false";

  const { noLazyLoad } = props;
  img.alt = img.alt.replace(/['"]+/g, "");
  return (
    <React.Fragment>
      {noLazyLoad ? (
        <img
          width={imgWidth}
          className={className}
          src={img.src}
          alt={img.alt}
          title={img.title}
          placeholder={placeholderSrc}
          data-adgdisplay={adgdisplay}
          style={props.style}
        />
      ) : (
        <LazyIntersection
          width={imgWidth}
          className={className}
          src={img.src}
          datasrc={img.src}
          alt={img.alt}
          title={img.title}
          placeholder={placeholderSrc}
          adgdisplay={adgdisplay}
          style={props.style}
        />
      )}
      {/* <LazyImage width="100%" className={className} src={img.src} alt={img.title} placeholder={placeholderSrc} /> */}
      {/* <img width="100%" className={className} data-src={img.src} alt={img.title} placeholder={placeholderSrc} /> */}
      {/* {(props.schema == true) ? <meta itemProp="image" content={img.src}/> : ''} */}

      {/* && imgsize.indexOf("height") > -1  */}
      {props.schema && (
        <span itemProp="image" itemScope itemType="https://schema.org/ImageObject">
          <meta itemProp="url" content={img.ampsrc} />
          <meta content="1200" itemProp="width" />
          <meta content="900" itemProp="height" />
        </span>
      )}
    </React.Fragment>
  );
};

ImageCard.propTypes = {
  noLazyLoad: PropTypes.bool,
  width: PropTypes.string,
  size: PropTypes.string,
  className: PropTypes.string,
  schema: PropTypes.bool,
};

export default ImageCard;
