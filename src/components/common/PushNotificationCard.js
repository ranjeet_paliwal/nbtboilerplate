import React, { Component, useState, useEffect } from "react";
import { AnalyticsGA } from "./../../components/lib/analytics/index";
import { _getCookie, _setCookie, _isCSR, isMobilePlatform } from "../../utils/util";

import styles from "./css/Notification-cards.scss";
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const siteName = process.env.SITE;
const cookieName = siteConfig.pushNotification.cookieName;
// const cookieDays = 7;
const PushNotificationCard = React.memo(props => {
  const [PopUpShow, togglePopUp] = useState(false);
  const { cookieVal } = props;

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  });

  const clickBindEvent = event => {
    const cookieDays = (cookieVal && cookieVal[siteName].value) || 7;
    if (event == "later") {
      _setCookie(cookieName, "1", cookieDays);
      grx("track", "browser_notifications", { status: "no" });
    } else if (event == "allow") {
      grx("enablePush");
      // this is temporary solution if user browser not allwo to click in allwo notification
      _setCookie(cookieName, "1", cookieDays);
      grx("track", "browser_notifications", { status: "yes" });
    }
    togglePopUp(false);
  };

  const handleScroll = () => {
    // grx push allready subscribled
    // Notification.permission !== "granted"
    if (window.scrollY > 100 && !PopUpShow) {
      if (typeof Notification == "function" && Notification.permission == "default" && !_getCookie(cookieName)) {
        togglePopUp({ PopUpShow: true });
      }
    }
  };

  return PopUpShow ? (
    <div className={`notification_popup ${isMobilePlatform() ? "mobile" : ""}`}>
      <div className="logoWrap">
        <img
          alt={siteConfig.wapsitename}
          title={siteConfig.wapsitename}
          src={siteConfig.pushNotification.defaultThumb}
        />{" "}
        <h4>{siteConfig.pushNotification.heading}</h4>
      </div>
      <span className="btn allow" onClick={() => clickBindEvent("allow")} data-attr="btnPushYes">
        Allow
      </span>
      <span className="btn later" onClick={() => clickBindEvent("later")} data-attr="btnPushNo">
        Later
      </span>
    </div>
  ) : null;
});

export default PushNotificationCard;
