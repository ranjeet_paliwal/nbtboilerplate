import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router";
import { _getCookie, _setCookie, gaEvent, _getStaticConfig, _sessionStorage, _isCSR } from "../../utils/util";
import Ads_module from "../../components/lib/ads/index";

const siteConfig = _getStaticConfig();
var timeStart = false;
var timeleft = "";
export const lang_closeInt = click => {
  // clearInterval(this.downloadTimer);
  document.getElementById("interstitial_con").style.display = "none";
  document.querySelector("body").style.overflow = "visible";
};

export const lang_adClick = e => {
  //gaEvent("Interstitial Banner", "click", "banner");
  lang_closeInt();
};

export const setTimer = (type, time) => {
  timeleft = time;
  const _this = this;
  timeStart = true;

  var downloadTimer = setInterval(() => {
    document.querySelector(".timesText").innerHTML = `Skip in <span data-attr="counter">${timeleft}</span> seconds`;
    document.querySelector(".skip").style.display = "block";
    document.querySelector(".timesText").style.color = "#fed20a";
    if (timeleft == 0) {
      timeStart = false;
      clearInterval(downloadTimer);
      lang_closeInt();
    }
    timeleft -= 1;
  }, 1000);
};

export const interStitialHTML = prop => {
  const { _perday, _persession, _pv, _timerVideo, _timerDefault } = prop;
  return -_isCSR() ? (
    ReactDOM.createPortal(
      <React.Fragment>
        <div
          id="interstitial_con"
          className="interstitial_con"
          data-dcap={_perday}
          data-scap={_persession}
          data-pv={_pv}
          data-exclude="amp"
        >
          <div className="int_nav">
            <span className="logo">
              <Link to="/" title="">
                <img alt="" title="" src={siteConfig.logo_id} />
              </Link>
              <b className="timesText" data-vt={_timerVideo} data-dt={_timerDefault}>
                &nbsp;
              </b>
            </span>
            <a className="skip" onClick={e => lang_closeInt(true)} href="javascript:void(0)">
              Skip
            </a>
          </div>
          <div className="int_body">
            <div className="lang_intctn lang_con_intad" onClick={e => lang_adClick()}></div>
          </div>
        </div>
      </React.Fragment>,
      document.getElementById("interstitial-ad-wrapper"),
    )
  ) : (
    <React.Fragment>
      <div
        id="interstitial_con"
        className="interstitial_con"
        data-dcap={_perday}
        data-scap={_persession}
        data-pv={_pv}
        data-exclude="amp"
      >
        <div className="int_nav">
          <span className="logo">
            <Link to="/" title="">
              <img alt="" title="" src={siteConfig.logo_id} />
            </Link>
            <b className="timesText" data-vt={_timerVideo} data-dt={_timerDefault}>
              &nbsp;
            </b>
          </span>
          <a className="skip" onClick={e => lang_closeInt(true)} href="javascript:void(0)">
            Skip
          </a>
        </div>
        <div className="int_body">
          <div className="lang_intctn lang_con_intad" onClick={e => lang_adClick()}></div>
        </div>
      </div>
    </React.Fragment>
  );
};

export const intAdInit = prop => {
  // Flag to show interstitial once in a session
  const intShown = sessionStorage.getItem("intShown") ? sessionStorage.getItem("intShown") : 0;
  // Check is PIP enabled or not
  const bodyClass =
    document.body.className.indexOf("enable_dock") == -1 && document.body.className.indexOf("enable-video-show") == -1;

  const sessionCap = document.querySelector("[data-scap]")
    ? document.querySelector("[data-scap]").getAttribute("data-scap")
    : 1;

  if (intShown < sessionCap && bodyClass && !timeStart) {
    // FCap
    const ctncap = document.querySelector("[data-dcap]")
      ? document.querySelector("[data-dcap]").getAttribute("data-dcap")
      : 1;
    // Check is interstital cookie present
    let dayCount = 0;
    if (_getCookie("interstitial")) {
      dayCount = parseInt(_getCookie("interstitial"));
    }
    const intCon = document.getElementById("interstitial_con");
    if (dayCount < ctncap && !timeStart) {
      document.querySelector(".lang_con_intad").innerHTML =
        "<div data-name=" +
        siteConfig.ads.dfpads.dfpint +
        ' data-size="[[304, 350], [300, 250], [336, 280]]" data-slot="dfpint" data-plugin="dfp" class="ad1 dfpint" data-id="div-gpt-ad-1540206148700-int"></div>';
      // To Render CTN and DFP add
      // CTN_module.render({});
      //Ads_Module.render({});
      Ads_module.refreshAds(["dfpint"]);
      // poup open delay : ad is not coming quick
      setTimeout(() => {
        const divdfpint = document.querySelector("#interstitial_con .dfpint");
        if (divdfpint && !divdfpint.innerHTML) {
          Ads_module.refreshAds(["dfpint"]);
        }
        // setInterval(() => setTimer1(8), 1000);
        setTimer(
          "close",
          document.querySelector("[data-dt]") ? document.querySelector("[data-dt]").getAttribute("data-dt") : 8,
        );
        intCon.style.width = "100%";
        intCon.style.height = "110%";
        intCon.style.display = "block";
        document.querySelector("#footerContainer").style.display = "block";
        gaEvent("Interstitial Banner", "view");
        _setCookie("interstitial", dayCount + 1, 1);
        sessionStorage.setItem("intShown", parseInt(intShown) + 1);
      }, 3000);
    } else {
      intCon.style.display = "none";
    }
  }
};

export default { intAdInit, interStitialHTML };
