import React from "react";
import ReactDOM from "react-dom";
import { _isCSR } from "../../../utils/util";
const AmazonBtn = props => {
  let { type, data, isAmpPage } = props;
  const amazonUrl = `${process.env.WEBSITE_URL}pwafeeds/affiliate_amazon.cms?url=${encodeURIComponent(
    data && data._destinationurl,
  )}`;

  return _isCSR() ? (
    ReactDOM.createPortal(
      <a
        id="floating-widget-amazon-sale"
        href={amazonUrl}
        target="_blank"
        className="btn_openinapp float-amz"
        data-attr="btn_openinapp"
        style={!isAmpPage ? { display: "none" } : null}
      >
        <span className="txt">{data._text || "Sale"}</span>
        <span className="img">
          <img src={`https://static.langimg.com/thumb/msid-${data._imageid},width-36,height-36,resizemode-4/pic.jpg`} />
        </span>
      </a>,
      document.getElementById("share-container-wrapper"),
    )
  ) : (
    <a
      id="floating-widget-amazon-sale"
      href={amazonUrl}
      target="_blank"
      className="btn_openinapp float-amz"
      data-attr="btn_openinapp"
      style={!isAmpPage ? { display: "none" } : null}
    >
      <span className="txt">{data._text || "Sale"}</span>
      <span className="img">
        <img src={`https://static.langimg.com/thumb/msid-${data._imageid},width-36,height-36,resizemode-4/pic.jpg`} />
      </span>
    </a>
  );
};

export default AmazonBtn;
