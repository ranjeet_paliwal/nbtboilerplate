import React from "react";
import PropTypes from "prop-types";
import { AnalyticsGA } from "../lib/analytics";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const WpBroadCast = props => (
  <a
    data-exclude="amp"
    target="_blank"
    // onClick={() =>
    //   AnalyticsGA.event({
    //     category: "whatsapp_subscription",
    //     action: "click",
    //     label: props.pagetype,
    //   })
    // }
    data-link="whatsapp"
    className="news-on-whatsApp"
    href={siteConfig.locale.wp_broadcast.url}
  >
    <span className="tbl">
      <span className="tbl_row">
        <span className="tbl_col txt">{siteConfig.locale.wp_broadcast.description}</span>
        <span className="tbl_col logo">
          <b>{siteConfig.locale.wp_broadcast.btnText}</b>
        </span>
      </span>
    </span>
  </a>
);

WpBroadCast.defaultProps = {
  pagetype: "articleshow",
};

WpBroadCast.propTypes = {
  pagetype: PropTypes.string,
};

export default WpBroadCast;
