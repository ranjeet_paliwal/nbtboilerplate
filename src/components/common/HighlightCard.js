import React from "react";

import styles from "./css/SummaryCard.scss";

import {
  _getStaticConfig,
  _deferredDeeplink,
  isMobilePlatform
} from "../../utils/util";
const siteConfig = _getStaticConfig();
const appdeeplink = siteConfig.appdeeplink;

/*type == 'bulletContent' for inline bullet contents otherwise tophighlight*/
const HighlightCard = props => {
  let { data, type, briefsectioninfo, msid } = props;
  if (type == "lbhighlight" || type == "topfeature") {
    data = data;
  } else if (type == "bulletContent" && typeof data == "string") {
    data = data.split("-|$|-");
  } else if (
    typeof data == "object" &&
    data.artsummary &&
    data.artsummary.bulletecontentbody
  ) {
    data =
      data.artsummary.bulletecontentbody.bulletcontentrow.bulletcontentrowbody
        .bulletcontentrowcell["#text"];
  } else if (typeof data == "object" && data.artsummary && data.artsummary.ul) {
    data = data.artsummary.ul;
  } else if (
    typeof data == "object" &&
    data.artsummary &&
    data.artsummary instanceof Array
  ) {
    data = data.artsummary[0];
  }
  //Case when editor has filed the highlights without bullet points (EIS-1855)
  else if (
    typeof data == "object" &&
    data.artsummary &&
    typeof data.artsummary == "object" &&
    data.artsummary["#text"] instanceof Array
  ) {
    data = data.artsummary["#text"];
  } else {
    data = [];
  }

  return typeof data === "object" && data.length > 0 ? (
    <div className={`${styles[type]} top-article ${type == "topfeature" ? "tophighlight" : type}`}>
      <h3>{type == "topfeature" ? siteConfig.locale.tech.topfeature : siteConfig.locale.highlight}</h3>
      <ul>
        {data.map((item, index) => {
          if (typeof item == "object" && item.strong) item = item.strong;
          if (typeof item == "object" && item["#text"]) item = item["#text"];
          return (
            <li key={index}>
              {type != "lbhighlight" ? (
                item
              ) : (
                  <a href={`#${item.msid}`}>{item.title}</a>
                )}
            </li>
          );
        })}
      </ul>
      {process.env.SITE != "eisamay" &&
        type != "lbhighlight" && type != "topfeature" &&
        isMobilePlatform() ? (
          <a
            href={_deferredDeeplink(
              "section",
              appdeeplink,
              msid,
              null,
              briefsectioninfo
            )}
            className="btn_open_in_app"
          >
            {siteConfig.locale.highlightText}
          </a>
        ) : null}
    </div>
  ) : null;
};

export default HighlightCard;
