import React, { Component } from "react";
import ImageCard from "./ImageCard/ImageCard";

import styles from "./css/MovieReviewShowCard.scss";
import fetch from "../../utils/fetch/fetch";
import { _isCSR, _checkUserStatus, isMobilePlatform } from "../../utils/util";
import { fireActivity } from "../common/TimesPoints/timespoints.util";
import { openLoginPopup } from "./ssoLogin";

import { _getStaticConfig } from "../../utils/util";
import SocialShare from "./SocialShare";
import SummaryCard from "./SummaryCard";
const siteConfig = _getStaticConfig();

const locale = siteConfig.locale;
let ssoconfigapikey = "";
let pagetypedef = "";
let _rating = 0;
let usrRate = "2.5";

class MoviePoster extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { item, review, sharedata, openCommentsPopup } = this.props;
    let mr_otherinfo = [];
    let pagename = item.pwa_meta && review == "techreview" ? "techreview" : "moviereview";
    ssoconfigapikey =
      (item.pwa_meta && review == "techreview") ||
        // review == undefined ||
        review == "user"
        ? "APP_KEY_TECH"
        : "APP_KEY";
    pagetypedef =
      (item.pwa_meta && item.pwa_meta.subpagetype == "techreview") ||
        // review == undefined ||
        review == "user"
        ? "tech"
        : "";
    item.lang ? mr_otherinfo.push(item.lang) : null; //add Movie language
    item.gn ? mr_otherinfo.push(item.gn.split(",").join(", ")) : null;

    let ratings = item.gadgetinfo && item.gadgetinfo.rating ? item.gadgetinfo.rating : null;
    let userrating =
      item.ur && item.pwa_meta && (item.pwa_meta.subpagetype = "techreview")
        ? item.ur
        : item.gadgetinfo && item.gadgetinfo.userrating && item.gadgetinfo.userrating != "NaN"
          ? item.gadgetinfo.userrating
          : null;

    //{console.log('criticRating',criticRating)}
    //{console.log('criticRating before Cal', item.gadgetinfo.criticRating)}
    return (
      <div
        className={`animated fadeIn moviereview-summaryCard${!isMobilePlatform() ? " desktop" : ""}`}
        data-attr="news_card"
      >
        {item && item.hidePoster ? null : (
          <React.Fragment>
            {review == undefined || review == "critic" || review == "moviereview" || review == "techreview" ? (
              <div className="poster_card">
                {/* {isMobilePlatform() && (   //Commented as it was not being used
                  <ImageCard
                    type="absoluteImgSrc"
                    src={siteConfig.locale.movie_review_detail_bg}
                    className="bg_poster"
                  />
                )} */}
                <div className="con_poster">
                  {item.pwa_meta.pagetype != "moviereview" ? (
                    <span>
                      <meta itemProp="datePublished" content={item.dlseo ? item.dlseo : null} />
                      <meta itemProp="dateModified" content={item.luseo ? item.luseo : null} />
                    </span>
                  ) : null}
                  {!isMobilePlatform() && item.pwa_meta.pagetype == "moviereview" ? (
                    <h2 className="mvlabel">{siteConfig.locale.film_review}</h2>
                  ) : (
                    <React.Fragment>
                      <h1
                        itemProp={
                          item.pwa_meta.pagetype == "moviereview"
                            ? "name"
                            : item.pwa_meta && item.pwa_meta.subpagetype && item.pwa_meta.subpagetype == "techreview"
                              ? "headline"
                              : null
                        }
                      >
                        {item.hl}
                      </h1>
                      {/* <span className="mvlabel">{" " + (item.pwa_meta.pagetype == "moviereview" ? siteConfig.locale.film_review : "")}</span> */}
                    </React.Fragment>
                  )}

                  {/* for schema only */}
                  <span
                    itemProp={item.pwa_meta.pagetype == "moviereview" ? "director" : ""}
                    itemScope="itemScope"
                    itemType="https://schema.org/Person"
                  >
                    <meta itemProp="name" content={item.dir} />
                  </span>

                  {item && item.hideMovieTable ? (
                    <span style={{ height: "60%" }}>
                      {/* Poster Image */}
                      {item.imageid && <ImageCard noLazyLoad msid={item.imageid} size="midthumb" schema width="100%" />}
                    </span>
                  ) : (
                    <div className="table">
                      <div className="table_row">
                        <span className={styles["img_wrap"] + " table_col img_wrap handlereview"}>
                          {/* Poster Image */}
                          {item && item.image && item.image[0] && item.image[0].id && (
                            <ImageCard
                              noLazyLoad
                              msid={item && item.image && item.image[0] && item.image[0].id}
                              size="posterthumb"
                              schema
                            />
                          )}
                          {/* <span className={(item && item.gadgetinfo) ? null : 'type_icon video_icon'}></span> */}
                        </span>

                        <span className="table_col con_wrap">
                          {/*  On Desktop, Movie title shows up here */}
                          {!isMobilePlatform() ? (
                            item.pwa_meta.pagetype == "moviereview" ? (
                              <h1
                                className="movie-name"
                                itemProp={
                                  item.pwa_meta.pagetype == "moviereview"
                                    ? "name"
                                    : item.pwa_meta &&
                                      item.pwa_meta.subpagetype &&
                                      item.pwa_meta.subpagetype == "techreview"
                                      ? "headline"
                                      : null
                                }
                              >
                                {item.hl}
                              </h1>
                            ) : (
                              item.hl
                            )
                          ) : null}
                          {// For desktop, timestamp comes here for movie review
                            !isMobilePlatform() && item.pwa_meta.pagetype == "moviereview" ? (
                              <div className="moviereview-sourceCard">
                                {/* Check is author name, id and full name exist */
                                  SummaryCard.Byline(item)}
                              </div>
                            ) : null}
                          {/*  For desktop sharing comes here after timestamp */}
                          {!isMobilePlatform() ? (
                            <React.Fragment>
                              <div id="widget-two" className="wdt_timespoints" />
                              <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
                            </React.Fragment>
                          ) : (
                            ""
                          )}
                          {/* casting */}
                          {item.ct ? getMovieCast(item.ct) : null}

                          <span
                            className="des"
                            itemProp={item.pwa_meta.pagetype == "moviereview" && !isMobilePlatform() ? "" : ""}
                          >
                            {/* director info */}
                            {!isMobilePlatform() ? (
                              <span>
                                <b>{siteConfig.locale.director}:</b> {item.dir}
                              </span>
                            ) : (
                              ""
                            )}
                            {/* Print movie language and gener */}
                            <span>
                              {!isMobilePlatform() ? <b className="">{`${siteConfig.locale.genre}:`}</b> : ""}
                              {mr_otherinfo != "" ? mr_otherinfo.join(", ") : null}
                            </span>

                            {/* Duration */
                              item.du ? getDuration(item.du) : null}
                          </span>
                          {!isMobilePlatform() ? (
                            <span
                              className="btn_rate"
                              onClick={excuteReview.bind(this, this.props.id, sharedata, openCommentsPopup)}
                            >
                              {locale.review_movie}
                            </span>
                          ) : null}
                        </span>
                      </div>
                    </div>
                  )}

                  {process.env.PLATFORM == "mobile" ? (
                    pagetypedef == "tech" ? (
                      <div className="table rating">
                        <meta itemProp="author" content={item.bl} />
                        <div className="table_row">
                          <span className="table_col">
                            <span className="txt critic">{locale.critics_rating}</span>
                            <span className="count">
                              {item.cr == 0 || item.cr == undefined || item.cr == "NaN" ? (
                                <span>
                                  NA
                                  <meta content="1" />{" "}
                                </span>
                              ) : (
                                <span>
                                  <b>{item.cr}</b>/5
                                </span>
                              )}
                            </span>
                          </span>
                          <span className="table_col">
                            <span className="txt users">{locale.reader_rating}</span>
                            <span className="count">
                              {item.ur && item.ur != "NaN" && item.ur != "" ? (
                                <span>
                                  <b>{item.ur == "NaN" ? "NA" : item.ur}</b>/5
                                </span>
                              ) : (
                                "NA"
                              )}
                            </span>
                          </span>
                        </div>
                      </div>
                    ) : (
                      <div
                        className="table rating"
                      // itemProp={item.pwa_meta.pagetype == "moviereview" ? "Review" : ""}
                      // itemScope="itemscope"
                      // itemType={item.pagetype == "moviereview" ? "https://schema.org/Review" : ""}
                      >
                        {/* <meta itemProp="author" content={item.bl} /> */}

                        <div
                          className="table_row"
                        // itemType={
                        //   item.pagetype == "moviereview" ? "https://schema.org/Rating" : "https://schema.org/Rating"
                        // }
                        // itemProp={item.pagetype == "moviereview" ? "reviewRating" : "reviewRating"}
                        // itemScope="itemscope"
                        >
                          <span className="table_col">
                            {/* <meta itemProp="worstRating" content="0.5" />
                            <meta itemProp="bestRating" content="5" /> */}
                            <span className="txt critic">{locale.critics_rating}</span>
                            <span className="count">
                              {item.cr == 0 || item.cr == undefined || item.cr == "NaN" ? (
                                <span>
                                  NA
                                  {/* <meta itemProp="ratingValue" content="1" />{" "} */}
                                </span>
                              ) : (
                                <span>
                                  <b>{item.cr}</b>/5
                                  {/* itemProp="ratingValue" */}
                                </span>
                              )}
                            </span>
                          </span>
                          <span className="table_col">
                            <span className="txt users">{locale.reader_rating}</span>
                            <span className="count">
                              {item.ur && item.ur != "NaN" && item.ur != "" ? (
                                <span>
                                  <b>{item.ur == "NaN" ? "NA" : item.ur}</b>/5
                                </span>
                              ) : (
                                "NA"
                              )}
                            </span>
                          </span>
                        </div>
                      </div>
                    )
                  ) : null}
                </div>
              </div>
            ) : null}

            {/* {<GanaCard link={item.gaana} />} */}
            {review == undefined || review == "user" || review == "moviereview" || review == "techreview" ? (
              <Userrating
                userratings={ratings}
                review={review}
                userrating={userrating == 0 ? null : userrating}
                id={item.id}
                topfeature={item.topfeature && item.topfeature != "" ? item.topfeature : null}
                sharedata={sharedata}
                cr={item.cr}
                ur={item.ur}
                pagetype={pagename}
                openCommentsPopup={openCommentsPopup}
              />
            ) : null}
          </React.Fragment>
        )}

        {/*userrating line*/}

        {/* {MovieReviewSocialReactions !='' ? <a lang="en" className="twitter-timeline" data-widget-id={props.twitter} href={MovieReviewSocialReactions}>Tweets about {props.twitter_key}</a> : null} */}
      </div>
    );
  }
}

export const Userrating = props => {
  let ratings = props.userratings;
  let userrating = props.userrating;
  let review = props.review;
  let pagetype = props.pagetype;
  let openCommentsPopup = props.openCommentsPopup;

  return (
    <React.Fragment>
      <div className="con_review_rate" data-exclude="amp">
        {review == "user" ? (
          <React.Fragment>
            <UserReview userratings={ratings} userrating={userrating} id={props.id} />
            <TabsComponent
              id={props.id}
              review={review}
              sharedata={props.sharedata}
              openCommentsPopup={openCommentsPopup}
            />
            <RateBox id={props.id} cr={props.cr} ur={props.ur} pagetype={props.pagetype}/>
          </React.Fragment>
        ) : pagetype == "techreview" ? (
          <React.Fragment>
            <TabsComponent id={props.id} sharedata={props.sharedata} pagetype={pagetype} />
            <RateBox id={props.id} cr={props.cr} ur={props.ur} pagetype={pagetype} />
            <UserReview userratings={ratings} userrating={userrating} id={props.id} />
          </React.Fragment>
        ) : (
              <React.Fragment>
                <TabsComponent id={props.id} sharedata={props.sharedata} />
                <RateBox id={props.id} cr={props.cr} ur={props.ur} pagetype={props.pagetype} />
              </React.Fragment>
            )}
      </div>
      {props.topfeature && props.topfeature != "" ? <Topfeature topfeature={props.topfeature} /> : null}
    </React.Fragment>
  );
};

const createStars = (id, pagetype) => {
  let str = "";
  for (let i = 1; i <= 10; i++) {
    str += `<span key=${i} value=${i}> ${i * 0.5} </span>`;
  }
  return (
    <React.Fragment>
      <div
        className="hover-stars"
        onMouseOver={e => handleRating(e, id)}
        onClick={e => handleUserRating(id, e, pagetype)}
        dangerouslySetInnerHTML={{ __html: str }}
      />
    </React.Fragment>
  );
};

const handleUserRating = (id, e, pagetype) => {
  const hoverStars = document.querySelector(".hover-stars");
  if (hoverStars.classList.value.indexOf("disable") > -1) {
    return;
  }
  let starVal = e.target.getAttribute("value");
  if (starVal) {
    starVal = Number(starVal);
  }
  _rating = Number(starVal * 0.5);
  if(pagetype && pagetype === "movieshow"){
      submitRating(id, pagetype, e);
  }
};

const handleRating = (e, id) => {
  const hoverStars = document.querySelector(".hover-stars");
  if (hoverStars.classList.value.indexOf("disable") > -1) {
    return;
  }
  let starVal = e.target.getAttribute("value");
  if (starVal) {
    starVal = Number(starVal);
    let rateStar = document.getElementById("ratestar_" + id);
    let rateSpan = document.getElementById("ratespan_" + id);

    rateStar.classList.value = "";
    rateStar.classList.add("w" + starVal * 10);
    rateStar.classList.add("filled-stars");
    rateStar.classList.add("ratestar");
    rateSpan.innerHTML = Number(starVal * 0.5);
  }
  _rating = Number(starVal * 0.5);
};

const rateCard = props => {
  return (
    <React.Fragment>
      <div className="rating">
        <h3>{siteConfig.locale.critics_rating}</h3>
        <div className="star-and-value">
          <span className="rating-stars">
            <span className="empty-stars" />
            {props && props.cr ? (
              <span style={{ width: parseInt(props.cr * 20) + "%" }} className={"filled-stars critic"} />
            ) : null}
          </span>
          <span className="ratespan">
            {props && props.cr ? props.cr : "NA"}
            {props && props.cr ? <small>/5</small> : null}
          </span>
        </div>
      </div>
      <div className="rating">
        <h3>{siteConfig.locale.user_rating}</h3>
        <div className="star-and-value">
          <span className="rating-stars">
            <span className="empty-stars" />
            {props && props.ur ? (
              <span style={{ width: parseInt(props.ur * 20) + "%" }} className={"filled-stars users"} />
            ) : null}
          </span>
          <span className="ratespan">
            {props && props.ur ? props.ur : "NA"}
            {props && props.ur ? <small>/5</small> : null}
          </span>
        </div>
      </div>
    </React.Fragment>
  );
};

const RateBox = props => {
  let showSubmitButton = true;
  // let movieRatings = [];
  // if (typeof window !== "undefined") {
  //   movieRatings = JSON.parse(window.sessionStorage.getItem("mr-ratings"));
  // }
  // if (Array.isArray(movieRatings) && movieRatings.includes(props.id)) {
  //   showSubmitButton = false;
  //   let rateStar = document.querySelector(".hover-stars");
  //   if (rateStar) {
  //     rateStar.classList.add("disable");
  //   }
  // }
  return (
    <div className="rateBox" id={"ratingbox_" + props.id}>
      {process.env.PLATFORM == "desktop" ? (
        <React.Fragment>
          {/* <div className="head">
            <b>Rating</b>
          </div> */}
          {rateCard(props)}
        </React.Fragment>
      ) : null}

      <div className="rate_con rating">
        <h3>{locale.rate_text ? locale.rate_text : locale.slide_to_rate_movie}</h3>
        <div className="star-and-value">
          <span className="rating-stars">
            <span className="empty-stars" />
            {process.env.PLATFORM == "desktop" ? createStars(props.id,  props.pagetype) : null}
            <span className="filled-stars ratestar w50" id={"ratestar_" + props.id} />
          </span>
          <span className="ratespan" id={"rating_" + props.id}>
            <span id={"ratespan_" + props.id}>{usrRate}</span>
            <small>/5</small>
          </span>
        </div>
        {process.env.PLATFORM == "mobile" ? (
          <input className="rangeslider" type="range" min="0" max="5" step=".5" id={"rateslider_" + props.id} />
        ) : null }

        {/* <label className="custom-checkbox">
              <input
                className="rangesubmit"
                type="checkbox"
                name="submit"
                value="submit"
                
              />
              <span className="checkmark" />
            </label> */}
        {showSubmitButton ? (
          <button
            className="btn-submit"
            id={"ratesubmit_" + props.id}
            // onClick={e => handleSubmit(e, props.id, props.pagetype)}
            onClick={e => submitRating(props.id, props.pagetype, e)}
          >
            Submit
          </button>
        ) : null}

        <span id={`rating-status-${props.id}`} className="rating-status">
          {!showSubmitButton ? "Thank you for rating this movie!" : ""}
        </span>
      </div>
    </div>
  );
};

const TabsComponent = (props, review, sharedata, openCommentsPopup) => {
  return (
    <div className="tabs_circle">
      {process.env.PLATFORM == "mobile" || props.pagetype == "techreview" ? (
        <ul className="tabs_circle_list">
          <li onClick={excuteReview.bind(this, props.id, props.sharedata, openCommentsPopup)}>
            {props.pagetype != "techreview" ? locale.review_movie : locale.review_device}
          </li>
          <li
            id={"rating_" + props.id}
            onClick={e => {
              excuteRating(props.id, props.pagetype);
            }}
          >
            {props.pagetype != "techreview" ? locale.rate_movie : locale.rate}
            <input className="hide" type="checkbox" />
          </li>
        </ul>
      ) : null}
      {_isCSR() ? checkRating(props.id) : null}
    </div>
  );
};

const Topfeature = item => {
  {
    return item.topfeature["#text"] != null && item.topfeature && Array.isArray(item.topfeature["#text"]) ? (
      <div className="top_features">
        <h3>{siteConfig.locale.tech.topfeature}</h3>
        <ul>
          {item && item.topfeature && item.topfeature["#text"] && Array.isArray(item.topfeature["#text"])
            ? item.topfeature["#text"].map((item, index) => {
              return <li>{item}</li>;
            })
            : null}
        </ul>
      </div>
    ) : null;
  }
};

const UserReview = props => {
  let arrRatings = [];
  if (props.userratings && props.userratings.ratings) {
    arrRatings.push(props.userratings.ratings);
  } else if (props.userratings) {
    arrRatings = [...props.userratings];
  }

  let userrate = props.userrating;
  let userrating = [],
    pos,
    arrSpan = [],
    max = 0,
    rateStyle = {};
  userrating.length = 5;

  arrRatings.map((item, index) => {
    pos = item.rating / 2 - 1;
    userrating[pos] = item;
    max = parseInt(item.count) > max ? item.count : max;
  });

  for (let i = 4; i >= 0; i--) {
    rateStyle = {
      width: userrating[i] ? (parseInt(userrating[i].count) / parseInt(max)) * 100 + "%" : "0%",
    };
    arrSpan.push(
      <li>
        <span className="star_txt">{i + 1 + " ★ "}</span>
        <span className="star_bg">
          <span className={"star" + (i + 1)} style={rateStyle}>
            {userrating[i] ? userrating[i].count : "0"}
          </span>
        </span>
      </li>,
    );
  }

  return (
    <div className="rating-values" id={`rating-values${props.id}`}>
      <div className="user_review">
        {userrate ? (
          <div className="rating">
            <span>
              {userrate}
              <sub>/5</sub>
            </span>
          </div>
        ) : (
          "Be the first one to review"
        )}
      </div>

      {userrating ? (
        <div className="user_star">
          <ul>{arrSpan}</ul>
        </div>
      ) : null}
    </div>
  );
};

const GanaCard = props => {
  var gaanalink = props.link != undefined && props.link.indexOf("//gaana.com") > 0 ? props.link : "";
  return gaanalink != "" ? (
    <div className={styles["listen-on"] + " listen-on"}>
      <a target="_blank" href={gaanalink}>
        <img src={`${siteConfig.cookieLessDomain}/img/gaana-logo.png`} /> पर इस ऐल्बम को सुनें
      </a>
    </div>
  ) : null;
};

const getDuration = min => {
  min = parseInt(min);
  if (min) {
    var hours = min / 60;
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return (
      <span className="duration" itemProp="duration">
        {!isMobilePlatform() ? <b>{`${siteConfig.locale.duration}:`}</b> : ""}
        {rhours + " Hrs " + rminutes + " Min"}
      </span>
    );
  } else {
    return null;
  }
};

// Return Actors with microdata schema
const getMovieCast = casting => {
  let actors = casting.split(",");
  return (
    <div className="cast text_ellipsis" itemProp="actors" itemScope="itemScope" itemType="https://schema.org/Person">
      {!isMobilePlatform() ? <b>{`${siteConfig.locale.actors}:`}</b> : ""}
      <div className="items" itemProp="name">
        {actors.map((item, index) => {
          return (
            <span key={index}>
              {item}
              {actors.length === index + 1 ? "" : ","}
            </span>
          );
        })}
      </div>
    </div>
  );
};

//  Earlier openCommentPopup was opened with sharedata, but it doesn't contain msid for the same.
const excuteReview = (id, sharedata, openCommentsPopup) => {
  if (isMobilePlatform()) {
    let commentBox = document.getElementById("comment_" + id);
    if (commentBox) commentBox.click();
  } else {
    openCommentsPopup();
  }
};

const excuteRating = (id, pagetype) => {
  let rateButton = document.getElementById("rating_" + id);
  let rateArea = document.getElementById("rating-values" + id);
  let hidden_checkbox = rateButton.querySelector(".hide");
  let rateBox = document.getElementById("ratingbox_" + id);
  let rateSlider = document.getElementById("rateslider_" + id);
  let rateStar = document.getElementById("ratestar_" + id);
  let rateSpan = document.getElementById("ratespan_" + id);
  let rateSubmit = document.getElementById("ratesubmit_" + id);
  let emptyStars = document.querySelector(".empty-stars");
  // let _rating = 0;

  // if (!rateBox || !rateSlider || !rateSubmit) return false;
  if(hidden_checkbox){
    hidden_checkbox.checked = !hidden_checkbox.checked;
    hidden_checkbox.checked ? rateBox.classList.add("visible") : rateBox.classList.remove("visible");
    hidden_checkbox.checked ? rateButton.classList.add("active") : rateButton.classList.remove("active");
    if (rateArea) {
      hidden_checkbox.checked ? rateArea.classList.add("visible") : rateArea.classList.remove("visible");
    }
  }

  if (process.env.PLATFORM == "mobile") {
    rateSlider.oninput = e => {
      usrRate = parseFloat(e.target.value);
      e.target.style.background = `linear-gradient(to right, #00b3ff 0%,#00b3ff ${usrRate * 2 * 10}%,#fff ${usrRate *
        2 *
        10}%, #fff 100%)`;
      rateStar.classList.value = "";
      rateStar.classList.add("w" + usrRate * 2 * 10);
      rateStar.classList.add("filled-stars");
      rateStar.classList.add("ratestar");
      rateSpan.innerHTML = usrRate;
    };
  }

  if (rateSubmit) {
    rateSubmit.onchange = e => {
      if (e.target.checked && _rating > 0) {
        submitRating(id, pagetype, e);
      }
    };
  }
};

const submitRating = (id, pagetype, e) => {
  let rateSlider = document.getElementById("rateslider_" + id);
  let rateBox = document.getElementById("ratingbox_" + id);
  let ratingStatus = document.getElementById("rating-status-" + id);
  let rateSpan = document.getElementById("ratespan_" + id);
  let selectedRating = rateSpan.innerText;
  let ratingurl =
    pagetype == "techreview"
      ? siteConfig.mweburl + `/pwafeeds/sc_rategadget.cms?msid=${id}&getuserrating=1&criticrating=&vote=`
      : siteConfig.mweburl +
      (pagetypedef == "tech" ? "/tech" : "") +
      "/off-url-forward/rate_moviereview.cms?msid=" +
      id +
      "&getuserrating=1&criticrating=" +
      "&vote=";

  
  // checkbox submit, disable when rated and color change
  let submittedRating = _rating;
  if (process.env.PLATFORM == "mobile") {
    submittedRating = rateSlider.value;
  }
  ratingurl = ratingurl + Number(submittedRating) * 2;
    let ratingSpan;
    ratingSpan = document.getElementById("ratespan_" + id);
    if (window._user) {
      mytActivityLogging("Rated", id, "", Number(submittedRating) * 2, "")
      fetch(ratingurl, { type: "jsonp" })
        .then(
          () => {
            if (typeof window !== "undefined") {
              let submittedRatings = [];
              if (process.env.PLATFORM == "mobile") {
                rateSlider.classList.add("rate-submit");
                rateSlider.setAttribute("disabled", true);
                rateSlider.style.pointerEvents = "none";
            
              }
              if (window.sessionStorage.getItem("mr-ratings")) {
                submittedRatings = JSON.parse(window.sessionStorage.getItem("mr-ratings"));
              }
              submittedRatings.push(id);
              window.sessionStorage.setItem("mr-ratings", JSON.stringify(submittedRatings));
              const submitButton = document.getElementById(`ratesubmit_${id}`);
              if (submitButton) {
                submitButton.style.display = "none";
              }
              const hoverStars = document.querySelector(`.hover-stars`);
              if (hoverStars) {
                hoverStars.classList.add("disable");
              }
              ratingSpan.innerHTML = `<b>${submittedRating}</b>`;
              if (ratingStatus) {
                ratingStatus.innerHTML = "Thank you for submitting rating!";
              }
              // TP Activity Logging
              if (typeof fireActivity == "function") {
                fireActivity("MOVIE_RATE", `mvrate_${id}`);
              }
            }

            if (isMobilePlatform()) {
              setTimeout(() => {
                document.getElementById("rating_" + id).innerHTML = `<span><b>${submittedRating}</b>/5</span>`;
                rateBox.classList.remove("visible");
                let rateButton = document.getElementById("rating_" + id);
                rateButton.style.pointerEvents = "none";
                rateButton.classList.add("btn-rated");
              }, 2000);
            }
          },
          () => {
            if (ratingStatus) {
              ratingStatus.innerHTML = "ERROR";
            } else {
              rateBox.innerHTML = "ERROR";
            }
          },
        )
        .catch(() => {
          if (ratingStatus) {
            ratingStatus.innerHTML = "ERROR";
          } else {
            rateBox.innerHTML = "ERROR";
          }
        });
    } else {
      if (typeof document) {
        document.addEventListener("user.status", ()=>userLoginCallback(id, pagetype, e), false);
      }
      ratingSpan.addEventListener('click', openLoginPopup());
    }
};

const userLoginCallback = (id, pagetype, e) => {
  submitRating(id, pagetype, e);
}

const checkRating = id => {
  _checkUserStatus(user => {
    let ratingSpan;
    if (user.detail) {
      let alreadyRatedURL = `${siteConfig.sso["MYTIMES_ALREADY_RATED"]}${siteConfig.sso['APP_KEY']}&userId=${user.detail._id}&baseEntityId=0&uniqueAppKey=${id}`;
      fetch(alreadyRatedURL).then(text => {
        if (text && text != "") {
          const userRating = parseInt(text) / 2;
          ratingSpan = document.getElementById("ratespan_" + id);
          ratingSpan.innerHTML = `<b>${userRating}</b>`;
          ratingSpan.style.pointerEvents = "none";
          ratingSpan.classList.add("btn-rated");
          const submitButton = document.getElementById(`ratesubmit_${id}`);
          if (submitButton) {
            submitButton.style.display = "none";
          }
          const hoverStars = document.querySelector(`.hover-stars`);
          if (hoverStars) {
            hoverStars.classList.add("disable");
          }
          const rangeslider = document.getElementById("rateslider_" + id);
          if (rangeslider) {
            rangeslider.style.display = "none";
          }
          if (isMobilePlatform()) {
              document.getElementById("rating_" + id).innerHTML = `<span><b>${userRating}</b>/5</span>`;
              let rateBox = document.getElementById("ratingbox_" + id);

              rateBox.classList.remove("visible");
              let rateButton = document.getElementById("rating_" + id);
              rateButton.style.pointerEvents = "none";
              rateButton.classList.add("btn-rated");
          }
          let rateStar = document.getElementById("ratestar_" + id);
          rateStar.classList.value = "";
          rateStar.classList.add("w" + userRating * 2 * 10);
          rateStar.classList.add("filled-stars");
          rateStar.classList.add("ratestar");
        }
        
        // else{
        //   ratingSpan.addEventListener('click',excuteRating.bind(this,id));
        // }
      });
      // .catch((err)=> ratingSpan.addEventListener('click', excuteRating.bind(this,id)))
    } else {
      // ratingSpan.addEventListener('click',openLoginPopup(excuteRating.bind(this,id)));
    }
  });
};

const mytActivityLogging = (activityType, _m_msid, _m_msg, rateval, exCommentTxt, isTech = false, isUserReview = false) => {
  if (_m_msid != undefined && _m_msid != null) {
    const ssoconfigapikey =
      isTech ||
        isUserReview
        ? "APP_KEY_TECH"
        : "APP_KEY";
    let src = siteConfig.sso["MYTIMES_ADD_ACTIVITY"] + siteConfig.sso[ssoconfigapikey];
    src = src + "&activityType=" + activityType;
    src = src + "&uniqueAppID=" + _m_msid;
    src = src + "&baseEntityType=MOVIEW_REVIEW";
    src = src + "&objectType=B";
    src = src + "&url=" + window.location.href;
    if (rateval != null && rateval != undefined && rateval != "" && rateval != "0") {
      src = src + "&userrating=" + rateval;
    }
    if (exCommentTxt != null && exCommentTxt != undefined && exCommentTxt != "") {
      src = src + "&exCommentTxt=" + exCommentTxt;
    }
    if (_m_msg != null && _m_msg != undefined && _m_msg != "") {
      src = src + "&via=" + _m_msg;
    }
    var img = new Image(1, 1);
    img.src = src;
  }
};

export default MoviePoster;
