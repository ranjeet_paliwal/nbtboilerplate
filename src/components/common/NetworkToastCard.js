import React, { Component } from "react";
import styles from "./css/NetworkToastCard.scss";

import { _getStaticConfig } from "../../utils/util";
import SvgIcon from "../common/SvgIcon";
const siteConfig = _getStaticConfig();

class NetworkToastCard extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let _this = this;
    if (typeof window != "undefined") {
      // check first for navigator.online status
      if (typeof navigator != "undefined" && !navigator.onLine)
        _this.toggleElementClass("offline");

      window.addEventListener("offline", function(event) {
        console.log("You lost connection.");
        _this.toggleElementClass("offline");
      });
      window.addEventListener("online", function(event) {
        console.log("You are now back online.");
        _this.toggleElementClass("online");
      });
    }
  }

  closeclick = e => {
    e.preventDefault();
    e.target.parentElement.className = "networkContainer";
  };

  toggleElementClass(type) {
    let element = document.querySelector(".networkContainer");

    if (type == "offline") {
      //Appending offline class to body to fade video icons
      document.body.classList.add("offline");
      element.classList.add("offline");
      element.querySelector(".con_wrap").innerHTML =
        "<span>" +
        siteConfig.locale.oops +
        "</span>" +
        siteConfig.locale.offlineText;
      element.classList.remove("online");
    } else if (type == "online") {
      document.body.classList.remove("offline");
      element.classList.add("online");
      element.querySelector(".con_wrap").innerHTML =
        "<span>" +
        siteConfig.locale.thanks +
        "</span>" +
        siteConfig.locale.onlineText;
      element.classList.remove("offline");
    }
    // if (element.classList.contains("offline")) {
    //     element.classList.add("online");
    //     element.querySelector('.con_wrap').innerHTML = '<span>'+siteConfig.locale.thanks+'</span>'+siteConfig.locale.onlineText;
    //     element.classList.remove("offline");
    // } else {
    //     element.classList.add("offline");
    //     element.querySelector('.con_wrap').innerHTML = '<span>'+siteConfig.locale.oops+'</span>'+siteConfig.locale.offlineText;
    //     element.classList.remove('online');
    // }
  }

  render() {
    return (
      <div
        id="networkContainer"
        className="networkContainer"
        data-exclude="amp"
      >
        <span className="close_icon" onClick={this.closeclick}></span>
        <div className="table">
          <div className="table_row">
            <div className="table_col img_wrap">
              <SvgIcon name="offline" className="offline_icon" />
            </div>
            <div className="table_col con_wrap">
              <span>{siteConfig.locale.oops} </span>
              {siteConfig.locale.offlineText}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NetworkToastCard;
