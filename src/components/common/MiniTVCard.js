/* eslint-disable jsx-a11y/no-distracting-elements */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ImageCard from "./ImageCard/ImageCard";
import Minitv from "../../modules/videoplayer/minitv";
import { setMinitvVisibility, setMinitvPlaying, setMinitvWidgetClick } from "../../actions/videoplayer/videoplayer";
import { _getStaticConfig, _sessionStorage, isMobilePlatform } from "../../utils/util";

import styles from "./css/Notification-cards.scss";

const siteConfig = _getStaticConfig();

class MiniTVCard extends Component {
  closeclick = e => {
    e.preventDefault();
    e.stopPropagation();
    e.target.parentElement.remove();
    // set session storage close
    _sessionStorage().set("minitv", "close");
  };

  playMinitv = () => {
    // Remove this if user manually clicks on minitv strip
    if (_sessionStorage().get("MINITV_CLOSED")) {
      _sessionStorage().remove("MINITV_CLOSED");
    }
    if (isMobilePlatform()) {
      this.props.dispatch(setMinitvWidgetClick(true));
    }
    this.props.dispatch(setMinitvVisibility(true));
    this.props.dispatch(setMinitvPlaying(true));
  };

  checkRender = () => {
    const { minitv, videoplayer, location } = this.props;
    // MiniTvCard Shouldn't render for TimesPoint pages.
    const isTimesPointsPage =
      (location && typeof location.pathname == "string" && location.pathname.includes("timespoints.cms")) || false;
    return minitv && minitv.hl && videoplayer && !videoplayer.showMinitv && !isTimesPointsPage;
  };

  imageSizeDecider = type => {
    let imageSize = "smallthumb";
    if (type === "videolist" || type === "minitv") {
      imageSize = "smallwidethumb";
    }
    if (type === "photolist" || type === "photo") {
      imageSize = "midthumb";
    }
    return imageSize;
  };

  render() {
    const { minitv, className } = this.props;
    const stripRender = this.checkRender();
    return (
      <React.Fragment>
        {stripRender && (
          <div
            data-videourl="#"
            role="button"
            tabIndex={0}
            className={`top_notification minitv-in-list ${className}` || ""}
            onClick={() => this.playMinitv(minitv)}
            onKeyUp={() => this.playMinitv(minitv)}
          >
            {isMobilePlatform() && (
              <span
                role="button"
                tabIndex={0}
                className={`${styles.close_icon} close_icon`}
                onClick={this.closeclick}
                onKeyUp={this.closeclick}
              />
            )}
            <div className="table">
              <a className="table_row">
                <span className="table_col img_wrap minitv_icon">
                  <ImageCard
                    msid={minitv.imageid ? minitv.imageid : minitv.id}
                    title={minitv.seotitle ? minitv.seotitle : ""}
                    size={this.imageSizeDecider(minitv.tn)}
                  />
                  {isMobilePlatform() ? <span className="strip">WATCH</span> : ""}
                </span>
                {!isMobilePlatform() ? <span className="table_col strip">WATCH</span> : ""}
                <span className="table_col con_wrap">
                  {/* <h3 className="text_ellipsis">{minitv.hl ? minitv.hl : siteConfig.locale.live_video}</h3> */}
                  <h3 className="text_ellipsis">
                    {isMobilePlatform() && (minitv.hl ? minitv.hl : siteConfig.locale.live_video)}
                    {!isMobilePlatform() && (
                      <marquee direction="left" scrolldelay="100">
                        {minitv.hl ? minitv.hl : siteConfig.locale.live_video}
                      </marquee>
                    )}
                  </h3>
                </span>
              </a>
            </div>
          </div>
        )}
        {!this.checkRender() && !isMobilePlatform() && !_sessionStorage().get("MINITV_CLOSED")
          ? ReactDOM.createPortal(<Minitv item={minitv} />, document.getElementById("outer_minitv_container"))
          : null}
      </React.Fragment>
    );
  }
}

MiniTVCard.propTypes = {
  dispatch: PropTypes.func,
  minitv: PropTypes.object,
  videoplayer: PropTypes.object,
  className: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    videoplayer: { ...state.videoplayer },
  };
}

export default connect(mapStateToProps)(MiniTVCard);
