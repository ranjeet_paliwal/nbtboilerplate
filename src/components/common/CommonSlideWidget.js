import React, { Component } from "react";
import fetch from "utils/fetch/fetch";
import PropTypes from "prop-types";
import ImageCard from "./ImageCard";
import AnchorLink from "./AnchorLink";

import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import FakeListing from "./FakeCards/FakeListing";
import styles from "./../../components/common/css/GadgetNow.scss";
import { _getStaticConfig, _isCSR, _getCategory } from "../../utils/util";

const siteConfig = _getStaticConfig();

class CommonSlideWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: ""
    };
  }

  render() {
    const {
      items,
      twoGud,
      pagename,
      subsec1,
      index,
      subsec,
      heading,
      overridelink
    } = this.props;
    const amzIndex = index ? index : 0;
    //remove paytm content if exist
    let amzonContent =
      items && items[amzIndex] && twoGud && twoGud == "1"
        ? items[amzIndex].product
        : items && twoGud && twoGud == "1"
        ? items.product
        : items != undefined &&
          items[amzIndex] &&
          items[amzIndex].product != undefined &&
          process.env.SITE == "nbt"
        ? items[amzIndex].product.filter(function(item) {
            return item.affiliate == "amazon";
          })
        : null;
    return amzonContent && amzonContent.length > 0 ? (
      <div className="_paytmsponsored wdt_amazon">
        {heading ? (
          <h3>
            <span>{heading}</span>{" "}
            <img
              src="https://navbharattimes.indiatimes.com/photo/70699906.cms"
              width="80"
            />
          </h3>
        ) : siteConfig.locale &&
          siteConfig.locale.tech &&
          siteConfig.locale.tech.purchasehere ? (
          <h3>
            <span>{siteConfig.locale.tech.purchasehere}</span>{" "}
            <img
              src="https://navbharattimes.indiatimes.com/photo/58606011.cms"
              width="80"
            />
          </h3>
        ) : null}
        <ul>
          {amzonContent.map((item, index) => {
            let url = item.url ? item.url : "";
            let title = item.titleEn ? item.titleEn : item.title;
            let price = item.price ? item.price : "";
            // let prodUrl = 'https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=' + getURL(url) + '&tag=mweb_nbt_sponsored_' + pagename + '&title=' + removespace(title) + '&price=' + price + '&utm_source=gadgetsnow&'
            let prodUrl;
            if (overridelink == "1") prodUrl = item.prodUrl;
            else
              prodUrl =
                "https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=" +
                getURL(url) +
                "&utm_campaign=" +
                getTagName(pagename, subsec) +
                "&title=" +
                removespace(title) +
                "&price=" +
                price +
                "&utm_source=" +
                pagename +
                "&utm_medium=Affiliate";
            return (
              // index < 3 ?
              <li className="_paytm_card" key={"paytm" + index}>
                <a target="_blank" rel="nofollow" href={prodUrl}>
                  <div className="wgt-paytm tbl">
                    <div className="items tbl_row">
                      <div className="tbl_col prod_img">
                        {typeof item.imageUrl == "string" ? (
                          <img
                            src={item.imageUrl}
                            alt={item.titleEn}
                            title={item.title}
                          />
                        ) : (
                          <img
                            src={
                              "https://navbharattimes.indiatimes.com/photo/" +
                              siteConfig.imageconfig.thumbid +
                              ".cms"
                            }
                            alt={item.titleEn}
                            title={item.title}
                          />
                        )}
                      </div>
                      <div className="des tbl_col">
                        {item.title && item.title != "" ? (
                          <div className="heading">
                            <span className="text_ellipsis">{item.title}</span>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  <div className="tbl _paytm_price">
                    <div className="tbl_row">
                      <div className="tbl_col">
                        {item.price && item.price != "" ? (
                          <span className="main-price">
                            &#x20b9; {item.price.split(".")[0]}
                          </span>
                        ) : null}
                        {item.price && item.oldprice != "" ? (
                          <span className="old-price"> {item.oldprice}</span>
                        ) : null}
                      </div>
                      <div className="tbl_col rgt-btn">
                        {siteConfig.locale &&
                        siteConfig.locale.tech &&
                        siteConfig.locale.tech.purchase ? (
                          <span className="btn-buy">
                            {siteConfig.locale.tech.purchase}
                          </span>
                        ) : null}
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              // : null
             /*: null*/);
          })}
        </ul>
      </div>
    ) : null;
  }
}
const getURL = url => {
  if (url.indexOf("https://www.amazon.in") > -1) {
    return url.replace("https://www.amazon.in", "");
  } else {
    return url;
  }
};

const getTagName = (pagename, subsec) => {
  let pagetype = "";

  if (subsec == "2354729") {
    pagetype = "nbt_wap_relationship_articleshow_sponsoredwidget-21";
    return pagetype;
  }
  if (pagename) {
    switch (pagename) {
      case "home":
        pagetype = "nbt_wap_mainhome_sponsoredwidget-21";
        break;

      case "articlelist":
        pagetype = "nbt_wap_articlelist_sponsoredwidget-21";
        break;
      case "articleshow":
        pagetype = "nbt_wap_articleshow_sponsoredwidget-21";
        break;
      case "compare":
        pagetype = "nbt_wap_articlelist_sponsoredwidget-21";
        break;
      case "photoshow":
        pagetype = "nbt_wap_photoshow_sponsoredwidget-21";
        break;
        -21;
      case "gadgetshow":
        pagetype = "nbt_wap_pdp_sponsoredwidget-21";
        break;
      case "compareshow":
        pagetype = "nbt_wap_comparisondetail_sponsoredwidget-21";
        break;
    }
    return pagetype;
  }
};

const removespace = title => {
  if (typeof title != "undefined" && typeof title != "object") {
    title = title
      .trim()
      .replace(/\s+/g, "-")
      .toLowerCase();
    return title;
  } else return;
};
CommonSlideWidget.propTypes = {
  item: PropTypes.object
};

export default CommonSlideWidget;
