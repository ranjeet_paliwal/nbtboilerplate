import React, { Component } from "react";
import { _getStaticConfig } from "../../utils/util";

const siteConfig = _getStaticConfig();

class SearchCard extends Component {
  submitSearch(e) {
    e.preventDefault();
    document.getElementById("search-chk").checked = !document.getElementById(
      "search-chk"
    ).checked;
    //e.stopPropagation();
    let _this = this;
    let querytext = _this.refs.searchStringInput.value;
    querytext = querytext.replace(/\s+/g, " ").trim();
    querytext = querytext.replace(/[`~!#%()+\=?;:'",.<>\{\}\[\]\\\/]/gi, "");
    if (!querytext) {
      _this.refs.searchStringInput.value = "";
      return false;
    } else {
      window.location.href =
        siteConfig.mweburl + "/topics/" + _this.refs.searchStringInput.value;
    }
  }

  render() {
    return (
      <div className="search" data-exclude="amp">
        <form onSubmit={this.submitSearch.bind(this)} name="srchFrm">
          <label
            className="search_icon"
            htmlFor="search-chk"
            onClick={() => {
              this.refs.searchStringInput.focus();
            }}
          >
            Search
          </label>
          <input id="search-chk" type="checkbox" />
          <div className="table searchbox" id="srchFrm">
            <div className="table_row">
              <div className="table_col txtbox">
                <label className="search_box _hide" htmlFor="searchBox">
                  {siteConfig.locale.search_placeholder}
                </label>
                <input
                  ref="searchStringInput"
                  id="searchBox"
                  type="text"
                  placeholder={siteConfig.locale.search_placeholder}
                  name="query"
                />
              </div>
              <div className="table_col search">
                <input
                  type="submit"
                  onClick={this.submitSearch.bind(this)}
                  value={siteConfig.locale.search}
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchCard;
