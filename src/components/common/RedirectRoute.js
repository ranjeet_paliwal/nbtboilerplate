import React from "react";
import { Redirect, Route } from "react-router";

const RedirectRoute = props => {
  let redirect = false,
    redirectlocation = "";
  const { pwa_meta, pagetype } = props;
  if (pwa_meta && pwa_meta.pagetype !== pagetype) {
    redirectlocation = pwa_meta.canonical.split(".com")[1];
    redirect = true;
    console.log(redirect);
  }

  if (redirect) <Redirect to={redirectlocation} />;

  return null;
};

export default RedirectRoute;
