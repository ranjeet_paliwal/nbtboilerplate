import React from "react";
import ReactDOM from "react-dom";
import ImageCard from "./ImageCard/ImageCard";
import AnchorLink from "./AnchorLink";
import AdCard from "./AdCard";
import Parser from "html-react-parser";

import { parserOptions } from "./../lib/htmlReactParser/index";
import { isMobilePlatform, hasValue, _isCSR, _getStaticConfig } from "./../../utils/util";
import { fetchListDataIfNeeded } from "../../actions/topics/topics";
import { AnalyticsGA } from "../lib/analytics";
import SocialShare from "./SocialShare";
import Accordion from "./Accordion";
const siteConfig = _getStaticConfig();
import SectionHeader from "./SectionHeader/SectionHeader";
import SvgIcon from "./SvgIcon";
import { fireGRXEvent } from "../lib/analytics/src/ga";
import SummaryCard from "./SummaryCard";

/*TopicsCard componet will retrun data for topics page
  props : data  array(data listing)
  props : type : string( componet type name)
  props : pagetype : string(topic page type :all/news/photos/videos)

*/

const TopicsCard = props => {
  let { data, type, pagetype, dispatch, params, query, router, templateName, dtype } = props;

  // pagetype = pagetype == "default" ? "all" : pagetype;

  switch (type) {
    case "topicsTabs":
      return (
        <div className="tabs-circle" data-exclude={templateName !== "glossarylist" ? "amp" : ""}>
          <ul>
            {data && Array.isArray(data) && data.length > 0
              ? data.map(item => {
                  return (
                    <li key={item.label} className={item.label === dtype ? "active" : ""}>
                      {/* {item.label === pagetype ? item.hl : <AnchorLink href={item.wu}>{item.hl}</AnchorLink>} */}
                      {item.label === dtype ? (
                        item.hl
                      ) : templateName && templateName == "glossarylist" ? (
                        <AnchorLink href={item.wu}>{item.hl}</AnchorLink>
                      ) : (
                        <span onClick={() => onTabClick({ item, dispatch, params, query, router })}>{item.hl}</span>
                      )}
                    </li>
                  );
                })
              : null}
          </ul>
        </div>
      );

    case "topicsListing":
    // const random = Math.round(Math.random() * 10000);
    // console.log('ctnadspos', ctnadspos);

    // return (
    //   <div className="row mr30">
    //     <div className="col12">
    //       <ul className="news-card listing">
    //         {data && Array.isArray(data) && data.length > 0
    //           ? data.map((item, index) => {
    //               if (item.type == "dfp") {
    //                 return (
    //                   <li key={item.position}>
    //                     <AdCard mstype={item.mstype} adtype="dfp" />
    //                   </li>
    //                 );
    //               } else if (item.type == "ctn") {
    //                 return (
    //                   <li key={item.position}>
    //                     <AdCard mstype={item.mstype} adtype="ctn" />
    //                   </li>
    //                 );
    //               } else {
    //                 return (
    //                   <li key={item.id} className={index < 3 ? "lead" : ""}>
    //                     <span className="img_wrap">
    //                       <AnchorLink href={item.wu}>
    //                         <ImageCard
    //                           msid={item.imageid ? item.imageid : item.id}
    //                           title={item.hl ? item.hl : ""}
    //                           size="smallthumb"
    //                         />
    //                       </AnchorLink>
    //                     </span>
    //                     <span className="con_wrap">
    //                       <AnchorLink href={item.wu}>
    //                         <span className="text_ellipsis">{item.hl}</span>
    //                         {item.tn == "video" ? <span>-video icon-</span> : null}
    //                         {item.du ? <span>{item.du}</span> : null}
    //                       </AnchorLink>
    //                     </span>
    //                   </li>
    //                 );
    //               }
    //             })
    //           : null}
    //       </ul>
    //     </div>
    //   </div>
    // );

    default:
      return "";
  }
};

const onTabClick = ({ item, dispatch, params, query, router }) => {
  if (item) {
    let url;
    let pathname = router.location && router.location.pathname;
    let search = router.location && router.location.search;
    // let splatArr = item.wu && item.wu.split("/");
    // let searchType = splatArr[3];
    let searchType = item.label;
    // if (searchType) {
    params["searchtype"] = searchType;
    // } else {
    //   params["searchtype"] = "default";
    // }

    if (searchType && searchType !== "all") {
      if (pathname.indexOf("searchtype") > -1) {
        let reExp = `/searchtype=\w+/`;
        pathname.replace(reExp, "searchtype=" + searchType);
      } else {
        pathname = pathname + ((pathname.indexOf("?") > -1 ? "&searchtype=" : "?searchtype=") + searchType);
      }
    }

    window.history.replaceState({}, "", pathname);
    fireGRXEvent("track", "page_view", { url: pathname });

    // AnalyticsGA.pageview(pathname);

    dispatch(fetchListDataIfNeeded(params, query, router, "dynamicPage"));
  }
};

const getShareDetail = props => {
  const { isShareAll, item } = props;
  if (!_isCSR()) return false;

  let currentHeadline = "";
  let currentShortUrl = "";
  let currentMSID = "";
  if (item) {
    currentHeadline = item.hl ? item.hl : "";
    currentShortUrl = item.pwa_meta && item.pwa_meta.m ? item.pwa_meta.m : null;
    currentMSID = item.id ? item.id : "";
  }

  let sharedata = {
    title: currentHeadline || document.title,
    url: location.href,
    short_url: currentShortUrl || null,
    // openCommentsPopup: this.openCommentsPopup,
    msid: currentMSID,
  };

  if (!isShareAll) return sharedata;

  //for watsapp sharing feature along with GA
  AnalyticsGA.event({
    category: "social",
    action: "Whatsapp_Wap_stickyAS",
    label: sharedata.url,
  });

  if (currentShortUrl != "" && currentShortUrl != null) {
    var info = sharedata.short_url; // check short micron url availability
    info += "/l" + siteConfig.shortutm;
    info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
  } else {
    var info = sharedata.url;
    info += "?utm_source=Whatsapp_Wap_stickyAS" + siteConfig.fullutm;
    info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
  }
  var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + encodeURIComponent(info);

  window.location.href = whatsappurl;

  return false;
};

export const LandingPageNewsCard = props => {
  const { heading, item, bannerObj, isGlossary } = props;

  return (
    <React.Fragment>
      <h1>{heading}</h1>
      <div className="source_sharing">
        {/* <div className="sourcedate">{isGlossary && item ? SummaryCard.Byline(item) : null}</div> */}
        {!isMobilePlatform() ? (
          <SocialShare sharedata={() => getShareDetail({ item, isShareAll: false })} />
        ) : _isCSR() ? (
          ReactDOM.createPortal(
            <div className="share_container" data-exclude="amp">
              <div className="share_whatsapp">
                <a
                  data-attr="share_whatsapp"
                  rel="nofollow"
                  onClick={() => getShareDetail({ item, isShareAll: true })}
                  href="javascript:void(0);"
                >
                  <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                </a>
              </div>

              <SocialShare sharedata={() => getShareDetail({ item, isShareAll: false })} />
            </div>,
            document.getElementById("share-container-wrapper"),
          )
        ) : (
          <div className="share_container" data-exclude="amp">
            <div className="share_whatsapp">
              <a
                data-attr="share_whatsapp"
                rel="nofollow"
                onClick={() => getShareDetail({ item, isShareAll: true })}
                href="javascript:void(0);"
              >
                <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
              </a>
            </div>

            <SocialShare sharedata={() => getShareDetail({ item, isShareAll: false })} />
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export const FAQ = props => {
  if (!props.data.mainEntity || !Array.isArray(props.data.mainEntity)) {
    return "";
  }

  return (
    <div className="accordion-container">
      <ol>
        {props.data.mainEntity.map(question => (
          <Accordion
            heading={question.name}
            description={question.acceptedAnswer ? question.acceptedAnswer.text : ""}
          />
        ))}
      </ol>
    </div>
  );
};

export const ShortProfile = props => {
  const { data, profile, businessType, item, heading, compType } = props;

  return (
    <React.Fragment>
      <ul className="lead-topics">
        <li className={`news-card ${isMobilePlatform() ? "lead" : "horizontal-lead"}`}>
          <div className="img_wrap">
            <ImageCard
              msid={data.imageid ? data.imageid : data.id}
              title={heading ? heading : ""}
              noLazyLoad="true"
              //size="largethumb"
              size="posterthumb"
            />
          </div>
          <div className="con_wrap">
            {compType != "dynamicPage" ? <h1>{heading}</h1> : null}
            {profile && Object.keys(profile).length > 0 ? (
              <ShortInfo data={profile} businessType={businessType} />
            ) : null}
            <div className="profile-date">Updated: {item.lu}</div>
            {profile && Object.keys(profile).length > 0 ? (
              <ul className="profileInfo">
                <ProfileInfo data={profile} businessType={businessType} />
              </ul>
            ) : null}
          </div>
        </li>
      </ul>
      {/* {!profile ? (
        <span className="only-txt">
          {item.Story
            ? Parser(item.Story, {
                replace: parserOptions.replace.bind(this, item, {}, null, {}),
              })
            : data.arttextxml}
        </span>
      ) : null} */}
    </React.Fragment>
  );
};

export const ShortInfo = props => {
  const { data, businessType } = props;
  const shortInfoObj = [];

  switch (businessType) {
    case "politician":
      const profileData = hasValue(data, "yrdata.0.states.0.cns.0.cnt.0");
      if (profileData) {
        shortInfoObj.push(profileData.age + " Years");
        shortInfoObj.push(profileData.pr);
      }

      break;
    case "cricketer":
      shortInfoObj.push(data.player_skill && data.player_skill.text);
      shortInfoObj.push(dateFormat(data.dob));
      break;
    default:
  }

  return (
    <ul className="shortInfo">
      <RenderShortInfo data={shortInfoObj} />
    </ul>
  );
};

const RenderShortInfo = props => {
  const { data } = props;
  return (
    <li>
      {data.map(item => {
        return <span>{item}</span>;
      })}
    </li>
  );
};

const dateFormat = date => {
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  const dateArr = date && date.split("/");
  return dateArr && `${monthNames[Number(dateArr[1])]} ${dateArr[0]} ${dateArr[2]}`;
};

const calcAssests = value => {
  const numbersArr = ["Ones", "Tens", "Hundred", "Thousand", "", "Lakh", "", "Crore", ""];

  if (value == 0) return 0;
  let count = 0,
    numStr = "",
    tmpVal = value;

  while (tmpVal > 10) {
    tmpVal = tmpVal / 10;
    count++;
  }
  if (numbersArr[count] == "") {
    numStr = numbersArr[count - 1];
    value = Number(value / Math.pow(10, count - 1)).toFixed(2);
  } else {
    numStr = numbersArr[count];
    value = Number(value / Math.pow(10, count)).toFixed(2);
  }

  return `${value} ${numStr}`;
};

const politicianProfileInfo = (data, profileObj) => {
  const profileData = hasValue(data, "yrdata.0.states.0.cns.0.cnt.0");

  if (profileData) {
    profileObj["Educational Qualification "] = profileData.edu;
    profileObj["Total Assets "] = calcAssests(profileData.asst);
    profileObj["Movable Assets "] = calcAssests(profileData.movasst);
    profileObj["Immovable Assets "] = calcAssests(profileData.asst - profileData.movasst);
    profileObj["Total Liabilities "] = calcAssests(profileData.liability);
  }
};

const playerProfileInfo = (data, profileObj) => {
  let teamArr,
    formats,
    teamStr = "",
    formatsArr = ["Test", "ODI", "T20I"];

  // profileObj["Nickname : "] = data.nick_name ? data.nick_name : "-";

  teamArr = data.majorteams && data.majorteams.team;
  teamArr &&
    teamArr.map(item => {
      teamStr += item.text + ",";
    });
  teamStr = teamStr.slice(0, teamStr.length - 1);

  profileObj["Major Teams : "] = teamStr;

  formats = data.stats && data.stats.format;
  formats &&
    formats.map(item => {
      if (formatsArr.indexOf(item.comp_type) > -1) {
        profileObj[item.comp_type + " debut "] = item.debut
          ? `${item.debut && item.debut.match_date && dateFormat(item.debut.match_date)} vs ${item.debut &&
              item.debut.vs.text} `
          : "NA";
      }
    });
};

export const ProfileInfo = props => {
  const { data, businessType } = props;

  const profileObj = {};

  switch (businessType) {
    case "politician":
      politicianProfileInfo(data, profileObj);
      break;
    case "cricketer":
      playerProfileInfo(data, profileObj);
      break;
    default:
  }

  return Object.keys(profileObj).map(key => {
    return (
      <li>
        <span className="head">{key}</span>
        <span className="content">{profileObj[key]}</span>
      </li>
    );
  });
};

export const ProfileDesc = props => {
  const { data, item } = props;
  // console.log("this.props-------------------", item.Story);

  return item.Story
    ? Parser(item.Story, {
        replace: parserOptions.replace.bind(this, item, {}, null, {}),
      })
    : data.arttextxml;
};

export default TopicsCard;
