import React, { Component } from "react";
// import { Link } from 'react-router';

import {
  _isCSR,
  scrollTo,
  throttle,
  gaEvent,
  _filterALJSON,
  isMobilePlatform,
  filterAlaskaData,
  _getStaticConfig,
  getPWAmetaByPagetype,
} from "./../../utils/util";
import styles from "./css/footer.scss";
import "./css/desktop/footer.scss";

import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import NetworkToastCard from "./NetworkToastCard";
import QuickLinksCard from "./QuickLinksCard";

import AnchorLink from "./AnchorLink";
import { interStitialHTML, intAdInit } from "./InterStitial";
// import { getPWAmetaByPagetype } from "./../../utils/util";
// import globalconfig from './../../../common/global';
// import { AnalyticsGA } from './../../components/lib/analytics/index';

const siteConfig = _getStaticConfig();
const footerConfig = _getStaticConfig("footer");

import SocialMediaChannels from "../../components/common/SocialMediaChannels/SocialMediaChannels";
import SvgIcon from "./SvgIcon";
// const logo = globalconfig[process.env.SITE].logo;
let pwaMeta = "";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBind: false,
      isGDPRCont: false,
    };
  }

  backToTop() {
    //let elem = document.documentElement;
    gaEvent("backtotop", "tap");
    if (typeof window != "undefined") {
      //alert("hello"+document.body.toString());
    }
    scrollTo(document.body, 0, 100);
    scrollTo(document.documentElement, 0, 300);
  }

  componentDidMount() {
    const { header } = this.props;
    const intConfig = filterAlaskaData(header && header.alaskaData, ["pwaconfig", "Interstitial"], "label");
    let _this = this;
    // console.log('pwa_meta in footer',pwaMeta);
    if (typeof window != "undefined") {
      if (window.geoinfo && window.geoinfo.isGDPRRegion) {
        this.setState({ isGDPRCont: true });
      }
      window.setTimeout(() => {
        this.afterRender(_this, intConfig);
      }, 1000);
    }
  }

  afterRender(_this, intConfig) {
    if (this.state._scrollEventBind == false) {
      window.addEventListener("scroll", throttle(_this.handleScroll.bind(_this), 100));
      this.setState({
        _scrollEventBind: true,
      });
    }
    //For interstitial and addtohome
    window.addEventListener("page_count", function(e) {
      if (e && e.detail && e.detail.pageview == 2) {
        console.log("Add to Home Screen : Page view 2");
        _this.addToHomeScreenHandler();
      }

      // console.log('page_count', e.detail.pageview);
      let intPV =
        document.querySelector("[data-pv]") && document.querySelector("[data-pv]").getAttribute("data-pv") != undefined
          ? document.querySelector("[data-pv]").getAttribute("data-pv")
          : 3;
      if (
        e &&
        e.detail &&
        e.detail.pageview % intPV == 0 &&
        process.env.PLATFORM === "mobile" &&
        intConfig &&
        intConfig._status == "true"
      ) {
        intAdInit();
      }
    });
  }

  //TO show addtohome guided screen
  addToHomeScreenHandler() {
    const curdateTime = Date.now();
    let addToHome = localStorage.getItem("AddToHome") ? parseInt(localStorage.getItem("AddToHome")) : null;
    let addToHomeExpiry = addToHome + 7 * 24 * 60 * 60 * 1000; //15 days

    if (window.deferredPrompt && ((addToHome && curdateTime > addToHomeExpiry) || !addToHome)) {
      localStorage.setItem("AddToHome", curdateTime);
      //Setting toggle to fire Add/Dismiss/AppInstall GA events in HTML.js accordingly
      window.manualAddToHomescreenPrompt = true;
      window.deferredPrompt.prompt();
      // window.deferredPrompt.userChoice
      // .then((choiceResult) => {
      //         if (choiceResult.outcome === 'accepted') {
      //           AnalyticsGA.event({ category: 'pwainstall', action: document.location.pathname, label: 'add' })
      //             //ga('send', 'event', 'pwainstall', document.location.pathname, 'add');
      //             console.log('User accepted the A2HS prompt');
      //         } else {
      //           AnalyticsGA.event({ category: 'pwainstall', action: document.location.pathname, label: 'dismiss' })
      //            // ga('send', 'event', 'pwainstall', document.location.pathname, 'dismiss');
      //             console.log('User dismissed the A2HS prompt');
      //         }

      //         window.deferredPrompt = null;
      // });
    } else {
      console.log("Add to Home Screen : Local storage set addToHome");
    }
  }

  handleGAEvent(gaEventOptions) {
    gaEvent(gaEventOptions.category, gaEventOptions.action);
  }

  handleScroll(e) {
    return true;

    // if (this.state._scrollEventBind == false) return;
    // //let scrollValue = document.body.scrollTop;

    // let scrollValue = document.body.scrollTop;
    // if (scrollValue == 0) {
    //   scrollValue = document.documentElement.scrollTop;
    // }

    // let body = document.body;
    // let html = document.documentElement;

    // let height = Math.max(
    //   body.scrollHeight,
    //   body.offsetHeight,
    //   html.clientHeight,
    //   html.scrollHeight,
    //   html.offsetHeight,
    // );

    // if (scrollValue > 100 && scrollValue + 1000 < height) {
    //   document.getElementById("backtotop").classList.add("stick");
    // } else {
    //   document.getElementById("backtotop").classList.remove("stick");
    // }

    // //work on header
    // let subHeaderElem;

    // subHeaderElem = document.getElementById("categoryHeader");
    // if (!subHeaderElem) {
    //   subHeaderElem = document.getElementById("wdt_pubcatnav");
    // }

    // let headerOffsetTop = subHeaderElem.offsetTop;
    // if (headerOffsetTop <= 0) headerOffsetTop = 60;
    // if (scrollValue > headerOffsetTop) {
    //   subHeaderElem.classList.add("fixed-header");
    //   this.setUpMoreMenu();
    // } else {
    //   subHeaderElem.classList.remove("fixed-header");
    //   this.setUpMoreMenu();
    // }
  }

  mapObject(object, callback) {
    if (typeof object !== "object") {
      return "";
    }

    if (Object.keys(object).length === 3) {
      return Object.keys(object).map(function(key, index) {
        return callback(key, object[key], index);
      });
    } else {
      return Object.keys(object).map(function(key, index) {
        if (
          key != "label" &&
          key != "weblink" &&
          key != "mweb" &&
          key != "error" &&
          key != "isFetching" &&
          key != "name"
        )
          return callback(key, object[key], index);
      });
    }
  }

  setUpMoreMenu() {
    //code for more subnavigation
    try {
      if (document.querySelector(".more-cat-wrapper").classList.contains("movefromright")) {
        let categoryHeader = document.getElementById("categoryHeader");
        let offsetTop = categoryHeader.offsetTop + categoryHeader.offsetHeight;
        document.querySelector(".more-cat-wrapper").style.top = offsetTop + "px";

        if (categoryHeader.classList.contains("fixed-header")) {
          document.querySelector(".more-cat-wrapper").style.height = "100vh";
        } else {
          let height = document.querySelector("#c_wdt_header_2").clientHeight + categoryHeader.clientHeight;
          let footerheight = document.querySelector(".ad1.FBN") ? document.querySelector(".ad1.FBN").clientHeight : 0;
          let windowHeight = document.body.clientHeight;

          document.querySelector(".more-cat-wrapper").style.height = windowHeight - height - footerheight + 5 + "px";
        }
      }
    } catch (ex) {}
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return false;
  // }

  render() {
    const { header, offfoot, initialState, pagetype, routing } = this.props;
    pwaMeta = getPWAmetaByPagetype(initialState, pagetype);

    let queryparameter = routing && routing.locationBeforeTransitions ? routing.locationBeforeTransitions.search : "";
    const FooterSeq = filterAlaskaData(header && header.alaskaData, ["pwaconfig", "footer"], "label");

    const intConfig = filterAlaskaData(header && header.alaskaData, ["pwaconfig", "Interstitial"], "label");

    return (
      <div id="footerContainer" className="transform" style={{ transform: "translateY(0px)" }}>
        <footer
          className={`${!isMobilePlatform() ? "footer desktopFooter" : "footer footer-pd-bottom"}`}
          id={isMobilePlatform() ? "mobileFooter" : "desktopFooter"}
        >
          {offfoot == 0 ? (
            <div>
              {/* <h4>खबरें एक झलक में</h4> */}
              {FooterSeq && typeof FooterSeq === "object"
                ? Object.keys(FooterSeq).map(function(key, index) {
                    if (
                      FooterSeq[key].label != undefined &&
                      FooterSeq[key].label != null &&
                      FooterSeq[key].cat &&
                      (FooterSeq[key].cat == "tech" || FooterSeq[key].cat == "all")
                    ) {
                      let FooterContent = FooterSeq[key];
                      return (
                        <div key={"links" + index} className="footlinks-wrapper">
                          <span className="head">{FooterSeq[key].label} : </span>
                          {FooterContent &&
                            typeof FooterContent === "object" &&
                            Object.keys(FooterContent).map(function(key, index) {
                              return FooterContent[key].label != undefined && FooterContent[key].label != null ? (
                                <AnchorLink key={"flinks" + index} href={FooterContent[key].weblink}>
                                  {FooterContent[key].label}
                                </AnchorLink>
                              ) : null;
                            })}
                        </div>
                      );
                    }
                  })
                : null}
            </div>
          ) : (
            <React.Fragment>
              {/* <h4>
                {siteConfig.locale && siteConfig.locale.footerheading
                  ? siteConfig.locale.footerheading
                  : ""}
              </h4> */}
              {FooterSeq && typeof FooterSeq === "object"
                ? Object.keys(FooterSeq).map(function(key, index) {
                    if (
                      FooterSeq[key].label != undefined &&
                      FooterSeq[key].label != null &&
                      (FooterSeq[key].cat == undefined || FooterSeq[key].cat == "all")
                    ) {
                      let FooterContent = FooterSeq[key];

                      return (
                        <div key={"links" + index} className="footlinks-wrapper">
                          <span className="head">{FooterSeq[key].label} : </span>
                          {FooterContent &&
                            typeof FooterContent === "object" &&
                            Object.keys(FooterContent).map(function(key, index) {
                              return FooterContent[key].label != undefined && FooterContent[key].label != null ? (
                                <AnchorLink key={"flinks" + index} href={FooterContent[key].weblink}>
                                  {FooterContent[key].label}
                                </AnchorLink>
                              ) : null;
                            })}
                        </div>
                      );
                    }
                  })
                : null}
            </React.Fragment>
          )}
          <ErrorBoundary key="quick_links">
            <div className="footlinks-wrapper">
              <QuickLinksCard pwaMeta={pwaMeta} />
            </div>
          </ErrorBoundary>
          <h4>{footerConfig.languagesHeading ? footerConfig.languagesHeading : ""}</h4>
          <div className="footlinks-wrapper">
            {footerConfig &&
              Array.isArray(footerConfig.languages) &&
              footerConfig.languages.map((item, index) => {
                return (
                  <AnchorLink key={"lang" + index} href={isMobilePlatform() ? item.link : item.weblink || item.link}>
                    {item.name}
                  </AnchorLink>
                );
              })}
          </div>
          {/* <div className="sociallink">
            <span className="heading">FOLLOW US ON</span>
            <span className="icons">
              <a
                rel="nofollow noopener"
                className="facebook"
                href={siteConfig.fb}
                target="_blank"
                title="facebook"
              />

              <a
                rel="nofollow noopener"
                className="twitter"
                href={siteConfig.twitter}
                target="_blank"
                title="twitter"
              />
              <a
                rel="nofollow noopener"
                className="youtube"
                href={siteConfig.youtubeUrl}
                target="_blank"
                title="youtube"
              />
            </span>
            <div className="clear" />
          </div> */}

          <div className="app-n-follow">
            <div className="app-wrapper">
              <span>{footerConfig && footerConfig.app && footerConfig.app.name}</span>
              <a
                href={footerConfig && footerConfig.app && footerConfig.app.link}
                target="_blank"
                rel="nofollow"
                className="andriod"
              >
                <SvgIcon name="android" center="center" />
              </a>
              <a
                href={footerConfig && footerConfig.app && footerConfig.app.linkIOS}
                target="_blank"
                rel="nofollow"
                className="ios"
              >
                <SvgIcon name="apple" center="center" />
              </a>
              <a
                href={footerConfig && footerConfig.app && footerConfig.app.linkHuawei}
                target="_blank"
                rel="nofollow"
                className="huawei"
              >
                <SvgIcon name="huawei" center="center" />
              </a>
            </div>
            <SocialMediaChannels parentClass="sociallink" siteConfig={siteConfig} icons="fb,tw,yt,telegram" heading={true} />
          </div>
          <div className={"footer-links " + styles["footlinks-wrapper"] + " footlinks-wrapper"}>
            {footerConfig &&
              footerConfig.links &&
              footerConfig.links.map((item, index) => {
                let href = item.absolutePath ? item.link : `${process.env.WEBSITE_URL}${item.link}`;
                const relVal = item.rel || false;
                if (isMobilePlatform(true) == "desktop" && item.isMobile) {
                  return "";
                }
                if (this.state.isGDPRCont && item.name === "Privacy policy") {
                  href = `${process.env.WEBSITE_URL}privacypolicyeu.cms`;
                }
                return (
                  <a key={index.toString()} rel={relVal} href={href} target="_blank">
                    {item.name}
                  </a>
                );
              })}
            <span>
              {_isCSR() && window.isCCPARegion && window.isCCPARegion === true ? (
                <a href={`${siteConfig.weburl}/ccpainfo.cms`} rel="nofollow">
                  Do not sell data
                </a>
              ) : null}
            </span>
          </div>
          <div className={styles["copy-right"] + " copy-right"}>
            Copyright - 2020 Bennett, Coleman &amp; Co. Ltd. All rights reserved. For reprint rights :{" "}
            <a rel="nofollow noopener" href="https://timescontent.com/" target="_blank">
              Times Syndication Service
            </a>
            {/* {_isCSR() ? (
              <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: `` }} />
            ) : process.env.SITE != "iag" ? (
              <div
                data-adtype="inuxu"
                className="ad1 inuxu emptyAdBox"
                data-id="div-gpt-ad-1558437895757-0-inuxu"
                data-name={siteConfig.ads.dfpads.inuxu}
                data-size="[[1, 1]]"
              />
            ) : null} */}
          </div>
        </footer>
        {/*<AdCard mstype='fbn' style={{'bottom' : '-60px'}} />*/}

        {/*offline online*/}
        <NetworkToastCard />

        {/* Interstital Ad */ process.env.PLATFORM === "mobile" ? interStitialHTML(intConfig) : null}
      </div>
    );
  }
}

Footer.propTypes = {};

export default Footer;
