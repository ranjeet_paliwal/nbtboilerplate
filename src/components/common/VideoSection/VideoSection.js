import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import SectionHeader from "../SectionHeader/SectionHeader";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import Slider from "../../desktop/Slider/index";
import "../css/desktop/CardVideoWidget.scss";
import VideoItem from "../VideoItem/VideoItem";
import { _getStaticConfig } from "../../../utils/util";
import fetch from "../../../utils/fetch/fetch";
import YSubscribeCard from "../../../components/common/YSubscribeCard";
// import { createConfig } from '../../../modules/videoplayer/utils';

const siteConfig = _getStaticConfig();
// const slikeApiKey = siteConfig.slike.apikey;

class VideoSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      videoData: "",
    };
  }

  componentDidMount() {
    if (!this.props.videoData) {
      this.fetchvideoData(this.props.videoSectionId);
    }
  }

  fetchvideoData(msid) {
    const newsId = msid || (siteConfig && siteConfig.video && siteConfig.video.newsId) || "";
    const rootId = (siteConfig && siteConfig.video && siteConfig.video.rootId) || "";
    fetch(
      `${process.env.API_BASEPOINT}/sc_homelist/${newsId}.cms?platform=web&type=video&count=17&secid=${rootId}&feedtype=sjson`, // Path needs to be changes by env Variable
    )
      // .then(res => res.json())
      .then(response => {
        // console.log("vdata", response);
        this.setState({ videoData: response });
      });
  }

  render() {
    let { videoData } = this.state;
    const { index, alaskaData } = this.props;
    if (this.props.videoData) {
      // eslint-disable-next-line prefer-destructuring
      videoData = this.props.videoData;
    }
    const videoDataCopy = JSON.parse(JSON.stringify(videoData));
    const firstVideoArr =
      videoDataCopy && videoDataCopy.items && Array.isArray(videoDataCopy.items) && videoDataCopy.items.length > 0
        ? videoDataCopy.items.splice(0, 1)
        : "";
    const firstVideo = (Array.isArray(firstVideoArr) && firstVideoArr[0]) || "";
    const restVideos = (videoDataCopy && videoDataCopy.items) || "";
    // console.log("ddd", siteConfig && siteConfig.video && siteConfig.video.rootId);
    return (
      <React.Fragment>
        {firstVideo && (
          <div className="card-video-widget">
            <div className="section">
              <ErrorBoundary>
                <SectionHeader
                  data={alaskaData}
                  sectionId={siteConfig && siteConfig.video && siteConfig.video.rootId}
                  weblink={videoDataCopy.override || videoDataCopy.wu}
                  morelink={false}
                  sectionhead={videoDataCopy && videoDataCopy.secname}
                />
              </ErrorBoundary>
            </div>
            <div className="video_body news-card video">
              <VideoItem
                item={firstVideo}
                videoIntensive={false}
                imgsize="largewidethumb"
                index={index}
                parentMsid={videoDataCopy ? videoDataCopy.id : ""}
                isSyn={true}
              />
            </div>
            <div className="video-slider">
              {restVideos && Array.isArray(restVideos) && restVideos.length > 0 && (
                <Slider
                  type="grid"
                  // subtype="videosection"
                  size="4"
                  videoIntensive={false}
                  sliderData={restVideos}
                  sliderClass="videosection"
                  margin="30"
                  width="210"
                  parentMsid={videoDataCopy ? videoDataCopy.id : ""}
                  noSeo
                />
              )}
            </div>
            <div className="btn-subscribe">
                <YSubscribeCard />
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}

VideoSection.propTypes = {
  videoData: PropTypes.object,
  index: PropTypes.any,
  alaskaData: PropTypes.object,
};

export default VideoSection;
