import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./../../components/common/css/PaytmWidget.scss";
// import { AnalyticsGA } from "../lib/analytics";
// import { checkIsAmpPage } from "../../utils/util";
import ImageCard from "./ImageCard/ImageCard";
import { _getStaticConfig } from "../../utils/util";
import AnchorLink from "./AnchorLink";

const siteConfig = _getStaticConfig();

export class SuperhitWidget extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    // dispatch(fetchSuperhitDataIfNeeded(dispatch));
  }

  superhitCard(item, index) {
    const { router } = this.props;
    let url = getURL(item.wu, index + 1, router);
    return (
      <li className="_paytm_card" key={"superhit" + index}>
        <AnchorLink href={url}>
          <div className="wgt-paytm tbl">
            <div className="items tbl_row">
              <div className="tbl_col prod_img">
                <ImageCard msid={item.imageid} size="rectanglethumb" />
              </div>
              <div className="des tbl_col">
                {item.hl && item.hl != "" ? (
                  <div className="heading">
                    <span className="text_ellipsis">{item.hl}</span>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </AnchorLink>
      </li>
    );
  }

  render() {
    const { dispatch, router, item } = this.props;

    //let { superhitdata } = this.props;
    // superhitwidget[0].secname
    let sectionname =
      item.recommended.superhitwidget && item.recommended.superhitwidget.newsItem
        ? item.recommended.superhitwidget.newsItem.secname
        : item.recommended.superhitwidget &&
          item.recommended.superhitwidget[0] &&
          item.recommended.superhitwidget[0].secname
        ? item.recommended.superhitwidget[0].secname
        : null;
    let superhitdata =
      item.recommended.superhitwidget && item.recommended.superhitwidget.newsItem
        ? item.recommended.superhitwidget.newsItem
        : item.recommended.superhitwidget && item.recommended.superhitwidget[0]
        ? item.recommended.superhitwidget[0]
        : null;

    return superhitdata != undefined && superhitdata.items != undefined && superhitdata.items instanceof Array ? (
      <div className="_paytmsponsored wdt_superhit">
        {sectionname ? (
          <h3>
            {sectionname} : {siteConfig.locale.superhit}
          </h3>
        ) : null}
        <ul className="scroll">
          {superhitdata.items instanceof Array
            ? superhitdata.items.map((item, index) => {
                return this.superhitCard(item, index);
              })
            : this.superhitCard(superhitdata.items, 0)}
        </ul>
      </div>
    ) : null;
  }
}

const getURL = (url, index, router) => {
  let utmSrc = "";
  if (router && router.location && router.location.pathname && router.location.pathname.indexOf("amp_") > -1) {
    utmSrc = "utm_source=amp_mostreadwidget&utm_medium=referral&utm_campaign=article" + index;
  } else {
    utmSrc = "utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article" + index;
  }

  // utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article1

  let queryStr = url && url.indexOf("?") > -1 ? url.split("?")[1] : "";
  let exctURL;
  if (queryStr && queryStr.length > 1) {
    exctURL = url + "?" + queryStr + "&" + utmSrc;
  } else {
    exctURL = url + "?" + utmSrc;
  }
  return exctURL;
};

function mapStateToProps(state) {
  return {
    ...state.superhitwidget,
  };
}

export default connect(mapStateToProps)(SuperhitWidget);
