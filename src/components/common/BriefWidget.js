import React from "react";
import { connect } from "react-redux";
import fetch from "../../utils/fetch/fetch";
import { getPageType, isMobilePlatform, _getStaticConfig } from "../../utils/util";
import Slider from "../desktop/Slider/Slider";
import "./css/BriefWidget.scss";
import FakeListing from "./FakeCards/FakeListing";
import SectionHeader from "./SectionHeader/SectionHeader";
const siteConfig = _getStaticConfig();

class BriefWidget extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      briefFeedData: "",
      sliderFeedData: [],
      isFetching: true,
    };
  }

  generateUrl(item) {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms?utm_source=briefs";
    return item.override ? item.override : url;
  }

  componentDidMount() {
    let apiUrl = `${process.env.API_BASEPOINT}/pwa_brief.cms?tag=pwabrief&feedtype=sjson&count=12`;

    fetch(apiUrl).then(response => {
      let briefData = response;
      let data = response && response.items;
      if (data && data.length > 0) {
        let filterSliderData = data.map((item, index) => {
          item.override = this.generateUrl(item);
          return item;
        });
        let isFetching = false;
        this.setState({ briefFeedData: briefData, sliderFeedData: filterSliderData, isFetching });
      }
    });
  }

  render() {
    const { briefFeedData, sliderFeedData } = this.state;
    const { pagetype, alaskaData } = this.props;
    const headerCopy = alaskaData ? JSON.parse(JSON.stringify(alaskaData)) : "";

    return sliderFeedData && sliderFeedData.length > 0 ? (
      <div className="row briefwidget">
        <React.Fragment>
          {pagetype == "articleshow" ? (
            <SectionHeader
              sectionhead={briefFeedData && briefFeedData.secname}
              datalabel={briefFeedData && briefFeedData.secname}
              weblink={briefFeedData && briefFeedData.override}
            />
          ) : (
            <SectionHeader sectionhead="Brief" sectionId={briefFeedData && briefFeedData.id} data={headerCopy} />
          )}

          <Slider
            type="briefnews"
            size={pagetype == "articleshow" ? "1" : "3"}
            width={pagetype == "articleshow" ? "290" : "325"}
            margin="0"
            sliderData={sliderFeedData}
            parentMsid={briefFeedData && briefFeedData.id}
          />
        </React.Fragment>
      </div>
    ) : null;
  }
}
function mapStateToProps(state) {
  return {
    isFetching: state.home.isFetching,
  };
}

export default connect(mapStateToProps)(BriefWidget);
