import Cookies from "react-cookie";
import { _getStaticConfig, isProdEnv } from "../../utils/util";
import { AnalyticsGA } from "../lib/analytics";
const siteConfig = _getStaticConfig();
const SSO = siteConfig.sso;
const SSOAPI = isProdEnv() ? siteConfig.sso : siteConfig.sso.stg;
export function openLoginPopup(callback, loginSource = "") {
  AnalyticsGA.event({
    category: "Login New",
    action: "Login_Screen",
    label: loginSource ? `Login_initiated_${loginSource}` : "Login_initiated",
    overrideEvent: "tp_login_new",
  });
  const ru = getRedirectURI(loginSource);
  const ssourl = `${SSOAPI.SSO_URL_LOGIN}?channel=${getChannel()}&ru=${ru}`;

  let pop = "";
  if (navigator.userAgent.match("CriOS")) {
    window.open(ssourl, "_blank");
  } else if (/iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent)) {
    window.open(ssourl, "_blank");
  } else {
    pop = window.open(ssourl, "signinwi", "toolbar=0,status=0,scrollbars=1,width=850,height=780");
    pop.moveTo(315, 250);
  }

  let timesRun = 0;
  var timer = setInterval(() => {
    timesRun += 1;
    const userInfo = Cookies.load("ssoid"); // changes lgc_ssoid to ssoid post development
    if (typeof callback === "function") {
      if (userInfo) {
        try {
          callback({ ssoid: userInfo });
          clearInterval(timer);
        } catch (e) {
          console.log("Error in Login", e);
        }
      } else if (timesRun === 50) {
        clearInterval(timer);
      }
    } else {
      clearInterval(timer);
    }
  }, 2000);
}

// if (timesRun === 50) {
//   clearInterval(timer);
// }

export function logOut(ssoid) {
  const ru = getRedirectURI();
  const ssourl = `${SSOAPI.SSO_URL_LOGOUT}?channel=${getChannel()}&ru=${ru}`;
  const Img = new Image(1, 1);
  Img.src = ssourl;

  Cookies.remove("ssoid"); // this only remove .indiatimes.com cookie
  if (document.domain.includes("samayam.com")) {
    document.cookie = "ssoid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.samayam.com; path=/;";
  } else if (document.domain === "vijaykarnataka.com") {
    document.cookie = "ssoid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.vijaykarnataka.com; path=/;";
  } else if (document.domain === "maharashtratimes.com") {
    document.cookie = "ssoid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.maharashtratimes.com; path=/;";
  } else if (document.domain === "iamgujarat.com") {
    document.cookie = "ssoid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.iamgujarat.com; path=/;";
  } else if (document.domain.includes(".gadgetsnow.com")) {
    document.cookie = "ssoid=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.gadgetsnow.com; path=/;";
  }
  // var pop = "";
  // if (navigator.userAgent.match("CriOS")) {
  //   window.open(ssourl, "_blank");
  // } else if (/iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent)) {
  //   var pop = window.open(ssourl, "_blank");
  // } else {
  //   pop = window.open(ssourl, "signinwi", "toolbar=0,status=0,scrollbars=1,width=850,height=780");
  //   pop.moveTo(315, 250);
  // }

  // var timesRun = 0;
  // var timer = setInterval(() => {
  //   timesRun += 1;
  //   //console.log('timesRun: '+timesRun)
  //   let userInfo = ssoid;
  //   if (typeof userInfo !== "undefined") {
  //     clearInterval(timer);
  //   }
  //   if (timesRun === 50) {
  //     clearInterval(timer);
  //   }
  // }, 1000);
}

function getChannel() {
  if (typeof document !== "undefined" && siteConfig.gadgetdomain.includes(document.domain)) {
    return SSO.GADGET_CHANNEL;
  }
  return SSO.SSO_CHANNEL;
}

function getRedirectURI(loginSource) {
  let ru = `${SSOAPI.BASE_URL}/login.cms`;
  if (typeof document !== "undefined" && siteConfig.gadgetdomain.includes(document.domain)) {
    ru = siteConfig.gadgetdomain + "/login.cms";
  }

  if (process.env.DEV_ENV === "stg1" || process.env.DEV_ENV === "stg2" || process.env.NODE_ENV === "development") {
    ru = `${document.location.origin}/login.cms`;
  }

  if (loginSource !== "") {
    ru = `${ru}?source=${loginSource}`;
  }

  return ru;
}

export function getSsoChannel() {
  return getChannel();
}
