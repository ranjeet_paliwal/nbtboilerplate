import React from "react";
import ReactDOM from "react-dom";
import CommentsPopup from "./CommentsPopup";
import { _isCSR } from "../../utils/util";
import "./css/Desktop.scss";

// import "../../components/common/css/desktop/DockPortal.scss";

const CommentsPortal = props => {
  if (_isCSR()) {
    return ReactDOM.createPortal(
      <CommentsPopup
        msid={props.msid}
        showCommentsPopup={props.showCommentsPopup}
        closeComments={props.closeComments}
        loggedIn={props.loggedIn}
        closeOnOverLay={props.closeOnOverLay}
        isNotEu={props.isNotEu}
      />,
      document.getElementById("comments-parent-container"),
    );
  }
  return "";
};

export default CommentsPortal;
