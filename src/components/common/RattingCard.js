import React from "react";

import styles from "./css/MovieReviewShowCard.scss";
import fetch from "../../utils/fetch/fetch";
import { _isCSR, _checkUserStatus } from "../../utils/util";
import { openLoginPopup } from "./ssoLogin";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

const locale = siteConfig.locale;

const RattingCard = props => {
  let { type, ur } = props;
  let ratePerc = ur != null ? (ur / 5) * 100 : null;

  return (
    <div className="con_review_rate" data-exclude="amp">
      <div className="tabs_circle">
        <ul className="tabs_circle_list">
          {!props.ratingOnly ? (
            <li onClick={excuteReview.bind(this, props.id)}>
              {locale.review_movie}
            </li>
          ) : null}

          <li
            id={"rating_" + props.id}
            className="btn-rated"
            onClick={e => {
              if (!window._user) {
                openLoginPopup(excuteRating.bind(this, props.id));
              } else {
                excuteRating(props.id);
              }
            }}
          >
            {type != undefined && type == "recipe" ? (
              <small>{locale.rate_recipe} | Avg. User Ratting</small>
            ) : (
              locale.rate_movie
            )}

            {/* To show ratting star on initial load for recipe */}
            {ratePerc != null ? (
              <span>
                <b style={{ width: ratePerc + "%" }}></b>
              </span>
            ) : null}

            <input className="hide" type="checkbox"></input>
          </li>
        </ul>
        {_isCSR() ? checkRating(props.id, type, ratePerc) : null}
      </div>
      <div className="rateBox" id={"ratingbox_" + props.id}>
        <div className="rate_con">
          <h3>
            {type != undefined && type == "recipe"
              ? locale.slide_to_rate
              : locale.slide_to_rate_movie}
          </h3>
          <div className="star-and-value">
            <span className="rating-stars">
              <span className="empty-stars"></span>
              <span
                className="filled-stars ratestar w50"
                id={"ratestar_" + props.id}
              ></span>
            </span>
            <span className="ratespan" id={"ratespan_" + props.id}>
              2.5
            </span>
          </div>
          <input
            className="rangeslider"
            type="range"
            min="0"
            max="5"
            step=".5"
            id={"rateslider_" + props.id}
          ></input>
          <label className="custom-checkbox">
            <input
              className="rangesubmit"
              type="checkbox"
              name="submit"
              value="submit"
              data-rate={ratePerc != null ? ratePerc : 0}
              data-attr={type == "recipe" ? "recipe" : "rate"}
              id={"ratesubmit_" + props.id}
            ></input>
            <span className="checkmark"></span>
          </label>
        </div>
      </div>
    </div>
  );
};

export default RattingCard;

const excuteReview = (id, e) => {
  let commentBox = document.getElementById("comment_" + id);
  if (commentBox) commentBox.click();
};

const excuteRating = id => {
  let rateButton = document.getElementById("rating_" + id);
  let hidden_checkbox = rateButton.querySelector(".hide");
  let rateBox = document.getElementById("ratingbox_" + id);
  let rateSlider = document.getElementById("rateslider_" + id);
  let rateStar = document.getElementById("ratestar_" + id);
  let rateSpan = document.getElementById("ratespan_" + id);
  let rateSubmit = document.getElementById("ratesubmit_" + id);
  let _rating = 0;
  let ratingurl =
    siteConfig.mweburl +
    "/rate_moviereview.cms?msid=" +
    id +
    "&getuserrating=1&criticrating=" +
    "&vote=";

  if (!rateBox || !rateSlider || !rateSubmit) return false;
  hidden_checkbox.checked = !hidden_checkbox.checked;
  hidden_checkbox.checked
    ? rateBox.classList.add("visible")
    : rateBox.classList.remove("visible");
  hidden_checkbox.checked
    ? rateButton.classList.add("active")
    : rateButton.classList.remove("active");

  rateSlider.oninput = e => {
    _rating = parseFloat(e.target.value);
    e.target.style.background = `linear-gradient(to right, #00b3ff 0%,#00b3ff ${_rating *
      2 *
      10}%,#fff ${_rating * 2 * 10}%, #fff 100%)`;
    rateStar.classList.value = "";
    rateStar.classList.add("w" + _rating * 2 * 10);
    rateStar.classList.add("filled-stars");
    rateStar.classList.add("ratestar");
    rateSpan.innerHTML = _rating;
  };

  rateSubmit.onchange = e => {
    if (e.target.checked && window._user && _rating > 0) {
      // checkbox submit, disable when rated and color change
      e.target.classList.add("rate-submit");
      e.target.setAttribute("disabled", true);
      e.target.style.pointerEvents = "none";
      rateSlider.style.pointerEvents = "none";
      ratingurl = ratingurl + _rating * 2;
      fetch(ratingurl, { type: "jsonp" })
        .then(
          () => {
            setTimeout(() => {
              document.getElementById("rating_" + id).innerHTML =
                rateSubmit.getAttribute("data-attr") == "recipe"
                  ? `<small>Submitted | Avg. User Ratting</small><span><b style="width:${rateSubmit.getAttribute(
                      "data-rate"
                    )}%"></b></span>`
                  : `<span><b>${_rating}</b>/5</span>`;
              rateBox.classList.remove("visible");
              rateButton.style.pointerEvents = "none";
              rateButton.classList.add("btn-rated");
            }, 500);
          },
          () => {
            rateBox.innerHTML = "ERROR";
          }
        )
        .catch(() => {
          rateBox.innerHTML = "ERROR";
        });

      mytActivityLogging("Rated", id, "", _rating * 2, "");
    }
  };
};

const checkRating = (id, pagetype, avgur) => {
  _checkUserStatus(user => {
    let ratingSpan;
    if (user.detail) {
      let alreadyRatedURL = `${siteConfig.sso["MYTIMES_ALREADY_RATED"]}userId=${user.detail._id}&baseEntityId=0&uniqueAppKey=${id}`;
      fetch(alreadyRatedURL).then(text => {
        if (text && text != "") {
          ratingSpan = document.getElementById("rating_" + id);
          let rateHtml =
            pagetype == "recipe"
              ? `<small>Allready Rated | Avg. User Ratting</small> <span><b style="width:${avgur}%"></b></span>`
              : `<span><b>${parseInt(text) / 2}</b>/5</span>`;
          ratingSpan.innerHTML = rateHtml;
          ratingSpan.style.pointerEvents = "none";
          ratingSpan.classList.add("btn-rated");
        }
        // else{
        //   ratingSpan.addEventListener('click',excuteRating.bind(this,id));
        // }
      });
      // .catch((err)=> ratingSpan.addEventListener('click', excuteRating.bind(this,id)))
    } else {
      // ratingSpan.addEventListener('click',openLoginPopup(excuteRating.bind(this,id)));
    }
  });
};

const mytActivityLogging = (
  activityType,
  _m_msid,
  _m_msg,
  rateval,
  exCommentTxt
) => {
  if (_m_msid != undefined && _m_msid != null) {
    let src = siteConfig.sso["MYTIMES_ADD_ACTIVITY"];
    src = src + "activityType=" + activityType;
    src = src + "&uniqueAppID=" + _m_msid;
    src = src + "&baseEntityType=MOVIEW_REVIEW";
    src = src + "&objectType=B";
    src = src + "&url=" + window.location.href;
    if (
      rateval != null &&
      rateval != undefined &&
      rateval != "" &&
      rateval != "0"
    ) {
      src = src + "&userrating=" + rateval;
    }
    if (
      exCommentTxt != null &&
      exCommentTxt != undefined &&
      exCommentTxt != ""
    ) {
      src = src + "&exCommentTxt=" + exCommentTxt;
    }
    if (_m_msg != null && _m_msg != undefined && _m_msg != "") {
      src = src + "&via=" + _m_msg;
    }
    var img = new Image(1, 1);
    img.src = src;
  }
};
