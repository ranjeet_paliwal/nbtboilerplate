import React, { Component } from "react";
import { Link } from "react-router";
import PropTypes from "prop-types";
import { capitalizeFirstLetter, calculateTime } from "./../../utils/util";
import styles from "./css/PhotoMazzaShowCard.scss";

class PhotoAdCard extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  render() {
    let { walmartad } = this.props;
    return (
      <li className={styles["adcard"] + " adcard"} key={1}>
        <a href="">
          <div>
            <div className={styles["brandctn"] + " brandctn"}>
              Ad ZEEAYURVEDA
              <img
                src="https://static.clmbtech.com/ctn/commons/images/colombia-icon-gray.png"
                alt=""
              />
            </div>
          </div>
          {!walmartad ? (
            <img
              src="https://navbharattimes.indiatimes.com/nabmht/2312/images/3/ce47d602ff51a20e5ae4eb2017b9abf0_1516358079192_0.jpg"
              width="100%"
            />
          ) : (
            <img
              src="https://www.weeklyadshere.com/wp-content/uploads/2017/03/0001.jpg"
              width="100%"
            />
          )}
          <div className={styles["brandctn"] + " brandctn"}>
            Melt excess "belly fat" with this one simple trick.
          </div>
        </a>
      </li>
    );
  }
}

export default PhotoAdCard;
