/* eslint-disable no-alert */
import React, { PureComponent } from "react";

import { _getStaticConfig, isMobilePlatform } from "../../../utils/util";
import GridCardMaker from "../../common/ListingCards/GridCardMaker";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import fetch from "../../../utils/fetch/fetch";

const siteConfig = _getStaticConfig();

// Gold top news widget which fetches data in CSR

class GoldTopNewsWidget extends PureComponent {
  state = {
    goldTopNewsData: null,
  };

  componentDidMount() {
    const goldTopNewsApi = `${siteConfig.gold.goldTopNewsApi}&source=${isMobilePlatform() ? "mobile" : "desktop"}`;

    fetch(goldTopNewsApi)
      .then(data => {
        this.setState({
          goldTopNewsData: data,
        });
      })
      .catch({});
  }

  //FIXME: Add code for mobile / desktop ui handling
  render() {
    const { goldTopNewsData } = this.state;
    return goldTopNewsData && goldTopNewsData.items && Array.isArray(goldTopNewsData.items) ? (
      <div className="goldsection row">
        <SectionHeader
          sectionhead={goldTopNewsData && goldTopNewsData.secname}
          datalabel={goldTopNewsData && goldTopNewsData.secname}
          weblink={goldTopNewsData && goldTopNewsData.override}
        />

        {/* <div className="row mr0"> */}
          <ul className="col12">
            {goldTopNewsData &&
              Array.isArray(goldTopNewsData.items) &&
              //   Show top 4 items
              goldTopNewsData.items.slice(0, 4).map((item, index) => {
                return (
                  <GridCardMaker
                    card={item}
                    key={item.id}
                    keyName={item.imageid}
                    cardType="horizontal"
                    className=""
                    imgsize={isMobilePlatform() ? "midthumb" : "smallthumb"}
                  />
                );
              })}
          </ul>
        {/* </div> */}
      </div>
    ) : null;
  }
}
export default GoldTopNewsWidget;
