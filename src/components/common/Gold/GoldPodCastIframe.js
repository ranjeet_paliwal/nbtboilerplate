import React from "react";
import { _getStaticConfig } from "../../../utils/util";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";

const siteConfig = _getStaticConfig();
//FIXME: Change Iframe ui according to mobile / Desktop
const GoldPodCastIframe = React.memo(props => (
  <ErrorBoundary>
    <iframe
      src={siteConfig.gold.homepagePodCastUrl}
      height="185"
      width="100%"
      border="0"
      marginWidth="0"
      marginHeight="0"
      hspace="0"
      vspace="0"
      frameBorder="0"
      scrolling="no"
      title="nbtgold"
      allowTransparency="true"
    />
  </ErrorBoundary>
));

export default GoldPodCastIframe;
