import React from "react";
// import VideoListCard from "../VideoListCard";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import AnchorLink from "../AnchorLink";
import ImageCard from "../ImageCard/ImageCard";
import {
  addVideoConfig,
  removeVideoConfig,
  showVideoPopup,
  removeSliderData,
} from "../../../actions/videoplayer/videoplayer";

import { createConfig } from "../../../modules/videoplayer/utils";
import globalconfig from "../../../globalconfig";
import { _getStaticConfig, throttle } from "../../../utils/util";

if (!globalconfig.position) {
  globalconfig.position = 1;
}

const siteConfig = _getStaticConfig();

const VideoItem = props => {
  const {
    item,
    width,
    imgsize,
    videoIntensive,
    clicked,
    index,
    type,
    src,
    alt,
    showIcon,
    listlink,
    parentMsid,
    isrelative,
    noSeo,
    istrending,
    isSyn,
    lead,
  } = props;
  let statusVideoIntensive;
  if (typeof videoIntensive === "undefined") {
    // by default open video in video show page
    statusVideoIntensive = true;
  } else {
    statusVideoIntensive = videoIntensive;
  }

  const openVideoInPopup = clickedItem => {
    // creating config for video here
    const subsecmsid = parentMsid || clickedItem.subsecmsid || "";
    const config = createConfig(clickedItem, subsecmsid, siteConfig, "videoshow");
    props.dispatch(removeVideoConfig());
    props.dispatch(addVideoConfig(config));
    openPopUp();
  };

  const openPopUp = () => {
    if (document) {
      document.body.style.overflow = "hidden";
    }
    props.dispatch(showVideoPopup());
    props.dispatch(removeSliderData());
  };

  const changeVideoConfig = clickedItem => {
    const itemParentMsid = itemParentMsid || clickedItem.subsecmsid || "";
    const config = createConfig(clickedItem, itemParentMsid, siteConfig, "videoshow");
    props.dispatch(addVideoConfig(config));
  };

  const videoItemClick = clickedItem => {
    if (props.sliderClick) {
      props.sliderClick(clickedItem);
      return;
    }
    if (statusVideoIntensive) {
      if (props.clicked) {
        clicked(clickedItem);
      } else {
        changeVideoConfig(clickedItem);
      }
    } else {
      openVideoInPopup(clickedItem);
    }
  };

  const throttledVideoItemClick = throttle(videoItemClick, 6000, this);

  const url = listlink && listlink.override && listlink.override !== "" ? listlink.override : item.wu || item.override;
  return (
    <React.Fragment>
      <span
        className="table_col img_wrap"
        data-tag={item.du || ""}
        role="button"
        tabIndex={0}
        onClick={() => throttledVideoItemClick(item)}
        onKeyUp={() => throttledVideoItemClick(item)}
      >
        <AnchorLink
          href={url}
          hrefData={item}
          itemProp={!(item && (noSeo || item.pl === "1" || item.noseo === "1")) ? "url" : ""}
          isrelative={isrelative}
          preventDefault={!statusVideoIntensive}
          istrending={istrending}
        >
          {!noSeo && !(item && (item.pl === "1" || item.noseo === "1")) ? (
            <meta itemProp="position" content={globalconfig.position++} />
          ) : null}
          <ImageCard
            type={type || null}
            width={width || undefined}
            alt={alt || ""}
            src={src || ""}
            noLazyLoad={lead}
            msid={item.imageid}
            title={item.seotitle ? item.seotitle : item.hl || ""}
            size={imgsize || "smallwidethumb"}
            schema={false}
            imgver={item.imgsize}
          />
        </AnchorLink>
        {showIcon && <span className="type_icon video_icon" />}
      </span>

      <span
        className="table_col con_wrap"
        role="button"
        tabIndex={0}
        onClick={() => throttledVideoItemClick(item)}
        onKeyUp={() => throttledVideoItemClick(item)}
      >
        {item.subsecname && item.subsecname !== "" && item.subsecseolocation && item.subsecmsid ? (
          // Below part is used to show section name for each listing item
          <AnchorLink
            key="section_name"
            className="section_name"
            href={`/${item.subsecseolocation}/videolist/${item.subsecmsid}.cms`}
            onClick={e => e.stopPropagation()}
          >
            {item.subsecname}
          </AnchorLink>
        ) : null}

        <AnchorLink
          href={url}
          hrefData={item}
          // Below item prop replicates the url schema for video listing
          // itemProp={!(item && (item.pl === "1" || item.noseo === "1")) ? "url" : ""}
          isrelative={isrelative}
          preventDefault={!statusVideoIntensive}
          istrending={istrending}
        >
          <span className="text_ellipsis">{item.hl}</span>
        </AnchorLink>
        {isSyn && item.Story ? (
          <span className="videodesc" dangerouslySetInnerHTML={{ __html: item.Story }}></span>
        ) : null}
      </span>
    </React.Fragment>
  );
};

VideoItem.displayName = "VideoItem";

VideoItem.propTypes = {
  dispatch: PropTypes.func,
  item: PropTypes.object,
  width: PropTypes.any,
  imgsize: PropTypes.string,
  videoIntensive: PropTypes.bool,
  clicked: PropTypes.func,
  index: PropTypes.any,
  type: PropTypes.any, // Need to check what is the type of type
  src: PropTypes.any,
  alt: PropTypes.any,
  showIcon: PropTypes.bool,
  listlink: PropTypes.object,
  parentMsid: PropTypes.string,
  isrelative: PropTypes.bool,
  noSeo: PropTypes.bool,
  sliderClick: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.videoplayer,
  };
}

export default connect(mapStateToProps)(VideoItem);
