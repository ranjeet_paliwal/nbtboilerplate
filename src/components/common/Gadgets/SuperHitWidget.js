import React from "react";
import { isMobilePlatform, _getStaticConfig } from "../../../utils/util";
import GridSectionMaker from "../ListingCards/GridSectionMaker";
const siteConfig = _getStaticConfig();
import AnchorLink from "../../../components/common/AnchorLink";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import Slider from "../../desktop/Slider";
import { defaultDesignConfigs } from "../../../containers/defaultDesignConfigs";

const SuperHitWidget = props => {
    const superHitWidgetData = props.data;
    superHitWidgetData.items && superHitWidgetData.items.map((item,index) => item.override = getURL(item.wu, index + 1, props.router,false));
    superHitWidgetData.wu=superHitWidgetData.wu ? superHitWidgetData.wu.indexOf("utm_source=") >-1  ? superHitWidgetData.wu : getURL(superHitWidgetData.wu, null,props.router,true): null;
    return (
        <div className="row wdt_superhit" data-exclude="amp">
            <ErrorBoundary>
                {!isMobilePlatform() ? (
                    <div className="superhit-wrapper">
                        <h3>
                            {superHitWidgetData && superHitWidgetData.wu ? (
                                <span>
                                    <AnchorLink href={superHitWidgetData.wu}>
                                        {`${siteConfig.locale.read_more}: ${superHitWidgetData.secname}`}
                                    </AnchorLink>
                                </span>
                            ) : (
                                    <span>{`${siteConfig.locale.read_more}: ${superHitWidgetData.secname}`}</span>
                                )}
                        </h3>
                        <Slider
                            type="grid"
                            size="3"
                            movesize="1"
                            sliderData={[].concat(superHitWidgetData.items)}
                            width={isMobilePlatform() ? "280" : "200"}
                            SliderClass="popular_videos"
                            islinkable
                            margin={isMobilePlatform() ? "15" : "10"}
                        />
                    </div>
                ) : (
                        <React.Fragment>
                            <div className="section">
                                <h3>
                                    {superHitWidgetData && superHitWidgetData.wu ? (
                                        <span>
                                            <AnchorLink href={superHitWidgetData.wu}>
                                                {superHitWidgetData.secname}
                                            </AnchorLink>
                                        </span>
                                    ) : (
                                            <span>
                                                {superHitWidgetData && superHitWidgetData.secname}
                                            </span>
                                        )}
                                </h3>
                            </div>
                            <GridSectionMaker
                                type={defaultDesignConfigs.topicslisting}
                                data={[].concat(superHitWidgetData.items)}
                            />
                        </React.Fragment>
                    )}
            </ErrorBoundary>
        </div>
    );
};

export const getURL = (url, index, router,isHeading) => {
    let utmSrc = "";
    if (router && router.location && router.location.pathname && router.location.pathname.indexOf("amp_") > -1) {
      utmSrc = `utm_source=amp_mostreadwidget&utm_medium=referral${isHeading ? "" : `&utm_campaign=article${index}`}`;
    } else {
      utmSrc = `utm_source=mostreadwidget&utm_medium=referral${isHeading ? "" : `&utm_campaign=article${index}`}`;
    }
    
    let queryStr = url && url.indexOf("?") > -1 ? url.split("?")[1] : "";
    let exctURL;
    if (queryStr && queryStr.length > 1) {
      exctURL = url + "?" + queryStr + "&" + utmSrc;
    } else {
      exctURL = url + "?" + utmSrc;
    }
    return exctURL;
  };

export default SuperHitWidget;