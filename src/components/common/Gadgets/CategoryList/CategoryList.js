import React from "react";
import { getKeyByValue, getSectionName, _getStaticConfig } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import SvgIcon from "../../../common/SvgIcon";
import AnchorLink from "../../AnchorLink";

const siteConfig = _getStaticConfig();

const getSeoName = (obj,pagetype) => {
  let seoName = getKeyByValue(gadgetsConfig.gadgetMapping, obj.keyword);
  if(pagetype == "compare") {
    return "/tech/compare-" + seoName;
  }
  return "/tech/" + seoName;
};

const categoryList = ({ clicked, activeGadget, pagetype, labelLang }) => {
  const Gadgets = siteConfig.locale.tech.category;
  const deviceHtml =
    Gadgets &&
    Gadgets.map(item => {
      return (
        <li key={item.keyword} className={`gdt-${item.keyword}${item.keyword == activeGadget ? " active" : ""}`}>
          {/* <b /> */}
          <AnchorLink id={item.keyword} onClick={clicked} href={getSeoName(item, pagetype)}>
            <SvgIcon name={`gn-${item.keyword}`} />
            <label> {labelLang == "eng" ? (item.keylabelEng || item.keyword) : item.keylabel } </label>
          </AnchorLink>
        </li>
      );
    });

  return (
    <div className="wdt-inline-gadgets" id="wdt-inline-gadgets">
      <ul className="scroll">{deviceHtml}</ul>
    </div>
  );
};

export default categoryList;
