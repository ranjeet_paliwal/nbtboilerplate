import React, { useState } from "react";
import { getKeyByValue, isMobilePlatform, _getStaticConfig } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import SvgIcon from "../../SvgIcon";
import AnchorLinkGN from "../../AnchorLinkGN";

const siteConfig = _getStaticConfig();

const getSeoName = (obj, pagetype) => {
  let seoName = getKeyByValue(gadgetsConfig.gadgetMapping, obj.keyword);
  if (pagetype == "compare") {
    return "/compare-" + seoName;
  }
  return "/" + seoName;
};

const slideGadget = (type, setSlider) => {
  const status = type == "next" ? true : false;
  setSlider(status);
};

const categoryList = ({ clicked, activeGadget, pagetype, labelLang, slider }) => {
  const Gadgets = siteConfig.locale.tech.category;
  const [isNextClicked, setSlider] = useState(false);
  const deviceHtml =
    Gadgets &&
    Gadgets.map(item => {
      return (
        <li key={item.keyword} className={`gdt-${item.keyword}${item.keyword == activeGadget ? " active" : ""}`}>
          <AnchorLinkGN id={item.keyword} onClick={clicked} href={getSeoName(item, pagetype)}>
            <SvgIcon name={`gn-${item.keyword}`} />
            <label> {labelLang == "eng" ? item.keylabelEng || item.keyword : item.keylabel} </label>
          </AnchorLinkGN>
        </li>
      );
    });

  let sliderClass = "";
  if (slider) {
    sliderClass += " enable-slider";
  }
  if (isNextClicked) {
    sliderClass += " next-slide";
  }

  return (
    <div className={`wdt-inline-gadgets${sliderClass}`} id="wdt-inline-gadgets">
      {slider && !isMobilePlatform() ? (
        <div className="slider">
          <span onClick={() => slideGadget("prev", setSlider)} className="btnPrev"></span>
          <div className="slider-hidden">
            <ul className="scroll">{deviceHtml}</ul>
          </div>
          <span onClick={() => slideGadget("next", setSlider)} className="btnNext"></span>
        </div>
      ) : (
        <ul className="scroll">{deviceHtml}</ul>
      )}
    </div>
  );
};

export default categoryList;
