import React from "react";
import Slider from "../../../desktop/Slider";
import SectionHeader from "../../SectionHeader/SectionHeader";

import { isMobilePlatform } from "../../../../utils/util";

const Photo = ({ data, designType }) => {
  const cssClass = isMobilePlatform() ? "wdt_next_slider" : "wdt_highlight";
  const width = isMobilePlatform() ? "150" : "221";
  const margin = isMobilePlatform() ? "0" : "20";

  return data && data.items ? (
    <div className="row">
      <div className="col12">
        <div className={cssClass}>
          <SectionHeader sectionhead={data.secname} weblink={data.override} morelink={data.override} />
          <Slider type="grid" size="4" width={width} margin={margin} sliderData={data.items} />
        </div>
      </div>
    </div>
  ) : (
    ""
  );
};

export default Photo;
