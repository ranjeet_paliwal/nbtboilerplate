import React from "react";
import SectionHeader from "../../SectionHeader/SectionHeader";
import SectionLayoutMemo from "../../../../containers/desktop/home/SectionLayout";
import GridListMaker from "../../ListingCards/GridListMaker";

const News = ({ data, cssClass, designType }) => {
  if (data && data.items) {
    if (designType == "ibeatGridHorizontal") {
      return (
        <div className={`row ${cssClass}`}>
          <SectionHeader sectionhead={data.secname} weblink={data.wu} morelink={data.wu} />
          <div className="col12">
            <SectionLayoutMemo datalabel="ibeatGridHorizontal" hideHeader={true} data={data} />
          </div>
        </div>
      );
    }

    return (
      <div className={`row ${cssClass}`}>
        <SectionHeader sectionhead={data.secname} weblink={data.wu} morelink={data.wu} />
        <GridListMaker
          list={{
            imgsize: "smallthumb",
            type: "horizontal",
            className: "col12",
          }}
          datalist={data.items.slice(0, 6)}
          noLazyLoad={false}
          className={"col12"}
        />
      </div>
    );
  }
  return "";
};

export default News;
