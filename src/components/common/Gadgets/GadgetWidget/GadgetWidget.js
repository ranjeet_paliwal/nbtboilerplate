import React, { Component } from "react";
import PropTypes from "prop-types";
import AnchorLink from "../../../../components/common/AnchorLink";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import { truncateStr, getBuyLink, _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import { getGadgetPrice } from "../../../../containers/utils/gadgets_util";

import ErrorBoundary from "../../../lib/errorboundery/ErrorBoundary";
// import CommonLoader from "../Loaders/CommonLoader";
import { fetchGadgetListDataIfNeeded } from "../../../../actions/gn/gadgetlist/gadgetlist";
// import { fetchFilterListDataIfNeeded } from "../../actions/gadgetfiltertool/gadgetfiltertool";
import AdCard from "../../../../components/common/AdCard";

import AffiliateGadgets from "../AffiliateGadgets/AffiliateGadgets";
import SvgIcon from "../../SvgIcon";

const siteConfig = _getStaticConfig();
class GadgetWidget extends Component {
  constructor(props) {
    super(props);
    // const sortbystate = (props.params && props.params.filters) || "sort=popular";
    const sortbystate =
      props.params && props.params.filters && props.params.filters.includes("sort=")
        ? props.params.filters
        : "sort=popular";
    this.state = {
      category: "",
      sortbystate,
    };
    // this.dataType = "latest";
  }

  componentWillMount() {
    const { router, pagetype, _category, params } = this.props;
    const category = _category || params.category;
    if (category) this.setState({ category });
    params ? (params.category = category) : null;
  }

  componentDidUpdate(prevProps, prevState) {
    const { params } = this.props;
    if (prevProps.params.category !== params.category) {
      this.setState({
        sortbystate: "sort=popular",
      });
    }
  }

  setCategory(obj) {
    // debugger
    const { dispatch, params, query, router, pagetype } = this.props;
    this.dataType = "popular";
    // document.getElementById('popular').className = "active";

    const category = obj.currentTarget.id;
    if (pagetype && pagetype == "gadgetlist") {
      this.props.router.push(`/tech/${category}`);
      this.restForm();
    }
    if (category) this.setState({ category });
    const dataType = this.dataType;

    const categoryoverride = category;
    if (pagetype && pagetype == "gadgetlist") {
      dispatch(fetchFilterListDataIfNeeded(params, query, router, categoryoverride));
    } else {
      return dispatch(fetchGadgetListDataIfNeeded(params, query, router, dataType, categoryoverride));
    }
  }

  restForm(obj) {
    const checkBox = document.getElementsByTagName("input");
    if (checkBox && checkBox.length > 0) {
      for (let b = 0; b < checkBox.length; b++) {
        if (checkBox[b].type == "radio") {
          checkBox[b].checked = false;
        }
        if (checkBox[b].type == "checkbox") {
          checkBox[b].checked = false;
        }
      }
    }
    if (document.getElementById("filterapplied")) document.getElementById("filterapplied").innerHTML = "";
  }

  amz_minilist(amz, index, prodUrl) {
    return (
      <li>
        <span className="buy-btn">
          <a target="_blank" rel="nofollow" href={prodUrl}>
            Buy on <b>Amazon</b>
          </a>
        </span>
      </li>
    );
  }

  render() {
    const _this = this;
    // let category=siteConfig.locale.tech.category;
    const {
      gadgetList,
      sectionLink,
      params,
      router,
      pagetype,
      isFrmTechHome,
      navigationfilter,
      ClickAddToCompare,
      setDataType,
      sortCriteria,
      isFetching,
      heading,
      _category,
    } = this.props;
    const sortbystateCls = _this.state.sortbystate;
    const { price, rating, active } = sortCriteria;
    let activeTab = active;
    if (params && params.filters && params.filters.includes("sort=latest")) {
      activeTab = "latest";
    }

    const gadgetCategory = router && router.params && router.params.category;

    return (
      <React.Fragment>
        <div className="gl_tabs" data-exclude="amp">
          {!isMobilePlatform() && <span>{siteConfig.locale.tech.sortby}</span>}
          {pagetype == "gadgetlist" ? (
            <div>
              <ul className="tabs">
                <li className={activeTab == "latest" ? "active" : false} onClick={setDataType.bind(this, "latest")}>
                  {siteConfig.locale.tech.latest}
                </li>
                <li className={activeTab == "popular" ? "active" : false} onClick={setDataType.bind(this, "popular")}>
                  {siteConfig.locale.tech.popular}
                </li>
                {!isMobilePlatform() && (
                  <React.Fragment>
                    <li
                      className={activeTab == "rating" ? "active" : false}
                      onClick={setDataType.bind(this, "rating")}
                      data-sort={`rating-${rating}`}
                    >
                      {siteConfig.locale.tech.rating}
                    </li>
                    <li
                      className={activeTab == "price" ? "active" : false}
                      onClick={setDataType.bind(this, "price")}
                      data-sort={`price-${price}`}
                    >
                      {siteConfig.locale.tech.price}
                    </li>
                  </React.Fragment>
                )}
              </ul>
            </div>
          ) : (
            ""
          )}
        </div>

        <div className="bg-gray">
          <div className="gadgets-in-list">
            <ErrorBoundary>
              {gadgetList && gadgetList instanceof Array ? (
                <ul>
                  {gadgetList.length > 0 ? (
                    gadgetList.map((item, index) => {
                      const gadgetName =
                        item &&
                        item.regional &&
                        item.regional instanceof Array &&
                        item.regional.filter(gname => {
                          return gname.host === siteConfig.hostid;
                        });

                      const gadgetPrice = isMobilePlatform() ? getGadgetPrice(item) : "";

                      let rumourClass = false;
                      if (item.rumoured == 1 || item.upcoming == 1) {
                        if (item.rumoured == 1 && item.upcoming == 1) {
                          rumourClass = "rumoured";
                        } else {
                          rumourClass = item.rumoured == 1 ? "rumoured" : "upcoming";
                        }
                      }

                      const imageId = item.imageMsid instanceof Array ? item.imageMsid[0] : item.imageMsid;

                      return (
                        <React.Fragment>
                          <li key={`gadgetList${index}`} className={rumourClass}>
                            {isMobilePlatform() && (
                              <React.Fragment>
                                {isMobilePlatform() && (
                                  <AddToCompare params={params} item={item} clicked={ClickAddToCompare} />
                                )}

                                <TopCaption item={item} />
                              </React.Fragment>
                            )}
                            <div className="top_spec">
                              <span className="gadget_img">
                                {!isMobilePlatform() && <TopCaption item={item} />}

                                <AnchorLink href={`/tech/${params.category}/${item.lcuname}`}>
                                  <ImageCard msid={imageId} size="gnthumb" />
                                </AnchorLink>
                              </span>
                              <span className="gadget_detail">
                                <span className="gd_name">
                                  <AnchorLink
                                    href={`/tech/${params.category}/${item.lcuname}`}
                                    className="text_ellipsis"
                                  >
                                    {item.devicename}
                                  </AnchorLink>
                                </span>
                                <span className="gd_rating">
                                  {item.criticRating && (
                                    <span className={item.criticRating ? "gd-input" : ""}>
                                      {siteConfig.locale.tech.criticsrating}:
                                      <b>
                                        <small>{(parseFloat(item.criticRating) / 2).toFixed(1)}</small>
                                        /5
                                      </b>
                                    </span>
                                  )}

                                  {item.averageRating && parseInt(item.averageRating) != 0 && (
                                    <span className="gd-input">
                                      <span>{siteConfig.locale.tech.userrating}:</span>
                                      <b>
                                        <small>{(parseFloat(item.averageRating) / 2).toFixed(1)}</small>
                                        /5
                                      </b>
                                    </span>
                                  )}

                                  <span className="gd-input">
                                    <AnchorLink
                                      href={`/tech/${params.category}/${item.lcuname}#rate`}
                                      className="gd-submit"
                                    >
                                      {siteConfig.locale.tech.submitrating}
                                    </AnchorLink>
                                  </span>
                                </span>
                                {isMobilePlatform() && gadgetPrice && <span class="gd_price">{gadgetPrice}</span>}

                                {item.launch_date ? (
                                  <span className="gd_launch">
                                    {siteConfig.locale.tech.launchdate}: {item.launch_date}
                                  </span>
                                ) : null}

                                {!isFrmTechHome && !isMobilePlatform() && <GadgetSpecs item={item} />}
                              </span>
                              {!isFrmTechHome && isMobilePlatform() && <GadgetSpecs item={item} />}
                              {isMobilePlatform() && <ReadMore category={gadgetCategory} item={item} />}
                              {isMobilePlatform() && <span className="gd_head">{siteConfig.locale.tech.seemore}</span>}
                              <AffiliateGadgets affiliateData={item && item.affiliate} />
                            </div>

                            <div className="bottom_spec">
                              {!isMobilePlatform() && <AddToCompare params={params} item={item} />}
                              {!isFrmTechHome && !isMobilePlatform() && (
                                <ReadMore category={gadgetCategory} item={item} />
                              )}
                            </div>
                          </li>

                          {isMobilePlatform() && (index == 0 || index % 5 == 0) ? (
                            <AdCard key={`listad_${index}`} mstype="ctnTechGL" adtype="ctn" />
                          ) : null}
                        </React.Fragment>
                      );
                    })
                  ) : (
                    <div className="no-data-list">
                      <ul>
                        <li>No records found</li>
                      </ul>
                    </div>
                  )}
                  {isFrmTechHome ? (
                    <div>
                      <AnchorLink className="more-btn" href={`/tech/${_this.state.category}`}>
                        {siteConfig.locale.tech.gadgets} {siteConfig.locale.read_more_listing}
                      </AnchorLink>
                    </div>
                  ) : null}
                </ul>
              ) : isFetching == "loaded" ? (
                <div className="no-data-list">
                  <ul>
                    <li>No records found</li>
                  </ul>
                </div>
              ) : (
                <div className="wdt_loading">
                  <span className="spinner_circle" />
                </div>
              )
              // <FakeListing showImages={true} />
              }
            </ErrorBoundary>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const TopCaption = ({ item }) => {
  return (
    <span
      className="top-caption"
      data-attr={
        item.rumoured == 1 ? siteConfig.locale.tech.rumoured : item.upcoming == 1 ? siteConfig.locale.tech.upcoming : ""
      }
    />
  );
};

const GadgetSpecs = ({ item }) => {
  return (
    <div className="gadget_specs">
      <ul>
        {Object.keys(item.keyFeatures).map((key, index) => {
          // dont show feature attribute "price_in_india" for mobile
          // as Price is already displaying at top
          if (key == "price_in_india" && isMobilePlatform()) {
            return "";
          }

          let keyVal = "";
          if (key == "price_in_india") {
            keyVal = parseInt(item.keyFeatures[key].value);
            keyVal = "₹ " + keyVal.toLocaleString("en-IN");
          } else {
            keyVal = item.keyFeatures[key].value;
          }
          return (
            <li key={`keyfFeatures${index}`}>
              <label>{item.keyFeatures[key].key && item.keyFeatures[key].key.replace(/\_/g, " ")} : </label>
              <b>{keyVal}</b>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

const AddToCompare = ({ params, item, clicked }) => {
  if (isMobilePlatform()) {
    return (
      <React.Fragment>
        <span className="add-to-compare" itemname={item.name} seo={item.uname} onClick={clicked} data-exclude="amp">
          <SvgIcon name="gn-compare-icon" />
        </span>
      </React.Fragment>
    );
  }
  return (
    <span className="add_to_compare">
      <AnchorLink
        href={`/tech/compare-${params.category}?device=${item.name && encodeURIComponent(item.name)}&name=${
          item.lcuname
        }`}
        shouldUppercase="true"
      >
        <b className="plus_icon" />
        {siteConfig.locale.tech.addtocompare}
      </AnchorLink>
    </span>
  );
};

const ReadMore = ({ category, item }) => {
  return (
    <span className="rd_more">
      <AnchorLink href={`/tech/${category}/${item.lcuname}?#specifications`}>
        {siteConfig.locale.tech.fullspecification}
      </AnchorLink>
    </span>
  );
};

GadgetWidget.propTypes = {
  item: PropTypes.object,
};

export default GadgetWidget;
