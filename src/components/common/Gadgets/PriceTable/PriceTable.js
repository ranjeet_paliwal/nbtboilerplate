import React from "react";
import PropTypes, { array } from "prop-types";
import { _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";

const siteConfig = _getStaticConfig();

const PriceTable = ({ data, params }) => {
  let gadgets = [];
  if (data && data.gadgets && Array.isArray(data.gadgets)) {
    gadgets = data.gadgets.slice(0, 10);
  }
  const category = params && params.category;
  const launchDate = gadgets && gadgets.filter(item => item.launch_date);
  let latestPopularRegional = siteConfig.locale.tech.popular;
  let latestPopular = "Popular";
  if (params && params.filters && params.filters.includes("sort=latest")) {
    latestPopularRegional = siteConfig.locale.tech.latest;
    latestPopular = "Latest";
  }

  return gadgets && gadgets.length > 0 ? (
    <div className={!isMobilePlatform() ? "row gl_latestGadgets" : "row gl_latestGadgets pwa"}>
      <div className="section">
        <div className="top_section">
          <h2 className="sectionHead">
            <span>{`${latestPopularRegional.toUpperCase()} ${data.brandheader.toUpperCase()} ${
              siteConfig.locale.tech.inIndia
            }  - ${latestPopular.toUpperCase()} ${data.brandheadereng ? data.brandheadereng.toUpperCase() : ""}  ${
              gadgetsConfig.gadgetsDisplay[category]
            } IN INDIA`}</span>
          </h2>
        </div>
      </div>
      <div className="col12 gn_table_layout">
        <table>
          <tbody>
            <tr>
              <th>
                {data.brandheader} {siteConfig.locale.tech.priceList} <br />
                {data.brandheadereng || ""} {gadgetsConfig.gadgetsDisplay[category]} Price List
              </th>
              {launchDate && launchDate.length > 0 && (
                <th>
                  {siteConfig.locale.tech.indiaLaunchDates} <br /> Launch Date in India
                </th>
              )}
              <th>
                {siteConfig.locale.tech.indiaPrice} <br /> Price in India
              </th>
            </tr>
            {gadgets &&
              gadgets.map(gData => (
                <tr key={gData.uname}>
                  <td>
                    <span className="gadName" title={gData.name}>
                      {gData.name}
                    </span>
                  </td>
                  {launchDate && launchDate.length > 0 && <td>{gData.launch_date || "NA"}</td>}
                  <td>{gData.price ? `₹ ${gData.price}` : "NA"}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  ) : (
    ""
  );
};

PriceTable.propTypes = {
  data: PropTypes.object,
};

export default PriceTable;
