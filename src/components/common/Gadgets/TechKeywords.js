import React from "react";
import AnchorLink from "../../common/AnchorLink";
import { Link } from "react-router";
import { isMobilePlatform } from "../../../utils/util";

const TechKeywords = React.memo(({ data, params, clicked }) => {
  return (
    <ul className={!isMobilePlatform() ? "gl_suggested_keywords" : "gl_suggested_keywords scroll"}>
      {data && Array.isArray(data) && data.length > 0
        ? data.map(item => {
            let className = "";
            const link = item.link;
            if (params && params.filters) {
              const filterlist = params.filters.split("&");
              for (let i = 0; i < filterlist.length; i++) {
                let filtervalue = filterlist[i].substring(filterlist[i].lastIndexOf("=") + 1).toLowerCase();
                if (link.toLowerCase().indexOf(`=${filtervalue}`) > -1) {
                  className = "active";
                  break;
                }
              }
            }

            return (
              <li className={className}>
                <AnchorLink href={item.link} shouldUppercase="true" onClick={clicked}>
                  {item.title}
                </AnchorLink>
              </li>
            );
          })
        : null}
    </ul>
  );
});

export default TechKeywords;
