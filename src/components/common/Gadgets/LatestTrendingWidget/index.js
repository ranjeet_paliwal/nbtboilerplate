import React, { Component } from "react";
import PropTypes from "prop-types";
import { _getStaticConfig } from "../../../../utils/util";
import AnchorLink from "../../AnchorLink";

const Config = _getStaticConfig();

class LatestTrendingWidget extends Component {
  constructor(props) {
    super(props);
    const data = {
      latest: props.data && props.data[0] && props.data[0].items ? [].concat(props.data[0].items) : [],
      trending: props.data && props.data[1] && props.data[1].items ? [].concat(props.data[1].items) : [],
    };
    this.state = {
      data,
      currentTab: "latest",
      loading: false,
    };
  }

  changeTab = newTab => {
    const { currentTab } = this.state;
    if (currentTab !== newTab) {
      this.setState({
        currentTab: newTab,
      });
    }
  };

  render() {
    const { data, loading, currentTab } = this.state;
    const { title } = this.props;
    let LoaderImg;
    // if (loading) {
    //   LoaderImg = <CommonLoader />;
    // }
    const widgetData = data[currentTab];
    return (
      <React.Fragment>
        <div className="box-item wdt_lst-tred">
          <div className="section">
            <div className="top_section">
              <h2>
                <span>{title}</span>
              </h2>
            </div>
          </div>
          <ul className="gn-tabs">
            <li onClick={() => this.changeTab("latest")} className={this.state.currentTab === "latest" ? "active" : ""}>
              {Config.locale.tech.latest}
            </li>
            <li
              onClick={() => this.changeTab("trending")}
              className={this.state.currentTab === "trending" ? "active" : ""}
            >
              {Config.locale.trending}
            </li>
          </ul>
          <div className="list-txt">
            {LoaderImg}
            {widgetData ? (
              <ul>
                {widgetData.map(tData => (
                  <li key={tData.id}>
                    <AnchorLink href={tData.wu}>{tData.hl}</AnchorLink>
                  </li>
                ))}
              </ul>
            ) : (
              ""
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

LatestTrendingWidget.propTypes = {
  value: PropTypes.array,
  navData: PropTypes.array,
};

export default LatestTrendingWidget;
