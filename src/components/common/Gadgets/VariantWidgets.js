import React from "react";
import PropTypes from "prop-types";

import AnchorLink from "../AnchorLink";
import { _getStaticConfig } from "../../../utils/util";
import SectionHeader from "../SectionHeader/SectionHeader";

const Config = _getStaticConfig();

const GadgetVariants = ({ data, currentlyActive, gadgetCategory }) => {
  return (
    <div className="wdt_varients">
      <SectionHeader sectionhead={Config.locale.varients} />
      <ul>
        <li className="active">
          <span>{currentlyActive}</span>
        </li>
        {data &&
          data.variants.map(value => {
            return (
              <li>
                <AnchorLink href={`${Config.mweburl}/tech/${gadgetCategory}/${value}`}>
                  {value && value.replace(/-/g, " ")}
                </AnchorLink>
              </li>
            );
          })}
      </ul>
    </div>
  );
};

GadgetVariants.propTypes = {
  data: PropTypes.object,
  currentlyActive: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

export default GadgetVariants;
