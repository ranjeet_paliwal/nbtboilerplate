import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Slider from "../../../desktop/Slider/Gadget";

class Wdt2GudAffiliatesList extends PureComponent {
    state = {
        data: {},
    };

    componentDidMount() {
        const { brandName, price, productName } = this.props;

        // console.log("brandName :"+brandName);
        // console.log("price :"+price);
        // console.log("productName :"+productName);
        const APIURL = `https://shop.gadgetsnow.com/find2GudAffiliateProducts/?brandName=${brandName}&price=${price}&productName=${productName}&product_per_page=30&v=2`;
        // console.log(`APIURL 2Gud:${APIURL}`);

        fetch(APIURL)
            .then(response => response.json())
            .then(data => {
                // console.log("======fetch data=====",data);
                if (data !== null) {
                    this.setState({
                        data,
                    });
                }
            });
    }

    render() {
        const { data } = this.state;
        const { twogudData, msid, tag } = this.props;

        return (
            <div className="wdt_amz_horizontal twogud">
                <h2>
                    नया जैसा मोबाइल पुराने के दाम पर
          <b className="twogud_icon" />
                </h2>
                {Array.isArray(data) ? (
                    <Slider
                        type="2gudwidgetslider"
                        size="1"
                        sliderData={data}
                        category=""
                        width=""
                        height=""
                        SliderClass="brandslider"
                        islinkable="false"
                        sliderWidth="600"
                        tag={tag}
                    />
                ) : null}
            </div>
        );
    }
}

Wdt2GudAffiliatesList.propTypes = {
    twogudData: PropTypes.array,
    brandName: PropTypes.string,
    price: PropTypes.string,
    productName: PropTypes.string,
    tag: PropTypes.string,
    msid: PropTypes.string,
};

export default Wdt2GudAffiliatesList;
