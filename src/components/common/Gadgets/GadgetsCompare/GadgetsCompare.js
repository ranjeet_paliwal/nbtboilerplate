import React from "react";
import { getKeyByValue, _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import SectionHeader from "../../SectionHeader/SectionHeader";
import gadgetConfig from "../../../../utils/gadgetsConfig";
import AnchorLink from "../../AnchorLink";
import ImageCard from "../../ImageCard/ImageCard";
import SvgIcon from "../../SvgIcon";

const siteConfig = _getStaticConfig();
const locale = siteConfig && siteConfig.locale;

const GadgetsCompare = React.memo(({ data, linksData, type, category, seoName, cssClass, deviceIcon, regionalDeviceName }) => {
  const prepareDeviceURL = `${process.env.WEBSITE_URL}tech/compare-${seoName}`;

  return (
    <div className={cssClass || false}>
      {data && (
        <React.Fragment>
          <SectionHeader
            sectionhead={`${siteConfig.locale.tech[type]} ${regionalDeviceName || locale.tech.gadgetSliderConfig[category]}
            ${process.env.SITE !== "mly" ? siteConfig.locale.tech.ki : ""} ${siteConfig.locale.tech.comparetxt}`}
            weblink={`${prepareDeviceURL}/${type}-comparisons`}
            morelink={true}
          />

          <div className="slider">
            <div className="slider_content">
              <ul>
                {data
                  ? data.map(item => {
                    let toolTip = "";
                    const seoPath =
                      item && item._id && item._id.indexOf("-vs-") !== -1
                        ? item._id
                          .split("-vs-")
                          .sort()
                          .join("-vs-")
                        : "";

                    if (item && item.product_name) {
                      toolTip = `${siteConfig.locale.tech.comparedotxt} ${item.product_name.join(" vs ")}`;
                    }

                    const deviceCategory =
                      gadgetConfig && gadgetConfig.gadgetMapping && item.category
                        ? getKeyByValue(gadgetConfig.gadgetMapping, item.category)
                        : "";
                    return (
                      <li key={item._id}>
                        <AnchorLink href={`/tech/compare-${deviceCategory}/${seoPath}`} title={toolTip}>
                          {item.product_name.map((device, key) => {
                            const deviceKey = `${item._id}-${item.product_name_seo[key]["#text"]}`;
                            return (
                              <React.Fragment key={deviceKey}>
                                <div className="slide">
                                  <span className="prod_img">
                                    <ImageCard size="gnthumb" msid={item.imageMsid[key]} />
                                  </span>
                                  <span className="title text_ellipsis">{device}</span>
                                  <span className="vs">VS</span>
                                </div>
                              </React.Fragment>
                            );
                          })}
                        </AnchorLink>
                      </li>
                    );
                  })
                  : ""}
              </ul>
            </div>
          </div>
        </React.Fragment>
      )}

      {linksData && (
        <div className="items-in-list">
          <ul>
            <GetHtml data={linksData} deviceIcon={deviceIcon} />
          </ul>
        </div>
      )}
    </div>
  );
});

const GetHtml = ({ data, deviceIcon }) => {
  return data && Array.isArray(data)
    ? data.map(item => {
      const seoPath =
        item && item._id && item._id.indexOf("-vs-") !== -1
          ? item._id
            .split("-vs-")
            .sort()
            .join("-vs-")
          : "";
      const deviceCategory =
        gadgetConfig && gadgetConfig.gadgetMapping && item.category
          ? getKeyByValue(gadgetConfig.gadgetMapping, item.category)
          : "";
      const urlCmpDetail = `/tech/compare-${deviceCategory}/${seoPath}`;
      return (
        <li key={item.product_name}>
          <AnchorLink className="text_ellipsis" href={urlCmpDetail}>
            {`${siteConfig.locale.tech.comparedotxt} ${item.product_name.join(" vs ")}`}
            {deviceIcon && isMobilePlatform() && <SvgIcon name="gn-compare-icon" />}
          </AnchorLink>
        </li>
      );
    })
    : "";
};

export default GadgetsCompare;
