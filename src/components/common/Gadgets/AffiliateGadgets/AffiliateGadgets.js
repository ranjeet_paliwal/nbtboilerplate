import React from "react";
import { _getStaticConfig, getAffiliateTags, getBuyLink, truncateStr, isMobilePlatform } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
const siteConfig = _getStaticConfig();

const AffiliateGadgets = React.memo(({ affiliateData }) => {
  let affiliateGadgets = [];
  if (affiliateData && affiliateData.exact) {
    // affiliateGadgets = affiliateData.exact;
    affiliateGadgets = [...affiliateGadgets, ...affiliateData.exact];
  }
  if (affiliateData && affiliateData.related) {
    // affiliateGadgets = affiliateData.related;
    affiliateGadgets = [...affiliateGadgets, ...affiliateData.related];
  }

  const uniqueID = Math.random()
    .toString(36)
    .substr(2, 9);
  let affData = "";
  if (affiliateGadgets && Array.isArray(affiliateGadgets)) {
    affData = affiliateGadgets.filter(item => {
      return item.Identifier === "amazon";
    });
  }

  if (affData && affData.length > 0) {
    return (
      <span className="gadget_buy related">
        {!isMobilePlatform() && (
          <React.Fragment>
            <b>{siteConfig.locale.tech.buyhere}</b>
            <input type="checkbox" id={`chk-nxt${uniqueID}`} className="chk-nxt" />
          </React.Fragment>
        )}
        {affData.length > 1 && !isMobilePlatform() ? <label className="nxt" htmlFor={`chk-nxt${uniqueID}`} /> : ""}
        <ul className="dotted-box">
          {affData.map((amz, index) => {
            const amztitle = (amz && amz.name.replace(/\s+/g, "-")) || "";
            const amzga = `${amztitle}_${amz.sort_price}`;
            const affiliateTag = getAffiliateTags(gadgetsConfig.affiliateTags.GL);
            const price = amz.sort_price;
            const title = amztitle;
            const url = amz.url;
            const buyURL = getBuyLink({
              url,
              price,
              title,
              amzga,
              tag: affiliateTag,
            });
            return (
              <li>
                <span className="txt text_ellipsis">{truncateStr(amz.name, 60)}</span>
                {
                  <span className="gd_price">
                    ₹ {amz.sort_price || ""}
                    {isMobilePlatform() && amz.ListPrice && amz.ListPrice.Amount && <b> ₹ {amz.ListPrice.Amount} </b>}
                  </span>
                }
                <span className="btm">
                  <span className={amz.Identifier}>
                    <img src="https://static.langimg.com/photo/58606011.cms" width="65" />
                  </span>
                  <span className="btn-buy">
                    <a target="_blank" rel="nofollow" href={buyURL}>
                      {siteConfig.locale.tech.purchase}
                    </a>
                  </span>
                </span>
              </li>
            );
          })}
        </ul>
      </span>
    );
  }
  return "";
});

export default AffiliateGadgets;
