import React, { PureComponent } from "react";
import { getDeviceObject } from "../../../../containers/utils/gadgets_util";
import { _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import CategoryList from "../CategoryList/CategoryList";
import CategoryListGadgets from "../CategoryList/CategoryListGadgets";

import SearchModule from "../SearchModule/SearchModule";

const siteConfig = _getStaticConfig();
const deviceText = (siteConfig && siteConfig.locale && siteConfig.locale.tech.gadgetSliderConfig) || {};

class GadgetsCompareModule extends PureComponent {
  state = {
    activeGadget: "mobile",
    suggestedDevices: "",
    updatedDeviceInfo: "",
  };

  switchDeviceHandler = obj => {
    obj.preventDefault();
    const deviceType = obj.currentTarget.id;
    this.setState({
      activeGadget: deviceType,
      suggestedDevices: "",
    });
  };

  suggestedDevicesCallBack = (item, deviceListInfo) => {
    let { suggestedDevices, updatedDeviceInfo } = this.state;
    suggestedDevices = item;
    updatedDeviceInfo = deviceListInfo;
    this.setState({
      suggestedDevices,
      updatedDeviceInfo,
    });
  };

  render() {
    const { activeGadget } = this.state;
    const { heading, source } = this.props;
    const inputBoxToDisplay = isMobilePlatform() ? 3 : 4;
    const deviceInfoParam = getDeviceObject(inputBoxToDisplay);
    const regionalDeviceName = deviceText[activeGadget];
    const HeadingTag = heading || "h1";
    return (
      <div className="wdt_compare-gadgets">
        {source == "gadgetsnow" ? (
          <CategoryListGadgets
            clicked={this.switchDeviceHandler.bind(this)}
            activeGadget={activeGadget}
            pagetype="compare"
            source={source}
            slider
          />
        ) : (
          <CategoryList
            clicked={this.switchDeviceHandler.bind(this)}
            activeGadget={activeGadget}
            pagetype="compare"
            slider
          />
        )}

        <HeadingTag>
          {`${regionalDeviceName} ${siteConfig.locale.tech.compare}`}
          <span className="search-suggest"> ({siteConfig.locale.tech.englishtype})</span>
        </HeadingTag>

        <div className="row-fields">
          <SearchModule
            activeGadget={activeGadget}
            deviceInfo={deviceInfoParam}
            suggestedDevicesCallBack={this.suggestedDevicesCallBack}
            source={source}
          />
        </div>
      </div>
    );
  }
}

export default GadgetsCompareModule;
