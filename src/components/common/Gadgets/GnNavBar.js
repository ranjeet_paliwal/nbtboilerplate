export const navConfig = {
  nbt: [
    {
      label: "न्यूज़",
      weblink: "/news/articlelist/83349198.cms",
    },
    {
      label: "टिप्स-ट्रिक्स",
      weblink: "/tips-tricks/articlelist/83349262.cms",
    },
    {
      label: "फोटो",
      weblink: "/photolist/83709175.cms",
    },
    {
      label: "विडियो",
      weblink: "/videos/videolist/83709144.cms",
    },
  ],
  mt: [
    {
      label: "Mobile",
      weblink: "/news/articlelist/83515703.cms",
    },
    {
      label: "Tips and Tricks",
      weblink: "/tips-tricks/articlelist/83516705.cms",
    },
    {
      label: "Photo",
      weblink: "/photolist/83712466.cms",
    },
    {
      label: "Fact Check",
      weblink: "/videos/videolist/83515737.cms",
    },
    {
      label: "Computer",
      weblink: "/videos/videolist/83515753.cms",
    },
    {
      label: "Science Technology",
      weblink: "/videos/videolist/83516747.cms",
    },
  ],
  eisamay: [
    {
      label: "Tech News",
      weblink: "/news/articlelist/83549023.cms",
    },
    {
      label: "How to",
      weblink: "/tips-tricks/articlelist/83549137.cms",
    },
    {
      label: "Videos",
      weblink: "/videos/videolist/83708377.cms",
    },
    {
      label: "Games",
      weblink: "/videos/videolist/83549185.cms",
    },
  ],
  vk: [
    {
      label: "Tech News",
      weblink: "/news/articlelist/83548490.cms",
    },
    {
      label: "Tips and Tricks",
      weblink: "/tips-tricks/articlelist/83548551.cms",
    },
    {
      label: "Photos",
      weblink: "/photolist/83710519.cms",
    },
    {
      label: "Videos",
      weblink: "/videos/videolist/83710543.cms",
    },
  ],
  tml: [
    {
      label: "டெக் நியூஸ்",
      weblink: "/news/articlelist/83350004.cms",
    },
    {
      label: "டிப்ஸ் & ட்ரிக்ஸ்",
      weblink: "/tips-tricks/articlelist/83350068.cms",
    },
    {
      label: "போட்டோஸ்",
      weblink: "/photolist/83709377.cms",
    },
    {
      label: "வீடியோ",
      weblink: "/videos/videolist/83709395.cms",
    },
  ],
  tlg: [
    {
      label: "Tech News",
      weblink: "/news/articlelist/83551613.cms",
    },
    {
      label: "Mobile",
      weblink: "/news/articlelist/83551617.cms",
    },
    {
      label: "Tips and Tricks",
      weblink: "/tips-tricks/articlelist/83551654.cms",
    },
    {
      label: "Photos",
      weblink: "/photolist/83710036.cms",
    },
    {
      label: "Videos",
      weblink: "/videos/videolist/83709944.cms",
    },
  ],
  mly: [
    {
      label: "Tech News",
      weblink: "/news/articlelist/83552387.cms",
    },
    {
      label: "Tips and Tricks",
      weblink: "/tips-tricks/articlelist/83552441.cms",
    },
    {
      label: "Videos ",
      weblink: "/videos/videolist/83709098.cms",
    },
  ],
};
