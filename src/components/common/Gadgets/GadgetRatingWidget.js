/* eslint-disable indent */
import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import GadgetPostreview from "./GadgetPostreview";

import { isMobilePlatform, _getStaticConfig, _set_cookie } from "../../../utils/util";

const Config = _getStaticConfig();

function convertDate(d) {
  if (!d) {
    return "";
  }
  const parts = d.split(" ");
  // Fri Oct 23 04:09:36 IST 2020
  const months = {
    Jan: "01",
    Feb: "02",
    Mar: "03",
    Apr: "04",
    May: "05",
    Jun: "06",
    Jul: "07",
    Aug: "08",
    Sep: "09",
    Oct: "10",
    Nov: "11",
    Dec: "12",
  };
  return `${parts[5]}-${months[parts[1]]}-${parts[2]}`;
}

function handleComments(msid, router) {
  console.log("msid in handlecomments", msid);
  if (typeof window !== undefined && typeof document !== undefined) {
    _set_cookie("comment_id", parseInt(msid), 1, "/", Config.mwebdomain);
  }
  const url = `${Config.mweburl}/off-url-forward/pwa_comments.cms?frmapp=yes&pagetype=articleshow`;
  window.open(url);
}

const GadgetsRating = ({
  data,
  getreview,
  closereview,
  showReviewCard,
  isReviewDisabled,
  showLoginRegister,
  category,
  loggedIn,
}) => {
  let max = 0;
  const defaultVal = [5, 4, 3, 2, 1];
  const rateArr = {};

  if (!data) {
    return null;
  }
  const {
    isProductAvailable,
    productCategory,
    productName,
    brandName,
    imageLink,
    itemDescription,
    itemPrice,
    sortPrice,
    pid,
    rating,
    ur,
    updatedDate,
    announced,
    canonical,
    router,
  } = data;

  // eslint-disable-next-line no-unused-expressions
  if (data && Array.isArray(data.rating)) {
    data.rating.map(item => {
      // debugger;
      const gadRate = Math.round(parseInt(item.rating) / 2);
      rateArr[gadRate] = item.count;
      max = parseInt(item.count) > max ? parseInt(item.count) : max;
    });
  } else if (typeof data.rating !== "undefined") {
    const gadRate = Math.round(parseInt(data.rating.rating) / 2);
    rateArr[gadRate] = data.rating.count;
    max = parseInt(data.rating.count);
  }

  const plainDescription = itemDescription && itemDescription.replace(/(<([^>]+)>)/gi, "");
  let ratingCount = 0;

  if (rating && Array.isArray(rating)) {
    rating.forEach(item => {
      ratingCount += parseInt(item.count);
    });
  } else if (rating && rating.count) {
    ratingCount = parseInt(rating.count);
  }

  const priceValidUntil = convertDate(updatedDate);
  const userRating = ur && ur != Config.locale.tech.befirsttoreview ? ur : "";

  return defaultVal ? (
    <div className="wdt_review_rate" id="rate">
      {(sortPrice && sortPrice != "0") || userRating ? (
        <div itemType="http://schema.org/Product" itemScope>
          <meta itemProp="category" content={productCategory} />
          <meta itemProp="sku" content={pid} />
          <meta itemProp="releaseDate" content={convertDate(announced)} />
          <meta itemProp="name" content={productName} />
          <meta itemProp="brand" content={brandName} />
          {userRating && (
            <span itemType="http://schema.org/AggregateRating" itemScope itemProp="AggregateRating">
              <meta itemProp="ratingValue" content={userRating} />
              <meta itemProp="ratingCount" content={ratingCount} />
              <meta itemProp="worstRating" content="1" />
              <meta itemProp="bestRating" content="5" />
            </span>
          )}

          <meta itemProp="image" content={imageLink} />
          <meta itemProp="description" content={plainDescription} />
          {sortPrice && sortPrice != "0" && (
            <span itemType="http://schema.org/Offer" itemScope itemProp="offers">
              <meta itemProp="priceCurrency" content="INR" />
              <meta itemProp="price" content={sortPrice} />
              <meta itemProp="priceValidUntil" content={priceValidUntil} />
              <meta itemProp="url" content={canonical} />
              {isProductAvailable ? <link href="http://schema.org/InStock" itemProp="availability" /> : ""}
            </span>
          )}
        </div>
      ) : (
        ""
      )}
      <div className="section">
        <div className="top_section">
          <h2>
            <span>{Config.locale.tech.userreviewandrating}</span>
          </h2>
        </div>
      </div>
      <div className="rating-values">
        <div className="user_review">
          <div className="caption">
            {Config.locale.tech.averagerating} {ratingCount} {Config.locale.tech.ratingbased}
          </div>
          {data && data.ur ? (
            <React.Fragment>
              <div>
                {Number.isNaN(Number.parseFloat(data.ur)) ? (
                  <b className="first_review">{data.ur}</b>
                ) : (
                  <span>
                    {data.ur}
                    <small>/5</small>
                  </span>
                )}
              </div>
              {/* <b>{Config.Locale.averageuserrating}</b> */}
            </React.Fragment>
          ) : (
            ""
          )}
        </div>
        <div className="user_star" data-exclude="amp">
          <ul>
            {defaultVal.map(item => {
              const gadgetWidth =
                typeof rateArr[item] !== "undefined" ? `${parseInt((rateArr[item] / max) * 100)}%` : "0%";
              return (
                // eslint-disable-next-line react/jsx-key
                <li>
                  <span className="star_txt">{item} &#9733;</span>
                  <span className="star_bg">
                    <span
                      className={`star${item}`}
                      style={{ width: gadgetWidth }}
                      data-attr={typeof rateArr[item] !== "undefined" ? rateArr[item] : "0"}
                    />
                  </span>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <div data-exclude="amp">
        <button
          disabled={isReviewDisabled}
          onClick={getreview}
          showReviewCard={showReviewCard}
          className={`btn-blue ${isReviewDisabled ? "disabled" : ""}`}
        >
          {Config.locale.tech.ratedevice}
        </button>
        {isMobilePlatform() && (
          <button className="btn-blue" onClick={handleComments.bind(this, data.id, router)}>
            {Config.locale.tech.text_write_reviews}
          </button>
        )}
      </div>
      {showReviewCard
        ? ReactDOM.createPortal(
            <div className="rating_popup">
              <div className="container">
                <header>
                  <h3>{Config.locale.tech.ratedevice}</h3>
                  <span onClick={closereview} className="close_icon" />
                </header>
                <GadgetPostreview
                  reviewID={data && data.id ? data.id : ""}
                  showLoginRegister={showLoginRegister}
                  loggedIn={loggedIn}
                />
              </div>
            </div>,
            document.getElementById("comments-parent-container"),
          )
        : ""}
    </div>
  ) : (
    ""
  );
};

GadgetsRating.propTypes = {
  data: PropTypes.object,
  getreview: PropTypes.func,
  closereview: PropTypes.func,
};

export default GadgetsRating;
