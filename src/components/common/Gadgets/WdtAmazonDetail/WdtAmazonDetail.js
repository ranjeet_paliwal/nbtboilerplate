import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Slider from "../../../desktop/Slider/Gadget";

import { _getStaticConfig, isMobilePlatform } from "../../../../utils/util";

const siteConfig = _getStaticConfig();

class WdtAmazonDetail extends PureComponent {
  state = {
    data: {},
  };

  componentDidMount() {
    const { productid } = this.props;
    // console.log(`productid :${productid}`);
    // console.log(process.env.API_ENDPOINT);

    const APIURL = `${process.env.API_BASEPOINT}/amazonshowwidget.cms?feedtype=json&productid=${productid}`;
    // console.log(`APIURL :${APIURL}`);

    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        // console.log("data",data);
        if (data !== null) {
          this.setState({
            data,
          });
        }
      });
  }

  render() {
    const { data } = this.state;
    const { affview, noimg, tag, msid, type, title } = this.props;
    let secData = "";
    secData =
      data &&
      data.product &&
      data.product.filter(data => {
        return data.affiliate === "amazon";
      });

    if (data && affview === "3") {
      const amazonlistId = `amazonlist_${msid}`;
      if (data && data.product) {
        return (
          <div
            className={!isMobilePlatform() ? "wdt_amazon_slider horizontal ui_slider" : "wdt_amazon_slider ui_slider"}
          >
            <div className="section">
              <div className="top_section">
                <h2>
                  <span>{title || siteConfig.locale.tech.buyhere}</span>
                  <b className="amazon_icon" />
                </h2>
              </div>
            </div>

            <div id={amazonlistId}>
              {data && data.product ? (
                <Slider
                  type="amazonwidgetslider"
                  size="1"
                  width={isMobilePlatform() ? "165" : "570"}
                  margin="15"
                  sliderData={secData}
                  category=""
                  islinkable="true"
                  amazonTag={tag}
                />
              ) : (
                ""
              )}
            </div>
          </div>
        );
      }
      return null;
    }

    return null;
  }
}

WdtAmazonDetail.propTypes = {
  productid: PropTypes.string,
  affview: PropTypes.string,
  noimg: PropTypes.string,
  tag: PropTypes.string,
  msid: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
};

export default WdtAmazonDetail;
