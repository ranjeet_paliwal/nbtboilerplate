import React from "react";
import PropTypes from "prop-types";

import AnchorLink from "../AnchorLink";
import { _getUrlOfCategory, _getStaticConfig } from "../../../utils/util";

const siteConfig = _getStaticConfig();

const GadgetVariants = ({ data, gadVarients, gadgetCategory }) => (
  <div className="wdt_varients">
    <h2>
      <span>{siteConfig.locale.tech.othervariants}</span>
    </h2>
    <ul>
      <li className="active">
        <span>{gadVarients}</span>
      </li>
      {data.gadgetinfo.variants.map(value => (
        // eslint-disable-next-line react/jsx-key
        <li>
          <AnchorLink
            href={`${process.env.WEBSITE_URL}tech/${_getUrlOfCategory(gadgetCategory)}/${value.seoname}`}
            target="_blank"
          >
            {value.name}
          </AnchorLink>
        </li>
      ))}
    </ul>
  </div>
);

GadgetVariants.propTypes = {
  data: PropTypes.object,
  gadVarients: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

export default GadgetVariants;
