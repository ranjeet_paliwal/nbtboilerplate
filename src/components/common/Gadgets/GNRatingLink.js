import React from "react";
import { _getStaticConfig } from "../../../utils/util";
import gadgetsConfig from "../../../utils/gadgetsConfig";

const siteConfig = _getStaticConfig();

const GNRatingLink = React.memo(({ card, submitRating }) => {
  if (!card) {
    return "";
  }

  const averageRating = card.averageRating ? (parseFloat(card.averageRating) / 2).toFixed(1) : null;
  const crictingRating = card.criticRating ? (parseFloat(card.criticRating) / 2).toFixed(1) : null;
  const seoCategory = gadgetsConfig.gadgetCategories[card.category];
  return (
    <span className="rating">
      {crictingRating && (
        <span className={card.criticRating ? "input" : ""}>
          {siteConfig.locale.tech.criticsrating}:
          <b>
            <small> {crictingRating}</small>/5
          </b>
        </span>
      )}

      {averageRating && (
        <span className="input">
          {siteConfig.locale.tech.userrating}:
          <b>
            <small> {averageRating}</small>/5
          </b>
        </span>
      )}

      {card.rumoured == 1 && card.upcoming == 1 && (
        <span className="input">
          <span className="sub-rating" onClick={submitRating(card)}>
            {siteConfig.locale.tech.submitrating}
          </span>

          {/* <AnchorLinkGN href={`/${seoCategory}/${card.lcuname}#rate`} className="submit">
            {siteConfig.locale.tech.submitrating}
          </AnchorLinkGN> */}
        </span>
      )}
    </span>
  );
});

export default GNRatingLink;
