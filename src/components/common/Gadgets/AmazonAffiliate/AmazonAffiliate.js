/* eslint-disable indent */
import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import ImageCard from "../../../common/ImageCard/ImageCard";
import { _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import SectionHeader from "../../../common/SectionHeader/SectionHeader";
import Slider from "../../../desktop/Slider/Gadget";

const siteConfig = _getStaticConfig();

class WdtAffiliatesList extends PureComponent {
  state = {
    data: {},
  };

  componentDidMount() {
    const { category } = this.props;
    const categoryName = category !== undefined && category ? category : "mobile";

    const APIURL = `https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=${categoryName}&format=json&product_per_page=30&showAffiliate=amazon`;

    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        this.setState({
          data,
        });
      });
  }

  render() {
    const { data } = this.state;
    const { tag, noimg, noh2, isslider } = this.props;

    if (data && data.product && Array.isArray(data.product) && isslider === "1") {
      return (
        <div className="wdt_amazon_slider ui_slider">
          {noh2 !== undefined && noh2 !== 1 ? (
            <div className="section">
              <div className="top_section">
                <h2>
                  <span>{siteConfig.locale.tech.purchasehere}</span>
                </h2>
                <b className="amazon_icon" />
              </div>
            </div>
          ) : null}
          {data && data.product ? (
            <Slider
              type="amazonwidgetslider"
              size="5"
              sliderData={data.product}
              category=""
              SliderClass="brandslider"
              islinkable="true"
              sliderWidth="960"
              margin={isMobilePlatform() ? "15" : "15"}
              amazonTag={tag}
            />
          ) : null}
        </div>
      );
    }

    if (data && data.product && Array.isArray(data.product)) {
      return (
        <div className="box-item wdt_amazon_list with-scroll">
          {noh2 !== undefined && noh2 !== 1 ? (
            <div className="section">
              <div className="top_section">
                <h2>
                  <span>{siteConfig.locale.tech.purchasehere}</span>
                  <b className="amazon_icon" />
                </h2>
              </div>
            </div>
          ) : null}
          <ul>
            {data && data.product && Array.isArray(data.product)
              ? data.product.map(item => {
                return <LiData key={item.pid} item={item} tag={tag} noimg={noimg} />;
              })
              : null}
          </ul>
        </div>
      );
    }
    return null;
  }
}

const LiData = ({ item, tag, noimg, apnd }) => {
  if (item.affiliate === "amazon") {
    const amzga = `${item.title}_${item.price}`;
    const url = encodeURIComponent(
      `${item.url}?psc=1&SubscriptionId=AKIAJX7PDW7DEP2LLJLA&tag=${tag}&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B07HGJK535`,
    );
    const affprice = item.price;

    const affiliateUrl = `${process.env.WEBSITE_URL}affiliate_amazon.cms?url=${url}&price=${affprice}&title=${item.title}&amz_ga=${amzga}`;

    // console.log("amzga :"+amzga);
    // console.log("url :"+url);
    // console.log("affiliateUrl :"+affiliateUrl);

    return (
      <li key={item.pid} className="news-card horizontal">

        {noimg !== undefined && noimg !== 1 ? (
          <span className="img_wrap">
            <a title={item.title} href={affiliateUrl} target="_blank" rel="nofollow">
              <ImageCard src={item.imageUrl} type="absoluteImgSrc" size="gnthumb" />
            </a>
          </span>
        ) : null}

        <span className="con_wrap">
          <h4><a title={item.title} href={affiliateUrl} target="_blank" rel="nofollow" className="text_ellipsis">{item.title}</a></h4>
          <span className="price_tag">&#8377; {item.price}</span>
        </span>
        <span className="btn_wrap absolute">
          <button className="btn-buy"><a title={item.title} href={affiliateUrl} target="_blank" rel="nofollow">{siteConfig.locale.tech.purchase}</a></button>
        </span>
      </li>
    );
  }
};

WdtAffiliatesList.propTypes = {
  title: PropTypes.string,
  category: PropTypes.string,
  tag: PropTypes.string,
  noimg: PropTypes.string,
  noh2: PropTypes.string,
  isslider: PropTypes.string,
};

export default WdtAffiliatesList;
