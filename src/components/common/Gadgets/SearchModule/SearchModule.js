import React, { PureComponent } from "react";
import PropTypes from "prop-types";
// import { createConfig } from '../../../modules/videoplayer/utils';
import "../../css/desktop/GadgetNow.scss";
import { _getStaticConfig, isMobilePlatform } from "../../../../utils/util";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import { browserHistory } from "react-router";
const gadgetMapping = (gadgetsConfig && gadgetsConfig.gadgetMapping) || "";

const siteConfig = _getStaticConfig();

class SearchModule extends PureComponent {
  constructor(props) {
    super(props);
    this.count = 0;
    const deviceInfoProp = props && props.deviceInfo;
    this.state = {
      deviceInfo: {
        device1: {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        },
        device2: {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        },
        device3: {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        },
        device4: {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        },
      },
      deviceData: "",
      suggestedDevices: "",
      IsdeviceAdded: false,
      activeGadget: this.props.activeGadget,
      searchCategory: "",
      searchSeoName: "",
      addedSuggestedDevice: "",
      count: 0,
      // itemAddedToCompare: 0,
    };

    if (deviceInfoProp) {
      this.state.deviceInfo = deviceInfoProp;
    }
  }

  componentWillReceiveProps(nextProps) {
    let { activeGadget, addedSuggestedDevice } = this.state;
    const { location } = this.props;
    const { deviceData } = this.state;
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };

    if (nextProps && nextProps.activeGadget !== this.props.activeGadget) {
      activeGadget = nextProps.activeGadget;
      for (const key in updatedDeviceInfo) {
        if (Object.prototype.hasOwnProperty.call(updatedDeviceInfo, key)) {
          updatedDeviceInfo[key].searchResult = "";
          updatedDeviceInfo[key].userInput = "";
          updatedDeviceInfo[key].userInputHtml = "";
        }
      }
      this.setState({
        activeGadget,
        deviceInfo: updatedDeviceInfo,
        deviceData,
        suggestedDevices: "",
      });
    }
    if (nextProps && nextProps.addedSuggestedDevice !== this.props.addedSuggestedDevice) {
      addedSuggestedDevice = nextProps.addedSuggestedDevice;
      this.setState({ addedSuggestedDevice });
      this.addFromSuggestions(addedSuggestedDevice);
    }

    // This is for auto fill the compare box when landed on comaparsion page from gadget List page
    // clicking by compare link in device description

    if (nextProps.deviceInfo && nextProps.deviceInfo.device1 && nextProps.deviceInfo.device1.userInputHtml) {
      const deviceInfoCopy = { ...nextProps.deviceInfo };

      // Object.keys(deviceInfoCopy).forEach(item => {
      //   const paramDeviceInfo = nextProps.deviceInfo.device1;
      //   updatedDeviceInfo.device1.userInputHtml = paramDeviceInfo.userInputHtml;
      //   updatedDeviceInfo.device1.seoName = paramDeviceInfo.seoName;
      // });

      this.setState({
        activeGadget,
        deviceInfo: deviceInfoCopy,
      });
    }
  }

  addFromSuggestions = deviceInfo => {
    const { activeGadget, state } = this;
    let device;
    for (const key in state.deviceInfo) {
      if (!state.deviceInfo[key].userInputHtml) {
        device = key;
        break;
      }
    }
    this.updateSuggestedData(state.activeGadget, deviceInfo, device);
  };

  updateSuggestedData = (activeGadget, deviceInfo, device) => {
    // const { suggestedDevices, deviceInfo } = this.state;
    if (device) {
      const updatedDeviceInfo = {
        ...this.state.deviceInfo,
      };

      for (const key in updatedDeviceInfo) {
        if (updatedDeviceInfo[key].userInputHtml === deviceInfo.Product_name) {
          updatedDeviceInfo[device].userInput = "";
          return this.setState({
            deviceInfo: updatedDeviceInfo,
            IsdeviceAdded: true,
          });
        }
      }

      updatedDeviceInfo[device] = {
        userInput: updatedDeviceInfo[device].userInput,
        isVisible: updatedDeviceInfo[device].isVisible,
        userInputHtml: deviceInfo.Product_name,
        seoName: deviceInfo.seoname,
        searchResult: updatedDeviceInfo[device].searchResult,
      };

      if (
        !this.state.suggestedDevices ||
        this.state.searchCategory !== activeGadget ||
        this.state.searchSeoName !== deviceInfo.seoname
      ) {
        const apiSuggested = `${process.env.API_BASEPOINT}/pwafeeds/wdt_gadgetreleted.cms?feedtype=sjson&type=brand&category=${activeGadget}&productid=${deviceInfo.seoname}`;
        fetch(apiSuggested)
          .then(promise => promise.json())
          .then(response => {
            this.props.suggestedDevicesCallBack(response, updatedDeviceInfo);
            this.setState({
              deviceInfo: updatedDeviceInfo,
              suggestedDevices: response,
              IsdeviceAdded: false,
              searchCategory: activeGadget,
              searchSeoName: deviceInfo.seoname,
            });
          });
      } else {
        this.setState({
          deviceInfo: updatedDeviceInfo,
          IsdeviceAdded: false,
        });
      }
    }
  };

  searchDevice(value, device, gadget) {
    const { state } = this;
    const { deviceData } = state;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    fetch(`${process.env.API_BASEPOINT}/autosuggestion.cms?type=brand&tag=product_cat&category=${gadget}&q=${value}`)
      .then(response => {
        // Examine the text in the response
        response.json().then(data => {
          // console.log(data);
          const devices =
            data &&
            data.map(item => {
              return { Product_name: item.Product_name, seoname: item.seoname };
            });

          updatedDeviceInfo[device] = {
            userInput: updatedDeviceInfo[device].userInput,
            isVisible: updatedDeviceInfo[device].isVisible,
            userInputHtml: updatedDeviceInfo[device].userInputHtml,
            searchResult: devices.filter(item => item !== ""),
          };

          this.setState({
            deviceInfo: updatedDeviceInfo,
            deviceData,
          });
          // return deviceData.filter(item => item != '');
        });
      })
      .catch(() => {
        // console.log('Fetch Error :-S', err);
      });
  }

  removeDevice = (deviceInfo, deviceName) => {
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };
    updatedDeviceInfo[deviceName] = {
      userInput: "",
      userInputHtml: "",
      seoName: "",
      searchResult: "",
    };

    this.setState({
      deviceInfo: updatedDeviceInfo,
    });
  };
  handleChange = evt => {
    const { state } = this;
    const { activeGadget } = state;
    const userInput = evt.currentTarget.value;
    const device = evt.currentTarget.name;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    updatedDeviceInfo[device].userInput = userInput;
    this.setState({
      deviceInfo: updatedDeviceInfo,
      IsdeviceAdded: false,
    });
    // console.log('handleChange', this.state);
    this.searchDevice(userInput, device, activeGadget);
  };

  onFocusHandler = () => {
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    for (const key in deviceInfoCopy) {
      if ({}.hasOwnProperty.call(deviceInfoCopy, key)) {
        const device = deviceInfoCopy[key];
        deviceInfoCopy[key] = {
          userInput: "",
          userInputHtml: device.userInputHtml,
          seoName: device.seoName,
          searchResult: "",
        };
      }
    }
    this.setState({ deviceInfo: deviceInfoCopy });
  };

  keyDownHandler = event => {
    const deviceName = event.currentTarget.name;
    if (event.keyCode !== 40 && event.keyCode !== 38 && event.keyCode !== 13) {
      return;
    }
    const liItem = document.querySelector(`#ulinput_${deviceName} .active`);
    const inputSelected = document.querySelector(`#ulinput_${deviceName}`);

    if (event.keyCode === 40) {
      // Arrow Down Key Pressed
      if (!liItem.nextSibling) {
        const firstLi = inputSelected.firstChild;
        liItem.classList.remove("active");
        firstLi.classList.add("active");
      } else {
        liItem.nextSibling.classList.add("active");
        liItem.classList.remove("active");
      }
    } else if (event.keyCode === 38) {
      // Arrow Up Key Pressed
      if (!liItem.previousSibling) {
        const lastLi = inputSelected.lastChild;
        liItem.classList.remove("active");
        lastLi.classList.add("active");
      } else {
        liItem.previousSibling.classList.add("active");
        liItem.classList.remove("active");
      }
    } else if (event.keyCode === 13) {
      // ENTER key is pressed, prevent the form from being submitted,
      event.preventDefault();
      liItem.click();
    }
  };

  getKeyByValue = value => {
    const gadgets = gadgetMapping;
    for (const key in gadgets) {
      if (Object.prototype.hasOwnProperty.call(gadgets, key)) {
        if (gadgets[key] === value) return key;
      }
    }
    return "";
  };

  submitEventHandler = event => {
    event.preventDefault();
    const { searchBoxStatus, source } = this.props;
    if (typeof searchBoxStatus == "function") {
      searchBoxStatus();
    }

    const { deviceInfo, activeGadget, count } = this.state;
    const seoItem = [];
    if (count >= 2) {
      if (deviceInfo) {
        for (const key in deviceInfo) {
          if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
            if (deviceInfo[key].seoName) {
              seoItem.push(deviceInfo[key].seoName);
            }
          }
        }
        if (seoItem.length >= 2) {
          seoItem.sort();
          const seoName = this.getKeyByValue(activeGadget);
          const seoPath = `/tech/compare-${seoName}/${seoItem.join("-vs-")}`;

          if (source == "gadgetsnow") {
            // This code is only to make home page live only
            // once will go live with all gadgets pages will remove this and would be used else condition
            window.open(`${siteConfig.weburl}${seoPath}`);
          } else {
            browserHistory.push(seoPath && seoPath.toLowerCase());
          }
        }
      }
    }
  };

  clickEventHandler = (deviceInfo, deviceName) => {
    // const {
    //   routeParams: { device },
    // } = this.props;
    const activeGadget = this.state.activeGadget;
    this.updateSuggestedData(activeGadget, deviceInfo, deviceName);
  };

  countCallBack = value => {
    let { count } = this.state;
    count = value;
    this.setState({ count });
  };

  suggestedDevicesCallBack = () => {};

  render() {
    const { deviceInfo, activeGadget, IsdeviceAdded, suggestedDevices } = this.state;
    let count = 0;
    const deviceInfoCopy = { ...deviceInfo };

    const gadgetSeo = gadgetMapping[activeGadget];
    const prepareDeviceURL = `${process.env.WEBSITE_URL}/compare-${gadgetSeo}`;

    if (deviceInfo) {
      for (const key in deviceInfo) {
        if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
          if (deviceInfo[key].userInputHtml) {
            count++;
            this.countCallBack(count);
          }
          if (count === 2) {
            this.countCallBack(count);
            break;
          }
        }
      }
    }
    return (
      <React.Fragment>
        <div className="input_gadgets">
          <form onSubmit={this.submitEventHandler}>
            {Object.keys(deviceInfoCopy).map(deviceName => {
              return (
                <ManageDevice
                  key={deviceName}
                  device={deviceInfoCopy[deviceName]}
                  deviceName={deviceName}
                  changed={this.handleChange}
                  focused={this.onFocusHandler}
                  keydown={this.keyDownHandler}
                  clicked={this.removeDevice}
                  self={this}
                  IsdeviceAdded={IsdeviceAdded}
                />
              );
            })}
            <div className="btn-captions">
              <input
                disabled={count < 2}
                type="submit"
                name="compare"
                className={`btn-blue ${count < 2 ? "disabled" : ""}`}
                value={siteConfig.locale.tech.comparedotxt}
              />
              {count < 2 ? (
                <span className="info-camparsion">
                  {siteConfig.locale && siteConfig.locale.tech.minimumComparisonError}
                </span>
              ) : (
                ""
              )}
            </div>
            {IsdeviceAdded ? <span className="info-camparsion red">{siteConfig.locale.tech.alreadyAdded}</span> : ""}
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const ManageDevice = ({ device, deviceName, changed, focused, keydown, self, IsdeviceAdded }) => {
  //  console.log("device.searchResult", device);
  return device && !device.userInputHtml ? (
    <div className="input_field">
      <input
        type="text"
        placeholder={siteConfig.locale.tech.addDevice}
        name={deviceName}
        className={deviceName}
        value={device.userInput}
        onChange={changed}
        onKeyDown={keydown}
        onFocus={focused}
        id={`input_${deviceName}`}
        autoComplete="off"
      />
      {!IsdeviceAdded && device.searchResult && Array.isArray(device.searchResult) && (
        <div className="auto-suggest">
          <ul id={`ulinput_${deviceName}`}>
            {device.searchResult.length > 0 ? (
              device.searchResult.map((item, index) => (
                <li
                  key={item.Product_name}
                  data-item={item.Product_name}
                  onClick={self.clickEventHandler.bind(this, item, deviceName)}
                  className={index == 0 ? "active" : ""}
                >
                  <span>{item.Product_name}</span>
                </li>
              ))
            ) : device.userInput ? (
              <li className="active" style={{ "text-align": "center" }}>
                No results found
              </li>
            ) : (
              ""
            )}
          </ul>
        </div>
      )}
    </div>
  ) : (
    <div className="input_field">
      <input type="text" value={device.userInputHtml} />
      <span className="close_icon" onClick={self.removeDevice.bind(this, device, deviceName)} />
    </div>
  );
};

SearchModule.propTypes = {
  alaskaData: PropTypes.object,
};

export default SearchModule;
