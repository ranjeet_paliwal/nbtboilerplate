import React, { useContext } from "react";
import DataContext from "../../../../utils/utilityContext";
import Slider from "../../../../components/desktop/Slider/index";
import AdCard from "../../AdCard";
import KeyWordCard from "../../KeyWordCard";
import VideoItem from "../../VideoItem/VideoItem";
import { isMobilePlatform } from "../../../../utils/util";
import SectionHeader from "../../SectionHeader/SectionHeader";
import News from "../Widgets/News";

const GadgetNowRHS = () => {
  const data = useContext(DataContext);
  const { newsSectionData, photoSectionData, trendingData, videoSectionData, mostReadNews } = data;
  // console.log("data111", data);

  return (
    <React.Fragment>
      <AdCard mstype="mrec1" adtype="dfp" className="ad1 mrec1 box-item  rhs-ads" />
      <News data={newsSectionData} cssClass="tech-rhs-news" />
      <div className="box-item gn_trending">
        <div className="list-trending-topics">
          {trendingData ? (
            <KeyWordCard items={trendingData.items} secname={trendingData && trendingData.secname} />
          ) : null}
        </div>
      </div>
      <div className="box-item rhs-ads">
        <AdCard mstype="mrec2" adtype="dfp" className="ad1 mrec2" ctnstyle="mrec2" />
      </div>
      <VideoSection data={videoSectionData} />
      {/* Photos */}
      <div className={`box-item ${!isMobilePlatform() ? "tech-rhs-gallery" : "wdt_next_slider"}`}>
        {photoSectionData && (
          <React.Fragment>
            <SectionHeader
              sectionhead={photoSectionData.secname}
              weblink={photoSectionData.override}
              morelink={photoSectionData.override}
            />
            <div className="gallery">
              <Slider
                margin={isMobilePlatform() ? "0" : "25"}
                size="2"
                sliderData={photoSectionData.items}
                width="130"
                type="grid"
              />
            </div>
          </React.Fragment>
        )}
      </div>
      {/* Most Viewed Articles */}
      {mostReadNews && mostReadNews.items ? <News data={mostReadNews} cssClass="tech-rhs-news" /> : null}
    </React.Fragment>
  );
};

export default GadgetNowRHS;

const VideoSection = ({ data }) => {
  return data && data.items ? (
    <div className="box-item rhs-video-list">
      <SectionHeader
        sectionhead={data && data.secname}
        weblink={data.wu || data.override}
        morelink={data.wu || data.override}
      />

      <ul className="list-horizontal video">
        {data &&
          data.items &&
          data.items.map((vidData, index) => {
            return vidData && typeof vidData === "object" && vidData.constructor === Object ? (
              vidData.tn != "ad" ? (
                <li key={vidData.id} className="news-card video">
                  <VideoItem
                    key={vidData.id}
                    item={vidData}
                    index={index}
                    className="vd-slider-listview"
                    videoIntensive={!vidData.eid}
                    imgsize="smallwidethumb"
                    istrending
                  />
                </li>
              ) : null
            ) : null;
          })}
      </ul>
    </div>
  ) : null;
};
