import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchFilterListDataIfNeeded } from "../../../actions/gn/gadgetlist/gadgetfiltertool";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import { AnalyticsGA } from "../../lib/analytics/index";
import { _getStaticConfig, isMobilePlatform } from "../../../utils/util";
import SvgIcon from "../../../components/common/SvgIcon";
const siteConfig = _getStaticConfig();

class GadgetFilterTool extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFilterBox: false,
    };
  }

  componentDidMount() {
    const { dispatch, params, router, value, category } = this.props;
    const { query } = this.props.router.location.pathname;

    GadgetFilterTool.fetchFilterListData({ dispatch, query, params, router }).then(data => {});
  }

  componentDidUpdate() {
    const { params } = this.props;
    const clearAll = document.querySelector('b[id="clearAll"]');
    if (clearAll) {
      if (document.querySelectorAll('input[type="checkbox"]:checked').length > 0) {
        clearAll.style.display = "block";
      } else {
        clearAll.style.display = "none";
      }
    }

    const brandCheckBoxes = document.querySelectorAll(".filtercontent-brand li input");
    if (brandCheckBoxes) {
      for (var i = 0; i < brandCheckBoxes.length; i++) {
        if (params && params.brand && brandCheckBoxes[i].value.toLowerCase() == params.brand.toLowerCase()) {
          brandCheckBoxes[i].checked = "checked";
        }
      }
    }
  }

  removeFilter(obj) {
    // console.log('selected option closed', obj.target.parentElement.id);
    if (obj.target.getAttribute("class") == "close_icon") {
      document.getElementById(obj.target.parentElement.id).remove();
      const getInputboxId = obj.target.parentElement.id.substring(3);
      eval(`document.filtercriteria.${getInputboxId}.checked=false`);
      this.filterApply(obj.target.parentElement.id.substring(3));
      if (getInputboxId.includes("brand")) {
        const listItems = document.querySelectorAll(".filtercontent-brand li");
        if (listItems.length > 0) {
          for (let i = 0; i < listItems.length; i++) {
            listItems[i].classList.remove("hide");
          }
        }
        document.getElementById("brand").value = "";
      }
      if (getInputboxId.includes("screen_size")) {
        const listItemsScreens = document.querySelectorAll(".filtercontent-screen-size li");
        if (listItemsScreens.length > 0) {
          for (let i = 0; i < listItemsScreens.length; i++) {
            listItemsScreens[i].classList.remove("hide");
          }
        }
        document.getElementById("screen-size").value = "";
      }
    }
  }

  filterCases(obj) {
    // debugger;
    if (obj.target.checked) {
      // console.log('selected option', obj.target.value);
      const closenode = document.createElement("span");
      closenode.className = "close_icon";
      closenode.innerText = "Close";
      const div = document.createElement("div");
      div.className = "filterkey";
      div.id = `div${obj.target.name}`;
      // alert(obj.target.parentElement.innerText);
      if (obj.target.getAttribute("datavalues") == "rumoured" || obj.target.getAttribute("datavalues") == "upcoming")
        div.innerHTML = obj.target.getAttribute("datavalues");
      else div.innerHTML = obj.target.parentElement.innerText;
      div.appendChild(closenode);
      document.getElementById("filterapplied").appendChild(div);
      this.filterApply(obj.target.value);
    } else {
      // alert('remove');
      document.getElementById(`div${obj.target.getAttribute("name")}`).remove();
      this.filterApply(obj.target.value);
    }
  }

  filterApply(obj) {
    // debugger;
    const { router, sortCriteria, params } = this.props;
    let category = (params && params.category) || "";

    const filterkeyword = [];
    let filtercriteria = "";
    const { activeSort } = sortCriteria;
    let sortby = "";
    let sortStr = "";
    let filterby = "";
    let upcoming = false;
    let pathName = router.location.pathname;

    const checkBox = document.getElementsByTagName("input");
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == "radio") {
        checkBox[b].checked && checkBox[b].value != "popular" ? (sortby += `sort=${checkBox[b].value}`) : null;
      }
      // if(checkBox[b].type=='checkbox' && checkBox[b].checked==true && checkBox[b].getAttribute("datavalues")=='upcoming'){
      //     prepend=checkBox[b].getAttribute("datavalues");
      // }
      // else
      if (checkBox[b].type == "checkbox" && checkBox[b].checked == true) {
        if (checkBox[b].getAttribute("datavalues") == "upcoming") {
          upcoming = true;
        } else if (filterkeyword.includes(checkBox[b].getAttribute("datavalues"))) {
          filterby += `%7C${checkBox[b].value}`;
        } else {
          filterkeyword.push(checkBox[b].getAttribute("datavalues"));
          filterby += filterby != "" || sortby != "" ? "&" : "";
          filterby += `${checkBox[b].getAttribute("datavalues")}=${checkBox[b].value}`;
        }
      }
    }

    // handle if single brand is selected no filters parms added
    if (upcoming) {
      category = `upcoming-${category}`;
    }
    let filterText = `/tech/${category}/filters/`;

    if (
      sortby == "" &&
      filterkeyword.length > 0 &&
      filterkeyword.length < 2 &&
      filterkeyword[0].toLowerCase() == "brand" &&
      filterby != "" &&
      filterby.indexOf("%7C") < 0
    ) {
      const temparray = filterby.split("=");
      if (activeSort) {
        filtercriteria = filterText + `brand=${temparray[1]}&sort=${activeSort}`;
      } else {
        filtercriteria += `/tech/${category}/${temparray[1] && temparray[1].toLowerCase()}`;
      }
    } else {
      if (activeSort) {
        sortStr += `&sort=${activeSort}`; // append sorting if required
      }

      if (category) {
        if (filterby && sortStr) {
          filtercriteria = filterText + filterby + sortStr;
        } else if (filterby) {
          filtercriteria = filterText + filterby;
        } else if (sortStr) {
          filtercriteria = filterText + sortStr;
        } else {
          filtercriteria = `/tech/${category}`;
        }
      }
    }

    // if (upcoming) {
    //   routepath = `/tech/upcoming-${this.props.params.category}`;
    // } else {
    //   routepath = `/tech/${this.props.params.category}`;
    // }
    // const array = routepath.split("/");
    // const routearray = [];
    /* if (array.length > 2) {
      // Re Manipulate routepath if route have already filters

      for (let i = 0; i < array.length; i++) {
        if (i < 3) routearray.push(array[i]);
      }
      const updateroute = routearray.join("/");
      routepath = updateroute;
    } */

    // let pathName = routepath;
    if (filtercriteria) {
      pathName = filtercriteria;
    }

    // let sortCrteria = "";
    // if (document.querySelector(".gl_tabs .tabs li.active")) {
    //   sortCrteria = document.querySelector(".gl_tabs .tabs li.active").getAttribute("data-sort");
    //   if (sortCrteria) {
    //     sortCrteria = "&sort=" + sortCrteria;
    //     pathName += sortCrteria;
    //   }
    // }

    /*  if (filtercriteria != "") {
      this.props.router.push(routepath + filtercriteria + sortCrteria);
    } else {
      this.props.router.push(routepath);
    } */

    this.props.router.push(pathName);

    AnalyticsGA.event({
      category: "Web GS",
      action: "GDS_category_filter",
      label: "filtercriteria",
      // overrideEvent: "",
    });

    // analyticsGA.event("Web GS", "GDS_category_filter", filtercriteria);
    // if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
    //   analyticsGA.pageview(window.location.origin + location.pathname);
    // }
  }

  restForm(obj) {
    // clear check box

    const checkBox = document.getElementsByTagName("input");
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == "radio") {
        checkBox[b].checked = false;
      }
      if (checkBox[b].type == "checkbox") {
        checkBox[b].checked = false;
      }
      if (checkBox[b].type == "text") {
        const listItems = document.querySelectorAll(".filtercontent-brand li");
        const listItemsScreens = document.querySelectorAll(".filtercontent-screen-size li");
        if (listItems.length > 0) {
          for (let i = 0; i < listItems.length; i++) {
            listItems[i].classList.remove("hide");
          }
        }
        if (listItemsScreens.length > 0) {
          for (let i = 0; i < listItemsScreens.length; i++) {
            listItemsScreens[i].classList.remove("hide");
          }
        }
        // }
        checkBox[b].value = "";
      }
    }
    document.getElementById("filterapplied").innerHTML = "";
    this.filterApply(checkBox);
  }

  HtmlSearch(obj) {
    // debugger;
    const keyword = obj.target.value.toLowerCase();
    if (keyword == "") {
      // show all check box
      const allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        // let inputbox=allcheckbox[i].getElementsByTagName("input");
        allcheckbox[i].classList.remove("hide");
      }
    } else {
      // showcase based on keyword
      const allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        const inputbox = allcheckbox[i].getElementsByTagName("input");

        const inputval = inputbox[0].value.toLowerCase();
        if (inputval.indexOf(keyword) < 0) allcheckbox[i].classList.add("hide");
      }
    }
  }

  handlerFilterBox = () => {
    const compareTool = document.getElementById("CompareTool");
    if (compareTool.style.display == "block") {
      compareTool.style.display = "none";
    } else {
      compareTool.style.display = "block";
    }

    // this.setState({ showFilterBox: !showFilterBox });
  };

  render() {
    const _this = this;
    const { showFilterBox } = this.state;
    const { techgadgetfilter, category, params } = this.props;
    if (isMobilePlatform()) {
      return (
        <MobileFilterTool data={techgadgetfilter} clicked11={this.handlerFilterBox} showFilterBox={showFilterBox} />
      );
    }

    return (
      <React.Fragment>
        <div className="section">
          <div className="top_section">
            <h2>
              <span>{siteConfig.locale.tech.filtertxt}</span>
              <b id="clearAll" onClick={() => this.restForm(this)}>
                {siteConfig.locale.tech.clearall}
              </b>
            </h2>
          </div>
        </div>
        <div className="con_filters">
          <form name="filtercriteria" id="filtercriteria">
            <React.Fragment>
              <ErrorBoundary>
                <div className="filterapplied" id="filterapplied" onClick={this.removeFilter.bind(this)} />
                <div className="filters-options" id="sortData">
                  {techgadgetfilter && techgadgetfilter.facets && techgadgetfilter.facets.length > 0
                    ? techgadgetfilter.facets.map((item, index) => {
                        let filtercase = item.q_param;
                        return (
                          // eslint-disable-next-line react/jsx-key
                          <div className="options_item">
                            <h3>{item.d_name}</h3>
                            <ul className="hide" className={`filtercontent-${item.q_param}`}>
                              {item.q_param == "brand" || item.q_param == "screen-size" ? (
                                <div className="search">
                                  <input
                                    onChange={_this.HtmlSearch.bind(this)}
                                    placeholder={siteConfig.locale.tech.search}
                                    data-search-for={item.q_param}
                                    className="filters-search-box"
                                    type="text"
                                    autoComplete="off"
                                    id={item.q_param}
                                  />
                                  <SvgIcon name="search" className="search_icon" />
                                </div>
                              ) : null}

                              {item && item.values && item.values.length > 0 ? (
                                item.values.map((item, index) => {
                                  return (
                                    <li>
                                      <label>
                                        <input
                                          onClick={_this.filterCases.bind(this)}
                                          index={index}
                                          datavalues={filtercase}
                                          type="checkbox"
                                          name={filtercase.replace("-", "_") + index}
                                          value={item.qname}
                                        />
                                        {item.d_name}
                                      </label>
                                    </li>
                                  );
                                })
                              ) : item && item.values && item.values.val ? (
                                <li>
                                  <label>
                                    <input
                                      onClick={_this.filterCases.bind(this)}
                                      index="0"
                                      datavalues={filtercase}
                                      type="checkbox"
                                      name={`${filtercase.replace("-", "_")}0`}
                                      value={item.values.val.qname}
                                    />
                                    {item.values.val.d_name}
                                  </label>
                                </li>
                              ) : null}
                            </ul>
                          </div>
                        );
                      })
                    : null}
                </div>
              </ErrorBoundary>
            </React.Fragment>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

const MobileFilterTool = ({ data, clicked11, showFilterBox }) => {
  // console.log("MobileFilterTool", data);

  // data &&
  //   data.facets &&
  //   data.facets.forEach(element => {
  //     console.log("element1", element);
  //   });

  const elements = data && data.facets && data.facets.map(element => element.values);
  console.log("elementselements", elements);

  return data && data.facets && Array.isArray(data.facets) ? (
    <div id="Tab2">
      <div class="btn-filter btn" data-attr="overlay_compare_btn2" style={{ bottom: "160px" }}>
        <span>ddddd</span>
      </div>
      <div id="CompareTool" class="overlay" style={{ display: "none" }} onClick={clicked11.bind(this, "ddd")}>
        <div class="backToScreen">
          <a id="FilterCloseOverLayScreen" href="javascript:void(0)" class="back_icon"></a>
          FILTER
          <span class="btn_filter apply">APPLY</span>
          <span id="resetform" class="btn_filter clear">
            CLEAR
          </span>
        </div>
        <div class="overlay-content">
          <form name="filtercriteria" id="filtercriteria">
            <div class="filters">
              <div class="filterapplied scroll" id="filterapplied">
                {/* <div class="filterkey" id="divos0">
                  ऐंड्रॉयड<span class="close_icon"></span>
                </div> */}
              </div>
              <div class="tabs">
                <ul class="active">
                  <li index="0" id="filterby-sort" class="filterby-sort">
                    <span>SORT BY</span>
                  </li>
                  {data.facets.map((item, index) => {
                    return (
                      <li index={index} className={`filterby-${item.q_param}`}>
                        <span>{item.d_name}</span>
                      </li>
                    );
                  })}
                </ul>
              </div>

              <div class="tab-values">
                <div id="sortData">
                  <ul>
                    <li class="filterby-sort" id="filterby-sortcontent">
                      <label>
                        <input type="radio" name="sortby" value="latest" selected="" />
                        Latest
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="popular" />
                        Popular
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="rating-desc" />
                        Rating
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="price-asc" />
                        Price (Low To High)
                      </label>
                      <label>
                        <input type="radio" name="sortby" value="price-desc" />
                        Price (High To Low)
                      </label>
                    </li>

                    {elements.map((element, methodIndex) => {
                      return element.map((item, unitIndex) => {
                        return (
                          <li class={`filtercontent-${item.d_name}`} key={item.d_name}>
                            <label>
                              <input
                                index={unitIndex}
                                datavalues={item.qname}
                                type="checkbox"
                                // name={element.q_param}
                                value={item.qname}
                              />
                              {item.d_name}
                            </label>
                          </li>
                        );
                      });
                    })}

                    {/* {elements.forEach(element => {
                      console.log("aaa", element);
                      element.map((item, index) => {
                        return (
                          <li class={`filtercontent-${item.d_name}`} key={item.d_name}>
                            <label>
                              <input
                                index={index}
                                datavalues={item.qname}
                                type="checkbox"
                                // name={element.q_param}
                                value={item.qname}
                              />
                              {item.d_name}
                            </label>
                          </li>
                        );
                      });
                    })} */}
                  </ul>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  ) : (
    ""
  );
};

function mapStateToProps(state) {
  return {
    ...state.gadgetfiltertool,
  };
}
GadgetFilterTool.fetchFilterListData = ({ dispatch, params, query, router, category, keyword, index }) => {
  return dispatch(fetchFilterListDataIfNeeded(params, query, router, category, keyword, index));
};
export default connect(mapStateToProps)(GadgetFilterTool);
