import React from 'react';
import PropTypes from 'prop-types';
import ImageCard from '../ImageCard/ImageCard';
import AnchorLink from '../AnchorLink';

const ArticlesListBlock = React.memo(props => {
    const { data, widgetClassName } = props;
    const widgetClass = widgetClassName || '';

    return data && data.items && Array.isArray(data.items) && data.items.length > 0 ? (
        <div
            className={`box-item lst-news ${data.id === 'trending' ? 'trending-topics' : ''
                } ${widgetClass}`}
        >
            <div className="section">
                <div className="top_section">
                    <h2>
                        <span>{data.secname}</span>
                    </h2>
                </div>
            </div>
            
            <ul>
                {data.items.map(item => (
                    <li key={item.id} className="news-card horizontal">
                        <span className="img_wrap">
                            <ImageCard
                                msid={item.imageid}
                                imgSize={item.imgsize}
                                width="115"
                                height="85"
                                islinkable="true"
                                link={item.wu}
                                alt={item.hl}
                                title={item.hl}
                            />
                        </span>
                        <span className="con_wrap">
                            <AnchorLink href={item.wu}><span className="text_ellipsis">{item.hl}</span></AnchorLink>
                        </span>
                    </li>
                ))}
            </ul>
            
        </div>
    ) : null;
});

ArticlesListBlock.propTypes = {
    data: PropTypes.object,
};

ArticlesListBlock.displayName = 'ArticlesListBlocks';

export default ArticlesListBlock;