/* eslint-disable react/prop-types */
/* eslint-disable indent */
import React, { PureComponent } from "react";
import "../css/liveblog-strip.scss";
import AnchorLink from "../AnchorLink";
import fetch from "../../../utils/fetch/fetch";
import { _getStaticConfig, _isCSR } from "../../../utils/util";

const siteConfig = _getStaticConfig();

// require("es6-promise").polyfill();

class LiveBlog extends PureComponent {
  state = {
    lbData: [],
  };

  // eslint-disable-next-line react/sort-comp
  getliveblogHandler = (type, dataSource) => {
    let apilb = siteConfig.liveblogApi;
    if (type === "liveblog") {
      apilb = dataSource;
    }

    fetch(apilb)
      .then(result => {
        this.setState({
          lbData: result,
        });
      })
      .catch({});
  };

  componentDidMount() {
    const { type, dataSource } = this.props;
    this.getliveblogHandler(type, dataSource);
    setInterval(() => {
      this.getliveblogHandler(type, dataSource);
    }, 30000); // 3 sec
  }

  render() {
    const { cssClass } = this.props;
    const { lbData } = this.state;
    const updatedItems = lbData.lbcontent;
    const lbUrl = (lbData && lbData.lbcontainer && lbData.lbcontainer.wu) || "";

    return updatedItems && Array.isArray(updatedItems) && updatedItems.length > 0 ? (
      <React.Fragment>
        <div className={`${cssClass}`}>
          <h3>
            <AnchorLink href={lbUrl}>{siteConfig.locale.live_blog}</AnchorLink> <span className="liveblink_newscard large"></span>
          </h3>
          <div className="lb-strip">
            <ul className="lb-card">
              {updatedItems.map(value => {
                return (
                  <li key={value.title} className="con_wrap">
                    <span className="time-caption">{value.shortDateTime}</span>
                    {value.hoverridLink && value.hoverridLink !== "null" ? (
                      <AnchorLink to={value.hoverridLink} className="text_ellipsis">
                        {value.title}
                      </AnchorLink>
                    ) : (
                        <span className="text_ellipsis">{value.title}</span>
                      )}
                  </li>
                );
              })}
            </ul>
          </div>
          <AnchorLink href={lbUrl} className="read_more_open">
            {siteConfig.locale.readmore}...
          </AnchorLink>
        </div>
      </React.Fragment>
    ) : (
        ""
      );
  }
}

export default LiveBlog;
