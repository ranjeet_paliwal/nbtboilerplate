import React from "react";
import AnchorLink from "../AnchorLink";
import ImageCard from "../ImageCard/ImageCard";

const Banner = props => {
  // console.log("Banner", props);
  const {
    data: { imageid, classname, status, wu },
    divTag,
  } = props;
  const ParentTag = divTag ? "div" : "li";
  return (
    <ParentTag className="banner-image news-card horizontal">
      <AnchorLink href={wu}>
        <ImageCard size="bigimage" msid={imageid} />
      </AnchorLink>
    </ParentTag>
  );
};

export default Banner;
