/* eslint-disable camelcase */
/* eslint-disable no-cond-assign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/no-danger */
/* eslint-disable no-return-assign */
/* eslint-disable prefer-const */
/* eslint-disable indent */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { _isCSR, _getStaticConfig } from "../../utils/util";
import Ads_module from "../lib/ads/index";
import CTN_module from "../lib/ads/ctnAds";
import globalconfig from "../../globalconfig";
const siteConfig = _getStaticConfig();

class AdCard extends Component {
  componentDidMount() {
    const _this = this;

    let { adtype, pagetype, renderall, pwameta, mstype, artilceUrl } = _this.props;
    pagetype = pagetype || "others";
    const section = pwameta ? pwameta.subsectitle1 : "";
    const subsection = pwameta ? pwameta.subsectitle2 : "";

    if (pwameta) Ads_module.setSectionDetail(pwameta);
    if (_isCSR() && _this.adElem) {
      window.setTimeout(() => {
        adtype == "ctn"
          ? CTN_module.render({ pathname: artilceUrl }, _this.adElem)
          : renderall
          ? Ads_module.render({ pathname: artilceUrl })
          : Ads_module.render({ pathname: artilceUrl }, _this.adElem, section, subsection);
      }, 1000);
    }

    // For ESI Ads
    // if (_isCSR()) {
    //   if (document.getElementById("esi_col")) {
    //     this.getScriptTags(document.getElementById("esi_col").innerText, document.getElementById("esi_col"));
    //   }
    //   if (document.getElementById("esi_ab")) {
    //     this.getScriptTags(document.getElementById("esi_ab").innerText, document.getElementById("esi_ab"));
    //   }

    //   const esiParents = Array.prototype.slice.call(document.querySelectorAll("#esi_parent"));

    //   if (esiParents != undefined) {
    //     esiParents.forEach((element, index) => {
    //       // Get inner data sting and create HTML at run time
    //       const esiData = element.querySelector("#esi_con");
    //       const text = esiData.innerText;
    //       const esicon = document.createElement("div");
    //       esicon.innerHTML = text;

    //       const parent = document.getElementById("esi_parent");
    //       element.insertBefore(esicon, element.lastChild);
    //       element.removeAttribute("id");

    //       _this.getScriptTags(text, esiData);
    //     });
    //   }
    // }
  }

  // Convert String to HTML object
  getScriptTags(str, ele) {
    const reg = /<script\b[^>]*>([\s\S]*?)<\/script>/gm;
    let match;
    while ((match = reg.exec(str))) {
      const src = document.createElement("script");
      src.innerHTML = match[1];
      ele.innerHTML = "";
      ele.appendChild(src);
      ele.removeAttribute("id");
      ele.style.display = "block";
    }
  }

  // @adtype for ctn/dfp
  // @mstype type of ctn/dfp as key
  // @elemtype for li/div
  // @ctnstyle only for ctn , diff btw inline & others
  render() {
    const _this = this;
    let {
      adtype,
      mstype,
      elemtype,
      ctnstyle,
      content,
      query,
      // esiad,
      pagetype,
      classtype,
      className,
      rendertype,
      artilceUrl,
    } = _this.props;

    classtype = classtype || "";
    className = className || "";
    pagetype = pagetype || globalconfig.ad_pagetype;
    // const slotname = mstype || ctnstyle;
    // check for ESI
    // const showEsi = !!(process.env.SITE != "mt" && process.env.SITE != "vk" && esiad);
    // const esi = !!(content && content.cad && content.cad != undefined && showEsi);
    // const slots =
    //   siteConfig.ads.ctnslots && siteConfig.ads.ctnslots[pagetype] ? siteConfig.ads.ctnslots[pagetype] : null;
    // const msid =
    //   pagetype == "photoshow" && content && content.it && content.it.id
    //     ? content.it.id
    //     : content && content.id
    //     ? content.id
    //     : "";

    // define dfpSlot
    let dfpSlot =
      adtype === "dfp"
        ? siteConfig.ads.dfpads[pagetype] && siteConfig.ads.dfpads[pagetype][mstype]
          ? siteConfig.ads.dfpads[pagetype][mstype]
          : siteConfig.ads.dfpads.others[mstype]
        : {};

    // if (mstype === "atf" && isMobilePlatform()) {
    //   if (_isCSR() && window.newAtfAdSize && window.newAtfAdSize !== []) {
    //     dfpSlot.size = JSON.parse(window.newAtfAdSize);
    //   } else if (globalconfig.newAtfAdSize) {
    //     dfpSlot.size = globalconfig.newAtfAdSize;
    //   }
    // }
    let ctnid;
    let ctnAdSlotId;
    let randNum;
    if (adtype === "ctn" && rendertype === "prerender") {
      ctnid = siteConfig.ads.ctnads[mstype] ? siteConfig.ads.ctnads[mstype] : siteConfig.ads.ctnads.ctnHome;
      randNum = Math.floor(Math.random() * 100001);
      ctnAdSlotId = `div-clmb-ctn-${ctnid}-${randNum}`;
    }
    let overrideWrapperClass = "";
    // if (adtype === "dfp" && mstype === "atf" && isMobilePlatform()) {
    //   overrideWrapperClass = `ad1 ${mstype} ${classtype} ${className}`;
    // }
    return (
      // this for CSR ESI only
      // esi == true ? (
      //   <div data-exclude="amp">
      //     {content.cad._col_script && content.cad._col_ab_call ? (
      //       <div>
      //         {window != undefined && window.esi != true ? (
      //           <span id="esi_col" style={{ display: "none" }}>
      //             {content.cad._col_script}
      //           </span>
      //         ) : null}
      //         {window != undefined && window.colab == true ? (
      //           <span id="esi_ab" style={{ display: "none" }}>
      //             {content.cad._col_ab_call}
      //           </span>
      //         ) : null}
      //       </div>
      //     ) : null}
      //     <div id="esi_parent">
      //       <div id="esi_con" style={{ display: "none" }}>
      //         {content.cad[`${siteConfig.ads.ctnslots[slotname]}~${msid}~0`]}
      //       </div>
      //     </div>
      //   </div>
      // )
      // : // this for SSR ESI only
      // (query && query.wesi == 1 && showEsi && slots != undefined) ||
      //   (_isCSR() && window && window.isesi == 1 && showEsi && slots != undefined) ? (
      //   <div className="esicon" data-exclude="amp">
      //     {/* Ftech */}

      //     {slotname == "atf" ? EsiAd({ adunit: slotname }).ssr({ slots, type: "fetch" }) : null}

      //     {/* {slotname == 'ctnshow' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'fetch'}) : null} */}

      //     {/* Draw */}
      //     {EsiAd({ adunit: slotname }).ssr({ slots, type: "slot" })}
      //     {/* {EsiAd({adunit:slotname}).adComment({type:'slot'})} */}

      //     {_isCSR() && window != undefined ? (window.esi = true) : false}
      //   </div>
      // )
      // JS Ads
      adtype && adtype === "ctn" ? (
        rendertype === "prerender" &&
        (typeof globalconfig.isLandingPage === "undefined" || globalconfig.isLandingPage === true) ? (
          _isCSR() ? (
            <li suppressHydrationWarning dangerouslySetInnerHTML={{ __html: "" }} />
          ) : elemtype && elemtype === "li" ? (
            <li
              id={ctnAdSlotId}
              ctn-style={ctnstyle || mstype}
              data-slot={ctnid}
              data-slotname={mstype}
              data-plugin="ctn"
              data-section="0"
              data-position={randNum}
              className={`ad1 ${mstype} ${classtype} colombia`}
            />
          ) : (
            <div
              id={ctnAdSlotId}
              ctn-style={ctnstyle || mstype}
              data-slot={ctnid}
              data-slotname={mstype}
              data-plugin="ctn"
              data-section="0"
              data-position={randNum}
              className={`ad1 ${mstype} ${classtype} colombia`}
            />
          )
        ) : elemtype && elemtype === "li" ? (
          <li
            ctn-style={ctnstyle || mstype}
            data-slot={mstype}
            data-slotname={mstype}
            data-plugin="ctn"
            className={`ad1 ${mstype} ${classtype}`}
            ref={ele => (_this.adElem = ele)}
          />
        ) : (
          <div
            ctn-style={ctnstyle || mstype}
            data-slot={mstype}
            data-slotname={mstype}
            data-plugin="ctn"
            className={`ad1 ${mstype} ${classtype}`}
            ref={ele => (_this.adElem = ele)}
          />
        )
      ) : adtype && adtype === "dfp" && rendertype === "prerender" ? (
        <div className={overrideWrapperClass}>
          {_isCSR() ? (
            <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: "" }} />
          ) : dfpSlot ? (
            <div
              className={overrideWrapperClass ? "prerender" : `ad1 ${mstype} prerender ${classtype} ${className}`}
              // commenting the below lines for critical(first) view
              // style={{ display: "none" }}
              data-adtype={mstype}
              id={dfpSlot.id}
              data-size={JSON.stringify(dfpSlot.size)}
              // data-size="[[320,50]]"
              data-path={dfpSlot.name}
            />
          ) : null}
        </div>
      ) : elemtype && elemtype === "li" ? (
        <li className={className}>
          <div
            className={`ad1 ${mstype} ${classtype}`}
            // commenting the below lines for critical(first) view
            // style={{ display: "none" }}
            data-adtype={mstype}
            ref={ele => (_this.adElem = ele)}
          />
        </li>
      ) : (
        <div
          className={`ad1 ${mstype} ${classtype} ${className}`}
          // commenting the below lines for critical(first) view
          // style={{ display: "none" }}
          data-adtype={mstype}
          ref={ele => (_this.adElem = ele)}
        />
      )

      // <li data-adtype={mstype} className={`ad1 ${mstype} ${classtype} ${className}`} ref={(ele) => _this.adElem = ele}></li>
      // :
      // <div data-adtype={mstype} className={`ad1 ${mstype} ${classtype}`} ref={(ele) => _this.adElem = ele} style={_this.props.style}></div>
    );
  }
}

AdCard.propTypes = {
  item: PropTypes.object,
};

export default AdCard;
