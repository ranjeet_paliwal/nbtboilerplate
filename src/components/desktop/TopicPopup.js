import React from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import { _getStaticConfig, _isCSR } from "../../utils/util";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import AdCard from "../common/AdCard";
import { fetchTopicsData } from "../../actions/topics/topics";
import { AnalyticsGA } from "../../components/lib/analytics/index";
import { refreshAds } from "../lib/ads/index";
import AnchorLink from "../common/AnchorLink";

const siteConfig = _getStaticConfig();
let timeout;
class TopicPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { adrendered: false };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.topic !== "" && nextProps.topic !== this.props.topic) {
      if (this.state.adrendered === false) {
        this.setState({ adrendered: true });
      } else {
        refreshAds(["topicmrec1"], true);
      }
      const { topic, dispatch, router } = nextProps;
      fetchTopicsData(topic, dispatch, router).then(data => {
        // Check for data type of topicData
        if (data && data.stry instanceof Array) this.coordinate(nextProps.topic);
      });
    }
  }

  coordinate(topic) {
    const width = 620;
    const height = 270;
    const popup = document.querySelector(".topicpopup");
    const selectedTopic = document.querySelector(".selectedTopic");
    let x;
    let y;
    if (selectedTopic) {
      x = selectedTopic.getBoundingClientRect().x;
      y = selectedTopic.getBoundingClientRect().y;
    }
    if (popup && selectedTopic) {
      popup.style.display = "block";
      AnalyticsGA.pageview(`/topics/${topic}`);
      const maxx = window.screen.availWidth; // height of the space available for web content on the screen
      const maxy = 600; // weight of the space available for web content on the screen

      if (y + height > maxy && x + width > maxx) {
        // extreme bottom-right, invert popup upside and to the left)
        popup.style.bottom = `${maxy - y - 5}px`;
        popup.style.top = `unset`;
        popup.style.left = `unset`;
        popup.style.right = `${x + 20}px`;
      } else if (y + height > maxy) {
        // extreme bottom, invert popup upside)
        popup.style.bottom = `${maxy - y - 5}px`;
        popup.style.top = `unset`;
        popup.style.left = `${x}px`;
        popup.style.right = `unset`;
      } else if (x + width > maxx) {
        popup.style.left = `unset`;
        popup.style.right = `${x + 10}px`;
        popup.style.bottom = `unset`;
        popup.style.top = `${y + 20}px`;
      } else {
        popup.style.bottom = `unset`;
        popup.style.top = `${y + 20}px`;
        popup.style.left = `${x}px`;
        popup.style.right = `unset`;
      }
    }
  }

  closePopUp = () => {
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(() => {
      this.props.parentObj.setState({ topicVal: "" });
    }, 1500);
  };

  render() {
    const topicData = this.props.topic in this.props.topicsData && this.props.topicsData[this.props.topic];
    let filteredData = [];
    if (topicData && topicData.stry && topicData.stry.length > 0) {
      filteredData = topicData.stry.filter(stry => stry.id !== this.props.id);
      if (filteredData.length > 3) {
        filteredData = filteredData.slice(0, 3);
      }
    }
    return _isCSR()
      ? ReactDOM.createPortal(
          <div
            className="topicpopup"
            style={{ display: filteredData.length > 0 ? "block" : "none" }}
            onMouseLeave={() => this.closePopUp()}
          >
            <div className="lft">
              <h3>More on this topic</h3>
              <ul className="row mr0">
                {filteredData.map((data, index) => {
                  return (
                    <li className={index === 2 ? "col12 topic_article" : "col6 topic_article"}>
                      <AnchorLink href={`${data.link}`} isrelative target="_blank" className="img_wrap">
                        <ImageCard msid={data.img} size="smallthumb" title={data.alttitle} alt={data.alttitle} />
                        <span className="con_wrap">
                          <span className="text_ellipsis">{data.title}</span>
                        </span>
                      </AnchorLink>
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="rgt topics_ads">
              {/*<h3>Featured Advertising</h3>*/}
              {this.state.adrendered && <AdCard adtype="dfp" mstype="topicmrec1" />}
            </div>
          </div>,
          document.getElementById("position-fixed-floater"),
        )
      : null;
  }
}

function mapStateToProps(state) {
  return {
    ...state.topics,
  };
}

export default connect(mapStateToProps)(TopicPopup);
