import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import AnchorLink from "../common/AnchorLink";
import { truncateStr } from "../../utils/util";
import ImageCard from "../common/ImageCard/ImageCard";

//FIXME: Convert loader to component and import
// import HoverLoader from "../../Loaders/HoverLoader";

class GoldHoverSection extends PureComponent {
  constructor(props) {
    super(props);
    const { data } = props;
    const currentData = data && Array.isArray(data.section) && data.section.length > 0 ? data.section[0] : null;
    this.state = {
      currentData,
      currentSectionOpened: 0,
    };
    this.renderCurrentSection = this.renderCurrentSection.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // To render first section after data finishes loading
    const { data } = nextProps;
    const dataFetched = Boolean(data && data && Array.isArray(data.section) && data.section.length > 0);
    if (!prevState.currentData && dataFetched) {
      return { currentData: data.section[0], currentSectionOpened: 0 };
    }
    return prevState;
  }

  renderCurrentSection(index) {
    const { data } = this.props;
    let currentData = null;
    if (data && Array.isArray(data.section) && index < data.section.length) {
      currentData = data.section[index];

      currentData.items = currentData.items.length > 8 ? currentData.items.slice(0, 8) : currentData.items;
    }
    this.setState({
      currentData,
      currentSectionOpened: index,
    });
  }

  render() {
    const { hideCurrentSection, data, secName } = this.props;
    const { currentSectionOpened, currentData } = this.state;

    // FIXME: Convert to common nav loading component
    if (!data) {
      return (
        <div className="hover_content goldnavloader">
          <div className="wdt_loading"><span className="spinner_circle"></span></div>
        </div>
      );
    }
    return (
      <div className="hover_content" onMouseLeave={hideCurrentSection}>
        <div className="goldcontent">
          <LeftNavSection
            data={data && data.section}
            currentSectionOpened={currentSectionOpened}
            renderCurrentSection={this.renderCurrentSection}
          />
          <div id="nav_gold" className="nav_nbtgold">
            <ul id={currentData && currentData.id ? currentData.id : ""}>
              {currentData && Array.isArray(currentData.items)
                ? currentData.items.map(goldData => (
                    <li key={goldData.imageid}>
                      <AnchorLink
                        target="_blank"
                        className="thumb"
                        title={truncateStr(goldData.hl, 40)}
                        href={goldData.override}
                      >
                        <span className="img_wrap">
                          <ImageCard
                            alt={goldData.hl}
                            msid={goldData.imageid}
                            size="smallthumb"
                            className="candidate_img"
                          />
                        </span>
                        <span className="con_wrap">
                          <span className="text_ellipsis">{goldData.hl ? truncateStr(goldData.hl, 50) : null}</span>
                        </span>
                      </AnchorLink>
                    </li>
                  ))
                : null}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

GoldHoverSection.propTypes = {
  hideCurrentSection: PropTypes.func,
  data: PropTypes.object,
  secName: PropTypes.string,
};

const LeftNavSection = props => {
  const { data, renderCurrentSection, currentSectionOpened } = props;
  return (
    <ul className="top_sub_menu">
      {Array.isArray(data) &&
        data.map((goldNavLeftData, idx) => (
          <li
            className={currentSectionOpened === idx ? "current" : ""}
            key={goldNavLeftData.id}
            onMouseEnter={() => renderCurrentSection(idx)}
          >
            <AnchorLink href={goldNavLeftData.override}>{goldNavLeftData.secname}</AnchorLink>
          </li>
        ))}
    </ul>
  );
};

LeftNavSection.propTypes = {
  data: PropTypes.array,
  renderCurrentSection: PropTypes.func,
  currentSectionOpened: PropTypes.number,
};

export default GoldHoverSection;
