import React, { Component } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
// import LoginControl from "./LoginControl";
import AdCard from "../common/AdCard";
import { _getStaticConfig, _isCSR } from "../../utils/util";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import SearchCard from "../common/SearchCard";
import Logo from "../common/logo";
import "../common/css/desktop/footer.scss";
const siteConfig = _getStaticConfig();

const ASSET_PATH = process.env.ASSET_PATH || "/";
const logo = siteConfig.logo;

class DesktopFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <footer id="footerContainer" className="wrap" style={{ transform: "translateY(0px)" }}>
        <div className="top_social_links">
          <div className="wrap">
            <div className="first">
              <div className="logo">
                <Logo />
              </div>
            </div>
            <div className="second">
              <a target="_blank" href="https://www.facebook.com/navbharattimes">
                <span className="nbt_gadget_sprite icon_facebook"></span>
              </a>
              <a target="_blank" href="https://twitter.com/navbharattimes">
                <span className="nbt_gadget_sprite icon_twitter"></span>
              </a>
            </div>
            <div className="third">
              <b>
                हमेशा कनेक्टेड रहें
                <br />
                नवभारत टाइम्स की ऐप
              </b>
              <a
                href="https://play.google.com/store/apps/details?id=com.nbt.reader"
                target="_blank"
                className="nbt_gadget_sprite icon_android"
                title=""
                rel="nofollow noopener"
              >
                android
              </a>
              <a
                href="https://itunes.apple.com/us/app/navbharat-times/id656093141?ls=1&amp;mt=8"
                target="_blank"
                className="nbt_gadget_sprite icon_ios"
                title=""
                rel="nofollow noopener"
              >
                ios
              </a>
            </div>
          </div>
        </div>
        <div className="headingsWithOtherSites">
          <div className="wrap">
            <div className="inside-links">
              <h4>खबरें एक झलक में</h4>
              <a title="" rel="" href="https://navbharattimes.indiatimes.com/india/articlelist/1564454.cms">
                भारत
              </a>
              <a href="https://navbharattimes.indiatimes.com/sports/cricket.cms" target="_blank" title="" rel="">
                खेल
              </a>
              <a href="https://blogs.navbharattimes.indiatimes.com/" target="_blank" title="" rel="nofollow noopener">
                NBT ब्लॉग
              </a>
              <a title="" rel="" href="https://navbharattimes.indiatimes.com/metro/delhi/articlelist/4836708.cms">
                दिल्ली
              </a>
              <a
                href="https://navbharattimes.indiatimes.com/movie-masti/movies/2279793.cms"
                target="_blank"
                title=""
                rel=""
              >
                मूवी-मस्ती
              </a>
              <a
                href="https://readerblogs.navbharattimes.indiatimes.com/"
                target="_blank"
                title=""
                rel="nofollow noopener"
              >
                अपना ब्लॉग
              </a>
              <a title="" rel="" href="https://navbharattimes.indiatimes.com/metro/mumbai/articlelist/5722181.cms">
                मुंबई
              </a>
            </div>
            <div className="other-sites">
              <h4>हमारी दूसरी साइट्स</h4>
              <a href="https://timesofindia.indiatimes.com/" target="_blank" title="" rel="nofollow noopener">
                Times of India
              </a>
              <a href="https://economictimes.indiatimes.com/" target="_blank" title="" rel="nofollow noopener">
                Economic Times
              </a>
              <a href="https://maharashtratimes.com//" target="_blank" title="" rel="nofollow noopener">
                Marathi News
              </a>
              <a href="https://eisamay.indiatimes.com/" target="_blank" title="" rel="nofollow noopener">
                Bengali News
              </a>
              <a href="https://vijaykarnataka.indiatimes.com/" target="_blank" title="" rel="nofollow noopener">
                Kannada News
              </a>
              <a href="https://www.iamgujarat.com/" target="_blank" title="" rel="nofollow noopener">
                Gujarati News
              </a>
              <a href="https://tamil.samayam.com/" target="_blank" title="" rel="nofollow noopener">
                Tamil News
              </a>
              <a href="https://telugu.samayam.com/" target="_blank" title="" rel="nofollow noopener">
                Telugu News
              </a>
              <a href="https://malayalam.samayam.com/" target="_blank" title="" rel="nofollow noopener">
                Malayalam News
              </a>
            </div>
          </div>
        </div>
        <div className="popluarCategories">
          <div className="wrap">
            <ul>
              <li className="wdth25">
                <h3>Top Stories</h3>
                <a
                  href="http://172.24.88.21:9338/tech/laptops/filters/price-range=1-to-30000"
                  target="_blank"
                  title=""
                  rel=""
                >
                  ₹30000 से कम के लैपटॉप
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/filters/price-range=1-to-15000"
                  target="_blank"
                  title=""
                  rel=""
                >
                  ₹15000 से कम के फोन
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/filters/price-range=1-to-20000"
                  target="_blank"
                  title=""
                  rel=""
                >
                  ₹20000 से कम के फोन
                </a>
              </li>
              <li className="wdth25">
                <h3>Top Categories</h3>
                <a href="http://172.24.88.21:9338/tech/upcoming-mobile-phones" target="_blank" title="" rel="">
                  आने वाले मोबाइल
                </a>
                <a href="http://172.24.88.21:9338/tech/tablets" target="_blank" title="" rel="">
                  ऑनलाइन खरीदें टैबलेट
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/filters/network=4G"
                  target="_blank"
                  title=""
                  rel=""
                >
                  4G मोबाइल
                </a>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/filters/sim=Dual" target="_blank" title="" rel="">
                  ड्यूल सिम मोबाइल फ़ोन
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/filters/os=Android"
                  target="_blank"
                  title=""
                  rel=""
                >
                  ऐंड्राइड मोबाइल फ़ोन
                </a>
              </li>
              <li>
                <h3>Trending Stories</h3>
                <a
                  href="https://navbharattimes.indiatimes.com/tech/gadgets-news/vivo-z1x-with-triple-rear-camera-launched-in-india-know-price-and-specifications/articleshow/71005939.cms"
                  target="_blank"
                  title=""
                  rel=""
                >
                  vivo z1x price
                </a>
                <a
                  href="https://navbharattimes.indiatimes.com/tech/gadgets-news/nokia-110-nokia-2720-flip-nokia-800-tough-nokia-6-2-and-nokia-7-2-unveiled-know-price-and-feature/articleshow/71002235.cms"
                  target="_blank"
                  title=""
                  rel=""
                >
                  nokia upcoming smartphone
                </a>
                <a
                  href="https://navbharattimes.indiatimes.com/tech/gadgets-news/reliance-jio-gigafiber-plans-price-free-tv-set-top-box-details/articleshow/70996291.cms"
                  target="_blank"
                  title=""
                  rel=""
                >
                  jio fibre data plans
                </a>
                <a
                  href="https://navbharattimes.indiatimes.com/tech/tech-photogallery/reliance-jio-gigafiber-commercial-launch/photoshow/70933099.cms"
                  target="_blank"
                  title=""
                  rel=""
                >
                  Reliance Jio GigaFiber
                </a>
                <a
                  href="https://navbharattimes.indiatimes.com/tech/gadgets-news/vivo-v17-pro-will-come-with-dual-pop-up-selfie-camera/articleshow/70992484.cms"
                  target="_blank"
                  title=""
                  rel=""
                >
                  Vivo V17 Pro
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <h3>मोबाइल फोन</h3>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Moto" target="_blank" title="" rel="">
                  MOTO के मोबाइल
                </a>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Apple" target="_blank" title="" rel="">
                  ऐपल के iPhone
                </a>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Lenovo" target="_blank" title="" rel="">
                  लेनोवो के मोबाइल
                </a>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Intex" target="_blank" title="" rel="">
                  इंटेक्स के मोबाइल
                </a>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Nokia" target="_blank" title="" rel="">
                  नोकिया के मोबाइल
                </a>
              </li>
              <li>
                <h3>Popular Gadgets</h3>
                <a href="http://172.24.88.21:9338/tech/laptops/Dell" target="_blank" title="" rel="">
                  डेल के लैपटॉप
                </a>
                <a href="http://172.24.88.21:9338/tech/laptops/HP" target="_blank" title="" rel="">
                  एचपी के लैपटॉप
                </a>
                <a href="http://172.24.88.21:9338/tech/laptops/Lenovo" target="_blank" title="" rel="">
                  लेनोवो के लैपटॉप
                </a>
                <a href="http://172.24.88.21:9338/tech/laptops/Acer" target="_blank" title="" rel="">
                  ऐसर के लैपटॉप
                </a>
              </li>
              <li>
                <h3>Latest Mobiles</h3>
                <a href="http://172.24.88.21:9338/tech/mobile-phones/Honor-View-20" target="_blank" title="" rel="">
                  Honor View 20
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/Xiaomi-Redmi-Note-6-Pro"
                  target="_blank"
                  title=""
                  rel=""
                >
                  Redmi Note 6 Pro
                </a>
                <a
                  href="http://172.24.88.21:9338/tech/mobile-phones/Samsung-Galaxy-J4-Core"
                  target="_blank"
                  title=""
                  rel=""
                >
                  Samsung Galaxy J4 Core
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="con-copy-right">
          <div className="wrap">
            <div className="times-links">
              <a rel="nofollow noopener" href={`${siteConfig.mweburl}aboutus.cms`} target="_blank">
                About Us
              </a>
              <a rel="nofollow noopener" href={`${siteConfig.mweburl}termsandcondition.cms`} target="_blank">
                Terms of use
              </a>
              <a rel="nofollow noopener" href={siteConfig.footerDesktopLink} target="_blank">
                {" "}
                Desktop Version{" "}
              </a>
            </div>
            <div className="other-info">
              Copyright © 2019 Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights
              <a href="https://timescontent.com/" target="_blank" title="" rel="">
                Times Syndication Service
              </a>
              <br />
              This site is best viewed with Internet Explorer 6.0 or higher Firefox 2.0 or higher at a minimum screen
              resolution of 1024x768
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default DesktopFooter;
