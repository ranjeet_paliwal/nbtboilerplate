import React, { PureComponent } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
// import LoginControl from "./LoginControl";
import AdCard from "../common/AdCard";
import {
  filterAlaskaData,
  getAlaskaSections,
  _isCSR,
  _getStaticConfig,
  isMobilePlatform,
  getAlaskaLinks,
  isBusinessSection,
  isGadgetPage,
  isInternationalUrl,
  getCountryCode,
} from "../../utils/util";
const siteConfig = _getStaticConfig();
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";

import Logo from "../common/logo";
// import Navigation from "../../components/common/Navigation";
import NavBar from "../../components/common/NavBar/NavBar";
import VideoItem from "../../components/common/VideoItem/VideoItem";
// import CoronaBanner from "../common/coronabanner/coronabanner";
import "../common/css/desktop/header.scss";
// import HoverSection from "./HoverSection";
import AnchorLink from "../common/AnchorLink";
import SocialMediaChannels from "../../components/common/SocialMediaChannels/SocialMediaChannels";
import LoginControl from "../common/LoginControl";
import fetch from "../../utils/fetch/fetch";
import GoldHoverSection from "./GoldHoverSection";
import SvgIcon from "../common/SvgIcon";
import AnchorLinkGN from "../common/AnchorLinkGN";
import gadgetsConfig from "../../utils/gadgetsConfig";
import { internationalconfig } from "../../utils/internationalpageConfig";
import SelectCity from "../common/SelectCity/SelectCity";
import GnSearchBox from "../common/GnSearchBox/GnSearchBox";
import LangDropDown from "../common/LangDropDown";
import { navConfig } from "../../components/common/Gadgets/GnNavBar";
const footerConfig = _getStaticConfig("footer");

const ASSET_PATH = process.env.ASSET_PATH || "/";
const countryCode = getCountryCode();
const siteName = process.env.SITE;

// console.log("navConfig", navConfig);

// const logo = siteConfig.logo;

// Need to remove this once data is inserted in alaska for gadget page

class DesktopHeader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { photo: false, video: false, datetime: "", gold: false, goldData: null, serchBoxVisible: false };
    this.currentNav = {};
    this.fetchingGoldData = false;
    this.isGulf = false;
  }

  componentDidMount() {
    const { router } = this.props;
    if (typeof document && typeof window) {
      const fixedMenu = document.querySelector("#fixedMenu");
      const top = fixedMenu.offsetTop || 92;

      document.addEventListener("scroll", () => {
        if (window.scrollY > top) {
          if (fixedMenu.classList && !fixedMenu.classList.contains("fixed")) {
            fixedMenu.classList.add("fixed");
          }
        } else {
          fixedMenu.classList.remove("fixed");
        }
      });
      if (window && window.location && window.location.href.indexOf("/gulf") > -1) {
        this.isGulf = true;
      } else {
        this.isGulf = false;
      }
    }

    this.getDateTime();
  }

  getDateTime() {
    const currentDateTimeStr = new Date().toString();
    const currentDateTimeArr = currentDateTimeStr.split(" ");
    const day = currentDateTimeArr[2];
    const month = currentDateTimeArr[1];
    const year = currentDateTimeArr[3];
    const timeStr = currentDateTimeArr[4];
    const timeStrArr = timeStr.split(":");
    const finalDate = `${day} ${month} ${year} ${timeStrArr[0]}${timeStrArr[1]} hrs IST`;
    this.setState({
      datetime: finalDate,
    });
    // console.log(finalDate);
  }

  mouseOverHandler = type => {
    // Fetch gold data in CSR if not fetched already , or not currently fetching
    if (type === "gold" && !this.state.goldData && !this.fetchingGoldData) {
      fetch(siteConfig.gold.goldNavigationApi).then(goldData => {
        this.setState({ goldData });
        this.fetchingGoldData = false;
      });
      this.fetchingGoldData = true;
    }
    // Open the respective hover section
    this.setState({
      [type]: true,
    });
  };

  mouseOutHandler = type => {
    this.setState({
      [type]: false,
    });
  };

  toggleSearchBox = () => {
    this.setState({ serchBoxVisible: !this.state.serchBoxVisible });
  };

  render() {
    const {
      subsec1,
      parentId,
      secId,
      hierarchylevel,
      header,
      pagetype,
      otherSitesInfo,
      topVideos,
      topVideoURL,
      location,
      router,
      config,
    } = this.props;
    const { photo, video, datetime, gold, serchBoxVisible } = this.state;
    let parentSections = "";
    let childSections = "";

    const alaskaDataCopy = JSON.parse(JSON.stringify(header && header.alaskaData));
    const rootSections = getAlaskaSections(alaskaDataCopy);
    const headerObj = filterAlaskaData(alaskaDataCopy, ["pwaconfig", "headerlinks"], "label");
    const headerLinks = getAlaskaLinks(headerObj);
    //const isBusinessSec = location && location.pathname && location.pathname.includes("/business/");
    // console.log("headerLinks", headerLinks);
    // const techHomePage = router && router.location.pathname && router.location.pathname == "/tech" ? true : false;
    // console.log("a1", filterAlaskaData(header && header.alaskaData, secId));
    // console.log("a2", filterAlaskaData(header && header.alaskaData, parentId));

    // console.log("childSectionslocationlocation", location);

    if (pagetype != "home") {
      if (secId) {
        const childSectionsObj = filterAlaskaData(alaskaDataCopy, secId);
        childSections = getAlaskaSections(childSectionsObj, true);
        if (childSections.length > 0) {
          parentSections = JSON.parse(JSON.stringify(childSections));
        } else if (parentId) {
          const objSectionInfo = filterAlaskaData(alaskaDataCopy, parentId);
          parentSections = getAlaskaSections(objSectionInfo, true);
        }
      }
    }

    //  console.log("parentSections", parentSections);
    // siteConfig: { fb, twitter, applist, rss, youtubeUrl },

    const navHoverData = header && header.navHoverData;

    const dropdownconfig = internationalconfig[siteName].dropdownArray;
    let intPathName = router && router.location && router.location.pathname;
    intPathName = pagetype != "home" ? "/" : intPathName == "/default.cms" ? "/" : intPathName;
    const HeadingTag = "h2";
    const gadgetPage = isGadgetPage({ pagetype });
    return (
      <div className="headerContainer">
        <div className="ieWrap">
          <a href="https://www.google.com/chrome/" target="_blank">
            {siteConfig.locale.ieText}
          </a>
          {/* <span>{siteConfig.locale.closeText}</span> */}
        </div>
        {/* <div className="top_banner">
          <a
            pg="earpannel1"
            target="_blank"
            href={`${siteConfig.weburl}/coronavirus/trending/${siteConfig.coronaId}.cms?utm_source=masthead&amp;utm_medium=referral&amp;utm_campaign=coronavirus`}
          >
           <CoronaBanner />
          </a>
        </div> */}
        <header className="header-desktop">
          <div className="logo">
            <Logo config={config} />
            <LangDropDown location={location} pagetype={pagetype} />
          </div>
          {/* Top Nav */}
          <div className="top_nav">
            <ul className="extranav">
              {/* Internation Pages Dropdown */}
              {process.env.SITE == "nbt" ||
              process.env.SITE == "mt" ||
              process.env.SITE == "mly" ||
              process.env.SITE == "eisamay" ? (
                <li className="other-sites">
                  <span className="head">
                    <i>
                      {_isCSR() && (
                        <SvgIcon
                          name={
                            isInternationalUrl(router)
                              ? internationalconfig[siteName].urldropdowm[router.location.pathname].iconName
                              : "flag-india"
                          }
                        />
                      )}
                      {isInternationalUrl(router)
                        ? internationalconfig[siteName].urldropdowm[router.location.pathname].countryName
                        : "IND"}
                    </i>
                  </span>
                  <ul className="restLang">
                    {dropdownconfig &&
                      dropdownconfig.length > 0 &&
                      dropdownconfig.map(item => {
                        if (item.urlPath !== intPathName) {
                          return (
                            <li key={item.countryName + item.iconName}>
                              <AnchorLink key={item.countryName} href={item.urlPath}>
                                <SvgIcon name={item.iconName} />
                                {siteName == "mly" && item.countryName == "UAE" ? "GULF" : item.countryName}
                              </AnchorLink>
                            </li>
                          );
                        }
                      })}
                  </ul>
                </li>
              ) : null}
              <li
                key={"videogallery"}
                className="videogallery"
                onMouseOver={this.mouseOverHandler.bind(this, "video")}
                //onMouseOut={this.mouseOutHandler.bind(this, "video")}
              >
                <AnchorLink href={topVideoURL} onClick={this.mouseOutHandler.bind(this, "video")}>
                  <span name="videoGallery" className="video-play-button">
                    <span></span>
                  </span>
                </AnchorLink>
                {video && (
                  <div className="hover_content">
                    <ul className="top-videos">
                      {topVideos &&
                        Array.isArray(topVideos) &&
                        topVideos.map(item => {
                          return (
                            <li
                              key={item.id}
                              className="news-card vertical video"
                              onClick={this.mouseOutHandler.bind(this, "video")}
                            >
                              <VideoItem item={item} />
                            </li>
                          );
                        })}
                    </ul>
                  </div>
                )}
              </li>

              <li
                key={"photogallery"}
                className="photogallery"
                onMouseOver={this.mouseOverHandler.bind(this, "photo")}
                // onMouseOut={this.mouseOutHandler.bind(this, "photo")}
              >
                {/* hardcoded https:// is not a good practice, it is done to avoid any mishappening with exisiting code, ideally it should be added in same CONSTANT photogallerydomain */}
                <AnchorLink href={`https://${siteConfig.photogallerydomain}`}>
                  <span name="photoGallery" className="photogallery_icon">
                    Photogallery
                  </span>
                </AnchorLink>
                {photo && (
                  <div className="hover_content">
                    <iframe
                      title="photogallery frame"
                      width="651"
                      height="386"
                      hspace="0"
                      vspace="0"
                      scrolling="no"
                      align="center"
                      src={`https://tamil.samayam.com/topgalleies_tamil_pwa.cms?channel=${process.env.SITE}`}
                      // we will be using tamil frame for all channels.
                    />
                  </div>
                )}
              </li>
              {process.env.SITE === "eisamay" || process.env.SITE === "nbt" ? (
                <li
                  key={"wdt_gold"}
                  className={`wdt_gold ${siteConfig.gold.logoClass}`}
                  onMouseOver={e => this.mouseOverHandler("gold")}
                  // onMouseOut={this.mouseOutHandler.bind(this, "gold")}
                >
                  <AnchorLink href={siteConfig.gold.redirectUrl} target="_blank" title="" rel="">
                    <span className="goldlogo">GOLD</span>
                  </AnchorLink>
                  {gold ? <GoldHoverSection data={this.state.goldData} /> : null}
                </li>
              ) : null}

              <li className="dropDown_city">
                <SelectCity sectionId={siteConfig.pages.stateSection} parentComponent="headericon" />
              </li>

              <React.Fragment>
                {headerLinks.length > 0 &&
                  headerLinks.map(item => {
                    return (
                      <li className={item.class || false} key={item.link}>
                        <a href={item.link} target="_blank">
                          {item.label}
                        </a>
                      </li>
                    );
                  })}

                {/* <li className="txt-link">
                  <a title="Bihar Election" href="/elections/assembly-elections/bihar/news/articlelist/74439903.cms">
                    बिहार चुनाव
                  </a>
                </li>
                <li className="apanaBazar">
                  <a title="Apna Bazar" href="/apna-bazaar/articlelist/71186319.cms">
                    अपना बाजार
                  </a>
                </li> */}
              </React.Fragment>
            </ul>
            <div className="globalnav">
              {/* timespoint widget */}
              <div id="widget-head" className="wdt_timespoints" />
              <LoginControl />
              <SocialMediaChannels dropdown parentClass="social_share" siteConfig={siteConfig} icons="rss,app,telegram,tw,yt,fb" />
            </div>
          </div>
          <nav className="nav_wrap">
            {/* <Navigation {...this.props} /> */}
            {gadgetPage || config.siteName == gadgetsConfig.gnMetaVal ? (
              <NavBarGadgetsNow
                data={navConfig[process.env.SITE]}
                clicked={this.toggleSearchBox}
                serchBoxVisible={serchBoxVisible}
                config={config}
                gadgetPage={gadgetPage}
              />
            ) : (
              <ErrorBoundary>
                <NavBar
                  rootSections={rootSections}
                  parentSections={parentSections}
                  dispatch={this.props.dispatch}
                  header={header}
                  pagetype={pagetype}
                  parentId={parentId}
                  subsec1={subsec1}
                  secId={secId}
                  hierarchylevel={hierarchylevel}
                  location={location}
                />
              </ErrorBoundary>
            )}
          </nav>
          <div className="clear" />
        </header>
        {/* pre render dfp */}
        <ErrorBoundary>
          <div className="atf-wrapper">
            <div id="general-atf-wrapper">
              <AdCard mstype="atf" adtype="dfp" rendertype="prerender" />
              {/*Internal DFP Ads related Divs */}
              {config && config.pagetype == "home" ? (
                <React.Fragment>
                  <div style={{ opacity: 0, height: "0px" }} id="tileyeDiv"></div>
                  <div className="adsdivlyr" id="adsdivLyr"></div>
                </React.Fragment>
              ) : null}
            </div>
          </div>
        </ErrorBoundary>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    config: state.config,
  };
}

export default connect(mapStateToProps)(DesktopHeader);

const NavBarGadgetsNow = ({ data, clicked, serchBoxVisible, config, gadgetPage }) => {
  return (
    <div id="fixedMenu" class="first-level-menu" itemtype="http://www.schema.org/SiteNavigationElement" itemscope="">
      <h3 class="home active" itemprop="name">
        <a href={siteConfig.gadgetdomain}>
          <i class="svgiconcount">
            <svg class="svg-icons " width="1em" height="1em">
              <use href="#home"></use>
            </svg>
          </i>
        </a>
      </h3>
      <div class="items">
        <ul>
          {data &&
            Array.isArray(data) &&
            data.map(item => {
              return (
                <li key={item.weblink}>
                  <AnchorLink href={`${siteConfig.gadgetdomain}${item.weblink}`}>{item.label}</AnchorLink>
                </li>
              );
            })}
        </ul>
      </div>
      <div class={`search_in_header${serchBoxVisible ? " active" : ""}`}>
        {serchBoxVisible && (
          <div className="searchbox">
            <GnSearchBox />

            <span className="close_icon" onClick={clicked} />
          </div>
        )}
        {(gadgetPage || config.siteName !== "gn") && (
          <b onClick={clicked}>
            <svg class="svg-icons " width="1em" height="1em">
              <use href="#search"></use>
            </svg>
          </b>
        )}
      </div>
    </div>
  );
};
