import React, { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import AdCard from "../../common/AdCard";
import ImageCard from "../../common/ImageCard/ImageCard";
import SocialShare from "../../common/SocialShare";
import ErrorBoundary from "../../lib/errorboundery/ErrorBoundary";
import RhsWidget from "../RhsWidget";
import TopComment from "../TopComments";
import {
  hasValue,
  isMobilePlatform,
  _getStaticConfig,
  isTechSite,
  isTechPage,
  _isCSR,
  getMobileOs,
  _deferredDeeplink,
  shouldTPRender,
} from "../../../utils/util";
import WebTitleCard from "../../common/WebTitleCard";
import KeyWordCard from "../../common/KeyWordCard";
import AnchorLink from "../../common/AnchorLink";
import { SeoSchema } from "../../common/PageMeta";
import { Userrating } from "../../common/MoviePoster";
import Slider from "../../desktop/Slider/Slider";
import styles from "../../../components/common/css/ArticleShow.scss";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/desktop/ArticleShow.scss";
import "../../../components/common/css/desktop/MovieShow.scss";
import SvgIcon from "../../common/SvgIcon";
import SectionHeader from "../../common/SectionHeader/SectionHeader";
import SocialSubscribeCard from "../../common/SocialMediaChannels/SocialSubscribeCard";
// import YSubscribeCard from "../../common/YSubscribeCard";
import TPWidgets from "../../common/TimesPoints/TPWidgets/TPWidgets";
import CommentsSection from "../CommentsSection";
import Parser from "html-react-parser";
import MSNews from "./MSNews";
import ReadMore from "./ReadMore";
import Breadcrumb from "../../common/Breadcrumb";
import SummaryCard from "../../common/SummaryCard";
import CommentBox from "../../common/CommentBox";
import fetch from "../../../utils/fetch/fetch";

const siteConfig = _getStaticConfig();

export function MovieShow({
  item,
  sharedata,
  pwaConfigAlaska,
  rhsCSRData,
  rhsCSRVideoData,
  openCommentsPopup,
  isAmp,
  router,
  handleComments,
  getDuration,
  watsAppShare,
  showCommentBox,
  closeCommentBox,
}) {
  const [showGenre, setShowGenre] = useState(false);
  const [showLang, setShowLang] = useState(false);
  const [review, setReview] = useState(null);

  const shareBtnRef = useRef();

  const pwaMeta = hasValue(item, "pwa_meta", null);
  const leadImage = hasValue(item, "lead.image", null);
  const videoData = hasValue(item, "rc.vdo", null);

  const trailers =
    videoData &&
    videoData.filter(vid => {
      return vid.trailer === "1";
    });

  const videos =
    videoData &&
    videoData.filter(vid => {
      return vid.trailer !== "1";
    });

  const photoData = hasValue(item, "rc.image", null);
  const castData = hasValue(item, "moviecast.item", null);
  const story = hasValue(item, "Story", null);
  const relatedNews = hasValue(item, "rel_news", null);
  const movieReview = hasValue(item, "moviereview", null);

  const gaanaEleRef = useRef();

  const schema = SeoSchema({ pagetype: "articleshow" }).attr().Movie;
  const pagetype = "articleshow";
  const isMobile = isMobilePlatform();
  const locale = siteConfig.locale;
  const taswidgetNBTPath = "https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas_new.cms";
  const taswidgetMTPath = "https://maharashtratimes.com/pwafeeds/amazon_wdt_tas_new.cms";

  useEffect(() => {
    animate_btn_App();
  }, []);

  const amazonIframeDataSource =
    process.env.SITE === "nbt"
      ? `${taswidgetMTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
          pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
        }`
      : `${taswidgetNBTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
          pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
        }`;

  const showHide = (type, show) => {
    if (type === "genre") {
      setShowGenre(show);
    } else {
      setShowLang(show);
    }
  };

  const showHideToggle = type => {
    if (type === "genre") {
      setShowGenre(!showGenre);
    } else {
      setShowLang(!showLang);
    }
  };

  const animate_btn_App = () => {
    // Check is button visible or not
    let shareContainer = shareBtnRef.current;
    if (shareContainer) {
      shareContainer.classList.add("hide2right");
      shareContainer.style.display = "none";

      window.addEventListener("scroll_down", function() {
        shareContainer.classList.remove("hide2right");
      });

      window.addEventListener("scroll_up", function() {
        shareContainer.style.display = "block";
        shareContainer.classList.add("hide2right");
      });
    }
  };

  const list = (items, type, show) => {
    let initialItems,
      moreItems = [];
    const itemsToShow = isMobile ? 3 : 4;
    if (!isAmp && items.length > itemsToShow) {
      initialItems = items.slice(0, itemsToShow - 1);
      moreItems = items.slice(itemsToShow - 1);
    } else {
      initialItems = items;
    }
    return (
      <React.Fragment>
        {initialItems.map(item => (
          <span key={item} className="dotlist">
            {item}
          </span>
        ))}
        {moreItems.length > 0 && (
          <span className="moreitems">
            <span
              key={"more"}
              onClick={() => showHideToggle(type)}
              onMouseEnter={() => showHide(type, true)}
              onMouseLeave={() => showHide(type, false)}
            >
              + {items.length - itemsToShow + 1} more
            </span>
            {
              <ul className={show ? "" : "hide"}>
                {moreItems.map(item => (
                  <li key={item}>{item}</li>
                ))}
              </ul>
            }
          </span>
        )}
      </React.Fragment>
    );
  };

  const trimComma = string => {
    return string.replace(/,\s*$/, "");
  };

  /**
   * Function to formate date from 07-22-1991 to 22 Jul, 1991
   * @param {date} string | mm-dd-yyyy format date input
   */
  const dateFormat = date => {
    const t_date = new Date(date.replace(/-/g, "/"));
    return t_date.getDate() + " " + t_date.toLocaleString("en-us", { month: "short" }) + ", " + t_date.getFullYear();
  };

  useEffect(() => {
    if(movieReview && movieReview.id){
      fetch(
        `${process.env.API_BASEPOINT}/sc_articleshow.cms?msid=${movieReview.id}&feedtype=sjson`,
      )
        .then(response => {
          setReview(response);
        })
        .catch(error => console.log(error));
    }
  }, []);

  return (
    <ErrorBoundary>
      <div className={isMobile ? "col12" : "col8"}>
        <div {...schema} className="movieshowlft">
          {SeoSchema({
            pagetype: pagetype,
          }).mainEntry(pwaMeta.canonical)}
          {SeoSchema({
            pagetype: pagetype,
          }).metaUrl(pwaMeta.canonical)}
          {SeoSchema({
            pagetype: pagetype,
          }).dateStatus(item.dlseo, item.luseo)}
          {SeoSchema({ pagetype: pagetype }).language()}
          {pwaMeta.alternatetitle ? SeoSchema().alternateName(pwaMeta.alternatetitle) : ""}
          {pwaMeta.key ? SeoSchema().keywords(pwaMeta.key) : ""}
          {item.syn ? SeoSchema().artdesc(pwaMeta.syn) : ""}
          {SeoSchema().disambiguatingDescription(item.webtitle)}
          {SeoSchema().publisherObj()}

          <div className="moviewshowcard">
            <div className="movieinfo">
              <div className="movietitle">
                <div className="moviemeta">
                  <h1 itemProp="name">
                    {item.hl} {item.certificate ? <b>{item.certificate}</b> : null}
                  </h1>
                  {isMobile && (
                    <button
                      data-exclude="amp"
                      className="ms_button"
                      onClick={() =>
                        gaanaEleRef.current.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
                      }
                    >
                      <SvgIcon name="music" /> {locale.listenAlbum}
                    </button>
                  )}
                  {/* for schema only */}
                  <span itemProp={"director"} itemScope="itemScope" itemType="https://schema.org/Person">
                    <meta itemProp="name" content={item.dir} />
                  </span>
                  <div>
                    <span className="metaitems mb10">
                      {item.rd && item.rd !== "--" && (
                        <span className="metafirst">
                          <SvgIcon name="calendar" /> {dateFormat(item.rd)}
                        </span>
                      )}
                      {item.lang && list(trimComma(item.lang).split(","), "lang", showLang)}
                    </span>
                    <span className="metaitems mb10">
                      {item.du && (
                        <span className="metafirst">
                          <SvgIcon name="duration" /> {getDuration(item.du)}
                        </span>
                      )}
                      {item.gn && list(trimComma(item.gn).split(","), "genre", showGenre)}
                    </span>
                  </div>
                </div>
                {!isMobile && (
                  <div className="ms_actions">
                    <button
                      className="ms_button"
                      onClick={() =>
                        gaanaEleRef.current.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
                      }
                    >
                      <SvgIcon name="music" /> {locale.listenAlbum}
                    </button>
                    {/* <button className="ms_button">Check Showtimes</button> */}
                  </div>
                )}
              </div>
              {isMobile && (
                <div className="table rating">
                  <div className="table_row">
                    <span className="table_col">
                      <span className="count critic">
                        {item.cr == 0 || item.cr == undefined || item.cr == "NaN" ? (
                          <span>NA</span>
                        ) : (
                          <span>
                            <b>{item.cr}</b>/5
                          </span>
                        )}
                      </span>
                      <span className="txt">{locale.critics_rating}</span>
                    </span>
                    <span className="table_col">
                      <span className="count users">
                        {item.ur && item.ur != "NaN" && item.ur != "" ? (
                          <span>
                            <b>{item.ur == "NaN" ? "NA" : item.ur}</b>/5
                          </span>
                        ) : (
                          "NA"
                        )}
                      </span>
                      <span className="txt">{locale.reader_rating}</span>
                    </span>
                  </div>
                </div>
              )}
              <div className="ms_poster">
                <div className="ms_posterimg">
                  <ImageCard
                    noLazyLoad
                    msid={leadImage && leadImage.id}
                    imgsize={item.imgsize}
                    size="posterthumb"
                    schema
                  />
                </div>

                <div className="ms_posterinfo">
                  <div className="ms_ratings">
                    {!isMobile && (
                      <Userrating
                        review={"techreview"}
                        userrating={item.ur == 0 ? null : item.ur}
                        id={item.id}
                        sharedata={sharedata}
                        cr={item.cr}
                        ur={item.ur}
                        pagetype={"movieshow"}
                        openCommentsPopup={openCommentsPopup}
                      />
                    )}
                  </div>
                  {!isMobile && (
                    <div className="ms_share">
                      <span className="sharetxt">Share</span>
                      <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
                    </div>
                  )}
                  <div className="ms_synopsis">
                    <h3>Synopsis</h3>
                    <ReadMore
                      className="read-more-content"
                      charLimit={isAmp ? pwaMeta.syn.length + 1 : isMobile ? 140 : 80}
                      readMoreText="Read More"
                      readLessText="Read Less"
                    >
                      {pwaMeta.syn}
                    </ReadMore>
                  </div>
                </div>
              </div>
            </div>
            {trailers && trailers.length > 0 && (
              <div className="ms_trailers">
                <SectionHeader sectionhead={`Trailer | ${trailers.length} videos`} headerClass="noborder" />
                <div className="wdt_video slider-horizontalView">
                  <Slider
                    type="grid"
                    size="3"
                    sliderData={trailers}
                    sliderClass="videosection"
                    margin="25"
                    width="180"
                    noSeo
                  />
                </div>
              </div>
            )}
          </div>
          {isMobile && (
            <div className="reviewbuttons">
              <Userrating
                review={"techreview"}
                userrating={item.ur == 0 ? null : item.ur}
                id={item.id}
                sharedata={sharedata}
                cr={item.cr}
                ur={item.ur}
                pagetype={"movieshow"}
                openCommentsPopup={openCommentsPopup}
              />
            </div>
          )}
          {/* <div className="adcards">
                <AdCard mstype="ctnsluglist" adtype="ctn" className="box-item ad-top" pagetype="home" />
                <AdCard mstype="ctnsluglist" adtype="ctn" className="box-item ad-top" pagetype="home" />
              </div> */}
          {castData && castData.length > 0 && (
            <React.Fragment>
              <SectionHeader sectionhead={locale.castncrew} headerClass="noborder" />
              <div className="moviewidgets castprofile">
                <Slider margin="15" size={4} sliderData={castData} width="140" type="castncrew" />
              </div>
            </React.Fragment>
          )}
          {!isAmp && review && review.Story && review.Story !== "" && (
            <React.Fragment>
              <SectionHeader sectionhead={locale.movie_review} headerClass="noborder" />
              <div className="moviewidgets moviestry moviereview">
                <SectionHeader sectionhead={item.hl} headerClass="noborder" />
                <div className="news_card_source">
                  {SummaryCard.Byline(review)}
                  <span className="rating">
                    <b>{locale.critics_rating}</b>
                    <span className="rating-stars">
                      <span className="empty-stars"></span>
                      <span className={"filled-stars critic " + "w" + parseInt(review.cr) * 20}></span>
                    </span>
                    <span className="rate_val">
                      {parseFloat(review.cr) > 0 ? parseFloat(review.cr).toFixed(1) : review.cr}
                      <small>/5</small>
                    </span>
                  </span>
                </div>
                <div>{Parser(review.Story, {})}</div>
              </div>
              <div className="readfullreview">
                <AnchorLink
                  hrefData={
                    {
                          seo: movieReview.seolocation,
                          tn: "moviereview",
                          id: movieReview.id,
                        }
                  }
                >
                  <span>{locale.readmore}</span>
                </AnchorLink>
              </div>
            </React.Fragment>
          )}
          
          {story && story !== "" && (
            <React.Fragment>
              <SectionHeader sectionhead={item.hl} headerClass="noborder" />
              <div className="news_card_source">{SummaryCard.Byline(item)}</div>
              <div className="moviewidgets moviestry">{Parser(story, {})}</div>
            </React.Fragment>
          )}

          {isAmp && movieReview && movieReview.seolocation && (
            <div className="readfullreview">
              <AnchorLink
              hrefData={
                {
                      seo: movieReview.seolocation,
                      tn: "moviereview",
                      id: movieReview.id,
                    }
              }
            >
              {`${item.hl} ${locale.read_movie_review}`}
            </AnchorLink>
            </div>
          )}
          {videos && videos.length > 0 && (
            <React.Fragment>
              <SectionHeader sectionhead={locale.videos} headerClass="noborder" />
              <div className="moviewidgets">
                <Slider
                  margin="15"
                  size={3}
                  sliderData={videos}
                  width="180"
                  type="grid"
                  sliderClass="videosection"
                  movesize="1"
                />
              </div>
            </React.Fragment>
          )}
          {photoData && photoData.length > 0 && (
            <React.Fragment>
              <SectionHeader sectionhead={locale.photos} headerClass="noborder" />
              <div className="moviewidgets">
                <Slider margin="15" size={3} sliderData={photoData} width="190" type="grid" movesize="1" noSeo />
              </div>
            </React.Fragment>
          )}
          {item.gaana && (
            <div ref={gaanaEleRef} className="gaanaiframe">
              <SectionHeader sectionhead={locale.filmSongs} headerClass="noborder" />
              <iframe
                valign="top"
                align="center"
                scrolling="yes"
                frameBorder="0"
                marginHeight="0"
                marginWidth="0"
                height="300"
                width="100%"
                src={item.gaana}
                style={{ background: "#f6f6f6" }}
              ></iframe>
            </div>
          )}
          {relatedNews && relatedNews.length > 0 && (
            <MSNews title={`${item.hl} ${locale.news}`} data={relatedNews && relatedNews.slice(0, 6)} />
          )}
          <div data-exclude="amp">
            {/* <SectionHeader sectionhead={locale.user_review} headerClass="noborder" /> */}
            <TopComment msid={item.id} pagetype={"movieshow"} />
            {isMobile ? (
              <div className="comment_wrap" data-exclude="amp">
                {/*<AnchorLink key={'comment'+item.id} className="btn_comment" target="_blank" hrefData={{override : `https://navbharattimes.indiatimes.com/off-url-forward/pwa_comments.cms?frmapp=yes&pagetype=${pagetype}`}}>कॉमेंट लिखें</AnchorLink>*/}
                <a
                  id={"comment_" + item.id}
                  className="btn_comment"
                  href="javascript:void(0)"
                  onClick={() => handleComments(item.id)}
                >
                  {locale.review_movie}
                </a>
                {_isCSR() && showCommentBox ? (
                  <CommentBox msid={item.id} headline={item.hl} closeCommentsBox={closeCommentBox} />
                ) : null}
              </div>
            ) : (
              <div className="comments-wrapper" role="button" tabIndex="0">
                <div className="bottom-comments-btns">
                  <div className="cmtbtn-wrapper">
                    <span className="cmtbtn add" onClick={() => openCommentsPopup(item.id)}>
                      <span>{locale.review_movie}</span>
                      <b className="icon" />
                    </span>
                  </div>
                </div>
              </div>
            )}
          </div>

          {pwaConfigAlaska &&
          (pwaConfigAlaska._topdeal == "true" || pwaConfigAlaska._topoffer == "true") &&
          isMobile &&
          ((isTechSite() && !isTechPage(pwaMeta.canonical)) || !isTechSite()) ? (
            <div className="wdt_amz pwa-deals">
              <iframe
                scrolling="no"
                frameBorder="no"
                height={isAmp ? "330" : "310"}
                src={
                  isAmp
                    ? amazonIframeDataSource
                    : `${taswidgetNBTPath}?host=${process.env.SITE}&wdttype=${
                        pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                      }`
                }
              />
            </div>
          ) : null}

          {isMobile && typeof shouldTPRender == "function" && shouldTPRender() && (
            <ErrorBoundary>
              <TPWidgets type="AS" router={router} />
            </ErrorBoundary>
          )}
          {isMobile && (
            <div className="con_fbapplinks">
              <h3>{locale.social_download.title}</h3>
              <div className="wdt_story_social">
                <SocialSubscribeCard telegramLink={item.pwa_meta.teleChannel} />
              </div>
            </div>
          )}
          {pwaMeta && pwaMeta.nextItem && pwaMeta.nextItem !== "" ? (
            <div className="next_article" data-attr="next_article">
              <AnchorLink href={`/${pwaMeta.nextItem.seolocation}/` + `movieshow` + `/${pwaMeta.nextItem.msid}.cms`}>
                <span className="nxttitle">
                  <span className="text_ellipsis">{pwaMeta.nextItem.title}</span>
                </span>
                <span className="nxtlink">
                  {locale.next_article.movieshow ? locale.next_article.movieshow : "Next Movie"}
                </span>
              </AnchorLink>
            </div>
          ) : null}
          {isMobile ? (
            item && pwaMeta && pwaMeta.AdServingRules && pwaMeta.AdServingRules === "Turnoff" ? null : (
              <AdCard mstype="mrec2" adtype="dfp" pagetype="articleshow" renderall />
            )
          ) : null}
          {!(pwaMeta && pwaMeta.AdServingRules && pwaMeta.AdServingRules === "Turnoff") && (
            <AdCard adtype="ctn" mstype="ctnshow" />
          )}
          {pwaMeta && pwaMeta.topicskey && pwaMeta.topicskey !== "" ? (
            <div className="keywords_wrap">{KeyWordCard(pwaMeta.topicskey)}</div>
          ) : null}
          {WebTitleCard(pwaMeta)}
          {isMobile && <Breadcrumb items={item.breadcrumb.div.ul} />}
          {!isMobile ? (
            <React.Fragment>
              <CommentsSection msid={item.id} showComments={openCommentsPopup} type="review" />
            </React.Fragment>
          ) : (
            ""
          )}
          {!isMobile && rhsCSRVideoData && rhsCSRVideoData.items ? (
            <div className="wdt_popular_videos" data-exclude="amp">
              <ErrorBoundary>
                <Slider
                  type="grid"
                  size="3"
                  movesize="3"
                  sliderData={[].concat(rhsCSRVideoData.items)}
                  width="190"
                  SliderClass="popular_videos"
                  islinkable="true"
                  margin="20"
                  heading={locale.popularVideos}
                />
              </ErrorBoundary>
            </div>
          ) : null}
        </div>
      </div>
      {!isMobile && (
        <div className="col4">
          <RhsWidget
            showDataDrawer
            pwaConfigAlaska={pwaConfigAlaska}
            rhsCSRData={rhsCSRData}
            articleId={item.id}
            articleSecId={item && item.subsec1}
            sectionType={pwaMeta && pwaMeta.sectiontype}
          />
        </div>
      )}
      <div>
        {_isCSR()
          ? isMobile &&
            ReactDOM.createPortal(
              <div ref={shareBtnRef} className="share_container" style={!isAmp ? { display: "none" } : null}>
                <div className="share_whatsapp">
                  <a
                    data-attr="share_whatsapp"
                    rel="nofollow"
                    onClick={() => watsAppShare(item)}
                    href="javascript:void(0);"
                  >
                    <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                  </a>
                </div>
                <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
              </div>,
              document.getElementById("share-container-wrapper"),
            )
          : isMobile && (
              <div ref={shareBtnRef} className="share_container" style={!isAmp ? { display: "none" } : null}>
                <div className="share_whatsapp">
                  <a
                    data-attr="share_whatsapp"
                    rel="nofollow"
                    onClick={() => watsAppShare(item)}
                    href="javascript:void(0);"
                  >
                    <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                  </a>
                </div>
                <SocialShare sharedata={sharedata} openCommentsPopup={openCommentsPopup} />
              </div>
            )}
      </div>
    </ErrorBoundary>
  );
}

export default MovieShow;
