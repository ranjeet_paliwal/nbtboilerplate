import React from "react";
import PropTypes from "prop-types";
import AnchorLink from "../../common/AnchorLink";
import ImageCard from "../../common/ImageCard/ImageCard";
import SectionHeader from "../../common/SectionHeader/SectionHeader";

export default function MSNews({ title, data }) {
  return (
    <div className="msnews">
      <SectionHeader sectionhead={title} headerClass="noborder" />
      <ul>
        {data.map(item => (
          <li className="news-card horizontal col news" key={item.link}>
            <span className="img_wrap">
              <AnchorLink href={`${item.link}`}>
                <ImageCard msid={item.img} size="smallthumb" title={item.alttitle} alt={item.alttitle} />
              </AnchorLink>
            </span>
            <span className="con_wrap">
              <AnchorLink href={`${item.link}`}>
                <span className="text_ellipsis">{item.title}</span>
              </AnchorLink>
            </span>
          </li>
        ))}
      </ul>
    </div>
  );
}

MSNews.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
};
