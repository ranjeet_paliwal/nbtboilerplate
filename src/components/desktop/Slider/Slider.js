/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import VideoItem from "../../common/VideoItem/VideoItem";

import "./HP_slider.scss";
import ImageCard from "../../common/ImageCard/ImageCard";
import GridCardMaker from "../../common/ListingCards/GridCardMaker";
import AdCard from "../../common/AdCard";
import AnchorLink from "../../common/AnchorLink";
import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

class Slider extends PureComponent {
  constructor(props) {
    super(props);
    const { sliderData } = this.props;
    this.state = {
      data: sliderData,
      currentIndex: 0,
      translateValue: 0,
    };
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue, 10) + parseInt(this.slideWidth(), 10),
    }));
  };

  goToNextSlide = datalength => {
    // Exiting the method early if we are at the end of the images array.
    // We also want to reset currentIndex and translateValue, so we return
    // to the first image in the array.
    const { currentIndex } = this.state;

    if (currentIndex === parseInt(datalength, 10) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth(),
    }));

    return 0;
  };

  goToSlide = index => {
    this.setState(prevState => ({
      currentIndex: index,
      translateValue: prevState.translateValue + (prevState.currentIndex - index) * this.slideWidth(),
    }));
  };

  getClosestParent = (elem, selector) => {
    for (; elem && elem !== document; elem = elem.parentNode) {
      if (elem.matches(selector)) return elem;
    }
    return null;
  };

  slideWidth = () => {
    let { size, movesize } = this.props;
    movesize = movesize || size;
    // const self = this;
    // const margin = self.props.margin ? parseInt(self.props.margin) : 0;
    // // // return self.props.width ? parseInt(self.props.width) + parseInt(margin) : 300;
    // let slideWidth =
    //   parseInt(self.props.sliderWidth) * parseInt(movesize) + margin;
    // return slideWidth;
    // const classSelector = sliderClass ? `.${sliderClass} .slide` : ".slide";

    try {
      // returns the slider node
      const parent = this.getClosestParent(event.target, ".slider");
      const slidWidth = parent.querySelector(".slide").clientWidth;
      return (
        // document.querySelector(classSelector).clientWidth * parseInt(movesize)
        slidWidth * parseInt(movesize)
        // event.target.parentElement.querySelector(".slide").clientWidth *
        // parseInt(movesize)
      );
    } catch (error) {
      console.log(error);
    }
  };

  getUpdatedData = (data, size) => {
    const newArr = [];
    const newdata = [...data];
    while (newdata.length > 0) {
      newArr.push(newdata.splice(0, size));
    }
    return newArr;
  };

  render() {
    const { translateValue, data } = this.state;
    let {
      type,
      subtype,
      videoIntensive,
      size,
      width,
      height,
      heading,
      sliderClass,
      link,
      sliderWidth,
      clicked,
      headingRequired,
      margin,
      playerConfig,
      compType,
      parentMsid,
      className,
      imgsize,
      noSeo,
      showCandidateStatus,
    } = this.props;

    // in case of type video, slider will by default open video in pop up
    if (typeof videoIntensive === "undefined" && type === "video") {
      videoIntensive = false;
    }
    margin = margin ? parseInt(margin) : 0;
    const updatedArr = data ? this.getUpdatedData(data, size) : [];
    return (
      <React.Fragment>
        {headingRequired !== "no" && heading && <h2>{heading}</h2>}
        <div className={`slider ${sliderClass}`} style={{ width: `${(parseInt(width) + margin) * size - margin}px` }}>
          {/* slider width:- (width + margin) multiply by number of items and margin removed from last item */}
          <div className="hidden_layer">
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: "transform ease-out 0.45s",
              }}
            >
              <ul>
                {Array.isArray(updatedArr)
                  ? updatedArr.map((item, index) => (
                      <ManageSlider
                        key={`${item[0].id ? item[0].id : index.toString()}_slider`}
                        type={type}
                        subtype={subtype}
                        videoIntensive={videoIntensive}
                        data={item}
                        width={width}
                        height={height}
                        sliderWidth={sliderWidth}
                        clicked={clicked}
                        margin={margin}
                        playerConfig={playerConfig}
                        props={this.props}
                        istrending={this.props.istrending}
                        compType={compType}
                        parentMsid={parentMsid}
                        className={className}
                        link={link}
                        imgsize={imgsize}
                        noSeo={noSeo}
                        showCandidateStatus={showCandidateStatus}
                      />
                    ))
                  : null}
              </ul>
            </div>
          </div>
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <LeftArrow goToPrevSlide={this.goToPrevSlide.bind(this)} />
          )}
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <RightArrow goToNextSlide={this.goToNextSlide.bind(this, updatedArr.length)} />
          )}
          {this.props.slideIndicator ? (
            <div className="slideIndicator">
              {updatedArr.map((slide, index) => (
                <SlideIndicator
                  key={index}
                  index={index}
                  currentIndex={this.state.currentIndex}
                  isActive={this.state.currentIndex == index}
                  onClick={this.goToSlide.bind(this, index)}
                />
              ))}
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

const SlideIndicator = props => (
  <a className={props.index == props.currentIndex ? "active" : ""} onClick={props.onClick} />
);

const ManageSlider = ({
  type,
  subtype,
  videoIntensive,
  data,
  width,
  height,
  sliderWidth,
  clicked,
  margin,
  playerConfig,
  istrending,
  props,
  compType,
  parentMsid,
  className,
  link,
  imgsize,
  noSeo,
  showCandidateStatus,
}) => {
  let slider = "";

  switch (type) {
    case "zodiac_sign":
      slider = (
        <li key="zodiac_sign">
          {data.map((item, index) => (
            <AnchorLink key={index.toString()} href={`${link}?story=${item.pos}`}>
              <div
                className="slide"
                style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                key={item.name}
                role="button"
                tabIndex={0}
                onClick={clicked}
                onKeyUp={clicked}
              >
                <div className={`astro-sign sign-${item.name}`} id={item.name} />
                <div className="txt">{item.regName}</div>
              </div>
            </AnchorLink>
          ))}
        </li>
      );
      break;
    case "video":
      // Add Switch Case as per UI
      slider = (
        <li
          key="video"
          itemProp={!noSeo ? "itemListElement" : undefined}
          itemScope="1"
          itemType="http://schema.org/ListItem"
          className={`${className || ""} news-card video`}
        >
          {Array.isArray(data)
            ? data.map((item, index) => {
                if (item.tn == "ad") {
                  return item.platform && item.platform != "" && item.platform != process.env.PLATFORM ? null : (
                    <li
                      key={index}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      className={`news-card ${item.type} col${item.adcols ? item.adcols : ""}`}
                    >
                      {<AdCard mstype={item.mstype} adtype={item.type} />}
                      {/* <div className={`ad1 ${card.mstype}`} /> */}
                    </li>
                  );
                }
                return (
                  <div
                    className="slide"
                    key={item.imageid}
                    style={{
                      width: `${width}px`,
                      height: `${height}px`,
                      paddingRight: `${margin}px`,
                    }}
                  >
                    {clicked ? (
                      <VideoItem
                        key={item.id}
                        item={item}
                        index={index}
                        videoIntensive={videoIntensive}
                        clicked={clicked.bind(this, item.eid, item)}
                        className="vd-slider-listview"
                        istrending={props.istrending}
                        parentMsid={parentMsid}
                      />
                    ) : (
                      <VideoItem
                        key={item.id}
                        item={item}
                        videoIntensive={videoIntensive}
                        className="vd-slider-listview"
                        istrending={props.istrending}
                        parentMsid={parentMsid}
                      />
                    )}
                  </div>
                );
              })
            : null}
        </li>
      );
      break;

    case "videopopup":
      // Add Switch Case as per UI
      slider = (
        <li key="videopopup" itemProp="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => (
                <div
                  className="slide"
                  key={item.imageid}
                  onClick={() => clicked(item, parentMsid)}
                  style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                >
                  <div className="img_wrap" data-tag={item.du || ""}>
                    <ImageCard
                      msid={item.imageid || ""}
                      title={item.hl}
                      size="largewidethumb"
                      className="videoicons"
                      imgver={item.imgsize}
                    />
                    <span className="type_icon video_icon" />
                  </div>
                  <div className="con_wrap">
                    <span className="text_ellipsis">{item.hl}</span>
                  </div>
                </div>
              ))
            : null}
        </li>
      );
      break;

    case "embedslider":
      slider = (
        <li key="embedslider">
          {Array.isArray(data)
            ? data.map(item => {
                const title = item.imgtitle;
                return (
                  <div className="slide" key={item.id} style={{ width: `${width}px` }}>
                    <ImageCard
                      msid={item.id ? item.id : "69347424"}
                      title={title}
                      size="bigimage"
                      className="candidate_img"
                      imgver={item.imgsize}
                    />
                    <div className="embedslide_desc" dangerouslySetInnerHTML={{ __html: item.cap }}></div>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;

    case "relatedvdoslider":
      slider = (
        <li key="relatedvdoslider" className="news-card video">
          {Array.isArray(data)
            ? data.map(item => (
                <div className="slide" key={item.imageid} style={{ width: `${width}px`, paddingRight: `${margin}px` }}>
                  <VideoItem
                    key={item.id}
                    item={item}
                    videoIntensive={!item.eid}
                    // clicked={clicked.bind(this, item.eid, item)}
                    className="candidate_img"
                    imgsize="smallwidethumb"
                    istrending={props.istrending}
                    parentMsid={parentMsid}
                  />
                </div>
              ))
            : null}
        </li>
      );
      break;

    case "starCandidates":
      slider = (
        <li key="slider" itemrop="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => (
                <div className="slide" key={item.imageid} style={{ width: `${width}px`, paddingRight: `${margin}px` }}>
                  <CandidateSlide item={item} showCandidateStatus={showCandidateStatus} />
                </div>
              ))
            : null}
        </li>
      );
      break;

    case "budgetSlider":
      slider = (
        <React.Fragment>
          <li>
            {Array.isArray(data)
              ? data.map((card, index) => {
                  const title = card.imgtitle;
                  return (
                    <div
                      className="slide"
                      key={card.imageid}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      key={index}
                    >
                      <AnchorLink href={card.override}>
                        <ImageCard
                          msid={card.imageid ? card.imageid : "69347424"}
                          title={title}
                          size="bigimage"
                          className="candidate_img"
                          imgver={card.imgsize}
                        />
                      </AnchorLink>
                    </div>
                  );
                })
              : null}
          </li>
        </React.Fragment>
      );
      break;
    case "castncrew":
      slider = (
        <li key="slider">
          {Array.isArray(data)
            ? data.map(item => (
                <div
                  className="slide"
                  key={item.imageid}
                  style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                  itemProp="actor"
                  itemScope="1"
                  itemType="http://schema.org/Person"
                >
                  <ProfileSlide item={item} />
                </div>
              ))
            : null}
        </li>
      );
      break;

    case "budgetSlider":
      slider = (
        <React.Fragment>
          <li>
            {Array.isArray(data)
              ? data.map((card, index) => {
                  const title = card.imgtitle;
                  return (
                    <div
                      className="slide"
                      key={card.imageid}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      key={index}
                    >
                      <AnchorLink href={card.override}>
                        <ImageCard
                          msid={card.imageid ? card.imageid : "69347424"}
                          title={title}
                          size="bigimage"
                          className="candidate_img"
                          imgver={card.imgsize}
                        />
                      </AnchorLink>
                    </div>
                  );
                })
              : null}
          </li>
        </React.Fragment>
      );
      break;

    case "grid":
      // Do not put imgsize here, it would be handled by gridcardmaker itself
      slider = (
        <React.Fragment>
          {Array.isArray(data)
            ? data.map((card, index) => {
                if (card.tn === "ad") {
                  return card.platform && card.platform !== "" && card.platform !== process.env.PLATFORM ? null : (
                    <li
                      key={index.toString()}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      className={`news-card ${card.type} col${card.adcols ? card.adcols : ""}`}
                    >
                      {<AdCard mstype={card.mstype} adtype={card.type} />}
                    </li>
                  );
                }
                return (
                  card.imageid &&
                  card.imageid !== "" && (
                    <GridCardMaker
                      card={card}
                      key={card.imageid}
                      keyName={card.imageid}
                      cardType={card.tn}
                      className="slide"
                      columns=""
                      imgsize={imgsize}
                      width={width}
                      margin={margin}
                      index={index}
                      istrending={props.istrending}
                      compType={compType}
                      parentMsid={parentMsid}
                      noSeo={noSeo}
                    />
                  )
                );
              })
            : null}
        </React.Fragment>
      );
      break;

    case "briefnews":
      slider = (
        <React.Fragment>
          <li>
            {Array.isArray(data)
              ? data.map((item, index) => {
                  const title = item.imgtitle;
                  const briefLink =
                    `/newsbrief/${item.seolocation}/newsbrief/` + `articleid-${parentMsid},msid-${item.id}.cms`;
                  // const briefSectLink = "/newsbrief.cms";
                  // let briefsyn =
                  //   item.briefsyn && item.briefsyn != "" && item.briefsyn.indexOf("--|$|--") > -1 ? (
                  //     <ul>
                  //       {item.briefsyn.split("--|$|--").map(function(item, index) {
                  //         return <li key={`briefsyn-${index}`}>{item}</li>;
                  //       })}
                  //     </ul>
                  //   ) : (
                  //     item.briefsyn
                  //   );
                  return (
                    <div
                      className="slide"
                      key={item.imageid}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      key={index}
                    >
                      <div className="briefslide">
                        <div className="top_content">
                          <AnchorLink href={briefLink}>
                            <span className="table_col img_wrap">
                              <ImageCard
                                msid={item.imageid ? item.imageid : "69347424"}
                                title={title}
                                size="largethumb"
                                imgver={item.imgsize}
                              />
                            </span>
                          </AnchorLink>
                        </div>
                        <div className="bottom_content">
                          <AnchorLink className="news_sec_name" href={briefLink}>
                            {item.subsecname}
                          </AnchorLink>
                          <AnchorLink href={briefLink} className="table_row">
                            <span className="text_ellipsis">{item.hl} </span>

                            {/* <div className="briefsyn">{briefsyn}</div> */}
                          </AnchorLink>
                          <div className="actionbar">
                            <AnchorLink href={item.override} className="more">
                              {siteConfig.locale.read_more_liveblog}
                            </AnchorLink>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })
              : null}
          </li>
        </React.Fragment>
      );
      break;

    default:
      slider = (
        <li key="slider" itemrop="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => {
                const title = type === "showcase" ? item.syn : item.hl;
                return (
                  <div
                    className="slide"
                    key={item.imageid}
                    style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                  >
                    <a href="#" onClick={e => e.preventDefault()}>
                      <span className="img_wrap">
                        <ImageCard
                          msid={item.imageid || "69347424"}
                          title={title}
                          size="smallthumb"
                          className="candidate_img"
                          imgver={item.imgsize}
                        />
                      </span>

                      <span className="con_wrap">
                        <span className="text_ellipsis">{item.hl}</span>
                      </span>
                    </a>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
  }
  return slider;
};

Slider.propTypes = {
  type: PropTypes.string,
  sliderData: PropTypes.array,
  // secname: PropTypes.string,
  // currentIndex: PropTypes.any,
  // translateValue: PropTypes.number,
  size: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
  link: PropTypes.string,
  sliderWidth: PropTypes.string,
  clicked: PropTypes.func,
  headingRequired: PropTypes.string,
};

ManageSlider.propTypes = {
  data: PropTypes.array,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
};

const LeftArrow = props => {
  const { goToPrevSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      className="btnPrev"
      onClick={goToPrevSlide}
      onKeyUp={goToPrevSlide}
    />
  );
};

const RightArrow = props => {
  const { goToNextSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      pg="nxtphoto"
      className="btnNext"
      onClick={goToNextSlide}
      onKeyUp={goToNextSlide}
    />
  );
};

const CandidateSlide = props => {
  const { item, showCandidateStatus } = props;
  return (
    <div className={`candidate_box ${showCandidateStatus ? item.statusClass : ""}`}>
      <AnchorLink href={`/topics/${item.cn ? item.cn.replace(" ", "-").toLowerCase() : null}`}>
        <span className={`candidate_imgwrap ${showCandidateStatus ? item.statusClass : ""}`}>
          <ImageCard
            msid={item.img || "69347424"}
            title="title"
            size="smallthumb"
            className="candidate_img"
            imgver={item.imgsize}
          />
        </span>
        <span className="candidate_name">{item.cn}</span>
        <span className="constituency">{item.cns_name}</span>
        <span className="party-name" style={{ backgroundColor: item.cc }}>
          {item.an}
        </span>
        {/* <span className="state">{item.statenm}</span> */}
      </AnchorLink>
    </div>
  );
};

const ProfileSlide = props => {
  const { item } = props;
  const title = item.label + item.name;
  return (
    <div className="candidate_box">
      <link itemProp="sameAs" href={`${item.overridelink}`} />
      <AnchorLink key={title} href={`${item.overridelink}`}>
        <span className="candidate_imgwrap">
          <ImageCard
            msid={item.img || item.imgid || "69347424"}
            title={item.name}
            alt={item.seoname || item.name}
            size="smallthumb"
            className="candidate_img"
            imgver={item.imgsize}
            schema
          />
        </span>
        <span itemProp="name" className="candidate_name">
          {item.name}
        </span>
        <span className="constituency">{item.label}</span>
      </AnchorLink>
    </div>
  );
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

export default Slider;
