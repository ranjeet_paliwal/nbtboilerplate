/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */

import React, { Component } from "react";
import PropTypes from "prop-types";
import Loadable from "react-loadable";
import "./HP_slider.scss";
import ImageCard from "../../common/ImageCard/ImageCard";
import GridCardMaker from "../../common/ListingCards/GridCardMaker";
import AdCard from "../../common/AdCard";
import AnchorLink from "../../common/AnchorLink";
import { isMobilePlatform, LoadingComponent } from "../../../utils/util";

const VenueComp = Loadable({
  loader: () => import("../../../campaign/cricket/containers/IPL/IPL").then(module => module.VenueComp),
  LoadingComponent,
});
const GetPlayerDetails = Loadable({
  loader: () => import("../../../campaign/cricket/containers/IPL/IPL").then(module => module.GetPlayerDetails),
  LoadingComponent,
});
const ScheduleCard = Loadable({
  loader: () => import("../../../campaign/cricket/components/schedulecard"),
  LoadingComponent,
});
class Slider extends Component {
  constructor(props) {
    super(props);
    const { sliderData } = this.props;
    this.state = {
      data: sliderData,
      currentIndex: 0,
      translateValue: 0,
    };
  }

  componentDidMount() {
    const { startFromIndex } = this.props;
    if (
      startFromIndex &&
      startFromIndex >= 0 &&
      Array.isArray(this.state.data) &&
      startFromIndex < this.state.data.length
    ) {
      this.startAtSlide(startFromIndex);
    }
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue, 10) + parseInt(this.slideWidth(), 10),
    }));
  };

  goToNextSlide = datalength => {
    // Exiting the method early if we are at the end of the images array.
    // We also want to reset currentIndex and translateValue, so we return
    // to the first image in the array.
    const { currentIndex } = this.state;

    if (currentIndex === parseInt(datalength, 10) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth(),
    }));

    return 0;
  };

  goToSlide = index => {
    this.setState(prevState => ({
      currentIndex: index,
      translateValue: prevState.translateValue + (prevState.currentIndex - index) * this.slideWidth(),
    }));
  };

  startAtSlide = index => {
    try {
      const { movesize } = this.props;
      const sliderElement = document.querySelector(`.slider.${this.props.sliderClass}`);

      const nextButton = sliderElement.querySelector(".btnNext");
      // returns the slider node
      const parent = this.getClosestParent(nextButton, ".slider");
      const clientWidth = parent.querySelector(".slide") && parent.querySelector(".slide").clientWidth;

      if (parent && nextButton && clientWidth) {
        const slideWidth = clientWidth * parseInt(movesize);

        this.setState(prevState => ({
          currentIndex: index,
          // Because will only move towards RIGHT -> using startIndex
          translateValue: 0 - index * slideWidth,
        }));
      }
    } catch (error) {
      console.log(error);
    }
  };

  getClosestParent = (elem, selector) => {
    for (; elem && elem !== document; elem = elem.parentNode) {
      // Polyfill for IE
      if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
      }
      if (elem.matches(selector)) return elem;
    }
    return null;
  };

  slideWidth = () => {
    let { size, movesize, type, margin } = this.props;
    movesize = movesize || size;
    // const self = this;
    // const margin = self.props.margin ? parseInt(self.props.margin) : 0;
    // // // return self.props.width ? parseInt(self.props.width) + parseInt(margin) : 300;
    // let slideWidth =
    //   parseInt(self.props.sliderWidth) * parseInt(movesize) + margin;
    // return slideWidth;
    // const classSelector = sliderClass ? `.${sliderClass} .slide` : ".slide";

    try {
      // returns the slider node
      const parent = this.getClosestParent(event.target, ".slider");
      let slidWidth = parent.querySelector(".slide").clientWidth;
      slidWidth = type == "schedule" ? slidWidth + parseInt(margin) : slidWidth;
      return (
        // document.querySelector(classSelector).clientWidth * parseInt(movesize)
        slidWidth * parseInt(movesize)
        // event.target.parentElement.querySelector(".slide").clientWidth *
        // parseInt(movesize)
      );
    } catch (error) {
      console.log(error);
    }
  };

  getUpdatedData = (data, size, sliderInfo) => {
    const newArr = [];
    let newdata = [];
    if (data) {
      if (Array.isArray(data) && data.length > 0) {
        newdata = [...data];
      } else {
        newdata = [data];
      }
      if (sliderInfo.type === "grid" && !sliderInfo.designConfig) {
        newdata = newdata.filter(card => card.tn !== "ad");
      }
      while (newdata.length > 0) {
        newArr.push(newdata.splice(0, size));
      }
    }
    return newArr;
  };

  componentWillReceiveProps(nextProps) {
    if (this.state.sliderData !== nextProps.sliderData) {
      this.setState({
        data: nextProps.sliderData,
      });
    }
  }

  render() {
    const { translateValue, data } = this.state;
    let {
      type,
      subtype,
      videoIntensive,
      size,
      width,
      height,
      heading,
      sliderClass,
      link,
      sliderWidth,
      clicked,
      headingRequired,
      margin,
      playerConfig,
      compType,
      parentMsid,
      className,
      imgsize,
      noSeo,
      sliderClick,
      tag,
      designConfig,
    } = this.props;

    // in case of type video, slider will by default open video in pop up
    if (typeof videoIntensive === "undefined" && type === "video") {
      videoIntensive = false;
    }
    margin = margin ? parseInt(margin) : 0;
    const containerwdt = `${(parseInt(width) + margin) * size - margin}px`;
    const updatedArr = data ? this.getUpdatedData(data, size, { type, designConfig }) : [];
    return (
      <React.Fragment>
        {headingRequired !== "no" && heading && <h2>{heading}</h2>}
        <div className={`slider ${sliderClass}`} style={{ width: isMobilePlatform() ? "auto" : containerwdt }}>
          {/* slider width:- (width + margin) multiply by number of items and margin removed from last item */}
          <div className="hidden_layer">
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: "transform ease-out 0.45s",
              }}
            >
              <ul>
                {Array.isArray(updatedArr)
                  ? updatedArr.map((item, index) => (
                      <ManageSlider
                        key={`${item[0].id ? item[0].id : index.toString()}_slider`}
                        type={type}
                        subtype={subtype}
                        videoIntensive={videoIntensive}
                        data={item}
                        width={width}
                        height={height}
                        sliderWidth={sliderWidth}
                        clicked={clicked}
                        margin={margin}
                        playerConfig={playerConfig}
                        props={this.props}
                        istrending={this.props.istrending}
                        compType={compType}
                        parentMsid={parentMsid}
                        className={className}
                        link={link}
                        imgsize={imgsize}
                        noSeo={noSeo}
                        sliderClick={sliderClick}
                        tag
                        designConfig={designConfig}
                      />
                    ))
                  : null}
              </ul>
            </div>
          </div>
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <LeftArrow goToPrevSlide={this.goToPrevSlide.bind(this)} />
          )}
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <RightArrow goToNextSlide={this.goToNextSlide.bind(this, updatedArr.length)} />
          )}
          {this.props.slideIndicator ? (
            <div className="slideIndicator">
              {updatedArr.map((slide, index) => (
                <SlideIndicator
                  key={index}
                  index={index}
                  currentIndex={this.state.currentIndex}
                  isActive={this.state.currentIndex == index}
                  onClick={this.goToSlide.bind(this, index)}
                />
              ))}
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

const SlideIndicator = props => (
  <a className={props.index == props.currentIndex ? "active" : ""} onClick={props.onClick} />
);

const ManageSlider = ({
  type,
  subtype,
  videoIntensive,
  data,
  width,
  height,
  sliderWidth,
  clicked,
  margin,
  playerConfig,
  istrending,
  props,
  compType,
  parentMsid,
  className,
  link,
  imgsize,
  noSeo,
  sliderClick,
  designConfig,
}) => {
  let slider = "";

  switch (type) {
    case "zodiac_sign":
      slider = (
        <li key="zodiac_sign">
          {data.map((item, index) => (
            <AnchorLink key={index.toString()} href={`${link}?story=${item.pos}`}>
              <div
                className="slide"
                style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                key={item.name}
                role="button"
                tabIndex={0}
                onClick={clicked}
                onKeyUp={clicked}
              >
                <div className={`astro-sign sign-${item.name}`} id={item.name} />
                <div className="txt">{item.regName}</div>
              </div>
            </AnchorLink>
          ))}
        </li>
      );
      break;

    case "embedslider":
      slider = (
        <li key="embedslider">
          {Array.isArray(data)
            ? data.map(item => {
                const title = item.imgtitle;
                return (
                  <div className="slide" key={item.id} style={{ width: `${width}px` }}>
                    <ImageCard
                      msid={item.id ? item.id : "69347424"}
                      title={title}
                      size="largethumb"
                      className="candidate_img"
                      imgver={item.imgsize}
                    />
                    <div className="embedslide_desc" dangerouslySetInnerHTML={{ __html: item.cap }}></div>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;

    case "grid":
      // Do not put imgsize here, it would be handled by gridcardmaker itself
      slider = (
        <React.Fragment>
          {Array.isArray(data)
            ? data.map((card, index) => {
                if (card.tn === "ad" && designConfig && designConfig.offads) {
                  return card.platform && card.platform !== "" && card.platform !== process.env.PLATFORM ? null : (
                    <li
                      key={index.toString()}
                      style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                      className={`news-card ${card.type} col${card.adcols ? card.adcols : ""}`}
                    >
                      {<AdCard mstype={card.mstype} adtype={card.type} />}
                    </li>
                  );
                }
                return (
                  card &&
                  card.imageid &&
                  card.imageid !== "" && (
                    <GridCardMaker
                      card={card}
                      key={card.imageid}
                      keyName={card.imageid}
                      cardType={card.tn}
                      className={`slide ${className}`}
                      columns=""
                      imgsize={imgsize}
                      width={width}
                      margin={margin}
                      index={index}
                      istrending={props.istrending}
                      compType={compType}
                      parentMsid={parentMsid}
                      noSeo={noSeo}
                      sliderClick={sliderClick}
                      videoIntensive={videoIntensive}
                    />
                  )
                );
              })
            : null}
        </React.Fragment>
      );
      break;

    case "iplwidgets":
      if (compType == "minischedule") {
        slider = <ScheduleCard compType={compType} schedule={data} classes={"slide"} />;
      } else if (compType == "venues") {
        slider = (
          <VenueComp
            items={data}
            type="slider"
            classes={"slide news-card"}
            style={{ width: `${width}px`, paddingRight: `${margin}px` }}
          />
        );
      } else if (compType == "players") {
        slider = (
          <GetPlayerDetails
            items={data}
            classes={"slide news-card"}
            style={{ width: `${width}px`, paddingRight: `${margin}px` }}
          />
        );
      }
      break;
    case "webstories":
      // Do not put imgsize here, it would be handled by gridcardmaker itself
      slider = (
        <React.Fragment>
          {Array.isArray(data)
            ? data.map((card, index) => {
                return (
                  card &&
                  card.imageid &&
                  card.imageid !== "" && (
                    <GridCardMaker
                      card={card}
                      key={card.imageid}
                      keyName={card.imageid}
                      cardType="webstories"
                      className={`slide ${className}`}
                      columns=""
                      imgsize={imgsize}
                      width={width}
                      margin={margin}
                      index={index}
                      istrending={props.istrending}
                      compType={compType}
                      parentMsid={parentMsid}
                      noSeo={noSeo}
                      sliderClick={sliderClick}
                      videoIntensive={videoIntensive}
                    />
                  )
                );
              })
            : null}
        </React.Fragment>
      );
      break;

    case "timespointcheckin":
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => (
                <div key={item.date} className="slide" style={{ width: `${width}px`, paddingRight: `${margin}px` }}>
                  <div className={`checkin ${item.bonusClass} `}>
                    <i className="icon tp-sprite" />
                    <h5>{item.bonusearned}</h5>
                    <span className="date">{item.dateString}</span>
                  </div>
                </div>
              ))
            : null}
        </li>
      );
      break;
    case "castncrew":
      slider = (
        <li key="slider" itemrop="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => {
                const title = item.label + item.name;
                return (
                  <div
                    className="slide"
                    key={item.imageid}
                    style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                  >
                    <AnchorLink key={title} href={`${item.overridelink}`}>
                      <span className="img_wrap">
                        <ImageCard
                          msid={item.imgid}
                          title={title}
                          size="smallthumb"
                          className="candidate_img"
                          imgsize={item.imgsize}
                        />
                      </span>
                      <span className="con_wrap">
                        <span className="text_ellipsis">{item.name}</span>
                      </span>
                      <span className="con_wrap">
                        <span className="text_ellipsis">{item.label}</span>
                      </span>
                    </AnchorLink>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
    default:
      slider = (
        <li key="slider" itemrop="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => {
                const title = type === "showcase" ? item.syn : item.hl;
                return (
                  <div
                    className="slide"
                    key={item.imageid}
                    style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                  >
                    <a href="#" onClick={e => e.preventDefault()}>
                      <span className="img_wrap">
                        <ImageCard
                          msid={item.imageid || "69347424"}
                          title={title}
                          size="smallthumb"
                          className="candidate_img"
                          imgver={item.imgsize}
                        />
                      </span>

                      <span className="con_wrap">
                        <span className="text_ellipsis">{item.hl}</span>
                      </span>
                    </a>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
  }
  return slider;
};

Slider.propTypes = {
  type: PropTypes.string,
  sliderData: PropTypes.array,
  size: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
  link: PropTypes.string,
  sliderWidth: PropTypes.string,
  clicked: PropTypes.func,
  headingRequired: PropTypes.string,
};

ManageSlider.propTypes = {
  data: PropTypes.array,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
};

const LeftArrow = props => {
  const { goToPrevSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      className="btnPrev"
      onClick={goToPrevSlide}
      onKeyUp={goToPrevSlide}
    />
  );
};

const RightArrow = props => {
  const { goToNextSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      pg="nxtphoto"
      className="btnNext"
      onClick={goToNextSlide}
      onKeyUp={goToNextSlide}
    />
  );
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

export default Slider;
