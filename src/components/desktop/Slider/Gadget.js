/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "./HP_slider.scss";
import ImageCard from "../../common/ImageCard/ImageCard";
import GridCardMaker from "../../common/ListingCards/GridCardMaker";
import AdCard from "../../common/AdCard";
import AnchorLink from "../../common/AnchorLink";
import { _getStaticConfig, isMobilePlatform, getBuyLink } from "../../../utils/util";
import gadgetConfig from "../../../utils/gadgetsConfig";
import GNRatingLink from "../../common/Gadgets/GNRatingLink";

const siteConfig = _getStaticConfig();
const techLocale = siteConfig && siteConfig.locale && siteConfig.locale.tech;

const brandUrlMapping = gadgetConfig.gadgetCategories;

const launchDate = techLocale.launchdate;

class Slider extends PureComponent {
  constructor(props) {
    super(props);
    const { sliderData } = this.props;
    this.state = {
      data: sliderData,
      currentIndex: 0,
      translateValue: 0,
    };
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue, 10) + parseInt(this.slideWidth(), 10),
    }));
  };

  componentWillReceiveProps(nextProps) {
    let { data } = this.state;
    const { sliderData } = this.props;
    if (
      sliderData &&
      sliderData[0] &&
      nextProps.sliderData &&
      nextProps.sliderData[0] &&
      nextProps.sliderData[0].uname !== sliderData[0].uname
    ) {
      data = nextProps.sliderData;
      this.setState({ data });
    }
  }

  goToNextSlide = datalength => {
    // Exiting the method early if we are at the end of the images array.
    // We also want to reset currentIndex and translateValue, so we return
    // to the first image in the array.
    const { currentIndex } = this.state;

    if (currentIndex === parseInt(datalength, 10) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth(),
    }));

    return 0;
  };

  goToSlide = index => {
    this.setState(prevState => ({
      currentIndex: index,
      translateValue: prevState.translateValue + (prevState.currentIndex - index) * this.slideWidth(),
    }));
  };

  getClosestParent = (elem, selector) => {
    for (; elem && elem !== document; elem = elem.parentNode) {
      if (elem.matches(selector)) return elem;
    }
    return null;
  };

  slideWidth = () => {
    let { size, movesize } = this.props;
    movesize = movesize || size;
    // const self = this;
    // const margin = self.props.margin ? parseInt(self.props.margin) : 0;
    // // // return self.props.width ? parseInt(self.props.width) + parseInt(margin) : 300;
    // let slideWidth =
    //   parseInt(self.props.sliderWidth) * parseInt(movesize) + margin;
    // return slideWidth;
    // const classSelector = sliderClass ? `.${sliderClass} .slide` : ".slide";

    try {
      // returns the slider node
      const parent = this.getClosestParent(event.target, ".slider");
      const slidWidth = parent.querySelector(".slide").clientWidth;
      return (
        // document.querySelector(classSelector).clientWidth * parseInt(movesize)
        slidWidth * parseInt(movesize)
        // event.target.parentElement.querySelector(".slide").clientWidth *
        // parseInt(movesize)
      );
    } catch (error) {
      console.log(error);
    }
  };

  getUpdatedData = (data, size) => {
    const newArr = [];
    const newdata = [...data];
    while (newdata.length > 0) {
      newArr.push(newdata.splice(0, size));
    }
    return newArr;
  };

  render() {
    const { translateValue, data } = this.state;
    let {
      type,
      subtype,
      videoIntensive,
      size,
      width,
      height,
      heading,
      sliderClass,
      link,
      sliderWidth,
      clicked,
      headingRequired,
      margin,
      playerConfig,
      compType,
      parentMsid,
      className,
      imgsize,
      noSeo,
      sliderClick,
      toggle,
      brandClicked,
      submitRating,
      gadgetItemClick,
      amazonBuyClick,
      addFromSuggestions,
      isComparison,
      gadgetCategory,
      amazonTag,
    } = this.props;

    // in case of type video, slider will by default open video in pop up
    if (typeof videoIntensive === "undefined" && type === "video") {
      videoIntensive = false;
    }
    margin = margin ? parseInt(margin) : 0;
    const updatedArr = data ? this.getUpdatedData(data, size) : [];
    return (
      <React.Fragment>
        {headingRequired !== "no" && heading && <h2>{heading}</h2>}
        <div
          className={`slider ${sliderClass}`}
          style={width ? { width: `${(parseInt(width) + margin) * size - margin}px` } : null}
        >
          {/* slider width:- (width + margin) multiply by number of items and margin removed from last item */}
          <div className="hidden_layer">
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: "transform ease-out 0.45s",
              }}
            >
              <ul>
                {Array.isArray(updatedArr)
                  ? updatedArr.map((item, index) => (
                      <ManageSlider
                        key={`${item && item[0].id ? item[0].id : index.toString()}_slider`}
                        type={type}
                        subtype={subtype}
                        toggle={toggle}
                        videoIntensive={videoIntensive}
                        data={item}
                        width={width}
                        height={height}
                        sliderWidth={sliderWidth}
                        clicked={clicked}
                        margin={margin}
                        playerConfig={playerConfig}
                        props={this.props}
                        istrending={this.props.istrending}
                        compType={compType}
                        parentMsid={parentMsid}
                        className={className}
                        link={link}
                        imgsize={imgsize}
                        noSeo={noSeo}
                        sliderClick={sliderClick}
                        brandClicked={brandClicked}
                        submitRating={submitRating}
                        gadgetItemClick={gadgetItemClick}
                        amazonBuyClick={amazonBuyClick}
                        gadgetCategory={gadgetCategory}
                        addFromSuggestions={addFromSuggestions}
                        isComparison={isComparison}
                        amazonTag={amazonTag}
                      />
                    ))
                  : null}
              </ul>
            </div>
          </div>
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <LeftArrow goToPrevSlide={this.goToPrevSlide.bind(this)} />
          )}
          {Array.isArray(updatedArr) && updatedArr.length > 1 && (
            <RightArrow goToNextSlide={this.goToNextSlide.bind(this, updatedArr.length)} />
          )}
          {this.props.slideIndicator ? (
            <div className="slideIndicator">
              {updatedArr.map((slide, index) => (
                <SlideIndicator
                  key={index}
                  index={index}
                  currentIndex={this.state.currentIndex}
                  isActive={this.state.currentIndex == index}
                  onClick={this.goToSlide.bind(this, index)}
                />
              ))}
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

const SlideIndicator = props => (
  <a className={props.index == props.currentIndex ? "active" : ""} onClick={props.onClick} />
);

const ManageSlider = ({
  type,
  data,
  width,
  margin,
  toggle,
  brandClicked,
  submitRating,
  gadgetItemClick,
  amazonBuyClick,
  addFromSuggestions,
  isComparison,
  gadgetCategory,
  amazonTag,
}) => {
  let slider = "";
  switch (type) {
    case "inlineGadgets":
      slider = Array.isArray(data)
        ? data.map((card, index) => {
            const averageRating = card.averageRating === "" ? "NA" : (parseFloat(card.averageRating) / 2).toFixed(1);
            const gadgetLink = `${siteConfig.mweburl}/tech/${brandUrlMapping[gadgetCategory]}/${card.lcuname}`;
            const deviceName = card.regional ? card.regional.name : card.name;
            const amazonBuyLink = getBuyLink({ data: card });
            const ram = (card && card.keyFeatures && card.keyFeatures.ram) || "";
            const storage = (card && card.keyFeatures && card.keyFeatures.storage) || "";

            return (
              <li key={`${card.lcuname}-${card.price}-${toggle}-${index}`}>
                <div
                  className="slide rumoured"
                  style={{ width: `${width}px`, marginRight: `${margin}px` }}
                  //  onClick={gadgetItemClick(card)}
                >
                  <div>
                    {card.rumoured == 1 ? (
                      <div className="top-caption" data-attr={techLocale.rumoured} />
                    ) : card.upcoming == 1 ? (
                      <div className="top-caption" data-attr={techLocale.upcoming} />
                    ) : (
                      ""
                    )}
                  </div>
                  <span className="prod_img">
                    <AnchorLink href={gadgetLink}>
                      <ImageCard
                        msid={card.imageMsid && card.imageMsid[0] ? card.imageMsid[0] : siteConfig.imageconfig.defaultthumb}
                        title={card.uname}
                        size="gnthumb"
                        className="candidate_img"
                        // imgver={item.imgsize}
                      />
                    </AnchorLink>
                  </span>
                  <span className="gadget_detail">
                    <span className="title">
                      <span className="text_ellipsis" title={deviceName}>
                        <AnchorLink title={deviceName} href={gadgetLink} className="text_ellipsis">
                          {deviceName}
                        </AnchorLink>
                      </span>
                    </span>

                    {ram && storage && (
                      <ul className="ram-storage">
                        <li>
                          <label>{ram.key} :</label> <b>{ram.value}</b>
                        </li>
                        <li>
                          <label>{storage.key} :</label> <b>{storage.value}</b>
                        </li>
                      </ul>
                    )}

                    <GNRatingLink card={card} submitRating={submitRating} />

                    <span className="price">
                      <b className="symbol_rupees">{card.price ? card.price : "NA"} </b>
                    </span>
                    {card.affiliate && (card.affiliate.exact || card.affiliate.related) ? (
                      <span className="btn_wrap">
                        <a href={amazonBuyLink} target="_blank">
                          <button className="btn-buy">{techLocale.buyAtAmazon}</button>
                        </a>
                      </span>
                    ) : null}
                    {isMobilePlatform() && card && card.launch_date ? (
                      <span className="gd_launch">
                        {launchDate} : {card.launch_date}
                      </span>
                    ) : null}
                  </span>
                </div>
              </li>
            );
          })
        : null;
      break;

    case "comparisonGadgets":
      // gadgets items for homepage
      slider = Array.isArray(data)
        ? data.map((card, index) => {
            //  const buyLink = getBuyLink({ data: card });
            const deviceName = card.regional && card.regional[0] ? card.regional[0].name : card.name;
            let imageId = siteConfig.imageconfig.defaultthumb;
            if (typeof card.imageMsid === "string") {
              imageId = card.imageMsid;
            } else if (card.imageMsid && card.imageMsid[0]) {
              imageId = card.imageMsid[0];
            }

            return (
              <li key={`${card.uname}-${card.price}`}>
                <div
                  className="slide rumoured"
                  style={{ width: `${width}px`, marginRight: `${margin}px` }}
                  onClick={gadgetItemClick(card)}
                >
                  <span onClick={addFromSuggestions(card)} className="plus_icon"></span>
                  <span className="prod_img">
                    <ImageCard
                      msid={imageId}
                      title={card.uname}
                      size="gnthumb"
                      className="candidate_img"
                      // imgver={item.imgsize}
                    />
                  </span>
                  <span className="title text_ellipsis" title={deviceName}>
                    {deviceName}
                  </span>
                  <span className="price">
                    <b className="symbol_rupees">{card.price ? card.price : "NA"} </b>
                  </span>
                  {card.affiliate && (card.affiliate.exact || card.affiliate.related) ? (
                    <span className="btn_wrap">
                      <button onClick={amazonBuyClick(card)} className="btn-buy">
                        {techLocale.buyAtAmazon}
                      </button>
                    </span>
                  ) : null}
                  {isMobilePlatform() && card && card.launch_date ? (
                    <span className="gd_launch">
                      {launchDate} : {card.launch_date}
                    </span>
                  ) : null}
                </div>
              </li>
            );
          })
        : null;
      break;

    case "gadgetShow":
      // gadgets items for homepage

      const category = data && data[0] && data[0].category;
      slider = Array.isArray(data)
        ? data.map((card, index) => {
            const productLink = `${siteConfig.mweburl}/tech/${gadgetConfig.gadgetCategories[category]}/${card.lcuname}`;

            // const deviceName = card.regional && card.regional[0] ? card.regional[0].name : card.name;
            let deviceName = "";
            if (card.regional && Array.isArray(card.regional)) {
              deviceName = card.regional.filter(item => item.host == siteConfig.hostid);
              deviceName = deviceName && deviceName[0] && deviceName[0].name;
            }

            let imageId = siteConfig.imageconfig.defaultthumb;
            if (typeof card.imageMsid === "string") {
              imageId = card.imageMsid;
            } else if (card.imageMsid && card.imageMsid[0]) {
              imageId = card.imageMsid[0];
            }

            return (
              <li key={`${card.uname}-${card.price}`}>
                <div className="slide rumoured" style={{ width: `${width}px`, marginRight: `${margin}px` }}>
                  <AnchorLink href={productLink}>
                    <span className="prod_img">
                      <ImageCard
                        msid={imageId}
                        title={card.uname}
                        size="gnthumb"
                        className="candidate_img"
                        // imgver={item.imgsize}
                      />
                    </span>
                    <span className="title text_ellipsis" title={deviceName}>
                      {deviceName}
                    </span>
                    <span className="price">
                      <b className="symbol_rupees">{card.price ? card.price : "NA"} </b>
                    </span>

                    {isMobilePlatform() && card && card.launch_date ? (
                      <span className="gd_launch">
                        {launchDate} : {card.launch_date}
                      </span>
                    ) : null}
                  </AnchorLink>
                </div>
              </li>
            );
          })
        : null;
      break;

    case "2gudwidgetslider":
      // console.log('2gudwidgetslider', sliderData);
      slider = (
        <li className="gn-amazon-listitem">
          {data.map(item => {
            const twogudga = `${item.title}_${item.price}`;
            const affid = "gadgetsnow";
            const afname = "2gud";
            const url = encodeURIComponent(`${item.url}&afname=${afname}&affextparam2=${tag}`);
            const price = item.price;
            const listPrice = item.listPrice;
            const affiliateUrl = `https://navbharattimes.indiatimes.com/tech/affiliate_2gud.cms?url=${url}&price=${price}&title=${item.title}&twogud_ga=${twogudga}&affid=${affid}&afname=${afname}&affextparam2=${tag}`;

            // console.log(process.env.WEBSITE_URL);
            // console.log("tag :"+tag);
            // console.log("twogudga :"+twogudga);
            // console.log("url :"+url);
            // console.log("price :"+price);
            // console.log("listPrice :"+listPrice);
            // console.log("affiliateUrl :"+affiliateUrl);

            return (
              <div className="slide" key={item.title}>
                <AnchorLink title={item.title} to={affiliateUrl} rel="nofollow" target="_blank">
                  <span className="img_wrap">
                    <ImageCard src={item.imageUrl} type="absoluteImgSrc" alt={item.title} title={item.title} />
                  </span>

                  <span className="con_wrap">
                    <h4 className="text_ellipsis">{item.title}</h4>

                    <span className="price_tag">
                      &#8377; {price}
                      {listPrice !== undefined && listPrice > 0 && price != listPrice ? (
                        <b>&#8377; {listPrice}</b>
                      ) : null}
                    </span>
                  </span>
                  <span className="btn_wrap">
                    <button className="buy_btn">{techLocale.purchase}</button>
                  </span>
                </AnchorLink>
              </div>
            );
          })}
        </li>
      );
      break;

    case "brandSlider":
      slider = Array.isArray(data)
        ? data.map((card, index) => (
            <li key={index}>
              <div className="slide" style={{ width: `${width}px` }} onClick={brandClicked(card)}>
                <img src={card.image} alt={card.name} title={card.name} />
                <span className="txt_container">
                  <span className="text_ellipsis">{card.name}</span>
                </span>
              </div>
            </li>
          ))
        : null;
      break;
    case "amazonwidgetslider":
      slider = (
        <li>
          {data.map(item => {
            if (item.affiliate === "amazon") {
              const price = item.price;
              const amzga = `${item.pid}_${price}`;

              // const affiliateTag =
              //   (siteConfig && siteConfig.affiliateTags && siteConfig.affiliateTags.CD) || '';

              const title = item.title;
              const url = item.url;
              const buyURL = getBuyLink({
                url,
                price,
                title,
                amzga,
                tag: amazonTag,
              });

              // console.log('buyURL', buyURL);
              const listPrice = item.listPrice;
              return (
                <div className="slide" key={item.pid} style={{ width: `${width}px`, marginRight: `${margin}px` }}>
                  <a title={item.title} href={buyURL} rel="nofollow" target="_blank">
                    <span className="prod_img">
                      <img
                        src={item.imageUrl}
                        imgSize=""
                        width=""
                        height=""
                        alt={item.title}
                        title={item.title}
                        islinkable=""
                        link=""
                      />
                    </span>
                    <span className="prod_con">
                      <span className="title text_ellipsis">{item.title}</span>
                      <span className="price_tag">
                        &#8377; {price}
                        {listPrice !== undefined && listPrice > 0 && price != listPrice ? (
                          <b>&#8377; {listPrice}</b>
                        ) : null}
                      </span>
                    </span>
                    <span className="btn_wrap">
                      <button className="btn-buy">{techLocale.purchase}</button>
                    </span>
                  </a>
                </div>
              );
            }
          })}
        </li>
      );

      break;

    default:
      slider = (
        <li key="slider" itemrop="itemListElement" itemScope="1" itemType="http://schema.org/ListItem">
          {Array.isArray(data)
            ? data.map(item => {
                const title = type === "showcase" ? item.syn : item.hl;
                return (
                  <div
                    className="slide"
                    key={item.imageid}
                    style={{ width: `${width}px`, paddingRight: `${margin}px` }}
                  >
                    <a href="#" onClick={e => e.preventDefault()}>
                      <span className="img_wrap">
                        <ImageCard
                          msid={item.imageid || siteConfig.imageconfig.defaultthumb}
                          title={title}
                          size="smallthumb"
                          className="candidate_img"
                          imgver={item.imgsize}
                        />
                      </span>

                      <span className="con_wrap">
                        <span className="text_ellipsis">{item.hl}</span>
                      </span>
                    </a>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
  }
  return slider;
};

Slider.propTypes = {
  type: PropTypes.string,
  sliderData: PropTypes.array,
  size: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
  link: PropTypes.string,
  sliderWidth: PropTypes.string,
  clicked: PropTypes.func,
  headingRequired: PropTypes.string,
};

ManageSlider.propTypes = {
  data: PropTypes.array,
  width: PropTypes.string,
  height: PropTypes.string,
  sliderClass: PropTypes.string,
};

const LeftArrow = props => {
  const { goToPrevSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      className="btnPrev"
      onClick={goToPrevSlide}
      onKeyUp={goToPrevSlide}
    />
  );
};

const RightArrow = props => {
  const { goToNextSlide } = props;
  return (
    <span
      role="button"
      tabIndex={0}
      data-exclude="amp"
      pg="nxtphoto"
      className="btnNext"
      onClick={goToNextSlide}
      onKeyUp={goToNextSlide}
    />
  );
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

export default Slider;
