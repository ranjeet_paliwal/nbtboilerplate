/* 
    Designed to be an independant component to be included in desktop
    that will be updated by one reducer, and update itself automatically.
    (common across VS, AS etc)

*/

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import ImageCard from "../common/ImageCard";
import fetch from "../../utils/fetch/fetch";
import ErrorBoundary from "../lib/errorboundery/ErrorBoundary";
import ArticlesListBlock from "./ArticleListBlock";
import NewsLetter from "../common/Newsletter";
import AnchorLink from "../common/AnchorLink";
import AdCard from "../common/AdCard";
// import TopStoryWidget from "../common/TopStoryWidget";
import { _getStaticConfig, _isCSR, isProdEnv, isTechSite, isTechPage } from "../../utils/util";
import VideoItem from "../common/VideoItem/VideoItem";
import GridSectionMaker from "../common/ListingCards/GridSectionMaker";
import RecommendedNewsWidget from "../common/RecommendedNewsWidget";
import { defaultDesignConfigs } from "../../containers/defaultDesignConfigs";
import YSubscribeCard from "../common/YSubscribeCard";
import DataDrawer from "./DataDrawer";
import BriefWidget from "../common/BriefWidget";
// import Gaana from "../desktop/GaanaWidget/GaanaWidget";

const siteConfig = _getStaticConfig();
const taswidgetNBTPath = "https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas_new.cms";
const taswidgetMTPath = "https://maharashtratimes.com/pwafeeds/amazon_wdt_tas_new.cms";
const siteName = process.env.SITE;

//FIXME: Change Config import and same component as on mobile for RHS SSR SECTION
class Rhswidget extends Component {
  constructor(props) {
    super(props);
    this.state = { viralAddaData: null };
    this.resizeObserver = undefined;
  }

  componentDidMount() {
    this.getRhsData();
    // const newElement = document.getElementById("rhs-mrec-container");
    // if (newElement) {
    //   console.log(newElement);
    //   this.observeResizeChange(newElement);
    // }
    // if (!this.state.alaskBanner && typeof window && window.bannerData) {
    //   this.setState({
    //     alaskBanner: bannerData,
    //   });
    // }
  }

  componentDidUpdate() {
    // if (!this.resizeObserver) {
    //   const newElement = document.getElementById("rhs-mrec-container");
    //   if (newElement) {
    //     this.observeResizeChange(newElement);
    //   }
    // }
  }

  componentWillUnmount() {
    // if (this.resizeObserver) {
    //   this.resizeObserver.disconnect();
    // }
  }

  // observeResizeChange(elem) {
  //   const resizeObserver = new ResizeObserver(resizedEntry => {
  //     try {
  //       const resizedHeight = Math.max(resizedEntry[0].contentRect.height - 250, 0);
  //       if (document.getElementById("rhs-other-container")) {
  //         document.getElementById("rhs-other-container").style.transform = "translateY(" + resizedHeight + "px)";
  //       }
  //     } catch (e) {
  //       console.log(e);
  //     }
  //   });
  //   this.resizeObserver = resizeObserver;
  //   resizeObserver.observe(elem);
  // }

  getRhsData = () => {
    // fetch(
    //   `https://navbharattimesfeeds.indiatimes.com/pwafeeds/web_common.cms?feedtype=sjson&&platform=web&msid=${secId}&tag=video,ibeatmostread,mostpopularL2,mostpopularL1,trending`
    // )
    //   .then(data => {
    //     this.setState({ rhsCSRData: data });
    //   })
    //   .catch({});
    //   fetch(`${process.env.API_BASEPOINT}/pwafeeds/web_combine.cms?tag=viraladda&donotshowurl=1&feedtype=sjson`)
    //     .then(viralAddaData => {
    //       this.setState({ viralAddaData });
    //     })
    //     .catch({});
  };

  render() {
    const {
      rhsCSRData,
      rhsCSRVideoData,
      rhsData,
      articleId,
      articleSecId,
      sectionType,
      isFeaturedArticle,
      dispatch,
      showDataDrawer,
      pwaConfigAlaska,
      pagetype,
      hideInElections,
      pathname,
      bannerdata,
      router,
      subsecname,
    } = this.props;

    const { alaskBanner } = this.state;
    const apanaBazarSec = siteConfig.pages && siteConfig.pages.apanabazaar === articleSecId ? true : false;
    if (!rhsData) {
      return null;
    }
    // For nbt, amp widget works from MT domain
    // let amazonIframeDataSource =
    //   process.env.SITE === "nbt"
    //     ? `${taswidgetMTPath}?host=${process.env.SITE}&tag=amp`
    //     : `${taswidgetNBTPath}?host=${process.env.SITE}&tag=amp`;

    return (
      <div>
        {!isFeaturedArticle && (
          <div class="rhs-mrec-wrapper">
            <div id="rhs-mrec-container">
              <div className="box-item">
                {/*  Add prerender parameter here */}
                <AdCard mstype="mrec1" adtype="dfp" className="ad-top" />
              </div>
            </div>
          </div>
        )}

        <div id="rhs-other-container" className="rhs-other-container">
          {!isFeaturedArticle && (
            <React.Fragment>
              {/* amazon banner  alaska */}
              {bannerdata && typeof bannerdata == "object" && bannerdata._type === "banner" ? (
                <ErrorBoundary key={"amz_banner"}>
                  <div className="box-item">
                    <a className="banner" href={bannerdata && bannerdata._override} target="_blank" rel="nofollow">
                      <img src={bannerdata && bannerdata._image} />
                    </a>
                  </div>
                </ErrorBoundary>
              ) : null}

              {showDataDrawer ? (
                <div className="box-item section-wrapper dataDrawerContainer">
                  {_isCSR() && typeof dispatch == "function" ? <DataDrawer dispatch={dispatch} /> : null}
                </div>
              ) : null}

              {/* BCCL CTN */}
              {!hideInElections && (
                <div className="box-item adblockdiv">
                  <AdCard className="emptyAdBox" mstype="ctnbccl" adtype="ctn" />
                </div>
              )}
            </React.Fragment>
          )}
          {/* Brief Widget Start */}
          {pathname && !pathname.includes("/newsbrief") && (
            <ErrorBoundary>
              <div className="briefParent">
                <BriefWidget pagetype="articleshow" />
              </div>
            </ErrorBoundary>
          )}
          {/* Brief Widget End */}
          {/* For nbt and vk tech pages, widget should not come */}
          {pwaConfigAlaska &&
          (pwaConfigAlaska._topdeal == "true" || pwaConfigAlaska._topoffer == "true") &&
          ((isTechSite() && !isTechPage(router && router.location && router.location.pathname)) || !isTechSite()) ? (
            <div className="wdt_amz pwa-deals">
              <iframe
                scrolling="no"
                frameBorder="no"
                height="400"
                src={`${taswidgetNBTPath}?host=${process.env.SITE}&platform=desktop&type=vertical&wdttype=${
                  pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                }&msid=${articleId}&category=${subsecname}`}
              />
            </div>
          ) : null}

          {/* Recommended news - spotlight cabre in design */}
          {/* condition added for nbt apna bazar  */}
          {!apanaBazarSec ? (
            <ErrorBoundary>
              {/*  Video widget */}
              {rhsCSRVideoData ? <VideoSection data={rhsCSRVideoData} /> : null}

              <RecommendedNewsWidget
                articleId={articleId}
                sectionType={sectionType}
                isFeaturedArticle={isFeaturedArticle}
              />
              {/* {rhsCSRData &&
              rhsCSRData.section &&
              Array.isArray(rhsCSRData.section) &&
              rhsCSRData.section[0] &&
              rhsCSRData.section[0].secname && <VideoSection data={rhsCSRData.section[0]} />} */}

              {/* gaana widget for NBT only  */}
              {/* {process.env.SITE == "nbt" ? <Gaana /> : ""} */}

              {!hideInElections && <NewsLetter pagetype={"articleshow"} articleid={articleId} />}

              {!isFeaturedArticle && pagetype !== "liveblog" && pagetype !== "brief" && (
                <div className="box-item">
                  <AdCard mstype="mrec3" adtype="dfp" />
                </div>
              )}

              {pagetype === "brief" && (
                <div className="box-item briefad">
                  <AdCard mstype="mrec2" adtype="dfp" />
                </div>
              )}

              {!isFeaturedArticle && pagetype === "liveblog" && (
                <div className="box-item">
                  <AdCard mstype="mrec2" adtype="dfp" />
                </div>
              )}
            </ErrorBoundary>
          ) : (
            <div>
              {rhsCSRData &&
              rhsCSRData.section &&
              Array.isArray(rhsCSRData.section) &&
              rhsCSRData.section[2] &&
              rhsCSRData.section[2].items ? (
                <ErrorBoundary>
                  <div className="box-item rest-releated-news">
                    <div className="section">
                      <div className="top_section">
                        <h3>
                          {rhsCSRData && rhsCSRData.section[2] ? (
                            <span>
                              <AnchorLink href={rhsCSRData.section[2].wu}>{rhsCSRData.section[2].secname}</AnchorLink>
                            </span>
                          ) : (
                            <span>{rhsCSRData && rhsCSRData.section[2] && rhsCSRData.section[2].secname}</span>
                          )}
                        </h3>
                      </div>
                    </div>
                    <GridSectionMaker
                      type={defaultDesignConfigs.topicslisting}
                      data={
                        rhsCSRData &&
                        rhsCSRData.section &&
                        Array.isArray(rhsCSRData.section) &&
                        rhsCSRData.section[2] &&
                        rhsCSRData.section[2].items
                          ? [].concat(rhsCSRData.section[2].items)
                          : []
                      }
                    />
                  </div>
                </ErrorBoundary>
              ) : null}
              {rhsCSRData && rhsCSRData.section && rhsCSRData.section[4] && rhsCSRData.section[4].items ? (
                <ErrorBoundary>
                  <ArticlesListBlock data={rhsCSRData.section[4]} />
                </ErrorBoundary>
              ) : (
                ""
              )}
            </div>
          )}

          {/*  Trending topics */}
          {rhsCSRData && rhsCSRData.section && rhsCSRData.section[3] && rhsCSRData.section[3].items ? (
            <ErrorBoundary>
              <ArticlesListBlock data={rhsCSRData.section[3]} />
            </ErrorBoundary>
          ) : (
            ""
          )}

          {/* video iframe added for 5 days */}
          {/* <div className="vd_pl" data-exclude="amp">
            <iframe
              src={`${siteConfig.weburl}/pwafeeds/videoplayer_iframe.cms?wapCode=${siteName}`}
              height="250"
              width="300"
              border="0"
              marginWidth="0"
              marginHeight="0"
              hspace="0"
              vspace="0"
              frameBorder="0"
              scrolling="no"
              align="center"
              title=""
              allowTransparency="true"
              id="vodplayer"
            />
          </div> */}

          {/*  Section widget */}
          {/* {rhsCSRData &&
        rhsCSRData.section &&
        Array.isArray(rhsCSRData.section) &&
        rhsCSRData.section[2] &&
        rhsCSRData.section[2].items ? (
          <ErrorBoundary>
            <div className="box-item rest-releated-news">
              <div className="section">
                <div className="top_section">
                  <h3>
                    {rhsCSRData && rhsCSRData.section[2] ? (
                      <span>
                        <AnchorLink href={rhsCSRData.section[2].wu}>{rhsCSRData.section[2].secname}</AnchorLink>
                      </span>
                    ) : (
                      <span>{rhsCSRData && rhsCSRData.section[2] && rhsCSRData.section[2].secname}</span>
                    )}
                  </h3>
                </div>
              </div>
              <GridSectionMaker
                type={defaultDesignConfigs.topicslisting}
                data={
                  rhsCSRData &&
                  rhsCSRData.section &&
                  Array.isArray(rhsCSRData.section) &&
                  rhsCSRData.section[2] &&
                  rhsCSRData.section[2].items
                    ? [].concat(rhsCSRData.section[2].items)
                    : []
                }
              />
            </div>
          </ErrorBoundary>
        ) : null} */}

          {/*  Sub Section widget */}
          {/* {rhsCSRData &&
        rhsCSRData.section &&
        Array.isArray(rhsCSRData.section) &&
        rhsCSRData.section[3] &&
        rhsCSRData.section[3].items ? (
          <ErrorBoundary>
            <div className="box-item rest-releated-news">
              <div className="section">
                <div className="top_section">
                  <h3>
                    {rhsCSRData && rhsCSRData.section[3] ? (
                      <span>
                        <AnchorLink href={rhsCSRData.section[3].wu}>{rhsCSRData.section[3].secname}</AnchorLink>
                      </span>
                    ) : (
                      <span>{rhsCSRData && rhsCSRData.section[3] && rhsCSRData.section[3].secname}</span>
                    )}
                  </h3>
                </div>
              </div>

              <GridSectionMaker
                type={defaultDesignConfigs.topicslisting}
                data={
                  rhsCSRData &&
                  rhsCSRData.section &&
                  Array.isArray(rhsCSRData.section) &&
                  rhsCSRData.section[3] &&
                  rhsCSRData.section[3].items
                    ? [].concat(rhsCSRData.section[3].items)
                    : []
                }
              />
            </div>
          </ErrorBoundary>
        ) : null} */}

          {/*  Top Story Widget  commented for web and mobile as per TML-1886*/}
          {/* <TopStoryWidget numItems={5} /> */}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    rhsData: state.app.rhsData,
  };
}

const VideoSection = ({ data }) => {
  return data && data.items ? (
    <React.Fragment>
      <div className="box-item rhs-videolist">
        <div className="section">
          <div className="top_section">
            <h3>
              <span>
                <AnchorLink target="_blank" href={data.wu}>
                  {data.secname}
                </AnchorLink>
              </span>
              {/*
            //FIXME: Add youtube button
            <YoutubeBottn /> 
            */}
            </h3>
          </div>
        </div>
        <ul className="list-horizontal video">
          {data &&
            data.items &&
            [].concat(data.items).map((vidData, index) => {
              return vidData && typeof vidData === "object" && vidData.constructor === Object ? (
                vidData.tn != "ad" ? (
                  <li key={vidData.id} className="news-card video">
                    <VideoItem
                      key={vidData.id}
                      item={vidData}
                      index={index}
                      className="vd-slider-listview"
                      videoIntensive={!vidData.eid}
                      imgsize="smallwidethumb"
                      istrending
                    />
                  </li>
                ) : null
              ) : //  (
              //   <li className="news-card video">
              //     <AdCard mstype={vidData.mstype} adtype={vidData.type} />
              //   </li>
              // )
              null;
            })}
        </ul>
      </div>
      {/*Commented on product requirement
      <div className="box-item">
        <AdCard mstype="ctnrhsvideo" adtype="ctn" />
      </div>*/}
    </React.Fragment>
  ) : null;
};

export default connect(mapStateToProps)(Rhswidget);
