import React, { Component } from "react";
import PropTypes from "prop-types";

import ImageCard from "../common/ImageCard/ImageCard";
import fetch from "../../utils/fetch/fetch";
import { _getStaticConfig } from "../../utils/util";

const siteConfig = _getStaticConfig();

class CommentsSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentsData: null,
      msid: props.msid,
    };
  }

  componentDidMount() {
    const { msid } = this.props;

    // FIXME: Consider moving this to common util for desktop

    const objToQueryStr = obj => {
      if (obj && typeof obj === "object" && obj.constructor === Object) {
        return Object.keys(obj)
          .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
          .join("&");
      }
      return "";
    };

    const commentsParams = Object.assign({}, siteConfig.Comments.allComments);

    fetch(`${process.env.API_BASEPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}&msid=${msid}`)
      // .then(data => data.json())
      .then(commentsData => {
        this.setState({ commentsData });
      })
      .catch(() => null);
  }

  render() {
    const { commentsData } = this.state;
    const { showComments, type } = this.props;
    if (!commentsData) {
      return null;
    }

    const removeSpcChar = str => {
      const removed = str.replace(/<\/?[^>]+(>|$)/g, "");
      return removed;
    };

    // FIXME: Change <a> tag to common link component

    return (
      <div className="comments-wrapper" role="button" tabIndex={0}>
        {commentsData && Array.isArray(commentsData) && commentsData.length > 0 ? (
          <div className="bottom-comments">
            {commentsData.slice(1).length > 0 &&
              commentsData.slice(1).map(c => (
                <div
                  className={`comment-box${c && c.C_A_ID ? " authorComment" : ""}`}
                  onClick={showComments}
                  key={c.C_T}
                >
                  <div className="user-thumbnail">
                    <ImageCard
                      type="absoluteImgSrc"
                      src={
                        (c.user_detail && c.user_detail.thumb) ||
                        `${process.env.WEBSITE_URL}${siteConfig.Thumb.userImage}`
                      }
                      className="bg_poster"
                    />
                  </div>

                  <a to={c.A_ID ? `${siteConfig.sso.PROFILE_URL}/${c.A_ID}` : ""} className="name" target="_blank">
                    {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                  </a>

                  <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                  <div className="footbar clearfix">
                    <span
                      commentsData-action="comment-reply"
                      className="cpointer"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      Reply
                    </span>
                    <span
                      className="up cpointer"
                      commentsData-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      commentsData-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span>
                  </div>
                </div>
              ))}
          </div>
        ) : null}
        <div className="bottom-comments-btns">
          <div className="cmtbtn-wrapper">
            {commentsData && Array.isArray(commentsData) && commentsData.length > 0 ? (
              <span className="cmtbtn view" onClick={showComments} role="button" tabIndex={0}>
                {`${type === "review" ? siteConfig.locale.tech.read_fullreview : siteConfig.locale.view_comment} (${
                  commentsData[0].cmt_c
                })`}
                <b className="icon" />
              </span>
            ) : null}
            <span
              className="cmtbtn add"
              onClick={showComments}
              // style={{ width: commentsData.length === 0 ? "100%" : "50%" }}
              role="button"
              tabIndex={0}
            >
              <span>{type === "review" ? siteConfig.locale.review_movie : siteConfig.locale.write_comment}</span>
              <b className="icon" />
            </span>
          </div>
        </div>
      </div>
    );
  }
}

CommentsSection.defaultProps = {
  type: "comment",
};

CommentsSection.propTypes = {
  showComments: PropTypes.func,
  type: PropTypes.string,
};

export default CommentsSection;
