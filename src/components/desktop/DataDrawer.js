import React from "react";
import { connect } from "react-redux";
import { ScoreCard } from "../../modules/scorecard/ScoreCard";
import { getCountryCode, isInternationalUrl, isMobilePlatform } from "../../utils/util";
import WeatherCard from "../common/WeatherCard";

class DataDrawer extends React.PureComponent {
  render() {
    const { scorecard, dispatch, weather, hideWeather, router } = this.props;
    const match =
      scorecard &&
        scorecard.mini_scorecard &&
        scorecard.mini_scorecard.Calendar &&
        scorecard.mini_scorecard.Calendar.length > 0
        ? scorecard.mini_scorecard.Calendar
        : null;

    const onClickMove = event => {
      const keyName = event.target.getAttribute("keyName") || 0;
      document
        .querySelector(".data-drawer")
        .querySelector("div")
        .setAttribute(
          "style",
          `overflow:initial;transition: transform 1s;transform:translateX(${-325 * parseInt(keyName)}px)`,
        );
      const childrens = event.target.parentElement.children;
      for (let i = 0; i < childrens.length; i++) {
        childrens[i].classList.remove("active");
      }
      event.target.classList.add("active");
    };
    if (!match && (!weather || hideWeather || isMobilePlatform())) {
      return null;
    }
    return (
      <div
        className="data-drawer weather-data-drawer"
        style={{ width: "315px", overflow: "hidden", position: "relative", margin: "5px 0" }}
      >
        <div className="scorecard-listview scroll_content">
          {match ? <ScoreCard dispatch={dispatch} scorecard={scorecard} /> : null}
          {(!isMobilePlatform() || !match) && (!isInternationalUrl(router)) && weather && !hideWeather ? (
            <div className="miniscorecard">
              <WeatherCard dispatch={dispatch} weather={weather} />
            </div>
          ) : null}
        </div>
        {match && !hideWeather ? (
          <div className="drawer-icon">
            {match.map((item, index) => (
              <span key={index} keyName={index} onClick={onClickMove} className={index == 0 ? "active" : ""} />
            ))}
            {weather && !hideWeather && <span keyName={match.length} onClick={onClickMove} />}
          </div>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    scorecard: state.scorecard,
    weather: state.app.weather,
  };
}

export default connect(mapStateToProps)(DataDrawer);
