import React, { Component } from "react";

class DesktopNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchBoxVisible: false,
      sectionExpanded: null,
      sectionName: ""
    };
  }

  hideCurrentSection = () => {
    //TODO: Add hiding and showing code
  };

  showCurrentSection = () => {
    //TODO: Add hiding and showing code
  };

  render() {
    const { data } = this.props;
    const { sectionName, searchBoxVisible, sectionExpanded } = this.state;

    if (!data) {
      return null;
    }

    //TODO: Replace link with global component( for Links)

    return (
      <div id="fixedMenu">
        <div onMouseLeave={this.hideCurrentSection}>
          {data && data.level1 && data.level1.length ? (
            <div
              className="first-level-menu"
              itemType="http://www.schema.org/SiteNavigationElement"
              itemScope="1"
            >
              <h3 className="home active" itemProp="name">
                <a
                  className="nbt-home-sprite"
                  to={process.env.WEBSITE_URL}
                  title="Home"
                  itemProp="url"
                  target="notblank"
                >
                  {/* {Config.seoConfig.englishName} */}
                  HOME
                </a>
              </h3>
              <ul>
                {data.level1.map((item, index) => {
                  return (
                    <li
                      key={item.hl}
                      itemProp="name"
                      className={`nav-${item.secname} ${
                        item.secname === this.state.SetActiveClass
                          ? "active"
                          : ""
                      }`}
                    >
                      <a
                        className="nav-item"
                        itemProp="url"
                        to={item.override}
                        onMouseOver={e =>
                          this.showCurrentSection(
                            item.secname,
                            item.hoverid,
                            e,
                            index
                          )
                        }
                        target="_blank"
                      >
                        {item.hl}
                      </a>
                    </li>
                  );
                })}
                {/* {this.state.sectionExpanded ? (
                  <HoverSection
                    sectionName={this.state.sectionName}
                    sectionData={this.state.sectionData}
                    hideCurrentSection={this.hideCurrentSection}
                    hoverData={this.props.hoverData || []}
                    photogalData={
                      pageHeaderData &&
                      pageHeaderData.nav &&
                      pageHeaderData.nav.photogallery
                        ? pageHeaderData.nav.photogallery
                        : null
                    }
                  />
                ) : null} */}

                <li className="nav_right">
                  <ul>
                    <li
                      className={
                        searchBoxVisible ? "serchicon active" : "serchicon"
                      }
                      onClick={this.toggleSearchBox}
                    >
                      <b />
                      {this.state.serchBoxVisible && (
                        <div className="searchbox">
                          <iframe
                            taget="_parent"
                            scrolling="no"
                            // src={Config.Links.serchiFrame}
                          />
                        </div>
                      )}
                    </li>

                    {data &&
                      data.miscellaneous &&
                      Array.isArray(data.miscellaneous) &&
                      data.miscellaneous.map((item, index) => {
                        return (
                          <li
                            key={item.hl}
                            className={`nav-${item.secname} ${
                              item.secname === sectionName ? "active" : ""
                            }`}
                            onMouseOver={e =>
                              this.showCurrentSection(item.secname, e, index)
                            }
                          >
                            {/* <a to={item.override}>{item.hl}</a> */}
                            <AnchorLink href={item.override}>
                              {item.hl}
                            </AnchorLink>
                          </li>
                        );
                      })}
                  </ul>
                </li>
              </ul>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default DesktopNavigation;
