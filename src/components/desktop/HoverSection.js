import React from "react";
import PropTypes from "prop-types";

//FIXME: Change a tag to common link component
const HoverSection = ({ sectionName, hideCurrentSection }) => {
  switch (sectionName) {
    case "photoGallery":
      return (
        <div className="hover_content" onMouseLeave={hideCurrentSection}>
            <iframe
              title="photogallery frame"
              width="790"
              height="270"
              hspace="0"
              vspace="0"
              scrolling="no"
              align="center"
              src="https://navbharattimes.indiatimes.com/topgalleies_nav_top2.cms"
            />
            {/* FIXME: Need links later than can be uncommented.
            <div className="liveblocklink">
              <ul>
                {photogalData && Array.isArray(photogalData)
                  ? photogalData.map(value => {
                      return (
                        <li key={value.id}>
                          <a to={value.wu}>{value.hl}</a>
                        </li>
                      );
                    })
                  : null}
              </ul>
            </div> */}
        </div>
      );
    default:
      return null;
  }
};

HoverSection.propTypes = {
  sectionName: PropTypes.string
};

export default HoverSection;
