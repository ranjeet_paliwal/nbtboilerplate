import React from "react";
import PropTypes from "prop-types";
import AnchorLink from "../common/AnchorLink";

//FIXME: Change <a> tag to common link component

const ArticlesListBlock = props => {
  const { data } = props;
  return data && data.items && Array.isArray(data.items) && data.items.length > 0 ? (
    <div className={`box-item article-most-viewed ${data.id === "trending" ? "trending-topics" : ""}`}>
      <div className="section">
        <div className="top_section">
          <h3>
            {data && data.wu ? (
              <span>
                {/* <a to={data.wu}>{data.secname}</a> */}
                <AnchorLink href={data.wu}>{data.secname}</AnchorLink>
              </span>
            ) : (
              <span>{data.secname}</span>
            )}
          </h3>
        </div>
      </div>
      {/* <ul className={`${data.id === "trending" ? "trending-topics" : "list-horizontal"}`}>
        {[].concat(data.items).map(item => (
          <li key={item.id}>
            <AnchorLink href={item.wu}>{item.hl}</AnchorLink>
          </li>
        ))}
      </ul> */}
      <ul className="trending-topics">
        {[].concat(data.items).map(item => (
          <li key={item.id}>
            <AnchorLink href={item.wu}>{item.hl}</AnchorLink>
          </li>
        ))}
      </ul>
    </div>
  ) : null;
};

ArticlesListBlock.propTypes = {
  data: PropTypes.object,
};

export default ArticlesListBlock;
