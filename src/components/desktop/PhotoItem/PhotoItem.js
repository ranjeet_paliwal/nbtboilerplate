import React from "react";
// import VideoListCard from "../../../components/common/VideoListCard";
import PropTypes from "prop-types";
import AnchorLink from "../../common/AnchorLink";
import ImageCard from "../../common/ImageCard/ImageCard";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

const PhotoItem = props => {
  const { item, width } = props;
  const platform = process.env.PLATFORM;
  // console.log("propss", props, platform);
  if (platform === "desktop") {
    return (
      <li key={item.wu || item.override} className="table nbt-listview photolistview ">
        <AnchorLink href={generateUrl(item)}>
          <span className="table_col img_wrap" data-tag={item.tn == "photo" ? item.du : null}>
            <ImageCard width={width} noLazyLoad={false} msid={item.imageid} title={item.seotitle || ""} schema />
          </span>
          <span className="table_col con_wrap">
            <span className="text_ellipsis">{item.hl}</span>
          </span>
        </AnchorLink>
      </li>
    );
  }
  //    else {
  //     // <VideoListCard key={item.id} source="videoshow" item={item} />;
  //   }
  return null;
};

PhotoItem.propTypes = {
  item: PropTypes.object,
  width: PropTypes.string,
};

function generateUrl(item) {
  if (item.override) {
    // Changing domain from absolute to relative
    return item.override.replace(/^.*\/\/[^\/]+/, "");
  }
  const templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
  const url = `/${item.seolocation ? `${item.seolocation}/` : ""}${templateName}/${
    item.tn === "photo" ? item.imageid : item.id
  }.cms`;
  return url;
}

PhotoItem.displayName = "PhotoItem";

export default PhotoItem;
