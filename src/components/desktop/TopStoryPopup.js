/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prop-types */
// import React, { PureComponent } from "react";
import React, { PureComponent } from "react";

import fetch from "../../utils/fetch/fetch";
import ImageCard from "../../components/common/ImageCard/ImageCard";
import AnchorLink from "../common/AnchorLink";
import { _getStaticConfig, _setCookie, _getCookie } from "../../utils/util";
const siteConfig = _getStaticConfig();
import SvgIcon from "../common/SvgIcon";
const colors = ["gray", "yellow", "orange", "blue"];
class TopStoryPopup extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      shouldShow: false,
      colorName: "gray",
    };
  }

  componentDidMount() {
    // this is done only to load the API faster when invoked in fetchData function
    fetch(`${process.env.API_BASEPOINT}/web_topstory_popup.cms?feedtype=sjson`);
    setTimeout(() => {
      document.querySelector("body").addEventListener("mouseleave", this.fetchData);
    }, 5000);
  }

  componentWillUnmount() {
    document.querySelector("body").removeEventListener("mouseleave", this.fetchData);
  }

  componentDidUpdate(preProps) {
    if (preProps.location && this.props.location && preProps.location.pathname !== this.props.location.pathname) {
      this.setState({ shouldShow: false });
    }
  }

  fetchData = e => {
    const { shouldShow } = this.state;
    const _this = this;
    if (e.clientY < 0 && !_getCookie("tpstoryPopup") && !shouldShow) {
      this.setState({ shouldShow: true });
      fetch(`${process.env.API_BASEPOINT}/web_topstory_popup.cms?feedtype=sjson`).then(data => {
        if (data) {
          data.sort(() => Math.random() - 0.5);
        }
        const colorName = colors[Math.floor(Math.random() * colors.length)];

        this.setState({ data, colorName });
        // restrict popup to appear multiple time, once it gets visible
        _setCookie("tpstoryPopup", "closed", 1 / 48); // 30 minute
      });

      if (document.querySelector(".topStorieswrap")) {
        document.querySelector(".topStorieswrap").addEventListener("click", _this.closePopup);
      }
    }
  };

  closePopup = clicked => {
    this.setState({
      shouldShow: false,
    });
    if (clicked == "closebtn") {
      _setCookie("tpstoryPopup", "closed", 1 / 48); // 30 minute
    }
  };

  render() {
    const { data, shouldShow, colorName } = this.state;

    return shouldShow && !_getCookie("tpstoryPopup") ? (
      <React.Fragment>
        <div className="topStorieswrap" />
        <div className={`top-stories-modal ${colorName}`}>
          <h2>{siteConfig.locale.missedNews}</h2>
          <div className="close" onClick={this.closePopup.bind(this, "closebtn")} />
          {data ? (
            <ul>
              {data.map((item, index) => {
                // weblink is updated to keep utm_campaign(comming in feed) value from story1 to story6
                const weblink = `${item.wu}${index + 1}`;
                return (
                  <li key={item.wu} className={item.tn}>
                    {item.tn == "photo" && <SvgIcon name="photo" className="photoicon" />}
                    <AnchorLink href={weblink}>
                      <ImageCard msid={item.imageid} size="smallwidethumb" imgsize={item.imgsize} width="200" />
                      <span>{item.hl}</span>
                    </AnchorLink>
                  </li>
                );
              })}
            </ul>
          ) : (
            <div className="wdt_loading">
              <span className="spinner_circle"></span>
            </div>
          )}
        </div>
      </React.Fragment>
    ) : null;
  }
}

export default TopStoryPopup;
