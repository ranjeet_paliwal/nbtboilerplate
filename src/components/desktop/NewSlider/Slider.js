import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import "../Slider/HP_slider.scss";

class Slider extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      translateValue: 0,
    };
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue, 10) + parseInt(this.props.moveByPixel, 10),
    }));
  };

  goToNextSlide = datalength => {
    // to the first image in the array.
    const { currentIndex } = this.state;

    if (currentIndex === parseInt(datalength, 10) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue - this.props.moveByPixel,
    }));

    return 0;
  };

  goToSlide = index => {
    this.setState(prevState => ({
      currentIndex: index,
      translateValue: prevState.translateValue + (prevState.currentIndex - index) * this.props.moveByPixel,
    }));
  };

  render() {
    const { translateValue, currentIndex } = this.state;
    const { width, heading, sliderClass, headingRequired, children, dataLength } = this.props;
    return (
      <React.Fragment>
        {headingRequired && heading && <h2>{heading}</h2>}
        <div className={`slider ${sliderClass}`} style={{ width: `${width}px` }}>
          <div className="hidden_layer">
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: "transform ease-out 0.45s",
              }}
            >
              {children}
            </div>
          </div>
          <LeftArrow goToPrevSlide={() => this.goToPrevSlide()} currentIndex={currentIndex} />
          <RightArrow goToNextSlide={() => this.goToNextSlide(dataLength)} length={dataLength} />
        </div>
      </React.Fragment>
    );
  }
}

const LeftArrow = props => {
  const { goToPrevSlide, currentIndex } = props;
  if (currentIndex > 0) {
    return (
      <span
        role="button"
        tabIndex={0}
        data-exclude="amp"
        className="btnPrev"
        onClick={goToPrevSlide}
        onKeyUp={goToPrevSlide}
      />
    );
  }
  return null;
};

const RightArrow = props => {
  const { goToNextSlide, length } = props;
  if (length && length > 1) {
    return (
      <span
        role="button"
        tabIndex={0}
        data-exclude="amp"
        pg="nxtphoto"
        className="btnNext"
        onClick={goToNextSlide}
        onKeyUp={goToNextSlide}
      />
    );
  }
  return null;
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

Slider.propTypes = {
  width: PropTypes.string, // width of the widget needed
  heading: PropTypes.any, // heading of the slider
  sliderClass: PropTypes.string, // if want to add any specific class on the slider
  headingRequired: PropTypes.bool, // if heading is required - bool
  children: PropTypes.any, // content inside the slider tag
  dataLength: PropTypes.number, // number of elements in the slider
  moveByPixel: PropTypes.string, // By how many pixel is the intended behaviour of the slider to move
};

export default Slider;
