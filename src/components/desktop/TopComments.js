import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import fetch from "../../utils/fetch/fetch";

import { _getStaticConfig } from "../../utils/util";
const siteConfig = _getStaticConfig();

class TopComment extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      commentShown: false,
    };
  }

  componentDidMount() {
    const { msid } = this.props;
    const topCommentsParams = Object.assign({}, siteConfig.Comments.topComments, {
      msid,
    });

    // FIXME: Consider moving this to common util for desktop
    // This might increase bundle size as -> desktop-util -> TopComments ->  Story (which is common)
    const objToQueryStr = obj => {
      if (obj && typeof obj === "object" && obj.constructor === Object) {
        return Object.keys(obj)
          .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
          .join("&");
      }
      return "";
    };

    const topComments = fetch(`${process.env.API_BASEPOINT}/commentsdata.cms?${objToQueryStr(topCommentsParams)}`)
      // .then(data => data.json())
      .then(data => this.setState({ data }));
  }

  toggleHideComment = () => {
    const { commentShown } = this.state;
    this.setState({
      commentShown: !commentShown,
    });
  };

  render() {
    const { commentShown, data } = this.state;
    const { showComments, pagetype } = this.props;
    if (!data) {
      return null;
    }
    if (Array.isArray(data) && data.length > 0) {
      let name;
      let commentText;
      let heading;
      if (data[0] && data[0].authorsComment && data[0].authorsComment.length > 0) {
        heading = siteConfig.locale.text_author_comment;
        if (data[0].authorsComment) {
          const authCmt = data[0];

          if (authCmt.authorsComment[0].C_A_ID) {
            commentText = authCmt.authorsComment[0].C_T;
            name = authCmt.authorsComment[0].A_D_N;
          } else if (typeof authCmt.authorsComment[0].CHILD === "object") {
            for (let g = 0; g < 2; g++) {
              if (authCmt.authorsComment[0].CHILD[g].C_A_ID) {
                commentText = authCmt.authorsComment[0].CHILD[g].C_T;
                name = authCmt.authorsComment[0].CHILD[g].A_D_N;
                break;
              }
            }
          }
        }
      } else if (typeof data[1] === "object") {
        const cmtData = data[1];
        name = data[1].A_D_N;
        // LENGTH OF NULL ISSUE POSSIBLE (LNULL)
        commentText = data[1].C_T;
        if (cmtData.AC_A_C > 0) {
          heading = siteConfig.locale.text_topcomment;
        } else {
          heading = siteConfig.locale.text_latestcomment;
        }
      }

      // Moving it inside TopComment for movieshow page , as comments count can't be checked outside (LAN-7652)
      if (pagetype == "movieshow") {
        heading = siteConfig.locale.user_review;
      }

      const cmtLen = 180;
      return (
        <div className="wdt_top_comments">
          <div className="cmt_head">
            <span>{heading}</span>
          </div>
          <div className={`cmt_txt ${commentText.length > cmtLen ? "topcomment_container" : ""}`}>
            {commentText.length > 180 ? (
              <React.Fragment>
                {commentShown ? commentText : `${commentText.substr(0, cmtLen)}...`}
                <small onClick={this.toggleHideComment}>{commentShown ? "-" : "+"}</small>
              </React.Fragment>
            ) : (
              commentText
            )}
          </div>
          <div className="author_name">{name}</div>
          {/* <div className="btn-allcomment">
            <a href="javascript:void(0)" onClick={() => showComments()}>
              {siteConfig.locale.comment_text}
            </a>
          </div>
          <div className="btn-wrcomment">
            <a href="javascript:void(0)" onClick={() => showComments()}>
              {siteConfig.locale.write_comment}
            </a>
          </div> */}
        </div>
      );
    }
    // return <div class="nocomments">Be the first one to review.</div>;
    return null;
  }
}

TopComment.propTypes = {
  data: PropTypes.object,
  showComments: PropTypes.func,
};

export default TopComment;
