import React, { PureComponent } from "react";
import "./GaanaWidget.scss";
import ImageCard from "../../common/ImageCard/ImageCard";

class Gaana extends PureComponent {
  state = {
    playStatus: false,
    currentClass: null,
  };

  setFrameProps = () => {
    this.setState({
      playStatus: true,
    });
  };

  clickEventHandler = event => {
    const currentClass = !this.state.currentClass ? "active" : null;
    this.setState({
      currentClass,
    });
  };

  render() {
    const { playStatus } = this.state;
    return (
      <div className={`gaanaplayer_box ${this.state.currentClass}`} onClick={this.clickEventHandler}>
        <div className="box_head">
          <span className="gaanaplayer_title" onClick={this.setFrameProps}>
            <img src="https://static.langimg.com/photo/58243404.cms" alt="gaana icon" /> of the Day
          </span>
        </div>
        {playStatus ? (
          <iframe
            frameBorder="0"
            width="100%"
            height="310px"
            id="gaanatvplayer"
            className="jLivePlayer"
            isloadedfirsttime="true"
            src="https://gaana.com/gaanawidget-v7"
            title="Play Gaana"
          />
        ) : null}
      </div>
    );
  }
}

export default Gaana;
