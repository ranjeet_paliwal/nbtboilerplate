import React from "react";
import { compose } from "redux";
import axios from "axios";
import { renderToString, renderToStaticMarkup } from "react-dom/server";
import { createMemoryHistory, match, RouterContext } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";
import { Provider } from "react-redux";
import Helmet from "react-helmet";
import { flushWebpackRequireWeakIds } from "react-loadable";
import configureStore from "store/configureStore";
import Html from "containers/html/Html";
import fs from "fs";
import routes, { NotFoundComponent } from "./router/master/routes";
import {
  checkIsAmpPage,
  getPageType,
  getPageTypeUpdated,
  isOperaMiniAtServer,
  checkIsFbPage,
  extractMsidFromUrl,
  _filterRecursiveALJSON,
  filterAlaskaData,
  parseQueryString,
  checkTopAtf,
  hasValue,
} from "./utils/util";
import { _getStaticConfig, getCssVersion, logError } from "./utils/util";
import globalconfig from "./globalconfig";
import { getSplatsVars } from "./components/lib/ads/lib/utils";
import Amp from "./amp/Amp";
import { ampConverter, replaceHTMLContent } from "./amp/lib/ampConverter";
import fetch from "./utils/fetch/fetch";
import FbIA from "./FbIA/FbIA";
import { fbIAConverter, getRHS } from "./FbIA/fbIAConverter";
const ASSET_PATH = process.env.ASSET_PATH || "/";
// import { func } from "prop-types";
// import HoverSection from "./components/desktop/HoverSection";
const siteConfig = _getStaticConfig();
let counterForAtfV1 = 0;
let counterForAtfV2 = 0;
const separator = "\n";

const errorHTML = `
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "https://www.w3.org/TR/html4/strict.dtd">
<html >
   <head >
      <meta content="NOINDEX, NOFOLLOW" name="ROBOTS">
      <meta name="viewport" content="width=device-width, height=device-height,initial-scale=1.0,user-scalable=no">
      <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
      <meta http-equiv="content-language" content="en">
      <title>Page not found  | Navbharat Times</title>
      <meta name="description" content="Page not found">
      <link rel='stylesheet' type='text/css' href='/css/main.css'/>
   </head>
   <body>        
        <div id="c_wdt_articleshow_1" class="error-article-section"><div class="error-wrapper"><span class="heading-text">Sorry, page not found</span><div class="sub-heading">It may have expired, or there could be a typo. Maybe you can find what you need on our homepage.</div><a class="return-btn" href="/">RETURN</a></div></div>
   </body>
</html>
`;

// cache life time for respective pages in min
const cachelifeObj = {
  home: 10,
  liveblog: 3,
  moviereview: process.env.SITE === "iag" ? 180 : 600, // setting this time to 3 hrs in case of iag according to @Maninder
  articleshow: process.env.SITE === "iag" ? 180 : 600,
  photoshow: process.env.SITE === "iag" ? 180 : 600,
  videoshow: process.env.SITE === "iag" ? 180 : 600,
  photolist: 15,
  videolist: 15,
  articlelist: 15,
  topics: 30,
  default: 30,
};

// TODO- need add cache time for list pages
// list pages=15 min  show pageconst10hrs

/** This method move into Util.js with same name */
function logError_(ex) {
  const today = new Date();
  const todayString = `${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`;

  const timeString = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
  let errorMsg = `ERROR START AT : ${todayString} ${timeString}${separator}`;
  errorMsg += ex.stack + separator + separator;

  fs.appendFileSync(`/var/log/node/lang-error-${todayString}.log`, errorMsg);
}

function parseCookie(cookie) {
  if (typeof cookie !== "undefined" && cookie != "") {
    const cookieArr = cookie.split(";");
    const cookieFinalArr = {};
    for (const c of cookieArr) {
      const temp = c.split("=");
      if (typeof temp[0] !== "undefined" && typeof temp[1] !== "undefined") {
        cookieFinalArr[temp[0].trim()] = temp[1].trim();
      }
    }
    return cookieFinalArr;
  }
  return [];
}

function flatten(arr) {
  return [].concat.apply([], arr);
}

function uniq(arr) {
  return [...new Set(arr)];
}

function isTruthy(val) {
  return !!val;
}

const flattenUniq = compose(uniq, flatten);

function fetchComponentData(renderProps, store, isOperaMiniBrowser) {
  const requests = renderProps.components.filter(isTruthy).map(component => {
    // Handle `connect`ed components.
    if (component.WrappedComponent) {
      component = component.WrappedComponent;
    }
    if (component.fetchData) {
      let { query, params, history, router, routes } = renderProps;
      if (!query) {
        query = Object.assign({}, renderProps.location.query);
      }
      const pagetype = "";
      // TODO ShortURL Need to remove this
      // const longurl = routes && routes[2] && routes[2].longurl;
      // if (longurl) {
      //   pagetype = getPageType(longurl);
      // }
      // if (routes && routes[2] && routes[2].longurl) {
      //   params.msid = extractMsidFromUrl(routes[2].longurl);
      // }
      return (
        component
          .fetchData({
            dispatch: store.dispatch,
            query,
            params,
            history,
            router,
            state: store.getState(),
          })
          // Make sure promise always successfully resolves
          .catch(() => {})
      );
    }
  });

  return Promise.all(requests);
}

function isNotFound(renderProps) {
  return !renderProps || renderProps.components.some(component => component === NotFoundComponent);
}

/*
 * TODO
 * It is a hack where we check ,if inside parentContainer div data present or not ,
 * Sometimes when feeds return error or blank , containers return FAKELOADERS
 * SO HERE we returning blank containers , which will help for 404 error
 */
function isReactNotFound(html, store, pagetype) {
  try {
    const initialState = store.getState();
    if (pagetype == "photoshow") {
      pagetype = "photomazzashow";
    }
    // console.log("===pagetype", initialState[pagetype]);
    if (initialState && pagetype && initialState[pagetype]) {
      const response = initialState[pagetype];
      //  console.log("==========" + response["error"]);
      if (response.error || (response.error && response.errorMessage == "404")) {
        return true;
      }
      if (html.indexOf('<div id="childrenContainer" class="con_content"></div>') != -1) {
        return true;
      }
      return false;
    }
    // return html.indexOf('<div id="childrenContainer" class="con_content"></div>') != -1 ? true : false;
  } catch (err) {
    // console.log("XXXXX==========XXX", err.message);
    return false;
  }
}

function getJsByChunkName(name, { assetsByChunkName }) {
  let assets = assetsByChunkName[name];
  if (!Array.isArray(assets)) {
    assets = [assets];
  }
  return assets.find(asset => /\.js$/.test(asset));
}

function getJsByModuleIds(moduleIds, { modulesById, chunksById }) {
  const chunkIds = flatten(
    moduleIds.map(id => {
      const clientModule = modulesById[id];
      if (!clientModule) {
        throw new Error(`${id} not found in client stats`);
      }
      return clientModule.chunks;
    }),
  );
  return flattenUniq(
    chunkIds.map(id =>
      chunksById[id].files.filter(file => /\.js$/.test(file)).filter(file => !/\.hot-update\.js$/.test(file)),
    ),
  );
}

function getCssByChunkName(name, { assetsByChunkName }) {
  let assets = assetsByChunkName[name];
  if (!Array.isArray(assets)) {
    assets = [assets];
  }
  return assets.find(asset => /\.css$/.test(asset));
}

function getJs(moduleIds, stats) {
  return [
    getJsByChunkName("bootstrap", stats),
    ...getJsByModuleIds(moduleIds, stats),
    getJsByChunkName("client", stats),
    getJsByChunkName("vendor", stats),
  ].filter(isTruthy);
}

function getCss(stats) {
  return [getCssByChunkName("client", stats)].filter(isTruthy);
}

/**
 * middleware to handle redirection if wrong msid hit
 * @param {object}   state react store state , used to get pwa_meta data for pagetype
 * @param {object}   req  express request object , for getting location
 * @param {object}   res  express response object , for redirection with 301
 */
function checkRedirectLocation(state, req, res) {
  let location = req.url;

  let pwa_meta;
  let pagetype = getPageTypeUpdated(location);
  // if (globalconfig.position) {
  //   delete globalconfig.position;
  // }

  // Redirection is handled through express server, we need 301 redirection instead of 302.
  // RedirectURL property is used for this.
  // const techpage =
  //   (!isTechSite() && location && location.indexOf("/tech") > -1) ||
  //   (location && location.indexOf("/tech/amp_articlelist") > -1);

  // Above check avoid redirection of tech amp home page

  const isPDPPage =
    state.gadgetshow &&
    state.gadgetshow.data &&
    state.gadgetshow.data.gadgetData &&
    state.gadgetshow.data.gadgetData.pwa_meta;

  const isCDPPage =
    state.compareDetail &&
    state.compareDetail.data &&
    state.compareDetail.data.compareData &&
    state.compareDetail.data.compareData.pwa_meta;

  if (
    location &&
    (location.indexOf("/tech/articlelist/19615041.cms") > -1 ||
    location.indexOf("/tech/amp_articlelist/19615041.cms") > -1 || // for nbt
    location.indexOf("/tech/articlelist/60023487.cms") > -1 ||
    location.indexOf("/tech/amp_articlelist/60023487.cms") > -1 || // for vk
      location.indexOf("picid-") > -1 ||
      location.indexOf("/sports/cricket/iplt20/amp_articlelist") > -1)
  ) {
    return false;
  }

  if (
    pagetype === "articleshow" ||
    pagetype === "amp_articleshow" ||
    pagetype === "moviereview" ||
    pagetype === "amp_moviereview"
  ) {
    pagetype = "articleshow";
    const temp_items = state[pagetype].items;
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
  } else if (pagetype === "photomazaashow" || pagetype === "photoshow" || pagetype === "amp_photoshow") {
    pagetype = "photomazzashow"; // tempfix for photoshow
    const temp_items = state[pagetype].value[0];
    pagetype = "photoshow"; // tempfix for photoshow
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
  } else if (pagetype === "videoshow") {
    const temp_items = state[pagetype].value;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "liveblog") {
    const temp_items = state[pagetype].value;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "newsbrief") {
    const temp_items = state[pagetype].value[0];
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (
    pagetype === "articlelist" ||
    pagetype === "amp_articlelist" ||
    pagetype === "videolist" ||
    pagetype === "photolist"
  ) {
    pagetype = "articlelist";
    const temp_items = pagetype === "videolist" ? state[pagetype].value : state[pagetype].value[0];
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "topics") {
    const temp_items = state[pagetype].value;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "amp_gadgetshow" || isPDPPage) {
    pagetype = "gadgetshow";
    const temp_items = state[pagetype].data.gadgetData;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (isCDPPage || pagetype === "amp_compareshow") {
    const temp_items = state.compareDetail.data.compareData;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "amp_gadgetslist") {
    const temp_items = state.gadgetlist.gadgetsData;
    pwa_meta = temp_items && temp_items.pwa_meta ? temp_items.pwa_meta : undefined;
  } else if (pagetype === "movieshow" || pagetype === "amp_movieshow") {
    pagetype = "movieshow";
    const temp_items = state[pagetype].items;
    pwa_meta =
      temp_items && temp_items.length > 0 && temp_items[0] && temp_items[0].pwa_meta
        ? temp_items[0].pwa_meta
        : undefined;
  }

  setResponseHeader(pagetype, res);

  // redirect handling  domain/topics/ampdefault/query/ampdefault --- to ---- domain/topics/query/ampdefault
  if (pagetype === "topics" && location.includes("/topics/ampdefault/") && pwa_meta.redirectUrl == undefined) {
    const topicredirection = location.replace("topics/ampdefault", "topics");
    res.redirect(301, topicredirection);
  }

  // if(pwa_meta && pwa_meta.pagetype !== pagetype){
  if (pwa_meta && pwa_meta.redirectUrl && pwa_meta.redirectUrl != "") {
    let redirectlocation =
      pwa_meta.redirectUrl.indexOf(".com") > -1 ? pwa_meta.redirectUrl.split(".com")[1] : pwa_meta.redirectUrl;

    // check for search/query in URL and if present append to Redirection url
    redirectlocation += req._parsedUrl && req._parsedUrl.search ? req._parsedUrl.search : "";

    // LAN-5938
    // Check for old type of url in case of list pages, and correct it, eg  https://telugu.samayam.com/photo-gallery/cinema/photolist/msid-47783654,curpg-5.cms
    if (
      pagetype === "articlelist" ||
      pagetype === "amp_articlelist" ||
      pagetype === "videolist" ||
      pagetype === "photolist"
    ) {
      const regexMsidWithCurpg = /\/\w+\/msid-(\d+),curpg-(\d+).cms/;
      const result = regexMsidWithCurpg.exec(location);
      // IF matched, with capture groups present
      if (result && result[1] && result[2]) {
        const matchedMsid = result[1];
        const matchedCurPg = result[2];
        // Additional check to see if data from feed is same as id obtained in request
        if (redirectlocation && redirectlocation.includes(`${matchedMsid}.cms`)) {
          // Form correct url with curpg as querystring
          redirectlocation = `${redirectlocation}?curpg=${matchedCurPg}`;
        }
      }
    }
    // special handling for amp pages , add 'amp_' in redirectlocation
    if (checkIsAmpPage(location)) {
      // handling all individual cases for photolist - photogallery, photolist, photoarticlelist
      if (pwa_meta.pagetype === "photolist") {
        if (redirectlocation.includes("/photolist/")) {
          redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/amp_${pwa_meta.pagetype}/`);
        } else if (redirectlocation.includes("/photoarticlelist/")) {
          redirectlocation = redirectlocation.replace(`/photoarticlelist/`, `/amp_photolist/`);
        } else if (redirectlocation.includes("/photogallery/")) {
          redirectlocation = redirectlocation.replace(`/photogallery/`, `/amp_photolist/`);
        } else if (redirectlocation.includes("/photomazza/")) {
          redirectlocation = redirectlocation.replace(`/photomazza/`, `/amp_photolist/`);
        } else {
          // case for photolist
          redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/amp_${pwa_meta.pagetype}/`);
        }
      } else if (location && location.endsWith("/amp_gadgetshow.cms")) {
        redirectlocation = `${redirectlocation}/amp_gadgetshow.cms`;
      } else if (location && location.endsWith("/amp_gadgetslist.cms")) {
        redirectlocation = `${redirectlocation}/amp_gadgetslist.cms`;
      } else if (location && location.endsWith("/amp_compareshow.cms")) {
        redirectlocation = `${redirectlocation}/amp_compareshow.cms`;
      } else {
        redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/amp_${pwa_meta.pagetype}/`);
      }
    }
    // special handling for fp ia pages , add 'fb_' in redirectlocation
    if (checkIsFbPage(location)) {
      // handling all individual cases for photolist - photogallery, photolist, photoarticlelist
      if (pwa_meta.pagetype === "photolist") {
        if (redirectlocation.includes("/photolist/")) {
          redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/fb_${pwa_meta.pagetype}/`);
        } else if (redirectlocation.includes("/photoarticlelist/")) {
          redirectlocation = redirectlocation.replace(`/photoarticlelist/`, `/fb_photolist/`);
        } else if (redirectlocation.includes("/photogallery/")) {
          redirectlocation = redirectlocation.replace(`/photogallery/`, `/fb_photolist/`);
        } else if (redirectlocation.includes("/photomazza/")) {
          redirectlocation = redirectlocation.replace(`/photomazza/`, `/fb_photolist/`);
        } else {
          // case for photolist
          redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/fb_${pwa_meta.pagetype}/`);
        }
      } else {
        redirectlocation = redirectlocation.replace(`/${pwa_meta.pagetype}/`, `/fb_${pwa_meta.pagetype}/`);
      }
    }

    // Move to lowercase , check feed for this implementation
    if (pwa_meta.shouldCheckCaseSensitiveRU == "false") {
    } else {
      redirectlocation = redirectlocation.toLowerCase();
      location = location.toLowerCase();
    }
    // Encode only for topics pages
    redirectlocation = pagetype === "topics" ? encodeURI(redirectlocation) : redirectlocation;
    redirectlocation = redirectlocation.toLowerCase();

    // Below condition checks if redirection needed for dynamic page URLs.
    if (pagetype === "topics" && dynamicRedirection(location, redirectlocation)) {
      redirectlocation = redirectlocation.split("?")[0];
      res.redirect(301, redirectlocation);
    }
    // Dynamic page check avoids redirection of url. The below line cause too many redirection
    // e.g. /a/landing/mehndi != /a/landing.cms
    else if (
      redirectlocation != location &&
      !(
        location.indexOf("/landing.cms") > -1 ||
        location.indexOf("/glossary.cms") > -1 ||
        location.indexOf("/cricketer.cms") > -1 ||
        location.indexOf("/politician.cms") > -1
      )
    ) {
      /*  if (pwa_meta.redirectUrl.includes(siteConfig.gadgetdomain)) {
        redirectlocation = siteConfig.gadgetdomain + redirectlocation;
      } */

      res.redirect(301, redirectlocation);
    }
  }
}

/**
 * This method is used to check if redirection needed or not.
 * Method compares seopath of redirect and location url if unequal returns true else false.
 * @param {*} location
 * @param {*} redirectlocation
 */
function dynamicRedirection(location, redirectlocation) {
  let tempname = "";
  let redirectlocSeopath;
  let locationSeopath;
  let bool = false;
  let queryname;

  if (location.indexOf("/landing.cms") > -1) {
    tempname = "landing";
  } else if (location.indexOf("/glossary.cms") > -1) {
    tempname = "glossary";
  } else if (location.indexOf("/cricketer.cms") > -1) {
    tempname = "cricketer";
  } else if (location.indexOf("/politician.cms") > -1) {
    tempname = "politician";
  }

  if (tempname != "") {
    redirectlocSeopath = redirectlocation.split(`/${tempname}`)[0];
    locationSeopath = location.split(`/${tempname}`)[0];

    // For alias redirection
    const arrQueryParams = parseQueryString(location);
    if (arrQueryParams && arrQueryParams.name) {
      queryname = redirectlocation.split("?")[0];
      queryname = queryname.split(`/${tempname}/`)[1];
      if (arrQueryParams.name != queryname) {
        bool = true;
      }
    }

    if (redirectlocSeopath != locationSeopath) {
      bool = true;
    }
  }
  return bool;
}

/**
 * This method is used to check if redirection needed or not.
 * Method compares seopath of redirect and location url if unequal returns true else false.
 * @param {*} location
 * @param {*} redirectlocation
 */
function dynamicRedirection(location, redirectlocation) {
  let tempname = "";
  let redirectlocSeopath;
  let locationSeopath;
  let bool = false;
  let queryname;

  if (location.indexOf("/landing.cms") > -1) {
    tempname = "landing";
  } else if (location.indexOf("/glossary.cms") > -1) {
    tempname = "glossary";
  }
  if (tempname != "") {
    redirectlocSeopath = redirectlocation.split(`/${tempname}`)[0];
    locationSeopath = location.split(`/${tempname}`)[0];

    // For alias redirection
    const arrQueryParams = parseQueryString(location);
    if (arrQueryParams && arrQueryParams.name) {
      queryname = redirectlocation.split("?")[0];
      queryname = queryname.split(`/${tempname}/`)[1];
      if (arrQueryParams.name != queryname) {
        bool = true;
      }
    }

    if (redirectlocSeopath != locationSeopath) {
      bool = true;
    }
  }
  return bool;
}

function setResponseHeader(pagetype, res) {
  let cachelife = cachelifeObj[pagetype];
  cachelife = cachelife ? cachelife * 60000 : cachelifeObj.default * 60000;

  if (process.env.NODE_ENV) {
    res.setHeader("refreshTime", cachelife);
    res.setHeader("cache-control", `private ,must-revalidate , max-age=${cachelife / 1000}`);
    res.setHeader("expires", new Date(Date.now() + cachelife).toUTCString());
  }
}

function render(renderProps, store, stats, res, req, pagetype = "") {
  const markup = renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>,
  );
  const version = stats.version;

  const head = Helmet.rewind();
  const moduleIds = flushWebpackRequireWeakIds();
  const js = getJs(moduleIds, stats);
  const httpPushCache = [];
  const css = getCss(stats);
  const svgsprite_path = `img/pwa_sprite.svg?v=${getCssVersion()}`;
  const initialState = store.getState();
  const location = req.url;
  pagetype = getPageType(location);

  let leadImg = "";
  if (pagetype === "home" || pagetype === "articleshow" || pagetype === "videoshow" || pagetype === "photoshow") {
    leadImg = getLeadImageUrl(initialState, pagetype);
  } else if (pagetype === "others" && location === "/tech") {
    pagetype = "articlelist";
  }
  if (leadImg && leadImg !== "") {
    httpPushCache.push("<" + leadImg + ">; rel=preload; as=image");
  }

  // push svg file
  // httpPushCache.push(`<${ASSET_PATH}${svgsprite_path}>; rel=preload; as=image`);
  httpPushCache.push(`<${ASSET_PATH}${css}>; rel=preload; as=style`);
  httpPushCache.push(`<${siteConfig.mweburl}/service-worker.js?v=${version}>; rel=preload; as=script`);
  httpPushCache.push(`<${siteConfig.icon}>; rel=preload; as=image`);

  // push bootstrap and vendor
  for (const j in js) {
    if (js[j].indexOf("bootstrap") > -1 || js[j].indexOf("vendor") > -1) {
      httpPushCache.push(`<${ASSET_PATH}${js[j]}>; rel=preload; as=script`);
    }
  }
  // httpPushCache.push('<https://static.nbt.indiatimes.com/fonts/NotoSansDevanagari-Regular.woff>; rel=preload; as=font');

  const httpCache = httpPushCache.join(", ");
  if (process.env.NODE_ENV == "production") {
    res.setHeader("Link", httpCache);
    // res.setHeader("refreshTime",30000);
  }

  return renderToStaticMarkup(
    <Html
      js={js}
      css={css}
      html={markup}
      head={head}
      initialState={initialState}
      version={version}
      pagetype={pagetype}
      isOperaMini={stats.isOperaMini}
      reqURL={req.url}
    />,
  );
}

function getLeadImageUrl(initialState, pagetype) {
  let initState;
  let imageid = null;
  let imgsize = "";
  let imgconfig = "largethumb";

  try {
    if (initialState && pagetype) {
      if (pagetype === "photoshow") {
        initState = initialState["photomazzashow"];
      } else {
        initState = initialState[pagetype];
      }
    }
    if (initState && typeof initState === "object") {
      if (pagetype === "home" && initState && hasValue(initState, "value.0.items.0.imageid")) {
        imageid = initState.value[0].items[0].imageid;
        imgsize = `imgsize-${initState.value[0].items[0].imgsize},`;
      } else if (pagetype === "articleshow") {
        if (initState && hasValue(initState, "items.0.lead.image.0.id")) {
          const leadImgObj = initState.items[0].lead.image[0];
          imageid = leadImgObj.id ? leadImgObj.id : "";
          imgsize = leadImgObj.imgsize ? `imgsize-${leadImgObj.imgsize},` : "";
        }
      } else if (pagetype === "videoshow") {
        if (initState && hasValue(initState, "value.items.0.imageid")) {
          const videoObj = initState.value.items[0];
          imageid = videoObj.imageid ? videoObj.imageid : "";
          imgsize = videoObj.imgsize ? `imgsize-${videoObj.imgsize},` : "";
          imgconfig = "largewidethumb";
        }
      } else if (pagetype === "photoshow") {
        if (initState && hasValue(initState, "value.0.0.items.0.id")) {
          const photoObj = initState.value[0][0].items[0];
          imageid = photoObj.id ? photoObj.id : "";
          // imgsize = photoObj.imgsize ? `imgsize-${photoObj.imgsize},` : "";
          imgconfig = "bigimage";
        }
      }
      if (imageid) {
        const imageSource = `${siteConfig.imageconfig.imagedomain + imageid},${imgsize}${
          siteConfig.imageconfig[imgconfig]
        }/${siteConfig.wapsitename.toLowerCase().replace(" ", "-")}.jpg`;
        return imageSource;
      }
    } else {
      return "";
    }
  } catch (error) {
    console.log(error);
    return "";
  }
}

function renderAmp(renderProps, store, stats, pagetype, requrl, res) {
  const markup = renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>,
  );

  const head = Helmet.rewind();
  const moduleIds = flushWebpackRequireWeakIds();
  const js = getJs(moduleIds, stats);
  const css = getCss(stats);
  const initialState = store.getState();
  const httpPushCache = [];

  let leadImg = "";
  if (pagetype === "home" || pagetype === "articleshow" || pagetype === "videoshow" || pagetype === "photoshow") {
    leadImg = getLeadImageUrl(initialState, pagetype);
  }
  if (leadImg && leadImg !== "") {
    httpPushCache.push("<" + leadImg + ">; rel=preload; as=image");
  }

  const httpCache = httpPushCache.join(", ");
  if (process.env.NODE_ENV == "production") {
    res.setHeader("Link", httpCache);
    // res.setHeader("refreshTime",30000);
  }

  return renderToStaticMarkup(
    <Amp
      js={js}
      css={css}
      html={markup}
      head={head}
      initialState={initialState}
      version={stats.version}
      pagetype={pagetype}
      requrl={requrl}
    />,
  );
}
function renderFbIA(renderProps, store, stats, pagetype, requrl) {
  const markup = renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>,
  );
  // console.log(markup)

  const head = Helmet.rewind();
  const moduleIds = flushWebpackRequireWeakIds();
  const js = getJs(moduleIds, stats);
  const css = getCss(stats);
  const initialState = store.getState();
  // console.log(markup)

  return renderToStaticMarkup(
    <FbIA
      js={js}
      css={css}
      html={markup}
      head={head}
      initialState={initialState}
      version={stats.version}
      pagetype={pagetype}
      requrl={requrl}
      location={renderProps.location}
    />,
  );
}

/**
 * Express middleware to render HTML using react-router
 * @param  {object}     stats Webpack stats output
 * @return {function}   middleware function
 */
export default ({ clientStats }) => {
  // Build stats maps for quicker lookups.
  const modulesById = clientStats.modules.reduce((modules, mod) => {
    modules[mod.id] = mod;
    return modules;
  }, {});
  const chunksById = clientStats.chunks.reduce((chunks, chunk) => {
    chunks[chunk.id] = chunk;
    return chunks;
  }, {});
  const assetsByChunkName = clientStats.assetsByChunkName;

  /**
   * @param  {object}     req Express request object
   * @param  {object}     res Express response object
   * @return {undefined}  undefined
   */

  return (req, res, next) => {
    globalconfig.reqUrl = req.url;
    // Logging related improvements in case of errors
    globalconfig.userAgent = "NA";
    globalconfig.referer = "NA";
    if (req.headers) {
      globalconfig.referer = req.headers.referer || "NA";
      globalconfig.userAgent = req.headers["user-agent"] || "NA";
    }
    // console.log(process.env.API_MASTER_FEED);
    fetch(process.env.API_MASTER_FEED)
      // .then(r => r.json())
      .then(data => {
        const initialState = { app: data };
        const url = req.url;
        let isAmpPage = false;
        let isFbPage = false;
        const isOperaMiniBrowser = isOperaMiniAtServer(req);
        if (checkIsAmpPage(url)) {
          isAmpPage = true;
        } else if (checkIsFbPage(url)) {
          isFbPage = true;
        }
        const memoryHistory = createMemoryHistory(url);
        const store = configureStore(initialState);
        const history = syncHistoryWithStore(memoryHistory, store);
        const cookie = parseCookie(req.headers.cookie);
        // if (
        //   initialState.app.adSizeConfig &&
        //   initialState.app.adSizeConfig[process.env.SITE] &&
        //   initialState.app.adSizeConfig[process.env.SITE].atfSizePercentage &&
        //   initialState.app.adSizeConfig[process.env.SITE].atfSizePercentage !== "false"
        // ) {
        //   const disributionArr = initialState.app.adSizeConfig[process.env.SITE].atfSizePercentage.split(":");
        //   const percentage = parseInt(disributionArr[0]) / 100;
        //   if (counterForAtfV1 / (counterForAtfV1 + counterForAtfV2) < percentage.toFixed(2)) {
        //     globalconfig.newAtfAdSize = initialState.app.adSizeConfig[process.env.SITE].atfSizeVariant1;
        //     counterForAtfV1 += 1;
        //   } else {
        //     globalconfig.newAtfAdSize = initialState.app.adSizeConfig[process.env.SITE].atfSizeVariant2;
        //     counterForAtfV2 += 1;
        //   }
        //   globalconfig.atfheight = globalconfig.newAtfAdSize[0][1];
        //   globalconfig.isExpando = false;
        // } else if (initialState.app.adsize && initialState.app.adsize.atfheight) {
        //   globalconfig.atfheight = initialState.app.adsize.atfheight;
        //   globalconfig.isExpando = true;
        //   delete globalconfig.newAtfAdSize;
        // }
        match(
          {
            history,
            routes,
            location: url,
          },
          (error, redirectLocation, renderProps) => {
            if (error) {
              res.status(500).send(error.message);
            } else if (redirectLocation) {
              res.redirect(302, `${redirectLocation.pathname}${redirectLocation.search}`);
            } else {
              let pagetype = "";
              const longurl =
                renderProps && renderProps.routes && renderProps.routes[2] && renderProps.routes[2].longurl;
              if (longurl) {
                pagetype = getPageType(longurl);
              }
              fetchComponentData(renderProps, store, isOperaMiniBrowser).then(async resp => {
                // call for redirection check api
                const iniState = store.getState();
                checkRedirectLocation(iniState, req, res);
                const dfpAdConfig = _filterRecursiveALJSON(iniState.header, ["pwaconfig", "dfpAdConfig"], true);
                const alaskaMastHead = filterAlaskaData(
                  iniState.header.alaskaData,
                  ["pwaconfig", "masthead", "mobile"],
                  "label",
                );
                const isTimesPointsPage = url.indexOf("timespoints.cms") > -1;

                if (checkTopAtf(dfpAdConfig, url)) {
                  globalconfig.topatfEnabled = true;
                } else {
                  globalconfig.topatfEnabled = false;
                }

                if (
                  !isTimesPointsPage &&
                  alaskaMastHead &&
                  alaskaMastHead._status == "active" &&
                  alaskaMastHead._banner != ""
                ) {
                  globalconfig.alaskaMastHeadEnabled = true;
                } else {
                  globalconfig.alaskaMastHeadEnabled = false;
                }
                let html;
                pagetype = pagetype || getPageType(req.url.replace(/amp_|fb_/gi, ""));
                try {
                  if (isAmpPage) {
                    // console.log(version)
                    html = renderAmp(
                      renderProps,
                      store,
                      {
                        modulesById,
                        chunksById,
                        assetsByChunkName,
                        version: clientStats.hash,
                      },
                      pagetype,
                      req.url,
                      res,
                    );
                    // console.log(html)
                    html = ampConverter(html, getPageType(req.url.replace("amp_", "")), req.url, clientStats.hash);
                    html = replaceHTMLContent(html, 0);
                    html = replaceHTMLContent(html, 1);
                    // html = replaceHTMLContent(html,2);
                    html = replaceHTMLContent(html, 3);
                  } else if (isFbPage) {
                    html = renderFbIA(
                      renderProps,
                      store,
                      {
                        modulesById,
                        chunksById,
                        assetsByChunkName,
                        version: clientStats.hash,
                      },
                      pagetype,
                      req.url,
                    );
                    // Fetching Related articles data
                    const rhs = await getRHS();
                    html = fbIAConverter({ html, pagetype, rhs, renderProps });
                  } else {
                    html = render(
                      renderProps,
                      store,
                      {
                        modulesById,
                        chunksById,
                        assetsByChunkName,
                        cookie,
                        version: clientStats.hash,
                        isOperaMini: isOperaMiniBrowser,
                      },
                      res,
                      req,
                      pagetype,
                    );
                  }
                } catch (ex) {
                  if (process.env.NODE_ENV != "development") {
                    logError(ex.stack);
                    res.status(500).send(errorHTML);
                  } else {
                    return next(ex);
                  }
                }

                if (isReactNotFound(html, store, pagetype)) {
                  res.status(404).send(errorHTML);
                } else {
                  if(!isNotFound(renderProps) && process.env.NODE_ENV =='production' && process.env.PLATFORM=='desktop' && process.env.SITE=='nbt'){
                    if(req.url.indexOf('/tech/')> -1 && req.url.indexOf('/articleshow/')> -1 ){
                      let ex = {}
                      ex.config  ={};
                      ex.config.url = req.url
                      ex.ampDuplicateError = html;
                      logError(ex);
                    }
                    
                  }
                  res.status(isNotFound(renderProps) ? 404 : 200).send(`<!doctype html>${html}`);
                }
              });
            }
          },
        );
      })
      .catch(e => {
        logError(e.stack);
        if (process.env.NODE_ENV == "development") {
          console.log("ERROR EXCEPTIION IN CATCH 3", e);
        }

        res.status(404).send();
      });
  };
};
