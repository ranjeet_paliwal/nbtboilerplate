import React from "react";
import { Route, IndexRoute, Redirect } from "react-router";
import App from "containers/app/App";
// import IndexLoadable from "../../containers/mobile/index/IndexLoadable";
import IndexLoadable from "../../containers/mobile/home/HomeLoadable";
import NotFound from "../../containers/notfound/NotFound";

import MobileLayout from "../../layouts/mobile/MobileLayout";

import ArticleshowLoadable from "../../containers/mobile/articleshow/ArticleshowLoadable";
import Logout from "../../components/common/Logout";
import LoginControl from "../../components/common/LoginControl";
import LiveblogLoadable from "../../containers/liveblog/LiveblogLoadable";
import StaticPageLoadable from "containers/staticpage/StaticPageLoadable";

import ScheduleLoadable from "../../campaign/cricket/containers/schedule/ScheduleLoadable";
import PhotoMazzaShowLoadable from "../../containers/mobile/photomazzashow/PhotoMazzaShowLoadable";
import ListPageLoadable from "../../containers/mobile/listpage/ListPageLoadable";
// import SearchLoadable from "containers/search/SearchLoadable";
import VideoShowLoadable from "../../containers/desktop/videoshow/VideoShowLoadable";

import HomePageLoadable from "../../campaign/cricket/containers/home/indexloadable";
import ExitPoll from "../../campaign/election/containers/exitpoll/indexLoadable";
import CandidateListing from "../../campaign/election/containers/candidateListing/indexLoadable";
import ElectoralMapLoadable from "../../campaign/election/containers/electoralmap/indexLoadable";
import ElectionResultLoadable from "../../campaign/election/containers/results/indexLoadable";
import LokSabhaElectionResultLoadable from "../../campaign/election/loksabhaelectionresult/container/indexLoadable";
import LOKSABHAEXITPOLL_Loadable from "../../campaign/election/loksabhaexitpoll/container/indexLoadable";
import NewsBriefLoadable from "../../containers/mobile/newsbrief/NewsBriefLoadable";
import WebviewLoadable from "../../containers/mobile/webviewpage/WebviewLoadable";
import HomeRecommendedLoadable from "../../containers/mobile/homerecommended/HomeRecommendedLoadable";
import DynamicPageLoadable from "../../containers/mobile/dynamicpage/DynamicPageLoadable";
import TopicsLoadable from "../../containers/mobile/topics/TopicsLoadable";
import MovieShowLoadable from "../../containers/desktop/MovieShow/MovieShowLoadable";

import { getPageType, _getStaticConfig } from "../../utils/util";
import TimesPointLoadable from "../../containers/mobile/timespoint/TimesPointLoadable";
import GadgetsHomeLoadable from "../../containers/mobile/gadgets/gadgetsHome/GadgetsHomeLoadable";
import GadgetsNowHomeLoadable from "../../containers/mobile/gadgets/GadgetsNow/GadgetsNowHomeLoadable";
import IPLLoadable from "../../campaign/cricket/containers/IPL/IPLLoadable";
import ComparisonLoadable from "../../containers/mobile/gadgets/Comparison/List/ComparisonLoadable";
import ComparisonDetailsLoadable from "../../containers/desktop/Gadgets/Comparison/Detail/ComparisonDetailsLoadable";
import GadgetslistLodable from "../../containers/mobile/gadgets/Gadgetlist/GadgetslistLodable";
// import GadgetshowLoadable from "../../containers/mobile/gadgets/gadgetshow/GadgetshowLoadable";
import GadgetshowLoadable from "../../containers/mobile/gadgets/Gadgetshow/GadgetshowLoadable";
import TrendsComparisonLoadable from "../../containers/desktop/Gadgets/Comparison/Trends/TrendsComparisonLoadable";
import gadgetsConfig from "../../utils/gadgetsConfig";
const pathParams = gadgetsConfig.pathParams();
const siteConfig = _getStaticConfig();
const COMPONENT_MAP = {
  articlelist: ListPageLoadable,
};
// const routeArr = require("../../routeConfig.json");
export default (
  <Route path="/" component={App}>
    <Route component={MobileLayout}>
      {/* <IndexRoute component={IndexLoadable} /> */}
      <IndexRoute
        component={
          typeof window !== "undefined" && siteConfig.gadgetdomain.includes(window.location.host)
            ? GadgetsNowHomeLoadable
            : IndexLoadable
        }
      />

      <Route path="/pwa_home.cms" component={IndexLoadable} />
      <Route path="/default.cms" component={IndexLoadable} />
      <Route path="/amp_default.cms" component={IndexLoadable} />
      {/* Route for timespoint page for mobile */}
      <Route path="/timespoints.cms" component={TimesPointLoadable} />
      <Route path="/404.cms" component={NotFound} />
      {/* <Route path="/topics(/:term)(/:curpg)" component={SearchLoadable}/> */}
      {/* <Redirect from="/tech/articlelist/:msid.cms" to="/tech" component={GadgetsHomeLoadable} /> */}
      <Route path="/tech" component={GadgetsHomeLoadable} />
      <Route path="/tech/articlelist/60023487.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/amp_articlelist/60023487.cms" component={GadgetsHomeLoadable} />
      {/*tech  home page*/}
      <Route path={`/tech/articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      <Route path={`/tech/amp_articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />

      {/* MT's Tech Section Id */}
      <Route path={`/gadget-news/articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      <Route path={`/gadget-news/amp_articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />

      {/* .gadgetsnow.com Home Page */}
      <Route path={`/gnh`} component={GadgetsNowHomeLoadable} />
      <Route path={`/gadgetshome/${siteConfig.pages.gadgethome}.cms`} component={GadgetsNowHomeLoadable} />
      <Route path={`/amp_gadgetshome/${siteConfig.pages.gadgethome}.cms`} component={GadgetsNowHomeLoadable} />

      <Redirect
        from="/(*/)iplt20/(articlelist)/:msid.cms"
        to="/sports/cricket/iplt20.cms"
        component={HomePageLoadable}
      />
      <Route path="/tech/compare-:device" component={ComparisonLoadable} />
      <Route path="/gulf" component={IndexLoadable} />
      <Route path="/us" component={IndexLoadable} />
      <Route path="/bangladesh" component={IndexLoadable} />
      <Route path="(*/)iplt20.cms" component={HomePageLoadable} />
      <Route path="(*/)iplt20/amp_articlelist/:msid.cms" component={HomePageLoadable} />
      <Route path="/tech/default.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/articlelist/:msid.cms" component={ListPageLoadable} />
      {/* <Route path="/tech/articlelist/:msid.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/amp_articlelist/:msid.cms" component={GadgetsHomeLoadable} /> */}
      <Redirect from="/tech/reviews/:msid.cms" to="/tech/reviews.cms" component={ListPageLoadable} />
      <Route path="/tech/reviews.cms" component={ListPageLoadable} />
      <Route path="/tech/(*/)(articlelist)/:msid.cms" component={ListPageLoadable} />
      <Route path="/msid-:msid,q-:searchkey,type-:businessType(/*)/profile.cms" component={TopicsLoadable} />
      {/* To only redirect matches with msid-:msid,curpg-:curpg values */}
      {/* https://stackoverflow.com/questions/49162311/react-difference-between-route-exact-path-and-route-path */}
      <Route
        exact
        path="/(*/)(photoarticlelist)(videolist)(articlelist)(photolist)(photomazza)(photogallery)/msid-:msid,curpg-:curpg.cms"
        component={ListPageLoadable}
      />
      <Route
        path="/(*/)(articlelist)(reviews)(amp_reviews)(amp_articlelist)(electionlist)/:msid.cms"
        component={ListPageLoadable}
      />
      {/* (photoarticlelist) prev. used for photolisting     photomazza for NBT photogallery used in VK*/}
      <Route
        path="/(*/)(amp_photoarticlelist)(amp_photolist)(amp_photomazza)(amp_photogallery)/:msid.cms"
        component={ListPageLoadable}
      />
      {/* (photoarticlelist) prev. used for photolisting     photomazza for NBT photogallery used in VK*/}
      <Route
        path="/(*/)(photoarticlelist)(photolist)(photomazza)(photogallery)/:msid.cms"
        component={ListPageLoadable}
      />
      <Route path="/(*/)photomazaashow/:msid.cms" component={PhotoMazzaShowLoadable} />
      <Route path="/(*/)(photoshow)(fb_photoshow)/:msid.cms" component={PhotoMazzaShowLoadable} />
      <Route path="/(*/)(amp_photoshow)/:msid.cms" component={PhotoMazzaShowLoadable} />
      <Route path="/(*/)(photoshow)/msid-:msid,picid-:picid.cms" component={PhotoMazzaShowLoadable} />
      <Route
        path="/(*/)(articleshow)(amp_articleshow)(fb_articleshow)(articleshow_esi_test)/:msid.cms"
        component={ArticleshowLoadable}
      />
      <Route path="/(*/)(moviereview)(amp_moviereview)/:msid.cms" component={ArticleshowLoadable} />
      <Route path="/(*/)(liveblog)(amp_liveblog)/:msid.cms" component={LiveblogLoadable} />
      <Route path="/(*/)(videolist)(amp_videolist)/:msid.cms" component={ListPageLoadable} />
      <Route path="/(*/)(videoshow)(fb_videoshow)/:msid.cms" component={VideoShowLoadable} />
      <Route path="/(*/)(amp_videoshow)/:msid.cms" component={VideoShowLoadable} />
      <Route path="/(*/)schedule/:msid.cms" component={ScheduleLoadable} />
      <Route path="/(*/)pointstable/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)teams/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)venues/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)stats/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)statsdetail/:msid.cms" component={IPLLoadable} />

      <Route path="/newsbrief.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/*/newsbrief/articleid-:artid,msid-:msid.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/:msid.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/*/newsbrief/:msid.cms" component={NewsBriefLoadable} />

      <Route path="/(*/)webviewpage(/*).cms" component={WebviewLoadable} />

      {/* For CSR of Landing/glossary detail page */}
      <Route
        path="/(*/)(topics)(landing)(glossary)(cricketer)(politician)/:searchkey(/:searchtype)(/:curpg)"
        component={DynamicPageLoadable}
      />
      {/* For CSR of Glossary Listing page */}
      <Route path="/(*/):pathkey/glossary" component={DynamicPageLoadable} />
      {/* For SSR of Landing/Glossary detailpage and Glossary listing page */}
      <Route path="/(*/):pathkey/(landing)(glossary)(cricketer)(politician).cms" component={DynamicPageLoadable} />

      {/* <Route path="/(topics)(cricketer)(politician)/:searchkey(/:searchtype)(/:curpg)" component={TopicsLoadable} /> */}

      <Route path="/login.cms" component={LoginControl} />
      <Route path="/pwalogout.cms" component={Logout} />
      {/* {routeArr &&
        routeArr.map(route => {
          if (route.longurl.indexOf("articlelist") > -1) {
            const pageType = getPageType(route.longurl);
            return (
              <Route key={route.url} path={route.url} component={COMPONENT_MAP[pageType]} longurl={route.longurl} />
            );
          }
        })} */}
      {/* Route for personalisation homepage */}
      <Route path="/recommended.cms" component={HomeRecommendedLoadable} />
      {/* Temporary redirects to run new articleshow on PWA. Also added in nbt.js routes */}
      <Redirect from="/education.cms" to="/education/articlelist/2279784.cms" component={ListPageLoadable} />
      <Redirect from="/business.cms" to="/business/articlelist/2279786.cms" component={ListPageLoadable} />
      {/* Routes for election setup */}
      <Redirect
        from="/elections.cms"
        to={`/elections/articlelist/${siteConfig.pages.elections}.cms`}
        component={ListPageLoadable}
      />
      {/*<Route path="/elections/exit-polls" component={ExitPoll} />*/}
      <Route
        path="/elections/assembly-elections/:statename/(exitpolls)(amp_exitpolls)/:msid.cms"
        component={ExitPoll}
      />
      <Route path="/elections/assembly-elections/exitpolls/:msid.cms" component={ExitPoll} />
      <Route path="/elections/assembly-elections/:statename/candidates/:msid.cms" component={CandidateListing} />
      {/*<Redirect from="/elections/exitpolls/:msid.cms" to="/elections/exit-polls" component={ExitPoll} />*/}
      <Route path="/elections/assembly-elections/results" component={ElectionResultLoadable} />
      <Route path="/elections/assembly-elections/results/:msid.cms" component={ElectionResultLoadable} />
      <Route path="/elections/assembly-elections/(results)(amp_results)/:msid.cms" component={ElectionResultLoadable} />
      <Route
        path="/elections/assembly-elections/:statename/(results)(amp_results)/:msid.cms"
        component={ElectionResultLoadable}
      />
      {/* <Route path="/elections/assembly-elections/:statename/results/:msid.cms" component={ElectionResultLoadable} /> */}
      <Route
        path="/elections/lok-sabha-elections/(results)(amp_results)/:msid.cms"
        component={LokSabhaElectionResultLoadable}
      />
      <Route path="/elections/lok-sabha-elections/results.cms" component={LokSabhaElectionResultLoadable} />
      <Route path="/elections/lok-sabha-elections/exitpolls/:msid.cms" component={LOKSABHAEXITPOLL_Loadable} />
      <Route path="/elections/lok-sabha-elections/exitpolls.cms" component={LOKSABHAEXITPOLL_Loadable} />
      <Route path="/elections/assembly-elections/:statename/electoralmap/:msid.cms" component={ElectoralMapLoadable} />
      {/* routes for worldcup */}
      <Route path="/sports/cricket/(*/)(sportsresult)(amp_sportsresult)/:msid.cms" component={ScheduleLoadable} />
      {/* routes for Gadget Now */}
      <Route path="/tech/compare-:device/:criteria-comparisons" component={TrendsComparisonLoadable} />
      <Route path="/tech/compare-:device/:qstring/amp_compareshow.cms" component={ComparisonDetailsLoadable} />

      <Route path="/tech/compare-:device/:qstring" component={ComparisonDetailsLoadable} />
      <Route path="/tech/compare-:device" component={ComparisonLoadable} />
      <Route path="/tech/compare.cms" component={ComparisonLoadable} />

      {/*  </AMP Routes GadgetList>  */}
      {/*  <AMP Routes GadgetShow> */}
      <Route path="/tech/:category/*-*/amp_gadgetshow.cms" component={GadgetshowLoadable} />
      {/*  </AMP Routes GadgetShow> */}
      {/*  <AMP Routes GadgetList> */}

      {/*  <AMP Routes GadgetList> */}

      <Route path="/tech/compare.cms" component={ComparisonLoadable} />
      <Route path="/tech/(upcoming-):category/amp_gadgetslist.cms" component={GadgetslistLodable} />
      <Route path="/tech/:category/:brand/amp_gadgetslist.cms" component={GadgetslistLodable} />
      {/* <Route
        path="/tech/:category/(i-smart)(hi-tech)(sony-ericsson)(t-mobile)/amp_gadgetslist.cms"
        component={GadgetslistLodable}
      /> */}
      <Route path={`/tech/:category/${pathParams}`} component={GadgetslistLodable} />
      {/*  </AMP Routes GadgetList>  */}

      <Route path="/tech/(upcoming-):category" component={GadgetslistLodable} />
      <Route path="/tech/(upcoming-):category/filters/:filters" component={GadgetslistLodable} />
      <Route path="/tech/:category/(i-smart)(hi-tech)(sony-ericsson)(t-mobile)" component={GadgetslistLodable} />
      <Route path="/tech/:category/*-*" component={GadgetshowLoadable} />
      <Route path="/tech/(upcoming-):category/:brand" component={GadgetslistLodable} />

      <Route path="/staticpage/:staticpage" component={StaticPageLoadable} />
      <Route path=":hyperlocal" component={ListPageLoadable} />

      <Route path="/(*/)(movieshow)(amp_movieshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(tvshow)(amp_tvshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(webseriesshow)(amp_webseriesshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(webseriesreview)(amp_webseriesreview)/:msid.cms" component={ArticleshowLoadable} />

      <Route path="*" component={NotFound} />
    </Route>
  </Route>
);

export { NotFound as NotFoundComponent };
