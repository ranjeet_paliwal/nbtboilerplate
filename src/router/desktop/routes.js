import React from "react";
import { Route, IndexRoute, Redirect } from "react-router";
import NotFound from "containers/notfound/NotFound";
import HomeLoadableD from "containers/desktop/home/HomeLoadable";
import PhotoShowLoadable from "containers/desktop/photoshow/PhotoShowLoadable";
import ArticleshowLoadableD from "containers/desktop/articleshow/ArticleshowLoadable";
import AppD from "../../containers/app/AppD";
import globalconfig from "../../globalconfig";
// import HomeGnLoadableD from "containers/desktop/homegn/HomeGnLoadable";
// import GadgetshowLodableD from "containers/desktop/gadgetshow/GadgetshowLoadable";
// import CompareshowLodableD from "containers/desktop/compareshow/CompareshowLodable";
import HomePageLoadable from "../../campaign/cricket/containers/homeD/indexloadable";
import DesktopLayout from "../../layouts/desktop/DesktopLayout";
import VideoShowLoadable from "../../containers/desktop/videoshow/VideoShowLoadable";
import TopicsLoadable from "../../containers/desktop/topics/TopicsLoadable";
import DynamicPageLoadable from "../../containers/desktop/dynamicpage/DynamicPageLoadable";
import ListPageLoadable from "../../containers/desktop/listpage/ListPageLoadable";
import GadgetsHomeLoadable from "../../containers/desktop/Gadgets/gadgetsHome/GadgetsHomeLoadable";
import GadgetsNowHomeLoadable from "../../containers/desktop/Gadgets/GadgetsNow/GadgetsNowHomeLoadable";

import LoginControl from "../../components/common/LoginControl";
import LiveblogLoadable from "../../containers/liveblog/LiveblogLoadable";
import Logout from "../../components/common/Logout";
import ScheduleLoadable from "../../campaign/cricket/containers/schedule/ScheduleLoadable";
// import PointsTableLoadable from "../../campaign/cricket/containers/pointstable/PointsTableLoadable";
import ExitPoll from "../../campaign/election/containers/exitpoll/indexLoadable";
import Results from "../../campaign/election/containers/results/indexLoadable";
import CandidateListing from "../../campaign/election/containers/candidateListing/indexLoadable";
import ElectoralMapLoadable from "../../campaign/election/containers/electoralmap/indexLoadable";
// Gadgets Specfic Import
import ComparisonLoadable from "../../containers/desktop/Gadgets/Comparison/List/ComparisonLoadable";
import ComparisonDetailsLoadable from "../../containers/desktop/Gadgets/Comparison/Detail/ComparisonDetailsLoadable";
import GadgetshowLoadable from "../../containers/desktop/Gadgets/Gadgetshow/GadgetshowLoadable";
import GadgetslistLodable from "../../containers/desktop/Gadgets/Gadgetlist/GadgetslistLodable";
import TrendsComparisonLoadable from "../../containers/desktop/Gadgets/Comparison/Trends/TrendsComparisonLoadable";
import NewsBriefLoadable from "../../containers/mobile/newsbrief/NewsBriefLoadable";

import { getPageType, _getStaticConfig } from "../../utils/util";
import IPLLoadable from "../../campaign/cricket/containers/IPL/IPLLoadable";
import StaticPageLoadable from "../../containers/staticpage/StaticPageLoadable";
import MovieShowLoadable from "../../containers/desktop/MovieShow/MovieShowLoadable";
import gadgetsConfig from "../../utils/gadgetsConfig";

const pathParams = gadgetsConfig.pathParams();

const siteConfig = _getStaticConfig();
// const COMPONENT_MAP = {
//   articlelist: ListPageLoadable,
// };
//const routeArr = require("../../routeConfig.json");
// async function getRouteMapping() {
//   try {
//     const response = await fetch("http://dev.samayam.com/staticpage/file?filename=routeConfig.json");
//     const data = await response.json();
//     console.log(data);
//     return data;
//   } catch (e) {
//     console.log(e, "-----");
//     return [];
//   }
// }
export default (
  <Route path="/" component={AppD}>
    {/* <Route component={MobileLayout}> */}
    <Route component={DesktopLayout}>
      <IndexRoute
        component={
          typeof window !== "undefined" && siteConfig.gadgetdomain.includes(window.location.host)
            ? GadgetsNowHomeLoadable
            : HomeLoadableD
        }
      />
      <Route path="/default.cms" component={HomeLoadableD} />
      {/* {routeArr &&
        Array.isArray(routeArr) &&
        routeArr.map(route => {
          if (route.routetype === "route") {
            return <Redirect to={route.url} component={COMPONENT_MAP[route.pagetype]} from={route.longurl} />;
          }
        })} */}
      <Route path="/tech" component={GadgetsHomeLoadable} />
      {/* VK's Tech Section Id */}
      <Route path="/tech/articlelist/60023487.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/amp_articlelist/60023487.cms" component={GadgetsHomeLoadable} />
      <Route path="/gulf" component={HomeLoadableD} />
      <Route path="/us" component={HomeLoadableD} />
      <Route path="/bangladesh" component={HomeLoadableD} />
      {/* .gadgetsnow.com Home Page */}
      <Route path={`/gnh`} component={GadgetsNowHomeLoadable} />
      <Route path={`/gadgetshome/${siteConfig.pages.gadgethome}.cms`} component={GadgetsNowHomeLoadable} />
      <Route path={`/amp_gadgesthome/${siteConfig.pages.gadgethome}.cms`} component={GadgetsNowHomeLoadable} />
      {/*Tech  home page*/}
      <Route path={`/tech/articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      <Route path={`/tech/amp_articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      {/* MT's Tech Section Id */}
      <Route path={`/gadget-news/articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      <Route path={`/gadget-news/amp_articlelist/${siteConfig.pages.tech}.cms`} component={GadgetsHomeLoadable} />
      <Route path="/sports/cricket/(*/)sportsresult/:msid.cms" component={ScheduleLoadable} />
      <Route path="/sports/cricket/(*)/results/:msid.cms" component={ScheduleLoadable} />
      <Route path="/(*/)teams/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)venues/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)stats/:msid.cms" component={IPLLoadable} />
      <Route path="/(*/)statsdetail/:msid.cms" component={IPLLoadable} />
      <Route path="/newsbrief.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/*/newsbrief/articleid-:artid,msid-:msid.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/:msid.cms" component={NewsBriefLoadable} />
      <Route path="/newsbrief/*/newsbrief/:msid.cms" component={NewsBriefLoadable} />
      <Redirect
        from="/(*/)iplt20/(articlelist)/:msid.cms"
        to="/sports/cricket/iplt20.cms"
        component={HomePageLoadable}
      />
      <Route path="(*/)iplt20.cms" component={HomePageLoadable} />
      <Route path="/tech/default.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/articlelist/:msid.cms" component={ListPageLoadable} />
      <Route path="/(*/)schedule/:msid.cms" component={ScheduleLoadable} />
      <Route path="/(*/)pointstable/:msid.cms" component={IPLLoadable} />
      {/* <Route path="/tech/articlelist/:msid.cms" component={GadgetsHomeLoadable} />
      <Route path="/tech/amp_articlelist/:msid.cms" component={GadgetsHomeLoadable} /> */}
      <Redirect from="/tech/reviews/:msid.cms" to="/tech/reviews.cms" component={ListPageLoadable} />
      <Route path="/tech/reviews.cms" component={ListPageLoadable} />
      <Route path="/tech/(*/)(articlelist)/:msid.cms" component={ListPageLoadable} />
      <Route path="/msid-:msid,q-:searchkey,type-:businessType(/*)/profile.cms" component={TopicsLoadable} />
      {/* To only redirect matches with msid-:msid,curpg-:curpg values */}
      {/* https://stackoverflow.com/questions/49162311/react-difference-between-route-exact-path-and-route-path */}
      <Route
        exact
        path="/(*/)(photoarticlelist)(videolist)(articlelist)(photolist)(photomazza)(photogallery)/msid-:msid,curpg-:curpg.cms"
        component={ListPageLoadable}
      />
      <Route path="/(*/)(videolist)(amp_videolist)/:msid.cms" component={ListPageLoadable} />
      <Route
        path="/(*/)(photoarticlelist)(photolist)(photomazza)(photogallery)/:msid.cms"
        component={ListPageLoadable}
      />
      <Route path="/(*/)(articlelist)(reviews)(amp_articlelist)(electionlist)/:msid.cms" component={ListPageLoadable} />
      {/* Routes for election setup */}
      <Redirect
        from="/elections.cms"
        to={`/elections/articlelist/${siteConfig.pages.elections}.cms`}
        component={ListPageLoadable}
      />
      {/* <Route path="/elections/exit-polls" component={ExitPoll} /> */}
      <Route path="/elections/assembly-elections/:statename/exitpolls/:msid.cms" component={ExitPoll} />
      <Route path="/elections/assembly-elections/exitpolls/:msid.cms" component={ExitPoll} />
      <Route path="/elections/assembly-elections/:statename/candidates/:msid.cms" component={CandidateListing} />
      <Route path="/elections/assembly-elections/:statename/results/:msid.cms" component={Results} />
      <Route path="/elections/assembly-elections/results/:msid.cms" component={Results} />
      {/* <Redirect from="/elections/exitpolls/:msid.cms" to="/elections/exit-polls" component={ExitPoll} /> */}
      <Route path="/elections/assembly-elections/:statename/electoralmap/:msid.cms" component={ElectoralMapLoadable} />
      <Route
        path="/(*/)(articleshow)(amp_articleshow)(articleshow_esi_test)/:msid.cms"
        component={ArticleshowLoadableD}
      />
      <Route path="/(*/)(moviereview)/:msid.cms" component={ArticleshowLoadableD} />
      <Route path="/(*/)(liveblog)(amp_liveblog)/:msid.cms" component={LiveblogLoadable} />
      {/* photoshow route msid-:msid,picid-:picid need to be placed above (:msid) route, otherwise later one would work for all scenarios, create re rendering issue */}
      <Route path="/(*/)(photoshow)/msid-:msid,picid-:picid.cms" component={PhotoShowLoadable} />
      <Route path="/(*/)(photoshow)(amp_photoshow)/:msid.cms" component={PhotoShowLoadable} />
      <Route path="/(*/)(videoshow)/:msid.cms" component={VideoShowLoadable} />
      {/* For CSR of Landing/glossary detail page */}
      <Route
        path="/(*/)(topics)(landing)(glossary)(cricketer)(politician)/:searchkey(/:searchtype)(/:curpg)"
        component={DynamicPageLoadable}
      />
      {/* For CSR of Glossary Listing page */}
      <Route path="/(*/):pathkey/glossary" component={DynamicPageLoadable} />
      {/* For SSR of Landing/Glossary detailpage and Glossary listing page */}
      <Route path="/(*/):pathkey/(landing)(glossary)(cricketer)(politician).cms" component={DynamicPageLoadable} />
      {/* <Route path="/(topics)(cricketer)(politician)/:searchkey(/:searchtype)(/:curpg)" component={TopicsLoadable} /> */}
      <Route path="/login.cms" component={LoginControl} />
      <Route path="/pwalogout.cms" component={Logout} />
      <Route path="/(*/)(movieshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(tvshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(webseriesshow)/:msid.cms" component={MovieShowLoadable} />
      <Route path="/(*/)(webseriesreview)/:msid.cms" component={ArticleshowLoadableD} />
      {/** Gadgets Route */}
      <Route path="/tech/compare-:device/:criteria-comparisons" component={TrendsComparisonLoadable} />
      <Route path="/tech/compare.cms" component={ComparisonLoadable} />
      <Route path="/compare.cms" component={ComparisonLoadable} />
      <Route path="/tech/compare-:device" component={ComparisonLoadable} />
      <Route path="/tech/compare-:device/:qstring" component={ComparisonDetailsLoadable} />
      <Route path="/tech/(upcoming-):category" component={GadgetslistLodable} />
      <Route path="/tech/(upcoming-):category/filters/:filters" component={GadgetslistLodable} />
      {/* {["i-smart", "hi-tech", "sony-ericsson"].map((path, index) => (
        <Route path={`/tech/:category/${path}`} component={GadgetslistLodable} key={index} />
      ))} */}
      <Route path={`/tech/:category/${pathParams}`} component={GadgetslistLodable} />
      <Route path="/tech/:category/*-*" component={GadgetshowLoadable} />
      <Route path="/tech/(upcoming-):category/:brand" component={GadgetslistLodable} />
      {/** Gadgets Route */}
      {/* <Route path="/cacheinfo" component={FlushCache} /> */}
      {/* {!_isCSR() &&
        routeArr &&
        routeArr.map(route => {
          if (route.longurl.indexOf("articlelist") > -1) {
            const pageType = getPageType(route.longurl);
            return (
              <Route key={route.url} path={route.url} component={COMPONENT_MAP[pageType]} longurl={route.longurl} />
            );
          }
        })} */}
      <Route path="/staticpage/:staticpage" component={StaticPageLoadable} />
      <Route path="/404.cms" component={NotFound} />
    </Route>
  </Route>
);
export { NotFound as NotFoundComponent };
