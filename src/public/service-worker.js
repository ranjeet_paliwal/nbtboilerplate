/* eslint-disable no-restricted-globals */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-inner-declarations */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const DOMAIN_NAME = self.location.origin;
const API_BASEPOINT = "https://navbharattimes.indiatimes.com/pwafeeds";
const ARTICLE_API_CACHE = "articles-api-cache";
const PHOTO_API_CACHE = "photos-api-cache";
const VIDEO_API_CACHE = "videos-api-cache";
const HOME_HTML_CACHE = "homepage-html-cache";
const SITE_ID = "696089404";

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉 at`, DOMAIN_NAME);
  workbox.clientsClaim();
  workbox.precaching.suppressWarnings();

  self.__precacheManifest.unshift(
    {
      url: "/",
      revision: new Date().getTime(),
    },
    {
      url: "/?web=no",
      revision: new Date().getTime(),
    },
    {
      url: "/icons/nbtfavicon.ico",
    },
  );

  workbox.precaching.precacheAndRoute(self.__precacheManifest || [], {
    directoryIndex: null,
    cleanUrls: false,
  });

  workbox.routing.registerRoute(
    "/",
    workbox.strategies.staleWhileRevalidate({
      cacheName: HOME_HTML_CACHE,
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 1,
          maxAgeSeconds: 7 * 24 * 60 * 60, // 7 days
        }),
      ],
    }),
  );

  workbox.routing.registerRoute(
    "/?web=no",
    workbox.strategies.staleWhileRevalidate({
      cacheName: HOME_HTML_CACHE,
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 1,
          maxAgeSeconds: 7 * 24 * 60 * 60, // 7 days
        }),
      ],
    }),
  );

  // const homeApiHandler = workbox.strategies.staleWhileRevalidate({
  //   cacheName: 'top-news',
  //   plugins: [
  //     new workbox.broadcastUpdate.Plugin('topnews_api_updates')
  //   ]
  // });
  const articleApiHandler = workbox.strategies.staleWhileRevalidate({
    cacheName: ARTICLE_API_CACHE,
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        maxAgeSeconds: 24 * 60 * 60, // 1day
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [200],
      }),
    ],
  });
  const photoApiHandler = workbox.strategies.cacheFirst({
    cacheName: PHOTO_API_CACHE,
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 10,
        maxAgeSeconds: 24 * 60 * 60, // 1day
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [200],
      }),
    ],
  });
  const videoApiHandler = workbox.strategies.cacheFirst({
    cacheName: VIDEO_API_CACHE,
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 10,
        maxAgeSeconds: 24 * 60 * 60, // 1day
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [200],
      }),
    ],
  });

  workbox.routing.registerRoute(/(.*)\/pwa_articleshow\.cms/, args =>
    articleApiHandler
      .handle(args)
      .then(response => {
        if (response.status === 404) {
          return caches.match("404.html");
        }
        return response;
      })
      .catch(ex => caches.match("500.html")),
  );

  workbox.routing.registerRoute(/(.*)\/pwa_photoshow\.cms/, args =>
    photoApiHandler
      .handle(args)
      .then(response => {
        if (response.status === 404) {
          return caches.match("404.html");
        }
        return response;
      })
      .catch(ex => caches.match("500.html")),
  );

  workbox.routing.registerRoute(/(.*)\/pwa_videoshow\.cms/, args =>
    videoApiHandler
      .handle(args)
      .then(response => {
        if (response.status === 404) {
          return caches.match("404.html");
        }
        return response;
      })
      .catch(ex => caches.match("500.html")),
  );

  function cacheAllHomePageApi() {
    const acceptedTemplates = ["news", "photo", "video", "moviereview"];
    let template;
    let cacheName;
    return fetch(`${API_BASEPOINT}/pwa_home/${SITE_ID}.cms?type=headline&count=20&feedtype=sjson`).then(response => {
      response.json().then(result => {
        result.items.map((item, index) => {
          if (typeof item.id === "undefined" || !acceptedTemplates.includes(item.tn) || index > 20) return;

          switch (item.tn) {
            case "news":
              template = "pwa_articleshow";
              cacheName = ARTICLE_API_CACHE;
              break;
            case "photo":
              template = "pwa_photoshow";
              cacheName = PHOTO_API_CACHE;
              break;
            case "video":
              template = "pwa_videoshow";
              cacheName = VIDEO_API_CACHE;
              break;
            case "moviereview":
              template = "pwa_articleshow";
              cacheName = ARTICLE_API_CACHE;
              break;
            default:
              break;
          }
          const apiUrl = `${API_BASEPOINT}/${template}.cms?feedtype=sjson&version=v9&msid=${item.id}`;
          fetch(apiUrl).then(response => {
            if (response.status == 200) {
              caches.open(cacheName).then(cache => {
                cache.put(apiUrl, response);
              });
            }
          });
        });
      });
    });
  }

  self.addEventListener("install", event => {
    console.log("Inside Install");
    const urls = ["/"];
    const cacheName = HOME_HTML_CACHE; // workbox.core.cacheNames.runtime
    event.waitUntil(
      caches.open(cacheName).then(cache =>
        cache
          .addAll(urls)
          .then(data => {})
          .catch(error => {
            self.skipWaiting();
          }),
      ),
    );
    return event.waitUntil(self.skipWaiting());
  });

  self.addEventListener("activate", event => {
    event.waitUntil(self.clients.claim()); // Become available to all pages
  });

  self.addEventListener("sync", event => {
    if (event.tag == "syncHomePage") {
      event.waitUntil(cacheAllHomePageApi());
    }
  });

  self.addEventListener("message", event => {
    console.log(`SW Received Message: ${event.data}`);
    // var Promise1 = fetch('/'+event.data[0]);
    // var Promise2 = fetch('/'+event.data[1]);
    // var Promise3 = fetch('/'+event.data[2]);
    const jsArray = event.data.map(js => fetch(js));
    Promise.all(jsArray).then(
      data => event.ports[0].postMessage("SW Says 'Hello back!'"),
      error => event.ports[0].postMessage("SW Says 'Hello back Error!'"),
    );
  });

  workbox.routing.registerRoute(
    /https:\/\/static.langimg.com\/thumb/,
    workbox.strategies.cacheFirst({
      cacheName: "dynamic-images",
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [200],
        }),
        new workbox.expiration.Plugin({
          maxEntries: 20,
          maxAgeSeconds: 6 * 60 * 60, // 6hrs
        }),
      ],
    }),
    "GET",
  );

  workbox.routing.registerRoute(
    /https:\/\/static.nbt.indiatimes.com/,
    workbox.strategies.cacheFirst({
      cacheName: "static-resources",
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [200],
        }),
        new workbox.expiration.Plugin({
          maxEntries: 100,
          maxAgeSeconds: 30 * 24 * 60 * 60, // 30d
        }),
      ],
    }),
    "GET",
  );

  // workbox.routing.registerRoute(/http:\/\/localhost/,
  //   workbox.strategies.cacheFirst({
  //     cacheName: "static-resources-js",
  //     plugins: [
  //       new workbox.cacheableResponse.Plugin({
  //         statuses: [200]
  //       }),
  //       new workbox.expiration.Plugin({
  //         maxEntries: 100,
  //         maxAgeSeconds: 30 * 24 * 60 * 60 // 30d
  //       })
  //     ]
  //   }), 'GET');
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
