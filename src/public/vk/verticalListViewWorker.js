/* eslint-disable no-unused-expressions */
let apiUrl1 =
  document &&
  document.getElementById("listApiUrl") &&
  document.getElementById("listApiUrl").attributes &&
  document.getElementById("listApiUrl").attributes.length > 0 &&
  document.getElementById("listApiUrl").attributes[1] &&
  document.getElementById("listApiUrl").attributes[1].value;

let apiUrl0 =
  document &&
  document.getElementById("listApiUrl") &&
  document.getElementById("listApiUrl").attributes &&
  document.getElementById("listApiUrl").attributes.length > 0 &&
  document.getElementById("listApiUrl").attributes[0] &&
  document.getElementById("listApiUrl").attributes[0].value;

const listTypeMap = {
  photo: "photolist",
  video: "videolist",
  news: "articlelist",
  moviereview: "articlelist",
  slideshow: "photolist",
};

const apiUrl = apiUrl1.includes("https://") ? apiUrl1 : apiUrl0;

if (apiUrl && apiUrl != "" && apiUrl != undefined) {
  fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
      let items = data && data.items;
      let title = (data && data.secname) || "";

      let content = "";
      if (items) {
        items.forEach((item, i) => {
          if (item.tn === "news" || item.tn === "photo" || item.tn === "video" || item.tn === "slideshow") {
            let type = "";
            if (item.tn === "news") {
              type = "articleshow";
            } else if (item.tn === "photo" || item.tn === "slideshow") {
              type = "photoshow";
            } else if (item.tn === "video") {
              type = "videoshow";
            }
            let listType = "";
            if (item.tn) {
              listType = listTypeMap[item.tn];
            }
            let articleUrl = `/${item.seolocation}/${type}/${item.id}.cms`;
            const imgSrc =
              item && item.imageid
                ? `https://static.langimg.com/thumb/msid-${item.imageid},imgver-65252,width-320,height-240,resizemode-75/navbharat-times.jpg`
                : "";
            content += `<li data-attr="as" title="${item && item.hl}" itemtype="http://schema.org/ListItem"
            class=" news-card col news midthumb ${i === 0 ? "col12 lead" : "horizontal"}">
               <span class="img_wrap" >
                 <a href="${articleUrl}">
                   <amp-img alt= "${item && item.hl}" src="${imgSrc}"
                   layout= "responsive" width="540" height="300">
                     <amp-img fallback layout="fill" src="https://static.langimg.com/thumb/msid-67144443,width-540,height-405,resizemode-4/xyz.jpg" width="540" height="405" alt="" />
                   </amp-img>
                 </a>
               </span>
               <span class="table_col con_wrap">
                 <a href="${articleUrl}">
                   <span class="text_ellipsis">${item && item.hl}</span>
                 </a>
               </span>
             </li>`;
          }
        });
      }
      //     Array.isArray(superHitWidgetData) &&
      //     superHitWidgetData.forEach(element => {
      //       superHitWidgetContent += `<li data-attr="as" title="${element &&
      //         element.hl}" itemtype="http://schema.org/ListItem"
      // class="news-card horizontal col news">
      //    <span class="table_col img_wrap" >
      //      <a href="${element && element.wu}">
      //        <amp-img alt= "${element && element.hl}" src="${element && element.imgsrc}"
      //        layout= "responsive" width="540" height="300">
      //          <amp-img fallback layout="fill" src="https://static.langimg.com/thumb/msid-67144443,width-540,height-405,resizemode-4/xyz.jpg" width="540" height="405" alt="" />
      //        </amp-img>
      //      </a>
      //    </span>
      //    <span class="table_col con_wrap">
      //      <a href="${element && element.wu}">
      //        <span class="text_ellipsis">${element && element.hl}</span>
      //      </a>
      //    </span>
      //  </li>`;
      //     });

      if (content != "") {
        let mainContent = `<div class="row box-item">
        <div class="section">
          <div class="top_section">
            <h3>
              <span>${title}</span>
            </h3>
          </div>
        </div>
        <ul class="col12">
          ${content}
        </ul>
      </div>`;
        document.body.innerHTML = mainContent;
      }
    });
}
