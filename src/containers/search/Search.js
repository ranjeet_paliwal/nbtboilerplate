import React, { Component } from "react";
import { Link, browserHistory } from "react-router";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import Loader from "components/lib/loader/Loader";
import Error from "components/lib/error/Error";
import Cookies from "universal-cookie";
import {
  fetchSearchIfNeeded,
  fetchNextSearchIfNeeded,
  clearSearchResult
} from "actions/search/search";
import { changeSearchHeader } from "../../actions/config/config";
import FakeListing from "./../../components/common/FakeCards/FakeListing";
import NewsListCard from "./../../components/common/NewsListCard";
import ListHorizontalCard from "./../../components/common/ListHorizontalCard";
import AdCard from "./../../components/common/AdCard";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import { throttle, scrollTo } from "./../../utils/util";
import { _getStaticConfig } from "./../../utils/util";
const siteConfig = _getStaticConfig();
const pages = siteConfig.pages;

export class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      _scrollEventBind: false,
      searchDataFetching: false
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params } = this.props;
    const { query } = this.props.location;
    const _this = this;

    let term = "";
    if (params.hasOwnProperty("term") && typeof params.term != "undefined") {
      term = params.term;
    }
    dispatch(changeSearchHeader(true, term));

    document.getElementById("root").classList.add("adjustPadding");
    Search.fetchData({ dispatch, query, params });
    window.setTimeout(() => {
      _this.afterRender();
    }, 1000);
    scrollTo(document.documentElement, 0, 0);
  }

  afterRender() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  handleScroll(e) {
    if (this.state._scrollEventBind == false) return;

    let winHeight = window.innerHeight;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = document.documentElement.scrollTop;
    }
    let footerHeight = document.getElementById("footerContainer").offsetHeight;
    docHeight = docHeight - footerHeight;

    if (scrollValue + 1000 > docHeight) {
      const { dispatch, params, isFetching } = this.props;
      const { query } = this.props.location;
      if (isFetching == false) {
        try {
          Search.fetchNextSearchListData({ dispatch, query, params });
        } catch (ex) {}
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    let nterm = nextProps.params.hasOwnProperty("term")
      ? nextProps.params.term
      : "";
    let pterm = this.props.params.hasOwnProperty("term")
      ? this.props.params.term
      : "";

    let _this = this;
    if (nterm != pterm && this.state.searchDataFetching == false) {
      this.state.searchDataFetching = true;
      const { dispatch, params, query } = nextProps;
      Search.fetchData({ dispatch, query, params }).then(() => {
        _this.state.searchDataFetching = false;
      });
      let term = "";
      if (nextProps.params.hasOwnProperty("term")) {
        term = nextProps.params.term;
      }
      dispatch(changeSearchHeader(true, term));
      scrollTo(document.body, 0, 300);
    }
  }

  componentWillUnmount() {
    const _this = this;
    this.props.dispatch(clearSearchResult());
    if (typeof window != "undefined") {
      this.props.dispatch(changeSearchHeader(false, ""));
      document.getElementById("root").classList.remove("adjustPadding");
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  render() {
    const { isFetching, error, value, params } = this.props;

    if (value && value.hasOwnProperty("data")) {
      return (
        <div id="content" role="main">
          {isFetching ? <Loader /> : ""}

          <Helmet defaultTitle={"NBT"} titleTemplate={"NBT"} />
          <div className="search-section">
            <ul>
              {value.data.map((item, index) => {
                return (
                  <li key={index}>
                    <Link to={"/topics/" + encodeURI(item)}>{item}</Link>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      );
    } else if (
      value &&
      value.hasOwnProperty("items") &&
      value.items.length > 0
    ) {
      let term =
        params.hasOwnProperty("term") && typeof params.term != "undefined"
          ? params.term
          : "";
      return (
        <div id="content" data-search-term={term} className="body-content">
          {isFetching ? <Loader /> : ""}
          {error ? <Error>{error}</Error> : ""}

          <Helmet
            defaultTitle={term + ": Latest News, Photos & Videos on " + term}
            titleTemplate={"%s - NBT"}
            meta={[
              {
                name: "description",
                content:
                  "Find latest news, photos, and videos of " +
                  term +
                  " from across all newspapers on News Point"
              },
              {
                name: "keywords",
                content:
                  term +
                  " News, " +
                  term +
                  " photos, latest " +
                  term +
                  " news, " +
                  term +
                  " videos"
              }
            ]}
          />
          <div className="top-news-content">
            {typeof params.term != "undefined" ? (
              <h3 className="search-title">{params.term}</h3>
            ) : null}
            <ul className="nbt-list">
              {value.items.map((item, index) => {
                if (item.tn == "news" || item.tn == "html") {
                  return (
                    <ErrorBoundary key={index}>
                      <NewsListCard item={item} />
                    </ErrorBoundary>
                  );
                } else if (item.tn == "photopreview") {
                  return (
                    <ErrorBoundary key={index}>
                      <ListHorizontalCard item={item} />
                    </ErrorBoundary>
                  );
                }
              })}

              {isFetching ? <FakeListing showImages={true} /> : null}
            </ul>
          </div>
        </div>
      );
    } else if (
      isFetching == false &&
      value &&
      (!value.hasOwnProperty("items") || value.items.length <= 0)
    ) {
      return (
        <div
          id="content"
          data-search-term={params.hasOwnProperty("term") ? params.term : ""}
        >
          {isFetching ? <Loader /> : ""}
          {error ? <Error>{error}</Error> : ""}

          <Helmet
            defaultTitle={"NBT"}
            titleTemplate={"%s - NBT"}
            meta={[{ name: "description", content: "NBT" }]}
          />
          <div className="videoshow article-section">
            <section>
              <Error key={1}>No data found</Error>
            </section>
          </div>
        </div>
      );
    } else {
      return (
        <div id="content">
          {isFetching ? <Loader /> : ""}
          {error ? <Error>{error}</Error> : ""}

          <Helmet
            defaultTitle={"NBT"}
            titleTemplate={"%s - NBT"}
            meta={[{ name: "description", content: "NBT" }]}
          />
          <div className="videoshow article-section">
            <section>
              <FakeListing />
            </section>
          </div>
        </div>
      );
    }
  }
}

Search.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchSearchIfNeeded(params, query));
};

Search.fetchNextSearchListData = function({ dispatch, query, params }) {
  return dispatch(fetchNextSearchIfNeeded(params, query));
};

function mapStateToProps(state) {
  return {
    ...state.search
  };
}

export default connect(mapStateToProps)(Search);
