import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import Loadable from "react-loadable";
import LiftFetchData from "components/lib/liftfetchdata/liftFetchData";
import Error from "components/lib/error/Error";
import FakeStoryCardDesktop from "../../../components/common/FakeCards/FakeStoryCardDesktop";

const webpackRequireWeakId = () => require.resolveWeak("./Articleshow");

const LoadingComponent = ({ isLoading, error, pastDelay }) => {
  if (isLoading && pastDelay) {
    return <FakeStoryCardDesktop />;
  } else if (error) {
    return <Error>Error!!!</Error>;
  }
  return null;
};

LoadingComponent.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  pastDelay: PropTypes.bool.isRequired,
  error: PropTypes.bool,
};

// NOTE: We're making a trade off for more aggresive code splitting (i.e. includes
// action creators) for waterfall requests when fetching the chunk and the data
// in the client.
const enhance = compose(LiftFetchData(webpackRequireWeakId), Loadable);

export default enhance({
  loader: () => import(/* webpackChunkName: "AS" */ "./Articleshow"),
  LoadingComponent,
  webpackRequireWeakId,
});
