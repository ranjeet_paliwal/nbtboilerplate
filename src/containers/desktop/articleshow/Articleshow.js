import React, { Component } from "react";
import { connect } from "react-redux";

import {
  fetchArticleShowIfNeeded,
  fetchNextArticleShowData,
  resetOnUnmount,
} from "../../../actions/articleshow/articleshow";
// import { fetchAmazonDataIfNeeded, fetchAmazonNextData } from "../../../modules/amazonwidget/actions/amazonwidget";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import StoryCard from "../../../components/common/StoryCard";

import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module, { setSectionDetail } from "../../../components/lib/ads/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
import { setParentId, setPageType } from "../../../actions/config/config";
import TopicPopup from "../../../components/desktop/TopicPopup";
import TopStoryPopup from "../../../components/desktop/TopStoryPopup";

import {
  debounce,
  _isCSR,
  setHyp1Data,
  handleReadMore,
  _checkUserStatus,
  _getCookie,
  _deferredDeeplink,
  _getStaticConfig,
  isLoggedIn,
  updateConfig,
  _apiBasepointUpdate,
  elementInView,
  shouldTPRender,
  isProdEnv,
  filterAlaskaData,
} from "../../../utils/util";
import fetch from "../../../utils/fetch/fetch";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import AdCard from "../../../components/common/AdCard";

import styles from "../../../components/common/css/ArticleShow.scss";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/Desktop.scss";
import "../../../components/common/css/desktop/ArticleShow.scss";
import "../../../components/common/css/desktop/GadgetNow.scss";
// import { fetchDesktopHeaderPromise } from "../../../actions/header/header";
import { fetchRHSDataPromise } from "../../../actions/app/app";
import RhsWidget from "../../../components/desktop/RhsWidget";

import CommentsPopup from "../../../components/common/CommentsPortal";
import { topListDataPromise, fetchTopListDataIfNeeded, makeDataChangedFalse } from "../../../actions/home/home";
import {
  trackersOnScroll,
  mod_symmetricalAds,
  commonCompWillRecieveProps,
  didMountActions,
  didUpdateActions,
  videoDockStatus,
  fetchAdvertorialNews,
  addElementsForScrollDepth,
} from "../../utils/articleshow_util";
import Breadcrumb from "../../../components/common/Breadcrumb";
import FakeStoryCardDesktop from "../../../components/common/FakeCards/FakeStoryCardDesktop";
import globalconfig from "./../../../globalconfig";
import { isFeatureURL } from "../../../components/lib/ads/lib/utils";
import { clearFeedInterval, setRefreshFeedInterval } from "../../utils/home_util";

const siteConfig = _getStaticConfig();
const adsObj = siteConfig.ads;

class Articleshow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBinded: false,
      index: 0,
      rhsCSRData: null,
      rhsCSRVideoData: null,
      superHitWidgetData: null,
      showCommentsPopup: false,
      userChanged: false,
      topicVal: "",
      topics: [],
      advertorialNews: [],
    };
    this.config = {
      perpetual: true,
      maxArticles: 9,
      adcounter: 0,
      load: true,
      resume: true,
      gatracked: true,
      cur_art_height: 0,
      art_scrolled_per: 0,
      last_scrolled_per: 0,
      lastscrolled: 0,
      init_art: null,
      cur_art: null,
      timeout_var: null,
      read_marker: null,
      short_url: null,
      keepnextmsid: null,
      listCount: 1,
      listCountInc: () => {
        return this.config.listCount++;
      },
      advInvoked: false,
    };

    this.scrollWithThrottle = debounce(this.handleScroll.bind(this));
  }

  componentDidMount() {
    didMountActions(this, Articleshow);
    window.scrollTo(0, 0);
    const { router, dispatch } = this.props;

    fetchRHSDataPromise(dispatch);

    const topicNodes = Array.from(document.querySelectorAll(".article-section .topic"));
    const topics = topicNodes.map(topicNode => {
      return topicNode.text;
    });
    topics.forEach(topic => {
      const apiUrl = `${_apiBasepointUpdate(router)}/topiclinks.cms?feedtype=sjson&q=${topic}`;
      fetch(apiUrl);
    });
    this.setState({ topics });
    setRefreshFeedInterval(
      120,
      this.props.home && this.props.home.refreshFeedInterval,
      dispatch,
      "articleshow",
      router,
    );
  }
  componentDidUpdate(prevProps, prevState) {
    const { router, dispatch } = this.props;
    didUpdateActions(this, prevProps, prevState);
    // if (this.props.home.dataChanged) {
    //   this.props.dispatch(makeDataChangedFalse());
    //   fetchRHSDataPromise(dispatch);
    // }
    if (this.props.items.length !== prevProps.items.length) {
      const topicNodes = Array.from(document.querySelectorAll(".article-section .topic"));
      const topics = topicNodes.map(topicNode => {
        return topicNode.text;
      });
      if (topics.length !== this.state.topics.length) {
        const newTopics = topics.filter(newTopic => {
          return !this.state.topics.some(prevTopic => {
            return newTopic === prevTopic;
          });
        });
        this.setState({ topics: newTopics });
        newTopics.forEach(topic => {
          const apiUrl = `${_apiBasepointUpdate(router)}/topiclinks.cms?feedtype=sjson&q=${topic}`;
          fetch(apiUrl);
        });
        this.setState({ topics });
      }
      addElementsForScrollDepth(this);
    }
  }

  componentWillUnmount() {
    let _this = this;
    const { dispatch } = _this.props;
    clearFeedInterval(_this.props.home.refreshFeedInterval, dispatch);
    window.removeEventListener("scroll", _this.scrollWithThrottle);
    // window.removeEventListener("scroll", _this.animate_btn_App);
    window.clearInterval(_this.config.timeout_var);
    _this.state._scrollEventBinded = false;
    // reset pagetype and parentid
    dispatch(setParentId(""));
    dispatch(setPageType(""));
    resetOnUnmount(dispatch);
    //reset section window data
    Ads_module.setSectionDetail();
    //reset hyp1 to ''
    setHyp1Data();
  }

  componentWillReceiveProps(nextProps) {
    commonCompWillRecieveProps(this, nextProps, Articleshow);
  }

  openCommentsPopup = commentsMSID => {
    this.setState({
      showCommentsPopup: true,
      commentsMSID,
    });
  };

  closeCommentsPopup = () => {
    this.setState({ showCommentsPopup: false, commentsMSID: "" });
  };

  getShareDetail = isShareAll => {
    if (!_isCSR()) return false;

    const { items } = this.props;
    let currentHeadline = "";
    let currentShortUrl = "";
    let currentMSID = "";
    if (items) {
      // loop on items length
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const article = document.querySelector(`[data-artBox="article-${item.id}"]`);
        // checking box id view on window height half
        if (item && article && elementInView(article, true)) {
          currentHeadline = item.hl;
          currentShortUrl = item.m;
          currentMSID = item.id;
        }
      }
    }

    let sharedata = {
      title: currentHeadline || document.title,
      url: location.href,
      short_url: currentShortUrl || this.config.short_url,
      openCommentsPopup: this.openCommentsPopup,
      msid: currentMSID,
    };

    if (!isShareAll) return sharedata;

    //for watsapp sharing feature along with GA
    AnalyticsGA.event({
      category: "social",
      action: "Whatsapp_Wap_stickyAS",
      label: sharedata.url,
    });

    if (this.config.short_url != "" && this.config.short_url != null) {
      var info = sharedata.short_url; // check short micron url availability
      info += "/l" + siteConfig.shortutm;
      info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
    } else {
      var info = sharedata.url;
      info += "?utm_source=Whatsapp_Wap_stickyAS" + siteConfig.fullutm;
      info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
    }
    var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + encodeURIComponent(info);
    try {
      if (shouldTPRender() && currentMSID) {
        fireActivity("WHATSAPP_SHARE", `ws_share_${currentMSID}`);
      }
    } catch (e) {}

    window.location.href = whatsappurl;

    return false;
  };

  handleScroll() {
    let _this = this;
    const { header } = _this.props;

    if (_this.state._scrollEventBinded == false) return;

    trackersOnScroll(_this);

    //collect article as per index if perpetual not started
    if (_this.config.cur_art == null || _this.config.cur_art == undefined)
      _this.config.cur_art = _this.config.init_art = document.querySelectorAll("[data-artBox]")[_this.state.index];

    if (_this.config.cur_art_height === 0) _this.config.cur_art_height = _this.config.cur_art.scrollHeight;

    /*article scrolled more then 50% */
    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop == 0 ? html.scrollTop : body.scrollTop;
    let footerHeight = document.getElementById("footerContainer").offsetHeight;
    docHeight = docHeight - footerHeight;
    // console.log("scrollValue", scrollValue);
    if (scrollValue >= 50 && !this.config.advInvoked) {
      const alaskaData = header && header.alaskaData;
      fetchAdvertorialNews(this, alaskaData);
    }
    if (scrollValue + 1000 > docHeight && _this.config.maxArticles >= 1) {
      const { dispatch, params, query, isFetching, router, advertorialData, advertorialPerpectual } = _this.props;

      if (!isFetching) {
        let nextmsid = null;
        if (_this.props.items[0].ismarkreview == "1") {
          let reviewlist = _this.props.items[0].reviewlist;
          nextmsid = reviewlist[_this.state.index].id;
        } else {
          nextmsid = _this.config.nextmsid =
            _this.props.items[_this.state.index].pwa_meta && _this.props.items[_this.state.index].pwa_meta.nextItem
              ? _this.props.items[_this.state.index].pwa_meta.nextItem.msid
              : null;
        }
        //keep same article show trail after Advertorial Perpectual
        if (_this.config.keepnextmsid) {
          nextmsid = _this.config.keepnextmsid;
          _this.config.keepnextmsid = null;
        }

        // Advertorial Perpectual
        if (advertorialPerpectual) {
          let checkPlatform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";
          let filteradvertorial =
            advertorialData && Array.isArray(advertorialData) && advertorialData.length > 0
              ? advertorialData.filter(fdata => {
                  return (
                    (checkPlatform === "mobile" ? fdata.ctype == "adv_wap" : fdata.ctype == "adv") &&
                    fdata.wu &&
                    fdata.wu.includes(process.env.WEBSITE_URL) &&
                    fdata.wu.includes("/articleshow/")
                  );
                })
              : [];

          filteradvertorial.splice(1);

          let advertorialPerpectualMsid =
            filteradvertorial && filteradvertorial.length > 0 && filteradvertorial[0] && filteradvertorial[0].id;

          if (
            filteradvertorial &&
            filteradvertorial.length > 0 &&
            advertorialPerpectual &&
            params &&
            params.msid != advertorialPerpectualMsid
          ) {
            _this.config.keepnextmsid = nextmsid;
            nextmsid = advertorialPerpectualMsid;
          }
        }

        /*check if next-article exist , then resume loading next story */
        if (_this.config.resume && nextmsid && nextmsid !== "") {
          _this.config.resume = !_this.config.resume;
          fetchNextArticleShowData(nextmsid, dispatch, params, query, router, advertorialData)
            .then(data => {
              data = data.payload; //set payload as data
              let { pwa_meta, audetails } = data;

              _this.setState({ index: _this.state.index + 1 });
              _this.config.maxArticles--;
              _this.config.init_art = document.querySelectorAll(`[data-artBox]`)[_this.state.index];

              //reset hyp1 variable
              setHyp1Data(data.pwa_meta);
              setSectionDetail(data.pwa_meta);

              //set amazon key to fetch amazon content
              params.pagename = "articleshow";
              params.amazonkey = data && data.pwa_meta && data.pwa_meta.amazonkey ? data.pwa_meta.amazonkey : null;

              if (audetails) {
                window._editorname = audetails.cd;
              } else {
                //If editor present in pwa_meta , set in window Object
                window._editorname =
                  typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
              }
              //call for symmetrical ad positioning
              if (
                typeof data == "object" &&
                (typeof data.ag == "undefined" ||
                  (typeof data.ag == "string" &&
                    !(
                      data.ag.toLowerCase() == "brandwire" ||
                      data.ag.toLowerCase() == "mediawire" ||
                      data.ag.toLowerCase() == "colombia"
                    ))) &&
                // Load symmetrical ads for articles only if adserving rules are not turned off
                !(pwa_meta && pwa_meta.AdServingRules && pwa_meta.AdServingRules === "Turnoff")
              ) {
                const isMiniTvVisible = videoDockStatus(_this.props.minitv, data);
                mod_symmetricalAds(_this.config.init_art, "", isFeatureURL(data.wu), isMiniTvVisible, data.wu);
              }

              //Readmore button handling
              handleReadMore();

              // Update globalConfig router
              globalconfig.router.location.pathname = data.wu;

              // fetchAmazonNextData(dispatch, params, router)
              //   .then(data => {
              //     data = data.payload; //set payload as data
              //   })
              //   .catch(err => {
              //     console.log("Inside catch", err);
              //   });
            })
            .catch(err => {
              console.log("Inside catch", err);
            });
        }
      }
    }
    const currentUrl = window.location.pathname;
    if (currentUrl.indexOf("-fea-ture/") > -1) {
      // ads block removed in case of feature URL.
      document.querySelectorAll(".prerender").forEach(item => {
        item.style.display = "none";
      });
      document
        .querySelectorAll(
          '.featured-article .parallaxDiv, .featured-article .ad1 , .featured-article [data-plugin="ctn"]',
        )
        .forEach(item => {
          item.style.display = "none";
        });
    } else {
      document.querySelectorAll(".prerender").forEach(item => {
        item.style.display = "block";
      });
    }
  }

  render() {
    let { items, itemNIC, isFetching, params, electionresult, router, error, adconfig, header, dispatch } = this.props;

    // Filtering for amazon top deal widget in RHS
    const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");

    const { rhsCSRData, advertorialNews, rhsCSRVideoData, superHitWidgetData } = this.state;
    let HomePageWidgets = filterAlaskaData(header.alaskaData, ["pwaconfig", "HomePageWidgets"], "label");
    let bannerObj = "";
    let keys = Object.keys(HomePageWidgets);
    for (var i = 0; i < 10; i++) {
      if (HomePageWidgets[keys[i]] && HomePageWidgets[keys[i]]._type == "banner") {
        bannerObj = HomePageWidgets[keys[i]];
        break;
      }
    }

    // let advertorialNews = [];

    // if (advertorial && typeof advertorial == "object") {
    //   for (const key in advertorial) {
    //     if ({}.hasOwnProperty.call(advertorial, key)) {
    //       const item = advertorial[key];
    //       console.log("item", item);
    //       if (item && item._text) {
    //         advertorialNews.push(item);
    //       }
    //     }
    //   }
    // }

    let temp_items = [];
    let msid = "";
    //check for NIC and push it to items array
    let { query } = this.props.location;
    if (query && query.type == "nic" && itemNIC) {
      temp_items.push(itemNIC);
    } else {
      temp_items = items;
    }

    /* Set page type on the basis of current route*/
    let pagetype = "";
    if (this.props.location.pathname && this.props.location.pathname.indexOf("articleshow") > -1) {
      params.pagename = "articleshow";
      pagetype = "articleshow";
    } else if (this.props.location.pathname && this.props.location.pathname.indexOf("moviereview") > -1) {
      params.pagename = "moviereview";
      pagetype = "moviereview";
    }

    //set amazon key to fetch amazon content
    params.amazonkey =
      temp_items && temp_items[0] && temp_items[0].pwa_meta && temp_items[0].pwa_meta.amazonkey
        ? temp_items[0].pwa_meta.amazonkey
        : null;
    // if(temp_items != undefined && temp_items.length > 0 && temp_items[0] && temp_items[0].id === params.msid){

    if (temp_items != undefined && temp_items.length > 0 && temp_items[0] && temp_items[0].id != "") {
      let schema = temp_items[0].recipe
        ? SeoSchema({ pagetype: "articleshow" }).attr().Recipe
        : temp_items[0].pwa_meta && temp_items[0].pwa_meta.pagetype == "moviereview"
        ? SeoSchema({ pagetype: "articleshow" }).attr().Movie
        : temp_items[0].pwa_meta && temp_items[0].pwa_meta.sectionid == siteConfig.pages.giftsection
        ? SeoSchema({ pagetype: "articleshow" }).attr().Article
        : SeoSchema({ pagetype: "articleshow" }).attr().NewsArticle;
      return (
        <div className={`articleshow_body ${temp_items[0].recipe ? "recipeArticle" : null}`}>
          {/* For SEO Meta */
          PageMeta(temp_items[0].pwa_meta)}
          {temp_items[0] &&
          temp_items[0].breadcrumb &&
          temp_items[0].breadcrumb.div &&
          temp_items[0].breadcrumb.div.ul ? (
            <div className="breadcrumb-wrapper">
              <Breadcrumb items={temp_items[0].breadcrumb.div.ul} />
            </div>
          ) : null}

          {//Job or any other schema calling
          temp_items[0] && temp_items[0].pwa_meta && temp_items[0].pwa_meta.schema != ""
            ? SeoSchema().job(temp_items[0].pwa_meta.schema)
            : null}

          {/* {//faq schema calling
          temp_items[0] && temp_items[0].pwa_meta && temp_items[0].pwa_meta.schema != ""
            ? SeoSchema().schema(temp_items[0].pwa_meta.schema)
            : null} */}

          <span className={styles.read_marker + " read_marker"} />
          {temp_items.map((item, index) => {
            if (typeof item != "undefined") {
              msid = item.id;
              // (item.electionresult = electionresult);
              const isFeaturedArticle = isFeatureURL(item.wu);
              const customEventScript = Boolean(item && item.pwa_meta && item.pwa_meta.customevent === "1");
              return (
                <ErrorBoundary key={item.id}>
                  {/* top story popup */}
                  <div>
                    <TopicPopup topic={this.state.topicVal} parentObj={this} id={item.id} />
                  </div>
                  <div className="row" data-artbox={"article-" + item.id} gatracked={index == 0 ? "true" : ""}>
                    {index > 0 ? (
                      <div className="story_partition">
                        <span>{siteConfig.locale.next_article[pagetype ? pagetype : "articleshow"]}</span>
                      </div>
                    ) : null}
                    <div className="col8" {...schema}>
                      {/* this is custom script for bennet univercity */}
                      {customEventScript ? (
                        <div
                          data-exclude="amp"
                          dangerouslySetInnerHTML={{
                            __html: `<script>
                                  !function(f,b,e,v,n,t,s)
                                  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                  n.queue=[];t=b.createElement(e);t.async=!0;
                                  t.src=v;s=b.getElementsByTagName(e)[0];
                                  s.parentNode.insertBefore(t,s)}(window, document,'script',
                                  'https://connect.facebook.net/en_US/fbevents.js');
                                  fbq('init', '2009952072561098');
                                  fbq('track', 'PageView');
                                  </script>
                                  <noscript><img height="1" width="1" style="display:none"
                                  src="https://www.facebook.com/tr?id=2009952072561098&ev=PageView&noscript=1"
                                  /></noscript>
                                  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-922733241"></script>
                                  <script>
                                  window.dataLayer = window.dataLayer || [];
                                  function gtag(){dataLayer.push(arguments);}
                                  gtag('js', new Date());
                                  gtag('config', 'AW-922733241');
                                  </script> `,
                          }}
                        />
                      ) : null}
                      {/* End  custom script for bennet univercity */}
                      <StoryCard
                        item={item}
                        index={index}
                        isFetching={isFetching}
                        mostViewedData={rhsCSRData && rhsCSRData.section && rhsCSRData.section[0]}
                        rhsCSRVideoData={rhsCSRVideoData}
                        openCommentsPopup={this.openCommentsPopup.bind(this, item.id)}
                        superHitWidgetData={superHitWidgetData}
                        sharedata={this.getShareDetail}
                        pagetype={pagetype}
                        query={query}
                        dispatch={this.props.dispatch}
                        router={this.props.router}
                        isLastArticle={index === temp_items.length - 1}
                        platform={"desktop"}
                        isFeaturedArticle={isFeaturedArticle}
                        adconfig={adconfig}
                        parentObj={this}
                        advertorialNews={advertorialNews}
                        pwaConfigAlaska={pwaConfigAlaska}
                      />

                      {/* FIXME: Correct loggedIn */}
                      <CommentsPopup
                        msid={this.state.commentsMSID}
                        showCommentsPopup={this.state.showCommentsPopup}
                        closeComments={this.closeCommentsPopup}
                        loggedIn={isLoggedIn()}
                        closeOnOverLay={true}
                        isNotEu={true}
                      />
                    </div>
                    <div className="col4">
                      {/* <AdCard mstype="mrec1" adtype="dfp" /> */}
                      <RhsWidget
                        rhsCSRData={rhsCSRData}
                        rhsCSRVideoData={rhsCSRVideoData}
                        articleId={item.id}
                        pathname={this.props.location.pathname}
                        articleSecId={item && item.subsec1}
                        sectionType={item && item.pwa_meta && item.pwa_meta.sectiontype}
                        subsecname={item && item.pwa_meta && item.pwa_meta.subsectitle1}
                        isFeaturedArticle={isFeaturedArticle}
                        pwaConfigAlaska={pwaConfigAlaska}
                        dispatch={dispatch}
                        showDataDrawer={index === 0}
                        bannerdata={bannerObj}
                      />
                      {/*  DFP Ad commented as per TML-1886*/}

                      {/* <AdCard mstype="ctnrhsend" adtype="ctn" /> */}
                    </div>

                    {isFeaturedArticle ? null : (
                      <div className="top-ad">
                        <AdCard mstype="btf" adtype="dfp" />
                      </div>
                    )}
                  </div>
                </ErrorBoundary>
              );
            }
          })}

          {isFetching ? <FakeStoryCardDesktop /> : null}
        </div>
      );
    } else {
      return <FakeStoryCardDesktop />;
    }
  }
}

function mapStateToProps(state) {
  // console.log("ssss", state);
  const isPopUpVisible = state.videoplayer.showMinitv;
  const miniTvData = state && state.app && state.app.minitv;
  return {
    ...state.topics,
    ...state.articleshow,
    adconfig: (state && state.app && state.app.adconfig) || {},
    minitv: {
      isPopUpVisible,
      miniTvData,
    },
    home: state && state.home,
    advertorialData: state && state.app && state.app.rhsData,
    header: state && state.header,
    app: state.app,

    // electionresult:state.electionresult
  };
}

Articleshow.fetchData = ({ dispatch, params, query, history, router }) => {
  //set pagetype
  // dispatch(setPageType("articleshow"));
  // dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router));
  return dispatch(fetchArticleShowIfNeeded(params, query, router)).then(data => {
    //fetchDesktopHeaderPromise
    const pwaMeta = data && data.payload && data.payload.pwa_meta;
    dispatch(setPageType("articleshow", pwaMeta && pwaMeta.site));

    if (pwaMeta) {
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }

    //fetchRHSDataPromise(dispatch);
    fetchTopListDataIfNeeded(dispatch, "", router);

    return data;
    // return Promise.all([
    //   fetchDesktopHeaderPromise(dispatch, parentId),
    //   fetchRHSDataPromise(dispatch),
    //   // topListDataPromise(dispatch, 10)
    // ]);
  });
};

export default connect(mapStateToProps)(Articleshow);
