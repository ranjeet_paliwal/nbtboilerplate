import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
/*import PropTypes from 'prop-types';
import Loader from 'components/lib/loader/Loader';
import Error from 'components/lib/error/Error';
import Cookies from 'universal-cookie';*/
import Breadcrumb from "./../../../components/common/Breadcrumb";
import { fetchShowDataIfNeeded, updateVideoShow } from "../../../actions/videoshow/videoshow";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "./../../../components/lib/errorboundery/ErrorBoundary";
import VideoItem from "../../../components/common/VideoItem/VideoItem";
import fetch from "../../../utils/fetch/fetch";
import { getImageParameters } from "../../../components/common/ImageCard/ImageCardUtils";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import TPicon from "../../../components/common/TimesPoints/TPWidgets/TPicon";
import "./../../../components/common/css/desktop/VideoShowCard.scss";
import AmazonBtn from "../../../components/common/AmazonBtn/AmazonBtn";

import { SeoSchema, PageMeta } from "./../../../components/common/PageMeta"; //For SEO Meta and Schema

import {
  _getStaticConfig,
  isMobilePlatform,
  generateUrl,
  handleReadMore,
  _isCSR,
  updateConfig,
  checkIsAmpPage,
  filterAlaskaData,
  _deferredDeeplink,
} from "./../../../utils/util";
const siteConfig = _getStaticConfig();
//const pages = siteConfig.pages;

import { setPageType, setParentId } from "../../../actions/config/config";
import { setSectionDetail } from "../../../components/lib/ads";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import WebTitleCard from "../../../components/common/WebTitleCard";

import VideoPlayer from "../../../modules/videoplayer/player";
import { createConfig } from "../../../modules/videoplayer/utils";
import { playVideo, removeAllSlikePlayerInitializations } from "../../../modules/videoplayer/slike";
import Slider from "../../../components/desktop/Slider/index";
import { fetchHeaderDataIfNeeded, fetchHeaderPromise, fetchDesktopHeaderPromise } from "../../../actions/header/header";

import KeyWordCard from "../../../components/common/KeyWordCard";
import SocialShare from "../../../components/common/SocialShare";
import AdCard from "../../../components/common/AdCard";
import AnchorLink from "../../../components/common/AnchorLink";
import YSubscribeCard from "../../../components/common/YSubscribeCard";
// import tpConfig from "../../../components/common/TimesPoints/TimesPointConfig";

const tpConfig = siteConfig.TimesPoint;
const videoPlayerContainerName = "video_show_container";

export class VideoShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msid: "",
      relatedvideoData: null,
      embedOptionsShown: false,
      embedPopupShown: false,
      moreVideos: {
        items: [],
        nextvideos: {
          status: "false", // we keep true as string because we got the same as string in feed
          pageno: 2,
        },
      },
      watchLaterVideos: {},
    };
    if (_isCSR()) {
      removeAllSlikePlayerInitializations();
    }
    this.pageViewTriggered = false;
  }

  inVokeIbeat(pwaMeta) {
    if (pwaMeta && typeof pwaMeta == "object" && pwaMeta.ibeat) {
      if (typeof document) {
        document.title = pwaMeta.webtitle;
      }
      setIbeatConfigurations(pwaMeta.ibeat);
    }
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const _this = this;
    // create and dispatch the event
    // setTimeout(function() {
    //   var event = new CustomEvent("updateSectionID");
    //   document.dispatchEvent(event);
    // }, 1000);

    // document.querySelector('.master-player-container').style.visibility = 'hidden';
    /*try {
      if (typeof document !== "undefined") {
        const intervalId = setInterval(() => {
          const videoDesc = document.querySelector(".enable-read-more .text_ellipsis");
          if (videoDesc && videoDesc.scrollHeight > 54) {
            handleReadMore();
            const viewMoreButton = document.querySelector(".enable-read-more .second_col");
            if (viewMoreButton) {
              viewMoreButton.style.display = "inline-block";
            }
          }
          if (videoDesc && videoDesc.scrollHeight > 0) {
            clearInterval(intervalId);
          }
        }, 2000);
      }
    } catch (ex) {}*/

    handleReadMore();

    this.state.msid = params ? params.msid : this.props.msid;
    const pwaMeta = (value && value.pwa_meta) || {};
    const audetails = (value && value.audetails) || {};

    // this.inVokeIbeat(value && { ...value.pwa_meta });
    if (pwaMeta) {
      if (audetails) {
        window._editorname = audetails.cd;
      } else {
        window._editorname =
          typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
      }
      // Hit ga
      AnalyticsGA.pageview(window.location.pathname, {
        setEditorName: true,
      });
      this.pageViewTriggered = true;

      dispatch(setPageType("videoshow", pwaMeta.site));

      _this.inVokeIbeat(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }

    _this.animate_btn_App();
    VideoShow.fetchData({ dispatch, params }).then(data => {
      try {
        //set section and subsection in window
        const { pwa_meta } = value ? value : {};
        handleReadMore();
        if (pwa_meta && typeof pwa_meta == "object") {
          // Fetch RHS data in CSR using the subsection id
          const relatedvideomapid =
            pwa_meta && pwa_meta.parentid
              ? pwa_meta.parentid
              : pwa_meta.sectionid
              ? pwa_meta.sectionid
              : pwa_meta.navsecid;
          if (relatedvideomapid) {
            //const relatedvideomapid = pwa_meta.relatedvideomapid;
            const relatedVideoURL =
              relatedvideomapid && relatedvideomapid != ""
                ? `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=${
                    isMobilePlatform() ? 6 : 18
                  }&feedtype=sjson${pwa_meta.pagetype === "videoshow" ? "&pagetype=videoshow" : ""}`
                : null;
            if (relatedVideoURL) {
              fetch(relatedVideoURL)
                .then(data => {
                  this.setState({
                    relatedvideoData: data,
                  });
                })
                .catch({});
            }
          }

          setSectionDetail(pwa_meta);
          //reset hyp1 variable
        }
      } catch (e) {}
    });
    // Bind Eventlistner

    document.addEventListener("updateVideoShow", this.updateVideoShow.bind(this));
  }

  updateVideoShow(event) {
    const { dispatch } = this.props;
    this.props.dispatch(updateVideoShow(event.detail.msid));
  }

  componentDidUpdate(prevProps, prevState) {
    const _this = this;
    const { location, dispatch, params, value } = this.props;
    if (prevProps.value !== value) {
      _this.inVokeIbeat(value.pwa_meta);
      _this.animate_btn_App();

      //const { pwa_meta } = this.props.value;
      const pwa_meta = this.props.value && this.props.value.pwa_meta;
      const relatedvideomapid =
        pwa_meta && pwa_meta.parentid ? pwa_meta.parentid : pwa_meta.sectionid ? pwa_meta.sectionid : pwa_meta.navsecid;

      if (relatedvideomapid) {
        try {
          //const relatedvideomapid = this.props.value.pwa_meta.relatedvideomapid;
          const relatedVideoURL =
            relatedvideomapid && relatedvideomapid != ""
              ? `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=${
                  isMobilePlatform() ? 6 : 18
                }&feedtype=sjson${pwa_meta.pagetype === "videoshow" ? "&pagetype=videoshow" : ""}`
              : null;
          if (relatedVideoURL) {
            fetch(relatedVideoURL)
              .then(data => {
                this.setState({
                  relatedvideoData: data,
                });
              })
              .catch({});
          }
        } catch (e) {}
      }
    } //FIXME: Ask Ranjeet for the same
    else if (prevProps.location.pathname !== location.pathname) {
      VideoShow.fetchData({ dispatch, params }).then(() => {
        handleReadMore();
      });
      _this.animate_btn_App();
      /*const intervalId = setInterval(() => {
        const videoDesc = document.querySelector(".enable-read-more .text_ellipsis");
        if (videoDesc && videoDesc.scrollHeight > 54) {
          handleReadMore();
          const viewMoreButton = document.querySelector(".enable-read-more .second_col");
          if (viewMoreButton) {
            viewMoreButton.style.display = "inline-block";
          }
        }
        if (videoDesc && videoDesc.scrollHeight > 0) {
          clearInterval(intervalId);
        }
      }, 2000);*/
    }
  }

  showMoreVideos = (pageno, secId) => {
    // alert("ss", pageno);
    const _this = this;
    const moreVideosCopy = { ...this.state.moreVideos };

    fetch(`${process.env.API_BASEPOINT}/sc_videodata/${secId}.cms?feedtype=sjson&type=video&curpg=${pageno}`).then(
      function(response) {
        const videos = response.items;
        const previousVideos = moreVideosCopy.items;
        const totalVideos = [...previousVideos, ...videos];

        moreVideosCopy.items = totalVideos;
        moreVideosCopy.nextvideos = response.nextvideos;
        _this.setState({
          moreVideos: moreVideosCopy,
        });
      },
    );
  };

  addToWatchLater(videoId) {
    return fetch(
      `https://myt.indiatimes.com/myt.indiatimes.com/mytimes/addActivityDdup/?appKey=${Config.APP_KEY}&baseEntityType=VIDEO&objectType=B&activityType=WishList&_=1498213873895&uniqueAppID=${videoId}`,
      { type: "jsonp" },
    );
  }

  toggleWatchLater = videoId => {
    const { watchLaterVideos } = this.state;
    // const { authentication, showLoginRegister } = this.props;
    // const { loggedIn } = authentication;
    if (!loggedIn) {
      alert("Kindly Login to contiue!!");
      // showLoginRegister();
      // return;
    }
    if (watchLaterVideos[videoId]) {
      // Remove from watch later if it exists
      this.removeFromWatchLater(videoId);
      this.setState(prevState => ({
        watchLaterVideos: {
          ...prevState.watchLaterVideos,
          [videoId]: !prevState.watchLaterVideos[videoId],
        },
      }));
    } else {
      // Add to watch later
      this.addToWatchLater(videoId)
        .then(() => {
          this.setState(prevState => ({
            watchLaterVideos: {
              ...prevState.watchLaterVideos,
              [videoId]: true,
            },
          }));
        })
        .catch(() => {});
    }
  };

  removeFromWatchLater(videoId, activityId) {
    if (typeof activityId === "undefined") {
      fetch(
        `https://myt.indiatimes.com/mytimes/activities/entity?appKey=${Config.APP_KEY}&activityType=WishList&uniqueAppID=${videoId}`,
        { type: "jsonp" },
      ).then(data => {
        if (!data.length) {
          return;
        }
        if (data[0] && data[0].id) {
          removeFromWatchLater(videoId, data[0].id);

          return fetch(
            `https://myt.indiatimes.com/mytimes/removeActivity?appKey=${Config.APP_KEY}&baseEntityType=VIDEO&objectType=B&activityType=WishList&_=1500551520199&activityId=${activityId}&uniqueAppID=${videoId}`,
            { type: "jsonp" },
          )
            .then(data => {
              return true;
            })
            .catch(e => false);
        }
      });
    }
  }

  hideEmbedPopup = () => {
    this.setState({
      embedPopupShown: false,
    });
  };

  showEmbedPopup = () => {
    this.setState({
      embedPopupShown: true,
    });
  };
  toggleEmbedOptions = () => {
    const { embedOptionsShown } = this.state;
    this.setState({ embedOptionsShown: !embedOptionsShown });
  };
  // getLapsedTime(dateFormat) {
  //   // const d1 = new Date(dateFormat).getTime();
  //   const d1 = new Date("Mon 23 Dec 2019 17:12:04 GMT+0530").getTime();
  //   const d2 = Date.now();
  //   var timeDiff = d2 - d1; // Math.ceil(7.004)
  //   if (timeDiff / 1000 > 60) {
  //     if (timeDiff / (1000 * 60) > 60) {
  //       if (timeDiff / (1000 * 60 * 60) > 60) {
  //         if ((timeDiff / 1000) * 60 * 60 * 24 > 365) {
  //           timesElapsed = `${Math.ceil(
  //             timeDiff / (1000 * 60 * 60 * 24)
  //           )} Year ago`;
  //         } else {
  //           timesElapsed = `${Math.ceil(
  //             (timeDiff / 1000) * 60 * 60 * 24
  //           )} Days ago`;
  //         }
  //       } else {
  //         timesElapsed = `${Math.ceil(timeDiff / (1000 * 60 * 60))} Hours ago`;
  //       }
  //     } else {
  //       timesElapsed = `${Math.ceil(timeDiff / (1000 * 60))} Min ago`;
  //     }
  //   } else {
  //     timesElapsed = `${Math.ceil(timeDiff / 1000)} sec ago`;
  //   }
  // }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(setPageType(""));

    //reset section to ''
    setSectionDetail();
    // When video is in paused state, and we exit page, it doesn't get docked
    // Remove all slike initializations in this case ( also fires grx video_played event as a result)
    if (!window.adPlaying && window.currentPlayerInst) {
      if (window.currentPlayerInst.getVideoState) {
        const videoState = window.currentPlayerInst.getVideoState();
        if (videoState && videoState.paused) {
          removeAllSlikePlayerInitializations();
        }
      }
    }
  }

  getShareDetail = () => {
    const { value } = this.props;

    const dataToShare = {
      title:
        (value && Array.isArray(value.items) && value.items[0] && value.items[0].hl) ||
        (value && value.pwa_meta && value.pwa_meta.webtitle),
      url: value && value.pwa_meta && value.pwa_meta.canonical,
      msid: (value && Array.isArray(value.items) && value.items[0] && value.items[0].id) || "",
      short_url: value && Array.isArray(value.items) && value.items[0] && value.items[0].m,
    };

    return dataToShare;
  };

  thumbClick = (event, item) => {
    removeAllSlikePlayerInitializations();
    // event.preventDefault();
    // const config = createConfig(item, "", siteConfig, "videoShow");
    // playVideo(config, videoPlayerContainerName);
  };

  prepareLink(objectNode) {
    if (objectNode) {
      if (objectNode.override) {
        return objectNode.override;
      } else {
        return `${process.env.WEBSITE_URL}${objectNode.seolocation || ""}/videolist/${objectNode.secid}.cms`;
      }
    }
    return "";
  }

  animate_btn_App() {
    // Check is button visible or not
    let ele = document.querySelector('[data-attr="btn_openinapp"]');
    if (ele) {
      ele.style.left = "-175px";
      ele.style.display = "none";
      window.addEventListener("scroll_down", function() {});

      window.addEventListener("scroll_up", function() {
        ele.style.display = "block";
        ele.style.left = "0";
      });
    }
  }

  render() {
    const { value, error, isLoadingNextVideo, isFetching, router, header } = this.props;
    const source = "videoshow";
    const { moreVideos, watchLaterVideos, embedOptionsShown, embedPopupShown, relatedvideoData } = this.state;
    const firstVideo = (value && value.items && value.items[0]) || "";
    const videosFromOtherSection = (value && value.othersection) || "";
    const recommendedVideos = (value && value.recommended) || "";
    const NextVideos = (value && value.items && value.items[0] && value.items[0].nextvideos) || "";

    const pwaMeta = value && value.pwa_meta ? { ...value.pwa_meta } : "";
    const videoEid = value && value.items && value.items[0] && value.items[0].eid;
    const embedIframe = `<iframe src="//tvid.in/${videoEid}/lang?autoplay=false" style="height: 100%; width: 100%; max-height: 100%; max-width: 100%; visibility: visible;" border="0" frameBorder="0" seamless="" scrolling="no" allowfullscreen="true" mozallowfullscreen="true" allowtransparency="true"></iframe>`;
    const temp_items = firstVideo;
    const msid = value && value.id;
    if (pwaMeta) {
      pwaMeta.seodescription = ""; // don't want to show descripton along with Web Title heading
    }

    const parentId = (pwaMeta && pwaMeta.parentid) || "";
    const editorName = (value && value.audetails && value.audetails.primary && value.audetails.primary.name) || "";

    if (error || (pwaMeta && pwaMeta.pagetype != "videoshow")) {
      return null;
    }
    let pathname = router && router.location && router.location.pathname;

    let isAmpPage = checkIsAmpPage(pathname);

    const newsArticleSchema = isAmpPage ? SeoSchema({ pagetype: "articleshow" }).attr().NewsArticle : null;

    const subsecmsid2 = pwaMeta.parentid;
    const config = createConfig(firstVideo, subsecmsid2, siteConfig, "videoshow");
    let configFloatBtn = filterAlaskaData(header.alaskaData, ["pwaconfig", "floatingamazon"], "label");

    let thumbnailURL = getImageParameters({
      msid: firstVideo && (firstVideo.imageid || firstVideo.id),
      title: (firstVideo && firstVideo.seotitle) || "",
      size: "largewidethumb",
    }).src;
    return !isFetching && firstVideo ? (
      <div className="body-content">
        {!isMobilePlatform() && value && value.breadcrumb && value.breadcrumb.div && value.breadcrumb.div.ul && (
          <ErrorBoundary>
            <Breadcrumb items={value.breadcrumb.div.ul} />
          </ErrorBoundary>
        )}
        {value && value.pwa_meta ? PageMeta(value.pwa_meta) : null}

        <div className="videoshow-container hypervideo">
          <div className="top_wdt_video">
            <div className="row">
              {firstVideo && (
                <React.Fragment>
                  <div className="col8" {...newsArticleSchema}>
                    {SeoSchema({
                      pagetype: source,
                    }).metaPosition(0)}
                    {SeoSchema({ pagetype: source }).mainEntry(firstVideo.wu)}
                    {SeoSchema({ pagetype: source }).headline(firstVideo.hl)}
                    {SeoSchema({
                      pagetype: source,
                    }).dateStatus(firstVideo.dlseo, firstVideo.luseo)}
                    {SeoSchema().publisherObj()}
                    {/* {SeoSchema().metaTag({ name: "author", content: "websitename" })} */}
                    <span itemProp="author" itemScope="1" itemType={"http://schema.org/Person"}>
                      <meta itemProp="name" content={editorName} />
                    </span>
                    {SeoSchema().metaTag({
                      name: "articleBody",
                      content: firstVideo && firstVideo.Story,
                    })}
                    {SeoSchema().metaTag({
                      name: "url",
                      content: firstVideo && firstVideo.wu,
                    })}
                    {SeoSchema({ pagetype: source }).language()}
                    <span itemProp="image" itemScope itemType="https://schema.org/ImageObject">
                      <meta itemProp="url" content={thumbnailURL} />
                      <meta content="1600" itemProp="width" />
                      <meta content="900" itemProp="height" />
                    </span>
                  </div>
                  <div
                    className="col8"
                    {...SeoSchema({ pagetype: source }).listItem()}
                    {...SeoSchema({
                      pagetype: source,
                    }).videoattr().videoObject}
                    {...SeoSchema({
                      pagetype: source,
                    }).attr().NewsArticle}
                    // {...newsArticleSchema}
                  >
                    {SeoSchema({
                      pagetype: source,
                    }).metaPosition(0)}
                    {SeoSchema({ pagetype: source }).mainEntry(firstVideo.wu)}
                    {SeoSchema({
                      pagetype: source,
                    }).uploadDate(firstVideo.dlseo)}
                    {SeoSchema({ pagetype: source }).duration(firstVideo.duv)}
                    {SeoSchema({ pagetype: source }).headline(firstVideo.hl)}
                    {SeoSchema({
                      pagetype: source,
                    }).dateStatus(firstVideo.dlseo, firstVideo.luseo)}
                    {/* {SeoSchema({ pagetype: source }).keywords("dddd")} //keyword */}
                    {SeoSchema({ pagetype: source }).name(firstVideo.hl)}
                    {/* there is no such property under videoshow schema */}
                    {SeoSchema({
                      pagetype: source,
                    }).contentUrl(firstVideo.pu)}
                    {SeoSchema().publisherObj()}
                    {/* {SeoSchema().metaTag({ name: "author", content: "websitename" })} */}
                    <span itemProp="author" itemScope="1" itemType={"http://schema.org/Person"}>
                      <meta itemProp="name" content={editorName} />
                    </span>
                    {SeoSchema({ pagetype: source }).thumbnail(thumbnailURL)}
                    <ErrorBoundary>
                      {isMobilePlatform() && (
                        <div className="section tpstory_title">
                          <h1>
                            <span>{firstVideo.hl}</span>
                          </h1>
                          <TPicon basePath={siteConfig.mweburl} pageType={"videoshow"} />
                        </div>
                      )}
                      <VideoPlayer
                        autoplay
                        autoDock
                        playerWrapper={videoPlayerContainerName}
                        width={typeof isMobilePlatform == "function" && isMobilePlatform() ? "320" : "630"}
                        height={typeof isMobilePlatform == "function" && isMobilePlatform() ? "180" : "354"}
                        config={config}
                        imageid={firstVideo.imageid}
                        videoMsid={firstVideo.id}
                        imgsize={firstVideo.imgsize}
                        schema
                        lead={true}
                        // type="pop-up"
                      />
                    </ErrorBoundary>

                    <div className="con_social share_container">
                      <YSubscribeCard />
                      {!isMobilePlatform() && (
                        <React.Fragment>
                          <div title="Embed" onClick={this.showEmbedPopup} className="vid_embbed">
                            Embed
                          </div>
                          <div style={{ display: embedPopupShown ? "block" : "none" }} className="body_overlay" />
                          <div style={{ display: embedPopupShown ? "block" : "none" }} className="emmbedUrl">
                            <div className="emmbedhead">
                              <span className="txt">Press CTRL+C to copy</span>
                              <span className="close_icon" onClick={this.hideEmbedPopup}>
                                X
                              </span>
                            </div>
                            <div id="emebecodeval">{embedIframe}</div>
                          </div>
                          <div id={`widget-two-vs-${firstVideo && firstVideo.id}`} className="wdt_timespoints" />
                        </React.Fragment>
                      )}
                      <SocialShare sharedata={this.getShareDetail.bind(this)} icons="fb,twitter,whatsapp" />
                    </div>

                    {/* Open in App Button Floating */
                    isMobilePlatform() &&
                    msid != "" &&
                    configFloatBtn &&
                    configFloatBtn._type == "appinstall" &&
                    _isCSR() ? (
                      ReactDOM.createPortal(
                        <a
                          className="btn_openinapp"
                          data-attr="btn_openinapp"
                          style={!isAmpPage ? { display: "none" } : null}
                          href={_deferredDeeplink(
                            "videoshow",
                            siteConfig.appdeeplink,
                            msid,
                            pwaMeta && pwaMeta.htmlview ? pwaMeta.htmlview : null,
                            null,
                            pwaMeta && pwaMeta.htmlview ? "openinapp_as" : pwaMeta && pwaMeta.app_tn,
                          )}
                        >
                          {siteConfig.locale.see_in_app}
                        </a>,
                        document.getElementById("share-container-wrapper"),
                      )
                    ) : isMobilePlatform() && msid != "" && configFloatBtn && configFloatBtn._type == "appinstall" ? (
                      <a
                        className="btn_openinapp"
                        data-attr="btn_openinapp"
                        style={!isAmpPage ? { display: "none" } : null}
                        href={_deferredDeeplink(
                          "videoshow",
                          siteConfig.appdeeplink,
                          msid,
                          pwaMeta && pwaMeta.htmlview ? pwaMeta.htmlview : null,
                          null,
                          pwaMeta && pwaMeta.htmlview ? "openinapp_as" : pwaMeta && pwaMeta.app_tn,
                        )}
                      >
                        {siteConfig.locale.see_in_app}
                      </a>
                    ) : null}
                    {isMobilePlatform() && configFloatBtn && configFloatBtn._type == "amazon" && (
                      <AmazonBtn data={configFloatBtn} isAmpPage={isAmpPage} />
                    )}
                    {!isMobilePlatform() && (
                      <div className="section tpstory_title">
                        <h1>
                          <span>{firstVideo.hl}</span>
                        </h1>
                      </div>
                    )}
                    <div className="con_wrap">
                      <div className="more_items ">
                        {/* <span
                            title="Add to Watch Later"
                            className={
                              watchLaterVideos[firstVideo.eid]
                                ? "active watchlater"
                                : "watchlater"
                            }
                            onClick={() => this.toggleWatchLater(firstVideo.eid)}
                          /> */}
                        <span className="newsdate">{firstVideo.lu}</span>

                        {firstVideo.viewed && firstVideo.viewed > 50 ? (
                          <span className="views">Views: {firstVideo.viewed}</span>
                        ) : null}
                      </div>
                      <div className="enable-read-more">
                        <div className="first_col">
                          <h2
                            className="caption text_ellipsis videodesc"
                            {...SeoSchema({ pagetype: source }).videoattr().description}
                            dangerouslySetInnerHTML={{ __html: (firstVideo && firstVideo.Story) || "" }}
                          ></h2>
                        </div>
                        {isMobilePlatform() ? (
                          <div className="second_col" data-exclude="amp">
                            <span className="readmore expand">&nbsp;</span>
                          </div>
                        ) : null}
                      </div>
                    </div>
                    {!isMobilePlatform() && (
                      <div className="related-links">
                        <div className="keywords">
                          {KeyWordCard(value && value.pwa_meta && value.pwa_meta.topicskey)}
                        </div>
                        {WebTitleCard(pwaMeta)}
                      </div>
                    )}

                    {NextVideos && NextVideos.items && (
                      <div className="row">
                        <div className="col12">
                          {/* For desktop, only more videos comes (Next videos are hidden below),
                        therefore heading should only come if more videos come or platform is mobile LAN-6151 */}

                          <div className="section">
                            <div className="top_section">
                              <h3>
                                <AnchorLink href={NextVideos.wu}>
                                  <span>{NextVideos.secname}</span>
                                </AnchorLink>
                              </h3>
                            </div>
                          </div>

                          <div className="section_videos">
                            <ul className="nbt-list">
                              {/* More Videos from same section */}

                              <SectionVideos data={NextVideos.items} thumbClick={this.thumbClick} />

                              {/* code invoked for page 2,3 and so on videos */}

                              {moreVideos && moreVideos.items && moreVideos.items.length > 0 && (
                                <SectionVideos data={moreVideos.items} thumbClick={this.thumbClick} />
                              )}
                            </ul>

                            {/* {Show More Button Logic} */}

                            {moreVideos && moreVideos.nextvideos && moreVideos.nextvideos.status == "true" && (
                              <span
                                className="more_icon"
                                onClick={this.showMoreVideos.bind(this, moreVideos.nextvideos.pageno, parentId)}
                              >
                                showMore
                              </span>
                            )}
                            {/* {Show More Button Logic} */}
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </React.Fragment>
              )}
              <div className="col4">
                {/* TODO: Make Ad slot calling from config */}
                {!isMobilePlatform() && <AdCard mstype="mrec1" adtype="dfp" />}
                {!isMobilePlatform() &&
                relatedvideoData &&
                relatedvideoData.items &&
                relatedvideoData.items.length > 0 ? (
                  <div className="row">
                    <div className="col12 box-item rhs-videolist">
                      <div className="section">
                        <div className="top_section">
                          <h3>
                            {relatedvideoData.wu ? (
                              <span>
                                <AnchorLink href={relatedvideoData.wu}>{relatedvideoData.secname}</AnchorLink>
                              </span>
                            ) : (
                              <span>{relatedvideoData.secname}</span>
                            )}
                          </h3>
                        </div>
                      </div>

                      <ul className="list-horizontal video">
                        <SectionVideos data={relatedvideoData.items} thumbClick={this.thumbClick} smallIcon />
                      </ul>
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>

          {isMobilePlatform() ? null : <AdCard mstype="btf" adtype="dfp" />}

          {/* LAN -6151 Changed recommended video feed to sc_relatedvideo ( in CSR) */}
          {recommendedVideos && recommendedVideos.items && recommendedVideos.items.length > 0 && (
            <div className="row">
              <div className="col12">
                <div className="section">
                  <div className="top_section">
                    <h3>
                      {recommendedVideos.wu ? (
                        <span>
                          <AnchorLink href={recommendedVideos.wu}>{recommendedVideos.secname}</AnchorLink>
                        </span>
                      ) : (
                        <span>{recommendedVideos.secname}</span>
                      )}
                    </h3>
                  </div>
                </div>

                <div className="section_most_viewed">
                  <ul className="nbt-list">
                    <SectionVideos data={recommendedVideos.items} thumbClick={this.thumbClick} />
                  </ul>
                </div>
              </div>
            </div>
          )}
          {/* LAN -6151 Changed recommended video feed to sc_relatedvideo ( in CSR) */}
          {isMobilePlatform() && relatedvideoData && relatedvideoData.items && relatedvideoData.items.length > 0 && (
            <div className="row" data-exclude="amp">
              <div className="col12">
                <div className="section">
                  <div className="top_section">
                    <h3>
                      {relatedvideoData.wu ? (
                        <span>
                          <AnchorLink href={relatedvideoData.wu}>{relatedvideoData.secname}</AnchorLink>
                        </span>
                      ) : (
                        <span>{relatedvideoData.secname}</span>
                      )}
                    </h3>
                  </div>
                </div>

                <div className="section_most_viewed">
                  <ul className="nbt-list">
                    <SectionVideos data={relatedvideoData.items} thumbClick={this.thumbClick} />
                  </ul>
                </div>
              </div>
            </div>
          )}

          {videosFromOtherSection && videosFromOtherSection.items && (
            <div className="row">
              <div className="col12">
                <div className="other_section_video">
                  <h2>
                    {videosFromOtherSection.wu ? (
                      <AnchorLink href={videosFromOtherSection.wu}>
                        {videosFromOtherSection && videosFromOtherSection.secname}
                      </AnchorLink>
                    ) : (
                      videosFromOtherSection.secname
                    )}
                  </h2>
                  {Array.isArray(videosFromOtherSection.items) && videosFromOtherSection.items.length > 0 && (
                    <ErrorBoundary>
                      <Slider
                        type="grid"
                        size="3"
                        width={typeof isMobilePlatform == "function" && isMobilePlatform() ? "225" : "306"}
                        sliderData={videosFromOtherSection.items}
                        sliderClick={this.thumbClick}
                        margin={typeof isMobilePlatform == "function" && isMobilePlatform() ? "15" : "20"}
                        videoIntensive={true}
                        className="big"
                      />
                    </ErrorBoundary>
                  )}
                </div>
              </div>
            </div>
          )}

          {isMobilePlatform() && (
            <div className="related-links">
              <div className="keywords_wrap">{KeyWordCard(value && value.pwa_meta && value.pwa_meta.topicskey)}</div>
              {WebTitleCard(pwaMeta)}
            </div>
          )}
          {isMobilePlatform() && value && value.breadcrumb && value.breadcrumb.div && value.breadcrumb.div.ul && (
            <ErrorBoundary>
              <Breadcrumb items={value.breadcrumb.div.ul} />
            </ErrorBoundary>
          )}
        </div>
      </div>
    ) : (
      <FakeListing showImages={true} pagetype="videoshow" />
    );
  }
}

VideoShow.fetchData = function({ dispatch, params }) {
  const _this = this;
  //set pagetype
  // dispatch(setPageType("videoshow"));

  return dispatch(fetchShowDataIfNeeded(params)).then(data => {
    //fetchDesktopHeaderPromise

    if (data && data.payload && data.payload.pwa_meta) {
      const pwa_meta = data.payload.pwa_meta;
      const audetails = data.payload.audetails;
      // if (this.pageViewTriggered === false) {
      window._editorname = "";
      if (audetails) {
        window._editorname = audetails.cd;
      } else {
        window._editorname =
          typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
      }
      // Hit ga
      AnalyticsGA.pageview(window.location.pathname, {
        setEditorName: true,
      });
      this.pageViewTriggered = true;
      // }
      if (typeof _this.inVokeIbeat == "function") {
        _this.inVokeIbeat(pwa_meta);
      }

      if (pwa_meta) {
        dispatch(setPageType("videoshow", pwa_meta.site));
        updateConfig(pwa_meta, dispatch, setParentId);
        setSectionDetail(pwa_meta);
      }
    }
  });
};

function mapStateToProps(state) {
  return {
    ...state.videoshow,
    header: state.header,
  };
}

const SectionVideos = props => {
  return props.data && Array.isArray(props.data) && props.data.length > 0
    ? props.data.map((item, index) => {
        return item.type == "ctn" && (item.platform ? item.platform == process.env.PLATFORM : true) ? (
          <li className="ad-ctn">
            <AdCard mstype={item.mstype} adtype="ctn" />
          </li>
        ) : (
          <li
            key={index.toString()}
            onClick={event => props.thumbClick(event, item)}
            className={`news-card video ${props.smallIcon ? "" : "big"}`}
          >
            <VideoItem item={item} isMobilePlatform={isMobilePlatform()} />
          </li>
        );
      })
    : "";
};

export default connect(mapStateToProps)(VideoShow);

// ga('send', 'event', 'Videos', 'play', 'Fall Campaign');

// OR

// ga('send', {
//   hitType: 'event',
//   eventCategory: 'Videos',
//   eventAction: 'play',
//   eventLabel: 'Fall Campaign'
// });

// ga('send', 'pageview', location.pathname);

// OR

// ga('send', {
//   hitType: 'pageview',
//   page: location.pathname
// });
