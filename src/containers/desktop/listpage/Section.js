import React from "react";
import { connect } from "react-redux";
import { fetchWidgetDataIfNeeded } from "../../../actions/home/home";
import FakeHorizontalListCard from "../../../components/common/FakeCards/FakeHorizontalListCard";
import SectionLayoutMemo from "../home/SectionLayout";

class Section extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, dataPath } = this.props;
    if (typeof dataPath === "undefined") {
      dispatch(fetchWidgetDataIfNeeded(this.props, {}, true));
    }
  }

  render() {
    const { label, designType, alaskaData, sections, data, pageH1Value } = this.props;
    const widgetData = sections;
    // console.log("widgetData", widgetData);
    let dataObj = {};
    if (typeof data !== "undefined" && !Array.isArray(data)) {
      dataObj = data;
    } else {
      dataObj = widgetData && widgetData[label];
    }
    return dataObj ? (
      <SectionLayoutMemo
        key={label}
        datalabel={designType}
        dataObj={dataObj}
        rlvideo={widgetData && widgetData[label] && widgetData[label].rlvideo ? widgetData[label].rlvideo : null}
        alaskaData={alaskaData}
        {...this.props}
        isExperiencePage
      />
    ) : null;
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.home.isFetching,
    sections: state.home.sections,
    alaskaData: state.header.alaskaData,
  };
}
export default connect(mapStateToProps)(Section);
