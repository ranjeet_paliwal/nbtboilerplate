/* eslint-disable indent */
import React, { Component } from "react";
import { connect } from "react-redux";
import UtilityWidgets from "../../../components/common/UtilityWidgets";
import PollutionCard from "../../../components/common/PollutionCard";
import FuelCard from "../../../components/common/FuelCard";
import Wrapper from "./Wrapper";
import AdCard from "../../../components/common/AdCard";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import DataDrawer from "../../../components/desktop/DataDrawer";
import KeyWordCard from "../../../components/common/KeyWordCard";
import ArticlesListBlock from "../../../components/desktop/ArticleListBlock";
import VideoSection from "../../../components/common/VideoSection/VideoSection";
import Section from "./Section";
import { checkIsAmpPage, hasValue, isMobilePlatform, _isCSR } from "../../../utils/util";
import Breadcrumb from "../../../components/common/Breadcrumb";

const componentMap = {
  SectionHeader,
  GridSectionMaker,
  DataDrawer,
  FuelCard,
  PollutionCard,
  AdCard,
  Wrapper,
  KeyWordCard,
  Section,
  ArticlesListBlock,
  VideoSection,
  UtilityWidgets,
  Breadcrumb,
};
class ExperiencePage extends Component {
  renderChildren = (children, compProps, key) =>
    children
      ? children.map((child, ind) => {
          const newChildren = child.children ? [...child.children] : [];
          const { componentName, config, configM, platform } = child;
          let props = config;
          let pageH1Value = compProps && compProps.pageH1Value;
          if (isMobilePlatform() && configM) {
            props = { ...config, ...configM };
          }
          if (pageH1Value && props && props.pageheading) {
            props = { ...props, pageH1Value };
          }
          if (platform && (platform !== process.env.PLATFORM || platform === "none")) {
            return null;
          }
          if (newChildren && newChildren[0] && newChildren[0].config && newChildren[0].config.dataPath && hasValue(compProps,newChildren[0].config.dataPath, []).length === 0) {
            return null;
          }
          if (newChildren.length && componentName && typeof componentMap[componentName] !== "undefined") {
            return React.createElement(
              componentMap[componentName],
              {
                key: `${componentName}-${ind}-${key}`,
                ...props,
                data: child.config && child.config.dataPath ? hasValue(compProps, child.config.dataPath, []) : [],
              },
              this.renderChildren(newChildren, compProps, `${key}-${ind}`),
            );
          }
          if (newChildren.length) {
            return this.renderChildren(newChildren, compProps, `${key}-${ind}`);
          }

          if (componentName && typeof componentMap[componentName] !== "undefined") {
            const element = componentMap[componentName];
            return React.createElement(element, {
              key: `${componentName}-${ind}-${key}`,
              ...props,
              data: child.config && child.config.dataPath ? hasValue(compProps, child.config.dataPath, []) : [],
            });
          }
          return null;
        })
      : null;

  render() {
    const { error, uiConfig, router } = this.props;
    if (error) {
      return null;
    }

    // const uiConfig1 = {};
    const pathname = router && router.location && router.location.pathname;

    if (uiConfig && uiConfig.sections && uiConfig.sections.length > 0) {
      return uiConfig.sections.map((row, ind) => {
        if (typeof row.platform !== "undefined" && (row.platform !== process.env.PLATFORM || row.platform === "none")) {
          return null;
        }
        if (typeof row.onlyCSR !== "undefined" && row.onlyCSR === true && checkIsAmpPage(pathname)) {
          return null;
        }
        return (
          <Wrapper className={isMobilePlatform() ? "box-item" : "row"}>
            {this.renderChildren(row.children, this.props, ind)}
          </Wrapper>
        );
      });
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    articlelist: state.articlelist,
    alaskaData: state.header.alaskaData,
  };
}

export default connect(mapStateToProps)(ExperiencePage);
