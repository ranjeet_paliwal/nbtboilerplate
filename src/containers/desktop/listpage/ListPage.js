import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchListDataIfNeeded, fetchNextListDataIfNeeded } from "../../../actions/listpage/listpage";
import FakeDesktopDefault from "../../../components/common/FakeCards/FakeDesktopDefault";
import FakeListDesktop from "../../../components/common/FakeCards/FakeListDesktop";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  updateConfig,
  generateUrl,
  getPageType,
  extractMsidFromUrl,
  createAndCacheObserver,
  addObserverToWidgets,
} from "../../../utils/util";
import styles from "../../../components/common/css/commonComponents.scss";
import stylesSection from "../../../components/common/css/desktop/SectionWrapper.scss";
import "../../../components/common/AstroWidget/AstroWidget.scss";

// import VideoItem from "../../../components/common/VideoItem/VideoItem"; // We can move this to common components from desktop folder

import { _getStaticConfig, _isCSR } from "../../../utils/util";
import { designConfigs } from "./designConfigs";

import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module from "../../../components/lib/ads/index";
import AnchorLink from "../../../components/common/AnchorLink";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import Slider from "../../../components/desktop/Slider/index";
// import { SuperhitWidget } from "../../../components/common/SuperhitWidget";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";

import "../../../components/common/css/desktop/VideoPhotoListCard.scss";
import AdCard from "../../../components/common/AdCard";
import VideoSection from "../../../components/common/VideoSection/VideoSection";
// import WebTitleCard from "../../../components/common/WebTitleCard";
import KeyWordCard from "../../../components/common/KeyWordCard";
import { setParentId, setPageType } from "../../../actions/config/config";
import YSubscribeCard from "../../../components/common/YSubscribeCard";
import GridCardMaker from "../../../components/common/ListingCards/GridCardMaker";
import fetch from "../../../utils/fetch/fetch";
import AmazonWidget from "../../../components/common/AmazonWidget/AmazonWidget";
import ExperiencePage from "./ExperiencePage";

import { fireGRXEvent } from "../../../components/lib/analytics/src/ga";
import SectionLayoutMemo from "../home/SectionLayout";
const siteConfig = _getStaticConfig();
// import PageMeta from '../../components/common/PageMeta'; //For Page SEO/Head Part

export class ListPage extends Component {
  constructor(props) {
    super(props);
    if (typeof window !== "undefined" && "scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    this.state = {
      curpg: 1,
      totalPages: 0,
      _scrollEventBind: false,
      relatedvideoData: null,
      relatedPhotoData: null,
      latestVidMsid: null,
      readMore: false,
    };
    this.config = {
      isFetchingNext: false,
      componentType: "",
      isExperiencePage: false,
    };
    this.scrollHandler = false;
    this.pagetype = "";
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({ readMore: !this.state.readMore });
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    const { dispatch, value, router, route } = this.props;
    const { query } = this.props.location;
    let { params } = this.props;
    let pagetype = "";
    if (typeof params.msid === "undefined" && window.meta && window.meta.longurl) {
      params.msid = extractMsidFromUrl(window.meta.longurl);
      pagetype = this.pagetype = getPageType(window.meta.longurl);
    }
    const parentId = (value[0] && value[0].pwa_meta && value[0].pwa_meta.parentidnew) || "";
    const subsec1 = (value[0] && value[0].pwa_meta && value[0].pwa_meta.subsec1) || "";

    if (params && params.msid == undefined) {
      params = this.setParams(params, router);
    }

    const observerCallback = (entries, self) => {
      entries.forEach(entry => {
        if (entry && entry.isIntersecting) {
          const eventData = {};
          const scrollId = entry.target.getAttribute("data-scroll-id");

          if (scrollId) {
            eventData.widget_id = scrollId;
          }

          fireGRXEvent("track", "scroll_depth", eventData);

          // Unobserve this
          self.unobserve(entry.target);
        }
      });
    };

    const { app } = this.props;

    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].listpage &&
        app.scrolldepth[process.env.SITE].listpage[process.env.PLATFORM] === "true") ||
      false;

    createAndCacheObserver(
      this,
      {
        root: null,
        rootMargin: "0px",
        threshold: 0.2, // when 20% in view
      },
      observerCallback,
      { shouldCreate: shouldCreateScrollDepth },
    );

    this.getLatestVideoAndPhoto();
    //   const relatedvideoApi = `${process.env.API_BASEPOINT}/sc_relatedvideo.cms?msid=${parentId}&tag=ibeatmostread&days=10&host=${siteConfig.webdomain}&perpage=6&feedtype=sjson`;
    // fetch(relatedvideoApi)
    //   .then(data => {
    //     this.setState({
    //       relatedvideoData: data,
    //     });
    //   })
    //   .catch({});

    // set section & subsection for first time
    // if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    ListPage.fetchData({ dispatch, query, params, history, router, pagetype }).then(data => {
      // attach scroll only when sections is not there in feed
      if (typeof this.props.value[0] !== "undefined" && !this.props.value[0].sections) {
        this.scrollBind();
      }

      addObserverToWidgets(
        this,
        { '[data-scroll-id="trending-topics"]': true, '[data-scroll-id="most-read"]': true },
        { shouldCreate: shouldCreateScrollDepth },
      );
      const { pwa_meta, pg } = this.props.value[0] ? this.props.value[0] : {};
      if (pg && pg.tp) {
        this.state.totalPages = pg.tp;
      }
      if (pwa_meta && typeof pwa_meta === "object") {
        if (pwa_meta.AdServingRules != "Turnoff") {
          // set section and subsection in window
          Ads_module.setSectionDetail(pwa_meta);
          // fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        }
        dispatch(setPageType("articlelist", pwa_meta.site));
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { params } = this.props;

    if (
      params.msid != null &&
      this.state.latestVidMsid != null &&
      params.msid != this.state.latestVidMsid &&
      prevProps &&
      prevProps.msid != this.state.latestVidMsid
    ) {
      this.getLatestVideoAndPhoto();
    }

    // const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
    // if (pwa_meta && typeof pwa_meta == "object") {
    //   //set section and subsection in window
    //   Ads_module.setSectionDetail(pwa_meta);
    //   //fire ibeat
    //   pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
    //   dispatch(setParentId(parentId, subsec1));
    // }
  }

  setParams(params, router) {
    // console.log("params, router------", params, router);
    const pathname = router.location.pathname;
    if (pathname.indexOf("/tech/reviews") > -1) {
      params.msid = siteConfig.pages.techreview;
    }
    return params;
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window !== "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    const _this = this;
    if (this.state._scrollEventBind == false) return;

    const body = document.body;
    const html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = (document.scrollingElement || document.documentElement).scrollTop;
    }
    const footerHeight = document.querySelector("#footerContainer,footer").offsetHeight;
    docHeight -= footerHeight;
    const curpg = parseInt(this.props.value[0].pg.cp) + 1;
    if (scrollValue + 1000 > docHeight && curpg <= this.state.totalPages && _this.isExperiencePage === false) {
      const { dispatch, params, isFetching, router } = this.props;
      const { query } = this.props.location;
      let pagetype = this.pagetype;
      if (typeof params.msid === "undefined" && window.meta && window.meta.longurl && window.meta.longurl != "") {
        params.msid = extractMsidFromUrl(window.meta.longurl);
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      }
      if (isFetching == false) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        ListPage.fetchNextListData({ dispatch, query, params, router, pagetype }).then(data => {
          const _data = data ? data.payload : {};
          // silentRedirect(_this.generateListingLink());
          _this.config.isFetchingNext = !_this.config.isFetchingNext;

          Ads_module.refreshAds(["fbn"]); // refresh fbn ads when next list added
          // fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";

          let url;
          url = location.origin + _this.generateListingLink();

          if (url != location.href || _data.pwa_meta.title != document.title) {
            // todo stop history push for webview
            window.history.replaceState({}, _data.pwa_meta.title, url);
            // This is used to fire GRX calls. Since query is already there, no need to pass it here (curpg=3)etc
            AnalyticsGA.pageview(window.location.pathname);
            document.title = _data.pwa_meta.title;
          }
        });
      }
    }
  }

  getLatestVideoAndPhoto() {
    // video data  Api call for AL page
    try {
      const { params } = this.props;
      const { app } = this.props;

      const shouldCreateScrollDepth =
        (app &&
          app.scrolldepth &&
          app.scrolldepth[process.env.SITE] &&
          app.scrolldepth[process.env.SITE].listpage &&
          app.scrolldepth[process.env.SITE].listpage[process.env.PLATFORM] === "true") ||
        false;

      const photVidmsid = params.msid == "reviews" ? siteConfig.pages.techreview : params.msid;
      const relatedvideoApi = `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${photVidmsid}&tag=video&perpage=6&feedtype=sjson`;
      this.setState({ latestVidMsid: photVidmsid });
      fetch(relatedvideoApi)
        .then(data => {
          this.setState(
            {
              relatedvideoData: data,
            },
            () => {
              // Adds observer to widgets
              addObserverToWidgets(
                this,
                { '[data-scroll-id="latest-videos"]': true },
                { shouldCreate: shouldCreateScrollDepth },
              );
            },
          );
        })
        .catch({});

      const relatedPhotoApi = `${process.env.API_BASEPOINT}/sc_relatedphoto.cms?tag=latestphoto&feedtype=sjson&msid=${photVidmsid}&perpage=9`;
      fetch(relatedPhotoApi)
        .then(data => {
          this.setState(
            {
              relatedPhotoData: data,
            },
            () => {
              // Adds observer to widgets
              addObserverToWidgets(
                this,
                { '[data-scroll-id="latest-photos"]': true },
                { shouldCreate: shouldCreateScrollDepth },
              );
            },
          );
        })
        .catch({});
    } catch (ex) {
      console.log("ex", ex);
    }
  }

  generateListingLink(type) {
    const props = this.props;

    if (props.location && props.location.pathname && props.value[0] && props.value[0].pg && props.value[0].pg.cp) {
      const curpg = type == "moreButton" ? parseInt(props.value[0].pg.cp) + 1 : parseInt(props.value[0].pg.cp);
      if (props.location.pathname.indexOf("curpg") > -1) {
        const reExp = /curpg=\\d+/;
        return props.location.pathname.replace(reExp, `curpg=${curpg}`);
      }
      return props.location.pathname + ((props.location.pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
    }
  }

  /**
   * Return only items whose type matches with the parameter type value
   * @param {*} arrItems
   * @param {*} type
   */
  getTypeItems(arrItems, type, len) {
    let arrData;
    arrData = arrItems && arrItems.length > 0 && arrItems.filter(item => type.indexOf(item.tn) > -1);
    arrData = arrData && arrData.length > 0 && arrData.slice(0, len);
    return arrData;
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.msid != nextProps.params.msid ||
      this.props.location.pathname != nextProps.location.pathname
    ) {
      const { dispatch, params, router, route, history } = nextProps;
      const { query } = nextProps.location;
      const { app } = this.props;

      const shouldCreateScrollDepth =
        (app &&
          app.scrolldepth &&
          app.scrolldepth[process.env.SITE] &&
          app.scrolldepth[process.env.SITE].listpage &&
          app.scrolldepth[process.env.SITE].listpage[process.env.PLATFORM] === "true") ||
        false;
      let pagetype = "";
      if (route && route.path && route.path == "/tech/reviews.cms") {
        params.msid = siteConfig.pages.techreview;
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      } else if (typeof params.msid === "undefined" && window.meta && window.meta.longurl) {
        params.msid = extractMsidFromUrl(window.meta.longurl);
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      } else if (typeof params.msid !== "undefined" && nextProps.location && nextProps.location.pathname) {
        window.meta = {};
        window.meta.longurl = nextProps.location.pathname;
      }
      try {
        ListPage.fetchData({ dispatch, query, params, history, router, pagetype }).then(data => {
          const _data = data && data.payload ? data.payload[0] : {};
          addObserverToWidgets(
            this,
            {
              '[data-scroll-id="most-read"]': true,
              '[data-scroll-id="trending-topics"]': true,
            },
            { shouldCreate: shouldCreateScrollDepth },
          );
          const { pg } = _data;
          if (pg && pg.tp) {
            this.state.totalPages = pg.tp;
          }
          if (data && data.payload[0] && !data.payload[0].sections) {
            this.scrollBind();
          } else {
            this.scrollUnbind();
          }
          // fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
          if (
            !(_data && _data.pwa_meta && _data.pwa_meta.AdServingRules && _data.pwa_meta.AdServingRules == "Turnoff")
          ) {
            Ads_module.setSectionDetail(_data.pwa_meta);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    delete window.meta;
    const _this = this;
    if (typeof window !== "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      // Reset section window object
      Ads_module.setSectionDetail();

      // let navigations = document.querySelector("#paginationHomePage").children;
      // if(navigations.length>0){
      //   for(let nav of navigations){
      //     try{
      //       nav.removeEventListener('click',this.handleNavigationClick, true);
      //     }catch(ex){}
      //     nav.classList.remove("active");
      //   }
      // }
      // document.querySelector(".mymenu_slide") ? (document.querySelector(".mymenu_slide").scrollLeft = 0) : '';
    }
  }

  getSectionItems(sections, lbl) {
    for (const section of sections) {
      if (section.name == lbl) {
        return section;
      }
    }
  }

  /* Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({
    dataObj,
    datalabel,
    configlabel,
    gridclasses,
    override,
    imgsize,
    sectionId,
    weblink,
    isSectionHead,
    compType,
    sectionHeading,
    noSeo,
  }) {
    const { header } = this.props;
    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference

    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    let sectionhead;
    sectionhead = sectionHeading || dataObj ? dataObj.secname : undefined;
    sectionhead = isSectionHead == false ? undefined : sectionhead;
    override = override || {};

    try {
      if (dataObj && dataObj.items) {
        if (!Array.isArray(dataObj.items)) {
          const arrObj = [];
          arrObj.push(dataObj.items);
          dataObj.items = arrObj.slice();
        }
      }

      return dataObj && dataObj.items && dataObj.items.length > 0 ? (
        <div className={`row ${gridclasses}`}>
          {sectionhead && (
            <SectionHeader
              sectionhead={sectionhead}
              weblink={dataObj && (dataObj.override || dataObj.wu)}
              sectionTag={dataObj && dataObj.tag ? dataObj.tag : ""}
              compType={compType}
            />
          )}

          <GridSectionMaker
            type={designConfigs[configlabel]}
            data={dataObj.items}
            override={override}
            compType={compType}
            noSeo={noSeo}
          />
        </div>
      ) : // <FakeDesktopDefault />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const { isFetching, value, params, header, dispatch, error, astroListingWidget, router } = this.props;

    const { sections, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";

    const sectionsCopy = value && value[0] && value[0].sections ? JSON.parse(JSON.stringify(value[0].sections)) : "";

    const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
    const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
    const pageH1Value = `${pageheading} ${pageheadingSlug}`;
    const parentId = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.subsec1) || undefined;

    this.config.componentType = parentSection ? parentSection.tn : "";
    this.config.componentType = this.config.componentType === "movieshowlist" ? "movielist" : this.config.componentType;
    const compType = this.config.componentType;
    const tech =
      router && router.location && router.location.pathname && router.location.pathname.indexOf("/tech") > -1
        ? "gn"
        : "";

    const latestSection = sections && this.getSectionItems(sections, "latest-items");

    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : "";

    const trendingItems =
      parentSection.recommended && parentSection.recommended.trending && parentSection.recommended.trending.items
        ? // &&
          // parentSection.recommended.trending.items.length > 0
          parentSection.recommended.trending.items
        : undefined;
    const nextItems =
      parentSection.recommended &&
      parentSection.recommended.nextitems &&
      parentSection.recommended.nextitems.items &&
      parentSection.recommended.nextitems.items.length > 0
        ? parentSection.recommended.nextitems.items
        : undefined;

    const trendingNowItems =
      parentSection.recommended &&
      parentSection.recommended.trendingnow &&
      parentSection.recommended.trendingnow[0] &&
      parentSection.recommended.trendingnow[0].items
        ? // &&
          // parentSection.recommended.trendingnow[0].items.length > 0
          parentSection.recommended.trendingnow[0].items
        : undefined;

    const trendingtopics =
      parentSection.recommended &&
      parentSection.recommended.trendingtopics &&
      parentSection.recommended.trendingtopics[0] &&
      parentSection.recommended.trendingtopics[0].items
        ? // &&
          // parentSection.recommended.trendingtopics[0].items.length > 0
          parentSection.recommended.trendingtopics[0].items
        : undefined;
    // video data  for last lavel
    const { relatedvideoData } = this.state;
    // console.log("videodata", relatedvideoData);
    if (compType == "articlelist" && this.props.value[0] && this.props.value[0].ui_config) {
      this.isExperiencePage = true;
      return (
        <React.Fragment>
          {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
          <div
            className="section-wrapper body-content experiencepage"
            {...SeoSchema({ pagetype: compType }).attr().itemList}
          >
            {parentSection.items && parentSection.items.length > 0
              ? SeoSchema().metaTag({
                  name: "numberOfItems",
                  content: parentSection.items.filter(
                    item => !!(item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")),
                  ).length,
                })
              : null}
            {parentSection.pwa_meta
              ? SeoSchema().metaTag({
                  name: "url",
                  content: parentSection.pwa_meta.canonical,
                })
              : null}
            <ExperiencePage
              uiConfig={this.props.value[0].ui_config}
              pageH1Value={pageH1Value}
              router={router}
              parentSecId={parentId}
            />
          </div>
        </React.Fragment>
      );
    }
    this.isExperiencePage = false;

    return this.props.value[0] && (!isFetching || this.config.isFetchingNext) ? (
      <div
        className={`${compType == "articlelist" || compType == "movielist" ? "section-wrapper" : ""} ${
          sections != undefined ? "L1" : "L2"
        } ${compType} ${tech} body-content`}
        {...SeoSchema({ pagetype: compType }).attr().itemList}
      >
        {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
        {parentSection.items && parentSection.items.length > 0
          ? SeoSchema().metaTag({
              name: "numberOfItems",
              content: parentSection.items.filter(
                item => !!(item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")),
              ).length,
            })
          : null}
        {parentSection.pwa_meta
          ? SeoSchema().metaTag({
              name: "url",
              content: parentSection.pwa_meta.canonical,
            })
          : null}
        {/* Breadcrumb  */}
        {parentSection &&
        typeof parentSection.breadcrumb !== "undefined" &&
        parentSection.breadcrumb.div &&
        parentSection.breadcrumb.div.ul ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
        {/* Business section iframe */}
        {parentSection && parentSection.subsec1 === siteConfig.pages.business ? (
          <div className="etframe">
            <iframe
              src={siteConfig.marketIframe}
              height="72"
              width="632"
              border="0"
              marginWidth="0"
              marginHeight="0"
              hspace="0"
              vspace="0"
              frameBorder="0"
              scrolling="no"
              title="business Market"
              allowTransparency="true"
            />
          </div>
        ) : null}
        {/* For articlelist L1/L2/L3 */}
        <div
          className={
            parentSection && parentSection.pdwidget && parentSection.pdwidget.pprice
              ? "only-heading withPetrolWidget"
              : "only-heading"
          }
        >
          <SectionHeader sectionhead={pageH1Value} parentSecId={parentId} headingTag="h1" />
          {compType == "videolist" ? (
            <div className="con_social">
              <YSubscribeCard />
            </div>
          ) : null}
          {/* petrol diesel widget for NBT */}
          <ErrorBoundary>
            <Petrolwidget data={parentSection} />
          </ErrorBoundary>
        </div>
        {/* To show Youtube icon */}
        {/* ATF text read more toggle */}
        {parentSection && parentSection.pwa_meta && parentSection.pwa_meta.atf ? (
          <div className="col12">
            <div className="top_atf">
              <div className={`caption text_ellipsis ${!this.state.readMore ? "" : "more"}`}>
                {parentSection.pwa_meta.atf}
              </div>
              <span onClick={this.toggle} className="rdmore">
                {!this.state.readMore ? "..." + siteConfig.locale.readmore : ""}
              </span>
            </div>
          </div>
        ) : null}
        {/* For REndering top section of photo/video list */}
        {
          <React.Fragment>
            {compType != "articlelist" && parentSection && parentSection.items && parentSection.secname && sections ? (
              <React.Fragment>
                {/* To Render Top slider for photolist and videolist */}
                {/* <div className="only-heading">
                  <SectionHeader
                    sectionhead={parentSection.pwa_meta && parentSection.pwa_meta.pageheading}
                    headingTag="h1"
                  />
                </div> */}
                <div
                  className={
                    compType == "photolist" &&
                    parentSection.seolocation &&
                    parentSection.seolocation.includes("web-stories")
                      ? "row webstorieslider"
                      : compType == "photolist"
                      ? "row top_img_slider"
                      : "row top_wdt_slider"
                  }
                >
                  {/* <TopHeading parentSection={parentSection} /> */}

                  <TopSection parentSection={parentSection} compType={compType} />
                </div>
              </React.Fragment>
            ) : null}

            {/* To Display PL articlelist  */}
            {compType == "articlelist" ? (
              parentSection ? (
                parentSection.items && parentSection.items.length > 0 ? (
                  <Articlelisting
                    compType={compType}
                    sections={sections}
                    parentSection={parentSection}
                    createSectionLayout={this.createSectionLayout.bind(this)}
                    astroListingWidget={astroListingWidget}
                  />
                ) : null
              ) : (
                <FakeDesktopDefault />
              )
            ) : null}
            {/* video widget added for AL page */}
            {compType == "articlelist" && parentSection && parentSection.vdata ? (
              <div className="col12 pd0">
                <div className="slidercomp">
                  <SectionLayoutMemo
                    key="1_slider"
                    datalabel="slider"
                    data={parentSection.vdata}
                    sliderObj={{ size: "4", width: "221" }}
                  />
                </div>
              </div>
            ) : null}
            {/* To Display trending section of VL/PL L1 - TO COMMENT */}
            {compType != "articlelist" ? (
              <div className="row">
                {sections != undefined &&
                parentSection &&
                parentSection.recommended &&
                parentSection.recommended.trending &&
                parentSection.recommended.trending.items &&
                Array.isArray(parentSection.recommended.trending.items) ? (
                  <div className={compType == "videolist" && sections != undefined ? "col4 listing" : "col12 listing"}>
                    <div
                      className={compType == "videolist" && sections != undefined ? "trending" : "highlight_section"}
                    >
                      {
                        <TrendingSection
                          compType={compType}
                          sections={sections}
                          parentSection={parentSection}
                          createSectionLayout={this.createSectionLayout.bind(this)}
                          getTypeItems={this.getTypeItems}
                          noSeo
                        />
                      }
                    </div>
                  </div>
                ) : null}
                {/* To Display Latest section of video L1 */}
                <LatestSection
                  parentSection={parentSection}
                  latestSection={latestSection}
                  createSectionLayout={this.createSectionLayout.bind(this)}
                  sections={sections}
                  headerCopy={headerCopy}
                  compType={compType}
                />

                {/* To Display L2 page of Video/Photo list */}
                {parentSection && parentSection.seolocation && parentSection.seolocation.includes("web-stories") ? (
                  <ListingPageWebStories
                    parentSection={parentSection}
                    createSectionLayout={this.createSectionLayout.bind(this)}
                    sections={sections}
                    compType={compType}
                  />
                ) : (
                  <ListingPage
                    parentSection={parentSection}
                    createSectionLayout={this.createSectionLayout.bind(this)}
                    sections={sections}
                    compType={compType}
                  />
                )}

                {/* Other section widget for Listing pages VL/PL L2 */}
                {(compType == "videolist" || compType == "photolist") && sections == undefined && nextItems ? (
                  <RecommendedSec recommendedObj={parentSection.recommended.nextitems} compType={compType} />
                ) : null}

                {/* Trending widget for PL L2  -> To comment */}
                {sections == undefined && compType != "articlelist" && compType != "movielist" && trendingItems ? (
                  <RecommendedSec
                    recommendedObj={parentSection.recommended.trending}
                    compType={compType}
                    istrending="true"
                  />
                ) : null}
              </div>
            ) : null}

            {/* {isFetching ? <FakeDesktopDefault showImages={true} /> : null} */}
          </React.Fragment>
        }

        {((compType == "articlelist" || compType == "movielist") && sections == undefined) ||
        ((compType == "articlelist" || compType == "movielist") &&
          parentSection &&
          parentSection.items &&
          parentSection.items.length > 0) ? (
          <div className="row wdt_photo_video">
            {this.state.relatedvideoData && Array.isArray(this.state.relatedvideoData.items) ? (
              <ErrorBoundary>
                <div className="col4" data-row-id="latest-videos" data-scroll-id="latest-videos">
                  <AnchorLink href={this.state.relatedvideoData && this.state.relatedvideoData.wu}>
                    <span>{siteConfig.locale.latest_video}</span>
                  </AnchorLink>

                  <div className="wdt_video slider-horizontalView">
                    <Slider
                      margin="25"
                      size="2"
                      sliderData={this.state.relatedvideoData.items.map(item => {
                        item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=latest videos`;
                        return item;
                      })}
                      width="130"
                      type="grid"
                      sliderClass="videosection"
                      istrending="true"
                      compType={compType}
                      noSeo
                    />
                  </div>
                </div>
              </ErrorBoundary>
            ) : null}
            {this.state.relatedPhotoData && Array.isArray(this.state.relatedPhotoData.items) ? (
              <ErrorBoundary>
                <div className="col4" data-row-id="latest-photos" data-scroll-id="latest-photos">
                  <AnchorLink href={this.state.relatedPhotoData && this.state.relatedPhotoData.wu}>
                    <span>{siteConfig.locale.latest_photo}</span>
                  </AnchorLink>
                  <div className="wdt_video slider-horizontalView">
                    <Slider
                      margin="25"
                      size="2"
                      sliderData={this.state.relatedPhotoData.items.map(item => {
                        item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=latest photos`;
                        return item;
                      })}
                      width="130"
                      type="grid"
                      sliderClass="videosection"
                      istrending="true"
                      compType={compType}
                      imgsize="smallwidethumb"
                      noSeo
                    />
                  </div>
                </div>
              </ErrorBoundary>
            ) : null}
            {parentSection &&
            parentSection.recommended &&
            parentSection.recommended.trending &&
            parentSection.recommended.trending.items &&
            Array.isArray(parentSection.recommended.trending.items) ? (
              <ErrorBoundary>
                <div className="col4" data-row-id="most-read" data-scroll-id="most-read">
                  <span>{siteConfig.locale.most_read}</span>

                  <div className="wdt_video slider-horizontalView">
                    <Slider
                      margin="25"
                      size="2"
                      sliderData={parentSection.recommended.trending.items.map(item => {
                        item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=most read`;
                        return item;
                      })}
                      width="130"
                      type="grid"
                      sliderClass="videosection"
                      istrending="true"
                      compType={compType}
                      noSeo
                    />
                  </div>
                </div>
              </ErrorBoundary>
            ) : null}
          </div>
        ) : null}
        {/* Below code is used to render sections for Articlelist L1/L2 */}
        {compType == "articlelist" && sectionsCopy && sectionsCopy instanceof Array
          ? sectionsCopy.map(item => (
              <ArticlelistSections item={item} createSectionLayout={this.createSectionLayout.bind(this)} />
            ))
          : null}
        {/* NBT Business section ET iframs */}
        {/* {process.env.SITE === "nbt" && parentSection && parentSection.subsec1 === siteConfig.pages.business ? (
          <ETiframs />
        ) : null} */}
        {/* SectionalData is used to render sections of photolist page L1 */}
        {compType == "photolist" && sections && sections instanceof Array ? (
          <SectionalData compType={compType} sections={sections} getTypeItems={this.getTypeItems} />
        ) : null}

        <AmazonWidget item={value && value[0]} type="sponsored" router={router} techPageOnly />

        {/* To Load perpetual data for last level pages */}
        {value && value[0] && value[0].seolocation && value[0].seolocation.includes("web-stories") ? (
          <PerpetualDatawebStories
            value={value}
            createSectionLayout={this.createSectionLayout.bind(this)}
            compType={compType}
          />
        ) : (
          <PerpetualData value={value} createSectionLayout={this.createSectionLayout.bind(this)} compType={compType} />
        )}

        {/* BTF content */}
        {parentSection && parentSection.pwa_meta && parentSection.pwa_meta.syn ? (
          <ErrorBoundary>
            <div className="btf_con" dangerouslySetInnerHTML={{ __html: parentSection.pwa_meta.syn || "" }}></div>
          </ErrorBoundary>
        ) : null}

        {/* To show fake listing until data is data is rendered on screen */}
        {isFetching ? <FakeListDesktop showImages /> : null}
        {/* To show more button on listing page L2 */}
        {parentSection &&
        parentSection.pg &&
        !sections &&
        parseInt(parentSection.pg.cp) < parseInt(parentSection.pg.tp) ? (
          <AnchorLink hrefData={{ override: this.generateListingLink("moreButton") }} className=" more-btn">
            {/* {parentSection.secname + siteConfig.locale.read_more_listing} */}
            {siteConfig.locale.read_more_listing}
          </AnchorLink>
        ) : (
          ""
        )}
      </div>
    ) : (
      <FakeDesktopDefault pagetype={compType} />
    );
  }
}

ListPage.fetchData = function({ dispatch, query, params, history, router, pagetype }) {
  //  dispatch(setPageType("articlelist"));
  // return dispatch(fetchListDataIfNeeded(params, query, router, pagetype));
  return dispatch(fetchListDataIfNeeded(params, query, router, pagetype)).then(data => {
    if (data && data.payload && data.payload[0] && data.payload[0].pwa_meta) {
      const pwaMeta = data.payload[0].pwa_meta;
      dispatch(setPageType("articlelist", pwaMeta.site));
      updateConfig(pwaMeta, dispatch, setParentId);
    }
    return data;
  });
};

ListPage.fetchNextListData = function({ dispatch, params, query, router, pagetype }) {
  return dispatch(fetchNextListDataIfNeeded(params, query, router, pagetype));
};

function mapStateToProps(state) {
  return {
    ...state.articlelist,
    edittrending: state.app.trending,
    header: state.header,
    gadgetlist: state.gadgetlist,
    astroListingWidget: state && state.articlelist && state.articlelist.astroListingWidget,
    app: state.app,
  };
}

const TopSection = props => {
  const { parentSection, compType } = props;

  return (
    <React.Fragment>
      {parentSection &&
      parentSection.items &&
      parentSection.seolocation &&
      parentSection.seolocation.includes("web-stories") ? (
        <div className="col8">
          <Slider
            margin="15"
            size="4"
            sliderData={parentSection.items}
            width="150"
            type="webstories"
            compType={compType}
            sliderClass="webstories"
            parentMsid={parentSection.id}
          />
        </div>
      ) : parentSection && parentSection.items ? (
        <div className={compType == "videolist" ? "col8" : "col12"}>
          <Slider
            margin={compType == "photolist" ? "5" : "30"}
            size={compType == "photolist" ? "1" : "2"}
            sliderData={parentSection.items}
            width={compType == "photolist" ? "700" : "300"}
            type="grid"
            sliderClass="grid_slider"
            slideIndicator
            compType={compType}
            imgsize={compType == "photolist" ? "gallerythumb" : undefined}
          />
        </div>
      ) : null}
      <div className="col4">
        <AdCard mstype="mrec1" adtype="dfp" />
      </div>
    </React.Fragment>
  );
};

const LatestSection = props => {
  const { parentSection, latestSection, createSectionLayout, sections, compType } = props;
  const { headerCopy } = props;
  // const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference
  const alaskaData = headerCopy && headerCopy.alaskaData;
  const isTrendingExists =
    parentSection &&
    parentSection.recommended &&
    parentSection.recommended.trending &&
    parentSection.recommended.trending.items &&
    Array.isArray(parentSection.recommended.trending.items);

  return compType == "videolist" && sections != undefined ? (
    <React.Fragment>
      <div className={isTrendingExists ? "col8 listing videolist" : "col12" + " listing videolist"}>
        {parentSection && parentSection.items ? (
          <ErrorBoundary>
            {createSectionLayout({
              dataObj: latestSection,
              compType,
              configlabel: isTrendingExists == true ? "videoLatestSectionFirst" : "videoLatestFirst",
            })}
            <div className="moreVideoSections">
              <SectionHeader data={alaskaData} sectionId={parentSection.id} />
            </div>
            {createSectionLayout({
              dataObj: latestSection,
              compType,
              configlabel: isTrendingExists ? "videoLatestSectionSecond" : "videoLatestSecond",
              isSectionHead: false,
            })}
          </ErrorBoundary>
        ) : null}
      </div>
      {
        <div className="btf-placeholder">
          <AdCard mstype="btf" adtype="dfp" />
        </div>
      }
    </React.Fragment>
  ) : null;
};

{
  /* To Display L2 section of Video/Photo list 
   Movie List is rendered through below written code
   */
}
const ListingPage = props => {
  const { parentSection, createSectionLayout, sections, compType } = props;

  return (
    <div
      className={
        compType == "videolist"
          ? "col12 listing videolist"
          : compType == "photolist"
          ? "col12 listing photolist"
          : "col12"
      }
    >
      {parentSection && parentSection.items && sections == undefined ? (
        <ErrorBoundary>
          {/* {parentSection && (
            <div className="only-heading">
              <SectionHeader
                sectionhead={parentSection.pwa_meta && parentSection.pwa_meta.pageheading}
                headingTag="h1"
              />
            </div>
          )} */}
          <div className={compType == "videolist" ? "top_l2_wdt row" : "row mr0"}>
            <div className={compType == "movielist" ? "col6" : "col8"}>
              <div>
                {createSectionLayout({
                  dataObj: parentSection,
                  compType,
                  configlabel: compType == "movielist" ? "movielistHeader" : "photoVideoHeaderL2",
                  // gridclasses: compType == "videolist" ? "top_l2_wdt" : "",
                  isSectionHead: false,
                  override: {
                    imgsize:
                      compType == "videolist"
                        ? "largewidethumb"
                        : compType == "movielist"
                        ? "posterthumb"
                        : "smallthumb",
                  },
                })}
              </div>
            </div>
            <div className={compType == "movielist" ? "col6 ad-movielist" : "col4"}>
              {compType != "articlelist" ? <AdCard mstype="mrec1" adtype="dfp" /> : null}
            </div>
          </div>

          {compType == "photolist"
            ? createSectionLayout({
                dataObj: parentSection,
                configlabel: "PLmiddleL2",
                isSectionHead: false,
                override: {
                  imgsize: "smallthumb",
                },
              })
            : null}

          {createSectionLayout({
            dataObj: parentSection,
            compType,
            configlabel:
              compType == "videolist" ? "videoListL2" : compType == "movielist" ? "movielistL2" : "photoListL2",
            isSectionHead: false,
            override: {
              noOfElements: compType == "movielist" ? 20 : undefined,
              imgsize:
                compType == "videolist" ? "largewidethumb" : compType == "movielist" ? "posterthumb" : "smallthumb",
            },
          })}

          {
            <div className="btf-placeholder">
              <AdCard mstype="btf" adtype="dfp" />
            </div>
          }
        </ErrorBoundary>
      ) : null}
    </div>
  );
};

const ListingPageWebStories = props => {
  const { parentSection, createSectionLayout, sections, compType } = props;
  return (
    <div className="col12 listing webstories">
      {parentSection && parentSection.items && sections == undefined ? (
        <ErrorBoundary>
          <div className="row mr0">
            <div className="col8">
              {createSectionLayout({
                dataObj: parentSection,
                compType,
                configlabel: "webstoriesList1",
                isSectionHead: false,
                override: {
                  imgsize: "webstoriesthumb",
                },
              })}
            </div>
            <div className="col4">
              <AdCard mstype="mrec1" adtype="dfp" />
            </div>
          </div>
          {createSectionLayout({
            dataObj: parentSection,
            compType,
            configlabel: "webstoriesList2",
            isSectionHead: false,
            override: {
              imgsize: "webstoriesthumb",
            },
          })}
          {
            <div className="btf-placeholder">
              <AdCard mstype="btf" adtype="dfp" />
            </div>
          }
        </ErrorBoundary>
      ) : null}
    </div>
  );
};

const RecommendedSec = props => {
  const { recommendedObj, compType, istrending } = props;

  return (
    <div className="col12">
      {recommendedObj && recommendedObj.items && (
        <div className="highlight_section">
          <div className="section">
            <h3>
              <span>{recommendedObj.secname}</span>
            </h3>
          </div>
          <Slider
            margin={compType == "photolist" ? "5" : "30"}
            size={compType == "photolist" ? "4" : "3"}
            sliderData={recommendedObj.items}
            width={compType == "photolist" ? "236" : "300"}
            type="grid"
            compType={compType}
            sliderClass="grid_slider"
            istrending={istrending ? true : undefined}
          />
        </div>
      )}
    </div>
  );
};

const PerpetualData = props => {
  const { value, createSectionLayout, compType } = props;

  return value.length > 1
    ? value.map((section, index) => {
        if (index > 0) {
          return (
            <div key={`perpSection${index}`}>
              {createSectionLayout({
                dataObj: section,
                compType,
                configlabel:
                  compType == "videolist"
                    ? "videoListL2"
                    : compType == "articlelist"
                    ? "default"
                    : compType == "movielist"
                    ? "movielistL2"
                    : "photoListL2",
                gridclasses:
                  compType == "videolist"
                    ? "listing videolist"
                    : compType == "photolist"
                    ? "listing photolist"
                    : compType == "movielist"
                    ? "movielist"
                    : "default",
                override: {
                  imgsize:
                    compType == "videolist" ? "largewidethumb" : compType == "movielist" ? "posterthumb" : "smallthumb",
                  startIndex: 0,
                },
              })}
            </div>
          );
        }
        return null;
      })
    : null;
};
const PerpetualDatawebStories = props => {
  const { value, createSectionLayout, compType } = props;
  return value.length > 1
    ? value.map((section, index) => {
        if (index > 0) {
          return (
            <div className="col12 listing webstories">
              <ErrorBoundary>
                {createSectionLayout({
                  dataObj: section,
                  compType,
                  configlabel: "webstoriesList3",
                  isSectionHead: false,
                  override: {
                    imgsize: "webstoriesthumb",
                  },
                })}
              </ErrorBoundary>
            </div>
          );
        }
        return null;
      })
    : null;
};

/**
 *  To render sections of photolist page L1
 *  */

const SectionalData = props => {
  const { compType, sections, getTypeItems } = props;
  return (
    <React.Fragment>
      {compType == "photolist"
        ? sections
          ? sections.map((section, index) =>
              section && section.items ? (
                <ErrorBoundary key={section.id}>
                  <div className={compType == "videolist" ? "row listing videolist" : "row listing photolist"}>
                    <div className="col12">
                      <SectionHeader sectionhead={section.secname} weblink={section.wu} compType={compType} />
                      <Slider
                        margin={section.seolocation && section.seolocation.includes("web-stories") ? "15" : "5"}
                        size={section.seolocation && section.seolocation.includes("web-stories") ? "6" : "4"}
                        // sliderData={getTypeItems(section.items, "photo")}
                        sliderData={getTypeItems(section.items, ["photo", "ad"], 8)}
                        width={section.seolocation && section.seolocation.includes("web-stories") ? "150" : "238"}
                        type={
                          section.seolocation && section.seolocation.includes("web-stories") ? "webstories" : "grid"
                        }
                        compType={compType}
                        // sliderClass="grid_slider"
                        // TODO -- Below check is made to set latest section items min height to cater the section name in it.
                        sliderClass={`${section.name && section.name == "latest" ? "latest" : ""}${
                          section.seolocation && section.seolocation.includes("web-stories") ? "webstories" : ""
                        } grid_slider`}
                      />
                    </div>
                    {/* {
                  <div className="btf-placeholder">
                    <AdCard mstype="btf" adtype="dfp" />
                  </div>
                } */}
                  </div>
                </ErrorBoundary>
              ) : null,
            )
          : ""
        : null}
      <div className="btf-placeholder">
        <AdCard mstype="btf" adtype="dfp" />
      </div>
    </React.Fragment>
  );
};

const TrendingSectionGrid = props => {
  const { compType, configLbl, parentSection, createSectionLayout, noSeo } = props;

  const trendingSection = parentSection.recommended.trending;
  if (!(parentSection && parentSection.recommended && parentSection.recommended.trending)) {
    return null;
  }

  return trendingSection && trendingSection.items
    ? // && trendingSection.items.length > 0
      createSectionLayout({
        dataObj: trendingSection,
        configlabel: configLbl,
        override: {
          imgsize: compType == "videolist" ? "largewidethumb" : "smallthumb",
        },
        compType,
        noSeo,
      })
    : null;
};

const TrendingSectionSlider = props => {
  const { compType, parentSection, getTypeItems, noSeo } = props;

  const trendingSection = parentSection.recommended.trending;
  if (!(parentSection && parentSection.recommended && parentSection.recommended.trending)) {
    return null;
  }

  // Always put checks before using Object properties
  return trendingSection && trendingSection.items ? (
    //  && trendingSection.items.length > 1
    <React.Fragment>
      <div className="section">
        <h3>
          <span>{trendingSection.secname}</span>
        </h3>
      </div>
      <Slider
        margin="5"
        size="4"
        sliderData={getTypeItems(trendingSection.items, ["video", "photo", "ad"], 8)}
        width="236"
        type="grid"
        sliderClass="grid_slider"
        istrending="true"
        compType={compType}
        noSeo={noSeo}
      />
    </React.Fragment>
  ) : null;
};

const TrendingSection = props => {
  const { sections, compType, parentSection, createSectionLayout, getTypeItems, noSeo } = props;

  return compType == "videolist" && sections != undefined ? (
    <TrendingSectionGrid
      compType={compType}
      parentSection={parentSection}
      configLbl="trending"
      createSectionLayout={createSectionLayout.bind(this)}
      noSeo={noSeo}
    />
  ) : (
    <TrendingSectionSlider
      compType={compType}
      parentSection={parentSection}
      getTypeItems={getTypeItems}
      noSeo={noSeo}
    />
  );
};

const ArticlelistSections = props => {
  const { item, createSectionLayout } = props;
  const _item = JSON.parse(JSON.stringify(item));

  if (typeof _item.items === "object") {
    return item.tn == "photolist" ? (
      <div className="wdt_photo">
        <SectionHeader sectionhead={item.secname} weblink={item.override || item.wu} />

        <Slider
          margin="10"
          size="4"
          sliderData={item.items}
          width="230"
          type="grid"
          sliderClass="showcase"
          movesize="4"
        />
      </div>
    ) : item.tn == "articlelist" ? (
      createSectionLayout({
        dataObj: _item,
        configlabel: "listSection",
      })
    ) : item.tn == "videolist" ? (
      <VideoSection videoData={item} />
    ) : null;
  }
  return null;
};

const Articlelisting = props => {
  const { parentSection, createSectionLayout, compType, sections, astroListingWidget } = props;
  return (
    <React.Fragment>
      <div className="row">
        {parentSection && parentSection.id == siteConfig.astrowidgetId ? (
          <AstroListingWidget labelDataObj={astroListingWidget} parentSection={parentSection} />
        ) : null}

        {parentSection && parentSection.id != siteConfig.astrowidgetId ? (
          <React.Fragment>
            <div
              className={
                parentSection.items[0].tn == "moviereview" || parentSection.items[0].tn == "movieshow"
                  ? "col12"
                  : "col8"
              }
            >
              {createSectionLayout({
                compType,
                dataObj: { items: parentSection.items },
                configlabel: "main",
              })}
            </div>
          </React.Fragment>
        ) : null}
        {parentSection.items[0].tn != "moviereview" || parentSection.items[0].tn == "movieshow" ? (
          <React.Fragment>
            <div className="col4">
              {/* {parentSection && parentSection.subsec1 === siteConfig.pages.business ? (
                <div>
                  <AdCard mstype="budslug1" adtype="dfp" className="box-item mr15 ad-top" />
                  <AdCard mstype="budslug2" adtype="dfp" className="box-item mr15 ad-top" />
                  <AdCard mstype="budslug3" adtype="dfp" className="box-item mr15 ad-top" />
                  <AdCard mstype="budslug4" adtype="dfp" className="box-item mr15 ad-top" />
                  <AdCard mstype="budslug5" adtype="dfp" className="box-item mr15 ad-top" />
                </div>
              ) : ( */}
              <React.Fragment>
                <AdCard mstype="mrec1" adtype="dfp" className="box-item mr15 ad-top" />
                <div className="box-item adblockdiv">
                  <AdCard className="emptyAdBox" mstype="ctnbccl" className="" adtype="ctn" />
                </div>
              </React.Fragment>
              {/* )} */}
            </div>
          </React.Fragment>
        ) : null}
      </div>
      {createSectionLayout({
        dataObj: { items: parentSection.items },
        configlabel: "default",
        compType,
        override: {
          startIndex: "2",
          imgsize: "smallthumb",
          noOfElements: sections == undefined ? 24 : 9,
        },
      })}
      {/* For SEO trending section in Articlelist */}
      {/* Merging trending now and trending topics in one location - LAN-6015 */}
      {/* If one exists, show them, otherwise merge both */}
      {parentSection.recommended &&
      parentSection.recommended.trendingtopics &&
      parentSection.recommended.trendingnow &&
      ((parentSection.recommended.trendingnow[0] && parentSection.recommended.trendingnow[0].items) ||
        (parentSection.recommended.trendingtopics[0] && parentSection.recommended.trendingtopics[0].items)) ? (
        // &&
        // parentSection.recommended.trendingtopics[0].items.length > 0
        <div className="trending-bullet" data-row-id="trending-topics" data-scroll-id="trending-topics">
          <KeyWordCard
            items={[]
              .concat(
                parentSection.recommended.trendingnow[0] && parentSection.recommended.trendingnow[0].items
                  ? parentSection.recommended.trendingnow[0].items
                  : [],
              )
              .concat(
                parentSection.recommended.trendingtopics[0] && parentSection.recommended.trendingtopics[0].items
                  ? parentSection.recommended.trendingtopics[0].items
                  : [],
              )}
            secname=" "
          />
        </div>
      ) : null}
      {
        <div className="btf-placeholder">
          <AdCard mstype="btf" adtype="dfp" />
        </div>
      }
    </React.Fragment>
  );
};

const Petrolwidget = ({ data }) =>
  data && data.pdwidget && data.pdwidget.pprice ? (
    <div className="pdwidget">
      <a href={data.pdwidget.wu}>
        <div className="pp">
          <svg width="14" height="15" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M13.38 3.525l.013-.012-3.16-3.096-.9.883 1.79 1.758A2.083 2.083 0 009.759 5c0 1.15.95 2.083 2.12 2.083.302 0 .59-.062.85-.175v6.009a.843.843 0 01-.85.833.843.843 0 01-.848-.833v-3.75c0-.921-.759-1.667-1.697-1.667h-.848V1.667C8.485.746 7.725 0 6.788 0H1.697C.759 0 0 .746 0 1.667V15h8.485V8.75h1.273v4.167c0 1.15.95 2.083 2.12 2.083C13.05 15 14 14.067 14 12.917V5c0-.575-.238-1.096-.62-1.475zM6.789 5.833H1.697V1.667h5.09v4.166zm5.09 0A.843.843 0 0111.03 5c0-.458.382-.833.849-.833.466 0 .848.375.848.833a.843.843 0 01-.848.833z"
              fill="#000"
              fillRule="nonzero"
            />
          </svg>
          <h3>{data.pdwidget.prate}</h3>
          <span>{data.pdwidget.pprice}</span>
        </div>
        <div className="pd">
          <svg width="11" height="17" xmlns="http://www.w3.org/2000/svg">
            <g fillRule="nonzero" fill="none">
              <path
                d="M7.92 14.784H2.64a.793.793 0 01-.792-.792V7.128c0-.437.355-.792.792-.792h5.28c.437 0 .792.355.792.792v6.864a.793.793 0 01-.792.792z"
                fill="#000"
              />
              <path
                d="M5.28 8.422l-1.357 2.17a1.497 1.497 0 001.268 2.287l.178.001a1.497 1.497 0 001.268-2.287L5.28 8.422zm.089 3.93h-.178a.968.968 0 01-.82-1.48l.909-1.454.909 1.455a.968.968 0 01-.82 1.48z"
                fill="#FFF"
              />
              <path
                d="M9.53 5.49L6.336 3.292V2.112h-.528V.792A.793.793 0 005.016 0H1.32a.793.793 0 00-.792.792v1.32H0v13.2c0 .582.474 1.056 1.056 1.056h8.448c.582 0 1.056-.474 1.056-1.056V7.447c0-.783-.385-1.515-1.03-1.958zM1.056.791c0-.145.119-.264.264-.264h.528v1.056h.528V.528h.528v1.056h.528V.528h.528v1.056h.528V.528h.528c.145 0 .264.119.264.264v1.32H1.056V.792zm8.976 14.52a.529.529 0 01-.528.528H1.056a.529.529 0 01-.528-.528V3.696h4.224v-.528H.528V2.64h5.28v.528H5.28v.528h.71L9.23 5.924c.502.345.801.914.801 1.523v7.865z"
                fill="#000"
              />
            </g>
          </svg>
          <h3>{data.pdwidget.drate}</h3>
          <span>{data.pdwidget.dprice}</span>
        </div>
        <div className="checplist">
          <span className="iocl">{data.pdwidget.io}</span>
          <span className="tdate">{data.pdwidget.date}</span>
          <div className="cp">{data.pdwidget.cp}</div>
        </div>
      </a>
    </div>
  ) : null;

const AstroListingWidget = props => {
  const { labelDataObj, parentSection } = props;
  const card = parentSection && parentSection.items && parentSection.items[1];
  let astroLink = "";
  if (labelDataObj && Object.keys(labelDataObj).length > 0) {
    astroLink = generateUrl(labelDataObj && labelDataObj.items && labelDataObj.items[0]);
  }
  return (
    <div className="col8 pd0">
      <ul className="col12 pd0">
        <li className="col6">
          <ul className="astro_widget in-AL">
            {labelDataObj && labelDataObj.items[0] ? (
              <React.Fragment>
                <GridCardMaker card={labelDataObj.items[0]} cardType="horizontal" imgsize="smallthumb"></GridCardMaker>
                <li className="astro_text">
                  <span className="text_ellipsis">{labelDataObj.items[0].syn}</span>
                </li>
              </React.Fragment>
            ) : null}
            <li className="astro_slider">
              <ErrorBoundary>
                <Slider
                  type="zodiac_sign"
                  size="5"
                  sliderData={siteConfig.ZodiacSigns.data}
                  width="55"
                  islinkable="false"
                  link={astroLink}
                  margin="5"
                />
              </ErrorBoundary>
            </li>
          </ul>
        </li>

        <GridCardMaker
          card={card}
          key=""
          keyName="key_astro"
          cardType="lead"
          columns="6"
          configImgsize="largethumb"
          imgsize=""
          index="2"
          compType="articlelist"
        />
      </ul>
    </div>
  );
};

// const ETiframs = () => (
//   <div className="row">
//     <div class="col4">
//       <iframe
//         src={siteConfig.etIframes.sensex}
//         height="200"
//         width="300"
//         border="0"
//         frameBorder="0"
//         scrolling="no"
//         title="business Market"
//       />
//     </div>
//     <div class="col4">
//       <iframe
//         src={siteConfig.etIframes.stokes}
//         height="230"
//         width="300"
//         border="0"
//         frameBorder="0"
//         scrolling="no"
//         title="business Market"
//       />
//     </div>
//     <div class="col4">
//       <iframe
//         src={siteConfig.etIframes.rateOfTheDay}
//         height="300"
//         width="300"
//         border="0"
//         frameBorder="0"
//         scrolling="no"
//         title="business Market"
//       />
//     </div>
//   </div>
// );

export default connect(mapStateToProps)(ListPage);
