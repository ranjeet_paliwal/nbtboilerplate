export const designConfigs = {
  trending: [
    {
      startIndex: 0,
      noOfColumns: 1,
      type: "lead",
      istrending: "true",
    },
  ],
  videoLatestSectionFirst: [
    {
      startIndex: 0,
      noOfColumns: 2,
      noOfElements: 6,
      className: "col12 pd0",
      imgsize: "largewidethumb",
    },
  ],
  videoLatestFirst: [
    {
      startIndex: 0,
      noOfColumns: 3,
      noOfElements: 9,
      className: "col12 pd0",
      imgsize: "largewidethumb",
    },
  ],
  videoLatestSectionSecond: [
    {
      startIndex: 6,
      noOfColumns: 2,
      className: "col12 pd0",
      imgsize: "largewidethumb",
    },
  ],
  videoLatestSecond: [
    {
      startIndex: 9,
      noOfColumns: 3,
      className: "col12 pd0",
      imgsize: "largewidethumb",
    },
  ],
  photoVideoHeaderL2: [
    {
      startIndex: 0,
      noOfElements: 2,
      noOfColumns: 2,
      className: "col12",
    },
  ],
  videoListL2: [
    {
      startIndex: 2,
      noOfColumns: 3,
      className: "col12 pd0",
    },
  ],
  PLmiddleL2: [
    {
      startIndex: 2,
      noOfElements: 3,
      noOfColumns: 3,
      className: "col12",
    },
  ],
  photoListL2: [
    {
      startIndex: 5,
      noOfColumns: 4,
      className: "col12",
    },
  ],
  webstoriesList1: [
    {
      startIndex: 0,
      noOfElements: 4,
      noOfColumns: 4,
      className: "col12",
      offads: true,
    },
  ],
  webstoriesList2: [
    {
      startIndex: 4,
      noOfColumns: 6,
      className: "col12",
      offads: true,
    },
  ],
  webstoriesList3: [
    {
      startIndex: 0,
      noOfColumns: 6,
      className: "col12",
      offads: true,
    },
  ],
  movielistHeader: [
    {
      startIndex: 0,
      noOfElements: 1,
      className: "col12 head movielisting",
      type: "horizontal-lead",
      offads: true,
    },
  ],
  movielistL2: [
    {
      startIndex: 1,
      noOfColumns: 2,
      className: "col12 bottom movielisting",
      type: "horizontal-lead",
      imgsize: "posterthumb",
      offads: true,
    },
  ],

  main: {
    sections: [
      {
        type: [
          {
            startIndex: 0,
            noOfElements: 2,
            noOfColumns: 2,
            type: "lead",
            imgsize: "largethumb",
            className: "col12 pd0",
          },
        ],
      },
    ],
  },
  listSection: {
    sections: [
      {
        className: "col12 hideSecName1 pd0",
        type: [
          {
            startIndex: 0,
            noOfElements: 2,
            type: "lead",
            className: "col4",
            imgsize: "largethumb",
          },
          {
            sections: [
              {
                className: "col4 pd0",
                type: [
                  {
                    startIndex: 2,
                    noOfElements: 2,
                    type: "vertical",
                    noOfColumns: 2,
                    className: "col12 pd0",
                    imgsize: "smallthumb",
                  },
                  {
                    startIndex: 4,
                    noOfElements: 3,
                    type: "only-info",
                    className: "col12",
                  },
                ],
              },
            ],
          },
          {
            startIndex: 8,
            type: "only-info",
            className: "col4",
            noOfElements: 4,
          },
        ],
      },
    ],
  },
  default: [
    {
      startIndex: 0,
      type: "horizontal-lead",
      className: "col12 pd0 medium_listing",
      noOfColumns: 3,
    },
  ],
  gridHorizontal: [
    {
      startIndex: 0,
      type: "vertical",
      noOfColumns: 5,
      className: "col12 most-read-stroies",
      istrending: "true",
    },
  ],
};
