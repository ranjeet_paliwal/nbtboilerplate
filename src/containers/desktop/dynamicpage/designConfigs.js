export const designConfigs = {
  topicslisting: [
    {
      startIndex: 0,
      noOfElements: 3,
      noOfColumns: 3,
      type: "lead",
      className: "11 col12 pd0",
    },
    {
      startIndex: 3,
      noOfColumns: 3,
      noOfElements: 11,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallthumb",
    },
    {
      startIndex: 14,
      noOfColumns: 1,
      noOfElements: 1,
      type: "ads-horizontal",
      className: "col12 pd0",
    },
    {
      startIndex: 16,
      noOfColumns: 3,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallthumb",
    },
  ],
  topicsOnlyInfo: [
    {
      startIndex: 0,
      noOfColumns: 4,
      type: "only-info",
      definitionTerm: true,
      className: "11 col12 pd0",
    },
  ],
  topiclistingVideo: [
    {
      startIndex: 0,
      noOfElements: 3,
      noOfColumns: 3,
      type: "lead",
      className: "11 col12 pd0",
    },
    {
      startIndex: 3,
      noOfColumns: 3,
      noOfElements: 12,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallthumb",
    },
    {
      startIndex: 14,
      noOfColumns: 1,
      noOfElements: 1,
      type: "ads-horizontal",
      className: "col12 pd0",
    },
    {
      startIndex: 16,
      noOfColumns: 3,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallwidethumb",
    },
  ],
};
