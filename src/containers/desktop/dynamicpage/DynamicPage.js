import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchListDataIfNeeded, fetchNextListDataIfNeeded, fetchProfileData } from "../../../actions/topics/topics";
import TopicsCard, { FAQ, LandingPageNewsCard, ProfileDesc, ShortProfile } from "../../../components/common/TopicsCard";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  _isCSR,
  setHyp1Data,
  _getStaticConfig,
  handleReadMore,
  loadInstagramJS,
  loadTwitterJS,
  handleFBembed,
  isMobilePlatform,
  hasValue,
} from "../../../utils/util";
import { designConfigs } from "./designConfigs";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import AdCard from "../../../components/common/AdCard";
import Ads_module from "../../../components/lib/ads/index";
import AnchorLink from "../../../components/common/AnchorLink";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/Desktop.scss";
import "../../../components/common/css/desktop/TopicsCard.scss";
import "../../../components/common/css/desktop/DynamicPages.scss";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import Accordion from "../../../components/common/Accordion";
import SocialShare from "../../../components/common/SocialShare";

import Slider from "../../../components/desktop/Slider/Slider";
import { setPageType } from "../../../actions/config/config";
import { mod_symmetricalAds } from "../../utils/articleshow_util";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import PlayerStats from "../../../campaign/cricket/components/PlayerStats";
const siteConfig = _getStaticConfig();

export class DynamicPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      totalPages: 0,
      _scrollEventBind: false,
    };
    this.config = {
      isFetchingNext: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params, value, router } = this.props;
    const { query } = this.props.location;
    const { pathname } = router.location;

    //set section & subsection for first time
    if (value && typeof value.pwa_meta == "object") {
      Ads_module.setSectionDetail(value.pwa_meta);

      if (value.pwa_meta.profileid && value.pwa_meta.profileid != "" && value.validtopic) {
        if (pathname && pathname.indexOf("/topics/") > -1) {
          if (value.pwa_meta.profile && value.pwa_meta.profile !== "") {
            dispatch(fetchProfileData(params, query, router, value.pwa_meta));
          }
        } else {
          dispatch(fetchProfileData(params, query, router, value.pwa_meta));
        }
      }
    }

    DynamicPage.fetchData({ dispatch, query, params, router }).then(data => {
      //attach scroll only when sections is not there in feed
      if (
        typeof this.props.value != "undefined" &&
        this.props.value.items &&
        !(this.props.value.tn && this.props.value.tn === "glossarylist")
      ) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta, pg } = this.props.value ? this.props.value : {};
      if (pg && pg.tp) {
        this.state.totalPages = pg.tp;
      }
      if (pwa_meta && typeof pwa_meta == "object") {
        Ads_module.setSectionDetail(pwa_meta);
        //set hyp1 variable
        pwa_meta ? setHyp1Data(pwa_meta) : "";
        //fire ibeat
        pwa_meta.ibeat && pwa_meta.ibeat.articleId ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      }

      handleReadMore();

      // mod_symmetricalAds("");
    });

    if (typeof window != "undefined") {
      window.setTimeout(() => {
        try {
          loadInstagramJS();
          loadTwitterJS();
          handleFBembed();
        } catch (ex) {
          console.log("ERROR PROCESSING EMBEDS", ex);
        }
      }, 2000);
    }
    dispatch(setPageType("topics"));
    Ads_module.render({});
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    let _this = this;
    if (this.state._scrollEventBind == false) return;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = (document.scrollingElement || document.documentElement).scrollTop;
    }
    let footerHeight = document.querySelector("#footerContainer,footer").offsetHeight;
    docHeight = docHeight - footerHeight;
    let curpg = parseInt(this.props.value.pg.cp) + 1;
    if (scrollValue + 1000 > docHeight && curpg <= this.state.totalPages) {
      const { dispatch, params, isFetching, router } = this.props;
      const { query } = this.props.location;
      if (isFetching == false) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        DynamicPage.fetchNextListData({ dispatch, query, params, router }).then(data => {
          let _data = data ? data.payload : {};
          //silentRedirect(_this.generateListingLink());
          _this.config.isFetchingNext = !_this.config.isFetchingNext;

          AnalyticsGA.pageview(location.origin + _this.generateListingLink());
          Ads_module.refreshAds(["fbn"]); //refresh fbn ads when next list added

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";

          // history push
          let queryString, url;
          queryString = window.location.search;
          url = location.origin + _this.generateListingLink();

          if (url != location.href || _data.pwa_meta.title != document.title) {
            //todo stop history push for webview
            window.history.replaceState({}, _data.pwa_meta.title, url);
            document.title = _data.pwa_meta.title;
          }
        });
      }
    }
  }

  generateListingLink(type) {
    let props = this.props;
    let pathname = props.location && props.location.pathname;

    if (pathname && props.value && props.value.pg && props.value.pg.cp) {
      let curpg = type == "moreButton" ? parseInt(props.value.pg.cp) + 1 : parseInt(props.value.pg.cp);

      // if (pathname.indexOf("/profile.cms") > -1) {
      //   if (pathname.indexOf("curpg") > -1) {
      //     let reExp = /curpg=\\d+/;
      //     return pathname.replace(reExp, "curpg=" + curpg);
      //   } else return pathname + ((pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
      // }
      var pathnameArr = pathname.split("/");
      var cp = pathnameArr[pathnameArr.length - 1];
      if (isNaN(cp)) {
        return pathname + "/" + curpg;
      } else {
        return pathname.replace(cp, curpg);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.searchkey != nextProps.params.searchkey ||
      this.props.params.searchtype != nextProps.params.searchtype ||
      this.props.params.curpg != nextProps.params.curpg
    ) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        DynamicPage.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();
          let _data = data && data.payload ? data.payload[0] : {};
          const { pg } = _data;
          if (pg && pg.tp) {
            this.state.totalPages = pg.tp;
          }

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
      //reset hyp1 to ''
      setHyp1Data();
    }
  }

  /*Its a generic function to create Sectional Layout in DynamicPage Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout(dataObj, datalabel, configlabel, showSlider) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    // this.manipulateDataObj(dataObj);
    return dataObj ? (
      <div className="row">
        <GridSectionMaker type={designConfigs[configlabel]} data={dataObj.items} />
      </div>
    ) : (
      <FakeListing />
    );
  }

  render() {
    const { isFetching, dispatch, params, value, error, router, profile } = this.props;
    const { query } = this.props.location;
    const topicsData = value;
    let isGlossary;
    let isTopics;

    const bannerObj =
      topicsData &&
      topicsData.cmsassocimg &&
      Array.isArray(topicsData.cmsassocimg) &&
      topicsData.cmsassocimg.length > 0 &&
      topicsData.cmsassocimg[0];

    const defaultPagetype = topicsData && hasValue(topicsData, "tabs.0.label");

    const pagetype =
      typeof params != "undefined" && typeof params.searchtype != "undefined" ? params.searchtype : defaultPagetype;

    let moreTxt = siteConfig.locale.next_topic.more_all;
    if (pagetype == "all") moreTxt = siteConfig.locale.next_topic.more_all;
    else if (pagetype == "news") moreTxt = siteConfig.locale.next_topic.more_news;
    else if (pagetype == "photos") moreTxt = siteConfig.locale.next_topic.more_photos;
    else if (pagetype == "videos") moreTxt = siteConfig.locale.next_topic.more_videos;

    const pathname = router && router.location && router.location.pathname;
    if (pathname) {
      if (pathname.indexOf("/glossary/") > -1) {
        isGlossary = true;
      } else if (pathname.indexOf("/topics/") > -1) {
        isTopics = true;
      }
    }

    const isGlossaryList = topicsData && topicsData.tn && topicsData.tn === "glossarylist";
    let dtype = topicsData && topicsData.dtype && topicsData.dtype.toLowerCase();
    dtype = dtype === "default" ? "all" : dtype;
    const businessType = profile && profile.businessType ? profile.businessType : undefined;

    return topicsData ? (
      <div className="body-content section-topics" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(topicsData.pwa_meta)}

        {/* For SEO Schema */}
        {SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}

        {topicsData.pwa_meta && topicsData.pwa_meta.faq && topicsData.pwa_meta.faq != ""
          ? SeoSchema().job(topicsData.pwa_meta.faq)
          : null}

        <div className="row player-stats">
          <div className="col12">
            <div className={`${isGlossaryList ? "glossary-list" : ""} dynamicpage `} data-artbox={"dynamicpage"}>
              {/* Breadcrumb  */}
              {topicsData &&
              typeof topicsData.breadcrumb != "undefined" &&
              typeof topicsData.breadcrumb.div != "undefined" &&
              typeof topicsData.breadcrumb.div.ul != "undefined" ? (
                <ErrorBoundary>
                  <Breadcrumb items={topicsData.breadcrumb.div.ul} />
                </ErrorBoundary>
              ) : null}

              {/* topics searchbox */}
              {isTopics && topicsData && topicsData.pagetype && topicsData.pagetype == "topicsall" ? (
                <div className="searchbox">
                  <iframe
                    taget="_parent"
                    scrolling="no"
                    src={`${siteConfig.weburl}${siteConfig.searchIframeLink}?layout=newtopic`}
                    height="90"
                    width="100%"
                    frameBorder="0"
                  />
                </div>
              ) : null}

              {!isGlossaryList &&
              topicsData &&
              topicsData.topicsdata &&
              Object.keys(topicsData.topicsdata).length > 0 ? (
                <div className="rightfloatedad">
                  <AdCard mstype="mrec1" adtype="dfp" />
                </div>
              ) : null}

              {topicsData.topicsdata ? (
                <LandingPageNewsCard
                  item={topicsData}
                  heading={topicsData.hl}
                  isGlossary={isGlossary}
                  bannerObj={bannerObj}
                />
              ) : null}

              {(businessType || isTopics) && topicsData && topicsData.topicsdata ? (
                <div className="con_stats">
                  {/* Topics Details */}
                  {topicsData && topicsData.topicsdata ? (
                    <ShortProfile
                      item={topicsData}
                      data={topicsData.topicsdata}
                      profile={profile}
                      businessType={businessType}
                      heading={topicsData.hl}
                      compType="dynamicPage"
                    />
                  ) : null}
                </div>
              ) : null}

              <div className="profileDesc story-content">
                {topicsData.topicsdata ? <ProfileDesc data={topicsData.topicsdata} item={topicsData} /> : null}
              </div>

              <div className="row">
                {profile && Object.keys(profile).length > 0 && businessType === "cricketer" ? (
                  <PlayerStats data={profile} />
                ) : null}
              </div>

              {/* embed articles slider for Desktop */}
              {topicsData.embedarticle ? (
                <div className="row wdt_superhit">
                  <ErrorBoundary>
                    <div className="superhit-wrapper">
                      <h3>
                        <span>{`Embedded articles`}</span>
                      </h3>
                      <Slider
                        type="grid"
                        size="5"
                        movesize="1"
                        sliderData={topicsData.embedarticle}
                        width={isMobilePlatform() ? "280" : "185"}
                        SliderClass="popular_videos"
                        islinkable
                        margin={isMobilePlatform() ? "15" : "10"}
                      />
                    </div>
                  </ErrorBoundary>
                </div>
              ) : null}

              {topicsData &&
                topicsData.seoschema &&
                Array.isArray(topicsData.seoschema) &&
                topicsData.seoschema.length > 0 &&
                topicsData.seoschema[0].item &&
                topicsData.seoschema[0].item["@type"] === "FAQPage" && <FAQ data={topicsData.seoschema[0].item}></FAQ>}

              {isGlossaryList ? (
                <div className="only-heading">
                  <SectionHeader sectionhead={"Dictionary"} headingTag="h1" />
                </div>
              ) : null}
              {/* Tablisting */}
              <div className="topic-listing">
                {topicsData.tabs && Array.isArray(topicsData.tabs) ? (
                  <ErrorBoundary>
                    <TopicsCard
                      data={topicsData.tabs}
                      type="topicsTabs"
                      params={params}
                      dispatch={dispatch}
                      router={router}
                      query={query}
                      templateName={topicsData.tn}
                      dtype={dtype}
                    />
                  </ErrorBoundary>
                ) : null}

                {isGlossaryList ? (
                  <div className="rightfloatedad">
                    <AdCard mstype="mrec1" adtype="dfp" />
                  </div>
                ) : null}

                {topicsData && isGlossaryList ? (
                  <RenderGlossaryList
                    topicsData={topicsData}
                    createSectionLayout={this.createSectionLayout.bind(this)}
                  />
                ) : null}

                {!isGlossaryList ? (
                  !isFetching || this.config.isFetchingNext ? (
                    topicsData.items && topicsData.items instanceof Array ? (
                      router &&
                      router.location &&
                      router.location.pathname &&
                      router.location.pathname.indexOf("/videos") > -1 ? (
                        this.createSectionLayout(topicsData, "", "topiclistingVideo")
                      ) : (
                        this.createSectionLayout(topicsData, "", "topicslisting")
                      )
                    ) : (
                      <div key={topicsData.hl} className="no-data-found">
                        <div className="nodata_txt_icon">
                          <b />
                          <span>No record found</span>
                        </div>
                      </div>
                    )
                  ) : (
                    <FakeListing />
                  )
                ) : null}

                {/* more-btn */}
                {topicsData &&
                !isGlossaryList &&
                parseInt(topicsData.pg && topicsData.pg.cp) < parseInt(topicsData.pg && topicsData.pg.tp) ? (
                  <AnchorLink hrefData={{ override: this.generateListingLink("moreButton") }} className="more-btn">
                    {moreTxt}
                  </AnchorLink>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    ) : (
      <FakeListing />
    );
  }
}

// fetchData
DynamicPage.fetchData = function({ dispatch, query, params, router }) {
  let isTopics = false;
  let type;
  dispatch(setPageType("topics"));

  let pathname = router && router.location && router.location.pathname;
  if (pathname.indexOf("/topics/") > -1) {
    isTopics = true;
  }
  type = isTopics ? undefined : "dynamicPage";
  return dispatch(fetchListDataIfNeeded(params, query, router, type));
};

// fetchNextListData
DynamicPage.fetchNextListData = function({ dispatch, query, params, router }) {
  return dispatch(fetchNextListDataIfNeeded(params, query, router, "dynamicPage"));
};

function mapStateToProps(state) {
  return {
    ...state.topics,
  };
}

const RenderGlossaryList = props => {
  const { topicsData, createSectionLayout } = props;
  return topicsData.sections && Array.isArray(topicsData.sections) && topicsData.sections.length > 0 ? (
    topicsData.sections.map(section => <GlossaryItems section={section} createSectionLayout={createSectionLayout} />)
  ) : topicsData.items && Array.isArray(topicsData.items) && topicsData.items.length > 0 ? (
    <GlossaryItems section={topicsData} createSectionLayout={createSectionLayout} />
  ) : null;
};

const GlossaryItems = props => {
  const { section, createSectionLayout } = props;
  return (
    <div className="wdt_glossary">
      <SectionHeader sectionhead={section.secname} weblink={section.wu} />
      {createSectionLayout(section, "", "topicsOnlyInfo")}
    </div>
  );
};

export default connect(mapStateToProps)(DynamicPage);
