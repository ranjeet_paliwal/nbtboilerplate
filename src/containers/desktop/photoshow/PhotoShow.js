import React, { Component } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import ImageCard from "../../../components/common/ImageCard/ImageCard";
import {
  fetchPhotoMazzaShowDataIfNeeded,
  fetchNextPhotoMazzaShowDataIfNeeded,
} from "../../../actions/photomazzashow/photomazzashow";
import FakePhotoMazzaCard from "../../../components/common/FakeCards/FakePhotoMazzaCard";
import PhotoMazzaShowCard from "../../../components/common/PhotoMazzaShowCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  elementInView,
  _isCSR,
  setHyp1Data,
  handleReadMore,
  _getStaticConfig,
  isMobilePlatform,
  isLoggedIn,
  updateConfig,
  isTechSite,
  isTechPage,
  filterAlaskaData,
} from "../../../utils/util";
import { fireActivity } from "../../../components/common/TimesPoints/timespoints.util";
import styles from "../../../components/common/css/PhotoMazzaShowCard.scss";
import stylesD from "../../../components/common/css/Desktop.scss";

// import { fetchAmazonDataIfNeeded } from "../../../modules/amazonwidget/actions/amazonwidget";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module from "../../../components/lib/ads/index";

import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import Breadcrumb from "../../../components/common/Breadcrumb";
import PhotoStoryCard from "../../../components/common/PhotoStoryCard";
import { PageMeta } from "../../../components/common/PageMeta";
import AdCard from "../../../components/common/AdCard";
import { setPageType, setParentId } from "../../../actions/config/config";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import CommentsPopup from "../../../components/common/CommentsPortal";
import WebTitleCard from "../../../components/common/WebTitleCard";
import KeyWordCard from "../../../components/common/KeyWordCard";
import AnchorLink from "../../../components/common/AnchorLink";
import { fetchRecommendedPhotoSection, nextGalLink } from "../../utils/photoshow_util";
import AmazonWidget from "../../../components/common/AmazonWidget/AmazonWidget";
import SvgIcon from "../../../components/common/SvgIcon";

const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;

export class PhotoMazzaShow extends Component {
  constructor(props) {
    super(props);
    if (typeof window !== "undefined" && "scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    this.state = {
      curpg: 1,
      _scrollEventBind: false,
      commentsMSID: "",
      photoshow_app_install: { active: false, show_slides: 6 },
      showCommentsPopup: false,
      isLoadingSlides: false,
      recommendedWidgetData: [],
      currentGalleryIndex: 0,
    }; //Default App Install Parameters
    this.config = { nextgalid: null, index: 0, counter: 0 }; //To refresh FBN ads on every 4th slide scroll up/down
    this.scrollHandler = false;
    this.adSlots = [];
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const { dispatch, params, value } = this.props;
    const { query } = this.props.location;
    const _this = this;

    //set section and subsection in window
    const pwaMeta = (value && value[0] && value[0][0] && value[0][0].pwa_meta) || "";
    const slideShowId = value && value[0] && value[0][0] && value[0][0].id;
    if (pwaMeta && typeof pwaMeta == "object") {
      // Fetch RHS data in CSR using the subsection id

      Ads_module.setSectionDetail(pwaMeta);
      //reset hyp1 variable
      setHyp1Data(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }

    if (params.msid) {
      // this.fetchRhsWidgetData(params.msid);
      fetchRecommendedPhotoSection(params.msid, this);
    }
    //Readmore button handling
    handleReadMore();

    PhotoMazzaShow.fetchData({
      dispatch,
      query,
      params,
    }).then(data => {
      try {
        //set section and subsection in window
        const { pwa_meta, audetails } =
          _this.props.value && _this.props.value[0] && _this.props.value[0][0] ? _this.props.value[0][0] : {};
        // TP Activity Logging
        const slideShowIdNew = data && data.payload && data.payload[0] && data.payload[0].id;
        // Doubt , in both case this can be replaced by params.msid
        if (typeof fireActivity == "function") {
          if (params.msid) {
            fireActivity("VIEW_PHOTO", params.msid);
          } else if (slideShowIdNew) {
            fireActivity("VIEW_PHOTO", slideShowIdNew);
          } else if (slideShowId) {
            fireActivity("VIEW_PHOTO", slideShowId);
            //FIXME: Hot fix for when above case fails ( old slideShowId is present and fires again when photoshow has loaded once)
          }
        }

        if (pwa_meta && typeof pwa_meta == "object") {
          // Fetch RHS data in CSR using the subsection id

          Ads_module.setSectionDetail(pwa_meta);
          //reset hyp1 variable
          setHyp1Data(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";

          updateConfig(pwa_meta, dispatch, setParentId);

          if (typeof data == "object" && data.type) {
            //Replace location pathname if not same with redirectUrl
            if (pwa_meta.redirectUrl.split(".com")[1] != window.location.pathname) {
              window.history.replaceState({}, pwa_meta.title, pwa_meta.redirectUrl.split(".com")[1]);
            }

            if (audetails) {
              window._editorname = audetails.cd;
            } else {
              //If editor present in pwa_meta , set in window Object
              window._editorname =
                typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
            }
            AnalyticsGA.pageview(window.location.pathname, {
              setEditorName: true,
            });
          }
        }
      } catch (e) {}

      //After render function once data
      _this.afterRender();

      //To scroll to slide in case of single theme
      const msid = _this.props.params.msid;
      let u = "";
      let d = null;
      if (msid.includes("msid-") && msid.includes("picid-")) {
        var h = msid.match(/picid-[0-9]+/);
        h && h[0] && ((u = h[0].split("picid-")[1]), (d = document.getElementById(u)));
      }
      var position = 0;
      u && d && (position = d.offsetTop),
        window.scrollTo({
          top: position,
        });
    });

    scrollTo(document.documentElement, 0, 0);
  }

  componentDidUpdate(prevProps) {
    if (this.props.value[0] && this.props.value[0].length > 0) {
      this.config.index = this.props.value[0].length - 1;
    }
    handleReadMore();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.isFetching != nextProps.isFetching ||
      this.props.params.msid != nextProps.params.msid ||
      this.state.showCommentsPopup != nextProps.showCommentsPopup
    );
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      PhotoMazzaShow.fetchData({ dispatch, query, params }).then(data => {
        try {
          // When page is reloaded with a different id,
          // fetch rhs feed again and reset widget data
          if (params.msid) {
            fetchRecommendedPhotoSection(params.msid, this);

            // Fire View_PHOTO activity after next call is successful
            if (typeof fireActivity == "function") {
              if (params.msid) {
                fireActivity("VIEW_PHOTO", params.msid);
              }
            }

            // this.fetchRhsWidgetData(params.msid);
          }

          this.setState({
            recommendedWidgetData: [],
            currentGalleryIndex: 0,
          });
        } catch (e) {}
      });
      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (_isCSR()) {
      //clear section window data
      Ads_module.setSectionDetail();
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset hyp1 to ''
      setHyp1Data();
    }
  }

  // fetchRhsWidgetData = galleryId => {
  //   const parsedGalleryId =
  //     typeof galleryId === "string" && galleryId.includes("msid-")
  //       ? galleryId.slice(galleryId.indexOf("msid-") + 5, galleryId.indexOf(","))
  //       : galleryId;
  //   fetch(`${process.env.API_BASEPOINT}/sc_related_photos/${parsedGalleryId}.cms?feedtype=sjson`)
  //     .then(recommendedWidgetData => {
  //       if (recommendedWidgetData && Array.isArray(recommendedWidgetData.stry)) {
  //         let filteredWidgetData = recommendedWidgetData;
  //         // Filtering data logic
  //         // 0  -> Next gal
  //         // 1  -> Prev gal
  //         // Next 5 main gallery show only 2 records (should not be next / prev)
  //         const nextGalData = recommendedWidgetData.stry[0];
  //         const prevGalData = recommendedWidgetData.stry[1];
  //         // Remove prevGalData , nextGalData, currentGallery from stories in widget
  //         if (prevGalData && nextGalData) {
  //           filteredWidgetData.stry = filteredWidgetData.stry.filter(
  //             widgetData =>
  //               widgetData.id !== prevGalData.id && widgetData.id !== nextGalData.id && widgetData.id !== galleryId,
  //           );
  //         }

  //         // Re add after filtering ( to only remove duplicates)
  //         if (nextGalData) {
  //           filteredWidgetData.stry = [nextGalData, ...filteredWidgetData.stry];
  //         }

  //         if (prevGalData) {
  //           filteredWidgetData.stry = [prevGalData, ...filteredWidgetData.stry];
  //         }

  //         // Concatenate already present data with current data fetched
  //         this.setState({
  //           recommendedWidgetData: [...this.state.recommendedWidgetData, filteredWidgetData],
  //         });
  //       }
  //     })
  //     .catch(e => console.log("Error fetching CSR RHS DATA"));
  // };

  afterRender() {
    const _this = this;
    if (_this.state._scrollEventBind == false) {
      _this.scrollHandler = throttle(_this.handleScroll.bind(_this));
      window.addEventListener("scroll", _this.scrollHandler);
      _this.state._scrollEventBind = true;
      if (
        _this.props.value[0][0] &&
        _this.props.value[0][0].pwa_meta &&
        _this.props.value[0][0].pwa_meta.templatelayout
      ) {
        window.scrollTo(0, 0);
      }
      // _this.config.observer = new IntersectionObserver(_this.intersectionCallback, { threshold: .5 });
      // _this.observerHandler(_this)
      // if(_this.props.value[0] && _this.props.value[0][0].it.id == "65337853")_this.sliderHandler()
    }
  }

  handleScroll(e) {
    let _this = this;

    if (this.state._scrollEventBind == false) return;

    let winHeight = window.innerHeight;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop == 0 ? html.scrollTop : body.scrollTop;

    let footerHeight = document.getElementById("footerContainer").offsetHeight;

    docHeight = docHeight - footerHeight;
    const hasMoreSlides =
      typeof _this.props.value[0][_this.config.index].nextset !== "undefined" &&
      _this.props.value[0][_this.config.index].nextset !== "";
    const offset = hasMoreSlides ? 3000 : 2000;

    if (scrollValue + offset > docHeight) {
      let nextgalid = (_this.config.nextgalid =
        _this.props.value[0][_this.config.index] &&
        _this.props.value[0][_this.config.index].pwa_meta &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid != ""
          ? _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid
          : null);

      const { dispatch, params, isFetching } = this.props;
      const { query } = this.props.location;
      const galIndex = _this.config.index;

      if (!isFetching && !this.state.isLoadingSlides && (nextgalid || hasMoreSlides)) {
        if (hasMoreSlides) {
          this.setState({ isLoadingSlides: true });
        }
        try {
          PhotoMazzaShow.fetchNextData({
            dispatch,
            query,
            params,
            nextgalid,
            galIndex,
          }).then(data => {
            this.setState({ isLoadingSlides: false });
            let _data = data ? data.payload : {};
            // _this.config.index++;
            /*window.setTimeout(()=>{
              var event = new CustomEvent("newsadded");
              // Dispatch/Trigger/Fire the event
              document.dispatchEvent(event);
            },500);*/

            //reset hyp1 variable
            setHyp1Data(_data.pwa_meta);

            // Fetch new rhs data with every perpetual galler
            fetchRecommendedPhotoSection(nextgalid || params.msid, this);
            // this.fetchRhsWidgetData(nextgalid || params.msid);

            //fire ibeat for next
            // _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
          });
        } catch (ex) {}
      }
    }

    if (!_this.config.shouldTrackerOnScroll) {
      _this.config.shouldTrackerOnScroll = document.querySelector("[data-active]")
        ? elementInView(document.querySelector("[data-active]"), true)
          ? true
          : false
        : true;
    }

    _this.config.shouldTrackerOnScroll ? _this.trackersOnScroll() : null;
  }

  refreshRhsAd = () => {
    try {
      Ads_module.refreshAds(["mrec1"]);
    } catch (e) {
      console.log("ERROR REFRESHING ADS");
    }
  };

  trackersOnScroll() {
    let _this = this;
    let items = document.querySelectorAll(`.photo_card`);
    let navHeight = document.getElementById("headerContainer")
      ? document.getElementById("headerContainer").clientHeight
      : 95;

    // // Calculate current RHS offsets and add class
    // const contentElements = document.querySelectorAll(".photomazzashow-content .lft_content");

    const currentScroll = window.scrollY;
    const rhsWidget = document.querySelector(".rgt_content");
    const contentStartElement = document.querySelector(".photomazzashow-content");
    let topOffset = 0;

    if (rhsWidget) {
      const rhsWidgetBot =
        rhsWidget && rhsWidget.firstChild ? rhsWidget.firstChild.offsetTop + rhsWidget.firstChild.offsetHeight : 700;
      topOffset = contentStartElement.offsetTop;
      if (
        currentScroll >= rhsWidgetBot &&
        rhsWidget.firstChild &&
        !rhsWidget.firstChild.classList.contains("sticky-rhs")
      ) {
        rhsWidget.firstChild.classList.add("sticky-rhs");
      }
      if (
        currentScroll < rhsWidgetBot &&
        rhsWidget.firstChild &&
        rhsWidget.firstChild.classList.contains("sticky-rhs")
      ) {
        rhsWidget.firstChild.classList.remove("sticky-rhs");
      }
    }

    for (let i = 0; i < items.length; i++) {
      let elem = items[i];
      if (elem && elementInView(elem, true, 50)) {
        let url = elem.getAttribute("data-url");
        let parentIndex =
          elem.parentElement && elem.parentElement.getAttribute("parent-index")
            ? elem.parentElement.getAttribute("parent-index")
            : 0;
        let pwa_meta = _this.props.value[0][parseInt(parentIndex)].pwa_meta;
        const audetails = _this.props.value[0][parseInt(parentIndex)].audetails;
        let _title = elem.querySelector("img") ? elem.querySelector("img").getAttribute("alt") : pwa_meta.title;
        // Refresh ad and current RhsWidget data on scrolling to next gallery / back up
        if (parentIndex != this.state.currentGalleryIndex) {
          this.refreshRhsAd();
          this.setState({
            currentGalleryIndex: parseInt(parentIndex),
          });
        }

        if (url != window.location.pathname) {
          window.history.replaceState({}, _title, url);
          document.title = _title;
        }

        // Recreate ad if not done for this slide
        // Apply attribute so not recreated multiple times for same slide.
        // If scrolled and reached a multiple of 3 , recreate ad
        if ((i + 1) % 3 === 0 && !elem.getAttribute("adrefreshed")) {
          this.refreshRhsAd();
          elem.setAttribute("adrefreshed", true);
        }
        // trigger page view on page load
        if (!elem.getAttribute("gatracked")) {
          //If editor present in pwa_meta , set in window Object
          window._editorname = "";
          if (audetails) {
            window._editorname = audetails.cd;
          } else {
            window._editorname =
              typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
          }
          if (i == 0) {
            elem.setAttribute("comscorepageview", true);
          }
          AnalyticsGA.pageview(url, {
            setEditorName: true,
            comscorepageview: elem.getAttribute("comscorepageview"),
          });
          //fire ibeat
          if (pwa_meta.ibeat) {
            if (elem.getAttribute("id") != "") {
              pwa_meta.ibeat.articleId = elem.getAttribute("id");
            }
            setIbeatConfigurations(pwa_meta.ibeat);
          }
          elem.setAttribute("gatracked", true);
          elem.setAttribute("comscorepageview", true);
        }
      } else {
        if (elem && elem.getAttribute("gatracked")) {
          elem.removeAttribute("gatracked");
        }
        // Removed attribute if present and element not in view.
        if (elem && elem.getAttribute("adrefreshed")) {
          elem.removeAttribute("adrefreshed");
        }
      }
    }
  }

  showAppInstall(items, msid) {
    let _this = this;
    if (_this.state.photoshow_app_install && !_this.state.photoshow_app_install.active) return false;
    let app_install_show_slides = _this.state.photoshow_app_install.show_slides
      ? _this.state.photoshow_app_install.show_slides
      : 0;
    let imagepos = "";
    for (let i = 0; i < items.length; i++) {
      if (items[i].id == msid) {
        imagepos = i;
        break;
      }
    }
    return items.length > app_install_show_slides && imagepos <= app_install_show_slides ? true : false;
  }

  openCommentsPopup = commentsMSID => {
    this.setState({ showCommentsPopup: true, commentsMSID });
  };

  closeCommentsPopup = () => {
    this.setState({ showCommentsPopup: false, commentsMSID: "" });
  };

  //Scroll Page to next article
  scrollInView = (e, msid) => {
    e.preventDefault();
    var nextArtPos = document.getElementById(msid)
      ? document.getElementById(msid).offsetTop
      : document.querySelector(".fake-photoview")
      ? document.querySelector(".fake-photoview").offsetTop
      : null;
    if (nextArtPos != null) {
      window.scrollTo(0, nextArtPos);
    }
  };

  getLHSWidget(item, index, pwaConfigAlaska, isAmpPage) {
    const { params, router } = this.props;
    const pagetype = "photoshow";

    return item.it ? (
      <ErrorBoundary key={item.it.id}>
        {/* For SEO Meta */
        index == 0 && item.pwa_meta ? PageMeta(item.pwa_meta) : null}

        {item.it.id != params.msid && index === 0 && item.pwa_meta && item.pwa_meta.templatelayout == "Layout1" ? (
          <Link to={item.it.wu} className={styles["view-from-start"] + " view-from-start"}>
            {locale.view_gallery_from_start}
          </Link>
        ) : null}
        {/* if templatelayout = layout1 ? PhotoMazzaShowCard : PhotoStoryCard */}
        {item.pwa_meta && item.pwa_meta.templatelayout && item.pwa_meta.templatelayout == "Layout1" ? (
          <PhotoMazzaShowCard
            parentid={item.it.id}
            photoshow_app_install={this.state.photoshow_app_install}
            openCommentsPopup={this.openCommentsPopup.bind(this, item.id)}
            // showAppInstall={this.showAppInstall.bind(this)}
            item={item}
            msid={params.msid}
            parentIndex={index}
            isLoadingSlides={this.state.isLoadingSlides}
            isAmpPage={isAmpPage}
          />
        ) : (
          <PhotoStoryCard
            parentid={item.it.id}
            photoshow_app_install={this.state.photoshow_app_install}
            // openCommentsPopup={this.openCommentsPopup}
            openCommentsPopup={this.openCommentsPopup.bind(this, item.id)}
            // showAppInstall={this.showAppInstall.bind(this)}
            item={item}
            msid={item.it.id}
            parentIndex={index}
            picid={params.picid}
            isLoadingSlides={this.state.isLoadingSlides}
            router={router}
            pwaConfigAlaska={pwaConfigAlaska}
          />
        )}

        {item.pwa_meta && WebTitleCard(item.pwa_meta, "photoshow")}
        {item.pwa_meta && item.pwa_meta.topicskey && (
          <div className="trending-bullet">{KeyWordCard(item.pwa_meta.topicskey, "photoshow")}</div>
        )}
        {/* Next gallery */}
        {item.pwa_meta && item.pwa_meta.nextGal && item.pwa_meta.nextGal !== "" && (
          <div className="next_article" data-attr="next_article">
            <a
              onClick={e => this.scrollInView(e, item.pwa_meta.nextGal.msid)}
              data-nextart={item.pwa_meta.nextGal.msid}
              href={nextGalLink(item, pagetype)}
            >
              <span className="nxttitle">
                <ImageCard
                  type="absoluteImgSrc"
                  noLazyLoad={true}
                  msid={item.pwa_meta.nextGal.thumbid}
                  title={item.pwa_meta.nextGal.title}
                  size="rectanglethumb"
                  className="nextArticleThumb"
                />
                <span className={`${!isMobilePlatform() ? "text_ellipsis" : ""}`}>{item.pwa_meta.nextGal.title}</span>
              </span>
              <span className="nxtlink">{siteConfig.locale.next_gallery}</span>
            </a>
          </div>
        )}
        <AdCard adtype="ctn" mstype="ctnshowphoto" />
      </ErrorBoundary>
    ) : null;
  }

  getRHSWidget(item, bannerdata) {
    //FIXME: Recommended photowidget can be moved to common component
    const { recommendedWidgetData, currentGalleryIndex } = this.state;
    const data = recommendedWidgetData[currentGalleryIndex];
    return (
      <ErrorBoundary>
        <AdCard adtype="dfp" mstype="mrec1" />
        {data && data.stry && Array.isArray(data.stry) && (
          <div className="row list-vertical">
            {/* <AnchorLink href={item.recommended.related_photo[0].wu}>
                <SectionHeader sectionhead={item.recommended.related_photo[0].secname} />
              </AnchorLink> */}
            <h2>{locale.photoshowrhsheading}</h2>
            <GridSectionMaker
              type={defaultDesignConfigs.gridView}
              data={data.stry}
              override={{ noOfColumns: 3, type: "vertical", noOfElements: 6 }}
            />
          </div>
        )}
        {bannerdata && typeof bannerdata == "object" && bannerdata._type === "banner" ? (
          <ErrorBoundary key={"amz_banner"}>
            <div className="box-item">
              <a className="banner" href={bannerdata && bannerdata._override} target="_blank" rel="nofollow">
                <img src={bannerdata && bannerdata._image} />
              </a>
            </div>
          </ErrorBoundary>
        ) : null}
        {/* {item.recommended &&
          item.recommended.most_viewed_photo &&
          item.recommended.most_viewed_photo[0] &&
          item.recommended.most_viewed_photo[0].items && (
            <div className="row list-vertical">
              <SectionHeader sectionhead={item.recommended.most_viewed_photo[0].secname} />
              <GridSectionMaker
                type={defaultDesignConfigs.gridView}
                data={item.recommended.most_viewed_photo[0].items}
                override={{ noOfColumns: 2, type: "vertical", noOfElements: 4 }}
              />
            </div>
          )} */}
        {/* <AdCard adtype="dfp" mstype="mrec2" /> */}
        <AdCard adtype="ctn" mstype="ctnphotoshowrhs" />
      </ErrorBoundary>
    );
  }

  render() {
    const { isFetching, value, params, isInlineContent, view, route, error, header, router } = this.props;
    let { query, pathname } = this.props.location;
    let isAmpPage = false;
    if (typeof pathname === "string" && pathname.includes("amp_")) {
      isAmpPage = true;
    }
    let HomePageWidgets = filterAlaskaData(header.alaskaData, ["pwaconfig", "HomePageWidgets"], "label");
    const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");
    let bannerObj = "";
    let keys = Object.keys(HomePageWidgets);
    for (var i = 0; i < 10; i++) {
      if (HomePageWidgets[keys[i]] && HomePageWidgets[keys[i]]._type == "banner") {
        bannerObj = HomePageWidgets[keys[i]];
        break;
      }
    }
    // Decide layout photostory or old concept on the basis of templatelayout in pwa_meta
    // If templatelayout is undefined or blank ? New Version : Old version
    // const isphotostory = (value[0] && value[0][0] && value[0][0].pwa_meta && value[0][0].pwa_meta.templatelayout && value[0][0].pwa_meta.templatelayout != 'undefined' && value[0][0].pwa_meta.templatelayout != '') ? false : true;

    if (!isInlineContent) {
      return (
        <div className="photoshow_body">
          {/* Breadcrumb  */}
          {typeof value[0] != "undefined" &&
            value[0].length > 0 &&
            value[0][0] &&
            typeof value[0][0].breadcrumb != "undefined" &&
            value[0][0].breadcrumb.div &&
            value[0][0].breadcrumb.div.ul && (
              <ErrorBoundary>
                <Breadcrumb items={value[0][0].breadcrumb.div.ul} />
              </ErrorBoundary>
            )}
          <div className="photomazzashow-content lft_content">
            {typeof value[0] != "undefined" && value[0].length > 0 && value[0][0] && value[0][0].it ? (
              value[0].map((item, index) => {
                // if(item.pwa_meta && item.pwa_meta.templatelayout && item.pwa_meta.templatelayout != 'undefined' && item.pwa_meta.templatelayout != '' && params.msid && !params.picid){// }
                return (
                  <React.Fragment>
                    {index > 0 ? (
                      <div className={"story_partition"}>
                        <span>{locale.next_gallery}</span>
                      </div>
                    ) : null}
                    <div
                      className="photomazzashow-content"
                      itemType="https://schema.org/ImageGallery"
                      itemScope="1"
                      id={item.it.id}
                    >
                      <meta itemProp="headline" content={item.it.hl || item.secname} />
                      <meta itemProp="mainEntityOfPage" content={item.pwa_meta.canonical} />
                      <meta itemProp="dateModified" content={item.pwa_meta.modified} />
                      <meta itemProp="datePublished" content={item.pwa_meta.publishtime} />
                      {typeof item.pwa_meta.first !== "undefined" && (
                        <div className="ps_view_from_start">
                          <Link to={item.it.wu}>
                            {locale.view_gallery_from_start}
                            <SvgIcon name="nextstory" className="nextstory" />
                          </Link>
                        </div>
                      )}
                      <div className="title_with_tpwidget">
                        <h1>
                          <span>{item.it.hl || item.secname}</span>
                        </h1>
                        <div id={`widget-two-ps-${item && item.id}`} className="wdt_timespoints" />
                      </div>

                      {this.getLHSWidget(item, index, pwaConfigAlaska, isAmpPage)}
                    </div>

                    {/* {<div className="rgt_content">{this.getRHSWidget(item)}</div>} */}

                    <CommentsPopup
                      msid={this.state.commentsMSID}
                      showCommentsPopup={this.state.showCommentsPopup}
                      closeComments={this.closeCommentsPopup}
                      loggedIn={isLoggedIn()}
                      closeOnOverLay={true}
                      isNotEu={true}
                    />
                    <div className="top-ad">
                      {/* Commented for removal  */}
                      {/* <AdCard mstype="btf" adtype="dfp" /> */}
                    </div>
                  </React.Fragment>
                );
              })
            ) : (
              <FakePhotoMazzaCard showImages={true} />
            )}

            {isFetching ? (
              <div style={{ width: "67%" }}>
                <FakePhotoMazzaCard showImages={true} />
              </div>
            ) : null}
          </div>
          {typeof value[0] != "undefined" && value[0].length > 0 && value[0][0] && value[0][0].it ? (
            <div className="rgt_content">
              <div>{this.getRHSWidget(value[0][0], bannerObj)}</div>
            </div>
          ) : null}
        </div>
      );
    } else {
      return (
        <div key={this.props.params.msid}>
          {typeof value[0] != "undefined" && value[0].length > 0 ? (
            value[0].map((item, index) => {
              return (
                <ErrorBoundary key={index}>
                  <PhotoMazzaShowCard
                    item={item}
                    msid={params.msid}
                    isInlineContent={isInlineContent}
                    view={view}
                    isAmpPage={isAmpPage}
                  />
                </ErrorBoundary>
              );
            })
          ) : (
            <FakePhotoMazzaCard showImages={true} />
          )}
        </div>
      );
    }
  }
}

PhotoMazzaShow.fetchData = function({ dispatch, query, params, router }) {
  // const { payload } = data;
  // const { pwa_meta } = payload;
  // console.log("payloadpayload", payload);
  // const parentId = (pwa_meta && pwa_meta.parentid) || "";
  // const subsec1 = (pwa_meta && pwa_meta.subsec1) || "";
  // dispatch(setParentId(parentId, subsec1));

  //set pagetype
  // dispatch(setPageType("photoshow"));

  // dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router, "amazonMobile"));
  return dispatch(fetchPhotoMazzaShowDataIfNeeded(params, query)).then(data => {
    // console.log("psdata", data);
    const pwaMeta = (data && data.payload && data.payload.pwa_meta) || "";
    if (pwaMeta) {
      Ads_module.setSectionDetail(pwaMeta);
      dispatch(setPageType("photoshow", pwaMeta.site));
    }
    // console.log("pwaMeta11", pwaMeta, data);
    // updateConfig(pwaMeta, dispatch, setParentId);
    return data;
  });
};

PhotoMazzaShow.fetchNextData = function({ dispatch, query, params, nextgalid, galIndex }) {
  return dispatch(fetchNextPhotoMazzaShowDataIfNeeded(params, query, nextgalid, galIndex));
};

function mapStateToProps(state) {
  return {
    ...state.photomazzashow,
    header: state && state.header,
  };
}

export default connect(mapStateToProps)(PhotoMazzaShow);
