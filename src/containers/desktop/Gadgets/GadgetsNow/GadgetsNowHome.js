import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded } from "../../../../actions/gn/home/home";
import { setPageType, setParentId } from "../../../../actions/config/config";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import GadgetSlider from "../../../../components/common/GadgetSlider";
import BrandSlider from "../../../../components/common/BrandSlider";
import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import SectionLayoutMemo from "../../home/SectionLayout";
import { updateConfig, _getStaticConfig, _isFrmApp } from "../../../../utils/util";
import { getPWAMeta } from "../../../utils/gadgets_util";
import Ads_module from "../../../../components/lib/ads/index";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import "../../../../components/common/css/desktop/SectionWrapper.scss";
import "../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../components/common/css/desktop/gn_compare_list.scss";
import AdCard from "../../../../components/common/AdCard";
import { PageMeta } from "../../../../components/common/PageMeta";
import { DataProvider } from "../../../../utils/utilityContext";
import GadgetsNowRHS from "../../../../components/common/Gadgets/GadgetsNowRHS/GadgetsNowRHS";
import GadgetsCompareModule from "../../../../components/common/Gadgets/GadgetsCompareModule/GadgetsCompareModule";
const siteConfig = _getStaticConfig();

export class GadgetsNowHome extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { dispatch, query, params, router, gadgetsNowHome } = this.props;
    const _this = this;
    let pwaMeta = getPWAMeta(gadgetsNowHome && gadgetsNowHome.data);

    GadgetsNowHome.fetchData({ dispatch, query, params, router }).then(data => {
      if (data && data.payload && data.payload.newsData) {
        pwaMeta = getPWAMeta(data.payload);
      }

      if (pwaMeta && typeof pwaMeta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwaMeta);
        //fire ibeat
        pwaMeta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwaMeta, dispatch, setParentId);
      }
    });
  }

  render() {
    const { alaskaData, home, articlelist, gadgetsNowHome, router } = this.props;
    let widgetData;
    let gadgetsData;
    let pwaMeta;

    if (gadgetsNowHome && gadgetsNowHome.data && gadgetsNowHome.data.newsData) {
      widgetData = gadgetsNowHome.data.newsData;
      gadgetsData = gadgetsNowHome.data.gadgetsData;
      pwaMeta = getPWAMeta(gadgetsNowHome.data);
    }
    const recommended = widgetData && widgetData.recommended;
    const sectionConfig = widgetData && widgetData.sectionconfig;

    // section data filter from config
    let photoSectionData = "";
    let videoSectionData = "";
    let reviewsSectionData = "";
    let newsSectionData = "";
    let tipsSectionData = "";
    if (widgetData && widgetData.sections && Array.isArray(widgetData.sections) && sectionConfig) {
      newsSectionData = widgetData.sections.filter(item => {
        return sectionConfig.news && item.id == sectionConfig.news.msid;
      });
      newsSectionData = newsSectionData && newsSectionData[0];
      tipsSectionData = widgetData.sections.filter(item => {
        return sectionConfig.ts && item.id == sectionConfig.ts.msid;
      });
      tipsSectionData = tipsSectionData && tipsSectionData[0];

      photoSectionData =
        sectionConfig.photos &&
        widgetData.sections.filter(item => {
          return sectionConfig.photos && item.id == sectionConfig.photos.msid;
        });
      photoSectionData = photoSectionData && photoSectionData[0];

      videoSectionData =
        sectionConfig.video &&
        widgetData.sections.filter(item => {
          return sectionConfig.video && item.id == sectionConfig.video.msid;
        });
      videoSectionData = videoSectionData && videoSectionData[0];

      // reviewsSectionData = widgetData.sections.filter(item => {
      //   return sectionConfig.reviews && item.id == sectionConfig.reviews.msid;
      // });
      // reviewsSectionData = reviewsSectionData[0];
    }

    const trendingData =
      recommended && recommended.trendingtopics && recommended.trendingtopics[0] && recommended.trendingtopics[0];
    const mostReadNews = recommended && recommended.trending;

    return widgetData ? (
      <div className="body-content section-wrapper gn">
        {pwaMeta ? PageMeta(pwaMeta) : null}
        {widgetData && typeof widgetData == "object" && widgetData.sections && widgetData.sections.length > 0 ? (
          <React.Fragment>
            <div className="row">
              <div className="col8">
                <div className="row">
                  <SectionHeader
                    sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.gadgets}
                    weblink={siteConfig.weburl + `/tech/mobile-phones`}
                    morelink={false}
                    headingTag="h1"
                  />

                  <GadgetSlider gadgetsData={gadgetsData} source="gadgetsnow" />
                </div>

                <div className="row container-comparewidget web gray_wdt">
                  <SectionHeader
                    sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.comparedotxt}
                    weblink=""
                  />
                  <div className="col12 pd0">
                    <GadgetsCompareModule heading="h2" source="gadgetsnow" />
                  </div>
                </div>

                <div className="row con_brands">
                  <SectionHeader
                    sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.brands}
                    weblink={""}
                  />
                  <BrandSlider />
                </div>

                {reviewsSectionData ? (
                  <SectionLayoutMemo
                    key={"1_gnHomeReview"}
                    datalabel={"gnHomeReview"}
                    dataObj={reviewsSectionData}
                    alaskaData={alaskaData}
                  />
                ) : null}

                <SectionLayoutMemo
                  key="1_tipsAndTricks"
                  datalabel={"gnHomeReview"}
                  dataObj={tipsSectionData}
                  alaskaData={alaskaData}
                />
              </div>

              <div className="col4">
                <DataProvider
                  value={{
                    newsSectionData,
                    alaskaData,
                    trendingData,
                    videoSectionData,
                    photoSectionData,
                    mostReadNews,
                  }}
                >
                  <GadgetsNowRHS />
                </DataProvider>
              </div>
            </div>

            <div className="btf-placeholder">
              <AdCard mstype="btf" adtype="dfp" />
            </div>
          </React.Fragment>
        ) : null}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

GadgetsNowHome.fetchData = function({ dispatch, query, params, router }) {
  dispatch(setPageType("gadgethome"));
  return dispatch(fetchListDataIfNeeded(params, query, router)).then(data => {
    const pwaMeta = getPWAMeta(data && data.payload);
    if (pwaMeta) {
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    app: state.app,
    gadgetsNowHome: state.gadgetsNowHome,
  };
}

export default connect(mapStateToProps)(GadgetsNowHome);
