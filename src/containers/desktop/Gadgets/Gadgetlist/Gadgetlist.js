import React, { Component } from "react";
import { connect } from "react-redux";
import Parser from "html-react-parser";
import {
  fetchGadgetListDataIfNeeded,
  fetchNextGadgetListDataIfNeeded,
} from "../../../../actions/gn/gadgetlist/gadgetlist";
import { PageMeta } from "../../../../components/common/PageMeta";
import Breadcrumb from "../../../../components/common/Breadcrumb";

import "../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../components/common/css/desktop/gn_gadgetList.scss";
import "../../../../components/common/css/commonComponents.scss";

import GadgetFilterTool from "../../../../components/common/Gadgets/GadgetFilterTool";
import GadgetWidget from "../../../../components/common/Gadgets/GadgetWidget/GadgetWidget";

import { fetchFilterListDataIfNeeded } from "../../../../actions/gn/gadgetlist/gadgetfiltertool";
import AnchorLink from "../../../../components/common/AnchorLink";
import { AnalyticsGA } from "../../../../components/lib/analytics/index";

import PriceTable from "../../../../components/common/Gadgets/PriceTable/PriceTable";
import CategoryList from "../../../../components/common/Gadgets/CategoryList/CategoryList";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import TechKeywords from "../../../../components/common/Gadgets/TechKeywords";

// eslint-disable-next-line import/named
//import { PageMeta } from "../../components/PageMeta/PageMeta";
import { _getStaticConfig, getKeyByValue, updateConfig } from "../../../../utils/util";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import { setParentId, setPageType } from "../../../../actions/config/config";
import Ads_module from "../../../../components/lib/ads/index";
import AdCard from "../../../../components/common/AdCard";

const siteConfig = _getStaticConfig();

class Gadgetlist extends Component {
  constructor(props) {
    super(props);
    let activeSortStr = "";

    if (props.params && props.params.filters) {
      activeSortStr = props.params.filters.split("sort=")[1];
    }

    this.state = {
      curpg: 1,
      category: "",
      sortCriteria: {
        recency: "popular",
        rating: "asc",
        price: "asc",
        active: "popular",
        activeSort: activeSortStr,
      },
    };
    this.dataType = "latest";

    this.config = {
      isFetchingNext: false,
    };
  }

  componentDidMount() {
    const { dispatch, params, router, gadgetsData } = this.props;
    const { query } = this.props.location;
    this.setState({
      category: params.category,
    });
    const _this = this;

    Gadgetlist.fetchData({ dispatch, query, params, router }).then(data => {
      let pwa_meta = null;
      if (gadgetsData !== null) {
        pwa_meta = gadgetsData.pwa_meta;
      } else if (data) {
        pwa_meta = data && data.payload && data.payload.gadgetListData && data.payload.gadgetListData.pwa_meta;
      }
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    dispatch(setPageType("gadgetlist"));

    setTimeout(() => {
      this.prefillfilter();
    }, 3000);
  }

  capitalize = s => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  componentWillReceiveProps(newProps, newState) {
    // debugger;
    const _this = this;
    const { dispatch, query, params, router, gadgetsData } = newProps;
    const dataType = document.querySelector(".tabs-square li.active")
      ? document.querySelector(".tabs-square li.active").id
      : null;
    if (
      newProps.params.filters !== this.props.params.filters ||
      newProps.params.brand !== this.props.params.brand ||
      newProps.params.category !== this.props.params.category ||
      router.location.pathname != this.props.location.pathname
      // newProps.location.pathname.indexOf('upcoming') > -1
    ) {
      const brandSelector = document.querySelectorAll(".filtercontent-brand input:checked");
      let brands = [];
      if (brandSelector) {
        brandSelector.forEach(item => brands.push(_this.capitalize(item.value)));
      }

      if (!params.brand && brands.length > 0) {
        params.brand = brands.join("|");
      }
      Gadgetlist.fetchData({ dispatch, query, params, router, dataType }).then(data => {
        // set first tab active no active tab in filter found
        // this.filterTabActive();
        // set section and subsection in window
        // const { pwa_meta } = this.props.item && this.props.item[0] ? this.props.item[0] : {};
        let pwa_meta = null;
        if (gadgetsData !== null) {
          pwa_meta = gadgetsData.pwa_meta;
        } else if (data) {
          pwa_meta = data && data.payload && data.payload.gadgetListData && data.payload.gadgetListData.pwa_meta;
        }
        if (pwa_meta && typeof pwa_meta === "object") {
          Ads_module.setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      });
    }
  }

  prefillfilter() {
    // debugger;
    const { params, router } = this.props;
    // let checkBox = document.getElementsByTagName('input');
    const checkBox = document.querySelectorAll("#sortData input");
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == "radio" && params.filters && params.filters.indexOf("sort=") > -1) {
        // set radio button value
        const filterlist = params.filters.split("&");
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf("sort=") + 5;
          const getvalue = filterlist[i].substr(getIndex);
          if (getvalue == checkBox[b].value) checkBox[b].click();
        }
      } else if (
        checkBox[b].type == "radio" &&
        (!params.filters || params.filters.indexOf("sort=") < 0) &&
        checkBox[b].value == "popular"
      ) {
        checkBox[b].checked = "checked";
      }
      if (
        checkBox[b].type == "checkbox" &&
        checkBox[b].getAttribute("datavalues") == "upcoming" &&
        router &&
        router.location &&
        router.location.pathname.indexOf("upcoming") > -1
      ) {
        // set Upcoming checkbox button value
        checkBox[b].checked = "checked";
      } else if (
        checkBox[b].type == "checkbox" &&
        params.brand &&
        checkBox[b].getAttribute("datavalues") == "brand" &&
        checkBox[b].value == params.brand
      ) {
        // set brand checkbox button value
        checkBox[b].checked = "checked";
      } else if (checkBox[b].type == "checkbox" && params.filters) {
        // set all filters checkbox button value
        // set radio button value
        const filterlist = params.filters.split("&");
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf("=") + 1;
          const getfiltecriteria = filterlist[i].substr(0, getIndex - 1);
          const getvalue = filterlist[i].substr(getIndex);
          if (getfiltecriteria == checkBox[b].getAttribute("datavalues") && getvalue.indexOf(checkBox[b].value) > -1) {
            checkBox[b].checked = "checked";
            // checkBox[b].checked = "checked";
          }
        }
      }
    }
  }

  setCategoryfromParent = obj => {
    // debugger;
    obj.preventDefault();

    const { dispatch, params, query, router } = this.props;
    const deviceType = obj.currentTarget.id;
    const seoName = getKeyByValue(gadgetsConfig.gadgetMapping, deviceType);

    this.props.router.push(`/tech/${seoName}`);
    this.restForm();
    const categoryoverride = seoName;
    if (seoName) {
      this.setState({
        category: seoName,
        sortCriteria: {
          recency: "popular",
          rating: "asc",
          price: "asc",
          active: "popular",
          activeSort: "",
        },
      });
    }

    const tabparams = { seoName };
    dispatch(fetchFilterListDataIfNeeded(tabparams, query, router, categoryoverride));
    dispatch(fetchGadgetListDataIfNeeded(tabparams, query, router, categoryoverride));
  };

  readMore(event) {
    const { dispatch, query, params, router, value, location } = this.props;

    const curpg = this.state.curpg + 1;
    this.setState({ curpg });

    const _this = this;
    if (_this.config.isFetchingNext == false) {
      _this.config.isFetchingNext = !_this.config.isFetchingNext;
      Gadgetlist.fetchNextListData({ dispatch, query, params, router, curpg }).then(data => {
        const _data = data ? data.payload : {};
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        AnalyticsGA.event({ category: "Web GS", action: "GadgetsList", label: "morepage" });

        if (typeof AnalyticsGA !== "undefined" && typeof AnalyticsGA.pageview !== "undefined") {
          AnalyticsGA.pageview(window.location.origin + location.pathname);
        }
        // AnalyticsGA.event({ category: "Wap_GS", action: "GadgetsList", label: "morepage" });
        // AnalyticsGA.pageview(location.origin + _this.generateListingLink());
        // Ads_module.refreshAds(['fbn']);//refresh fbn ads when next list added
        // fire ibeat for next
        // data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { params, routeParams } = this.props;

    if (prevProps.params.category !== params.category) {
      this.restForm();
      setTimeout(() => {
        Ads_module.refreshAds(["btf"]);
      }, 2000);
    }

    /*  if (
      prevProps.routeParams &&
      prevProps.routeParams.filters != routeParams.filters &&
      !routeParams.filters.includes("&") &&
      prevProps.routeParams.filters &&
      !prevProps.routeParams.filters.includes("&")
    ) {
      setTimeout(() => {
        this.prefillfilter();
      }, 2000);
    } */
  }

  setDataType = (tabType, tabVal) => {
    // debugger
    const { dispatch, params, query, router } = this.props;
    let {
      sortCriteria: { recency, rating, price },
    } = this.state;

    // const dataType = obj.currentTarget.id;

    //    this.dataType = dataType;
    // let category=this.state.category;
    const category = params.category;
    let filters = params && params.filters;
    //  params ? (params.category = category) : null;

    const sortData = {
      recency: {
        popular: "popular",
        latest: "latest",
      },
      rating: {
        asc: "desc",
        desc: "asc",
      },
      price: {
        asc: "desc",
        desc: "asc",
      },
    };

    let ratingVal = rating;
    let priceVal = price;
    let sortStr = "";
    let filterPath = "";
    let filterText = "/filters/";

    if (tabType == "rating") {
      ratingVal = sortData["rating"][rating];
      sortStr = `${tabType}-${ratingVal}`;
    } else if (tabType == "price") {
      priceVal = sortData["price"][price];
      sortStr = `${tabType}-${priceVal}`;
    } else {
      recency = tabType;
    }

    if (tabType == "latest") {
      sortStr = "latest";
    }

    // this.setState({
    //   sortbystate: `sort=${dataType}`,
    // });

    if (filters) {
      // sortStr = `${filters}`;
      let filtersArr = "";
      if (filters.includes("&sort=")) {
        filtersArr = filters.split("&sort=");
      } else {
        filtersArr = filters.split("sort=");
      }

      if (filtersArr.length > 1) {
        filtersArr.pop();
      }

      filters = filtersArr[0];
      // if (filters == `brand=${params.brand}`) {
      //   //if only single brand is there in filter
      //   filterText = "";
      //   filters = params.brand;
      // }

      if (filters && sortStr) {
        filters += "&";
      }

      if (sortStr) {
        filterPath = `${filterText}${filters}sort=${sortStr}`;
      } else {
        filterPath = `${filterText}${filters}`;
      }
    } else {
      if (sortStr) {
        if (params.brand) {
          filterPath = `${filterText}brand=${params.brand}&sort=${sortStr}`;
        } else {
          filterPath = `${filterText}sort=${sortStr}`;
        }
      } else {
        filterPath = `${filterText}`;
      }
    }

    // if (tabType == "popular") {
    //   sortStr = "";

    //   if (!filters) {
    //     filterPath = "";
    //   }
    // }

    if (tabType == "popular") {
      sortStr = "";
      if (
        router &&
        router.params &&
        router.params.brand &&
        !router.params.brand.includes("|") &&
        (!filters || (filters && !filters.includes("brand=")))
      ) {
        // if there is only a single brand in fliter

        filterPath = "/" + router.params.brand.toLowerCase();
      } else if (!filters) {
        filterPath = "";
      }
    }

    if (router && router.location.pathname.indexOf("articlelist") < 0) {
      this.props.router.push(`/tech/${category}${filterPath}`);
    } else {
      return dispatch(fetchGadgetListDataIfNeeded(params, query, router, tabType));
    }

    this.setState({
      sortCriteria: {
        recency,
        rating: ratingVal,
        price: priceVal,
        active: tabType,
        activeSort: sortStr,
      },
    });
  };

  techKeywordHandler = () => {
    //  alert("ss");
    this.restForm();
    setTimeout(() => {
      this.prefillfilter();
    }, 2000);
  };

  restForm(obj) {
    // clear check box

    const checkBox = document.getElementsByTagName("input");
    if (checkBox && checkBox.length > 0) {
      for (let b = 0; b < checkBox.length; b++) {
        if (checkBox[b].type == "radio") {
          checkBox[b].checked = false;
        }
        if (checkBox[b].type == "checkbox") {
          checkBox[b].checked = false;
        }
      }
    }
    if (document.getElementById("filterapplied")) document.getElementById("filterapplied").innerHTML = "";
  }

  render() {
    const { gadgetsData, isFetching, params, router, urlParams } = this.props;
    // console.log("this.props", this.props);
    const pagetype = "gadgetlist";
    const sortCriteria = { ...this.state.sortCriteria };

    const breadcrumb = gadgetsData && gadgetsData.breadcrumb;

    const heading = (gadgetsData && gadgetsData.brandheader) || "";

    const pagination = (gadgetsData && gadgetsData.pg) || "";
    const navigationfilter = gadgetsData && gadgetsData.navigationfilter;
    const pwaMeta = (gadgetsData && gadgetsData.pwa_meta) || "";
    const gadgetsArray = (gadgetsData && gadgetsData.gadgets) || "";
    const gadgetsCount = gadgetsData && gadgetsData.gadgets && gadgetsData.gadgets.length;
    const atfContent = gadgetsData && gadgetsData.seo && gadgetsData.seo.atfcontent;
    const btfContent = gadgetsData && gadgetsData.seo && gadgetsData.seo.btfcontent;
    let activeGadget = params && params.category;
    activeGadget = activeGadget ? gadgetsConfig.gadgetMapping[activeGadget] : "";
    return (
      <div>
        {breadcrumb && <Breadcrumb items={breadcrumb.div.ul} />}

        <div className="row gadgetlist_body">
          <div className="col12 gl_top_bar">
            {pwaMeta ? PageMeta(pwaMeta) : null}
            <div className="select-inline-gadgets">
              <CategoryList clicked={this.setCategoryfromParent.bind(this)} activeGadget={activeGadget} />
            </div>
            {atfContent ? Parser(atfContent) : ""}
            <TechKeywords data={navigationfilter} params={params} clicked={this.techKeywordHandler} />
          </div>

          <div className="gl_filters col3">
            <GadgetFilterTool
              category={this.state.category}
              params={this.props.params}
              isFetching={isFetching}
              router={router}
              pagetype={pagetype}
              dispatch={this.props.dispatch}
              sortCriteria={sortCriteria}
            />
          </div>

          <div className="gl_list_mobiles col9">
            <div className="section">
              <div className="top_section">
                <h1>
                  <span> {heading} </span>
                </h1>
              </div>
            </div>
            <GadgetWidget
              _category={this.state.category}
              params={this.props.params}
              isFetching={isFetching}
              router={router}
              pagetype={pagetype}
              gadgetList={gadgetsArray}
              dispatch={this.props.dispatch}
              navigationfilter={navigationfilter}
              setDataType={this.setDataType}
              sortCriteria={sortCriteria}
            />

            {gadgetsArray &&
            pagination &&
            pagination.cp &&
            pagination.tp &&
            parseInt(pagination.cp) < parseInt(pagination.tp) ? (
              <div className="more-list">
                <a onClick={this.readMore.bind(this)} className="more-btn">
                  {siteConfig.locale.tech.loadmore}
                </a>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div>
          <AdCard mstype="btf" adtype="dfp" />
        </div>
        <div className="row seo-btf-content">
          <div
            className="col12"
            dangerouslySetInnerHTML={{
              __html: btfContent,
            }}
          />
        </div>
        <PriceTable data={gadgetsData} params={params} />
        {/* <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" /> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.gadgetlist,
  };
}

Gadgetlist.fetchData = ({ dispatch, params, query, router }) => {
  dispatch(setPageType("gadgetlist"));
  return dispatch(fetchGadgetListDataIfNeeded(params, query, router));
};
Gadgetlist.fetchNextListData = ({ dispatch, params, query, router, curpg }) => {
  return dispatch(fetchNextGadgetListDataIfNeeded(params, query, router, curpg));
};

export default connect(mapStateToProps)(Gadgetlist);
// export default Gadgetlist;
