import React, { PureComponent } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

import PropTypes from "prop-types";
import AnchorLink from "../../../../../components/common/AnchorLink";
import Breadcrumb from "../../../../../components/common/Breadcrumb";
import SearchModule from "../../../../../components/common/Gadgets/SearchModule/SearchModule";
import AmazonAffiliate from "../../../../../components/common/Gadgets/AmazonAffiliate/AmazonAffiliate";
import FakeCompareShowCard from "../../../../../components/common/FakeCards/FakeCompareShowCard";
import {
  _getStaticConfig,
  _isCSR,
  getBuyLink,
  getAffiliateTags,
  getKeyByValue,
  isMobilePlatform,
  initEvent,
  updateConfig,
} from "../../../../../utils/util";
import { setParentId, setPageType } from "../../../../../actions/config/config";

import gadgetsConfig from "../../../../../utils/gadgetsConfig";
import { fetchDataIfNeeded, addGadget, removeGadget } from "../../../../../actions/gn/comparision/Detail/index";
import { PageMeta } from "../../../../../components/common/PageMeta";
import Ads_module from "../../../../../components/lib/ads/index";

// import WdtAffiliatesList from "../../../components/Amazon/WdtAffiliatesList";
import ImageCard from "../../../../../components/common/ImageCard/ImageCard";
import GadgetsCompare from "../../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";

import "../../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../../components/common/css/GadgetNow.scss";
import "../../../../../components/common/css/gn_compare_show.scss";
import SvgIcon from "../../../../../components/common/SvgIcon";
import { setIbeatConfigurations } from "../../../../../components/lib/analytics/src/iBeat";
import AdCard from "../../../../../components/common/AdCard";

const siteConfig = _getStaticConfig();
const techLocale = siteConfig && siteConfig.locale && siteConfig.locale.tech;

class ComparisonDetails extends PureComponent {
  constructor(props) {
    super(props);

    const { routeParams } = props;
    let searchQueryArr = "";
    if (routeParams && routeParams.qstring) {
      const qstring = routeParams.qstring;
      if (qstring && qstring.indexOf("-vs-") !== -1) {
        searchQueryArr = qstring.split("-vs-");
      }
    }

    this.state = {
      deviceInfo: {
        device1: {
          searchResult: "",
          userInput: "",
          userInputHtml: "",
        },
        device2: {
          searchResult: "",
          userInput: "",
          userInputHtml: "",
        },
      },
      searchQueryArr,

      statusMsg: "",

      deviceData: "",
      suggestedDevices: "",
      // activeGadget: deviceToSearch,
      addedDevicefromSuggestion: "",
      updatedDeviceInfo: "",
      showSearchBox: false,
      fixedComparison: false,
    };
  }

  componentDidMount() {
    const { dispatch, query, params, data } = this.props;

    if (typeof initEvent === "function") {
      initEvent("scroll", this.initPageScroll);
    }
    if (typeof window !== undefined) {
      window.addEventListener("scroll", this.myScrolltop, true);
    }

    ComparisonDetails.fetchData({ dispatch, query, params }).then(response => {
      let pwa_meta = null;
      if (data && data.compareData !== null) {
        pwa_meta = data && data.compareData && data.compareData.pwa_meta;
      } else if (response) {
        pwa_meta = response && response.compareData && response.compareData.pwa_meta;
      }
      // const pwa_meta = (data && data.compareData && data.compareData.pwa_meta) || "";
      if (pwa_meta && typeof pwa_meta === "object") {
        // set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        // fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    dispatch(setPageType("compareshow"));
  }

  componentDidUpdate(prevProps) {
    const { location, dispatch, query, params, data } = this.props;

    if (prevProps.location !== location) {
      ComparisonDetails.fetchData({ dispatch, query, params });
      window.scrollTo({ top: 0 });
    }
    if (prevProps.data !== data) {
      let pwa_meta = null;
      if (data && data.compareData !== null) {
        pwa_meta = data && data.compareData && data.compareData.pwa_meta;
      } else if (prevProps.data !== null) {
        pwa_meta = prevProps.data.compareData && prevProps.data.compareData.pwa_meta;
      }
      // const pwa_meta = (data && data.compareData && data.compareData.pwa_meta) || "";
      if (pwa_meta && typeof pwa_meta === "object") {
        // set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        // fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    }
  }

  initPageScroll = () => {
    try {
      const fixedMenu = document.querySelector("#childrenContainer");
      const productView = document.querySelector(".wdt_compare-gadgets.details");
      if (fixedMenu) {
        const position = fixedMenu.getBoundingClientRect().y;
        if (position < 21) {
          this.setState({ fixedComparison: true });
          // productView.classList.add("fixed");
        } else {
          this.setState({ fixedComparison: false });
          // productView.classList.remove("fixed");
        }
      }
    } catch (ex) {
      // continue regardless of error
    }
  };

  myScrolltop = () => {
    if (window.scrollY >= 1000) {
      document.querySelector("#gettop") ? document.querySelector("#gettop").classList.add("back2top_icon") : null;
    } else if (window.scrollY < 50) {
      document.querySelector("#gettop") ? document.querySelector("#gettop").classList.remove("back2top_icon") : null;
    }
  };

  scrollToTop = () => {
    window.scrollTo(0, 0);
    // scroll-behavior: smooth;
    document.querySelector(".navbar").scrollIntoView({ behavior: "smooth" });
    document.querySelector("#gettop") ? document.querySelector("#gettop").classList.remove("back2top_icon") : null;
  };

  onFocusHandler = () => {
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    for (const key in deviceInfoCopy) {
      if ({}.hasOwnProperty.call(deviceInfoCopy, key)) {
        const device = deviceInfoCopy[key];
        deviceInfoCopy[key] = {
          searchResult: "",
          userInput: "",
          userInputHtml: device.userInputHtml,
        };
      }
    }
    this.setState({ deviceInfo: deviceInfoCopy });
  };

  onKeyDownHandler = event => {
    const deviceName = event.currentTarget.name;
    if (event.keyCode !== 40 && event.keyCode !== 38 && event.keyCode !== 13) {
      return;
    }
    const liItem = document.querySelector(`#ulinput_${deviceName} .active`);
    const inputSelected = document.querySelector(`#ulinput_${deviceName}`);

    if (event.keyCode === 40) {
      // Arrow Down Key Pressed
      if (!liItem.nextSibling) {
        const firstLi = inputSelected.firstChild;
        liItem.classList.remove("active");
        firstLi.classList.add("active");
      } else {
        liItem.nextSibling.classList.add("active");
        liItem.classList.remove("active");
      }
    } else if (event.keyCode === 38) {
      // Arrow Up Key Pressed
      if (!liItem.previousSibling) {
        const lastLi = inputSelected.lastChild;
        liItem.classList.remove("active");
        lastLi.classList.add("active");
      } else {
        liItem.previousSibling.classList.add("active");
        liItem.classList.remove("active");
      }
    } else if (event.keyCode === 13) {
      // ENTER key is pressed, prevent the form from being submitted,
      event.preventDefault();
      liItem.click();
    }
  };

  onChangeEventHandler = (category, evt) => {
    const { state } = this;
    const userInput = evt.currentTarget.value;
    const deviceName = evt.target.name;

    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };
    updatedDeviceInfo[deviceName] = {
      userInput,
    };
    this.setState({ deviceInfo: updatedDeviceInfo });

    this.searchDevice(userInput, category, deviceName);
  };

  addDeviceHandler = (deviceClicked, deviceName, gadgetCategory) => {
    const { dispatch, data, routeParams } = this.props;
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    const searchQueryArr = [];

    const activeDevice = routeParams && routeParams.device;

    if (data.compareData && data.compareData.techgadget && data.compareData.techgadget.gadget) {
      const gadgets = data.compareData.techgadget.gadget;
      gadgets.forEach(item => {
        searchQueryArr.push(item.productname);
      });
    }

    const searchQueryArrCopy = [...searchQueryArr];

    if (searchQueryArrCopy.indexOf(deviceClicked) !== -1) {
      // if user selects same device again, alert a message
      deviceInfoCopy[deviceName] = {
        userInput: "",
        userInputHtml: "",
        searchResult: deviceInfoCopy[deviceName].searchResult,
      };

      return this.setState({
        deviceInfo: deviceInfoCopy,
        statusMsg: techLocale && techLocale.alreadyAdded,
      });
    }

    searchQueryArrCopy.push(deviceClicked);

    const finalSearchQuery = searchQueryArrCopy.join(",");

    deviceInfoCopy[deviceName] = {
      userInput: "",
      userInputHtml: deviceClicked,
    };
    searchQueryArrCopy.sort();
    const seoPath = searchQueryArrCopy.join("-vs-");
    if (typeof window !== "undefined") {
      window.history.pushState({}, "", `/tech/compare-${activeDevice}/${seoPath}`);
    }

    this.setState({ deviceInfo: deviceInfoCopy, searchQueryArr: searchQueryArrCopy });
    // console.log('clicked', qstring, finalSearchQuery);
    dispatch(addGadget({ finalSearchQuery, gadgetCategory }));
  };

  removeDeviceHandler = (deviceClicked, gadgetCategory) => {
    const { dispatch, data, routeParams } = this.props;

    const activeDevice = routeParams && routeParams.device;

    const searchQueryArr = [];
    if (data.compareData && data.compareData.techgadget && data.compareData.techgadget.gadget) {
      const gadgets = data.compareData.techgadget.gadget;
      gadgets.forEach(item => {
        searchQueryArr.push(item.productname);
      });
    }

    const searchQueryArrCopy = [...searchQueryArr];

    const index = searchQueryArrCopy.indexOf(deviceClicked);
    if (index > -1) {
      searchQueryArrCopy.splice(index, 1);
      const finalSearchQuery = searchQueryArrCopy.join(",");

      searchQueryArrCopy.sort();
      const seoPath = searchQueryArrCopy.join("-vs-");
      if (typeof window !== "undefined") {
        window.history.pushState({}, "", `/tech/compare-${activeDevice}/${seoPath}`);
      }

      dispatch(removeGadget({ finalSearchQuery, gadgetCategory })).then(() => {
        this.setState(prevState => ({
          deviceInfo: prevState.deviceInfo,
          searchQueryArr: searchQueryArrCopy,
        }));
      });
    }
  };

  labelHandler = event => {
    const classArr = event.target.classList;
    if (classArr.contains("active")) {
      classArr.remove("active");
    } else {
      classArr.add("active");
    }
  };

  scrollToElement = event => {
    const elemId = event.target.value;
    // console.log('elemId', elemId);
    const elmnt = document.getElementById(elemId);
    if (elmnt) {
      elmnt.scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
    } else {
      // console.log('Scroll Element Not found');
    }
  };

  showDifferences = evt => {
    const checkBox = document.getElementById(evt.target.id);
    // Get the output text
    if (checkBox.checked === true) {
      this.toggleElement("same", "none"); // Shows
    } else {
      this.toggleElement("same", ""); // Shows
    }
  };

  toggleElement = (className, displayState) => {
    const elements = document.getElementsByClassName(className);
    for (let i = 0; i < elements.length; i++) {
      elements[i].style.display = displayState;
    }

    const div = document.querySelectorAll(".gn_table_layout div");

    div.forEach(item => {
      //  console.log(item.id);
      const tr = document.querySelectorAll(`#${item.id} tr`);
      let isSame = 0;
      tr.forEach(item => {
        const classVal = item.getAttribute("class");
        if (classVal != "same") {
          isSame++;
        }
      });
      if (isSame == 0) {
        document.getElementById(`${item.id}`).style.display = displayState;
      }
    });
  };

  searchDevice(value, category, deviceName) {
    const { state } = this;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    fetch(`${process.env.API_BASEPOINT}/autosuggestion.cms?type=brand&tag=product_cat&category=${category}&q=${value}`)
      .then(response => {
        // Examine the text in the response
        response.json().then(data => {
          const devices = data && data.map(item => ({ Product_name: item.Product_name, seoname: item.seoname }));

          updatedDeviceInfo[deviceName] = {
            searchResult: devices.filter(item => item !== ""),
            userInput: value,
            userInputHtml: "",
          };

          this.setState(prevState => ({
            deviceInfo: updatedDeviceInfo,
            searchQueryArr: prevState.searchQueryArr,
            statusMsg: "",
          }));
        });
      })
      .catch(() => {
        // console.log('Fetch Error :-S', err);
      });
  }

  suggestedDevicesCallBack = (item, deviceListInfo) => {
    let { suggestedDevices, updatedDeviceInfo } = this.state;
    suggestedDevices = item;
    updatedDeviceInfo = deviceListInfo;
    this.setState({
      suggestedDevices,
      updatedDeviceInfo,
    });
  };

  render() {
    const { data } = this.props;

    const { deviceInfo, statusMsg } = this.state;

    const pwaMeta = (data && data.compareData && data.compareData.pwa_meta) || "";

    const breadcrumb =
      (data &&
        data.compareData &&
        data.compareData.techgadget &&
        data.compareData.techgadget.breadcrumb &&
        data.compareData.techgadget.breadcrumb.div &&
        data.compareData.techgadget.breadcrumb.div.ul) ||
      "";

    const gadgetsInfo =
      (data && data.compareData && data.compareData.techgadget && data.compareData.techgadget.gadget) || "";
    const gadgetCategory = (gadgetsInfo && gadgetsInfo[0] && gadgetsInfo[0].category) || "";

    if (isMobilePlatform() && gadgetsInfo && Array.isArray(gadgetsInfo) && gadgetsInfo.length == 4) {
      gadgetsInfo.splice(3);
    }
    const seoName = getKeyByValue(gadgetsConfig.gadgetMapping, gadgetCategory);
    const popularGadgets =
      (data &&
        data.compareData &&
        data.compareData.techgadget &&
        data.compareData.techgadget.popularGadgetPair &&
        data.compareData.techgadget.popularGadgetPair.compare) ||
      "";

    let arrComparedDevices = [];

    if (popularGadgets) {
      if (isMobilePlatform()) {
        const arrCompare = [...popularGadgets];
        arrComparedDevices = arrCompare.filter(item => item.product_name.length < 4);
      } else {
        arrComparedDevices = [...popularGadgets];
      }
    }

    const pageTitle = [];

    const indexForLabel = {};

    // Code for geetting diff devices

    // Code for getting  Label Index, as diffrent devices may have diffrent attributes
    const gadgetsInfoCopy = [...gadgetsInfo];

    for (const key in gadgetsInfoCopy) {
      if ({}.hasOwnProperty.call(gadgetsInfoCopy, key)) {
        const item = gadgetsInfoCopy[key];

        const specsData = gadgetsInfoCopy[key] && gadgetsInfoCopy[key].specs;
        const getregionalname =
          item &&
          item.regional &&
          Array.isArray(item.regional) &&
          item.regional.filter(data => data && data.host === siteConfig.hostid);
        pageTitle.push(getregionalname && getregionalname[0] && getregionalname[0].name);
        for (const speckey in specsData) {
          if ({}.hasOwnProperty.call(specsData, speckey)) {
            if (speckey && specsData[speckey]) {
              const specsItem = specsData[speckey];

              if (!indexForLabel[speckey]) {
                indexForLabel[speckey] = {
                  size: 0,
                  indexwithMaxCount: 0,
                };
              }
              if (specsItem && Object.keys(specsItem).length > indexForLabel[speckey].size) {
                indexForLabel[speckey].size = Object.keys(specsItem).length;
                indexForLabel[speckey].indexwithMaxCount = key;
                indexForLabel[speckey].regional = specsItem.regional;
              }
            }
          }
        }
      }
    }

    const indexForLabelArr = Object.keys(indexForLabel).map(key => ({ key, val: indexForLabel[key] }));

    let amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.CD);
    amazonTag += "-21";
    const deviceInfoParam = {};
    if (isMobilePlatform()) {
      gadgetsInfoCopy.forEach((item, index) => {
        deviceInfoParam[`device${index + 1}`] = {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: item.name,
          seoName: item.productname,
        };
      });

      if (gadgetsInfoCopy.length == 2) {
        deviceInfoParam.device3 = {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        };
      }
    }

    // console.log("deviceInfoParam", deviceInfoParam);

    return gadgetsInfo ? (
      <div className={`container-compare-show ${isMobilePlatform() ? "pwa" : ""}`}>
        {this.state.showSearchBox &&
          ReactDOM.createPortal(
            <div className="wdt_compare-gadgets search-tool">
              <div className="backToScreen">
                <span
                  onClick={() => {
                    this.setState({ showSearchBox: false });
                  }}
                >
                  <SvgIcon name="back" className="bck" />
                </span>
                Compare
              </div>
              <SearchModule
                activeGadget={gadgetCategory}
                deviceInfo={deviceInfoParam}
                suggestedDevicesCallBack={this.suggestedDevicesCallBack}
                countCallBack={() => { }}
                // addedSuggestedDevice={addedDevicefromSuggestion}
                searchBoxStatus={() => {
                  this.setState({ showSearchBox: false });
                }}
              />
            </div>,
            document.getElementById("position-fixed-floater"),
          )}

        {breadcrumb && !isMobilePlatform() && <Breadcrumb items={breadcrumb} />}
        {pwaMeta ? PageMeta(pwaMeta) : ""}
        <h1>{`${techLocale.comparedotxt} ${pageTitle.join(" vs ")}`}</h1>
        {this.state.fixedComparison && _isCSR() ? (
          ReactDOM.createPortal(
            <div
              className={`wdt_compare-gadgets details ${isMobilePlatform() ? "wap" : ""} ${this.state.fixedComparison ? "fixed" : ""
                }`}
            >
              {isMobilePlatform() && (
                <div className="wdt_compare" data-exclude="amp">
                  <div
                    className="btn-compare"
                    onClick={() => {
                      this.setState({ showSearchBox: true });
                    }}
                  >
                    <SvgIcon name="gn-compare-icon" className="compare_icon" />
                    {siteConfig.locale.tech.comparetxt}
                  </div>
                </div>
              )}
              <DeviceSearched
                data={gadgetsInfo}
                gadgetCategory={gadgetCategory}
                deviceInfo={deviceInfo}
                changed={this.onChangeEventHandler}
                keydown={this.onKeyDownHandler}
                focused={this.onFocusHandler}
                deviceAdded={this.addDeviceHandler}
                deviceRemoved={this.removeDeviceHandler}
                statusMsg={statusMsg}
                getKeyByValue={getKeyByValue}
              />
              <div className="compare_criteria" data-exclude="amp">
                <CompareCriteria data={indexForLabelArr} scrollto={this.scrollToElement} />

                <div className="only_difference">
                  <input type="checkbox" onClick={this.showDifferences} id="showdiff" />
                  <label htmlFor="showdiff">{siteConfig.locale.tech.showDiff}</label>
                </div>

                {statusMsg && <span className="info-camparsion">{statusMsg}</span>}
              </div>
            </div>,
            document.getElementById("position-fixed-floater"),
          )
        ) : (
          <div className={`wdt_compare-gadgets details ${this.state.fixedComparison ? "fixed" : ""}`}>
            {isMobilePlatform() && (
              <div className="wdt_compare" data-exclude="amp">
                <div
                  className="btn-compare"
                  onClick={() => {
                    this.setState({ showSearchBox: true });
                  }}
                >
                  <SvgIcon name="gn-compare-icon" className="compare_icon" />
                  {siteConfig.locale.tech.comparetxt}
                </div>
              </div>
            )}
            <DeviceSearched
              data={gadgetsInfo}
              gadgetCategory={gadgetCategory}
              deviceInfo={deviceInfo}
              changed={this.onChangeEventHandler}
              keydown={this.onKeyDownHandler}
              focused={this.onFocusHandler}
              deviceAdded={this.addDeviceHandler}
              deviceRemoved={this.removeDeviceHandler}
              statusMsg={statusMsg}
              getKeyByValue={getKeyByValue}
            />
            <div className="compare_criteria" data-exclude="amp">
              <CompareCriteria data={indexForLabelArr} scrollto={this.scrollToElement} />

              <div className="only_difference">
                <input type="checkbox" onClick={this.showDifferences} id="showdiff" />
                <label htmlFor="showdiff">{siteConfig.locale.tech.showDiff}</label>
              </div>

              {statusMsg && <span className="info-camparsion">{statusMsg}</span>}
            </div>
          </div>
        )}

        <div className="detailed-spec-show">
          <div className="gn_table_layout">
            {indexForLabelArr &&
              indexForLabelArr.map((item, index) => {
                // console.log('indexForLabelArr', indexForLabelArr);
                // console.log('item', item);
                const keyExsist = true;
                const className = !keyExsist ? "differ" : false;
                return (
                  <React.Fragment>
                    <div key={item.key} className={className} id={item.key}>
                      <label className="spec active" onClick={this.labelHandler}>
                        {item.val.regional}
                      </label>
                      <GetLabelData item={item.key} data={item.val} gadgetsInfo={gadgetsInfo} keyExsist={keyExsist} />
                      {isMobilePlatform() && item.key === "keyFeatures" && <AdCard adtype="ctn" mstype="ctnTechCDP" />}
                      {isMobilePlatform() && item.key === "special_features" && (
                        <AdCard mstype="mrec1" adtype="dfp" className="ad1 mrec1 box-item" />
                      )}
                      {isMobilePlatform() &&
                        (item.key === "multimedia" ||
                          item.key === "design" ||
                          item.key === "storage" ||
                          item.key === "battery") && (
                          <AdCard mstype="mrec2" adtype="dfp" className="ad1 mrec2 box-item" />
                        )}
                    </div>

                    {!isMobilePlatform() && index !== 0 && (index + 1) % 4 == 0 && (
                      <div>
                        <AdCard mstype="btf" adtype="dfp" />
                      </div>
                    )}
                  </React.Fragment>
                );
              })}
          </div>

          <AmazonAffiliate category={gadgetCategory} tag={amazonTag} isslider="1" noimg="0" noh2="0" />
          {popularGadgets && popularGadgets.length > 0 && (
            <div className="wdt_popular_slider ui_slider">
              <GadgetsCompare
                type="popular"
                data={arrComparedDevices.slice(0, 3)}
                category={gadgetCategory}
                seoName={seoName}
              />
            </div>
          )}
        </div>
        {breadcrumb && isMobilePlatform() && <Breadcrumb items={breadcrumb} />}
        {isMobilePlatform() && <span className="back2top_icon" id="gettop" onClick={this.scrollToTop}></span>}
      </div>
    ) : (
      <FakeCompareShowCard />
    );
  }
}

const CompareCriteria = ({ data, scrollto }) => {
  const options =
    data &&
    data.map(item => (
      <option key={item.key} value={item.key}>
        {item.val && item.val.regional}
      </option>
    ));
  return (
    <select onChange={scrollto}>
      <option value="">{siteConfig.locale.tech.compCriteria}</option>
      {options}
    </select>
  );
};

const GetLabelData = ({ item, data, gadgetsInfo }) => {
  const categoryData = gadgetsInfo[data.indexwithMaxCount].specs[item];
  const categoryDataArr = [];
  for (const key in categoryData) {
    if (Object.prototype.hasOwnProperty.call(categoryData, key)) {
      const item1 = categoryData[key];

      if (item1.key && item1.value) {
        categoryDataArr.push({
          name: key,
          regional: item1.key,
          value: item1.value,
        });
      }
    }
  }

  return (
    <table>
      {categoryDataArr &&
        categoryDataArr.map((item2, index) => {
          let getClass = "differ";
          const itemName = item2 && item2.name;
          let device1Value = "";
          let device2Value = "";
          let device3Value = "";
          let device4Value = "";
          const arrayDeviceValues = [];
          if (
            gadgetsInfo[0] &&
            gadgetsInfo[0].specs[item] &&
            gadgetsInfo[0].specs[item][itemName] &&
            gadgetsInfo[0].specs[item][itemName].value
          ) {
            device1Value = gadgetsInfo[0].specs[item][itemName].value;
            arrayDeviceValues.push(device1Value);
          }

          if (
            gadgetsInfo[1] &&
            gadgetsInfo[1].specs[item] &&
            gadgetsInfo[1].specs[item][itemName] &&
            gadgetsInfo[1].specs[item][itemName].value
          ) {
            device2Value = gadgetsInfo[1].specs[item][itemName].value;
            arrayDeviceValues.push(device2Value);
          }
          if (
            gadgetsInfo[2] &&
            gadgetsInfo[2].specs[item] &&
            gadgetsInfo[2].specs[item][itemName] &&
            gadgetsInfo[2].specs[item][itemName].value
          ) {
            device3Value = gadgetsInfo[2].specs[item][itemName].value;
            arrayDeviceValues.push(device3Value);
          }
          if (
            gadgetsInfo[3] &&
            gadgetsInfo[3].specs[item] &&
            gadgetsInfo[3].specs[item][itemName] &&
            gadgetsInfo[3].specs[item][itemName].value
          ) {
            device4Value = gadgetsInfo[3].specs[item][itemName].value;
            arrayDeviceValues.push(device4Value);
          }

          if (arrayDeviceValues.length == 1) {
            getClass = "differ";
          } else if (arrayDeviceValues.every((val, i, arr) => val === arr[0])) {
            getClass = "same";
          }

          const colSpan = isMobilePlatform() ? gadgetsInfo && gadgetsInfo.length : 4;
          return (
            <React.Fragment key={item2.key}>
              <tr className={getClass}>
                <th colSpan={colSpan}>
                  {item2.regional && item2.regional.includes("_") ? item2.regional.replace(/_/gi, " ") : item2.regional}
                </th>
              </tr>

              <tr className={getClass}>
                <GetTdData item={item} name={item2.name} itemData={item2} gadgetsInfo={gadgetsInfo} />
              </tr>
            </React.Fragment>
          );
        })}
    </table>
  );
};

const GetTdData = ({ item, name, gadgetsInfo }) => {
  let gadgetsInfoCopy = [...gadgetsInfo];
  if (!isMobilePlatform()) {
    // To show empty tds
    const slotsToFill = gadgetsInfoCopy.length < 4 ? 4 - gadgetsInfoCopy.length : 0;
    const emptyCells = Array(slotsToFill).fill("EMPTY");
    // Concat original array with 'EMPTY'
    gadgetsInfoCopy = gadgetsInfoCopy.concat(emptyCells);
  }

  let starWidth = 0;
  return gadgetsInfoCopy
    ? gadgetsInfoCopy.map(data => {
      if (data === "EMPTY") {
        return <td />;
      }

      // delete data.specs[item].regional;
      if (data.specs[item]) {
        const feature = data.specs[item];
        // console.log('feature1111', feature, 'ss', name, feature[name]);
        if (feature && feature[name]) {
          if (name && name === "quick_charging") {
            return (
              <td>
                <span className={`charging-${feature[name].value.toLowerCase()}`}>{feature[name].value}</span>
              </td>
            );
          }
          if (name && name === "price") {
            return (
              <td>
                <span>₹ {feature[name].value}</span>
              </td>
            );
          }
          if (name && (name === "userrating" || name === "criticRating")) {
            starWidth = (feature[name].value / 5) * 100;
            const seoName = getKeyByValue(gadgetsConfig.gadgetMapping, data.category);
            return (
              <td>
                {feature[name].value !== "NA" ? (
                  <React.Fragment>
                    <span className="stars-in-value">
                      <b>{feature[name].value}</b>
                        /5
                      </span>
                    <span className="stars-in-rating">
                      <span className="empty-stars" />
                      <span
                        className="filled-stars critic"
                        style={{
                          width: `${starWidth}%`,
                        }}
                      />
                    </span>
                  </React.Fragment>
                ) : name === "criticRating" ? (
                  "NA"
                ) : (
                  ""
                )}

                {name === "userrating" && (
                  <span>
                    <AnchorLink
                      className="rt-submit"
                      href={`${process.env.WEBSITE_URL}tech/${seoName}/${data.productname}#rate`}
                    >
                      {siteConfig.locale.submitrating}
                    </AnchorLink>
                  </span>
                )}
              </td>
            );
          }
          return <td>{feature[name].value}</td>;
        }
        return <td>NA</td>;
      }

      return <td key={data}>NA</td>;
    })
    : "";
};

const DeviceSearched = ({
  data,
  gadgetCategory,
  deviceInfo,
  changed,
  keydown,
  focused,
  deviceAdded,
  deviceRemoved,
  statusMsg,
  getKeyByValue,
}) => {
  const inputCount = data && Array.isArray(data) ? 4 - data.length : "";
  const deviceCount = (data && Array.isArray(data) && data.length) || "";
  const inputCountArr = [];
  for (let i = 1; i <= inputCount; i++) {
    inputCountArr.push(i);
  }

  const deviceHTML =
    data && Array.isArray(data)
      ? data.map((item, index) => {
        const getregionalDevice =
          item &&
          item.regional &&
          Array.isArray(item.regional) &&
          item.regional.filter(data => data.host === siteConfig.hostid);
        const amzga = `${item.productname}_${item.price}`;
        const affiliateTags = gadgetsConfig.affiliateTags.CDSM;
        const tag = getAffiliateTags(affiliateTags);
        const price = item.price;
        const title = item.productname;
        const seoName = getKeyByValue(gadgetsConfig.gadgetMapping, item.category);
        const linkToItem = `${gadgetsConfig.techPrefix}/${seoName}/${item.productname}`;
        const finalBuyUrl = getBuyLink({
          data: item,
          tag,
        });

        const imgId = item.imageMsid && Array.isArray(item.imageMsid) ? item.imageMsid[0] : item.imageMsid;

        return (
          <li key={item.productname}>
            {deviceCount && deviceCount > 2 && !isMobilePlatform() && (
              // Hide close_icon for PWA
              <b
                onClick={deviceRemoved.bind(this, item.productname, gadgetCategory)}
                role="button"
                className="close_icon"
              />
            )}
            <AnchorLink href={linkToItem}>
              <span className="img_wrap">
                <ImageCard
                  size="gnthumb"
                  msid={imgId}
                  alt={item.productname}
                  title={item.productname}
                  noLazyLoad={index === 0}
                />
              </span>
              <span className="con_wrap">
                <h4 className="text_ellipsis">
                  {getregionalDevice && getregionalDevice[0] && getregionalDevice[0].name}
                </h4>
                {item.price && <span className="price_tag">&#8377; {item.price}</span>}
              </span>
            </AnchorLink>

            {finalBuyUrl && (
              <span className="btn_wrap">
                <a href={finalBuyUrl} rel="nofollow" target="_blank" className="btn-buy">
                  {techLocale.buyAtAmazon}
                </a>
              </span>
            )}
          </li>
        );
      })
      : "";

  return (
    <React.Fragment>
      <ul>
        {deviceHTML}

        {inputCountArr.length > 0 &&
          !isMobilePlatform() &&
          inputCountArr.map(itemc => (
            <ManageDevice
              key={itemc}
              deviceName={`device${itemc}`}
              gadgetCategory={gadgetCategory}
              deviceInfo={deviceInfo}
              changed={changed}
              keydown={keydown}
              focused={focused}
              deviceAdded={deviceAdded}
              statusMsg={statusMsg}
            />
          ))}
      </ul>
    </React.Fragment>
  );
};

const ManageDevice = ({
  deviceName,
  deviceInfo,
  gadgetCategory,
  changed,
  keydown,
  focused,
  deviceAdded,
  statusMsg,
}) => {
  const device = deviceInfo[deviceName];
  return (
    <li>
      <div className="input_field">
        <SvgIcon name="search" className="search_icon" />
        <input
          type="text"
          name={deviceName}
          value={(device && device.userInput) || ""}
          onChange={changed.bind(this, gadgetCategory)}
          onKeyDown={keydown}
          onFocus={focused}
          placeholder={techLocale.addDevice}
          autoComplete="off"
        />

        {device && !statusMsg && !device.userInputHtml && device.searchResult && device.searchResult.length > 0 && (
          <div className="auto-suggest">
            <ul id={`ulinput_${deviceName}`}>
              {device.searchResult.map((item, index) => (
                <li
                  key={item.Product_name}
                  data-item={item.Product_name}
                  onClick={deviceAdded.bind(this, item.seoname, deviceName, gadgetCategory)}
                  className={index === 0 ? "active" : ""}
                >
                  <span>{item.Product_name}</span>
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </li>
  );
};

ComparisonDetails.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params })).then(data => {
    const { payload } = data;
    const pwaMeta = payload && payload.compareData && payload.compareData.pwa_meta;
    updateConfig(pwaMeta, dispatch, setParentId);
    dispatch(setPageType("compareshow"));
  });
};

ComparisonDetails.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.compareDetail,
  };
}

export default connect(mapStateToProps)(ComparisonDetails);
