import React, { PureComponent } from "react";

import { connect } from "react-redux";
import PropTypes from "prop-types";

import { PageMeta } from "../../../../../components/common/PageMeta";

import { fetchDataIfNeeded } from "../../../../../actions/gn/comparision/Trends";
import { fetchPhotoDataIfNeeded, fetchVideoDataIfNeeded } from "../../../../../actions/gn/comparision/List/index";
import SectionLayoutMemo from "../../../home/SectionLayout";

import Breadcrumb from "../../../../../components/common/Breadcrumb";
import { _getStaticConfig, isMobilePlatform, updateConfig } from "../../../../../utils/util";
import GadgetsCompare from "../../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";
import "../../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../../components/common/css/desktop/gn_compare_list.scss";
import "../../../../../components/common/css/gn_compare_list.scss";
import { setIbeatConfigurations } from "../../../../../components/lib/analytics/src/iBeat";
import Ads_module from "../../../../../components/lib/ads/index";
import { setParentId } from "../../../../../actions/config/config";
import FakeListing from "../../../../../components/common/FakeCards/FakeListing";
import FakeListDesktop from "../../../../../components/common/FakeCards/FakeListDesktop";
import AdCard from "../../../../../components/common/AdCard";

// import gadgetsConfig from "../../../../../utils/gadgetsConfig";
const siteConfig = _getStaticConfig();
const deviceText = (siteConfig && siteConfig.locale && siteConfig.locale.tech) || {};

class TrendsComparison extends PureComponent {
  constructor(props) {
    super(props);
    //let pathname = props.pathname.split('/');
    //pathname = pathname.slice(-1).toString();
    //const urlValue = pathname || '';
  }

  componentDidMount() {
    const { dispatch, query, params, data, location, trendsComparision } = this.props;
    let pathname = location.pathname.split("/");
    pathname = pathname.slice(-1).toString();

    //console.log('props', pathArr);

    TrendsComparison.fetchData({ dispatch, query, params, pathname }).then(response => {
      let pwa_meta = null;
      if (trendsComparision && trendsComparision.data !== null) {
        pwa_meta = trendsComparision.data.compareData && trendsComparision.data.compareData.pwa_meta;
      } else if (response) {
        pwa_meta =
          response && response.payload && response.payload.compareData && response.payload.compareData.pwa_meta;
      }
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    if (!isMobilePlatform()) {
      dispatch(fetchVideoDataIfNeeded({ query, params }));
      dispatch(fetchPhotoDataIfNeeded({ query, params }));
    }
    // dispatch(
    //   updateNavData({
    //     sectionName: "compare",
    //   }),
    // );
  }

  componentDidUpdate(prevProps, prevState) {
    const { dispatch, query, params, location, trendsComparision } = this.props;
    //let pathname = location.pathname.split('/');
    //pathname = pathname.slice(-1);
    if (prevProps.location.pathname !== location.pathname) {
      const { data } = this.props;
      let pathname = location.pathname.split("/");
      pathname = pathname.slice(-1).toString();

      TrendsComparison.fetchData({ dispatch, query, params, pathname }).then(response => {
        let pwa_meta = null;
        if (trendsComparision && trendsComparision.data !== null) {
          pwa_meta = trendsComparision.data.compareData && trendsComparision.data.compareData.pwa_meta;
        } else if (response) {
          pwa_meta =
            response && response.payload && response.payload.compareData && response.payload.compareData.pwa_meta;
        }
        if (pwa_meta && typeof pwa_meta == "object") {
          //set section and subsection in window
          Ads_module.setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      });
    }
  }

  render() {
    //  console.log("Render", this.props);
    const { data, location, params, videoData, photoData, trendsComparision } = this.props;
    // console.log("Render", this.props);
    let pathname = location.pathname.split("/");
    pathname = pathname.slice(-1).toString();
    const catName = pathname === "popular-comparisons" ? deviceText.popular : deviceText.latest;
    const deviceDataCopy =
      trendsComparision && trendsComparision.data && trendsComparision.data.compareData
        ? { ...trendsComparision.data.compareData }
        : "";
    const gadgetCategory = (deviceDataCopy && deviceDataCopy.compare && deviceDataCopy.compare[0].category) || "";
    // const gadgetCategory = params && params.device ? gadgetsConfig[params.device] : "";
    const seoName = params && params.device;
    let arrComparedDevices = [];

    if (deviceDataCopy && deviceDataCopy.compare) {
      if (isMobilePlatform()) {
        const arrCompare = [...deviceDataCopy.compare];
        arrComparedDevices = arrCompare.filter(item => item.product_name.length < 4);
        // arrCompare.forEach(item => {
        //   if (item && item.product_name && item.product_name.length < 4) {
        //     arrComparedDevices.push(item);
        //   }
        // });
      } else {
        arrComparedDevices = [...deviceDataCopy.compare];
      }
    }

    return (
      <React.Fragment>
        {deviceDataCopy && deviceDataCopy.breadcrumb && deviceDataCopy.breadcrumb.div && !isMobilePlatform() && (
          <Breadcrumb items={deviceDataCopy.breadcrumb.div.ul} />
        )}
        {deviceDataCopy && deviceDataCopy.pwa_meta && PageMeta(deviceDataCopy.pwa_meta)}

        <div className={`container-comparewidget ${isMobilePlatform() ? "pwa" : "web"}`}>
          {deviceDataCopy ? (
            <div className="row wdt_popular_items box-item">
              <div className="section">
                <div className="top_section">
                  <h1>
                    <span>{`${siteConfig.locale.tech.recentlyCompared}`}</span>
                  </h1>
                </div>
              </div>

              {arrComparedDevices && (
                <div className={`col12 ui_slider ${!isMobilePlatform() ? "pd0" : ""}`}>
                  <GadgetsCompare
                    type="popular"
                    linksData={arrComparedDevices}
                    category={gadgetCategory}
                    seoName={seoName}
                    deviceIcon="true"
                  />
                </div>
              )}
            </div>
          ) : isMobilePlatform() ? (
            <FakeListing />
          ) : (
            <FakeListDesktop />
          )}

          <AdCard adtype="ctn" mstype="ctnTechTC" />

          {!isMobilePlatform() && videoData && videoData.section && (
            <div className="row">
              <div className="col12">
                <div className="wdt_highlight video">
                  <SectionLayoutMemo
                    key="1_slider"
                    datalabel={"slider"}
                    data={videoData.section.newsItem}
                    sliderObj={{ size: "4" }}
                  />
                </div>
              </div>
            </div>
          )}

          {!isMobilePlatform() && photoData && photoData.section && (
            <div className="row">
              <div className="col12">
                <div className="wdt_highlight">
                  <SectionLayoutMemo
                    key="1_slider"
                    datalabel={"slider"}
                    data={photoData.section.newsItem}
                    sliderObj={{ size: "4" }}
                  />
                </div>
              </div>
            </div>
          )}
          {deviceDataCopy && deviceDataCopy.breadcrumb && deviceDataCopy.breadcrumb.div && isMobilePlatform() && (
            <Breadcrumb items={deviceDataCopy.breadcrumb.div.ul} />
          )}
        </div>
      </React.Fragment>
    );
  }
}

TrendsComparison.fetchData = function fetchData({ dispatch, query, params, router }) {
  // console.log("router", router);
  return dispatch(fetchDataIfNeeded({ query, params, router }));
};

TrendsComparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  const { trendsComparision, comparisionList } = state;
  return {
    ...state,
    ...trendsComparision,
    ...comparisionList,
  };
}

export default connect(mapStateToProps)(TrendsComparison);
