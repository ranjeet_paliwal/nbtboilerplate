/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */
import React, { PureComponent } from "react";

import { connect } from "react-redux";
import PropTypes, { array } from "prop-types";
import { browserHistory } from "react-router";

import AnchorLink from "../../../../../components/common/AnchorLink";
import {
  fetchDataIfNeeded,
  fetchPhotoDataIfNeeded,
  fetchVideoDataIfNeeded,
  updateGadgetData,
} from "../../../../../actions/gn/comparision/List/index";
import GadgetSlider from "../../../../../components/desktop/Slider/Gadget";
import Slider from "../../../../../components/desktop/Slider";
import "../../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../../components/common/css/desktop/gn_compare_list.scss";
import gadgetsConfig from "../../../../../utils/gadgetsConfig";
import GadgetsCompare from "../../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";
import { setParentId, setPageType } from "../../../../../actions/config/config";

const gadgetMapping = (gadgetsConfig && gadgetsConfig.gadgetMapping) || "";

import {
  _getStaticConfig,
  getKeyByValue,
  isMobilePlatform,
  getAffiliateUrl,
  getBuyLink,
  updateConfig,
  getAffiliateTags,
} from "../../../../../utils/util";

import { setSectionDetail, recreateAds, refreshAds } from "../../../../../components/lib/ads";

import { getDeviceObject } from "../../../../utils/gadgets_util";
import SearchModule from "../../../../../components/common/Gadgets/SearchModule/SearchModule";
import SectionLayoutMemo from "../../../home/SectionLayout";
import Ads_module from "./../../../../../components/lib/ads/index";
import Breadcrumb from "../../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../../components/lib/errorboundery/ErrorBoundary";
import SectionHeader from "../../../../../components/common/SectionHeader/SectionHeader";
import { PageMeta } from "../../../../../components/common/PageMeta";
import SvgIcon from "../../../../../components/common/SvgIcon";
import { setIbeatConfigurations } from "../../../../../components/lib/analytics/src/iBeat";
import CategoryList from "../../../../../components/common/Gadgets/CategoryList/CategoryList";
import AdCard from "../../../../../components/common/AdCard";

const siteConfig = _getStaticConfig();
const deviceText = (siteConfig && siteConfig.locale && siteConfig.locale.tech.gadgetSliderConfig) || {};
class Comparison extends PureComponent {
  constructor(props) {
    super(props);
    const activeDevice =
      props.routeParams && props.routeParams.device ? props.routeParams.device : props.location.query.category;
    const deviceToSearch = activeDevice ? gadgetMapping && gadgetMapping[activeDevice] : "mobile";
    const { router } = props;
    const queryParam = router && router.location && router.location.query;
    let updatedDeviceInfo = "";

    if (queryParam && queryParam.device) {
      updatedDeviceInfo = getDeviceObject();
      updatedDeviceInfo.device1.userInputHtml = queryParam.device;
      updatedDeviceInfo.device1.seoName = queryParam.name;
    }

    this.state = {
      deviceData: "",
      suggestedDevices: "",
      activeGadget: deviceToSearch,
      addedDevicefromSuggestion: "",
      updatedDeviceInfo: updatedDeviceInfo,
    };
  }

  componentDidMount() {
    const { dispatch, query, params, comparisionList, location } = this.props;
    dispatch(setPageType("comparelist"));

    Comparison.fetchData({ dispatch, query, params }).then(response => {
      const { data } = comparisionList;
      const pwa_meta = data && data.listData && data.listData.compareData && data.listData.compareData.pwa_meta;
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });
    Comparison.fetchVideoData({ dispatch, query, params });
    Comparison.fetchPhotoData({ dispatch, query, params });
  }

  componentDidUpdate(prevProps, prevState) {
    const { dispatch, query, params, comparisionList, location } = this.props;
    const { data } = comparisionList;
    const pwa_meta = data && data.listData && data.listData.compareData && data.listData.compareData.pwa_meta;
    if (pwa_meta && typeof pwa_meta == "object") {
      //set section and subsection in window
      Ads_module.setSectionDetail(pwa_meta);
      //fire ibeat
      pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      updateConfig(pwa_meta, dispatch, setParentId);
    }
    if (location && prevProps && location.pathname != prevProps.location.pathname) {
      setTimeout(() => {
        Ads_module.refreshAds(["btf"]);
      }, 2000);
    }
  }

  preventDefaultAndPropagation = e => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    if (e.preventDefault) {
      e.preventDefault();
    }
  };

  addFromSuggestions = deviceData => e => {
    this.preventDefaultAndPropagation(e);
    let { addedDevicefromSuggestion } = this.state;

    addedDevicefromSuggestion = {
      Product_name: deviceData.name,
      seoname: deviceData.uname,
    };
    this.setState({
      addedDevicefromSuggestion,
    });
  };

  switchDeviceHandler = obj => {
    obj.preventDefault();
    const deviceType = obj.currentTarget.id;

    const { deviceData } = this.state;
    const { dispatch } = this.props;

    const seoName = this.getKeyByValue(deviceType);
    this.props.router.push(`/tech/compare-${seoName}`);

    this.setState({
      activeGadget: deviceType,
      deviceData,
      suggestedDevices: "",
    });

    dispatch(updateGadgetData(deviceType));
  };
  onGadgetItemClick = item => e => {
    this.preventDefaultAndPropagation(e);
    const { activeGadget } = this.state;
    const { tab } = this.state;
    const seoName = this.getKeyByValue(activeGadget);
    window.location.href = `/tech/${seoName}/${item.lcuname}`;
  };

  getKeyByValue = value => {
    const gadgets = gadgetMapping;
    for (const key in gadgets) {
      if (Object.prototype.hasOwnProperty.call(gadgets, key)) {
        if (gadgets[key] === value) return key;
      }
    }
    return "";
  };

  onAmazonBuyButtonClick = card => e => {
    this.preventDefaultAndPropagation(e);
    // let amazonCard;
    // let prodUrl;
    // if (card.affiliate.exact) {
    //   amazonCard = card.affiliate.exact[0];
    // } else if (card.affiliate.related) {
    //   amazonCard = card.affiliate.related[0];
    // }
    const amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.CL);
    // const affiliateUrl = getAffiliateUrl(amazonCard, amazonTag);
    const affiliateUrl = getBuyLink({ data: card, tag: amazonTag });
    const win = window.open(affiliateUrl, "_blank");
    win.focus();
  };

  suggestedDevicesCallBack = (item, deviceListInfo) => {
    let { suggestedDevices, updatedDeviceInfo } = this.state;
    suggestedDevices = item;
    updatedDeviceInfo = deviceListInfo;
    this.setState({
      suggestedDevices,
      updatedDeviceInfo,
    });
  };

  render() {
    const { comparisionList, alaskaData, router } = this.props;

    const { activeGadget, suggestedDevices, addedDevicefromSuggestion, updatedDeviceInfo } = this.state;

    const seoName = this.getKeyByValue(activeGadget);

    const prepareDeviceURL = `${process.env.API_ENDPOINT}/tech/compare-${seoName}`;

    const { videoData, photoData, data } = comparisionList;

    const objPhotoData = photoData && photoData.section && photoData.section.newsItem;
    const objVideoData = videoData && videoData.section && videoData.section.newsItem;

    const deviceDataCopy = data && data.listData && data.listData.compareData ? { ...data.listData.compareData } : "";

    const breadcrumb = data && data.listData && data.listData.compareData && data.listData.compareData.breadcrumb;
    const pwa_meta = deviceDataCopy && deviceDataCopy.pwa_meta;

    const arrPopularTwoDevices = [];
    const arrPopularOtherThanTwo = [];
    const arrLatestTwoDevices = [];
    const arrLatestOtherThanTwo = [];
    let categoryInFeed = "";

    if (deviceDataCopy && deviceDataCopy.popularGadgetPair && deviceDataCopy.popularGadgetPair.compare) {
      const arrPopular = [...deviceDataCopy.popularGadgetPair.compare];
      categoryInFeed = arrPopular && arrPopular[0] && arrPopular[0].category;

      arrPopular.forEach(item => {
        // console.log(item);
        if (item && item.product_name && item.product_name.length === 2 && arrPopularTwoDevices.length <= 2) {
          arrPopularTwoDevices.push(item);
        } else {
          arrPopularOtherThanTwo.push(item);
        }
      });
    }

    if (deviceDataCopy && deviceDataCopy.recentcomp && deviceDataCopy.recentcomp.compare) {
      const arrLatest = [...deviceDataCopy.recentcomp.compare];

      arrLatest.forEach(item => {
        if (item && item.product_name && item.product_name.length === 2 && arrLatestTwoDevices.length <= 2) {
          arrLatestTwoDevices.push(item);
        } else {
          arrLatestOtherThanTwo.push(item);
        }
      });
    }

    const regionalDeviceName = categoryInFeed ? deviceText[categoryInFeed] : "";

    return (
      <React.Fragment>
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        {breadcrumb && (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        )}

        <div className="container-comparewidget web">
          <div className="wdt_compare-gadgets">
            <CategoryList
              clicked={this.switchDeviceHandler.bind(this)}
              activeGadget={activeGadget}
              pagetype="compare"
            />
            <h1>
              {`${regionalDeviceName} ${siteConfig.locale.tech.compare}`}
              <span className="search-suggest"> ({siteConfig.locale.tech.englishtype}) </span>
            </h1>
            <div className="row-fields">
              <SearchModule
                deviceInfo={updatedDeviceInfo}
                activeGadget={activeGadget}
                suggestedDevicesCallBack={this.suggestedDevicesCallBack}
                addedSuggestedDevice={addedDevicefromSuggestion}
              />
            </div>
          </div>
          {suggestedDevices && suggestedDevices.gadget && (
            <div className="wdt_suggested_slider ui_slider">
              <SectionHeader
                sectionhead={`${siteConfig.locale.tech.suggestedDevice} ${deviceText[activeGadget]}
                      ${siteConfig.locale.tech.ki} ${siteConfig.locale.tech.comparetxt}`}
                weblink={""}
              />
              <GadgetSlider
                type="comparisonGadgets"
                size="5"
                sliderData={suggestedDevices.gadget}
                width="182"
                sliderClass="trendingslider"
                videoIntensive={false}
                gadgetItemClick={this.onGadgetItemClick}
                amazonBuyClick={this.onAmazonBuyButtonClick}
                margin="10"
                addFromSuggestions={this.addFromSuggestions}
                isComparison={true}
              />
            </div>
          )}

          <div className="wdt_popular_slider ui_slider">
            <GadgetsCompare
              type="popular"
              data={arrPopularTwoDevices}
              linksData={arrPopularOtherThanTwo}
              category={activeGadget}
              seoName={seoName}
              regionalDeviceName={regionalDeviceName}
            />
          </div>

          <div className="wdt_lstcomparsion_slider ui_slider">
            <GadgetsCompare
              type="latest"
              data={arrLatestTwoDevices}
              linksData={arrLatestOtherThanTwo}
              category={activeGadget}
              seoName={seoName}
              regionalDeviceName={regionalDeviceName}
            />
          </div>
          {videoData && videoData.items && (
            <div className="row">
              <div className="col12">
                <div className="wdt_highlight video">
                  {/* <SectionLayoutMemo
                    key="1_slider"
                    datalabel={"slider"}
                    data={videoData.section.newsItem}
                    sliderObj={{ size: "4" }}
                    alaskaData={alaskaData}
                  /> */}

                  <SectionHeader
                    sectionhead={videoData.secname}
                    weblink={videoData.override}
                    morelink={videoData.override}
                  />

                  <Slider type="grid" size="4" width="221" margin="20" sliderData={videoData.items} />
                </div>
              </div>
            </div>
          )}

          {photoData && photoData.items && (
            <div className="row">
              <div className="col12">
                <div className="wdt_highlight">
                  <SectionHeader
                    sectionhead={photoData.secname}
                    weblink={photoData.override}
                    morelink={photoData.override}
                  />

                  <Slider type="grid" size="4" width="221" margin="20" sliderData={photoData.items} />

                  {/* <SectionLayoutMemo
                    key="1_slider"
                    datalabel={"slider"}
                    data={photoData.section.newsItem}
                    sliderObj={{ size: "4" }}
                    alaskaData={alaskaData}
                  /> */}
                </div>
              </div>
            </div>
          )}

          <div>
            <AdCard mstype="btf" adtype="dfp" />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

Comparison.fetchData = function fetchData({ dispatch, query, params }) {
  dispatch(setPageType("comparelist"));

  return dispatch(fetchDataIfNeeded({ query, params }));
};

Comparison.fetchVideoData = function fetchVideoData({ dispatch, query, params }) {
  return dispatch(fetchVideoDataIfNeeded({ query, params }));
};

Comparison.fetchPhotoData = function fetchVideoData({ dispatch, query, params }) {
  return dispatch(fetchPhotoDataIfNeeded({ query, params }));
};

Comparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    comparisionList: state.comparisionList,
    alaskaData: state.header.alaskaData,
    home: state.home,
  };
}

export default connect(mapStateToProps)(Comparison);
