/* eslint-disable indent */
/* eslint-disable no-underscore-dangle */
import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import Slider from "../../../../components/desktop/Slider/Gadget";
import GeneralSlider from "../../../../components/desktop/Slider/index";
import GadgetVariants from "../../../../components/common/Gadgets/VariantWidgets";
import SpecificationAccordion from "./SpecificationAccordion";
import "../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../components/common/css/desktop/gn_productDetails.scss";
import { Link } from "react-router";
import LatestTrendingWidget from "../../../../components/common/Gadgets/LatestTrendingWidget";
import GadgetsCompare from "../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";
import Ads_module from "../../../../components/lib/ads/index";

import {
  getBuyLink,
  objToQueryStr,
  _getCookie,
  _getStaticConfig,
  formatMoney,
  getKeyByValue,
  getAffiliateTags,
  isLoggedIn,
  updateConfig,
} from "../../../../utils/util";
import { getGadgetPrice } from "../../../utils/gadgets_util";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import AnchorLink from "../../../../components/common/AnchorLink";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import GadgetsRating from "../../../../components/common/Gadgets/GadgetRatingWidget";
import AmazonAffiliate from "../../../../components/common/Gadgets/AmazonAffiliate/AmazonAffiliate";
import ArticlesListBlock from "../../../../components/common/Gadgets/ArticleListBlock";
import AdCard from "../../../../components/common/AdCard";
import Wdt2GudAffiliatesList from "../../../../components/common/Gadgets/Wdt2GudAffiliatesList/Wdt2GudAffiliatesList";
import WdtAmazonDetail from "../../../../components/common/Gadgets/WdtAmazonDetail/WdtAmazonDetail";
import SocialShare from "../../../../components/common/SocialShare";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import { connect } from "react-redux";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import { setParentId } from "../../../../actions/config/config";
const gadgetCategories = (gadgetsConfig && gadgetsConfig.gadgetCategories) || "";

const Config = _getStaticConfig();

class GadgetShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: "OVERVIEW",
      commentsData: null,
      currentImageIndex: 0,
      isCommentClicked: false,
      readMoreExpanded: false,
      showReviewCard: false,
    };
  }

  componentDidMount() {
    const { data, dispatch } = this.props;
    const pwa_meta = data && data && data.pwa_meta;
    // for scroll top from as page
    const hashValue = window.location.hash;
    if (hashValue) {
      const spsId = hashValue.replace("#", "");
      const scrollToElement = document.getElementById(spsId);
      if (scrollToElement) {
        window.scrollTo({ top: scrollToElement.offsetTop });
      }
      // console.log('hashvalue', hashValue);
    }
    if (data && data.article) {
      const commentsParams = Object.assign({}, Config.Comments.allComments, {
        msid: data.article.msid,
      });

      fetch(`${Config.mweburl}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
        .then(res => res.json())
        .then(commentsData => {
          if (pwa_meta && typeof pwa_meta === "object") {
            // set section and subsection in window
            Ads_module.setSectionDetail(pwa_meta);
            // fire ibeat
            pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
            updateConfig(pwa_meta, dispatch, setParentId);
          }
          this.setState({
            commentsData,
          });
        })
        .catch(e => {
          this.setState({
            commentsData: null,
          });
        });
    }
  }

  componentDidUpdate(prevProps) {
    const { data, dispatch } = this.props;
    const pwa_meta = data && data && data.pwa_meta;
    if (!prevProps.data && data) {
      if (data && data.article) {
        const commentsParams = Object.assign({}, Config.Comments.allComments, {
          msid: data.article.msid,
        });

        fetch(`${Config.mweburl}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
          .then(res => res.json())
          .then(commentsData => {
            if (pwa_meta && typeof pwa_meta === "object") {
              // set section and subsection in window
              Ads_module.setSectionDetail(pwa_meta);
              // fire ibeat
              pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
              updateConfig(pwa_meta, dispatch, setParentId);
            }
            this.setState({
              commentsData,
            });
          })
          .catch(e => {
            this.setState({
              commentsData: null,
            });
          });
      }
    }
  }

  // Default values mobile and recently reviewed data

  closeIfOnOverlay = e => {
    if (e.target.id === "comments__body") {
      return false;
    }
    this.closeCommentSlider();
    return false;
  };

  closeCommentSlider = () => {
    this.setState({
      isCommentClicked: false,
    });

    document.body.classList.remove("disable-scroll");
  };

  showComments = () => {
    this.setState({
      isCommentClicked: true,
    });
    document.body.classList.add("disable-scroll");
  };

  openCommentsPopup = () => {
    // this.preventDefaultAndPropagation(e);
    this.setState({
      isCommentClicked: true,
    });
  };

  // eslint-disable-next-line react/sort-comp
  getReviewCard = () => {
    this.setState({
      showReviewCard: true,
    });
  };

  toggleReadMore = () => {
    const { readMoreExpanded } = this.state;
    this.setState({
      readMoreExpanded: !readMoreExpanded,
    });
  };

  scrollToSection = e => {
    const { data } = this.props;
    const gadgetData = data.gadget && data.gadget[0] && data.gadget[0];
    const targetId = e.target.id || e.target.parentNode.id || "";
    if (targetId) {
      const scrollToElement = document.getElementById(`${targetId}_${gadgetData ? gadgetData.uname : ""}`);
      if (scrollToElement) {
        window.scrollTo({ top: scrollToElement.offsetTop });
      }
    }
  };

  closeReviewCard = () => {
    this.setState({
      showReviewCard: false,
    });
  };

  openHoveredImage = currentImageIndex => {
    if (currentImageIndex !== this.state.currentImageIndex) {
      this.setState({
        currentImageIndex,
      });
    }
  };

  scrollToElementTop = (elemId, changeHash = false, setTimeout = false) => {
    if (changeHash) {
      window.history.replaceState({}, "", changeHash);
    }
    const elem = document.getElementById(elemId);
    if (elem) {
      if (!setTimeout) {
        elem.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
      } else {
        setTimeout(() => elem.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" }), 1000);
      }
    }
  };

  preventDefaultAndPropagation = e => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    if (e.preventDefault) {
      e.preventDefault();
    }
  };

  onGadgetItemClick = item => e => {
    this.preventDefaultAndPropagation(e);
    const { data } = this.props;
    const gadgetData = (data.gadget && data.gadget[0] && data.gadget[0]) || null;
    window.location.href = `${Config.mweburl}/tech/${gadgetCategories[gadgetData && gadgetData.category]}/${
      item.lcuname
    }`;
  };

  render() {
    const {
      data,
      category,
      authentication,
      fixedNav,
      trendingRightData,
      currentTab,
      showLoginRegister,
      latestTrendingData,
      isLastGadget,
      gadgetshow,
    } = this.props;

    const { isFetching } = gadgetshow;

    const { showReviewCard, currentImageIndex, readMoreExpanded, commentsData, isCommentClicked } = this.state;
    if (!data) {
      return null;
    }

    let mainImageId;

    if (data && Array.isArray(data.gadget)) {
      if (Array.isArray(data.gadget[0].imageMsid)) {
        mainImageId = data.gadget[0].imageMsid[currentImageIndex];
      } else if (typeof data.gadget[0].imageMsid === "string" && data.gadget[0].imageMsid) {
        mainImageId = data.gadget[0].imageMsid;
      }
    }

    const gadgetData = (data.gadget && data.gadget[0] && data.gadget[0]) || null;
    const getRegionaldata = [].concat(gadgetData && gadgetData.regional);

    const secData =
      getRegionaldata &&
      Array.isArray(getRegionaldata) &&
      getRegionaldata.filter(data => data && data.host === Config.hostid);
    const getregionalreview =
      secData && secData[0] && secData[0].reviewid && secData[0].reviewid.msid ? secData[0].reviewid.msid : "";
    let isReviewDisabled = false;

    if (typeof window !== "undefined" && window.localStorage && gadgetData) {
      const reviewAndLoginIds = localStorage.getItem("reviewAndLoginIds");
      const ssoid = _getCookie("ssoid");
      if (reviewAndLoginIds && Array.isArray(JSON.parse(reviewAndLoginIds)) && ssoid) {
        if (JSON.parse(reviewAndLoginIds).includes(`${getregionalreview}-${ssoid}`)) {
          isReviewDisabled = true;
        }
      }
    }

    let sliderClass = "";
    const productTitle = secData && secData[0] && secData[0].name ? secData[0].name : "";
    const h1Value = data && data.h2 ? data.h2 : productTitle;
    const textval = "";

    const amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.GS_RHS_SPONS);

    if (gadgetData && (parseInt(gadgetData.upcoming) || parseInt(gadgetData.rumoured))) {
      if (parseInt(gadgetData.upcoming) && parseInt(gadgetData.rumoured)) {
        sliderClass = "rumoured";
      } else {
        sliderClass = parseInt(gadgetData.upcoming) ? "upcoming" : "rumoured";
      }
    }

    const sortPrice = getGadgetPrice(gadgetData);
    const sortPriceSchema = sortPrice.replace(/[₹, ]/g, ""); // replaces space, ₹ and comma

    let priceTableInfo = "";
    if (gadgetData && gadgetData.affiliate) {
      const affiliateData = gadgetData.affiliate;
      // if (affiliateData && affiliateData.exact && affiliateData.exact.items) {
      //   sortPrice = affiliateData.exact.items.sort_price;
      // } else if (affiliateData && affiliateData.related && affiliateData.related[0]) {
      //   sortPrice = affiliateData.related[0].sort_price;
      // }

      if (affiliateData.exact) {
        if (affiliateData.exact.items) {
          priceTableInfo = affiliateData.exact.items;
        } else {
          priceTableInfo = affiliateData.exact[0];
        }
      }
    }

    let updatedDate = "";
    if (gadgetData && gadgetData.affiliate && gadgetData.affiliate.related) {
      const relatedNode = gadgetData.affiliate.related;
      if (relatedNode && relatedNode.items) {
        updatedDate = relatedNode.items.updatedAt;
      } else if (relatedNode && relatedNode[0]) {
        updatedDate = relatedNode[0].updatedAt;
      }
    }

    const arrTwoDevices = [];
    const arrOtherThanTwo = [];

    if (data && data.popularGadgetPair && data.popularGadgetPair.compare) {
      const arrPopular = [...data.popularGadgetPair.compare];

      arrPopular.forEach(item => {
        if (item && item.product_name && item.product_name.length === 2 && arrTwoDevices.length < 2) {
          arrTwoDevices.push(item);
        } else {
          arrOtherThanTwo.push(item);
        }
      });
    }

    const tagSeeAlso = getAffiliateTags(gadgetsConfig.affiliateTags.GS_SEEALSO);

    return (
      <div style={{ overflow: "hidden" }}>
        <div className="productdetails_body" id={`pdp-${gadgetData && gadgetData.lcuname}`}>
          <h1>{h1Value}</h1>
          <div className="row">
            <div className={`col12 pd_top_nav`}>
              <div className="nav_items">
                <ul className="tabs" onClick={this.scrollToSection}>
                  <li id="overview" className={currentTab === "overview" ? "active" : ""}>
                    <span>{Config.locale.tech.overview}</span>
                  </li>
                  {data && data.article && parseInt(data.article.isMarkedReview) ? (
                    <li id="critic_review" className={currentTab === "critic_review" ? "active" : ""}>
                      <span>{Config.locale.tech.criticReview}</span>
                    </li>
                  ) : (
                    ""
                  )}

                  <li id="user_review" className={currentTab === "user_review" ? "active" : ""}>
                    <span>{Config.locale.tech.userReview}</span>
                  </li>
                  <li id="specification" className={currentTab === "specification" ? "active" : ""}>
                    <span>{Config.locale.tech.specifications}</span>
                  </li>
                </ul>
              </div>
              <SocialShare openCommentsPopup={this.openCommentsPopup} />
            </div>
            <div className="col8">
              <div className="page_content">
                <div className="product_summary" id={`overview_${gadgetData ? gadgetData.uname : ""}`}>
                  {/* <Slider /> */}
                  <div className="product_slider">
                    <ul className="slider-tabs">
                      {data && Array.isArray(data.gadget) && data.gadget[0] && Array.isArray(data.gadget[0].imageMsid)
                        ? data.gadget[0].imageMsid.slice(0, 4).map((imageData, index) => (
                            <li
                              className={index === currentImageIndex ? "active" : ""}
                              key={imageData}
                              onMouseOver={() => this.openHoveredImage(index)}
                            >
                              <ImageCard msid={imageData} size="gnthumb" title={imageData} />
                            </li>
                          ))
                        : null}
                    </ul>
                    <div className={`slider-content ${sliderClass}`}>
                      <span className="top-caption" data-attr={textval}>
                        {Config.locale.tech.rumoured}
                        <b />
                      </span>
                      <div className="img_wrap">
                        <ImageCard
                          msid={mainImageId}
                          alt={
                            data &&
                            Array.isArray(data.gadget) &&
                            data.gadget[0] &&
                            Array.isArray(data.gadget[0].imageMsid)
                              ? data.gadget[0].imageMsid[currentImageIndex]
                              : "GadgetsNow"
                          }
                          noLazyLoad
                          title={
                            data &&
                            Array.isArray(data.gadget) &&
                            data.gadget[0] &&
                            Array.isArray(data.gadget[0].imageMsid)
                              ? data.gadget[0].imageMsid[currentImageIndex]
                              : "GadgetsNow"
                          }
                          size="gnthumb"
                        />
                      </div>

                      {gadgetData ? (
                        <div className="tabs_wrap">
                          {/*  TODO: Add links to correct sections here */}
                          <AnchorLink
                            href={`/tech/compare-${gadgetCategories[gadgetData.category]}?device=${
                              gadgetData.name
                            }&name=${gadgetData.uname}`}
                            className="active"
                          >
                            {Config.locale.tech.comparedotxt}
                          </AnchorLink>

                          <span onClick={() => this.scrollToElementTop(`more_like_this_${gadgetData.uname}`)}>
                            {Config.locale.tech.moreLikeThis}
                          </span>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  <span
                    className={`summary_content ${readMoreExpanded ? "" : "less"}`}
                    dangerouslySetInnerHTML={{ __html: data && data.story }}
                  />
                  {data && data.story && data.story.length > 500 ? (
                    <div className="rd_more" onClick={this.toggleReadMore}>
                      <span>{readMoreExpanded ? Config.locale.tech.readLess : Config.locale.readmore}</span>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                {data &&
                Array.isArray(data.gadget) &&
                data.gadget[0] &&
                (Array.isArray(data.gadget[0].variants) || data.gadget[0].variants) ? (
                  <GadgetVariants
                    currentlyActive={data.gadget[0].common_uname}
                    gadgetCategory={category}
                    data={{ variants: [].concat(data.gadget[0].variants) }}
                  />
                ) : null}

                {data && gadgetData && data.article && parseInt(data.article.isMarkedReview) ? (
                  <CriticReview
                    data={data.article}
                    gadgetId={gadgetData.uname}
                    averageRating={gadgetData.averageRating}
                    criticRating={data.criticRating}
                  />
                ) : (
                  ""
                )}

                {data && data.productid ? (
                  <ErrorBoundary>
                    <WdtAmazonDetail
                      affview="3"
                      type="gadgetshow"
                      productid={data.productid}
                      noimg="0"
                      msid={data.article.msid}
                      title={Config.locale.tech.seemore}
                      tag={tagSeeAlso}
                    />
                  </ErrorBoundary>
                ) : (
                  ""
                )}

                {data && data.twogud ? (
                  <ErrorBoundary>
                    <Wdt2GudAffiliatesList
                      twogudData={data.twogud}
                      brandName={data.twogud.brandname}
                      price={data.twogud.price}
                      productName={data.twogud.productname}
                      tag={gadgetsConfig.affiliateTags.PDP}
                      msid={data.id}
                    />
                  </ErrorBoundary>
                ) : (
                  ""
                )}
                {data && data.relatedgadgets && Array.isArray(data.relatedgadgets.gadget) ? (
                  <div
                    className="pd_suggested_slider ui_slider"
                    id={`more_like_this_${gadgetData ? gadgetData.uname : ""}`}
                  >
                    <div className="section">
                      <div className="top_section">
                        <h2>
                          <span>{Config.locale.tech.similarGadgets}</span>
                        </h2>
                      </div>
                    </div>
                    <Slider
                      type="gadgetShow"
                      sliderData={data.relatedgadgets.gadget}
                      size="4"
                      width="150"
                      margin="15"
                      sliderClass="trendingslider"
                      isGadgetShow
                      videoIntensive={false}
                      gadgetItemClick={this.onGadgetItemClick}
                    />
                  </div>
                ) : (
                  ""
                )}
                {data && data.gadget[0] && data.gadget[0].specs ? (
                  <div id={`specification_${gadgetData.uname}`}>
                    <SpecificationAccordion
                      heading={h1Value}
                      data={data && data.gadget[0] && data.gadget[0].specs}
                      price={sortPrice}
                    />
                  </div>
                ) : null}
                {/* {Config.gadgetShow.popularComparison} */}

                {arrTwoDevices.length > 0 && (
                  <GadgetsCompare
                    type="popular"
                    data={arrTwoDevices}
                    linksData={arrOtherThanTwo}
                    category={gadgetData && gadgetData.category}
                    seoName={category}
                    cssClass="wdt_popular_slider ui_slider"
                  />
                )}

                {priceTableInfo && data && (
                  <div className="price-widget gn_table_layout">
                    <div className="section">
                      <div className="top_section">
                        <h2>
                          <span>{`${data.gadget && data.gadget[0] && data.gadget[0].name} ${Config.locale.tech.ki} ${
                            Config.locale.tech.indiaPrice
                          }`}</span>
                        </h2>
                      </div>
                    </div>
                    <table>
                      <tr>
                        <td>STORE</td>
                        <td>PRODUCT NAME</td>
                        <td>OFFER PRICE</td>
                        {priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice && <td>ACTUAL PRICE</td>}
                      </tr>
                      <tr>
                        <td>{priceTableInfo.Identifier}</td>
                        <td>{priceTableInfo.name}</td>
                        <td>{priceTableInfo.LowestNewPrice && priceTableInfo.LowestNewPrice.FormattedPrice}</td>
                        <td>{priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice}</td>
                      </tr>
                    </table>
                  </div>
                )}
                {data && data.relarticle && data.relarticle.items && (
                  <div className="row">
                    <div className="col12">
                      <div className="wdt_highlight video">
                        <SectionHeader sectionhead={data.relarticle.secname} weblink={data.relarticle.wu} />
                        <GeneralSlider
                          type="grid"
                          size="3"
                          width="190"
                          margin="20"
                          sliderData={data.relarticle.items}
                          //parentMsid={data.relarticle.id}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {data && data.relphoto && data.relphoto.items && (
                  <div className="row">
                    <div className="col12">
                      <div className="wdt_highlight video">
                        <SectionHeader sectionhead={data.relphoto.secname} weblink={data.relphoto.wu} />
                        <GeneralSlider
                          type="grid"
                          size="3"
                          width="190"
                          margin="20"
                          sliderData={data.relphoto.items}
                          //parentMsid={data.relarticle.id}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {data && data.relvideo && data.relvideo.items && (
                  <div className="row">
                    <div className="col12">
                      <div className="wdt_highlight video">
                        <SectionHeader sectionhead={data.relvideo.secname} weblink={data.relvideo.wu} />
                        <GeneralSlider
                          type="grid"
                          size="3"
                          width="190"
                          margin="20"
                          sliderData={data.relvideo.items}
                          //parentMsid={data.relarticle.id}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {gadgetData ? (
                  <div id={`user_review_${gadgetData.uname}`}>
                    <GadgetsRating
                      data={{
                        ur: gadgetData.averageRating
                          ? Number.parseFloat(gadgetData.averageRating / 2).toFixed(1)
                          : Config.locale.tech.befirsttoreview,
                        rating: gadgetData.ratings,
                        id: getregionalreview,
                        productCategory: gadgetData.category,
                        productName: gadgetData.name,
                        brandName: gadgetData.brand_name,
                        imageLink: `${process.env.IMG_URL}/photo/${mainImageId || "66951362"}/${gadgetData.uname}.jpg`,
                        itemDescription: data && data.story,
                        isProductAvailable: sliderClass === "",
                        itemPrice: gadgetData.price,
                        sortPrice: sortPriceSchema,
                        pid: gadgetData.pid,
                        updatedDate,
                        announced: gadgetData.announced,
                        canonical: data && data.pwa_meta && data.pwa_meta.canonical,
                      }}
                      isReviewDisabled={isReviewDisabled}
                      showLoginRegister={showLoginRegister}
                      loggedIn={authentication.loggedIn}
                      getreview={this.getReviewCard}
                      closereview={this.closeReviewCard}
                      showReviewCard={showReviewCard}
                    />
                  </div>
                ) : (
                  ""
                )}

                <ErrorBoundary>
                  {commentsData ? (
                    <CommentsSection
                      data={commentsData}
                      loggedIn={authentication.loggedIn}
                      showComments={this.showComments}
                      type="reviews"
                    />
                  ) : (
                    ""
                  )}
                </ErrorBoundary>
              </div>
              <div className="gadgetShow_adCTN">
                <AdCard adtype="ctn" mstype="ctnshow1gadgets" />
              </div>
            </div>
            <div className="col4">
              <div className="box-item">
                <AdCard mstype="mrec1" adtype="dfp" />
              </div>
              {latestTrendingData && Array.isArray(latestTrendingData.section) ? (
                <ErrorBoundary>
                  <LatestTrendingWidget
                    data={latestTrendingData.section}
                    title={`${Config.locale.tech.others} ${data && data.regionalcat}`}
                  />
                </ErrorBoundary>
              ) : (
                ""
              )}

              {trendingRightData && trendingRightData.section && trendingRightData.section.newsItem ? (
                <ErrorBoundary>
                  <ArticlesListBlock data={trendingRightData.section.newsItem} widgetClassName="with-scroll" />
                </ErrorBoundary>
              ) : (
                ""
              )}

              <AdCard mstype="mrec2" adtype="dfp" />

              {data && Array.isArray(data.recentcomp) ? (
                <ErrorBoundary>
                  <RecentlyComparedWidget data={data.recentcomp} />
                </ErrorBoundary>
              ) : (
                ""
              )}
              <ErrorBoundary>
                {gadgetData ? (
                  <MoreFromBrand
                    gadgetCategory={gadgetCategories[(gadgetData && gadgetData.category) || ""]}
                    title={`${data.reginalname} ${data.regionalcat}`}
                    titleLink={`tech/${gadgetCategories[gadgetData && gadgetData.category]}/${gadgetData.brand_name}`}
                    scrollToElementTop={this.scrollToElementTop}
                    elemID={`user_review_${gadgetData.uname}`}
                    data={data && Array.isArray(data.similar) && data.similar[0] && data.similar[0].gadget}
                  />
                ) : (
                  ""
                )}
              </ErrorBoundary>
              <div className="box-item">{/* {<AdCard mstype="btf" adtype="dfp" />} */}</div>

              <AmazonAffiliate category={gadgetData && gadgetData.category} tag={amazonTag} noimg="0" noh2="0" />
              <div className="box-item">{/* {<AdCard mstype="atf" adtype="dfp" />} */}</div>
            </div>
          </div>
          {isCommentClicked
            ? ReactDOM.createPortal(
                <div className="popup_comments">
                  <div className="body_overlay" onClick={this.closeIfOnOverlay} />
                  <div style={{ right: 0 }} id="comments__body" className="comments-body">
                    <span className="close_icon" onClick={this.closeCommentSlider} />
                    <iframe
                      src={`${process.env.WEBSITE_URL}comments_slider_react_v1.cms?msid=${
                        data.article.msid
                      }&type=reviews&isloggedin=${isLoggedIn()}`}
                      height="100%"
                      width="550"
                      border="0"
                      MARGINWIDTH="0"
                      MARGINHEIGHT="0"
                      HSPACE="0"
                      VSPACE="0"
                      FRAMEBORDER="0"
                      SCROLLING="no"
                      align="center"
                      title="iplwidget"
                      ALLOWTRANSPARENCY="true"
                      id="comment-frame"
                    />
                  </div>
                </div>,
                document.getElementById("comments-parent-container"),
              )
            : ""}
        </div>
        {!isLastGadget ? (
          <div className="pdp-story-seperator" id="">
            <div className="end-ad">{<AdCard mstype="btf" adtype="dfp" />}</div>
            <div className="story_partition">
              <span>{Config.locale.tech.nextGadget}</span>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}

const CommentsSection = ({ data, showComments, type }) => {
  const removeSpcChar = str => {
    const removed = str.replace(/<\/?[^>]+(>|$)/g, "");
    return removed;
  };
  if (type === "reviews") {
    return (
      <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
        <button className="btn-blue">{Config.locale.tech.text_write_reviews}</button>
        {data && Array.isArray(data) && data.length > 0 ? (
          <div className="pd-bottom-comments">
            {data.slice(1).length > 0 &&
              data
                .slice(1)
                .slice(0, 2)
                .map(c => (
                  <div className="pd-comment-box" key={c.C_T}>
                    <div className="user-thumbnail">
                      <ImageCard
                        className="userimg flL"
                        src={
                          (c.user_detail && c.user_detail.thumb) ||
                          `${process.env.API_BASEPOINT}${Config.Thumb.userImage}`
                        }
                        type="absoluteImgSrc"
                        size="gnthumb"
                      />
                    </div>
                    {"user_detail" in c && c.user_detail.FL_N ? (
                      <AnchorLink
                        href={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ""}
                        className="name"
                        target="_blank"
                      >
                        {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                      </AnchorLink>
                    ) : null}
                    <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                    <div className="footbar clearfix">
                      <span
                        data-action="comment-reply"
                        className="cpointer"
                        onClick={() => showComments()}
                        role="button"
                        tabIndex={0}
                      >
                        Reply
                      </span>
                    </div>
                  </div>
                ))}
          </div>
        ) : null}
        {data.length > 2 ? (
          <span className="btn-blue btm" onClick={() => showComments()}>
            {Config.locale.tech.readMoreReview}
          </span>
        ) : (
          ""
        )}
      </div>
    );
  }
  return (
    <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
      <div className="pd-bottom-comments-btns">
        <div className="cmtbtn-wrapper">
          {data && Array.isArray(data) && data.length > 0 ? (
            <span className="cmtbtn view" onClick={showComments} role="button" tabIndex={0}>
              {`${Config.locale.tech.text_view_comment} (${data[0].cmt_c})`}
            </span>
          ) : null}
          <span
            className="cmtbtn add"
            onClick={showComments}
            style={{ width: data.length === 0 ? "100%" : "50%" }}
            role="button"
            tabIndex={0}
          >
            {type === "reviews" ? (
              <span>{Config.locale.tech.text_write_reviews}</span>
            ) : (
              <span>
                <span className="icon" />
                <span>{Config.locale.tech.text_write_comment}</span>
              </span>
            )}
          </span>
        </div>
      </div>
      {data && Array.isArray(data) && data.length > 0 ? (
        <div className="pd-bottom-comments">
          {data.slice(1).length > 0 &&
            data
              .slice(1)
              .slice(0, 2)
              .map(c => (
                <div className={`pd-comment-box${c && c.C_A_ID ? " authorComment" : ""}`} key={c.C_T}>
                  <div className="user-thumbnail">
                    <Thumb
                      className="userimg flL"
                      src={
                        (c.user_detail && c.user_detail.thumb) ||
                        `${process.env.API_BASEPOINT}${Config.Thumb.userImage}`
                      }
                    />
                  </div>
                  {"user_detail" in c && c.user_detail.FL_N ? (
                    <Link to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ""} className="name" target="_blank">
                      {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                    </Link>
                  ) : null}
                  <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                  <div className="footbar clearfix">
                    <span
                      data-action="comment-reply"
                      className="cpointer"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      Reply
                    </span>
                    <span
                      className="up cpointer"
                      data-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      data-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span>
                  </div>
                </div>
              ))}
        </div>
      ) : null}
      {data.length > 2 ? (
        <span className="btn-blue btm" onClick={() => showComments()}>
          {Config.locale.tech.readMoreReview}
        </span>
      ) : (
        ""
      )}
    </div>
  );
};

CommentsSection.propTypes = {
  data: PropTypes.array,
  loggedIn: PropTypes.bool,
  showComments: PropTypes.func,
  type: PropTypes.string,
};

const RecentlyComparedWidget = ({ data }) => {
  if (!Array.isArray(data)) {
    return null;
  }

  return (
    <div className="box-item pd-list with-scroll">
      <div className="section">
        <div className="top_section">
          <h2>
            <span>{Config.locale.tech.recentlyCompared}</span>
          </h2>
        </div>
      </div>
      <ul>
        {data.map(compData => (
          <li key={compData.uname}>
            <AnchorLink href={`/tech/compare-${gadgetCategories[compData.category]}/${compData._id}`}>
              {`${Config.locale.tech.comparedotxt} ${compData._id}`}
            </AnchorLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

RecentlyComparedWidget.propTypes = {
  data: PropTypes.array,
};

const MoreFromBrand = ({ data, title, titleLink, gadgetCategory, scrollToElementTop, elemID }) => {
  if (!Array.isArray(data)) {
    return null;
  }

  return (
    <div className="box-item wdt_amazon_list with-scroll">
      <div className="section">
        <div className="top_section">
          <h2>
            <span>
              <AnchorLink href={titleLink}>{title}</AnchorLink>
            </span>
          </h2>
        </div>
      </div>
      <ul>
        {data.map(brandData => {
          const amztitle = brandData.c_model_lcuname;
          const price = brandData.price;
          const amzga = `${amztitle}_${price}`;
          const affiliateTag = getAffiliateTags(gadgetsConfig.affiliateTags.GS_BRAND);

          const brandTitle = brandData.regional && brandData.regional.name ? brandData.regional.name : brandData.name;
          const url =
            brandData.affiliate && Array.isArray(brandData.affiliate.related) && brandData.affiliate.related[0]
              ? brandData.affiliate.related[0].url
              : `/tech/${gadgetCategory}/${brandData.uname}`;
          const buyURL = getBuyLink({
            url,
            price,
            title: brandTitle,
            amzga,
            tag: affiliateTag,
          });
          return (
            <li className="news-card horizontal" key={brandData.uname}>
              <span className="img_wrap">
                <AnchorLink href={`/tech/${gadgetCategory}/${brandData.uname}`}>
                  <ImageCard
                    title={brandTitle}
                    alt={brandTitle}
                    msid={[].concat(brandData.imageMsid)[0]}
                    size="gnthumb"
                  />
                </AnchorLink>
              </span>
              <span className="con_wrap">
                <h4 title={brandTitle}>
                  <AnchorLink className="text_ellipsis" href={`/tech/${gadgetCategory}/${brandData.uname}`}>
                    {brandTitle}
                  </AnchorLink>
                </h4>

                <span className="rtng">
                  <span>
                    {`${Config.locale.tech.criticRating}:`}
                    <b>
                      {brandData.criticRating && parseInt(brandData.criticRating) === 0
                        ? "NA"
                        : Math.round(brandData.criticRating / 2)}
                      {brandData.criticRating && parseInt(brandData.criticRating) !== 0 ? <small>/5</small> : ""}
                    </b>
                  </span>
                  <span>
                    {`${Config.locale.tech.user}:`}
                    {brandData.averageRating ? (
                      <b className="rtng1">
                        {Math.round(brandData.averageRating / 2)}
                        <small>/5</small>
                      </b>
                    ) : (
                      <AnchorLink href={`/tech/${gadgetCategory}/${brandData.uname}#rate`} className="sub-rating">
                        {Config.locale.tech.submitrating}
                      </AnchorLink>
                    )}
                  </span>
                </span>
                <span className="price_tag symbol_rupees">{formatMoney(brandData.price, 0)}</span>
              </span>

              <span className="btn_wrap absolute">
                {brandData &&
                brandData.affiliate &&
                Array.isArray(brandData.affiliate.related) &&
                brandData.affiliate.related[0] &&
                brandData.affiliate.related[0].Identifier === "amazon" ? (
                  <a href={buyURL} rel="nofollow noopener" target="_blank" className="btn-buy">
                    {Config.locale.tech.buyFrom}
                  </a>
                ) : (
                  ""
                )}
              </span>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

MoreFromBrand.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string,
  titleLink: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

const PopularComparison = ({ dataForPopularMobile, restdataForPopularMobile, title }) => {
  return (
    <div className="wdt_popular_slider ui_slider">
      <h2>
        <span>{title}</span>
      </h2>

      {dataForPopularMobile && (
        <Slider
          type="compareListPopular"
          size="1"
          sliderData={dataForPopularMobile}
          width="165"
          height="185"
          SliderClass="pd_slider"
          islinkable="true"
          sliderWidth="180"
          headingRequired="yes"
        />
      )}

      <div className="items-in-list">
        <ul>
          {restdataForPopularMobile
            ? restdataForPopularMobile.map(item => {
                return (
                  <li key={item.product_name.join(" vs ")}>
                    <span className="text_ellipsis">
                      {`${Config.locale.tech.comparedotxt} ${item.product_name.join(" vs ")}`}
                    </span>
                  </li>
                );
              })
            : ""}
        </ul>
      </div>
    </div>
  );
};

PopularComparison.propTypes = {
  dataForPopularMobile: PropTypes.array,
  restdataForPopularMobile: PropTypes.array,
  title: PropTypes.string,
};

const CriticReview = ({ data, criticRating, averageRating, gadgetId }) => (
  <div className="thumb_img_reviews" id={`critic_review_${gadgetId}`}>
    <div className="section">
      <div className="top-section">
        <h2>
          <span>{Config.locale.tech.criticReview}</span>
        </h2>
      </div>
    </div>
    <h3>{data.title}</h3>
    <div className="img_wrap">
      <ImageCard msid={data.msid} size="largethumb" />
      <span className="image_caption">
        <span className="lft">
          <span className="head">
            <b className="icon_star one" />
            {Config.locale.tech.criticRating}
          </span>
          {Number.parseInt(criticRating) !== 0 ? (
            <span className="rate_txt">
              {Number.parseFloat(criticRating).toFixed(1)}
              <small>/5</small>
            </span>
          ) : (
            "NA"
          )}
        </span>
        <span className="cen">
          <span className="head">
            <b className="icon_star one" />
            {Config.locale.tech.userRating}
          </span>
          <span className="rate_txt">
            {Number.parseFloat(averageRating / 2).toFixed(1)}
            <small>/5</small>
          </span>
        </span>
        {data.topfeature && Array.isArray(data.topfeature["#text"]) ? (
          <span className="rgt">
            <span className="head">{Config.locale.tech.topfeature}</span>
            <ul className="features_txt">
              {data.topfeature["#text"].map(topFeatData => (
                <li key={topFeatData}>{topFeatData}</li>
              ))}
            </ul>
          </span>
        ) : (
          ""
        )}
      </span>
    </div>
    <span className="review_text">{data.artsyn}</span>
    <AnchorLink href={data.url} className="more-btn">
      {Config.locale.tech.readCompleteReview}
    </AnchorLink>
  </div>
);

CriticReview.propTypes = {
  data: PropTypes.object,
  criticRating: PropTypes.string,
  averageRating: PropTypes.string,
};

GadgetShow.propTypes = {
  data: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    gadgetshow: state.gadgetshow,
  };
}

export default connect(mapStateToProps)(GadgetShow);
