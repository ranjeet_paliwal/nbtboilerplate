import React, { Component } from "react";
import PropTypes from "prop-types";
import { _getStaticConfig } from "../../../../utils/util";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";

const Config = _getStaticConfig();

class SpecificationAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlyExpanded: [0],
    };
  }

  toggleAccordion = index => {
    let { currentlyExpanded } = this.state;
    // Remove from array if exists, else, add.
    if (currentlyExpanded.includes(index)) {
      currentlyExpanded = currentlyExpanded.filter(value => value !== index);
    } else {
      currentlyExpanded = currentlyExpanded.concat(index);
    }
    this.setState({
      currentlyExpanded,
    });
  };

  render() {
    const { heading, data, price } = this.props;
    const { currentlyExpanded } = this.state;

    if (!data) {
      return null;
    }

    return (
      <div className="pd-accordion full-heading">
        <SectionHeader sectionhead={`${heading} ${Config.locale.tech.specifications}`} />
        <div className="pd-spec-show">
          {Object.values(data).map((specData, index) => (
            <div key={specData.regional} className="gn_table_layout">
              <label
                onClick={() => this.toggleAccordion(index)}
                className={`${currentlyExpanded.includes(index) ? "active" : ""} spec`}
              >
                {specData.regional && specData.regional.replace(/_/g, " ")}
              </label>

              <table className="gn_table_layout">
                {index === 0 && price && price != "0" && (
                  <tr>
                    <td>{Config.locale.tech.priceInIndia} </td>
                    <td>{price}</td>
                  </tr>
                )}
                {Object.values(specData).map((speckey, index) => {
                  return speckey != "regional" ? (
                    <React.Fragment>
                      {speckey && speckey.key && speckey.key !== "price_in_india" && (
                        <tr>
                          <td>{speckey && speckey.key && speckey.key.replace(/_/g, " ")}</td>
                          <td>{(speckey && speckey.value) || "-"}</td>
                        </tr>
                      )}
                    </React.Fragment>
                  ) : null;
                })}
              </table>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

SpecificationAccordion.propTypes = {
  heading: PropTypes.string,
  data: PropTypes.object,
};

export default SpecificationAccordion;
