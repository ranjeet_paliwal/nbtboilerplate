import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded, fetchBrandListDataIfNeeded } from "../../../../actions/listpage/listpage";
import SectionLayoutMemo from "../../home/SectionLayout";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";
import "../../../../components/common/css/desktop/SectionWrapper.scss";
import AdCard from "../../../../components/common/AdCard";
import KeyWordCard from "../../../../components/common/KeyWordCard";
import VideoSection from "../../../../components/common/VideoSection/VideoSection";
import FakeDesktopDefault from "../../../../components/common/FakeCards/FakeDesktopDefault";
import { designConfigs } from "../../home/designConfigs";
import { setPageType, setParentId } from "../../../../actions/config/config";
import GadgetSlider from "../../../../components/common/GadgetSlider";
import BrandSlider from "../../../../components/common/BrandSlider";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import { updateConfig, _getStaticConfig } from "../../../../utils/util";
import Ads_module from "./../../../../components/lib/ads/index";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { PageMeta } from "../../../../components/common/PageMeta";
import { json } from "body-parser";

const siteConfig = _getStaticConfig();

export class GadgetsNow extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { dispatch, query, params, router } = this.props;
    if (params && params.msid == undefined) {
      params.msid = siteConfig.pages.tech;
    }
    GadgetsNow.fetchData({ dispatch, query, params, router }).then(data => {
      // Do not remove below line articlelist will change after fetching data for particular MSID.
      const { articlelist } = this.props;
      if (articlelist && articlelist.value && articlelist.value[0]) {
        const { pwa_meta, pg } = articlelist.value[0] ? articlelist.value[0] : {};
        // if (pg && pg.tp) {
        //   this.state.totalPages = pg.tp;
        // }
        if (pwa_meta && typeof pwa_meta == "object") {
          //set section and subsection in window
          Ads_module.setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      }
    });
    const category = "camera";
    GadgetsNow.fetchBranchListData({ dispatch, query, params, router, category });
    if (typeof window !== undefined) {
      setTimeout(() => {
        window.scrollTo(0, 0);
      }, 2000);
    }
  }

  componentDidUpdate() {
    const { articlelist, dispatch, router } = this.props;
    if (articlelist.value && articlelist.value[0] && articlelist.value[0].pwa_meta) {
      const pwaMeta = { ...articlelist.value[0].pwa_meta };
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   if (this.props.params.msid != nextProps.params.msid) {
  //     const { dispatch, params, router } = nextProps;
  //     const { query } = nextProps.location;
  //     try {
  //       GadgetsNow.fetchData({ dispatch, query, params, router }).then(data => {
  //         let _data = data && data.payload ? data.payload[0] : {};
  //         //fire ibeat for next
  //         _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
  //       });
  //     } catch (ex) {}
  //   }
  // }

  render() {
    const { alaskaData, home, articlelist } = this.props;
    let { value, brandList, isFetching, gadgetsData } = articlelist;
    const widgetData = value && value[0] ? JSON.parse(JSON.stringify(value[0])) : null;
    const sectionConfig = widgetData && widgetData.sectionconfig;

    // section data filter from config
    let photoSectionData = "";
    let videoSectionData = "";
    let reviewsSectionData = "";
    let newsSectionData = "";
    let tipsSectionData = "";
    if (widgetData && widgetData.sections && Array.isArray(widgetData.sections) && sectionConfig) {
      newsSectionData = widgetData.sections.filter(item => {
        return item.id == sectionConfig.news.msid;
      });
      newsSectionData = newsSectionData[0];
      tipsSectionData = widgetData.sections.filter(item => {
        return item.id == sectionConfig.ts.msid;
      });
      tipsSectionData = tipsSectionData[0];

      photoSectionData = widgetData.sections.filter(item => {
        return item.id == sectionConfig && sectionConfig.photos && sectionConfig.photos.msid;
      });
      photoSectionData = photoSectionData[0];

      videoSectionData = widgetData.sections.filter(item => {
        return item.id == sectionConfig.video && sectionConfig.video.msid;
      });
      videoSectionData = videoSectionData[0];

      reviewsSectionData = widgetData.sections.filter(item => {
        return item.id == sectionConfig.reviews.msid;
      });
      reviewsSectionData = reviewsSectionData[0];
    }

    let { pwa_meta, recommended } =
      articlelist && articlelist.value instanceof Array && articlelist.value[0] ? articlelist.value[0] : {};
    return !isFetching ? (
      <div className="body-content section-wrapper gn">
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        {widgetData && typeof widgetData == "object" && widgetData.sections && widgetData.sections.length > 0 ? (
          <React.Fragment>
            {/* Breadcrumb  */}
            {widgetData &&
              typeof widgetData.breadcrumb != "undefined" &&
              widgetData.breadcrumb.div &&
              widgetData.breadcrumb.div.ul ? (
              <ErrorBoundary>
                <Breadcrumb items={widgetData.breadcrumb.div.ul} />
              </ErrorBoundary>
            ) : (
              ""
            )}
            {/**-------------------------------------Main headline Section Start --------------------------------- */}
            <div className="row">
              <div className="col8 pd0">
                <GridSectionMaker
                  type={designConfigs.gnHomeHeadline}
                  data={widgetData.items.slice(0, 8)}
                  noLazyLoad={true}
                />
              </div>
              <div className="col4 rhs-widget">
                <AdCard mstype="mrec1" adtype="dfp" className="ad1 mrec1 box-item" />
                <div className="row">
                  <GridSectionMaker
                    type={designConfigs.listInfo}
                    data={widgetData.items.slice(8, 14)}
                    noLazyLoad={true}
                  />
                </div>
              </div>
            </div>

            {/* <React.Fragment key={"headline"}>
              <SectionLayoutMemo
                key="1_gnHomeHeadline"
                datalabel={"gnHomeHeadline"}
                dataObj={widgetData.sections[0]}
                // dataObj={widgetDataHome.lifestyle}
                // sliderObj={{ showSlider: true }}
                // rlvideo={widgetData.lifestyle && widgetData.lifestyle["rlvideo"] ? widgetData.lifestyle["rlvideo"] : null}
                alaskaData={alaskaData}
              />
            </React.Fragment> */}
            {/**-------------------------------------Main headline Section END --------------------------------- */}

            <div className="row">
              <div className="col8">
                {widgetData && typeof widgetData == "object" && widgetData.sections ? (
                  <SectionLayoutMemo
                    key={"1_gnHomeReview"}
                    datalabel={"gnHomeReview"}
                    dataObj={reviewsSectionData}
                    // rlvideo={widgetData.movie && widgetData.movie["rlvideo"] ? widgetData.movie["rlvideo"] : null}
                    alaskaData={alaskaData}
                  />
                ) : null}
              </div>
              <div className="col4 rhs-widget">
                {
                  <div className="row gn_trending">
                    <div className="col12 list-trending-topics">
                      {recommended && recommended.trendingtopics && recommended.trendingtopics.length > 0 && recommended.trendingtopics[0] ? (
                        <KeyWordCard
                          items={recommended.trendingtopics[0].items}
                          secname={recommended.trendingtopics[0].secname}
                        />
                      ) : null}
                    </div>
                    <AdCard mstype="mrec2" adtype="dfp" className="ad1 mrec2" ctnstyle="mrec2" />
                  </div>
                }
              </div>
            </div>

            {videoSectionData && <VideoSection videoData={videoSectionData} />}

            {/**-------------------------------------Gadgets Section Start --------------------------------- */}
            <div className="row con_gadgets">
              <div className="col8">
                <div className="row">
                  <SectionHeader
                    sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.gadgets}
                    weblink={siteConfig.mweburl + `/tech/mobile-phones`}
                    morelink={false}
                  />
                  {/* Gadgets start */}
                  <GadgetSlider gadgetsData={gadgetsData} />
                  {/* Gadgets end */}
                </div>
              </div>
              <div className="col4">
                <SectionLayoutMemo
                  key="1_news"
                  datalabel={"news"}
                  designlabel={"listHorizontal"}
                  dataObj={newsSectionData}
                  alaskaData={alaskaData}
                  override={{ noOfElements: 5 }}
                />
                {/* {_this.createSectionLayout({
                  datalabel: "travel",
                  designlabel: "listHorizontal"
                })} */}
              </div>
            </div>
            {/**-------------------------------------Gadgets Section END --------------------------------- */}

            {/**-------------------------------------Photos Section Start ----------------------------------------- */}
            {photoSectionData &&
              <div className="row">
                <div className="col12">
                  <div className="wdt_photo">
                    <SectionLayoutMemo
                      key="1_slider"
                      datalabel={"slider"}
                      data={photoSectionData}
                      sliderObj={{ size: "4" }}
                      alaskaData={alaskaData}
                    />
                  </div>
                </div>
              </div>
            }

            {/**-------------------------------------Photos Section END ------------------------------------------- */}
            {/**-------------------------------------Tips and Tricks & most popular Section Start --------------------------------- */}
            <div className="row">
              <div className="col8">
                <SectionLayoutMemo
                  key="1_tipsAndTricks"
                  datalabel={"gnHomeReview"}
                  dataObj={tipsSectionData}
                  alaskaData={alaskaData}
                />
              </div>
              {recommended && recommended.trending && recommended.trending.items ? (
                <div className="col4">
                  <SectionLayoutMemo designlabel={"listHorizontal"} dataObj={recommended.trending} />
                </div>
              ) : null}
            </div>
            {/**-------------------------------------Tips and Tricks & most popular Section END --------------------------------- */}
            {
              <div className="btf-placeholder">
                <AdCard mstype="btf" adtype="dfp" />
              </div>
            }
            <div className="row con_brands">
              <SectionHeader
                sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.brands}
                weblink={""}
              />
              <BrandSlider />
            </div>
          </React.Fragment>
        ) : null}
      </div>
    ) : (
      /* fake card for home page */
      <FakeDesktopDefault />
    );
  }
}

GadgetsNow.fetchData = function ({ dispatch, query, params, router }) {
  //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
  // dispatch(setPageType("home"));

  if (params && typeof params === "object") {
    params.pageType = "gnhome";
  } else {
    params = {
      pageType: "gnhome",
    };
  }

  dispatch(setPageType("articlelist"));

  return dispatch(fetchListDataIfNeeded(params, query, router));
  // return dispatch(fetchListDataIfNeeded(params, query, router)).then(data => {
  //   // fetchDesktopHeaderPromise(dispatch);
  //   return data;
  // });
};

GadgetsNow.fetchBranchListData = function ({ dispatch, query, params, router, category }) {
  return dispatch(fetchBrandListDataIfNeeded(params, query, router, category));
};

GadgetsNow.propTypes = {};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    home: state.home,
    articlelist: state.articlelist,
  };
}

export default connect(mapStateToProps)(GadgetsNow);
