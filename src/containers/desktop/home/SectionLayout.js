import React from "react";
import Slider from "../../../components/desktop/Slider/index";
import GridOnlyMemo from "./GridOnlyMemo";
import FakeHorizontalListCard from "../../../components/common/FakeCards/FakeHorizontalListCard";
import { _getStaticConfig, _isCSR } from "../../../utils/util";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";

const SectionLayoutMemo = props => {
  /* Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  let {
    datalabel,
    designlabel,
    sliderObj,
    data,
    headingObj,
    dataObj,
    alaskaData,
    override,
    minischedule,
    ptMsid,
    isExperiencePage,
    headerLinkParams,
    secType,
    sliderClass,
    className,
    pageH1Value,
    hideHeader,
    rowNum,
  } = props;
  const TagName = datalabel == "slider" ? React.Fragment : "div";
  try {
    designlabel = designlabel && designlabel != "" ? designlabel : datalabel;
    const labelDataObj =
      datalabel == "slider" || datalabel == "webstories" || datalabel == "ibeatGridHorizontal"
        ? typeof data === "object"
          ? data
          : null
        : dataObj;

    return (
      <ErrorBoundary>
        <TagName>
          <GridOnlyMemo
            dataObj={dataObj}
            headingObj={headingObj}
            data={data}
            rowNum={rowNum}
            datalabel={datalabel}
            designlabel={designlabel}
            sliderObj={sliderObj}
            alaskaData={alaskaData}
            override={override}
            minischedule={minischedule}
            ptMsid={ptMsid}
            isExperiencePage={isExperiencePage}
            headerLinkParams={headerLinkParams}
            secType={secType}
            sliderClass={sliderClass}
            className={className}
            pageH1Value={pageH1Value}
            hideHeader={hideHeader}
          />
          {sliderObj && sliderObj.showSlider && labelDataObj ? (
            <div className="row">
              <div className="col12">
                <div className="wdt_video slider-horizontalView">
                  {labelDataObj.rlvideo ? (
                    <Slider
                      type="grid"
                      size={sliderObj.size || "4"}
                      // movesize="2"
                      sliderData={labelDataObj.rlvideo.items}
                      sliderClass="videosection"
                      margin="25"
                      width={sliderObj.width || "221"}
                      parentMsid={labelDataObj.rlvideo.id}
                      // height="243"
                    />
                  ) : (
                    <span data-rlvideoid={labelDataObj._rlvideoid}>
                      <FakeHorizontalListCard elemSize={sliderObj.size || "4"} />
                    </span>
                  )}
                </div>
              </div>
            </div>
          ) : null}
        </TagName>
      </ErrorBoundary>
    );
  } catch (e) {
    console.log(e.message);
    return null;
  }
};

export default SectionLayoutMemo;
