export const designConfigs = {
  headline: {
    sections: [
      {
        className: "col12 pd0 top_headlines",
        type: [
          {
            startIndex: 0,
            noOfElements: 1,
            type: "lead",
            className: "col6 pd0",
            imgsize: "midthumb",
          },
          {
            startIndex: 1,
            noOfElements: 5,
            type: "horizontal",
            className: "col6",
            offads: true,
            imgsize: "smallthumb",
          },
          {
            startIndex: 6,
            noOfColumns: 3,
            //noOfElements: 31,
            type: "only-info",
            className: "col12 more_headlines",
          },
        ],
      },
    ],
  },
  movie: [
    { startIndex: 0, noOfElements: 2, type: "lead", className: "col4" },
    {
      startIndex: 2,
      noOfElements: 4,
      type: "vertical",
      noOfColumns: 2,
      className: "col4 pd0",
      offads: true,
      imgsize: "smallthumb",
    },
    { startIndex: 6, noOfElements: 6, type: "horizontal", className: "col4", imgsize: "smallthumb" },
  ],
  ipl: [
    {
      startIndex: 0,
      noOfElements: 2,
      type: "lead",
      className: "col4",
      below: { type: "component", compName: "capcard", className: "col4", props: { type: "orange" } },
    },
    {
      startIndex: 2,
      noOfElements: 4,
      type: "vertical",
      noOfColumns: 2,
      className: "col4 pd0",
      offads: true,
      below: {
        type: "component",
        compName: "capcard",
        className: "col4",
        props: { type: "purple" },
      },
    },
    {
      type: "component",
      compName: "pointstablecard",
      className: "col4",
      props: { compType: "pointtablecard" },
    },
  ],
  city: [
    {
      startIndex: 0,
      noOfElements: 1,
      type: "lead",
      className: "col3",
    },
    {
      startIndex: 1,
      noOfElements: 9,
      noOfColumns: 3,
      type: "only-info",
      className: "col9 more-city-news",
    },
  ],
  business: [
    { startIndex: 0, noOfElements: 1, type: "lead", className: "col6" },
    { startIndex: 1, noOfElements: 4, type: "only-info", className: "col6" },
  ],
  lifestyle: [
    {
      startIndex: 0,
      noOfElements: 3,
      type: "lead",
      className: "col4",
      offads: true,
    },
    {
      startIndex: 4,
      noOfElements: 6,
      noOfColumns: 2,
      type: "vertical",
      className: "col4 pd0",
    },
    { startIndex: 10, noOfElements: 8, type: "only-info", className: "col4" },
  ],
  crime: [
    {
      startIndex: 0,
      noOfElements: 1,
      type: "horizontal-lead",
      className: "col12",
    },
    { startIndex: 1, noOfElements: 5, type: "only-info", className: "col12" },
  ],
  layout: {
    ipl_movie: "2400px",
    city: "700px",
    sports: "680px",
    auto: "810px",
    lifestyle: "1100px",
    webstories: "470px",
    india: "630px",
    astro: "730px",
    photoSection: "380px",
    Business_Crime: "450px",
    jobs: "480px",
    viral: "580px",
    photoGallery: "460px",
  },
  listWithOnlyLeadImage: [
    {
      startIndex: 0,
      noOfElements: 1,
      type: "horizontal-lead",
      className: "col12",
      immutable: true,
    },
    { startIndex: 1, noOfElements: 4, type: "only-info", className: "col12" },
  ],
  listWithOnlySmallLeadImage: [
    {
      startIndex: 0,
      noOfElements: 1,
      type: "horizontal",
      className: "col12",
      immutable: true,
    },
    {
      startIndex: 1,
      noOfElements: 4,
      type: "only-info",
      className: "col12",
    },
  ],
  listWithOnlyInfo: [{ startIndex: 0, noOfElements: 4, type: "only-info", className: "col12" }],
  listInfo: [{ startIndex: 0, type: "only-info", className: "col12" }],
  listHorizontal: [{ startIndex: 0, noOfElements: 8, type: "horizontal", className: "col12" }],
  ibeatGridHorizontal: [
    {
      startIndex: 0,
      type: "vertical",
      noOfColumns: 5,
      noOfElements: 10,
      className: "col12 most-read-stroies",
      istrending: "true",
    },
  ],
  gnHomeHeadline: [
    {
      startIndex: 0,
      noOfElements: 2,
      type: "lead",
      className: "col6",
      imgsize: "largethumb",
    },
    {
      startIndex: 2,
      noOfElements: 6,
      noOfColumns: 2,
      type: "vertical",
      className: "col6 pd0",
      imgsize: "smallthumb",
    },
    // { startIndex: 10, noOfElements: 8, type: "only-info", className: "col4" },
  ],
  gnHomeReview: [
    { startIndex: 0, noOfElements: 2, type: "lead", className: "col6" },
    {
      startIndex: 2,
      noOfElements: 4,
      type: "vertical",
      noOfColumns: 2,
      className: "col6 pd0",
    },
  ],
  sectionLayout1: {
    sections: [
      {
        className: "col12 hideSecName1 newtoplayout",
        type: [
          {
            sections: [
              {
                className: "col7 pd0",
                type: [
                  {
                    startIndex: 0,
                    noOfElements: 1,
                    type: "lead",
                    className: "col12 pd0",
                    imgsize: "largethumb",
                  },
                  {
                    startIndex: 1,
                    noOfElements: 2,
                    type: "vertical",
                    noOfColumns: 2,
                    offads: true,
                    className: "row pd0",
                    imgsize: "smallthumb",
                  },
                ],
              },
            ],
          },
          { startIndex: 3, noOfElements: 10, type: "only-info", className: "col5" },
        ],
      },
    ],
  },
  sectionLayout2: [
    { startIndex: 0, noOfElements: 2, type: "lead", className: "col6" },
    {
      startIndex: 2,
      noOfElements: 4,
      type: "vertical",
      noOfColumns: 2,
      className: "col6 pd0",
      offads: true,
      imgsize: "smallthumb",
    },
  ],
  horizontalSlider: [{ startIndex: 0, type: "vertical", className: "col12 view-horizontal", offads: true }],
};
