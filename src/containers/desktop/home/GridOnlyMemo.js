import React from "react";
import Slider from "../../../components/desktop/Slider/index";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import { _getStaticConfig, _isCSR, generateUrl, isMobilePlatform } from "../../../utils/util";
import { designConfigs } from "./designConfigs";
import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
// import AstroWidget from "../../../components/common/AstroWidget/AstroWidget";
import GridCardMaker from "../../../components/common/ListingCards/GridCardMaker";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import astroStyle from "../../../components/common/AstroWidget/AstroWidget.scss";
import { extractMiniSchedule } from "../../../campaign/cricket/service";
import FakeHorizontalListCard from "../../../components/common/FakeCards/FakeHorizontalListCard";
import CapCard from "../../../campaign/cricket/components/CapCard";

const siteConfig = _getStaticConfig();

const GridOnlyMemo = props => {
  /* Its a generic function to create Grid Layout in Home Page
    Provide datalabel and config label to get required structure
    */

  let {
    datalabel,
    designlabel,
    sliderObj,
    data,
    headingObj,
    dataObj,
    alaskaData,
    override,
    minischedule,
    ptMsid,
    headerLinkParams,
    secType,
    sliderClass,
    className,
    rowNum,
    pageH1Value,
    hideHeader,
    isExperiencePage,
  } = props;

  let designConfig = designConfigs;
  if (isMobilePlatform()) {
    designConfig = defaultDesignConfigs;
  }

  try {
    const headerCopy = alaskaData ? JSON.parse(JSON.stringify(alaskaData)) : ""; // for copying Objects by Reference

    designlabel = designlabel && designlabel != "" ? designlabel : datalabel;
    const labelDataObj =
      datalabel == "slider" || datalabel == "webstories" || datalabel == "ibeatGridHorizontal"
        ? typeof data === "object"
          ? data
          : null
        : dataObj;
    const sectionhead =
      headingObj && headingObj.secname ? headingObj.secname : labelDataObj ? labelDataObj.secname : undefined;
    const headingTag = headingObj && headingObj.headingTag ? headingObj.headingTag : undefined;
    let astroLink = "";
    if (datalabel == "astro") {
      astroLink = generateUrl(labelDataObj && labelDataObj.items && labelDataObj.items[0]);
    }
    let weblink =
      dataObj && dataObj.override
        ? generateUrl(dataObj)
        : labelDataObj && labelDataObj.override
        ? generateUrl(labelDataObj)
        : "";

    if (weblink === "" && dataObj && dataObj.pwa_meta && dataObj.pwa_meta.canonical) {
      weblink = dataObj.pwa_meta.canonical;
    }
    const sectionId = labelDataObj && labelDataObj.id;
    let sectionIdArr;
    if (secType && secType === "nearbyCities" && labelDataObj.msids) {
      sectionIdArr = labelDataObj.msids.split("|");
    }
    const extrctSchedule = minischedule && minischedule.schedule && extractMiniSchedule(minischedule);
    const cityStateId= dataObj && dataObj.citySecId;
    return labelDataObj ? (
      <ErrorBoundary>
        {labelDataObj.items instanceof Array ? (
          <div
            data-row-number={rowNum || null}
            data-scroll-id={datalabel}
            className={`${datalabel == "slider" ? className : className + " row " + datalabel}`}
          >
            {hideHeader
              ? null
              : sectionhead && (
                  <SectionHeader
                    data={headerCopy}
                    sectionId={sectionId}
                    key={labelDataObj && labelDataObj.id}
                    sectionhead={sectionhead}
                    datalabel={datalabel}
                    weblink={weblink}
                    headerLinkParams={headerLinkParams}
                    secType={secType}
                    sectionIdArr={sectionIdArr}
                    headingTag={headingTag}
                    pageH1Value={pageH1Value}
                    cityStateId={cityStateId}
                  />
                )}

            {datalabel == "astro" ? (
              <div className="row mr0">
                <div className="col7">
                  <ul className="astro_widget">
                    {labelDataObj.items[0] ? (
                      <React.Fragment>
                        <GridCardMaker
                          card={labelDataObj.items[0]}
                          cardType="horizontal"
                          imgsize="smallthumb"
                        ></GridCardMaker>
                        <li className="astro_text">
                          <span className="text_ellipsis">{labelDataObj.items[0].syn}</span>
                        </li>
                      </React.Fragment>
                    ) : null}
                    <li className="astro_slider">
                      <ErrorBoundary>
                        <Slider
                          type="zodiac_sign"
                          size="5"
                          sliderData={siteConfig.ZodiacSigns.data}
                          width="60"
                          islinkable="false"
                          link={astroLink}
                          margin="5"
                        />
                      </ErrorBoundary>
                    </li>
                  </ul>
                </div>
                <div className="col5 pd0">
                  <GridSectionMaker
                    key={`${props.datalabel}${labelDataObj.id}`}
                    type={designConfig[designlabel]}
                    data={labelDataObj ? labelDataObj.items : []}
                    imgsize="smallthumb"
                    override={override}
                  />
                </div>
              </div>
            ) : (datalabel == "slider" || datalabel == "photo_slider") && sliderObj ? (
              <Slider
                type="grid"
                size={sliderObj.size || "4"}
                width={sliderObj.width || "221"}
                margin="20"
                sliderData={labelDataObj.items}
                parentMsid={labelDataObj.id}
                sliderClass={sliderClass}
                designConfig={designConfig[designlabel]}
              />
            ) : datalabel == "webstories" && sliderObj ? (
              <Slider
                type="webstories"
                size={sliderObj.size || "4"}
                width={sliderObj.width || "150"}
                margin="15"
                sliderData={labelDataObj.items}
                parentMsid={labelDataObj.id}
              />
            ) : datalabel == "gagdetsSection" ? (
              <div>hello </div>
            ) : (
              <GridSectionMaker
                key={`${props.datalabel}${labelDataObj.id}`}
                type={designConfig[designlabel]}
                data={labelDataObj ? labelDataObj.items : []}
                imgsize={isExperiencePage ? "" : "smallthumb"}
                override={override}
                ptMsid={ptMsid}
              />
            )}
            {datalabel == "ipl" && (
              <React.Fragment>
                <div className="col12">
                  <div className="row">
                    <div className="col4 wdt_capcard">
                      <CapCard type="orange" />
                    </div>
                    <div className="col4 wdt_capcard">
                      <CapCard type="purple" />
                    </div>
                    <div className="col4">
                      {labelDataObj && (labelDataObj._rlvideoid || labelDataObj.rlvideo) ? (
                        <div className="row">
                          <div className="col12">
                            <div className="wdt_video slider-horizontalView">
                              {labelDataObj.rlvideo ? (
                                <Slider
                                  type="grid"
                                  size={sliderObj.size || "4"}
                                  sliderData={
                                    labelDataObj.rlvideo && labelDataObj.rlvideo.items ? labelDataObj.rlvideo.items : []
                                  }
                                  sliderClass="videosection"
                                  margin="25"
                                  width={sliderObj.width || "221"}
                                  parentMsid={
                                    labelDataObj.rlvideo && labelDataObj.rlvideo.id ? labelDataObj.rlvideo.id : ""
                                  }
                                />
                              ) : (
                                <span data-rlvideoid={labelDataObj._rlvideoid}>
                                  <FakeHorizontalListCard elemSize={sliderObj.size || "4"} />
                                </span>
                              )}
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  </div>
                </div>

                <div className="col12">
                  <div className="mini_schedule">
                    <Slider
                      margin="15"
                      size="3"
                      sliderData={extrctSchedule && extrctSchedule.schedule}
                      width="280"
                      type="iplwidgets"
                      compType="minischedule"
                      sliderClass="grid_slider"
                    />
                  </div>
                </div>
              </React.Fragment>
            )}
          </div>
        ) : null}
      </ErrorBoundary>
    ) : (
      <FakeListing />
    );
  } catch (e) {
    console.log(e.message);
    return null;
  }
};

export default GridOnlyMemo;
// export const GridOnlyMemo = React.memo(GridOnlyMemo, areEqual);
