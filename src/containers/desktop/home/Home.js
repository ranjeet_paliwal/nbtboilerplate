import React from "react";
import { connect } from "react-redux";

import {
  fetchTopListDataIfNeeded,
  // fetchArticleListDataIfNeeded,
  fetchWidgetDataIfNeeded,
  fetchWidgetRelatedVideoDataIfNeeded,
  fetchPollDataIfNeeded,
  makeDataChangedFalse,
  fetchHomeCityWidgetDataIfNeeded,
} from "../../../actions/home/home";
// election related stuff

import AdCard from "../../../components/common/AdCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { PageMeta } from "../../../components/common/PageMeta"; // For Page SEO/Head Part

import { setPageType, setDateTime, setParentId, setRegion, resetRegion } from "../../../actions/config/config";
import { setSectionDetail, refreshAds } from "../../../components/lib/ads";
// import { fetchLOKSABHAELECTIONRESULT_IfNeeded } from '../../../campaign/election/loksabhaelectionresult/action';
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import FakeDesktopDefault from "../../../components/common/FakeCards/FakeDesktopDefault";

import PollCard from "../../../components/common/PollCard";
import LiveBlog from "../../../components/common/LiveBlog/LiveBlog";

import {
  addObserverToWidgets,
  createAndCacheObserver,
  getCountryCode,
  isInternationalUrl,
  _getStaticConfig,
  _isCSR,
  fetchHomeCityWidgetData,
  _getCookie,
} from "../../../utils/util";
// import { fetchDesktopHeaderPromise } from "../../../actions/header/header";
import { designConfigs } from "./designConfigs";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/Desktop.scss";
import "../../../components/common/css/desktop/SectionWrapper.scss";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import VideoSection from "../../../components/common/VideoSection/VideoSection";
// import AstroWidget from "../../../components/common/AstroWidget/AstroWidget";
import DataDrawer from "../../../components/desktop/DataDrawer";
import KeyWordCard from "../../../components/common/KeyWordCard";
import SectionLayoutMemo from "./SectionLayout";
import { clearFeedInterval, setRefreshFeedInterval, trackHomePageScrollDepth } from "../../utils/home_util";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import GoldTopNewsWidget from "../../../components/common/Gold/GoldTopNewsWidget";
import GoldPodCastIframe from "../../../components/common/Gold/GoldPodCastIframe";
import NewsLetter from "../../../components/common/Newsletter";
import AnchorLink from "../../../components/common/AnchorLink";
import BudgetWidget from "../../../components/common/BudgetWidget/BudgetWidget";
import BriefWidget from "../../../components/common/BriefWidget";
import { fireGRXEvent } from "../../../components/lib/analytics/src/ga";
import { fetchMiniScheduleDataIfNeeded } from "../../../modules/minischedule/actions/minischedule";
import { fetchIplCapDataIfNeeded } from "../../../actions/app/app";
import { MediaWireServiceRequest } from "../../../utils/serviceUtil";
import { internationalconfig } from "./../../../utils/internationalpageConfig";

const siteConfig = _getStaticConfig();
const siteName = process.env.SITE;
const intPages = internationalconfig && internationalconfig.routes;

export class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      miniScorecardInterval: "",
      mediaWireData: "",
      fetchCitySection: true,
    };
    this.resizeObserver = undefined;
    this.rhsParentObserver = undefined;
  }

  componentDidMount() {
    const newElement = document.getElementById("hpRhsAdsContainer");
    if (newElement) {
      this.observeResizeChange(newElement);
    }
    const rhsOtherContainer = document.getElementById("hpRhsOtherContainer");
    if (rhsOtherContainer) {
      //console.log(rhsOtherContainer);
      this.rhsOtherContainerObserver(rhsOtherContainer);
    }
    const { dispatch, query, params, home, router } = this.props;

    // headlines data changes if user coming to home page to as page.
    //dispatch(fetchTopListDataIfNeeded(this.props.params, "", this.props.router, true));

    Home.fetchData({ dispatch, query, params, router }).then(data => {
      const _data = data && data.payload ? data.payload : {};
      if (_data && _data.pwa_meta && _data.pwa_meta.ibeat) {
        //fire ibeat
        setIbeatConfigurations(_data.pwa_meta.ibeat);
      }
      setSectionDetail({ subsectitle1: "home", adsec: "homepage" });
    });

    // console.log("sssssssssssssssss");
    dispatch(setDateTime(home && home.value && home.value[0] && home.value[0].date));
    dispatch(setParentId("", ""));
    // if (intPages && !intPages.includes("/")) {
    dispatch(setRegion(router && router.location.pathname));
    // }
    const _this = this;
    let homepagewidgets =
      // process.env.PLATFORM == "desktop"
      //   ? designConfigs.widgetsList
      //   :
      _this.props.home.value && _this.props.home.value[1] ? _this.props.home.value[1] : [];
    const homepagepollwidget = homepagewidgets && homepagewidgets.filter(widget => widget._type == "poll");
    homepagewidgets.filter(widget => {
      if (widget._platform && widget._platform !== process.env.PLATFORM) {
        return false;
      }
      return !(
        widget._type == "banner" ||
        widget._type == "servicedrawer" ||
        widget.type == "schedule" ||
        widget._type == "weather" ||
        widget._type == "poll"
      );
    });
    // condition for adding headlines section on international pages
    if (isInternationalUrl(router) || getCountryCode()) {
      homepagewidgets = [...homepagewidgets, internationalconfig[siteName].headlineConfig];
    }
    // set home page ads

    // setSectionDetail('home', '');
    for (let i = 0; i < homepagewidgets.length; i++) {
      if (homepagewidgets[i].label == "city" && process.env.SITE != "tlg") {
        continue;
      }

      if (
        (homepagewidgets[i]._sec_id && homepagewidgets[i]._type) ||
        homepagewidgets[i]._type == "viraladda" ||
        homepagewidgets[i]._type == "recipes" ||
        homepagewidgets[i]._type == "astro"
      ) {
        homepagewidgets[i]._rlvideoid = homepagewidgets[i]._rlvideoid ? homepagewidgets[i]._rlvideoid : "";
        homepagewidgets[i]._sec_id = homepagewidgets[i]._sec_id ? homepagewidgets[i]._sec_id : "";
        const _params = { ...params, ...homepagewidgets[i] };
        dispatch(fetchWidgetDataIfNeeded(_params)).then(data => {
          if (data && data.payload) {
            if ("IntersectionObserver" in window) {
              // console.log("IntersectionTest" + i);
              if (data.payload._rlvideoid) {
                _this.rlvideoEleObserver = new IntersectionObserver((entries, observer) => {
                  entries.forEach(entry => {
                    if (entry.isIntersecting && !entry.target.querySelector(".slider-horizontalView")) {
                      const tthis = entry.target;
                      // / tthis.removeAttribute("data-rlvideoid");
                      // console.log("IntersectionTestBaad" + i);
                      _this.rlvideoEleObserver.unobserve(tthis);
                      dispatch(fetchWidgetRelatedVideoDataIfNeeded(data.payload.params)).then(data => {});
                    }
                  });
                });
                const elems = Array.prototype.slice.call(
                  document.querySelectorAll(`[data-rlvideoid="${data.payload._rlvideoid}"]`),
                );
                elems.forEach(elem => {
                  _this.rlvideoEleObserver.observe(elem);
                });
              }
              if (homepagewidgets[homepagewidgets.length - 1]._sec_id == data.payload.id) {
                // Trackers for Widgets in Home
                // homeWidgetsTracker();
              }
            } else {
              // fall back
              console.log("!IntersectionObserver");
            }
          }
        });
      }
    }

    if (homepagepollwidget && homepagepollwidget.length > 0) {
      _this.loadPollWidget(homepagepollwidget[0]);
    }

    if (typeof document !== undefined) {
      document.documentElement.scrollTop = 0;
    }

    const config = {
      root: null,
      rootMargin: "0px",
      threshold: 0.2, // when 20% in view
    };

    const callback = (entries, self) => {
      entries.forEach(entry => {
        if (entry && entry.isIntersecting) {
          try {
            let classname = entry.target.getAttribute("class");
            const scrollId = entry.target.getAttribute("data-scroll-id");
            let widgetId = scrollId || "";

            if (
              _this &&
              _this.props &&
              _this.props.home &&
              _this.props.home.value &&
              _this.props.home.value[2] &&
              scrollId
            ) {
              if (scrollId && scrollId in _this.props.home.value[2]) {
                const widgetData = _this.props.home.value[2][scrollId];
                if (widgetData && widgetData.params) {
                  if (widgetData.params._actuallabel) {
                    widgetId = widgetData.params._actuallabel;
                  } else if (widgetData.params.label) {
                    widgetId = widgetData.params.label;
                  }
                }
              }
            }
            const rowNum = entry.target.getAttribute("data-row-number");
            if (!widgetId) {
              if (classname && classname.includes("undefined")) {
                classname = classname.replace("undefined", "");
                classname = classname.trim();
                classname = classname.replace(" ", "-");
              }
              widgetId = classname;
            }

            fireGRXEvent("track", "scroll_depth", { widget_id: widgetId, row_id: rowNum || "NA" });

            self.unobserve(entry.target);
          } catch (e) {
            console.log("error while observing home widget", e);
          }
        }
      });
    };

    let geoLocation = JSON.parse(_getCookie("geo_data"));
    if (_isCSR()) {
      let PLData =
        this.props.home && this.props.home.value && this.props.home.value[0] && this.props.home.value[0].items;
      let defaultData = PLData[PLData.length - 1];

      let options = {
        hostid: siteConfig.hostid,
        platform: 2,
        app_platform: "WEB",
        secid: siteConfig.channelid,
        sltno: siteConfig.mediawireslot,
        geocl: geoLocation && geoLocation.city,
        geostate: geoLocation && geoLocation.region_code,
        geolocation: geoLocation && geoLocation.CountryCode,
        geocontinent: geoLocation && geoLocation.Continent,
        defaultData: defaultData,
      };

      MediaWireServiceRequest(options).then(value => {
        //console.log("TestVal", value);
        this.setState({ mediaWireData: value });
        // expected output: "Success!"
      });
    }

    const { app } = this.props;

    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].home &&
        app.scrolldepth[process.env.SITE].home.desktop === "true") ||
      false;
    createAndCacheObserver(_this, config, callback, { shouldCreate: shouldCreateScrollDepth });
    this.observeWidgetSelectors();

    // createAndCac
    // this.addObserverToWidgets();
    dispatch(setPageType("home"));

    // Trackers for Widgets in Home
    // homeWidgetsTracker();
    setRefreshFeedInterval(120, this.props.home.refreshFeedInterval, this.props.dispatch, "", this.props.router);
    const msParams = Object.assign({}, params, { msid: siteConfig.iplpages.miniSchedule });
    dispatch(fetchMiniScheduleDataIfNeeded(msParams, query));
    dispatch(fetchIplCapDataIfNeeded());
  }

  componentDidUpdate(prevProps) {
    const { dispatch, location, params, router, home } = this.props;

    if (!this.resizeObserver) {
      const newElement = document.getElementById("hpRhsAdsContainer");
      if (newElement) {
        this.observeResizeChange(newElement);
      }
    }
    if (home.dataChanged) {
      dispatch(makeDataChangedFalse());
      refreshAds();
      AnalyticsGA.pageview("/", {
        forcefulGaPageview: true,
      });
    }
    if (prevProps.home.sections != home.sections) {
      this.observeWidgetSelectors();
    }
    // headline data changes on flag click
    if (
      location &&
      prevProps.location.pathname != location.pathname &&
      intPages &&
      intPages.includes(location.pathname)
    ) {
      dispatch(setRegion(location.pathname));
      dispatch(fetchTopListDataIfNeeded(params, "", router, "true", location.pathname));
      //setRefreshFeedInterval(120, this.props.home.refreshFeedInterval, this.props.dispatch, "", this.props.router);
    }
  }

  shouldComponentUpdate(nextProps) {
    //fetch city data after alaska data is present at CSR
    if (
      _isCSR() &&
      this.state.fetchCitySection &&
      nextProps.alaskaData &&
      this.props.alaskaData !== nextProps.alaskaData &&
      Object.keys(nextProps.alaskaData).length > 10
    ) {
      this.fetchCityData(nextProps.alaskaData);
    }
    return nextProps.home.isFetching !== true || this.props.home !== nextProps.home;
  }

  componentWillUnmount() {
    if (this.resizeObserver) {
      this.resizeObserver.disconnect();
    }
    if (this.rhsParentObserver) {
      this.rhsParentObserver.disconnect();
    }
    const { dispatch } = this.props;
    clearFeedInterval(this.props.home.refreshFeedInterval, dispatch);
    const _this = this;
    if (_isCSR()) {
      window.scrollTo(0, 0);
      try {
        // clearInterval(_this.state.miniScorecardInterval);
        setSectionDetail();
      } catch (ex) {}
    }
    dispatch(setPageType(""));
  }

  rhsOtherContainerObserver(elem) {
    const resizeObserver = new ResizeObserver(() => {
      try {
        this.resizeRHSWidget();
      } catch (e) {
        console.log(e);
      }
    });
    this.rhsParentObserver = resizeObserver;
    resizeObserver.observe(elem);
  }

  resizeRHSWidget = () => {
    const rhsOtherContainer = document.getElementById("hpRhsOtherContainer");
    const rhsOtherContainerHeight = rhsOtherContainer ? rhsOtherContainer.getBoundingClientRect().height : 0;
    const rhsAdsContainer = document.getElementById("hpRhsAdsContainer");
    const hpadsContainerHeight = rhsAdsContainer ? rhsAdsContainer.getBoundingClientRect().height : 0; // This is already provided in the critical css
    const rhsParentContainer = rhsOtherContainer.parentElement;
    const rhsParentContainerHeight = rhsParentContainer ? rhsParentContainer.getBoundingClientRect().height : 0;
    if (rhsOtherContainerHeight + hpadsContainerHeight > rhsParentContainerHeight) {
      rhsParentContainer.style.height = `${rhsOtherContainerHeight + hpadsContainerHeight}px`;
    }
  };

  observeResizeChange(elem) {
    const resizeObserver = new ResizeObserver(resizedEntry => {
      try {
        const resizedHeight = Math.max(resizedEntry[0].contentRect.height - 310, 0);
        // Here we reset the height of the div if the content is overflowing the container in which we want to have this animation
        this.resizeRHSWidget();
        if (document.getElementById("hpRhsOtherContainer")) {
          document.getElementById("hpRhsOtherContainer").style.transform = "translateY(" + resizedHeight + "px)";
        }
      } catch (e) {
        console.log(e);
      }
    });
    this.resizeObserver = resizeObserver;
    resizeObserver.observe(elem);
  }

  observeWidgetSelectors() {
    try {
      if (_isCSR()) {
        const { sections } = this.props.home;
        const { app } = this.props;

        const shouldCreateScrollDepth =
          (app &&
            app.scrolldepth &&
            app.scrolldepth[process.env.SITE] &&
            app.scrolldepth[process.env.SITE].home &&
            app.scrolldepth[process.env.SITE].home.desktop === "true") ||
          false;
        // Rows which are hardcoded
        let rowsSelector = {
          ".wdt-trending": true,
          ".video-section": true,
          "[data-scroll-id='poll-widget']": true,
          "[data-scroll-id='photo-widget']": true,
          '[data-scroll-id="photo-dhamaal"]': true,
          '[data-scroll-id="gold-widget"]': true,
        };
        // Rows from feed
        if (sections && typeof sections == "object" && Object.keys(sections).length > 0) {
          for (const key in sections) {
            rowsSelector[`.row.${key}`] = true;
          }
        }

        addObserverToWidgets(this, rowsSelector, { shouldCreate: shouldCreateScrollDepth });
      }
    } catch (e) {
      console.log("Error adding observer to widgets", e);
    }
  }

  // addObserverToWidgets() {
  //   if (_isCSR()) {
  //     const { sections } = this.props.home;
  //     // Rows which are hardcoded
  //     let rowsSelector = { ".wdt-trending": true, ".video-section": true };
  //     // Rows from feed
  //     if (sections && typeof sections == "object" && Object.keys(sections).length > 0) {
  //       for (const key in sections) {
  //         rowsSelector[`.row.${key}`] = true;
  //       }
  //     }
  //     if (!this.memoizedObserver) {
  //       this.memoizedObserver = trackHomePageScrollDepth(this.IObserver, {});
  //     }

  //     this.memoizedObserver(rowsSelector);
  //   }
  // }

  checkWidgetShouldShow(data, key) {
    return (
      data &&
      data[key] &&
      data[key].params &&
      (!data[key].params._platform || data[key].params._platform === process.env.PLATFORM)
    );
  }

  loadPollWidget(pollparams) {
    const _this = this;
    const { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            const tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); // Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      document.querySelector("#lazyLoadpollWidget") &&
        _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    } else {
      // fall back
      dispatch(fetchPollDataIfNeeded(pollparams)); // Load Poll Widget
    }
  }

  fetchCityData(alaskaData) {
    const { dispatch } = this.props;
    let sectionId = siteConfig.pages.stateSection;
    sectionId = sectionId && sectionId.split(",")[0];
    // let pagetype = getPageType(location.pathname);
    let homepagewidgets = this.props.home.value && this.props.home.value[1] ? this.props.home.value[1] : [];

    const homePageCityParams = homepagewidgets && homepagewidgets.filter(widget => widget.sec_id == sectionId);

    let geoData = JSON.parse(_getCookie("geo_data"));
    let selectedCityId = _getCookie("selected_cityId");
    this.setState({ fetchCitySection: false });
    //  check user previouslly clicked on city or not then geo location
    if (process.env.SITE != "tlg") {
      if (selectedCityId && selectedCityId != "" && selectedCityId != undefined) {
        let params = fetchHomeCityWidgetData(alaskaData, selectedCityId, sectionId, homePageCityParams[0], dispatch);
        dispatch(fetchHomeCityWidgetDataIfNeeded(params, "home"));
      } else if (geoData && geoData.city != "" && geoData.city != undefined) {
        let params = fetchHomeCityWidgetData(
          alaskaData,
          "/" + geoData.city + "/",
          sectionId,
          homePageCityParams[0],
          dispatch,
        );
        dispatch(fetchHomeCityWidgetDataIfNeeded(params, "home"));
      } else {
        let defaultparams = homePageCityParams[0];
        defaultparams.citySecId = undefined;
        dispatch(fetchHomeCityWidgetDataIfNeeded(defaultparams, "home"));
      }
    }
  }

  render() {
    const _this = this;
    const { home, app, dispatch, isFetching, alaskaData, error, minischedule, router } = _this.props;
    if (error) {
      return null;
    }
    const { pwa_meta, recommended } = home && home.value instanceof Array && home.value[0] ? home.value[0] : {};
    const { value } = home;
    const { budget } = app;

    const iframeStatus = this.props && this.props.app && this.props.app.electionframetop;

    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    const widgetData = value[2] || null;
    const bannerData = value[1] || null;

    // Removing last item from headline feed
    let homePageHeadline = JSON.parse(JSON.stringify(this.props.home.value[0] && this.props.home.value[0].items));
    if (homePageHeadline && Array.isArray(homePageHeadline)) {
      homePageHeadline.pop();
    }

    let alaskaDataCopy = JSON.parse(JSON.stringify(alaskaData));

    const countryCode = getCountryCode();
    // console.log("widgetData", bannerData[0] && bannerData[0]._type );
    // const channelWiseSection1 = process.env.SITE == "eisamay" ? "bangladesh" : "education";
    // const channelWiseSection2 = process.env.SITE == "eisamay" ? "food" : "religion";
    return !isFetching ? (
      <div className="body-content section-wrapper">
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        {/* Budget Widget driven from pwa campaign */}
        {/* {budget && budget.activeStatus && */}
        {budgetLbConfig && budgetLbConfig.status === "true" ? <BudgetWidget /> : null}
        {/* ------------------------------------Headline Structure START --------------------------*/}
        <div className="row" key="headline">
          <div className="col8">
            {this.props.home.value[0] ? (
              <GridSectionMaker
                type={designConfigs.headline}
                data={homePageHeadline}
                mediaWireData={this.state.mediaWireData}
                noLazyLoad
              />
            ) : null}

            {process.env.SITE === "nbt" ? (
              <div className="col12 pd0 read_more_headlines">
                <AnchorLink href={siteConfig.moreNewsUrl}>{siteConfig.moreNews}</AnchorLink>
              </div>
            ) : process.env.SITE === "eisamay" &&
              (router.location.pathname === "/bangladesh" || countryCode === "BD") ? (
              <div className="col12 pd0 read_more_headlines">
                <AnchorLink href={siteConfig.moreBangladeshNewsUrl}>{siteConfig.moreNews}</AnchorLink>
              </div>
            ) : null}
            <div className="col12 pd0">
              <div className="wdt_video headline_video">
                {recommended && recommended.video ? (
                  <SectionLayoutMemo
                    key="1_slider"
                    datalabel="slider"
                    data={recommended.video[0]}
                    hideHeader={true}
                    sliderObj={{ size: "3", width: "192" }}
                    alaskaData={alaskaData}
                  />
                ) : null}
              </div>
            </div>
          </div>
          <div className="col4 rhs-widget" key="rhs-widget">
            <div className="hp-rhs-ads-container">
              <div id="hpRhsAdsContainer">
                <AdCard mstype="mrec1" adtype="dfp" className="box-item ad-top" pagetype="home" />
                {/* BCCL CTN */}
                <div className="box-item adblockdiv">
                  <AdCard className="emptyAdBox" mstype="ctnbccl" adtype="ctn" />
                </div>
                <div className="clr-overflow"></div>
              </div>
            </div>

            <div id="hpRhsOtherContainer" className="transform" style={{ transform: "translateY(0px)" }}>
              {recommended &&
              recommended.seotrending &&
              recommended.seotrending[0] &&
              recommended.seotrending[0].items ? (
                <div key="seotrendingKeys" className="trending-bullet">
                  <KeyWordCard
                    key="seotrendingKeys"
                    items={
                      recommended.seotrending[0] &&
                      recommended.seotrending[0].items &&
                      recommended.seotrending[0].items.slice(0, 4)
                    }
                    secname=" "
                  />
                </div>
              ) : null}
              {widgetData && typeof widgetData === "object" ? (
                _this.checkWidgetShouldShow(widgetData, "corona") && (
                  <SectionLayoutMemo
                    key="1_corona"
                    datalabel="corona"
                    designlabel="listWithOnlySmallLeadImage"
                    dataObj={widgetData.corona}
                    alaskaData={alaskaData}
                    override={{ noOfElements: 6 }}
                  />
                )
              ) : (
                <FakeListing />
              )}
              {/* amazon banner  alaska */}
              {bannerData && typeof bannerData === "object" && bannerData[0] && bannerData[0]._type === "banner" ? (
                <ErrorBoundary key="amz_banner">
                  <div className="box-item">
                    <a
                      className="banner"
                      href={bannerData[0] && bannerData[0]._override}
                      target="_blank"
                      rel="nofollow"
                    >
                      <img src={bannerData[0] && bannerData[0]._image} />
                    </a>
                  </div>
                </ErrorBoundary>
              ) : null}

              <ErrorBoundary key="scorecard_weathercard">
                {_isCSR() ? <DataDrawer dispatch={dispatch} router={router} /> : <div />}
              </ErrorBoundary>
              <div>
                <AdCard mstype="slug" adtype="dfp" />
                <AdCard mstype="mrec2" adtype="dfp" />
              </div>
            </div>
          </div>
        </div>
        {/* -----------------------------------------Headline Structure END-------------------------------- */}

        {/* US Election iframe driven from pwa campaign */}
        {(process.env.SITE === "nbt" ||
          process.env.SITE === "mt" ||
          process.env.SITE === "vk" ||
          process.env.SITE === "iag") &&
        iframeStatus &&
        iframeStatus.status === "true" ? (
          <div className="wdt_elec" data-row-number={`0-${process.env.SITE}`}>
            <iframe
              src={`${iframeStatus.url}&host=${process.env.SITE}`}
              height={iframeStatus.height}
              width={iframeStatus.width}
              border="0"
              scrolling="no"
              Frameborder="0"
              align="center"
              title="electionIframe"
            />
          </div>
        ) : null}
        {/* condition for adding headlines section on homepage for international pages*/}

        {/* {widgetData && typeof widgetData === "object" ? (
          <React.Fragment>
            <div className="gulfHeadlines">
              {isInternationalUrl(router) || getCountryCode() ? (
                <ErrorBoundary>
                  <SectionLayoutMemo
                    key="1_headlines"
                    datalabel="movie"
                    dataObj={widgetData && widgetData.headlines}
                    alaskaData={alaskaData}
                    headingObj={{ secname: siteConfig.locale.top_headlines }}
                    // hideHeader={true}
                  />
                </ErrorBoundary>
              ) : null}
            </div>
          </React.Fragment>
        ) : null} */}
        {/** -------------------------------------Brief Section Section Start --------------------------------- */}
        <div className="briefContainer">
          <BriefWidget pagetype="home" alaskaData={alaskaData} />
        </div>
        {/** -------------------------------------Brief Section Section End --------------------------------- */}

        {/** -------------------------------------Movie_City Section Start --------------------------------- */}
        <div className="movie_city" key="movie_city">
          {widgetData && typeof widgetData === "object" ? (
            <React.Fragment>
              {_this.checkWidgetShouldShow(widgetData, "ipl") && (
                <SectionLayoutMemo
                  key="ipl"
                  datalabel="ipl"
                  rowNum="1"
                  sliderObj={{ showSlider: false, size: "2", width: "130" }}
                  dataObj={widgetData.ipl}
                  rlvideo={widgetData.ipl && widgetData.ipl.rlvideo ? widgetData.ipl.rlvideo : null}
                  alaskaData={alaskaData}
                  minischedule={minischedule}
                  ptMsid={siteConfig.iplpages.pointsTable}
                />
              )}
              {_this.checkWidgetShouldShow(widgetData, "movie") && (
                <SectionLayoutMemo
                  key="1_movie"
                  datalabel="movie"
                  rowNum="1"
                  sliderObj={{ showSlider: true }}
                  dataObj={widgetData.movie}
                  rlvideo={widgetData.movie && widgetData.movie.rlvideo ? widgetData.movie.rlvideo : null}
                  alaskaData={alaskaData}
                />
              )}
              {_this.checkWidgetShouldShow(widgetData, "city") && (
                <div className="cityContainer">
                  <SectionLayoutMemo
                    key="1_city"
                    datalabel="city"
                    rowNum="1"
                    dataObj={widgetData.city}
                    alaskaData={alaskaData}
                    sliderObj={{ showSlider: true }}
                  />
                </div>
              )}
            </React.Fragment>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"ipl_movie"} />
          )}
        </div>

        {process.env.SITE == "tlg" ? (
          <div className="city_2" key="city_2">
            {widgetData && typeof widgetData === "object" ? (
              _this.checkWidgetShouldShow(widgetData, "city2") && (
                <SectionLayoutMemo
                  key="2_city"
                  rowNum={`2-${process.env.SITE}`}
                  datalabel="city"
                  dataObj={widgetData.city2}
                  alaskaData={alaskaData}
                  sliderObj={{ showSlider: true }}
                />
              )
            ) : (
              <FakeLayout designConfigs={designConfigs} label={"city"} />
            )}
          </div>
        ) : null}

        {/** -------------------------------------Movie_City Section END --------------------------------- */}

        {/** -------------------------------------India World liveblog NBT GOLD iframe start for NBT--------------------------------- */}
        {process.env.SITE === "nbt" || process.env.SITE === "mt" || process.env.SITE === "eisamay" ? (
          <div className="row sports_world" data-row-number={`1-${process.env.SITE}`} key="sports_world">
            {widgetData && typeof widgetData === "object" ? (
              <React.Fragment>
                {_this.checkWidgetShouldShow(widgetData, "sports") && (
                  <div className="col4">
                    <SectionLayoutMemo
                      key="2_sports" // Only NBT
                      rowNum={`2-${process.env.SITE}`}
                      datalabel="sports"
                      designlabel="listWithOnlyLeadImage"
                      dataObj={widgetData && widgetData.sports}
                      alaskaData={alaskaData}
                    />
                  </div>
                )}

                {_this.checkWidgetShouldShow(widgetData, "world") && (
                  <div className="col4" key="world">
                    <SectionLayoutMemo
                      key="1_world"
                      datalabel="world"
                      rowNum="2"
                      dataObj={widgetData.world}
                      designlabel="listWithOnlyLeadImage"
                      alaskaData={alaskaData}
                      override={{ noOfElements: 5 }}
                    />
                  </div>
                )}

                <div data-row-number="2" className="rhs-widget col4" key="liveblog_gold_adcard">
                  <ErrorBoundary key="liveblog_section">
                    <LiveBlog type="indiablog" cssClass="wdt_lb_home" />
                  </ErrorBoundary>

                  {process.env.SITE !== "mt" ? (
                    <ErrorBoundary>
                      <GoldPodCastIframe />
                    </ErrorBoundary>
                  ) : null}
                </div>
              </React.Fragment>
            ) : (
              <FakeLayout designConfigs={designConfigs} label={"sports"} />
            )}
          </div>
        ) : null}
        {/** -------------------------------------India World liveblog NBT GOLD iframe End for NBT--------------------------------- */}

        {/** -------------------------------------Auto_Tech_Edu Section Start --------------------------------- */}
        <div className="row auto_tech" key="auto_tech">
          {widgetData && typeof widgetData === "object" ? (
            <React.Fragment>
              {_this.checkWidgetShouldShow(widgetData, "auto") && (
                <div className="col4" key="auto">
                  {process.env.SITE === "nbt" ? (
                    <SectionLayoutMemo
                      key="1_auto"
                      datalabel="auto"
                      rowNum="3"
                      dataObj={widgetData.auto}
                      designlabel="listWithOnlyLeadImage"
                      alaskaData={alaskaData}
                      override={{ noOfElements: 4 }}
                      sliderObj={{
                        showSlider: true,
                        size: "2",
                        width: "130",
                      }}
                    />
                  ) : (
                    <SectionLayoutMemo
                      key="1_auto"
                      datalabel="auto"
                      rowNum="3"
                      dataObj={widgetData.auto}
                      designlabel="listWithOnlyLeadImage"
                      alaskaData={alaskaData}
                      override={{ noOfElements: 6 }}
                    />
                  )}
                </div>
              )}
              {_this.checkWidgetShouldShow(widgetData, "tech") && (
                <div className="col4" key="tech">
                  {process.env.SITE === "mt" ? (
                    <SectionLayoutMemo
                      key="1_tech"
                      datalabel="tech"
                      rowNum={`3-${process.env.SITE}`}
                      designlabel="listWithOnlyLeadImage"
                      dataObj={widgetData.tech}
                      alaskaData={alaskaData}
                    />
                  ) : (
                    <SectionLayoutMemo
                      key="1_tech"
                      datalabel="tech"
                      rowNum="3"
                      designlabel="listWithOnlyLeadImage"
                      dataObj={widgetData.tech}
                      alaskaData={alaskaData}
                      sliderObj={{
                        showSlider: true,
                        size: "2",
                        width: "130",
                      }}
                    />
                  )}
                </div>
              )}
              <div className="rhs-widget col4" key="auto_tech_adcard">
                <AdCard mstype="mrecinf" adtype="dfp" />
                {_this.checkWidgetShouldShow(widgetData, "education") && (
                  <SectionLayoutMemo
                    key="1_education"
                    rowNum="3"
                    datalabel="education"
                    designlabel="listWithOnlySmallLeadImage"
                    dataObj={widgetData.education}
                    alaskaData={alaskaData}
                    // override={{ offads: true }}
                  />
                )}
              </div>
            </React.Fragment>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"auto"} />
          )}
        </div>
        {/** -------------------------------------Auto_Tech_Edu Section END --------------------------------- */}

        {/** -------------------------------------lifestyle Section Start --------------------------------- */}
        <div className="lifestyleSection" key="lifestyleSection">
          {widgetData && typeof widgetData === "object" ? (
            _this.checkWidgetShouldShow(widgetData, "lifestyle") && (
              <SectionLayoutMemo
                key="1_lifestyle"
                datalabel="lifestyle"
                rowNum="4"
                dataObj={widgetData.lifestyle}
                sliderObj={{ showSlider: true }}
                rlvideo={widgetData.lifestyle && widgetData.lifestyle.rlvideo ? widgetData.lifestyle.rlvideo : null}
                alaskaData={alaskaData}
              />
            )
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"lifestyle"} />
          )}
        </div>
        {/** -------------------------------------lifestyle Section END --------------------------------- */}

        {/** -------------------------------------Web Stories Section Start ----------------------------------------- */}
        {process.env.SITE === "nbt" ? (
          <div className="row webstories">
            {widgetData && typeof widgetData === "object" ? (
              <React.Fragment>
                {_this.checkWidgetShouldShow(widgetData, "webstories") && (
                  <div className="col8 wdt_webstories" key="web_stories">
                    <ErrorBoundary>
                      <SectionLayoutMemo
                        key="1_webstories"
                        rowNum={`5-${process.env.SITE}`}
                        datalabel="webstories"
                        data={widgetData && widgetData.webstories}
                        sliderObj={{ size: "4" }}
                        alaskaData={alaskaData}
                      />
                    </ErrorBoundary>
                  </div>
                )}
                {_this.checkWidgetShouldShow(widgetData, "apanabazaar") && (
                  <div className="rhs-widget col4">
                    <SectionLayoutMemo
                      key="1_apanabazaar"
                      datalabel="apanabazaar"
                      rowNum="5"
                      designlabel="listHorizontal"
                      dataObj={widgetData.apanabazaar}
                      alaskaData={alaskaData}
                      override={{ offads: true, noOfElements: 5 }}
                    />
                  </div>
                )}
              </React.Fragment>
            ) : (
              <FakeLayout designConfigs={designConfigs} label={"webstories"} />
            )}
          </div>
        ) : null}
        {/** -------------------------------------Web Stories  apana bazar Section END ------------------------------------------- */}

        {/** -------------------------------------Poll Section Start --------------------------------- */}

        <div className="row poll-n-ads">
          {widgetData && typeof widgetData === "object" ? (
            process.env.SITE === "nbt" ? (
              <React.Fragment>
                {_this.checkWidgetShouldShow(widgetData, "india") && (
                  <div className="col4" key="india">
                    <SectionLayoutMemo
                      key="1_india"
                      datalabel="india"
                      rowNum={`6-${process.env.SITE}`}
                      dataObj={widgetData.india}
                      designlabel="listWithOnlySmallLeadImage"
                      alaskaData={alaskaData}
                      override={{ noOfElements: 6 }}
                    />
                  </div>
                )}
                {_this.checkWidgetShouldShow(widgetData, "metro") && (
                  <div className="col4">
                    {/* NBT metro */}
                    <SectionLayoutMemo
                      key="1_metro" // Only NBT
                      datalabel="metro"
                      rowNum={`6-${process.env.SITE}`}
                      designlabel="listWithOnlySmallLeadImage"
                      dataObj={widgetData && widgetData.metro}
                      alaskaData={alaskaData}
                      override={{ noOfElements: 5 }}
                    />
                  </div>
                )}
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className="col4">
                  <AdCard mstype="mrecinf" adtype="dfp" />
                </div>
                <div className="col4 pd0">
                  <AdCard mstype="mrecinf" adtype="dfp" />
                </div>
              </React.Fragment>
            )
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"india"} />
          )}

          <div className="col4">
            <div
              key="poll241424d"
              id="lazyLoadpollWidget"
              style={{
                height: "160px",
                width: "320px",
              }}
            />
            {this.props.poll ? (
              <ErrorBoundary key="poll">
                <div className="box-content" data-row-number={"6-poll"} data-scroll-id="poll-widget">
                  <SectionHeader
                    // weblink={siteConfig.poll.link}
                    sectionhead="Polls"
                  />
                  <PollCard dispatch={dispatch} data={this.props.poll} widget="true" />
                </div>
              </ErrorBoundary>
            ) : null}
            {process.env.SITE === "nbt" ? <NewsLetter pagetype="home" articleid="homepage" /> : null}
          </div>
        </div>

        {/** -------------------------------------Poll Section END --------------------------------- */}

        {/** -------------------------------------Astro_Travel Section Start --------------------------------- */}
        <div className="row astroTravelsection">
          {widgetData && typeof widgetData === "object" ? (
            <React.Fragment>
              {_this.checkWidgetShouldShow(widgetData, "astro") && (
                <div className="col8 wdt_astro">
                  <SectionLayoutMemo
                    key="1_astro"
                    datalabel="astro"
                    rowNum="7"
                    designlabel="listWithOnlyInfo"
                    dataObj={widgetData.astro}
                    sliderObj={{
                      showSlider: true,
                      size: "3",
                      width: "190",
                    }}
                    rlvideo={widgetData.astro && widgetData.astro.rlvideo ? widgetData.astro.rlvideo : null}
                    alaskaData={alaskaData}
                    override={{ offads: true, startIndex: 1, noOfElements: 5 }}
                  />
                </div>
              )}
              {_this.checkWidgetShouldShow(widgetData, "travel") && (
                <div className="col4">
                  <SectionLayoutMemo
                    key="1_travel"
                    datalabel="travel"
                    rowNum="7"
                    designlabel="listHorizontal"
                    dataObj={widgetData.travel}
                    alaskaData={alaskaData}
                    override={{ noOfElements: 7 }}
                  />
                  {/* {_this.createSectionLayout({
                  datalabel: "travel",
                  designlabel: "listHorizontal"
                })} */}
                </div>
              )}
            </React.Fragment>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"astro"} />
          )}
        </div>
        {/** -------------------------------------Astro_Travel Section END --------------------------------- */}

        {/** -------------------------------------Photos Section Start ----------------------------------------- */}

        <div className="row" data-row-number="8" data-scroll-id="photo-widget">
          {recommended && recommended.photo ? (
            <div className="col12">
              <div className="wdt_photo">
                <SectionLayoutMemo
                  key="1_slider"
                  rowNum="8"
                  datalabel="slider"
                  data={recommended.photo[0]}
                  sliderObj={{ size: "4" }}
                  alaskaData={alaskaData}
                />
                {/* {_this.createSectionLayout({
                      datalabel: "slider",
                      data: recommended.photo[0],
                      sliderObj: { size: "4" }
                    })} */}
              </div>
            </div>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"photoSection"} />
          )}
        </div>

        {/** -------------------------------------Photos Section END ------------------------------------------- */}

        {/** -------------------------------------Business_Crime Section Start --------------------------------- */}
        <div className="row Business_Crime">
          {widgetData && typeof widgetData === "object" ? (
            <React.Fragment>
              {_this.checkWidgetShouldShow(widgetData, "business") && (
                <div className="col8">
                  <SectionLayoutMemo
                    key="1_business"
                    rowNum="9"
                    datalabel="business"
                    dataObj={widgetData.business}
                    alaskaData={alaskaData}
                  />
                  {/* {_this.createSectionLayout({ datalabel: "business" })} */}
                </div>
              )}
              <div className="col4 rhs-widget">
                {/* NBT Gold section */}
                {process.env.SITE === "nbt" || process.env.SITE === "eisamay" ? (
                  <ErrorBoundary>
                    <div data-row-number={`9-${process.env.SITE}`} data-scroll-id="gold-widget">
                      <GoldTopNewsWidget />
                    </div>
                  </ErrorBoundary>
                ) : (
                  _this.checkWidgetShouldShow(widgetData, "crime") && (
                    <SectionLayoutMemo
                      key="1_crime" // except NBT, Eisamay
                      datalabel="crime"
                      rowNum="9"
                      designlabel="listWithOnlySmallLeadImage"
                      dataObj={widgetData.crime}
                      alaskaData={alaskaData}
                    />
                  )
                )}
              </div>
            </React.Fragment>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"Business_Crime"} />
          )}
        </div>
        {/** -------------------------------------Business_Crime Section END --------------------------------- */}

        {/** -------------------------------------Religion Jobs sports NBT CR NBT Fact NBT job Section Start --------------------------------- */}
        {process.env.SITE === "nbt" ? (
          <React.Fragment>
            <div className="row jobs">
              {widgetData && typeof widgetData === "object" ? (
                <React.Fragment>
                  {_this.checkWidgetShouldShow(widgetData, "factcheck") && (
                    <div className="col4">
                      <SectionLayoutMemo
                        key="1_factcheck"
                        rowNum={`10-${process.env.SITE}`}
                        datalabel="factcheck"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.factcheck}
                        alaskaData={alaskaData}
                      />
                    </div>
                  )}
                  {_this.checkWidgetShouldShow(widgetData, "jobs") && (
                    <div className="col4">
                      <SectionLayoutMemo
                        key="1_jobs"
                        rowNum="10"
                        datalabel="jobs"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.jobs}
                        alaskaData={alaskaData}
                      />
                    </div>
                  )}
                  {_this.checkWidgetShouldShow(widgetData, "cr") && (
                    <div className="rhs-widget col4">
                      <SectionLayoutMemo
                        key="1_cr"
                        datalabel="cr"
                        rowNum="10"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.cr}
                        alaskaData={alaskaData}
                      />
                    </div>
                  )}
                </React.Fragment>
              ) : (
                <FakeLayout designConfigs={designConfigs} label={"jobs"} />
              )}
            </div>

            {/* NBT Blog vichar viral section start */}
            <div className="row viral">
              {widgetData && typeof widgetData === "object" ? (
                <React.Fragment>
                  {_this.checkWidgetShouldShow(widgetData, "vichar") && (
                    <div className="col4">
                      <SectionLayoutMemo
                        key="2_vichar"
                        datalabel="vichar"
                        rowNum="11"
                        designlabel="listHorizontal"
                        dataObj={widgetData.vichar}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 8, offads: true }}
                      />
                    </div>
                  )}
                  <div className="col4 blogs">
                    {_this.checkWidgetShouldShow(widgetData, "nbtblogs") && (
                      <SectionLayoutMemo
                        key="2_nbtblogs"
                        rowNum="11"
                        datalabel="nbtblogs"
                        designlabel="listHorizontal"
                        dataObj={widgetData.nbtblogs}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 3, offads: true }}
                      />
                    )}
                    {_this.checkWidgetShouldShow(widgetData, "readerblogs") && (
                      <SectionLayoutMemo
                        key="2_readerblogs"
                        datalabel="readerblogs"
                        rowNum="11"
                        designlabel="listHorizontal"
                        dataObj={widgetData.readerblogs}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 2, offads: true }}
                      />
                    )}
                  </div>
                  {_this.checkWidgetShouldShow(widgetData, "viral") && (
                    <div className="rhs-widget col4">
                      <SectionLayoutMemo
                        key="1_viral"
                        rowNum="11"
                        datalabel="viral"
                        designlabel="listHorizontal"
                        dataObj={widgetData.viral} // NBT viral
                        alaskaData={alaskaData}
                        override={{ noOfElements: 6 }}
                      />
                    </div>
                  )}
                </React.Fragment>
              ) : (
                <FakeLayout designConfigs={designConfigs} label={"viral"} />
              )}
            </div>
            {/* NBT Blog vichar viral section End */}
          </React.Fragment>
        ) : (
          <div className="row jobs">
            {widgetData && typeof widgetData === "object" ? (
              <React.Fragment>
                {_this.checkWidgetShouldShow(widgetData, "religion") && (
                  <div className="col4">
                    <SectionLayoutMemo
                      key="2_religion"
                      rowNum="12"
                      datalabel="religion"
                      designlabel="listWithOnlyLeadImage"
                      dataObj={widgetData.religion}
                      alaskaData={alaskaData}
                      override={{ offads: true }}
                    />
                    {/* {_this.createSectionLayout({
                  datalabel: "religion",
                  designlabel: "listWithOnlyLeadImage"
                })} */}
                  </div>
                )}
                {_this.checkWidgetShouldShow(widgetData, "jobs") && (
                  <div className="col4">
                    <SectionLayoutMemo
                      key="1_jobs" // except NBT
                      datalabel="jobs"
                      rowNum="12"
                      designlabel="listWithOnlyLeadImage"
                      dataObj={widgetData.jobs}
                      alaskaData={alaskaData}
                      override={{ offads: true }}
                    />
                    {/* {_this.createSectionLayout({
                  datalabel: "jobs",
                  designlabel: "listWithOnlyLeadImage"
                })} */}
                  </div>
                )}
                {process.env.SITE === "mt" || process.env.SITE === "eisamay" ? (
                  <div className="rhs-widget col4">
                    {_this.checkWidgetShouldShow(widgetData, "cr") && (
                      <SectionLayoutMemo
                        key="1_cr"
                        datalabel="cr"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.cr}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 6 }}
                      />
                    )}{" "}
                  </div>
                ) : (
                  <div className="rhs-widget col4">
                    {_this.checkWidgetShouldShow(widgetData, "sports") && (
                      <SectionLayoutMemo
                        key="1_sports" // except NBT
                        datalabel="sports"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.sports}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 6 }}
                      />
                    )}{" "}
                  </div>
                )}
              </React.Fragment>
            ) : (
              <FakeLayout designConfigs={designConfigs} label={"jobs"} />
            )}
          </div>
        )}
        {/** -------------------------------------Religion_Jobs Section END --------------------------------- */}

        <AdCard mstype="btf" adtype="dfp" className="ad1-btf" />

        {/** -------------------------------------PhotoGallery_Viral Section Start --------------------------- */}
        <div className="row photoGallery" data-row-number="13" data-scroll-id="photo-dhamaal">
          {widgetData && typeof widgetData === "object" ? (
            <React.Fragment>
              <div className="col8">
                <SectionHeader
                  sectionhead={siteConfig.locale.photomasti}
                  weblink={`https://${siteConfig.photogallerydomain}`}
                />
                {/* we will be using tamil frame for all channels. */}
                <div style={{ overflow: "hidden" }}>
                  <iframe
                    width="670"
                    height="386"
                    // src={`${siteConfig.weburl}/topgalleies_${siteConfig.wapCode}_pwa.cms`}
                    src={`//tamil.samayam.com/topgalleies_tamil_pwa.cms?channel=${process.env.SITE}`}
                  />
                </div>
              </div>
              <div className="col4">
                {/* joks section for nbt */}
                {process.env.SITE === "nbt"
                  ? _this.checkWidgetShouldShow(widgetData, "jokes") && (
                      <SectionLayoutMemo
                        key="1_jokes"
                        rowNum={`13-${process.env.SITE}`}
                        datalabel="jokes"
                        designlabel="listWithOnlySmallLeadImage"
                        dataObj={widgetData.jokes}
                        alaskaData={alaskaData}
                        override={{ noOfElements: 8 }}
                      />
                    )
                  : _this.checkWidgetShouldShow(widgetData, "lifestyle") && (
                      <SectionLayoutMemo
                        key="1_viral"
                        datalabel="viral"
                        designlabel="listHorizontal"
                        dataObj={widgetData.viral} // fact check for MT, viral for samayam  Not for NBT
                        alaskaData={alaskaData}
                        override={{ noOfElements: 5 }}
                      />
                    )}
                {/* {_this.createSectionLayout({
                      datalabel: "viral",
                      designlabel: "listHorizontal"
                    })} */}
              </div>
            </React.Fragment>
          ) : (
            <FakeLayout designConfigs={designConfigs} label={"photoGallery"} />
          )}
        </div>

        {/** -------------------------------------PhotoGallery_Viral Section End --------------------------------- */}

        <div className="video-section" data-row-number="14">
          <VideoSection alaskaData={alaskaData} />
        </div>

        {/** -------------------------------------recommended Section Start --------------------------------- */}
        {
          <div className="row wdt_trending" data-row-number="15">
            <div className="col8 list-trending-topics">
              {recommended &&
              recommended.seotrending &&
              recommended.seotrending[0] &&
              recommended.seotrending[0].items ? (
                <KeyWordCard
                  items={
                    recommended.seotrending[0] &&
                    recommended.seotrending[0].items &&
                    recommended.seotrending[0].items.slice(4)
                  }
                  secname={recommended.seotrending[0].secname}
                />
              ) : null}
            </div>
            <div className="col4">
              <AdCard adtype="dfp" mstype="subs" />
            </div>
          </div>
        }

        <ErrorBoundary>
          {recommended && recommended.trending && recommended.trending[0] && recommended.trending[0].items ? (
            <div className="row ibeat_trending" data-row-number="16">
              <SectionHeader sectionhead={recommended.trending[0].secname} />
              <div className="col12">
                <SectionLayoutMemo
                  key="1_ibeatGridHorizontal"
                  datalabel="ibeatGridHorizontal"
                  hideHeader={true}
                  data={recommended.trending[0]}
                  alaskaData={alaskaData}
                />
              </div>
            </div>
          ) : // _this.createSectionLayout({
          //       data: recommended.trending[0],
          //       datalabel: "ibeatGridHorizontal",
          //       headingObj: { secname: "Most Read" }
          //     })
          null}
        </ErrorBoundary>
        {/** -------------------------------------recommended Section Start --------------------------------- */}
      </div>
    ) : (
      /* fake card for home page */
      <FakeDesktopDefault pagetype="home" />
    );
  }
}

// HP election widget
// Home.fetchData = function ({dispatch, query, params}) {
//   //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
//   dispatch(setPageType('home'));
//   return dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(query, params, 'result')).then(() => dispatch(fetchTopListDataIfNeeded(params, query)));
// };

const FakeLayout = props => {
  const { designConfigs, label } = props;
  const secHeight =
    designConfigs && designConfigs.layout && designConfigs.layout[label] ? designConfigs.layout[label] : "";
  return <div style={{ minHeight: secHeight }}></div>;
};

Home.fetchData = function({ dispatch, query, params, router }) {
  //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
  dispatch(setPageType("home"));
  return dispatch(fetchTopListDataIfNeeded(params, query, router)).then(data => {
    // fetchDesktopHeaderPromise(dispatch);
    return data;
  });
};

Home.propTypes = {};

function mapStateToProps(state) {
  return {
    home: state.home,
    alaskaData: state.header.alaskaData,
    app: state.app,
    poll: state.home.poll,
    minischedule: state.minischedule,
  };
}

export default connect(mapStateToProps)(Home);
