import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchListDataIfNeeded, fetchNextListDataIfNeeded, fetchProfileData } from "../../../actions/topics/topics";
import TopicsCard, { ProfileDesc, ShortProfile } from "../../../components/common/TopicsCard";
import WebTitleCard from "../../../components/common/WebTitleCard";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  _isCSR,
  setHyp1Data,
  _getStaticConfig,
  handleReadMore,
  loadInstagramJS,
  loadTwitterJS,
  handleFBembed,
} from "../../../utils/util";
import { designConfigs } from "./designConfigs";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import AdCard from "../../../components/common/AdCard";
import Ads_module from "../../../components/lib/ads/index";
import AnchorLink from "../../../components/common/AnchorLink";
import PlayerStats from "../../../campaign/cricket/components/PlayerStats";
import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/Desktop.scss";
import "../../../components/common/css/desktop/TopicsCard.scss";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import RecommendedNewsWidget from "../../../components/common/RecommendedNewsWidget";
import { fetchRHSDataPromise } from "../../../actions/app/app";
const siteConfig = _getStaticConfig();

export class Topics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      totalPages: 0,
      _scrollEventBind: false,
    };
    this.config = {
      isFetchingNext: false,
      listCount: 1,
      listCountInc: () => {
        return this.config.listCount++;
      },
    };
    this.scrollHandler = false;
    // this.listCount = 1;
    // this.listCountInc = () => {
    //   return this.listCount++;
    // };
  }

  componentDidMount() {
    const { dispatch, params, value, router } = this.props;
    const { query } = this.props.location;
    //set section & subsection for first time
    if (value && typeof value.pwa_meta == "object") {
      Ads_module.setSectionDetail(value.pwa_meta);
      fetchRHSDataPromise(dispatch);

      // if (value.pwa_meta.hyp1 != "" && value.validtopic == "true") {
      //   dispatch(fetchProfileData(params, query, router, value.pwa_meta));
      // }
    }

    Topics.fetchData({ dispatch, query, params, router }).then(data => {
      //attach scroll only when sections is not there in feed
      if (typeof this.props.value != "undefined" && this.props.value.items) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta, pg } = this.props.value ? this.props.value : {};
      if (pg && pg.tp) {
        this.state.totalPages = pg.tp;
      }
      if (pwa_meta && typeof pwa_meta == "object") {
        Ads_module.setSectionDetail(pwa_meta);
        //set hyp1 variable
        pwa_meta ? setHyp1Data(pwa_meta) : "";
        //fire ibeat
        pwa_meta.ibeat && pwa_meta.ibeat.articleId ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      }

      handleReadMore();
    });

    if (typeof window != "undefined") {
      window.setTimeout(() => {
        try {
          loadInstagramJS();
          loadTwitterJS();
          handleFBembed();
        } catch (ex) {
          console.log("ERROR PROCESSING EMBEDS", ex);
        }
      }, 2000);
    }
    Ads_module.render({});
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    let _this = this;
    if (this.state._scrollEventBind == false) return;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = (document.scrollingElement || document.documentElement).scrollTop;
    }
    let footerHeight = document.querySelector("#footerContainer,footer").offsetHeight;
    docHeight = docHeight - footerHeight;
    let curpg = parseInt(this.props.value.pg.cp) + 1;
    if (scrollValue + 1000 > docHeight && curpg <= this.state.totalPages) {
      const { dispatch, params, isFetching, router } = this.props;
      const { query } = this.props.location;
      if (isFetching == false) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        Topics.fetchNextListData({ dispatch, query, params, router }).then(data => {
          let _data = data ? data.payload : {};
          //silentRedirect(_this.generateListingLink());
          _this.config.isFetchingNext = !_this.config.isFetchingNext;

          AnalyticsGA.pageview(location.origin + _this.generateListingLink());
          Ads_module.refreshAds(["fbn"]); //refresh fbn ads when next list added

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";

          // history push
          let queryString, url;
          queryString = window.location.search;
          url = location.origin + _this.generateListingLink();

          if (url != location.href || _data.pwa_meta.title != document.title) {
            //todo stop history push for webview
            window.history.replaceState({}, _data.pwa_meta.title, url);
            document.title = _data.pwa_meta.title;
          }
        });
      }
    }
  }

  generateListingLink(type) {
    let props = this.props;
    // console.log("generateListingLink : " + type);
    let pathname = props.location && props.location.pathname;
    // let search = props.location && props.location.search;

    if (pathname && props.value && props.value.pg && props.value.pg.cp) {
      let curpg = type == "moreButton" ? parseInt(props.value.pg.cp) + 1 : parseInt(props.value.pg.cp);

      if (pathname.indexOf("/profile.cms") > -1) {
        // if (pathname.indexOf("curpg") > -1) {
        //   let reExp = /curpg=\\d+/;
        //   return pathname.replace(reExp, "curpg=" + curpg);
        // } else return `${pathname}&curpg=${curpg}`;

        if (pathname.indexOf("curpg") > -1) {
          let reExp = /curpg=\\d+/;
          return pathname.replace(reExp, "curpg=" + curpg);
        } else return pathname + ((pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
      }
      var pathnameArr = pathname.split("/");
      var cp = pathnameArr[pathnameArr.length - 1];
      if (isNaN(cp)) {
        return pathname + "/" + curpg;
      } else {
        return pathname.replace(cp, curpg);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.searchkey != nextProps.params.searchkey ||
      this.props.params.searchtype != nextProps.params.searchtype ||
      this.props.params.curpg != nextProps.params.curpg
    ) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        Topics.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();
          let _data = data && data.payload ? data.payload[0] : {};
          const { pg } = _data;
          if (pg && pg.tp) {
            this.state.totalPages = pg.tp;
          }
          // if (!data.payload[0].items) {
          //   this.scrollBind();
          // } else {
          //   this.scrollUnbind();
          // }

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
      //reset hyp1 to ''
      setHyp1Data();
    }
  }

  /*Its a generic function to create Sectional Layout in Topics Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout(dataObj, datalabel, configlabel, showSlider) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    // this.manipulateDataObj(dataObj);
    return dataObj ? (
      <div className="row">
        <GridSectionMaker type={designConfigs[configlabel]} data={dataObj.items} />
      </div>
    ) : (
      <FakeListing />
    );
  }

  // manipulateDataObj(dataObj) {
  //   let arr = ["hl", "imageid", "lu", "id", "dl", "tn", "mstype", "mssubtype", "seolocation"];
  //   dataObj.items.map(item => {
  //     for (let key in item) {
  //       if (!(arr.indexOf(key) > -1)) delete item[key];
  //     }
  //   });
  //   console.log("dataObj---------", dataObj);
  // }

  render() {
    const { isFetching, dispatch, params, value, error, router, profile } = this.props;
    const { query } = this.props.location;
    const topicsData = value;
    const infoFields = [""];

    const pagetype =
      typeof params != "undefined" && typeof params.searchtype != "undefined" ? params.searchtype : "all";

    let moreTxt = siteConfig.locale.next_topic.more_all;
    if (pagetype == "all") moreTxt = siteConfig.locale.next_topic.more_all;
    else if (pagetype == "news") moreTxt = siteConfig.locale.next_topic.more_news;
    else if (pagetype == "photos") moreTxt = siteConfig.locale.next_topic.more_photos;
    else if (pagetype == "videos") moreTxt = siteConfig.locale.next_topic.more_videos;

    let dtype = topicsData && topicsData.dtype && topicsData.dtype.toLowerCase();

    //For handling of default case of topics
    if (dtype === "default") {
      dtype = "all";
    }

    const businessType = profile && profile.businessType ? profile.businessType.toLowerCase() : "";

    //console.log("moreTxt : ", moreTxt);

    return topicsData ? (
      <div className="body-content section-topics" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(topicsData.pwa_meta)}

        {/* seodescription */}
        {/* {(topicsData.pwa_meta && topicsData.pwa_meta.seodescription) !=
        "undefined"
          ? WebTitleCard(topicsData.pwa_meta)
          : null} */}

        {/* Breadcrumb  */}
        {topicsData &&
        typeof topicsData.breadcrumb != "undefined" &&
        typeof topicsData.breadcrumb.div != "undefined" &&
        typeof topicsData.breadcrumb.div.ul != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={topicsData.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}
        {/* For SEO Schema */}
        {SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}

        {/* topics searchbox */}
        {topicsData && topicsData.pagetype && topicsData.pagetype == "topicsall" ? (
          <div className="searchbox">
            <iframe
              taget="_parent"
              scrolling="no"
              src={`${siteConfig.weburl}${siteConfig.searchIframeLink}?layout=newtopic`}
              height="90"
              width="100%"
              frameBorder="0"
            />
          </div>
        ) : null}

        <div className="row player-stats">
          <div className={`col8 ${profile == undefined ? "txt-wrap" : ""}`}>
            <div className="con_stats">
              {/* Topics Details */}
              {topicsData && topicsData.topicsdata ? (
                <ShortProfile
                  item={topicsData}
                  data={topicsData.topicsdata}
                  profile={profile}
                  businessType={businessType}
                  heading={topicsData.hl}
                />
              ) : null}
            </div>
            <div className="profileDesc">
              {topicsData && topicsData.topicsdata && profile != undefined ? (
                <ProfileDesc data={topicsData.topicsdata} item={topicsData} />
              ) : null}
              <div className="row">
                {profile && Object.keys(profile).length > 0 && businessType === "cricketer" ? (
                  <PlayerStats data={profile} />
                ) : null}
              </div>
            </div>

            {/* Tablisting */}
            <div className="topic-listing">
              {topicsData && topicsData.tabs && Array.isArray(topicsData.tabs) ? (
                <ErrorBoundary>
                  <TopicsCard
                    data={topicsData.tabs}
                    type="topicsTabs"
                    pagetype={pagetype}
                    params={params}
                    dispatch={dispatch}
                    router={router}
                    query={query}
                    dtype={dtype}
                  />
                </ErrorBoundary>
              ) : null}

              {!isFetching || this.config.isFetchingNext ? (
                topicsData && topicsData.items && topicsData.items instanceof Array ? (
                  router &&
                  router.location &&
                  router.location.pathname &&
                  router.location.pathname.indexOf("/videos") > -1 ? (
                    this.createSectionLayout(topicsData, "", "topiclistingVideo")
                  ) : (
                    this.createSectionLayout(topicsData, "", "topicslisting")
                  )
                ) : (
                  <div key={topicsData.hl} className="no-data-found">
                    <div className="nodata_txt_icon">
                      <b />
                      <span>No record found</span>
                    </div>
                  </div>
                )
              ) : (
                <FakeListing />
              )}

              {/* more-btn */}
              {topicsData &&
              parseInt(topicsData.pg && topicsData.pg.cp) < parseInt(topicsData.pg && topicsData.pg.tp) ? (
                <AnchorLink hrefData={{ override: this.generateListingLink("moreButton") }} className="more-btn">
                  {moreTxt}
                </AnchorLink>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="col4">
            <div className="box-item">
              <AdCard mstype="mrec1" adtype="dfp" />
            </div>
            <RecommendedNewsWidget articleId={topicsData.msid} />
          </div>
        </div>
      </div>
    ) : (
      <FakeListing />
    );
  }
}

// fetchData
Topics.fetchData = function({ dispatch, query, params, router }) {
  return dispatch(fetchListDataIfNeeded(params, query, router));
};

// fetchNextListData
Topics.fetchNextListData = function({ dispatch, query, params, router }) {
  //console.log("========fetchNextListData===========");
  //console.log("query", query);
  //console.log("params", params);
  //console.log("router", router);
  return dispatch(fetchNextListDataIfNeeded(params, query, router));
};

function mapStateToProps(state) {
  return {
    ...state.topics,
  };
}

export default connect(mapStateToProps)(Topics);
