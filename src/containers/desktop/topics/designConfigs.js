export const designConfigs = {
  topicslisting: [
    {
      startIndex: 0,
      noOfElements: 2,
      noOfColumns: 2,
      type: "lead",
      className: "11 col12 pd0",
    },
    {
      startIndex: 2,
      noOfColumns: 2,
      noOfElements: 12,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallthumb",
    },
    {
      startIndex: 14,
      noOfColumns: 1,
      noOfElements: 1,
      type: "ads-horizontal",
      className: "col12 pd0",
    },
    {
      startIndex: 15,
      noOfColumns: 2,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics",
      imgsize: "smallthumb",
    },
  ],
  topiclistingVideo: [
    {
      startIndex: 0,
      noOfElements: 2,
      noOfColumns: 2,
      type: "lead",
      className: "11 col12 pd0",
    },
    {
      startIndex: 2,
      noOfColumns: 2,
      noOfElements: 12,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics on-video",
      imgsize: "smallwidethumb",
    },
    {
      startIndex: 14,
      noOfColumns: 1,
      noOfElements: 1,
      type: "ads-horizontal",
      className: "col12 pd0",
    },
    {
      startIndex: 15,
      noOfColumns: 2,
      type: "horizontal-lead",
      className: "col12 pd0 rest-topics on-video",
      imgsize: "smallwidethumb",
    },
  ],
};
