import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import fetch from "utils/fetch/fetch";
import { fetchRHSDataPromise } from "../../../actions/app/app";
import MovieShowComponent from "../../../components/desktop/MovieShow/MovieShow";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import FakeStoryCard from "../../../components/common/FakeCards/FakeStoryCard";
import { disableBodyScroll, elementInView, enableBodyScroll, filterAlaskaData, handleReadMore, hasValue, isLoggedIn, isMobilePlatform, throttle, updateConfig, _getStaticConfig, _isCSR, _set_cookie } from "../../../utils/util";
import AdCard from "../../../components/common/AdCard";
import { fetchMovieShowDataIfNeeded, fetchNextMovieShowData } from "../../../actions/movieshow/moviewshow";
import useInfiniteScroll from "../../../hooks/useInfinteScroll";
import CommentsPopup from "../../../components/common/CommentsPopup";
import { PageMeta } from "../../../components/common/PageMeta";
import Breadcrumb from "../../../components/common/Breadcrumb";
import { AnalyticsGA } from "../../../components/lib/analytics";
import { setParentId } from "../../../actions/config/config";
import Ads_module from "../../../components/lib/ads/index";

const siteConfig = _getStaticConfig();

export function MovieShow({ fetchRHSData, movieshow, params, dispatch, header, router, ...props }) {

  const [currentIndex, setCurrentIndex] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [rhsCSRData, setRhsCSRData] = useState({});
  const [rhsCSRVideoData, setRhsCSRVideoData] = useState({});
  const [commentsConfig, setCommentsConfig] = useState({commentsMSID: "", showCommentsPopup: false });
  const [showCommentBox, setShowCommentBox] = useState(false);
  const loaderRef = useRef();
  const [isFetching, setIsFetching] = useInfiniteScroll(fetchMoreListItems, loaderRef, 800);

  const isCSR = _isCSR();
  const isAmp = router && router.location.pathname && router.location.pathname.includes("amp_");
  const isMobile = isMobilePlatform();
  const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");
  
  useEffect(() => {
    if (typeof window !== "undefined" && "scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
      window.scrollTo(0, 0);
    }
    
    dispatch(fetchMovieShowDataIfNeeded(params));
    fetchRHSData();
    const twitter = document.createElement("script");
    twitter.src = "https://platform.twitter.com/widgets.js";
    twitter.defer = true;
    document.head.appendChild(twitter);
    const handleScrollCallback = () => {
        if(isFetching) return;
        const movieShowElementArr = document.querySelectorAll(".movieshow");
        if(movieShowElementArr){
          let inViewEle = null;
          for (const movieShowElement of movieShowElementArr) {
            if(elementInView(movieShowElement, true) && movieShowElement.getAttribute("data-url")){
              inViewEle = movieShowElement;
            }
          }
          if(inViewEle){
            const url = `${window.location.origin}/${inViewEle.getAttribute("data-url").split(".com/")[1]}`;
              if(window.location.href !== url){
                window.history.replaceState({}, inViewEle.getAttribute("data-title"), url);
                document.title = inViewEle.getAttribute("data-title");
              }
          }
      };
    }
    const handleScroll = throttle(handleScrollCallback)
    window.addEventListener('scroll', handleScroll)
    return () => window.removeEventListener('scroll', handleScroll)
  }, []);

  useEffect(() => {
    const pwaMeta = hasValue(movieshow.items[currentIndex], "pwa_meta", null);
    if (!isMobile && pwaMeta && pwaMeta.parentid) {
      fetchRHSCsrData(pwaMeta.parentid);
    }

    const relatedvideomapid = pwaMeta && pwaMeta.parentid ? pwaMeta.parentid : pwaMeta && pwaMeta.sectionid ? pwaMeta.sectionid : pwaMeta && pwaMeta.navsecid;
    if (!isMobile && relatedvideomapid) {
      fetchRHSCsrVideoData(relatedvideomapid, pwaMeta);
    }
  }, [currentIndex]);

  useEffect(() => {
    handleReadMore();
    const pwaMeta = hasValue(movieshow.items[currentIndex], "pwa_meta", null);
    if (pwaMeta && currentIndex === 0) {
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }
    if (pwaMeta && currentIndex !== 0) {
      Ads_module.setSectionDetail(pwaMeta);
    }
    const nextMsid = hasValue(movieshow.items[currentIndex], "pwa_meta.nextItem.msid", null);
    if(nextMsid){
      setHasMore(true);
    }else{
      setHasMore(false);
      setIsFetching(false);
    }
  }, [movieshow]);

  const fetchRHSCsrData = parentid => {
    fetch(
      `${process.env.API_BASEPOINT}/web_common.cms?feedtype=sjson&platform=web&msid=${parentid}&tag=ibeatmostread,mostpopularL2,mostpopularL1,trending`,
    )
      .then(data => {
        setRhsCSRData(data);
      })
      .catch({});
  };

  const fetchRHSCsrVideoData = (relatedvideomapid,pwaMeta) => {
    fetch(
      `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=6&feedtype=sjson${pwaMeta.pagetype === 'videoshow' ? '&pagetype=videoshow' : ''}`,
    ).then(data => {
      setRhsCSRVideoData(data);
    });
  };

  function fetchMoreListItems() {
    const nextMsid = hasValue(movieshow.items[currentIndex], "pwa_meta.nextItem.msid", null);
    if (nextMsid) {
      fetchNextMovieShowData(nextMsid, dispatch, params).then(data => {
        setIsFetching(false);
        setCurrentIndex(currentIndex + 1);
      });
    }else{
      setIsFetching(false);
      setHasMore(false);
    }
  }

  function getShareDetail(item) {
    if (!isCSR) return false;

    let currentHeadline = "";
    let currentShortUrl = "";
    let currentMSID = "";
    let currentUrl = "";
    if (item) {
      currentHeadline = item.hl;
      currentShortUrl = item.m;
      currentMSID = item.id;
      currentUrl = item.pwa_meta && item.pwa_meta.canonical;
    }

    const sharedata = {
      title: currentHeadline || document.title,
      url:  currentUrl || null,
      short_url: currentShortUrl,
      msid: currentMSID,
    };

    return sharedata;
  }

  const openCommentsPopup = commentsMSID => {
    setCommentsConfig({
      showCommentsPopup: true,
      commentsMSID
    });
  };

  const closeCommentsPopup = () => {
    setCommentsConfig({
      showCommentsPopup: false,
      commentsMSID: ""
    });
  };

  const handleComments = msid => {
    if (showCommentBox) return;
    setShowCommentBox(true);
    disableBodyScroll();
  };

  const closeCommentBox = () => {
    setShowCommentBox(false);
    enableBodyScroll();
  };

  const getDuration = min => {
      min = parseInt(min);
      if (min) {
        var hours = min / 60;
        var rhours = Math.floor(hours);
        var minutes = (hours - rhours) * 60;
        var rminutes = Math.round(minutes);
        return (
          <span className="duration" itemProp="duration">
            {rhours + " Hrs " + rminutes + " Mins"}
          </span>
        );
      } else {
        return null;
      }
  };

  const watsAppShare = (item) => {
      const sharedata = getShareDetail(item);
      //for watsapp sharing feature along with GA
      AnalyticsGA.event({
        category: "social",
        action: "Whatsapp_Wap_stickyAS",
        label: sharedata.url,
      });

      if (sharedata.short_url != "" && sharedata.short_url != null) {
        var info = sharedata.short_url; // check short micron url availability
        info += "/l" + siteConfig.shortutm;
        info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
      } else {
        var info = sharedata.url;
        info += "?utm_source=Whatsapp_Wap_stickyAS" + siteConfig.fullutm;
        info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
      }
      var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + encodeURIComponent(info);

      window.location.href = whatsappurl;

      return false;
  }

  if (!movieshow || !movieshow.items || movieshow.items.length === 0 || movieshow.items[0].id !== params.msid) {
    return <FakeStoryCard />;
  }

  return (
    <ErrorBoundary>
      <div>
        {!isMobile && <Breadcrumb items={movieshow.items[0].breadcrumb.div.ul} />}
        {PageMeta(movieshow.items[0].pwa_meta)}
        {movieshow.items.map((item, index) => (
          <div className="row movieshow articleshow_body" key={item.id} id={`movieshow_${item.id}`} data-url={item.pwa_meta.canonical} data-title={item.hl}>
            {index > 0 ? (
              <div className="story_partition">
                <span>{siteConfig.locale.next_article.movieshow}</span>
              </div>
            ) : null}
            <MovieShowComponent
              item={item}
              sharedata={() => getShareDetail(item)}
              pwaConfigAlaska={pwaConfigAlaska}
              rhsCSRData={rhsCSRData}
              rhsCSRVideoData={rhsCSRVideoData}//rhsCSR.VideoData}
              openCommentsPopup={() => openCommentsPopup(item.id)}
              isAmp={isAmp}
              router={router}
              handleComments={handleComments}
              closeCommentBox={closeCommentBox}
              showCommentBox={showCommentBox}
              getDuration={getDuration}
              watsAppShare={watsAppShare}
            />
            <div className="top-ad">
              <AdCard mstype="btf" adtype="dfp" pagetype="articleshow" />
            </div>
          </div>
        ))}
        {(isFetching || hasMore) && (
          <div ref={loaderRef}>
            <FakeStoryCard />
          </div>
        )}
      </div>
      <CommentsPopup
          msid={commentsConfig.commentsMSID}
          showCommentsPopup={commentsConfig.showCommentsPopup}
          closeComments={closeCommentsPopup}
          loggedIn={isLoggedIn()}
          closeOnOverLay={true}
          isNotEu={true}
      />
    </ErrorBoundary>
  );
}

MovieShow.fetchData = ({ dispatch, params, query, router, history }) =>
  dispatch(fetchMovieShowDataIfNeeded(params, query, router)).then(data => data);

const mapStateToProps = state => ({
  movieshow: state.movieshow,
  header: state.header,
});

export function mapDispatchToProps(dispatch) {
  return {
    fetchRHSData: () => fetchRHSDataPromise(dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieShow);
