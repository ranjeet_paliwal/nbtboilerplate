/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
import {
  elementInView,
  _isCSR,
  _checkUserStatus,
  _getCookie,
  _deferredDeeplink,
  _getStaticConfig,
  scrollTo,
  isMobilePlatform,
  handleReadMore,
  setHyp1Data,
  filterAlaskaData,
  loadInstagramJS,
  addObserverToWidgets,
  createAndCacheObserver,
  // setSectionDetail,
} from "../../utils/util";

import fetch from "../../utils/fetch/fetch";
import { isMiniTvVisible, isDockPlayerVisible } from "../../modules/videoplayer/slike";

import { setIbeatConfigurations } from "../../components/lib/analytics/src/iBeat";

import { AnalyticsGA } from "../../components/lib/analytics/index";
import CTN_module from "../../components/lib/ads/ctnAds";
import Ads_module from "../../components/lib/ads/index";
// import Articleshow from "../desktop/articleshow/Articleshow";
import { isFeatureURL } from "../../components/lib/ads/lib/utils";
import globalconfig from "../../globalconfig";
import { fireActivity } from "../../components/common/TimesPoints/timespoints.util";
import { fireGRXEvent } from "../../components/lib/analytics/src/ga";

const siteConfig = _getStaticConfig();
let highestScrollEncountered = 0;
const newsAgencies = ["brandwire", "colombia", "mediawire"];

const videoAds = [];
export const videoDockStatus = (miniTv, articleData) => {
  // isMiniTvVisible, isDockPlayerVisible
  // console.log("isMiniTvVisible", isMiniTvVisible(), isDockPlayerVisible());
  // if (minitv && minitv.hl) {
  //   return "0";
  // }
  const miniTvData = miniTv && miniTv.miniTvData;
  const miniTvPopUpVisible = miniTv && miniTv.isPopUpVisible;
  const leadVideo = articleData && articleData.lead && articleData.lead.vdo;
  const inContentVideo = articleData && articleData.vdo;

  if (miniTvData && miniTvData.slikeid && miniTvPopUpVisible == true) {
    // MiniTv is On
    return "0";
  }
  if (isDockPlayerVisible() || leadVideo || inContentVideo) {
    // Video is docked
    return "0";
  }
  if (document.querySelector(".CubeParent #box1")) {
    // Cube is On
    return "0";
  }
  return "1";
};

export function didUpdateActions(_this, prevProps, prevState) {
  const { minitv, items, router } = _this.props;
  const articleData = items && items[0];
  const flag = videoDockStatus(minitv, articleData);
  const agencyNameNew = (articleData && articleData.audetails && articleData.audetails.agency) || "";
  const agencyName = articleData && articleData.ag ? articleData.ag.toLowerCase() : agencyNameNew;
  const pageType =
    router.location.pathname && router.location.pathname.indexOf("articleshow_esi_test") > -1
      ? "articleshow_esi_test"
      : null;

  if (
    (!agencyName ||
      (agencyName && !newsAgencies.includes(agencyName) && videoAds.indexOf(articleData && articleData.id) == -1)) &&
    // Not of this condition because in some cases, Ad Serving rules is not present in pwa_meta
    !(articleData && articleData.pwa_meta && articleData.pwa_meta.AdServingRules === "Turnoff")
  ) {
    videoAds.push(articleData && articleData.id);
    mod_symmetricalAds("", pageType, isFeatureURL(), flag);
  }
}

export function addElementsForScrollDepth(_this) {
  if (Array.isArray(_this.props.items)) {
    const mappedSelectors = {};
    _this.props.items.forEach(dataPoint => {
      // Common widgets
      mappedSelectors[`[data-scroll-id="superhit-widget-${dataPoint.id}"]`] = true;
      mappedSelectors[`[data-scroll-id="popular-videos-${dataPoint.id}"]`] = true;
      // Recommended news widget comes on bottom for mobile
      if (isMobilePlatform()) {
        mappedSelectors[`[data-scroll-id="recommended-news-${dataPoint.id}"]`] = true;
      }
    });
    const { app } = _this.props;

    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].articleshow &&
        app.scrolldepth[process.env.SITE].articleshow[process.env.PLATFORM] === "true") ||
      false;
    addObserverToWidgets(_this, mappedSelectors, { shouldCreate: shouldCreateScrollDepth });
  }
}

function timesPointsActivity(activity, msid) {
  if (typeof fireActivity === "function" && msid) {
    fireActivity(activity, msid);
  }
}

export function didMountActions(_this, componentObj) {
  const { dispatch, params, items, router, location, minitv } = _this.props;
  const { query } = location;
  const pageConfig = _this.config;
  const pageProps = _this.props;
  const msid = items && items[0] && items[0].id;

  timesPointsActivity("READ_ARTICLE", msid);

  const scrollDepthObsConfig = {
    root: null,
    rootMargin: "0px",
    threshold: 0.2, // when 20% in view
  };

  const scrollDepthObscallback = (entries, self) => {
    entries.forEach(entry => {
      if (entry && entry.isIntersecting) {
        const eventData = {};
        const scrollId = entry.target.getAttribute("data-row-id") || "NA";

        if (scrollId) {
          eventData.widget_id = scrollId;
        }
        fireGRXEvent("track", "scroll_depth", eventData);
        // Unobserve this
        self.unobserve(entry.target);
      }
    });
  };

  const { app } = _this.props;
  const shouldCreateScrollDepth =
    (app &&
      app.scrolldepth &&
      app.scrolldepth[process.env.SITE] &&
      app.scrolldepth[process.env.SITE].articleshow &&
      app.scrolldepth[process.env.SITE].articleshow[process.env.PLATFORM] === "true") ||
    false;

  createAndCacheObserver(_this, scrollDepthObsConfig, scrollDepthObscallback, {
    shouldCreate: shouldCreateScrollDepth,
  });

  // if (location && location.query && location.query.utm_source == "Newsletter") {
  //   timesPointsActivity("READ_ARTICLE", msid);
  // }

  if (items[0] && typeof items[0].pwa_meta === "object") {
    Ads_module.setSectionDetail(items[0].pwa_meta);
  }

  _this.config.curmsid = params.msid;

  // fetch data for csr
  componentObj.fetchData({ dispatch, params, query, router }).then(data => {
    // _this.animate_btn_App();

    const pwaMeta = data && data.payload && data.payload.pwa_meta;
    const msid = data && data.payload && data.payload.id;
    //  console.log("msidmsid1", msid);
    timesPointsActivity("READ_ARTICLE", msid);

    if (pwaMeta && pwaMeta.parentid) {
      getRhsData(pwaMeta, _this);
    }

    // Symetrical ads on scroll
    // if (
    //   typeof items[0] === "object" &&
    //   (typeof items[0].ag === "undefined" ||
    //     (typeof items[0].ag === "string" &&
    //       !(
    //         items[0].ag.toLowerCase() == "brandwire" ||
    //         items[0].ag.toLowerCase() == "colombia" ||
    //         items[0].ag.toLowerCase() == "mediawire"
    //       )))
    // )
    //   mod_symmetricalAds(
    //     "",
    //     router.location.pathname.indexOf("articleshow_esi_test") > -1 ? "articleshow_esi_test" : null,
    //     isFeatureURL(items[0].wu),
    //   );

    const { pwa_meta, audetails } = _this.props.items[0] ? _this.props.items[0] : {};
    if (pwa_meta && typeof pwa_meta === "object") {
      // set hyp1 variable
      pwa_meta ? setHyp1Data(pwa_meta) : "";

      // set section and subsection in window
      // Ads_module.setSectionDetail(pwa_meta);

      // fire ibeat for article
      pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";

      // If editor present in pwa_meta , set in window Object
      window._editorname = "";
      if (typeof data === "object" && data.type) {
        if (audetails) {
          window._editorname = audetails.cd;
        } else {
          window._editorname =
            typeof pwa_meta.editorname !== "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
        }
        // Hit ga
        AnalyticsGA.pageview(window.location.pathname, {
          setEditorName: true,
        });
      }
    }

    // Forcelly render all ads in page
    Ads_module.render({});
    const story_number = _this.props.location.query.story || 0;
    let d = null;
    if (story_number > 0) {
      d = document.getElementById(`story_${story_number}`);
    }
    if (d) {
      d.scrollIntoView({ block: "center" });
    }
  });

  if (_isCSR()) {
    // check perpetual needed or not
    if (pageConfig.perpetual) {
      pageConfig.timeout_var = window.setTimeout(() => {
        afterRender(_this);
        pageConfig.read_marker = document.querySelector("body span.read_marker");
        // scrollTo(document.documentElement, 0, 200);
      }, 1000);
    }
    if (pageProps.items && pageProps.items[0] && pageProps.items[0].m) {
      pageConfig.short_url = pageProps.items[0].m;
    } // set short URL

    // For test page only requested by sales team
    if (router.location.pathname.indexOf("articleshow_esi_test") > -1 && document.querySelector(".con_intad")) {
      document.querySelector(".con_intad").classList.remove("intctn");
      document.querySelector(".con_intad").classList.add("intctn_test");
    }
  }

  // Load RHS Data @ Desktop
  // console.log("asutil", isMobilePlatform(), items && items[0] && items[0].pwa_meta && items[0].pwa_meta.parentid);
  if (!isMobilePlatform() && items && items[0] && items[0].pwa_meta && items[0].pwa_meta.parentid) {
    getRhsData(items[0].pwa_meta, _this);
    // console.log("asutil", isMobilePlatform(), items);
  }

  // Load Instagram JS
  loadInstagramJS();
}

export function getRhsData(pwaMeta, _this) {
  if (pwaMeta.parentid) {
    fetch(
      `${process.env.API_BASEPOINT}/web_common.cms?feedtype=sjson&platform=web&msid=${pwaMeta.parentid}&tag=ibeatmostread,mostpopularL2,mostpopularL1,trending`,
    )
      .then(data => {
        if (typeof _this.setState === "function") {
          _this.setState({ rhsCSRData: data });
        }
      })
      .catch({});
  }

  const relatedvideomapid =
    pwaMeta && pwaMeta.parentid ? pwaMeta.parentid : pwaMeta.sectionid ? pwaMeta.sectionid : pwaMeta.navsecid;
  // Related videos comes from here in RHS / mobile bottom
  if (relatedvideomapid) {
    fetch(
      `${
        process.env.API_BASEPOINT
      }/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=6&feedtype=sjson${
        pwaMeta.pagetype === "videoshow" ? "&pagetype=videoshow" : ""
      }`,
    ).then(data => {
      if (typeof _this.setState === "function") {
        _this.setState({ rhsCSRVideoData: data }, () => {
          const { app } = _this.props;
          const shouldCreateScrollDepth =
            (app &&
              app.scrolldepth &&
              app.scrolldepth[process.env.SITE] &&
              app.scrolldepth[process.env.SITE].articleshow &&
              app.scrolldepth[process.env.SITE].articleshow[process.env.PLATFORM] === "true") ||
            false;
          addObserverToWidgets(
            _this,
            { [`[data-scroll-id="popular-videos-${pwaMeta.msid}"]`]: true },
            { shouldCreate: shouldCreateScrollDepth },
          );
        });
      }
    });
  }

  // Superhit widget comes in CSR (bottom widget on desktop/mobile)
  if (pwaMeta.parentid) {
    fetch(
      `${process.env.API_BASEPOINT}/sc_superhitwidget.cms?msid=${pwaMeta.parentid}&tag=ibeatmostread&type=articleshow&feedtype=sjson`,
    ).then(data => {
      const widgetObj = data;
      if (widgetObj && Array.isArray(widgetObj.items)) {
        widgetObj.items = widgetObj.items.map((wData, index) => {
          wData.override = `${wData.override ||
            wData.wu}?utm_source=mostreadwidget&utm_medium=referral&utm_campaign=article${index + 1}`;
          return wData;
        });
      }
      // UTM at superhit widget

      if (typeof _this.setState === "function") {
        _this.setState(
          { superHitWidgetData: widgetObj },

          () => {
            const { app } = _this.props;
            const shouldCreateScrollDepth =
              (app &&
                app.scrolldepth &&
                app.scrolldepth[process.env.SITE] &&
                app.scrolldepth[process.env.SITE].articleshow &&
                app.scrolldepth[process.env.SITE].articleshow[process.env.PLATFORM] === "true") ||
              false;
            addObserverToWidgets(
              _this,
              { [`[data-scroll-id="superhit-widget-${pwaMeta.msid}"]`]: true },
              { shouldCreate: shouldCreateScrollDepth },
            );
          },
        );
      }
    });
  }
}

function afterRender(_this) {
  if (_this.state._scrollEventBinded == false) {
    window.addEventListener("scroll", _this.scrollWithThrottle);
    _this.state._scrollEventBinded = true;
    // Readmore button handling
    handleReadMore();
  }
}

export function commonCompWillRecieveProps(_this, nextProps, componentObj) {
  // let _this = this;
  const { dispatch, params, items } = nextProps;
  const { minitv } = _this.props;
  const query = nextProps.location.query;
  const cur_query = _this.props.location.query;
  const { pwa_meta, audetails } = items[0] ? items[0] : {};
  const headerContainer = document.getElementById("headerContainer");
  const footerContainer = document.getElementById("footerContainer");

  if (_this.props.params.msid !== nextProps.params.msid) {
    // If editor present in pwa_meta , set in window Object
    window._editorname = "";
    if (audetails) {
      window._editorname = audetails.cd;
    } else {
      window._editorname =
        typeof pwa_meta.editorname !== "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
    }
    // Hit ga

    AnalyticsGA.pageview(window.location.pathname, {
      setEditorName: true,
    });
  }
  // Update globalConfig router
  globalconfig.router.location = nextProps.location;

  // off perpetual for NIC
  if (query && query.type == "nic" && isMobilePlatform()) {
    componentObj.fetchData({ dispatch, params, query });
    // scrollTo(document.documentElement, 0, 200);
    window.removeEventListener("scroll", _this.scrollWithThrottle);
    if (headerContainer) {
      headerContainer.classList.add("hide");
    }
    if (footerContainer) {
      footerContainer.classList.add("hide");
    }
  }
  // reset config/state as per prev scroll article when returning from nic
  else if (cur_query && cur_query.type == "nic" && query && query.type != "nic" && isMobilePlatform()) {
    _this.state._scrollEventBinded = !_this.state._scrollEventBinded;
    _this.config.cur_art = _this.config.init_art = null;
    _this.config.scrollHeight = 0;
    _this.config.read_marker = null;
    afterRender(_this);
    if (headerContainer) {
      headerContainer.classList.remove("hide");
    }
    if (footerContainer) {
      footerContainer.classList.remove("hide");
    }
  }
  // When we click on new story in RHS or BottomWidget
  else if (
    _this.config.resume &&
    _this.config.nextmsid != params.msid &&
    _this.config.curmsid != params.msid &&
    !nextProps.isFetching
  ) {
    // Check if next story is from perpetual or user clicked
    // Refresh ads
    // Ads_module.refreshAds(["atf", "fbn", "innove", "skinrhs"]);
    // Update config.curmsid
    _this.config.curmsid = params.msid;
    // Update index
    _this.state.index = 0;
    // fetch data for article
    componentObj.fetchData({ dispatch, params, query }).then(data => {
      const articleId = data && data.payload && data.payload.id;

      timesPointsActivity("READ_ARTICLE", articleId);
      const flag = videoDockStatus(minitv, data);
      scrollTo(document.documentElement, 0, 1);

      // reset
      _this.config = Object.assign(_this.config, {
        maxArticles: 9,
        adcounter: 0,
        cur_art_height: 0,
        art_scrolled_per: 0,
        last_scrolled_per: 0,
        lastscrolled: 0,
        init_art: document.querySelectorAll(`[data-artBox]`)[0],
        cur_art: document.querySelectorAll(`[data-artBox]`)[0],
        read_marker: null,
        resume: true,
        articleIds: [],
      });
      getRhsData(data && data.payload && data.payload.pwa_meta, _this);
      // Its hack for OnClick articles so that when article move to TOP
      // afterwards we reset article data and initiate symmetrical ads
      setTimeout(() => {
        _this.config.lastscrolled = 0;
        _this.config.init_art = document.querySelectorAll(`[data-artBox]`)[0];
        videoAds.push(articleId);
        if (
          !videoAds.includes(articleId) &&
          // Not of this condition because in some cases AdServingRules node doesnt come
          // in pwa_meta
          !(data && data.payload && data.payload.pwa_meta && data.payload.pwa_meta.AdServingRules === "Turnoff")
        ) {
          mod_symmetricalAds(_this.config.init_art, "", "", flag, data && data.payload && data.payload.wu);
        }
      }, 100);
    });
  } else {
    _this.config.resume = !nextProps.isFetching;
  }
}

export function trackersOnScroll(_this) {
  try {
    // let _this = this;
    const { items } = _this.props; // getting items in every call
    // loop on items length
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      const _index = i;
      let queryString;
      let url;
      // getting box id from  array for check  div element.
      const article = document.querySelector(`[data-artBox="article-${item.id}"]`);

      // checking box id view on window height half
      if (article && elementInView(article, true)) {
        // let url = location.origin + '/' + item.sec + '/articleshow/' + item.id + '.cms';
        const _article = article;

        queryString = window.location.search;
        // currently comment for photofeature ,fixed later this comment part
        url = `${window.location.origin}/${item.wu.split(".com/")[1]}`;
        // if (_index == 0) {
        // 	url = location.origin + '/' + item.wu.split('.com/')[1] + queryString;
        // } else {
        // 	url = location.origin + '/' + item.wu.split('.com/')[1];
        // }
        const btn_app = document.querySelector('[data-attr="btn_openinapp"]');

        // collect all sections present in that article
        const sections = article.querySelectorAll("section");
        let sectionInView = null;
        let sectionInViewIndex = 0;
        for (let i = 0; i < sections.length; i++) {
          const section = sections[i];
          // check for a section in view
          if (section && elementInView(section, true)) {
            // set url of the page as the section suburl
            url = window.location.origin + section.getAttribute("sub_url");
            if (!section.getAttribute("gatracked")) {
              sectionInView = section;
              sectionInViewIndex = i;
              setIbeatConfigurations(item.pwa_meta.ibeat);
              break;
            }
          } else if (section) {
            section.removeAttribute("gatracked");
          }
        }

        if (url !== window.location.href) {
          // todo stop history push for webview
          window.history.replaceState({}, item.hl, url);
          document.title = item.pwa_meta.title;

          const elmId = (btn_app && btn_app.getAttribute("id")) || "";

          elmId != "floating-widget-amazon-sale" &&
            btn_app &&
            btn_app.setAttribute(
              "href",
              _deferredDeeplink(
                item.pwa_meta.htmlview ? "htmlview" : item.pwa_meta.app_tn,
                siteConfig.appdeeplink,
                item.id,
                item.pwa_meta.htmlview ? item.pwa_meta.htmlview : null,
                null,
                item.pwa_meta.htmlview ? "openinapp_as" : item.pwa_meta.app_tn,
              ),
            );
        }

        // Its hack for showing article without ?story= when user reach top
        if (!sectionInView) {
          url = `${window.location.origin}/${item.wu.split(".com/")[1]}`;
        }

        // Take out current editor name from item
        const editorName =
          item.audetails && typeof item.audetails.cd !== "undefined" && item.audetails.cd != ""
            ? item.audetails.cd
            : item.pwa_meta && typeof item.pwa_meta.editorname !== "undefined" && item.pwa_meta.editorname != ""
            ? item.pwa_meta.editorname
            : "";

        window._editorname = editorName;

        // for first article ad is already loaded but does not have any identifier
        // so need to refresh it forcefully
        if (_index === 0) {
          highestScrollEncountered = _index;
          if (!(window.location.href.indexOf("story=") > -1) && sectionInView) {
            sectionInView.setAttribute("comscorepageview", true);
          }
          // if (sectionInView) {
          // sectionInView.setAttribute("comscorepageview", true);
          // }
        }
        // Check ga tracked status and fire series of tasks
        if (sectionInView && !sectionInView.getAttribute("gatracked")) {
          sectionInView.setAttribute("gatracked", true);
          // Fire pageview for perpetual
          // AnalyticsGA.pageview(window.location.pathname);
          // Fire pageview
          AnalyticsGA.pageview(window.location.pathname, {
            setEditorName: true,
            pageCountInc: false,
            comscorepageview: sectionInView.getAttribute("comscorepageview"),
          });
          // added for pageview_candidate call
          sectionInView.setAttribute("comscorepageview", true);
          // Refresh fixed ads after 3 sections
          sectionInViewIndex % 3 === 0 ? Ads_module.refreshAds(["fbn", "innove"]) : "";
        }

        // trigger page view on page load
        if (!_article.getAttribute("gatracked")) {
          // Hit ga
          AnalyticsGA.pageview(window.location.pathname, {
            setEditorName: true,
            pageCountInc: true,
            comscorepageview: _article.getAttribute("comscorepageview"),
          });

          // Commented for now due to double calls being fired
          // if (_index === 0) {
          //   refreshAllAdsInContainer({
          //     container: _article,
          //     shouldRefreshDfp: true,
          //     shouldRefreshCTN: true,
          //     forcefulRefresh: true,
          //   });
          // } else {
          //   refreshAllAdsInContainer({ container: _article, shouldRefreshDfp: true, shouldRefreshCTN: true });
          // }
          if (highestScrollEncountered <= _index && _index !== 0) {
            // no scroll event possible for first article
            // this means going downwards - should hot page scroll
            AnalyticsGA.event({
              category: "scroll",
              action: "PerpScroll",
              label: `Perp Scroll ${_index + 1}`,
            });
          } else {
            // this means reverse scrolling - do not trigger event
          }
          highestScrollEncountered = _index;
          _article.setAttribute("gatracked", true);
          _article.setAttribute("comscorepageview", true);

          // refresh DFP fbn ads
          // off for tlg as modifyAdsOnScrollView working
          !isFeatureURL(url) ? Ads_module.refreshAds(["fbn", "innove"]) : "";

          // For ESI to check adblocker
          if (window.clrcs) {
            clrcs.refresh();
          }

          // check if colombia is there or load colombia
          if (
            typeof colombia === "undefined" &&
            JSON.parse(_getCookie("geo_data")) &&
            JSON.parse(_getCookie("geo_data")).notEU
          ) {
            // colombia = window.colombia || {};
            // colombia.fns = colombia.fns || [];
            // loadJS(siteConfig.ads.ctnAddress , ()=>{}, 'colombiajs', true);
            // Initialize ads for ctn
            CTN_module.initialize({
              ctnads: adsObj.ctnads,
              dmpUrl: adsObj.dmpUrl,
              ctnAddress: adsObj.ctnAddress,
            });
          }

          // refire ibeat for new article
          item.pwa_meta && item.pwa_meta.ibeat ? setIbeatConfigurations(item.pwa_meta.ibeat) : "";
          break;
        }
        _this.config.short_url = item.m ? item.m : null; // set short URL

        // break;
      } else if (article) {
        article.removeAttribute("gatracked");
      }
    }
    const slideshows = document.querySelectorAll("[data-slideshow]");

    for (let i = 0; i < slideshows.length; i++) {
      if (elementInView(slideshows[i], false, 200)) {
        slideshows[i].click();
        break;
      }
    }
  } catch (e) {
    console.log(`trackersOnScroll error${e.message}`);
  }
}

// Initialise symmetricalAds module
// 'screen_ratio' (ideal is 1) is getting used to custom screen height so that ad-position can be adjusted
// lower 'screen-ration' suggest ads-position are close to TOP
export const mod_symmetricalAds = (article, pagetype, isFeaturedArticle, flag, articleUrl) => {
  if (isFeaturedArticle) {
    return;
  }
  const config = {
    // story-content as default selector : to get immediate childrens of article tag only
    defaultselector: ".story-content",
    should_initCTN: false,
    should_initGPT: false,
    // As we putting parallax in first MREC , so adposition started from 2 ,
    adposition: 2,
    articleshow: {
      2: { mstype: "ctnmidart", screen_ratio: isMobilePlatform() ? 2 : 0.2 },
      // 1: { mstype: 'mrec', screen_ratio: .2 }, //off this as we implemented parallax in MREC
      default: { mstype: "mrec2", screen_ratio: 1 },
    },
    // For test page only
    articleshow_esi_test: {
      2: { mstype: "ctnmidart_test", screen_ratio: 2 },
      1: { mstype: "mrec1", screen_ratio: 0.2 },
      default: { mstype: "mrec2", screen_ratio: 1 },
    },
  };

  const _container =
    article && typeof article === "object"
      ? article
      : article && article != ""
      ? document.getElementById(article)
      : document.querySelector("[data-artbox]");
  const story_tag = _container ? _container.querySelector(config.defaultselector) : undefined;
  let adcounter = config.adposition;
  let mstype = "";
  let elem = {};
  let offsetTop = 0;
  let last_offsetTop = 0;
  let screen_ratio = 0;
  const mrecMidartElem = null;
  let gapBtwElemArtEnd = 0;

  if (typeof story_tag === "undefined" || typeof story_tag === "null") {
    return false;
  }

  const alreadyPresentMidArts = story_tag.getElementsByClassName("midArtContainer");

  if (alreadyPresentMidArts && alreadyPresentMidArts.length > 0) {
    return false;
  }

  pagetype = pagetype && pagetype != "" ? pagetype : "articleshow";
  mstype = config[pagetype].default.mstype;

  const ad_pagetype =
    pagetype === "articleshow_esi_test" ? "articleshow" : pagetype && pagetype !== "" ? pagetype : "articleshow";
  let adName = window.wapads[mstype] ? window.wapads[mstype].name : siteConfig.ads.dfpads[ad_pagetype][mstype].name;
  adName = JSON.parse(JSON.stringify(adName));
  const maxNameCount = 3;
  let adNameIndex = 2;
  let enableCtnOsv = false;

  if (window && window.dfpAdConfig && window.dfpAdConfig.enableCtnOsv && window.dfpAdConfig.enableCtnOsv === "true") {
    enableCtnOsv = true;
  }
  for (let i = 0; i < story_tag.childElementCount; i++) {
    elem = story_tag.children[i];
    offsetTop = elem.getBoundingClientRect().y - story_tag.getBoundingClientRect().y;
    gapBtwElemArtEnd = story_tag.clientHeight - offsetTop;

    if (adcounter == 1 || adcounter == 2) {
      mstype = config[pagetype][adcounter].mstype;
      screen_ratio = config[pagetype][adcounter].screen_ratio;
    } else {
      mstype = config[pagetype].default.mstype;
      screen_ratio = config[pagetype].default.screen_ratio;
    }
    // compare elem offsettop with screen height + last adposition ctnmidart
    if (
      gapBtwElemArtEnd >= 100 && // it is introduce to avoid adding Ads at end of story
      offsetTop > screen.height * screen_ratio + last_offsetTop
    ) {
      const adParent = document.createElement("div");
      const adelem = document.createElement("div");

      adParent.append(adelem);
      last_offsetTop = offsetTop;
      adelem.setAttribute("class", `ad1 ${enableCtnOsv ? mstype : "osv"}`);

      // if (adcounter == 1 || adcounter == 2) {
      if (adcounter == 2) {
        if (enableCtnOsv) {
          // adelem.setAttribute("data-plugin", "ctn");
          adelem.setAttribute("data-name", siteConfig.ads.dfpads[pagetype].osv.name);
          adelem.setAttribute("data-adtype", "osv");
          adelem.setAttribute("class", "ad1 osv");
          adelem.setAttribute("data-videoarticle", flag);
        } else {
          adelem.setAttribute("data-plugin", "ctn");
          adelem.setAttribute("data-videoarticle", flag);
          adelem.setAttribute("data-videoalign", "left");
          adelem.setAttribute("data-slot", mstype);
          // if (window.dfp_over_ctn && process.env.SITE === "nbt") {
          //   adelem.setAttribute("data-dfpslot", "/7176/MT_MWeb/MT_MWeb_Test/MT_Mweb_Test_AS_Bid_Experiment");
          //   adelem.setAttribute("data-type", "dfp");
          // }
        }

        adParent.classList.add("ad-wrapper-250");
        elem.before(adParent);
        config.should_initCTN = true;
      } else {
        adParent.classList.add("midArtContainer");
        adelem.setAttribute("data-adtype", mstype);
        adelem.setAttribute("data-name", `${adName.replace("mrec2", "mrec")}${adNameIndex}`);
        adNameIndex = "INF";
        // if (adNameIndex >= maxNameCount || adNameIndex === "INF") {
        //   adNameIndex = "INF";
        // } else {
        //   adNameIndex += 1;
        // }
      }

      if (elem.nodeName === "SECTION" && elem.querySelector("p")) {
        elem.querySelector("p").before(adParent);
      } else {
        elem.before(adParent);
      }
      config.should_initGPT = true;

      // initiate counter
      adcounter += 1;
    }
  }

  // render multiplex ads logic
  renderMultiPlexAd(_container, pagetype);

  // initate CTN ads
  if (config.should_initCTN) {
    CTN_module.render({ pathname: articleUrl || "undefined" });
  }

  // initate CTN ads
  if (config.should_initGPT) {
    // Render All DFP ads except MREC
    Ads_module.render({ pathname: articleUrl || "undefined" });
  }
};

// Private Function for Multiplex ads
const renderMultiPlexAd = (_container, pagetype) => {
  // Collect cookie for geo_data
  let cookie_geo_data = window._getCookie ? window._getCookie("geo_data") : "";
  const adelem = document.createElement("div");
  const _adCANContainer = _container.querySelector(".adCANContainer");
  // Add the new DIV in CAN ad container
  _adCANContainer ? _adCANContainer.appendChild(adelem) : "";
  try {
    if (cookie_geo_data !== "") {
      cookie_geo_data = JSON.parse(cookie_geo_data);
      // For India run ctn , rest Multiplex
      if (cookie_geo_data.CountryCode === "IN") {
        // For CTN
        adelem.setAttribute("data-plugin", "ctn");
        adelem.setAttribute("data-slot", "ctnshow");
        if (pagetype === "articleshow_esi_test" && window.dfp_over_ctn && process.env.SITE === "nbt") {
          adelem.setAttribute(
            "data-dfpslot",
            "/7176/NBT_MWeb/NBT_MWeb_Bid_Experiment/NBT_Mweb_ROS_AS_EOA_Bid_Experiment_300",
          );
          adelem.setAttribute("data-type", "dfp");
          _adCANContainer.classList.remove("adCANContainer");
        }
        CTN_module.render({}, adelem);
      } else {
        // For Bottomwidget , hit directly DFP codes
        const _id = `div-gpt-ad-1543293292770-bottom-${_container.getAttribute("id")}`;
        adelem.setAttribute("id", _id);
        googletag.cmd.push(() => {
          // handle empty case for Multiplex and backfill with ctn
          const slotRenderEndedFn = event => {
            if (event && event.slot) {
              const slotName = event.slot.getAdUnitPath().toLowerCase();
              const _adelem = document.getElementById(event.slot.getSlotElementId());
              // get elem by slotId and if once ctn rendering done , this id removed from element
              // it helped to hinder reentry to for ctnRendering if bottomWidget ads empty
              // TODO - its a hack as we dont hv any function remove lisentner('slotRenderEnded') of pubads
              if (_adelem && event.isEmpty && slotName.indexOf("mweb_ros_bottomwidget") > -1) {
                _adelem.setAttribute("data-plugin", "ctn");
                _adelem.setAttribute("data-slot", "ctnshow");
                _adelem.innerHTML = "";
                _adelem.style.display = "block";
                _adelem.parentElement.style.display = "block";
                CTN_module.render({}, _adelem);
              }
            }
          };
          googletag.pubads().addEventListener("slotRenderEnded", slotRenderEndedFn, true);
          googletag.defineSlot(siteConfig.ads.dfpads.multiplex, _id).addService(googletag.pubads());
          googletag.display(_id);
        });
      }
    } else {
      adelem.setAttribute("data-plugin", "ctn");
      adelem.setAttribute("data-slot", "ctnshow");
      CTN_module.render({}, adelem);
    }
  } catch (e) {
    adelem.setAttribute("data-plugin", "ctn");
    adelem.setAttribute("data-slot", "ctnshow");
    CTN_module.render({}, adelem);
  }
};

function refreshAllAdsInContainer({ container, shouldRefreshDfp, shouldRefreshCTN, forcefulRefresh }) {
  // if container is not provided search all ads on the document
  const queryContainer = container || document;
  let dfpAdsArray;
  let ctnAdsArray;

  if (shouldRefreshDfp) {
    dfpAdsArray = queryContainer.querySelectorAll(".ad1");
    if (dfpAdsArray && dfpAdsArray.length > 0) {
      for (let i = 0; i < dfpAdsArray.length; i++) {
        if (checkValidDfpToRefresh(dfpAdsArray[i])) {
          if (dfpAdsArray[i].getAttribute("data-refresh") || forcefulRefresh) {
            Ads_module.refreshAds([dfpAdsArray[i].getAttribute("data-id")], true);
          } else {
            dfpAdsArray[i].setAttribute("data-refresh", true);
          }
        }
      }
    }
  }

  if (shouldRefreshCTN) {
    ctnAdsArray = queryContainer.querySelectorAll(`div [data-plugin="ctn"]`);
    if (ctnAdsArray && ctnAdsArray.length > 0) {
      for (let i = 0; i < ctnAdsArray.length; i++) {
        if (checkValidCtnToRefresh(ctnAdsArray[i])) {
          if (ctnAdsArray[i].getAttribute("data-refresh") || forcefulRefresh) {
            colombia.refresh(ctnAdsArray[i].getAttribute("id"));
          } else {
            ctnAdsArray[i].setAttribute("data-refresh", true);
          }
        }
      }
    }
  }
}

function checkValidDfpToRefresh(dfpAd) {
  let isDfpValidToRefresh = false;
  if (
    // skip empty adContainers
    dfpAd &&
    dfpAd.innerHTML !== "" &&
    // skip fbn
    dfpAd.classList &&
    dfpAd.classList.value.indexOf("fbn") === -1 &&
    // skip if attribute 'ad-direct' present
    !dfpAd.getAttribute("ad-direct")
  ) {
    isDfpValidToRefresh = true;
  }
  return isDfpValidToRefresh;
}

// put validation code here for ctn ads
function checkValidCtnToRefresh(ctnad) {
  let isCtnValidToRefresh = false;
  if (ctnad.getAttribute("id")) {
    isCtnValidToRefresh = true;
  }
  return isCtnValidToRefresh;
}

export function fetchAdvertorialNews(_this, alaskaData) {
  if (alaskaData) {
    const advertorialNews = [];
    const advertorialObj = filterAlaskaData(alaskaData, ["pwaconfig", "advertorial"], "label");
    _this.config.advInvoked = true;
    if (advertorialObj && typeof advertorialObj === "object") {
      for (const key in advertorialObj) {
        if ({}.hasOwnProperty.call(advertorialObj, key)) {
          const item = advertorialObj[key];
          if (item && item._text) {
            advertorialNews.push(item);
          }
        }
      }
    }
    _this.setState({
      advertorialNews,
    });
  }
}
