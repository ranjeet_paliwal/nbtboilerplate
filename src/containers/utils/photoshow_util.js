import fetch from "../../utils/fetch/fetch";
/**
 * @description Utility common methot to fetch related photogallery data (RHS on desktop , bottom widget Mobile)
 * @param {*} galleryId The gallery id to be fetched
 * @param {*} _this  The reference of this to the component where the setState will occur ( to hold CSR DATA )
 * @author Abhinav Mishra
 */
export const fetchRecommendedPhotoSection = (galleryId, _this) => {
  const parsedGalleryId =
    typeof galleryId === "string" && galleryId.includes("msid-")
      ? galleryId.slice(galleryId.indexOf("msid-") + 5, galleryId.indexOf(","))
      : galleryId;
  fetch(`${process.env.API_BASEPOINT}/sc_related_photos/${parsedGalleryId}.cms?feedtype=sjson`)
    .then(rhsWidgetData => {
      if (rhsWidgetData && Array.isArray(rhsWidgetData.stry)) {
        let filteredWidgetData = rhsWidgetData;
        // Filtering data logic
        // 0  -> Next gal
        // 1  -> Prev gal
        // Next 5 main gallery show only 2 records (should not be next / prev / current)
        const nextGalData = rhsWidgetData.stry[0];
        const prevGalData = rhsWidgetData.stry[1];
        // Remove prevGalData , nextGalData, currentGallery from stories in widget
        if (prevGalData && nextGalData) {
          filteredWidgetData.stry = filteredWidgetData.stry.filter(
            widgetData =>
              widgetData.id !== prevGalData.id && widgetData.id !== nextGalData.id && widgetData.id !== galleryId,
          );
        }

        // Re add after filtering ( to only remove duplicates)
        if (nextGalData) {
          filteredWidgetData.stry = [nextGalData, ...filteredWidgetData.stry];
        }

        if (prevGalData) {
          filteredWidgetData.stry = [prevGalData, ...filteredWidgetData.stry];
        }

        // Concatenate already present data with current data fetched
        _this.setState({
          recommendedWidgetData: [..._this.state.recommendedWidgetData, filteredWidgetData],
        });
      }
    })
    .catch(e => console.log("Error fetching CSR RHS DATA"));
};

export const nextGalLink = (item, pagetype) => {
  let linkNextGal = "";
  if (item.pwa_meta && item.pwa_meta.nextGal) {
    if (item.pwa_meta.nextGal.url) {
      linkNextGal = item.pwa_meta.nextGal.url;
    } else if (item.pwa_meta.nextGal.seolocation) {
      linkNextGal = `/${item.pwa_meta.nextGal.seolocation}/${pagetype}/${item.pwa_meta.nextGal.msid}.cms`;
    }
  }
  linkNextGal = linkNextGal + "?utm_source=nextstory&utm_medium=referral&utm_campaign=photoshow";
  return linkNextGal;
};
