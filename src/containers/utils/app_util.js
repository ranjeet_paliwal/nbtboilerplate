import { fireGRXEvent, setGRXParameter } from "../../components/lib/analytics/src/ga";
import { _isCSR, getNetworkSpeed, getPageTypeUpdated } from "../../utils/util";

// Can't be placed in util.js due to cyclic dependancy arising
/**@description Helper function to fire grx source utm events - from Desktop / Mobile App.js
 * @param query query in the form of object which contains the utm sources */

export function fireUTMSourceGRX(query) {
  if (query && typeof query == "object" && "utm_source" in query) {
    const { utm_source } = query;
    const utmEventLabelMap = {
      // Add to home screen users come with this utm
      pwahomescreen: "add_to_home_screen",
      // Newsletter subscribed users to home screen users come with this utm
      newsletter: "subscribe_newsletter",
      dailynewsletter: "subscribe_newsletter",
      Newsletter: "subscribe_newsletter",
      // Subscription based users come through these utm_sources
      izooto: "push_subscription_izooto",
      wapbellchromenotification: "push_subscription_izooto",
      push_notification: "push_subscription_izooto",
      "nbtnotifications.indiatimes.com ": "push_subscription_izooto",
      browser_notification: "push_subscription_izooto",
    };
    if (utm_source in utmEventLabelMap) {
      fireGRXEvent("track", utmEventLabelMap[utm_source]);
    }
  }
}

/**
 * @description Attaches the event listener which fires grx event on izooto dismissal
 */
export const addListenerForIzootoDismissal = () => {
  if (_isCSR()) {
    // 3 -> Use clicks on block 6-> Prompt closed 7-> Clicked on later
    const fireGRXEventOnDismiss = data => {
      if (window.notification_fired) {
        return;
      }
      if (
        data &&
        data.detail &&
        (data.detail.statuscode == 3 || data.detail.statuscode == 6 || data.detail.statuscode == 7)
      ) {
        fireGRXEvent("track", "browser_notifications", { status: "no" });
      } else if (data && data.detail && (data.detail.statuscode == 1 || data.detail.statuscode == 9)) {
        fireGRXEvent("track", "browser_notifications", { status: "yes" });
      }
      window.notification_fired = true;
    };
    // Attach event
    document.addEventListener("browserNotification", fireGRXEventOnDismiss);
  }
};

/**
 * @description Central function for firing network_speed event (in mbps)
 */

export const setGRXNetworkSpeed = () => {
  // Calculate network speed if not calculated, fire event
  const fireEvent = networkSpeed => {
    const speed = networkSpeed || "NA";
    setGRXParameter("network_speed", speed);
    if (_isCSR()) {
      if (window.navigator && window.navigator.connection) {
        const networkInformation = navigator.connection;
        if (networkInformation) {
          if (networkInformation.downlink) {
            setGRXParameter("network_browser_speed", networkInformation.downlink);
          }
          if (networkInformation.effectiveType) {
            setGRXParameter("network_browser_effective_type", networkInformation.effectiveType);
          }
        }
      }
    }
  };
  getNetworkSpeed(fireEvent);
};
