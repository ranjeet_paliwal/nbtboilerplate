import { AnalyticsGA } from "../../components/lib/analytics/index";

export function getDeviceObject(deviceCount) {
  const deviceInfoParam = {};
  if (!deviceCount) {
    deviceCount = 4;
  }
  for (var i = 1; i <= deviceCount; i++) {
    deviceInfoParam[`device${i}`] = {
      isVisible: true,
      searchResult: "",
      userInput: "",
      userInputHtml: "",
      seoName: "",
    };
  }
  return deviceInfoParam;
}

export function getGadgetPrice(gadgetNode) {
  let price = "";
  if (gadgetNode && gadgetNode.keyFeatures && gadgetNode.keyFeatures.price_in_india) {
    price = gadgetNode.keyFeatures.price_in_india.value;
  } else if (
    gadgetNode &&
    gadgetNode.keyFeatures &&
    gadgetNode.keyFeatures.keyFeatures &&
    gadgetNode.keyFeatures.keyFeatures.price_in_india
  ) {
    price = gadgetNode.keyFeatures.keyFeatures.price_in_india.value;
  } else if (
    gadgetNode &&
    gadgetNode.keyFeatures &&
    gadgetNode.keyFeatures.affiliate &&
    gadgetNode.keyFeatures.affiliate.exact
  ) {
    const affiliateNode = gadgetNode.keyFeatures.affiliate.exact;
    if (Array.isArray(affiliateNode)) {
      price = affiliateNode[0].sort_price;
    } else {
      price = affiliateNode.sort_price;
    }
  } else if (gadgetNode && gadgetNode.price) {
    price = gadgetNode.price;
  }

  if (price) {
    price = "₹ " + parseInt(price).toLocaleString("en-IN");
  }

  return price;
}

export function invokeGaComscore(lcuname) {
  const pdpConatiner = document.querySelector(`#pdp-${lcuname}`);
  if (pdpConatiner && !pdpConatiner.getAttribute("comscorepageview")) {
    AnalyticsGA.pageview(window.location.pathname, {
      setEditorName: true,
      pageCountInc: true,
      comscorepageview: pdpConatiner.getAttribute("comscorepageview"),
      // comscorepageview: _article.getAttribute("comscorepageview"),
    });

    pdpConatiner.setAttribute("comscorepageview", true);
  }
}

export function getPWAMeta(gadgetData) {
  return gadgetData && gadgetData.newsData && gadgetData.newsData.pwa_meta;
}
