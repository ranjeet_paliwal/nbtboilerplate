/* eslint-disable no-shadow */

" use strict ";

import { AnalyticsGA } from "../../components/lib/analytics/index";
import { fetchTopListDataIfNeeded, setRefreshHomeFeed, removeRefreshHomefeed } from "../../actions/home/home";

// export function trackHomePageScrollDepth(obs, rowCache) {
//   return rowsToObserve => {
//     for (const rowKey in rowsToObserve) {
//       if (!(rowKey in rowCache)) {
//         const rowElem = document.querySelector(rowKey);
//         if (rowElem) {
//           obs.observe(rowElem);
//         }
//         rowCache[rowKey] = true;
//       }
//     }
//   };
// }

// export function createAndCacheObserver(
//   context,
//   config = {
//     root: null,
//     rootMargin: "0px",
//     threshold: 0.2, // when 20% in view
//   },
//   callback = (entries, self) => {
//     entries.forEach(entry => {
//       if (entry && entry.isIntersecting) {
//         console.log("observed");
//       }
//     });
//   },
// ) {
//   try {
//     const observer = createInterSectionObserver(callback, config);
//     context.IObserver = observer;
//   } catch (e) {
//     console.log("Error creating observer");
//   }
// }

export function clearFeedInterval(refreshFeedInterval, dispatch) {
  if (refreshFeedInterval) {
    clearInterval(refreshFeedInterval);
    dispatch(removeRefreshHomefeed());
  }
}

export function setRefreshFeedInterval(timeInSecs, refreshFeedInterval, dispatch, pagetype, router) {
  const time = timeInSecs * 1000;
  const shouldMakePageView = pagetype && pagetype === "articleshow" ? false : true;
  clearFeedInterval(refreshFeedInterval, dispatch);
  const intervalId = setInterval(() => {
    dispatch(fetchTopListDataIfNeeded("", "", router, shouldMakePageView));
  }, time);
  dispatch(setRefreshHomeFeed(intervalId));
}
