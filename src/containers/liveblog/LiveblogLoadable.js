import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import Loadable from "react-loadable";
import LiftFetchData from "components/lib/liftfetchdata/liftFetchData";
import Loader from "components/lib/loader/Loader";
import Error from "components/lib/error/Error";

const webpackRequireWeakId = () => require.resolveWeak("./Liveblog");

const LoadingComponent = ({ isLoading, error, pastDelay }) => {
  if (isLoading && pastDelay) {
    return <Loader />;
  } else if (error) {
    return <Error>Error!!!</Error>;
  }
  return null;
};

LoadingComponent.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  pastDelay: PropTypes.bool.isRequired,
  error: PropTypes.bool
};

const enhance = compose(LiftFetchData(webpackRequireWeakId), Loadable);

export default enhance({
  loader: () => import("./Liveblog"),
  LoadingComponent,
  webpackRequireWeakId
});
