import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import { fetchLIVEBLOG_IfNeeded, fetchLiveBlogCount } from "../../actions/liveblog/liveblog";
import ImageCard from "./../../components/common/ImageCard/ImageCard";
import FakeNewsListCard from "./../../components/common/FakeCards/FakeStoryCard";
import LiveblogCard from "./../../components/common/LiveblogCard";
import HighlightCard from "./../../components/common/HighlightCard";
import Breadcrumb from "./../../components/common/Breadcrumb";

import styles from "./../../components/common/css/liveblog.scss";
import AnchorLink from "./../../components/common/AnchorLink";
import { PageMeta, SeoSchema } from "./../../components/common/PageMeta";
import {
  _isCSR,
  setHyp1Data,
  _getStaticConfig,
  isMobilePlatform,
  filterAlaskaData,
  _isOPPO,
  _isVivo,
  _isSamsung,
  loadInstagramJS,
} from "../../utils/util";
import SocialShare from "./../../components/common/SocialShare";
import { AnalyticsGA } from "./../../components/lib/analytics";

import { setPageType } from "./../../actions/config/config";
import { setSectionDetail } from "./../../components/lib/ads";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import RecommendedNewsWidget from "./../../components/common/RecommendedNewsWidget";
import AdCard from "./../../components/common/AdCard";
import GridSectionMaker from "./../../components/common/ListingCards/GridSectionMaker";
import { defaultDesignConfigs } from "../defaultDesignConfigs";

// import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import RhsWidget from "./../../components/desktop/RhsWidget";
import fetch from "../../utils/fetch/fetch";
import SvgIcon from "../../components/common/SvgIcon";
const siteConfig = _getStaticConfig();

const taswidgetNBTPath = "https://navbharattimes.indiatimes.com/pwafeeds/amazon_wdt_tas_new.cms";
const taswidgetMTPath = "https://maharashtratimes.com/pwafeeds/amazon_wdt_tas_new.cms";

class Liveblog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rhsCSRData: null,
      rhsCSRVideoData: null,
      index: 0,
      // superHitWidgetData: null,
      advertorialNews: [],
    };
    this.config = {
      lbcount: 0,
      lbupdatecount: 0,
      poolInterval: 45, //unit in sec's
      elemUpdateBtn: null,
    };
  }

  componentDidMount() {
    const { dispatch, params, query, lbcontent } = this.props;
    const pwa_meta = this.props.value && this.props.value.pwa_meta;

    //hack for reset ads section
    // if (this.props.value && this.props.value.pwa_meta && this.props.value.pwa_meta) {
    //   setSectionDetail(this.props.value.pwa_meta);
    // }
    //window.waitForAdsEvent = "false";
    //hack for reset ads section
    Liveblog.fetchData({ dispatch, params, query }).then(data => {
      let _data = data && data.payload && data.payload instanceof Array ? data.payload[0] : [];
      if (_data && _data.pwa_meta) {
        setSectionDetail(_data.pwa_meta);
      }
    });

    if (typeof window != undefined) {
      this.shouldUpdateLiveblog();
    }

    if (typeof loadInstagramJS == "function") {
      loadInstagramJS();
    }

    //set hyp1 variable
    pwa_meta ? setHyp1Data(pwa_meta) : "";

    //twitter widget can be moved from here
    const twitter = document.createElement("script");
    twitter.src = "https://platform.twitter.com/widgets.js";
    twitter.defer = true;

    document.head.appendChild(twitter);
    // video data  Api call for AL page
    // const videoId =
    //   (this.props.value && this.props.value.pwa_meta && this.props.value.pwa_meta.relatedvideomapid) || "";
    const relatedvideomapid =
      pwa_meta && pwa_meta.parentid
        ? pwa_meta.parentid
        : pwa_meta && pwa_meta.sectionid
        ? pwa_meta.sectionid
        : pwa_meta && pwa_meta.navsecid;
    const relatedvideoApi = `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${relatedvideomapid}&tag=video&perpage=6&feedtype=sjson&pagetype=videoshow"
    }`;
    fetch(relatedvideoApi)
      .then(data => {
        this.setState({
          rhsCSRVideoData: data,
        });
      })
      .catch({});

    // trendingApi videos comes from here in RHS / mobile bottom
    const trendingId = (this.props.value && this.props.value.pwa_meta && this.props.value.pwa_meta.parentid) || "";
    const trendingApi = `${process.env.API_BASEPOINT}/web_common.cms?feedtype=sjson&platform=web&msid=${trendingId}&tag=ibeatmostread,mostpopularL2,mostpopularL1,trending`;
    fetch(trendingApi)
      .then(data => {
        this.setState({
          rhsCSRData: data,
        });
      })
      .catch({});

    // Superhit widget comes in CSR (bottom widget on desktop/mobile)
    // const parentId = (this.props.value && this.props.value.pwa_meta && this.props.value.pwa_meta.parentid) || "";
    // const superHitApi = `${process.env.API_BASEPOINT}/sc_superhitwidget.cms?msid=${parentId}&tag=ibeatmostread&type=articleshow&feedtype=sjson`;
    // fetch(superHitApi)
    //   .then(data => {
    //     this.setState({
    //       superHitWidgetData: data,
    //     });
    //   })
    //   .catch({});
  }
  componentWillUnmount() {
    //reset hyp1 to ''
    setHyp1Data();
    //reset section to ''
    setSectionDetail();
  }

  shouldUpdateLiveblog() {
    const _this = this;
    const { dispatch, params, query, location } = _this.props;
    setInterval(function() {
      AnalyticsGA.pageview(location.pathname, {
        forcefulGaPageview: true,
      });

      if (_this.props.value && _this.props.value.pwa_meta && _this.props.value.pwa_meta.cricketlb) {
        _this.config.elemUpdateBtn = document.querySelector(".btn_newupdates");
        _this.config.elemUpdateBtn.innerHTML = " New Updates"; //set total new content count
        _this.config.elemUpdateBtn.style.display = "inline-block"; //show update button
      } else {
        fetchLiveBlogCount(dispatch, params, query)
          .then(data => {
            _this.config.elemUpdateBtn = document.querySelector(".btn_newupdates");
            //for first time update all config counts
            if (data && _this.config.lbcount == 0) {
              _this.config.lbcount = parseInt(data.count);
              _this.config.lbupdatecount = parseInt(data.count);
            } else if (_this.config.elemUpdateBtn && data && _this.config.lbcount < parseInt(data.count)) {
              _this.config.elemUpdateBtn.innerHTML = parseInt(data.count) - _this.config.lbupdatecount + " New Updates"; //set total new content count
              _this.config.elemUpdateBtn.style.display = "inline-block"; //show update button

              _this.config.lbcount = parseInt(data.count); // update lbcount to new-count

              // GTM Call Needs to fire here
            }
          })
          .catch(err => {
            console.log("Inside catch", err);
          });
      }
    }, _this.config.poolInterval * 1000);
  }

  updateLiveblog() {
    const _this = this;
    const { dispatch, params, query } = this.props;
    dispatch(fetchLIVEBLOG_IfNeeded(params, query));

    //update count value on click
    _this.config.lbupdatecount = _this.config.lbcount;
    document.querySelector("ul.livepost li").scrollIntoView({ behavior: "smooth", block: "center" });
    _this.config.elemUpdateBtn.style.display = "none"; //show update button
  }

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;

    let sharedata = {
      title: document.title,
      url: location.href,
    };

    if (!isShareAll) return sharedata;

    //for watsapp sharing feature along with GA
    AnalyticsGA.event({
      category: "social",
      action: "Whatsapp_Wap_AS",
      label: sharedata.url,
    });

    var info = sharedata.url;
    info += "?utm_source=whatsapp" + siteConfig.fullutm;
    var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + info;
    window.location.href = whatsappurl;

    return false;
  }

  render() {
    let { value, isFetching, header, router } = this.props;
    let { lbcontent } = value && value != undefined ? value : {};
    const alaskaData = header && header.alaskaData;
    const pwaConfigAlaska = filterAlaskaData(alaskaData, ["pwaconfig"], "label");
    let isOPPO = _isOPPO(this.props.router);
    let isVivo = _isVivo(this.props.router);
    let isSamsung = _isSamsung(this.props.router);
    // const { rhsCSRData } = this.state;
    const { rhsCSRData, advertorialNews, rhsCSRVideoData } = this.state;
    let maxItems = 100,
      post = [],
      highlights = [],
      maxHighlights = 5,
      count = 0;
    let pagetype = "liveblog";
    let meta = lbcontent != undefined ? this.props.value.pwa_meta : {};
    // changing publishcontent key for page meta

    meta.publishcontent = meta && meta.publishcontent && meta.publishcontent.split("#")[0];
    meta.modifiedcontent = lbcontent && lbcontent.length > 0 && lbcontent[0].article_date;

    let breadcrumbData =
      this.props.value &&
      this.props.value.breadcrumb &&
      this.props.value.breadcrumb.div &&
      this.props.value.breadcrumb.div.ul
        ? this.props.value.breadcrumb.div.ul
        : null;

    let { query } = this.props.location;

    if (_isCSR()) {
      maxItems = meta.cricketlb ? 1000 : 100;
    }

    if (lbcontent != undefined) {
      lbcontent.some(function(item, index) {
        if (index < maxItems) post.push(item);
        if (item.keyevent == 1 && count < maxHighlights && index < maxItems) {
          highlights.push(item);
          count++;
        }
      });
    }
    // const endDate = getEndDtate(meta.updateddate);

    const endDate = lbcontent && lbcontent.length > 0 && lbcontent[0].updateDate;

    return lbcontent != undefined ? (
      <div
        className={styles.lb_container + " lb_container"}
        {...SeoSchema({ pagetype: pagetype, url: meta.canonical }).attr().liveBlog}
      >
        {/* For SEO */ PageMeta(meta)}
        {SeoSchema({ pagetype: pagetype }).metaUrl(meta.canonical)}
        {SeoSchema().metaTag({
          name: "datePublished",
          content: meta.createdate.split("#")[1],
        })}
        {SeoSchema().metaTag({
          name: "dateModified",
          content: endDate,
        })}
        {SeoSchema().metaTag({
          name: "coverageStartTime",
          content: meta.createdate.split("#")[1],
        })}
        {SeoSchema().metaTag({
          name: "coverageEndTime",
          content: endDate,
        })}
        {/* about schema for live blog */}
        <span itemProp="about" itemScope="itemscope" itemType="https://schema.org/Event">
          {SeoSchema().metaTag({
            name: "name",
            content: meta.title,
          })}
          {SeoSchema().metaTag({
            name: "startDate",
            content: meta.createdate.split("#")[1],
          })}
          {SeoSchema().metaTag({
            name: "endDate",
            content: endDate,
          })}
          {SeoSchema().metaTag({
            name: "description",
            content: meta.desc,
          })}
          {SeoSchema().metaTag({
            name: "eventAttendanceMode",
            content: "mixed",
          })}
          {SeoSchema().metaTag({
            name: "eventStatus",
            content: meta.livestatus,
          })}
          <meta
            itemprop="image"
            content={`https://navbharattimes.indiatimes.com/thumb/msid-${meta.imgmsid},width-1200,height-900/pic.jpg`}
          />
          <span itemprop="location" itemscope="itemscope" itemtype="https://schema.org/Place">
            {SeoSchema().metaTag({
              name: "location",
              content: "India",
            })}
            {SeoSchema().metaTag({
              name: "name",
              content: "India",
            })}
            <span itemprop="address" itemscope="itemscope" itemtype="https://schema.org/PostalAddress">
              {SeoSchema().metaTag({
                name: "address",
                content: "India",
              })}
              {SeoSchema().metaTag({
                name: "name",
                content: "India",
              })}
            </span>
          </span>
        </span>
        {/* Breadcrumb  */}
        {!isMobilePlatform() && breadcrumbData ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumbData} />
          </ErrorBoundary>
        ) : null}
        <div className="row">
          <div className={isMobilePlatform() ? "col12" : "col8"}>
            {
              <React.Fragment>
                <div className={`lb_summaryCard ${isMobilePlatform() ? "" : "desktop"}`}>
                  <div
                    className={
                      meta.cricketlb && value && value.tinyscore ? "lb_top_summary width_scorecard" : "lb_top_summary"
                    }
                  >
                    {meta.imgmsid != undefined && meta.imgmsid != "" ? (
                      <ImageCard className="lbimg" msid={meta.imgmsid} size="smallthumb" imgsize={meta.imgsize || ""} />
                    ) : (
                      <ImageCard
                        className="lbimg"
                        msid={meta.parentid}
                        size="smallthumb"
                        imgsize={meta.imgsize || ""}
                      />
                    )}
                    <span className="source">
                      {" "}
                      <span className="liveblink"></span>{" "}
                      {meta && meta.livestatus && meta.livestatus === "Live" ? (
                        <span className="live">Live - </span>
                      ) : null}{" "}
                      {lbcontent && lbcontent.length > 0 && lbcontent[0].article_date}
                    </span>
                    {
                      <h1 {...SeoSchema({ pagetype: pagetype }).attr().pageheadline}>
                        {meta && meta.h2 ? meta.h2 : meta.heading}
                      </h1>
                    }
                    {meta.seodescription != "" ? (
                      <span className="caption" {...SeoSchema({ pagetype: pagetype }).attr().description}>
                        {meta.seodescription}
                      </span>
                    ) : (
                      ""
                    )}
                    {_isCSR() && isMobilePlatform() ? (
                      ReactDOM.createPortal(
                        <div className="share_container lbshareicons">
                          {isMobilePlatform() ? (
                            <div className="share_whatsapp">
                              <a
                                rel="nofollow"
                                onClick={this.getShareDetail.bind(this, true)}
                                href="javascript:void(0);"
                              >
                                <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                  <path
                                    d="M20.1,3.9C17.9,1.7,15,0.5,12,0.5C5.8,0.5,0.7,5.6,0.7,11.9c0,2,0.5,3.9,1.5,5.6l-1.6,5.9l6-1.6c1.6,0.9,3.5,1.3,5.4,1.3l0,0l0,0c6.3,0,11.4-5.1,11.4-11.4C23.3,8.9,22.2,6,20.1,3.9z M12,21.4L12,21.4c-1.7,0-3.3-0.5-4.8-1.3l-0.4-0.2l-3.5,1l1-3.4L4,17c-1-1.5-1.4-3.2-1.4-5.1c0-5.2,4.2-9.4,9.4-9.4c2.5,0,4.9,1,6.7,2.8c1.8,1.8,2.8,4.2,2.8,6.7C21.4,17.2,17.2,21.4,12,21.4z M17.1,14.3c-0.3-0.1-1.7-0.9-1.9-1c-0.3-0.1-0.5-0.1-0.7,0.1c-0.2,0.3-0.8,1-0.9,1.1c-0.2,0.2-0.3,0.2-0.6,0.1c-0.3-0.1-1.2-0.5-2.3-1.4c-0.9-0.8-1.4-1.7-1.6-2c-0.2-0.3,0-0.5,0.1-0.6s0.3-0.3,0.4-0.5c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0-0.4,0-0.5c0-0.1-0.7-1.5-1-2.1C8.9,6.6,8.6,6.7,8.5,6.7c-0.2,0-0.4,0-0.6,0S7.5,6.8,7.2,7c-0.3,0.3-1,1-1,2.4s1,2.8,1.1,3c0.1,0.2,2,3.1,4.9,4.3c0.7,0.3,1.2,0.5,1.6,0.6c0.7,0.2,1.3,0.2,1.8,0.1c0.6-0.1,1.7-0.7,1.9-1.3c0.2-0.7,0.2-1.2,0.2-1.3C17.6,14.5,17.4,14.4,17.1,14.3z"
                                    stroke="none"
                                    fill="#fff"
                                  />
                                </svg>
                              </a>
                            </div>
                          ) : null}
                          <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                        </div>,
                        document.getElementById("share-container-wrapper"),
                      )
                    ) : (
                      <div className={`share_container lbshareicons ${_isCSR() ? "" : "hideFooter"}`}>
                        {isMobilePlatform() ? (
                          <div className="share_whatsapp">
                            <a rel="nofollow" onClick={this.getShareDetail.bind(this, true)} href="javascript:void(0);">
                              <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                  d="M20.1,3.9C17.9,1.7,15,0.5,12,0.5C5.8,0.5,0.7,5.6,0.7,11.9c0,2,0.5,3.9,1.5,5.6l-1.6,5.9l6-1.6c1.6,0.9,3.5,1.3,5.4,1.3l0,0l0,0c6.3,0,11.4-5.1,11.4-11.4C23.3,8.9,22.2,6,20.1,3.9z M12,21.4L12,21.4c-1.7,0-3.3-0.5-4.8-1.3l-0.4-0.2l-3.5,1l1-3.4L4,17c-1-1.5-1.4-3.2-1.4-5.1c0-5.2,4.2-9.4,9.4-9.4c2.5,0,4.9,1,6.7,2.8c1.8,1.8,2.8,4.2,2.8,6.7C21.4,17.2,17.2,21.4,12,21.4z M17.1,14.3c-0.3-0.1-1.7-0.9-1.9-1c-0.3-0.1-0.5-0.1-0.7,0.1c-0.2,0.3-0.8,1-0.9,1.1c-0.2,0.2-0.3,0.2-0.6,0.1c-0.3-0.1-1.2-0.5-2.3-1.4c-0.9-0.8-1.4-1.7-1.6-2c-0.2-0.3,0-0.5,0.1-0.6s0.3-0.3,0.4-0.5c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0-0.4,0-0.5c0-0.1-0.7-1.5-1-2.1C8.9,6.6,8.6,6.7,8.5,6.7c-0.2,0-0.4,0-0.6,0S7.5,6.8,7.2,7c-0.3,0.3-1,1-1,2.4s1,2.8,1.1,3c0.1,0.2,2,3.1,4.9,4.3c0.7,0.3,1.2,0.5,1.6,0.6c0.7,0.2,1.3,0.2,1.8,0.1c0.6-0.1,1.7-0.7,1.9-1.3c0.2-0.7,0.2-1.2,0.2-1.3C17.6,14.5,17.4,14.4,17.1,14.3z"
                                  stroke="none"
                                  fill="#fff"
                                />
                              </svg>
                            </a>
                          </div>
                        ) : null}
                        <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                      </div>
                    )}
                    {/* Scorecard Widget  here*/}
                    {value && value.tinyscore ? (
                      <div className="liveblog-scorecard">
                        <AnchorLink
                          data-type="scorecard"
                          target="_blank"
                          style={{ textDecoration: "none" }}
                          hrefData={{
                            override:
                              siteConfig.mweburl +
                              "/sports/cricket/live-score/" +
                              value.tinyscore.seoteamname +
                              "/" +
                              value.tinyscore.date +
                              "/scoreboard/matchid-" +
                              value.tinyscore.matchid +
                              ".cms",
                          }}
                        >
                          <div className="liveScore table">
                            <div className="teama table_col">
                              <span className="country">{value.tinyscore.teama}</span>
                              <span className="teaminfo">
                                <img height="60" width="60" src={value.tinyscore.teamalogo} />
                                <span className="score">{value.tinyscore.teamascore}</span>
                                <span className="over">
                                  {value.tinyscore.teamaovers != "" ? value.tinyscore.teamaovers : ""}
                                </span>
                              </span>
                            </div>

                            <div className="versus table_col">
                              {/* <i onClick={this.updateLiveblog} data-exclude="amp">
                                <SvgIcon name="refresh" />
                              </i> */}
                              <span>VS</span>
                            </div>
                            <div className="teamb table_col">
                              <span className="country">{value.tinyscore.teamb}</span>
                              <span className="teaminfo">
                                <img height="60" width="60" src={value.tinyscore.teamblogo} />
                                <span className="score">{value.tinyscore.teambscore}</span>
                                <span className="over">
                                  {value.tinyscore.teambovers != "" ? value.tinyscore.teambovers : ""}
                                </span>
                              </span>
                            </div>
                          </div>
                        </AnchorLink>
                      </div>
                    ) : null}
                  </div>
                  {highlights.length > 0 ? <HighlightCard data={highlights} type="lbhighlight" /> : null}
                  {isMobilePlatform() ? (
                    <AdCard key={`adctn_1`} adtype="ctn" pagetype="liveblog" mstype="ctnbccl" />
                  ) : null}
                </div>
                {meta.cricketlb && value && value.tinyscore ? (
                  <div className="liveblog_tabs">
                    <ul>
                      <li className="current">
                        <b>{siteConfig.locale.commentary}</b>
                      </li>
                      <li>
                        <AnchorLink
                          data-type="scorecard"
                          target="_blank"
                          style={{ textDecoration: "none" }}
                          hrefData={{
                            override:
                              siteConfig.mweburl +
                              "/sports/cricket/live-score/" +
                              value.tinyscore.seoteamname +
                              "/" +
                              value.tinyscore.date +
                              "/scoreboard/matchid-" +
                              value.tinyscore.matchid +
                              ".cms",
                          }}
                        >
                          {siteConfig.locale.scorecard}
                        </AnchorLink>
                      </li>
                    </ul>
                    <span class="last_updated">
                      Last Updated:
                      {value && value.lbcontent && value.lbcontent[0] && value.lbcontent[0].article_date}
                    </span>
                  </div>
                ) : null}
              </React.Fragment>
            }
            <div className={isMobilePlatform() ? "lb_summaryCard" : "lb_summaryCardD"}>
              <LiveblogCard lbcontent={post} pwa_meta={meta} query={query} />
              <span itemScope="" itemType="https://schema.org/NewsArticle">
                {SeoSchema().metaTag({
                  name: "mainEntityOfPage",
                  content: meta ? meta.canonical : "",
                })}
                {SeoSchema().metaTag({
                  name: "headline",
                  content:
                    meta.hindiheading && meta.hindiheading.length > 100
                      ? meta.hindiheading.substr(0, 100)
                      : meta.hindiheading,
                })}
                {SeoSchema().metaTag({
                  name: "alternativeHeadline",
                  content: meta.alternatetitle,
                })}
                {SeoSchema().metaTag({
                  name: "description",
                  content: meta ? meta.seodescription : "",
                })}
                {SeoSchema().metaTag({
                  name: "articleBody",
                  content: meta ? meta.seodescription : "",
                })}
                {SeoSchema().language()}

                <span itemType="https://schema.org/ImageObject" itemScope="itemScope" itemProp="image	">
                  <meta
                    itemProp="url"
                    content={
                      "https://navbharattimes.indiatimes.com/thumb/msid-" +
                      meta.imgmsid +
                      ",width-1200,height-900/pic.jpg"
                    }
                  />
                  <meta content="1200" itemProp="width" />
                  <meta content="900" itemProp="height" />
                </span>
                {SeoSchema().publisherObj()}
                {SeoSchema().metaTag({ name: "author", content: "websitename" })}
                {/* if post image not available */ SeoSchema().metaTag({
                  name: "thumbnailUrl",
                  content: meta.ogimg ? meta.ogimg : "",
                })}
                {SeoSchema({ pagetype: pagetype }).metaUrl(meta.canonical)}
              </span>
            </div>
            {_isCSR() &&
              ReactDOM.createPortal(
                <div className="pos_fixed">
                  {isFetching ? (
                    <span className="loader-anim" />
                  ) : (
                    <span className="btn_newupdates hide" onClick={this.updateLiveblog.bind(this)} />
                  )}
                </div>,
                document.getElementById("position-fixed-floater"),
              )}
          </div>
          {!isMobilePlatform() && (
            <div className="col4">
              <RhsWidget
                rhsCSRData={rhsCSRData}
                rhsCSRVideoData={rhsCSRVideoData}
                articleId={meta.msid}
                pathname={this.props.location.pathname}
                articleSecId={meta && meta.subsec1}
                sectionType={meta && meta.pwa_meta && meta.pwa_meta.sectiontype}
                // superHitWidgetData={superHitWidgetData}
                pagetype="liveblog"
                subsecname={meta && meta.pwa_meta && meta.pwa_meta.subsectitle1}
                pwaConfigAlaska={pwaConfigAlaska}
                // dispatch={dispatch}
                showDataDrawer="0"
                // bannerdata={bannerObj}
              />
            </div>
          )}
        </div>
        {isMobilePlatform() && breadcrumbData && !isOPPO && !isVivo && !isSamsung ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumbData} />
            <BottomWidgets
              item={meta}
              rhsCSRVideoData={rhsCSRVideoData}
              pwaConfigAlaska={pwaConfigAlaska}
              router={router}
            />
          </ErrorBoundary>
        ) : (
          <AdCard mstype="btf" adtype="dfp" pagetype="liveblog" />
        )}
        {/* {!isMobilePlatform() ?  : null} */}
      </div>
    ) : (
      <FakeNewsListCard />
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.liveblog,
    advertorialData: state && state.app && state.app.rhsData,
    header: state && state.header,
  };
}

Liveblog.fetchData = function({ dispatch, query, params }) {
  //set pagetype
  dispatch(setPageType("liveblog"));
  return dispatch(fetchLIVEBLOG_IfNeeded(params, query));
};

const getEndDtate = data => {
  let updateddate = data && data.includes("#") ? data.split("#") : data;
  updateddate = updateddate && updateddate[0];
  let date = new Date(updateddate);
  date.setDate(date.getDate() + 2);
  //console.log(date);
  return date;
};

const BottomWidgets = data => {
  let pwaConfigAlaska = data.pwaConfigAlaska;
  let router = data.router;
  let isAmpPage = false;
  // For nbt, amp widget works from MT domain
  let amazonIframeDataSource =
    process.env.SITE === "nbt"
      ? `${taswidgetMTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
          pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
        }&msid=${data && data.item && data.item.id}`
      : `${taswidgetNBTPath}?host=${process.env.SITE}&tag=amp&wdttype=${
          pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
        }&msid=${data && data.item && data.item.id}`;
  if (router && router.location.pathname && router.location.pathname.includes("amp_")) {
    isAmpPage = true;
  }
  let item = data.item ? data.item : null;
  let rhsCSRVideoData = data.rhsCSRVideoData || null;
  const isFeaturedArticle = data.isFeaturedArticle;
  return item != null ? (
    <ErrorBoundary>
      {/* Recommended news widget  for mobile placed after Topstory widget starts*/}
      <RecommendedNewsWidget articleId={item.id} sectionType={item && item.pwa_meta && item.pwa_meta.sectiontype} />
      {/* Recommended news widget  for mobile placed after Topstory widget starts*/}

      {/*  Mrec2  only for mobile comes second time after Popular Videos Widget 
        If pwa_meta contains AdServingRules="Turnoff" MREC won't show
      */}
      {isFeaturedArticle ? null : item &&
        item.pwa_meta &&
        item.pwa_meta.AdServingRules &&
        item.pwa_meta.AdServingRules === "Turnoff" ? null : (
        <div className="story-content ads-between-story">
          <AdCard mstype="mrec2" adtype="dfp" pagetype="articleshow" renderall={true} />
        </div>
      )}
      {/*  Mrec2 ends only for mobile comes after Popular Videos Widget  */}

      {/*  Popular videos for mobile placed after recommended starts*/}
      <div className="row wdt_popular_videos box-item" data-exclude="amp">
        {_isCSR() && rhsCSRVideoData ? (
          <ErrorBoundary>
            <div className="section">
              <div className="top_section">
                <h3>
                  <span>{rhsCSRVideoData.secname}</span>
                </h3>
              </div>
            </div>
            <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={[].concat(rhsCSRVideoData.items)} />
          </ErrorBoundary>
        ) : null}
      </div>
      {/*  Popular videos for mobile placed after recommended  ends*/}
      {pwaConfigAlaska && (pwaConfigAlaska._topdeal == "true" || pwaConfigAlaska._topoffer == "true") ? (
        <div className="wdt_amz pwa-deals">
          <iframe
            scrolling="no"
            frameBorder="no"
            height={isAmpPage ? "330" : "310"}
            src={
              isAmpPage
                ? amazonIframeDataSource
                : `${taswidgetNBTPath}?host=${process.env.SITE}&wdttype=${
                    pwaConfigAlaska._topdeal == "true" ? "generic" : "offer"
                  }&msid=${item && item.pwa_meta && item.pwa_meta.msid}`
            }
          />
        </div>
      ) : null}
    </ErrorBoundary>
  ) : null;
};
export default connect(mapStateToProps)(Liveblog);
