import { AnalyticsGA } from "../../components/lib/analytics";
import { _getStaticConfig } from "../../utils/util";

const siteConfig = _getStaticConfig();

const slikeApiKey = siteConfig.slike.apikey;

// React parser functions for live blog moved to util
// due to feed level changes and need in LiveBlogCard component
export const createVideoUrl = (item, msid, type, domNode) => {
  let data = {};
  if (domNode) {
    return {
      slikeid: domNode.attribs.src,
      hl: domNode.children[0].data,
      id: msid,
      // env: "stg",
      // version: "3.5.5",
      controls: {
        ui: "podcast",
      },
      seolocation: domNode.attribs.controls,
    };
  } else if (item.vdo) {
    for (let i = 0; i < item.vdo.length; i++) {
      if (item.vdo[i].id === msid) {
        data = item.vdo[i];
        break;
      }
    }
  }
  //handling for liveblog
  else if (item.msid) {
    data.override =
      item.override && item.override != "" && item.override != "null" ? item.override : `/-/videoshow/${item.msid}.cms`;
  }
  if (type == "data") return data;
  return generateUrl(data);
};

export const generateUrl = item => {
  if (item.override) {
    return item.override;
  } else {
    let templateName = siteConfig.listNodeLabels[item.tn] ? siteConfig.listNodeLabels[item.tn][0] : item.tn;
    let url =
      "/" +
      (item.seolocation ? item.seolocation + "/" : "") +
      templateName +
      "/" +
      (item.tn == "photo" ? item.imageid : item.id) +
      ".cms";
    return url;
  }
};

export const onVideoClickHandler = event => {
  let _this = this;
  // event.currentTarget.previousSibling.innerText
  let videourl = event.currentTarget.parentNode ? event.currentTarget.parentNode.getAttribute("data-videourl") : null;
  if (videourl) {
    try {
      //To maintain previous url
      window.history.pushState({}, "", videourl);
      AnalyticsGA.pageview(location.origin + videourl);
    } catch (ex) {
      console.log("Exception in history: ", ex);
    }
  }
};

export const createSlikeConfigLB = (item, slikeid) => {
  let adCode = "others";
  return {
    player: {
      section: adCode,
    },
    video: {
      id: slikeid || "",
      title: item.title || "",
      msid: item.msid || "",
      //FIXME:   Missing in new feed
      shareUrl: item.wu + "?" + siteConfig.slikeshareutm || "",
      //FIXME:   Missing in new feed
      seopath: item.seolocation ? item.seolocation : item.title,
      template: "liveblog",
      //FIXME:   Missing in new feed
      image: item.videoImgUrl || "",
    },
    sdk: {
      apikey: slikeApiKey,
    },
  };
};
