import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import routes from "../../router/master/routes";
import { Router } from "react-router";
import {
  _isCSR,
  getPageType,
  modifyAdsOnScrollView,
  isMobilePlatform,
  isIe11,
  getPageTypeUpdated,
  _getCookie,
} from "./../../utils/util";
import { publish } from "./../../utils/pubsub";

import { AnalyticsGA, Analytics_iBeat } from "./../../components/lib/analytics/index";
import Ads_module from "./../../components/lib/ads/index";
import CTN_module from "./../../components/lib/ads/ctnAds";
import GDPR_module from "./../../components/lib/gdpr/index";
import globalconfig from "./../../globalconfig";
import { _getStaticConfig } from "../../utils/util";
import { setGRXParameter } from "../../components/lib/analytics/src/ga";
import { PostGDPR } from "../html/services";
import { wapadsGenerator } from "../../components/lib/ads/lib/utils";
const siteConfig = _getStaticConfig();

const gaObj = siteConfig.ga;
const comscoreObj = siteConfig.comscore;
const adsObj = siteConfig.ads;
let isLandingPage = true;
globalconfig.isLandingPage = true;
if (_isCSR()) {
  //Initialize GDPR and wrap all ga and ads module on callback
  GDPR_module.init(
    {
      channelCode: siteConfig.channelCode,
      gatrackid: siteConfig.ga.gatrackid,
      mweburl: siteConfig.mweburl,
      wapsitename: siteConfig.wapsitename,
    },
    () => {
      let ad_pagetype =
        globalconfig && globalconfig.ad_pagetype != ""
          ? globalconfig.ad_pagetype
          : window.location.pathname == "/"
          ? "home"
          : "others";

      //Unique Google Analytics tracking id

      if (AnalyticsGA) {
        AnalyticsGA.initialize(gaObj.gatrackid, {
          comscore: comscoreObj,
          // isGDPR: window.geoinfo && !window.geoinfo.notEU,
          isGDPR: window.geoinfo && window.geoinfo.isGDPRRegion,
          gaprofile: gaObj.gaprofile,
          fbpixel: { id: siteConfig.fbpixelid },
          userId: true,
        });
      }

      // FIXME: commenting it as we using iBeat new js from HTML
      // Analytics_iBeat.initialize({
      //   ibeathost: siteConfig.iBeat.host,
      //   ibeatkey: siteConfig.iBeat.ibeatkey,
      //   wapsitename: siteConfig.iBeat.wapsitename,
      // });

      //Initialize ads ,  dfp
      Ads_module.initialize({
        wapads: adsObj.dfpads[ad_pagetype],
        // isGDPR: window.geoinfo && !window.geoinfo.notEU,
        isGDPR: window.geoinfo && window.geoinfo.isGDPRRegion,
        channelCode: siteConfig.channelCode,
      });
      //Initialize ads for ctn
      CTN_module.initialize({
        ctnads: adsObj.ctnads,
        dmpUrl: adsObj.dmpUrl,
        ctnAddress: adsObj.ctnAddress,
      });
    },
  );
}

function Root({ store, history }) {
  return (
    <Provider store={store}>
      <Router history={history} routes={routes} onUpdate={onRootUpdate} />
    </Provider>
  );
}

function onRootUpdate() {
  if (_isCSR()) {
    if (!isLandingPage) {
      const script = document.createElement("script");
      let dmpparam = "";
      if (_getCookie("_col_uuid") !== undefined) {
        dmpparam = `&fpc=${getCookie("_col_uuid")}`;
      }
      if (_getCookie("optout") !== undefined) {
        dmpparam += `&optout=${getCookie("optout")}`;
      }

      script.src = `https://ade.clmbtech.com/cde/aef/var=colaud?cid=${siteConfig.ccaudId}&_u=${window.location.href}${dmpparam}`;
      // script.src = `https://ade.clmbtech.com/cde/ae/${siteConfig.ccaudId}/var=_ccaud?${dmpparam}_u=${window.location.href}`;
      script.async = true;
      document.head.appendChild(script);
    }
    let { action, pathname } = this.state.location;
    if (window.meta && window.meta.longurl) {
      pathname = window.meta.longurl;
    }
    let cube = document.querySelector(".election_cube");
    try {
      const screenType = getPageTypeUpdated(pathname, null, true);

      // These parameters are common to all calls, thus set here
      // Set this only when popup video is not playing
      if (!window.is_popup_video) {
        setGRXParameter("screen_type", screenType);
      }
      setGRXParameter("url", location.pathname);
    } catch (e) {}

    // Update globalConfig router
    globalconfig.router.location = this.state.location;

    // trigger page view on page load in CSR
    let pagetype = getPageType(pathname);

    // trigger page view on page load
    if (
      !isLandingPage &&
      "articleshow,videoshow,photoshow,moviereview,movieshow".indexOf(pagetype) === -1 &&
      AnalyticsGA
    ) {
      AnalyticsGA.pageview(window.location.pathname);
    }

    domUpdates(pathname);
    scrollHandler(action, pathname);
    adsHandler(pathname);

    //Hack contains locations which the user have visited (Currently using to send  previous location in recommended feeds)
    window.locationArray = window.locationArray ? window.locationArray : [];
    window.locationArray.push(window.location.href);

    // update landing page flag
    isLandingPage = false;
    if (document.body.classList.contains("videoplaying") && pathname && pathname.indexOf("videoshow") < 0) {
      let self = this;
      self.masterPlayerContainer = !self.masterPlayerContainer
        ? document.querySelector(".master-player-container")
        : self.masterPlayerContainer;
      let event = new CustomEvent("dockMasterPlayer", { detail: "default" });
      document.dispatchEvent(event);
    }

    if (cube) {
      publish("ElectionCubeGA");
      publish("ShowCubeEvent");
    }
    //fire Event for ga of Election Cube Result
    // force on transform if it is switched off by the ad
    if (_isCSR() && typeof window.forceOnTransform === "function" && !window.isTransforming) {
      window.forceOnTransform();
    }
  }
}

function scrollHandler(action, pathname) {
  // check router location , scroll only if not articleshow
  if (pathname && pathname.indexOf("articleshow") < 0) {
    // (window.setTimeout(()=>{
    //    scrollTo(document.documentElement,0);
    // },100))
    if (action === "PUSH") {
      window.scrollTo(0, 0);
    }
  }
}

function adsHandler(pathname) {
  let pagetype = "others";
  let adsec = "others";

  // To load "tech" type adcodes (tech home page dfp)
  if (
    ("nbt".indexOf(process.env.SITE) > -1 && pathname.endsWith("/tech") == true) ||
    ("vk".indexOf(process.env.SITE) > -1 && pathname.endsWith("/tech") == true)
  ) {
    pagetype = "tech";
    adsec = "tech";
    // reset ctnads for tech
    window.ctnads = adsObj.ctnads[pagetype];
  } else {
    pagetype = getPageType(pathname, true);

    if (window.location.pathname == "/") {
      pagetype = "home";
      adsec = "homepage";
      window._SCN = "home";
    }

    if (pagetype == "elections") {
      // pagetype = "others";
      adsec = "microsite";
      window._SCN = "elections";
    } else if (pagetype == "newsbrief") {
      // pagetype = "others";
      adsec = "news";
      window._SCN = "news_briefs";
    } else if (pagetype == "tech") {
      adsec = "tech";
      window._SCN = "tech";
    } else if (pagetype == "liveblog") {
      adsec = "news";
      window._SCN = "liveblog";
    }

    // reset ctnads
    window.ctnads = adsObj.ctnads;
  }
  // trigger refresh of ads
  // window.wapads = adsObj.dfpads[pagetype];

  //check if landing page or not , return if landing page
  if (isLandingPage) {
    return;
  } else {
    if (
      pagetype == "home" ||
      pagetype == "tech" ||
      pagetype == "elections" ||
      pagetype == "newsbrief" ||
      pagetype == "liveblog"
    ) {
      window.wapads = wapadsGenerator({
        secname: adsec,
        defaultWapAds: siteConfig.ads.dfpads.others,
        pagetype: pagetype,
      });
      Ads_module.resetwaitForAdsEvent("false");
    } else {
      Ads_module.resetwaitForAdsEvent(window.waitForAdsEvent);
    }

    // and destroying all ads Slots on page change
    Ads_module.destroySlots();
    Ads_module.recreateAds(
      isMobilePlatform() ? ["topatf", "atf", "fbn"] : ["topatf", "atf", "skinrhs", "innove"],
      // {
      //   wapads: pagetype != "" ? adsObj.dfpads[pagetype] : adsObj.dfpads.others,
      // },
    );

    //refresh only canvasAd for Mobile
    isMobilePlatform() ? Ads_module.refreshAds(["canvasAd"]) : "";

    // after recreation , reset fbn refresh ads
    // process.env.SITE == "tlg" ? modifyAdsOnScrollView(true) : "";
  }
}

function domUpdates(pathname) {
  if (document.getElementById("mySidenav"))
    document.getElementById("mySidenav").style.transform = "translateX(-1000px)";
  if (document.querySelector(".greyLayer")) document.querySelector(".greyLayer").style.display = "none";

  document.body.style.overflow = "scroll";
  document.querySelector("body").setAttribute("pagetype", getPageType(pathname));
  if (isIe11()) {
    document.querySelector("body").setAttribute("class", "ie-eleven");
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Root;
