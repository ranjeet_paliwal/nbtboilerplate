export const designConfigs = {
  topicslisting: [
    {
      type: "horizontal",
      className: "col12 rest-topics",
      imgsize: "smallthumb",
    },
  ],
  topicsOnlyInfo: [
    {
      startIndex: 0,
      type: "only-info",
      definitionTerm: true,
      className: "col12 rest-topics",
    },
  ],
};
