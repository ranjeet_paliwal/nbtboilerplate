import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

import {
  debounce,
  elementInView,
  _isCSR,
  setHyp1Data,
  handleReadMore,
  _checkUserStatus,
  _getCookie,
  _deferredDeeplink,
  isMobilePlatform,
  updateConfig,
  filterAlaskaData,
  shouldTPRender,
  isProdEnv,
  isTechPage,
  isTechSite,
  loadInstagramJS,
  addObserverToWidgets,
  createAndCacheObserver,
} from "../../../utils/util";
import { fireActivity } from "../../../components/common/TimesPoints/timespoints.util";
import {
  fetchArticleShowIfNeeded,
  fetchNextArticleShowData,
  resetOnUnmount,
} from "../../../actions/articleshow/articleshow";
// import { fetchAmazonDataIfNeeded, fetchAmazonNextData } from "../../../modules/amazonwidget/actions/amazonwidget";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import FakeStoryCard from "../../../components/common/FakeCards/FakeStoryCard";
import SocialShare from "../../../components/common/SocialShare";

import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module, { setSectionDetail } from "../../../components/lib/ads/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
import { setParentId, setPageType } from "../../../actions/config/config";

import styles from "../../../components/common/css/ArticleShow.scss";
import "../../../components/common/css/desktop/GadgetNow.scss";

import CTN_module from "../../../components/lib/ads/ctnAds";
import { _getStaticConfig } from "../../../utils/util";
import StoryCard from "../../../components/common/StoryCard";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import { fetchRHSDataPromise } from "../../../actions/app/app";
import RecommendedNewsWidget from "../../../components/common/RecommendedNewsWidget";
import {
  trackersOnScroll,
  getRhsData,
  mod_symmetricalAds,
  commonCompWillRecieveProps,
  fetchAdvertorialNews,
  videoDockStatus,
  addElementsForScrollDepth,
} from "../../utils/articleshow_util";
// import TopStoryWidget from "../../../components/common/TopStoryWidget";
import AdCard from "../../../components/common/AdCard";
import globalconfig from "./../../../globalconfig";
import { isFeatureURL } from "../../../components/lib/ads/lib/utils";
import { topListDataPromise, fetchTopListDataIfNeeded, makeDataChangedFalse } from "../../../actions/home/home";
import { clearFeedInterval, setRefreshFeedInterval } from "../../utils/home_util";
import SvgIcon from "../../../components/common/SvgIcon";
import { fetchMiniScheduleDataIfNeeded } from "../../../modules/minischedule/actions/minischedule";
import AnchorLink from "../../../components/common/AnchorLink";
import AmazonBtn from "../../../components/common/AmazonBtn/AmazonBtn";
import { fireGRXEvent } from "../../../components/lib/analytics/src/ga";
import BriefWidget from "../../../components/common/BriefWidget";
const siteConfig = _getStaticConfig();
const adsObj = siteConfig.ads;

class Articleshow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBinded: false,
      index: 0,
      advertorialNews: [],
      scrollStart: false,
    };
    this.config = {
      perpetual: true,
      maxArticles: 9,
      rhsCSRVideoData: null,
      superHitWidgetData: null,
      adcounter: 0,
      load: true,
      resume: true,
      gatracked: true,
      cur_art_height: 0,
      art_scrolled_per: 0,
      last_scrolled_per: 0,
      lastscrolled: 0,
      init_art: null,
      cur_art: null,
      timeout_var: null,
      read_marker: null,
      short_url: null,
      articleIds: [],
      keepnextmsid: null,
      advInvoked: false,
      firstScroll: true,
    };

    this.scrollWithThrottle = debounce(this.handleScroll.bind(this));
  }
  componentWillMount() {
    let { params } = this.props;
    this.config.curmsid = params.msid;
    this.config.articleIds.push(this.config.curmsid);
  }

  timesPointsActivity(activity, msid) {
    if (typeof fireActivity == "function" && msid) {
      fireActivity(activity, msid);
    }
  }

  componentDidMount() {
    let _this = this;
    window.scrollTo(0, 0);
    if (typeof loadInstagramJS == "function") {
      loadInstagramJS();
    }

    const { dispatch, params, items, router } = _this.props;
    const { query } = _this.props.location;

    //set current msid
    _this.config.curmsid = params.msid;
    _this.config.articleIds.push(_this.config.curmsid);
    //Set Pagetype commented, called in fetchdata below.
    //set pagetype
    //dispatch(setPageType('articleshow'));

    //set section & subsection for first time
    if (items[0] && typeof items[0].pwa_meta == "object") Ads_module.setSectionDetail(items[0].pwa_meta);

    // dispatch(fetchMiniScheduleDataIfNeeded(params, query));
    const scrollDepthObsConfig = {
      root: null,
      rootMargin: "0px",
      threshold: 0.2, // when 20% in view
    };

    const scrollDepthObscallback = (entries, self) => {
      entries.forEach(entry => {
        if (entry && entry.isIntersecting) {
          console.log("observed");
          const eventData = {};
          const rowId = entry.target.getAttribute("data-row-id") || "NA";

          if (rowId) {
            eventData.widget_id = rowId;
          }
          fireGRXEvent("track", "scroll_depth", eventData);
          // Unobserve this
          self.unobserve(entry.target);
        }
      });
    };

    const { app } = _this.props;
    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].articleshow &&
        app.scrolldepth[process.env.SITE].articleshow[process.env.PLATFORM] === "true") ||
      false;

    createAndCacheObserver(_this, scrollDepthObsConfig, scrollDepthObscallback, {
      shouldCreate: shouldCreateScrollDepth,
    });
    fetchRHSDataPromise(dispatch);
    // TimesPoints Activity
    const msid = items && items[0] && items[0].id;
    _this.timesPointsActivity("READ_ARTICLE", msid);

    //fetch data for ssr
    Articleshow.fetchData({ dispatch, params, query, router }).then(data => {
      let parentId =
        _this.props.items instanceof Array && _this.props.items[0] && typeof _this.props.items[0].pwa_meta == "object"
          ? _this.props.items[0].pwa_meta.parentid
          : null;
      // dispatch(setParentId(parentId));
      _this.animate_btn_App();

      const { items, router } = _this.props;

      // TimesPoints Activity
      const msid = data && data.payload && data.payload.id;
      addObserverToWidgets(
        _this,
        {
          [`[data-scroll-id="recommended-news-${msid || (items && items[0] && items[0].id)}"]`]: true,
        },
        {
          shouldCreate: shouldCreateScrollDepth,
        },
      );
      _this.timesPointsActivity("READ_ARTICLE", msid);

      //Symetrical ads on scroll
      if (
        typeof items[0] == "object" &&
        (typeof items[0].ag == "undefined" ||
          (typeof items[0].ag == "string" &&
            !(
              items[0].ag.toLowerCase() == "brandwire" ||
              items[0].ag.toLowerCase() == "mediawire" ||
              items[0].ag.toLowerCase() == "colombia"
            ))) &&
        !(
          items[0] &&
          items[0].pwa_meta &&
          items[0].pwa_meta.AdServingRules &&
          items[0].pwa_meta.AdServingRules === "Turnoff"
        )
      ) {
        const flag = videoDockStatus(_this.props.minitv, items[0]);

        mod_symmetricalAds(
          "",
          router.location.pathname.indexOf("articleshow_esi_test") > -1 ? "articleshow_esi_test" : null,
          isFeatureURL(items[0].wu),
          flag,
          items[0] && items[0].wu ? items[0].wu : "undefined",
        );
      }

      const { pwa_meta, audetails } = _this.props.items[0] ? _this.props.items[0] : {};
      if (pwa_meta && typeof pwa_meta == "object") {
        getRhsData(pwa_meta, _this);
        // fetch(``);
        //set hyp1 variable
        pwa_meta ? setHyp1Data(pwa_meta) : "";

        //set section and subsection in window
        // Ads_module.setSectionDetail(pwa_meta);

        //fire ibeat for article
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";

        //If editor present in pwa_meta , set in window Object
        window._editorname = "";
        if (typeof data == "object" && data.type) {
          if (audetails) {
            window._editorname = audetails.cd;
          } else {
            window._editorname =
              typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
          }
          //Hit ga
          AnalyticsGA.pageview(window.location.pathname, {
            setEditorName: true,
          });
        }
      }
      const story_number = _this.props.location.query.story || 0;
      let d = null;
      if (story_number > 0) {
        d = document.getElementById("story_" + story_number);
      }
      if (d) {
        d.scrollIntoView({ block: "center" });
      }
    });

    //updateConfig(items && items[0] && items[0].pwa_meta, dispatch, setParentId);

    if (_isCSR()) {
      //check perpetual needed or not
      if (_this.config.perpetual) {
        _this.config.timeout_var = window.setTimeout(() => {
          _this.afterRender(_this);
          _this.config.read_marker = document.querySelector("body span.read_marker");
          // scrollTo(document.documentElement, 0, 200);
        }, 1000);
      }
      if (_this.props.items[0]) {
        _this.config.short_url = _this.props.items[0].m ? _this.props.items[0].m : null;
      } //set short URL

      // For test page only requested by sales team
      router.location.pathname.indexOf("articleshow_esi_test") > -1 &&
      document.querySelector(".con_intad") &&
      document.querySelector(".con_intad").classList.contains("intctn") ? (
        <ErrorBoundary>
          {document.querySelector(".con_intad").classList.remove("intctn")}
          {document.querySelector(".con_intad").classList.add("intctn_test")}
        </ErrorBoundary>
      ) : null;
    }
    setRefreshFeedInterval(120, this.props.home && this.props.home.refreshFeedInterval, dispatch, "articlshow", router);
  }

  componentDidUpdate(prevProps, prevState) {
    const { router, dispatch } = this.props;
    // if (this.props.home.dataChanged) {
    //   this.props.dispatch(makeDataChangedFalse());
    //   fetchRHSDataPromise(dispatch);
    // }
    if (prevProps.items != this.props.items && Array.isArray(this.props.items)) {
      addElementsForScrollDepth(this);
    }
  }

  componentWillUnmount() {
    let _this = this;
    const { dispatch } = _this.props;
    clearFeedInterval(_this.props.home.refreshFeedInterval, dispatch);
    window.removeEventListener("scroll", _this.scrollWithThrottle);
    window.removeEventListener("scroll", _this.animate_btn_App);
    window.clearInterval(_this.config.timeout_var);
    _this.state._scrollEventBinded = false;
    // reset pagetype and parentid
    dispatch(setParentId(""));
    dispatch(setPageType(""));
    //reset section window data
    Ads_module.setSectionDetail();
    //reset hyp1 to ''
    setHyp1Data();
    // const footerCont = document.querySelector("#footerContainer");
    // if (footerCont) {
    //   footerCont.classList.remove("hideFooter");
    // }
    //reset content
    resetOnUnmount(dispatch);
  }

  componentWillReceiveProps(nextProps) {
    commonCompWillRecieveProps(this, nextProps, Articleshow);
  }

  afterRender(_this) {
    if (_this.state._scrollEventBinded == false) {
      window.addEventListener("scroll", _this.scrollWithThrottle);
      _this.state._scrollEventBinded = true;
      //Readmore button handling
      handleReadMore();
    }
  }

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;

    const { items } = this.props;
    let currentHeadline = "";
    let currentShortUrl = "";
    let currentMSID = "";
    if (items) {
      // loop on items length
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const article = document.querySelector(`[data-artBox="article-${item.id}"]`);
        // checking box id view on window height half
        if (item && article && elementInView(article, true)) {
          currentHeadline = item.hl;
          currentShortUrl = item.m;
          currentMSID = item.id;
        }
      }
    }

    let sharedata = {
      title: currentHeadline || document.title,
      url: location.href,
      short_url: currentShortUrl || this.config.short_url,
      msid: currentMSID,
    };

    if (!isShareAll) return sharedata;

    //for watsapp sharing feature along with GA
    AnalyticsGA.event({
      category: "social",
      action: "Whatsapp_Wap_stickyAS",
      label: sharedata.url,
    });

    if (this.config.short_url != "" && this.config.short_url != null) {
      var info = sharedata.short_url; // check short micron url availability
      info += "/l" + siteConfig.shortutm;
      info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
    } else {
      var info = sharedata.url;
      info += "?utm_source=Whatsapp_Wap_stickyAS" + siteConfig.fullutm;
      info += siteConfig.locale.social_download.extrasharetxt + " " + siteConfig.applinks.android.social;
    }
    var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + encodeURIComponent(info);
    try {
      if (shouldTPRender() && currentMSID) {
        fireActivity("WHATSAPP_SHARE", `ws_share_${currentMSID}`);
      }
    } catch (e) {}

    window.location.href = whatsappurl;

    return false;
  }

  handleScroll() {
    let _this = this;
    const { items, header, dispatch, router } = _this.props;
    const { index } = _this.state;
    if (_this.state._scrollEventBinded == false) return;

    if (!_this.state.scrollStart) {
      _this.setState({ scrollStart: true });
    }
    //Symetrical ads on scroll

    if (this.config.firstScroll) {
      this.config.firstScroll = false;
      fetchRHSDataPromise(dispatch);
    }

    if (window.pageYOffset >= 300 && !this.config.advInvoked) {
      const alaskaData = header && header.alaskaData;
      fetchAdvertorialNews(this, alaskaData);
    }

    trackersOnScroll(_this);

    let windowScrolled = (document.scrollingElement || document.documentElement).scrollTop;

    //collect article as per index if perpetual not started
    if (_this.config.cur_art == null)
      _this.config.cur_art = _this.config.init_art = document.querySelectorAll("[data-artBox]")[_this.state.index];

    if (_this.config.cur_art_height === 0) _this.config.cur_art_height = _this.config.cur_art.scrollHeight;

    if (windowScrolled > _this.config.lastscrolled) {
      let art_scrolled = windowScrolled - _this.config.cur_art.offsetTop + 99;
      _this.config.art_scrolled_per = parseInt((art_scrolled / _this.config.cur_art_height) * 100);

      //calculate readmarkerbar height
      if (_this.config.read_marker) {
        _this.config.read_marker.style.height = _this.config.art_scrolled_per + "%";
      } else {
        _this.config.read_marker = document.querySelector("body span.read_marker");
      }

      if (60 <= _this.config.art_scrolled_per) {
        // reset perpetual variable
        _this.config.resume = true;
        // console.log(_this.config.resume);
        _this.config.cur_art = _this.config.init_art;
        _this.config.cur_art_height = 0;
      } else {
        _this.config.resume = false;
      }

      /*Get distance of footer from top of viewport */
      const footerCont = document.querySelector("#footerContainer");
      let footerHeightFromTop = footerCont && footerCont.getBoundingClientRect().top;
      // if (footerCont) {
      //   footerCont.classList.add("hideFooter");
      // }
      if (footerHeightFromTop < 6000 && _this.config.maxArticles >= 1) {
        // if (
        //   _this.config.maxArticles >= 1 &&
        //   50 <= _this.config.art_scrolled_per &&
        //   100 >= _this.config.art_scrolled_per
        // ) {
        const { dispatch, params, query, isFetching, router, advertorialData, advertorialPerpectual } = _this.props;

        if (!isFetching) {
          let nextmsid = null;
          //review section fetch Next
          if (_this.props.items[0].ismarkreview == "1") {
            let reviewlist = _this.props.items[0].reviewlist;
            nextmsid = reviewlist[_this.state.index].id;
          } else {
            //noraml articleshow get first next Item
            if (
              items &&
              items[index] &&
              items[index].pwa_meta &&
              items[index].pwa_meta.nextItem &&
              items[index].pwa_meta.nextItem
            ) {
              nextmsid = items[index].pwa_meta.nextItem.msid;
              _this.config.nextmsid = nextmsid;
            }
            // console.log("nextmsid", nextmsid);
            // nextmsid = _this.config.nextmsid =
            //   _this.props.items[_this.state.index].pwa_meta && _this.props.items[_this.state.index].pwa_meta.nextItem
            //     ? _this.props.items[_this.state.index].pwa_meta.nextItem.msid
            //     : null;
          }

          //keep same article show trail after Advertorial Perpectual
          if (_this.config.keepnextmsid) {
            nextmsid = _this.config.keepnextmsid;
            _this.config.keepnextmsid = null;
          }

          if (advertorialPerpectual) {
            let checkPlatform = process.env.PLATFORM ? process.env.PLATFORM : "mobile";
            let filteradvertorial =
              advertorialData && Array.isArray(advertorialData) && advertorialData.length > 0
                ? advertorialData.filter(fdata => {
                    return (
                      (checkPlatform === "mobile" ? fdata.ctype == "adv_wap" : fdata.ctype == "adv") &&
                      fdata.wu &&
                      fdata.wu.includes(process.env.WEBSITE_URL) &&
                      fdata.wu.includes("/articleshow/")
                    );
                  })
                : [];

            filteradvertorial.splice(1);

            let advertorialPerpectualMsid =
              filteradvertorial && filteradvertorial.length > 0 && filteradvertorial[0] && filteradvertorial[0].id;

            if (
              filteradvertorial &&
              filteradvertorial.length > 0 &&
              advertorialPerpectual &&
              params &&
              params.msid != advertorialPerpectualMsid
            ) {
              _this.config.keepnextmsid = nextmsid;
              nextmsid = advertorialPerpectualMsid;
              _this.config.nextmsid = advertorialPerpectualMsid;
            }
          }
          /*check if next-article exist , then resume loading next story */
          if (_this.config.resume && nextmsid && nextmsid !== "") {
            _this.config.articleIds.push(nextmsid);
            _this.config.resume = false;
            fetchNextArticleShowData(nextmsid, dispatch, params, query, router, advertorialData)
              .then(data => {
                data = data.payload; //set payload as data
                let { pwa_meta, audetails } = data;
                _this.setState({
                  index: _this.state.index + 1,
                });
                _this.config.maxArticles--;
                _this.config.init_art = document.querySelectorAll(`[data-artBox]`)[_this.state.index];

                //reset hyp1 variable
                setHyp1Data(data.pwa_meta);
                setSectionDetail(data.pwa_meta);

                //set amazon key to fetch amazon content
                params.pagename = "articleshow";
                params.amazonkey = data && data.pwa_meta && data.pwa_meta.amazonkey ? data.pwa_meta.amazonkey : null;
                //If editor present in pwa_meta , set in window Object
                if (audetails) {
                  window._editorname = audetails.cd;
                } else {
                  window._editorname =
                    typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
                }
                //call for symmetrical ad positioning
                if (
                  typeof data == "object" &&
                  (typeof data.ag == "undefined" ||
                    (typeof data.ag == "string" &&
                      !(
                        data.ag.toLowerCase() == "brandwire" ||
                        data.ag.toLowerCase() == "mediawire" ||
                        data.ag.toLowerCase() == "colombia"
                      ))) &&
                  !(data && data.pwa_meta && data.pwa_meta.AdServingRules && data.pwa_meta.AdServingRules === "Turnoff")
                ) {
                  const flag = videoDockStatus(_this.props.minitv, data);
                  mod_symmetricalAds(_this.config.init_art, "", "", flag, data.wu);
                }

                //Readmore button handling
                handleReadMore();

                // Update globalConfig router
                globalconfig.router.location.pathname = data.wu;

                // fetchAmazonNextData(dispatch, params, router)
                //   .then(data => {
                //     data = data.payload; //set payload as data
                //   })
                //   .catch(err => {
                //     console.log("Inside catch", err);
                //   });
              })
              .catch(err => {
                console.log("Inside catch", err);
              });
          }
          // if (footerCont && (!nextmsid || nextmsid === "")) {
          //   footerCont.classList.remove("hideFooter");
          // }
        }
      }

      _this.config.last_scrolled_per = _this.config.art_scrolled_per;
      _this.config.lastscrolled = windowScrolled;
    }
    const currentUrl = window.location.pathname;
    if (currentUrl.indexOf("-fea-ture/") > -1) {
      // ads block removed in case of feature URL.
      document.querySelectorAll(".prerender").forEach(item => {
        item.style.display = "none";
      });
      document
        .querySelectorAll(
          '.featured-article .parallaxDiv, .featured-article .ad1 , .featured-article [data-plugin="ctn"]',
        )
        .forEach(item => {
          item.style.display = "none";
        });
    } else {
      document.querySelectorAll(".prerender").forEach(item => {
        item.style.display = "block";
      });
    }
  }

  animate_btn_App() {
    // Check is button visible or not
    let ele = document.querySelector('[data-attr="btn_openinapp"]');
    let shareContainer = document.querySelector(".share_container");
    if (ele && shareContainer) {
      shareContainer.classList.add("hide2right");
      ele.style.left = "-175px";
      shareContainer.style.display = "none";
      ele.style.display = "none";

      window.addEventListener("scroll_down", function() {
        // ele.style.left = "0";
        shareContainer.classList.remove("hide2right");
      });

      window.addEventListener("scroll_up", function() {
        ele.style.display = "block";
        ele.style.left = "0";

        shareContainer.style.display = "block";
        shareContainer.classList.add("hide2right");
      });
    }
  }

  render() {
    let { items, itemNIC, isFetching, params, electionresult, router, error, adconfig, header } = this.props;
    const { advertorialNews, rhsCSRVideoData, superHitWidgetData, rhsCSRData } = this.state;
    if (error) {
      return null;
    }

    let temp_items = [];
    let msid = "";
    //check for NIC and push it to items array
    let { query, pathname } = this.props.location;
    if (query && query.type == "nic" && itemNIC) {
      temp_items.push(itemNIC);
    } else {
      temp_items = items;
    }

    let isAmpPage = false;
    // For nbt, amp widget works from MT domain

    if (typeof pathname === "string" && pathname.includes("amp_")) {
      isAmpPage = true;
    }
    //get next article in case of review page
    //review section fetch Next
    let nextreview = null;
    if (items && items[0] && items[0].pwa_meta && items[0].pwa_meta && items[0].pwa_meta.review == "1") {
      let reviewlist = items[0].nextarticles;
      for (var i = 0; i < reviewlist.length; i++) {
        if (this.config.articleIds && !this.config.articleIds.includes(reviewlist[i].id)) {
          nextreview = reviewlist[i];
          break;
        }
      }
    }

    //set amazon key to fetch amazon content
    params.pagename = "articleshow";
    params.amazonkey =
      temp_items && temp_items[0] && temp_items[0].pwa_meta && temp_items[0].pwa_meta.amazonkey
        ? temp_items[0].pwa_meta.amazonkey
        : null;
    // if(temp_items != undefined && temp_items.length > 0 && temp_items[0] && temp_items[0].id === params.msid){
    if (temp_items != undefined && temp_items.length > 0 && temp_items[0] && temp_items[0].id != "") {
      let schema = temp_items[0].recipe
        ? SeoSchema({ pagetype: "articleshow" }).attr().Recipe
        : temp_items[0].pwa_meta.pagetype == "moviereview"
        ? SeoSchema({ pagetype: "articleshow" }).attr().Movie
        : temp_items[0].pwa_meta && temp_items[0].pwa_meta.sectionid == siteConfig.pages.giftsection
        ? SeoSchema({ pagetype: "articleshow" }).attr().Article
        : // : temp_items[0] && temp_items[0].substory ?
          //   SeoSchema({ pagetype: "articleshow " }).attr().ImageGallery
          SeoSchema({ pagetype: "articleshow" }).attr().NewsArticle;

      if (
        router &&
        router.location.pathname.indexOf("articleshow_esi_test") > -1 &&
        temp_items[0].pwa_meta &&
        temp_items[0].pwa_meta.noindex == undefined
      ) {
        temp_items[0].pwa_meta.noindex = 1;
      }

      // suggesting vasu to add one label like amazondeal:true/false
      // Widgets like amazon and top deal
      const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");
      // need to change otherconfig to floatingbtn
      let configFloatBtn = filterAlaskaData(header.alaskaData, ["pwaconfig", "floatingamazon"], "label");

      return (
        <ErrorBoundary>
          <div className={temp_items[0].recipe ? "recipeArticle" : null}>
            {/* For SEO Meta */
            PageMeta(temp_items[0].pwa_meta)}
            {//Job or any other schema calling
            temp_items[0] && temp_items[0].pwa_meta && temp_items[0].pwa_meta.schema != ""
              ? SeoSchema().job(temp_items[0].pwa_meta.schema)
              : null}
            {/* {//faq schema calling
            temp_items[0].pwa_meta.schema && temp_items[0].pwa_meta.schema != ""
              ? SeoSchema().schema(temp_items[0].pwa_meta.schema)
              : null} */}
            <span className={styles.read_marker + " read_marker"} />
            {temp_items.map((item, index) => {
              if (typeof item != "undefined") {
                (msid = item.id), (item.electionresult = electionresult);
                const isFeaturedArticle = isFeatureURL(item.wu);
                // custom script for one bennet articles
                const customEventScript = Boolean(item && item.pwa_meta && item.pwa_meta.customevent === "1");
                return (
                  <div data-artbox={"article-" + item.id} gatracked={index == 0 ? "true" : ""}>
                    <ErrorBoundary key={item.id}>
                      <div {...schema}>
                        {/* this is custom script for bennet univercity */}
                        {customEventScript ? (
                          <div
                            data-exclude="amp"
                            dangerouslySetInnerHTML={{
                              __html: `<script>
                                  !function(f,b,e,v,n,t,s)
                                  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                  n.queue=[];t=b.createElement(e);t.async=!0;
                                  t.src=v;s=b.getElementsByTagName(e)[0];
                                  s.parentNode.insertBefore(t,s)}(window, document,'script',
                                  'https://connect.facebook.net/en_US/fbevents.js');
                                  fbq('init', '2009952072561098');
                                  fbq('track', 'PageView');
                                  </script>
                                  <noscript><img height="1" width="1" style="display:none"
                                  src="https://www.facebook.com/tr?id=2009952072561098&ev=PageView&noscript=1"
                                  /></noscript>
                                  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-922733241"></script>
                                  <script>
                                  window.dataLayer = window.dataLayer || [];
                                  function gtag(){dataLayer.push(arguments);}
                                  gtag('js', new Date());
                                  gtag('config', 'AW-922733241');
                                  </script> `,
                            }}
                          />
                        ) : null}
                        {/* End  custom script for bennet univercity */}
                        <StoryCard
                          nextreview={nextreview}
                          item={item}
                          index={index}
                          superHitWidgetData={superHitWidgetData}
                          isFetching={isFetching}
                          pagetype="articleshow"
                          query={query}
                          dispatch={this.props.dispatch}
                          router={router}
                          isFeaturedArticle={isFeaturedArticle}
                          adconfig={adconfig}
                          advertorialNews={advertorialNews}
                          pwaConfigAlaska={pwaConfigAlaska}
                          rhsCSRVideoData={rhsCSRVideoData}
                        />
                      </div>

                      <BottomWidgets
                        item={item}
                        index={index}
                        // rhsCSRVideoData={rhsCSRVideoData}
                        isFeaturedArticle={isFeaturedArticle}
                        router={router}
                        rhsCSRData={rhsCSRData}
                        scrollStart={this.state.scrollStart}
                      />
                    </ErrorBoundary>
                  </div>
                );
              } else {
                return null;
              }
            })}
            {_isCSR() ? (
              ReactDOM.createPortal(
                <div className="share_container" style={!isAmpPage ? { display: "none" } : null}>
                  <div className="share_whatsapp">
                    <a
                      data-attr="share_whatsapp"
                      rel="nofollow"
                      onClick={this.getShareDetail.bind(this, true)}
                      href="javascript:void(0);"
                    >
                      <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                    </a>
                  </div>

                  <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
                </div>,
                document.getElementById("share-container-wrapper"),
              )
            ) : (
              <div className="share_container" style={!isAmpPage ? { display: "none" } : null}>
                <div className="share_whatsapp">
                  <a
                    data-attr="share_whatsapp"
                    rel="nofollow"
                    onClick={this.getShareDetail.bind(this, true)}
                    href="javascript:void(0);"
                  >
                    <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                  </a>
                </div>

                <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
              </div>
            )}

            {/*
            <div id="commentContainer">
              <div id="comment-section" className="comment-sidebox">
                <span id="previous">BACK</span>
                <span id="title">{temp_items[0].hl}</span>
                <div id="commentHolder"><span className="loader-anim"></span></div>
              </div>
            </div>
            */}
            {/* Open in App Button Floating */
            msid != "" && configFloatBtn && configFloatBtn._type == "appinstall" ? (
              _isCSR() ? (
                ReactDOM.createPortal(
                  <a
                    className="btn_openinapp"
                    data-attr="btn_openinapp"
                    style={!isAmpPage ? { display: "none" } : null}
                    href={_deferredDeeplink(
                      temp_items[0].pwa_meta.htmlview ? "htmlview" : temp_items[0].pwa_meta.app_tn,
                      siteConfig.appdeeplink,
                      msid,
                      temp_items[0].pwa_meta.htmlview ? temp_items[0].pwa_meta.htmlview : null,
                      null,
                      temp_items[0].pwa_meta.htmlview ? "openinapp_as" : temp_items[0].pwa_meta.app_tn,
                    )}
                  >
                    {siteConfig.locale.openinapp}
                  </a>,
                  document.getElementById("share-container-wrapper"),
                )
              ) : (
                <a
                  className="btn_openinapp"
                  data-attr="btn_openinapp"
                  style={!isAmpPage ? { display: "none" } : null}
                  href={_deferredDeeplink(
                    temp_items[0].pwa_meta.htmlview ? "htmlview" : temp_items[0].pwa_meta.app_tn,
                    siteConfig.appdeeplink,
                    msid,
                    temp_items[0].pwa_meta.htmlview ? temp_items[0].pwa_meta.htmlview : null,
                    null,
                    temp_items[0].pwa_meta.htmlview ? "openinapp_as" : temp_items[0].pwa_meta.app_tn,
                  )}
                >
                  {siteConfig.locale.openinapp}
                </a>
              )
            ) : null}

            {configFloatBtn && configFloatBtn._type == "amazon" && (
              <AmazonBtn data={configFloatBtn} isAmpPage={isAmpPage} />
            )}

            {isFetching ? <FakeStoryCard /> : null}
          </div>
        </ErrorBoundary>
      );
    } else {
      return <FakeStoryCard />;
    }
  }
}

function mapStateToProps(state) {
  const isPopUpVisible = state.videoplayer.showMinitv;
  const miniTvData = state && state.app && state.app.minitv;

  return {
    ...state.articleshow,
    adconfig: (state && state.app && state.app.adconfig) || {},
    home: state && state.home,
    app: state.app,
    advertorialData: state && state.app && state.app.rhsData,
    header: state.header,
    minitv: {
      isPopUpVisible,
      miniTvData,
    },
    // electionresult:state.electionresult
  };
}

Articleshow.fetchData = ({ dispatch, params, query, router, history }) => {
  //set pagetype

  // dispatch(setPageType("articleshow"));
  // if (router.location.pathname.indexOf("/tech") > -1) {
  //   dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router));
  // } else {
  //   dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router, "amazondeal"));
  // }
  return dispatch(fetchArticleShowIfNeeded(params, query, router)).then(data => {
    // console.log("asdata", data);
    //fetchRHSDataPromise(dispatch);
    fetchTopListDataIfNeeded(dispatch, "", router);

    if (data && data.payload && data.payload.pwa_meta) {
      const pwaMeta = { ...data.payload.pwa_meta };
      dispatch(setPageType("articlelist", pwaMeta.site));
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }

    return data;
  });
};

export default connect(mapStateToProps)(Articleshow);

const BottomWidgets = data => {
  let item = data.item ? data.item : null;
  // let rhsCSRVideoData = data.rhsCSRVideoData || null;
  let rhsCSRData = data.rhsCSRData;
  const index = data.index;
  const isFeaturedArticle = data.isFeaturedArticle;
  const apanaBazarSec = siteConfig.pages && siteConfig.pages.apanabazaar === item.subsec1 ? true : false;
  const scrollStart = data.scrollStart;
  return item != null ? (
    <ErrorBoundary>
      {/* Top stories widget placed after Breadcrumb starts */}
      {/*  Top Story Widget  commented for web and mobile as per TML-1886*/}
      {/* {isMobilePlatform() ? <TopStoryWidget numItems={5} /> : null} */}
      {/* Top stories widget placed after Breadcrumb  ends*/}

      {/* Recommended news widget  for mobile placed after Topstory widget starts*/}
      {isMobilePlatform() && !apanaBazarSec ? (
        <RecommendedNewsWidget
          articleId={item.id}
          index={index}
          sectionType={item && item.pwa_meta && item.pwa_meta.sectiontype}
          isFeaturedArticle={isFeaturedArticle}
          hidethumbandsection={false}
        />
      ) : apanaBazarSec ? (
        <div>
          {rhsCSRData &&
          rhsCSRData.section &&
          Array.isArray(rhsCSRData.section) &&
          rhsCSRData.section[2] &&
          rhsCSRData.section[2].items ? (
            <ErrorBoundary>
              <div className="box-item recommended_news" data-exclude="amp">
                <div className="inner-content">
                  <div className="section">
                    <div className="top_section">
                      <h3>
                        {rhsCSRData && rhsCSRData.section[2] ? (
                          <span>
                            <AnchorLink href={rhsCSRData.section[2].wu}>{rhsCSRData.section[2].secname}</AnchorLink>
                          </span>
                        ) : (
                          <span>{rhsCSRData && rhsCSRData.section[2] && rhsCSRData.section[2].secname}</span>
                        )}
                      </h3>
                    </div>
                  </div>
                  <GridSectionMaker
                    type={defaultDesignConfigs.gridHorizontalAL}
                    data={
                      rhsCSRData &&
                      rhsCSRData.section &&
                      Array.isArray(rhsCSRData.section) &&
                      rhsCSRData.section[2] &&
                      rhsCSRData.section[2].items
                        ? [].concat(rhsCSRData.section[2].items)
                        : []
                    }
                  />
                </div>
              </div>
            </ErrorBoundary>
          ) : null}
        </div>
      ) : null}
      {/* Recommended news widget  for mobile placed after Topstory widget starts*/}

      {/*  Mrec2  only for mobile comes second time after Popular Videos Widget 
        If pwa_meta contains AdServingRules="Turnoff" MREC won't show
      */}
      {isFeaturedArticle || !isMobilePlatform() ? null : item &&
        item.pwa_meta &&
        item.pwa_meta.AdServingRules &&
        item.pwa_meta.AdServingRules === "Turnoff" ? null : (
        <div className="story-content ads-between-story">
          <AdCard mstype="mrec2" adtype="dfp" pagetype="articleshow" renderall={true} />
        </div>
      )}
      {/*  Mrec2 ends only for mobile comes after Popular Videos Widget  */}

      {/*  Popular videos for mobile placed after recommended starts*/}
      {/* <div className="row wdt_popular_videos box-item" data-exclude="amp">
        {isMobilePlatform() && _isCSR() && rhsCSRVideoData ? (
          <ErrorBoundary>
            <div
              className="section"
              data-scroll-id={`popular-videos-${item.id}`}
              data-row-id={`popular-videos${index == 0 ? "" : "-perpetual"}`}
            >
              <div className="top_section">
                <h3>
                  <span>{rhsCSRVideoData.secname}</span>
                </h3>
              </div>
            </div>

            <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={[].concat(rhsCSRVideoData.items)} />
          </ErrorBoundary>
        ) : null}
      </div> */}
      {/*  Popular videos for mobile placed after recommended  ends*/}
      <div className="brief-widget-wrapper">{scrollStart && <BriefWidget pagetype="articleshow" />}</div>
    </ErrorBoundary>
  ) : null;
};
