import React, { Component } from "react";
import { connect } from "react-redux";

import {
  fetchRecommendedTopListDataIfNeeded,
  fetchNextRecommendedTopListDataIfNeeded,
} from "../../../actions/homerecommended/homerecommended";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import NewsListCard from "../../../components/common/NewsListCard";
import ListHorizontalCard from "../../../components/common/ListHorizontalCard";
import ListSlideshowCard from "../../../components/common/ListSlideshowCard";
import AdCard from "../../../components/common/AdCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { PageMeta } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import { _isCSR, loadJS, throttle, _getCookie } from "../../../utils/util";
import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();
import { setPageType } from "../../../actions/config/config";
import ScoreCard from "../../../modules/scorecard/ScoreCard";
import { setSectionDetail } from "../../../components/lib/ads";

export class HomeRecommended extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      _scrollEventBind: false,
    };
    this.config = {
      isFetchingNext: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    let { dispatch, query, params } = this.props;
    //HomeRecommended.fetchData({ dispatch, query, params });
    dispatch(setPageType("recommended"));
    let _this = this;
    //params = params ? params : {};
    //FCP value has to be passed in feed if exists
    let fpc = _getCookie("_col_uuid") ? _getCookie("_col_uuid") : undefined;

    let referrer =
      window.locationArray && window.locationArray.length > 1
        ? window.locationArray[window.locationArray.length - 2]
        : document.referrer
        ? document.referrer
        : "";
    dispatch(fetchRecommendedTopListDataIfNeeded(params, query, fpc, referrer)).then(data => {
      if (typeof _this.props.homerecommended.value != "undefined") {
        this.scrollBind();
        //let ctnadsArr = _this.props.homerecommended.value.filter((item) => item.adSlot && item.items);
        if (!window.colombiaone) {
          loadJS(
            "https://static.clmbtech.com/ad/commons/js/cone.js",
            _this.renderCTNAds_OrganicNotify.bind(_this, _this.props.homerecommended.value),
            "personalizedCtnAdJs",
          );
        }
      }
    });

    //set recommended page section
    setSectionDetail({ subsectitle1: "home" });
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      _this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      _this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    let _this = this;
    if (this.state._scrollEventBind == false) return;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = (document.scrollingElement || document.documentElement).scrollTop;
    }
    let footerHeight = document.getElementById("footerContainer").offsetHeight;
    docHeight = docHeight - footerHeight;

    if (scrollValue + 1000 > docHeight) {
      const { dispatch, params } = this.props;
      const { query } = this.props.location;
      let isFetching = this.props.homerecommended.isFetching;
      if (isFetching == false) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        // params = params ? params : {};
        //FCP value has to be passed in feed if exists
        let fpc = _getCookie("_col_uuid") ? _getCookie("_col_uuid") : undefined;
        // console.log(window.locationArray);
        let referrer =
          window.locationArray && window.locationArray.length > 1
            ? window.locationArray[window.locationArray.length - 2]
            : document.referrer
            ? document.referrer
            : "";
        dispatch(fetchNextRecommendedTopListDataIfNeeded(params, query, fpc, referrer))
          .then(data => {
            let _data = data ? data.payload : {};
            //silentRedirect(_this.generateListingLink());
            _this.config.isFetchingNext = !_this.config.isFetchingNext;
            let startingIndex =
              _this.props.homerecommended.pn && _this.props.homerecommended.pn > 0
                ? 20 + 10 * (_this.props.homerecommended.pn - 1)
                : 0;
            _this.renderCTNAds_OrganicNotify(_this.props.homerecommended.value, startingIndex);
            //AnalyticsGA.pageview(location.origin + _this.generateListingLink());
            //Ads_module.refreshAds(['fbn']);//refresh fbn ads when next list added
            //fire ibeat for next
            //_data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
          })
          .catch(err => {
            _this.scrollUnbind();
          });
      }
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   if (this.props.params.msid != nextProps.params.msid) {
  //     const { dispatch, params } = nextProps;
  //     const { query } = nextProps.location;
  //     try {
  //       dispatch(fetchRecommendedTopListDataIfNeeded(params, query)).then((data) => {

  //         // CTN_module
  //         //CTN_module.renderCtn();
  //         //let _data = data && data.payload ? data.payload[0] : {}

  //         if (data.payload) {
  //           this.scrollBind();
  //         }
  //         else {
  //           this.scrollUnbind();
  //         }
  //       });
  //     } catch (ex) { }

  //     scrollTo(document.documentElement, 0, 100);
  //   }
  // }

  renderCTNAds_OrganicNotify(itemsArr, startingIndex = 0) {
    //startingIndex = startingIndex ? startingIndex : 0;
    if (_isCSR() && typeof window.colombiaone != "undefined") {
      for (let i = startingIndex; i < itemsArr.length; i++) {
        if (itemsArr[i].items && itemsArr[i].adSlot) {
          // console.log({ class: `${itemsArr[i].tn}_${itemsArr[i].index}`, ...itemsArr[i] })
          window.colombiaone.addItem(`${itemsArr[i].tn}_${itemsArr[i].index}`, itemsArr[i]);
        } else if (itemsArr[i].nmeta) {
          //console.log({ class: `${itemsArr[i].tn}_${itemsArr[i].index}`, nmeta: itemsArr[i].nmeta })
          window.colombiaone.addItem(`${itemsArr[i].tn}_${itemsArr[i].index}`, itemsArr[i].nmeta);
        }
      }
      window.colombiaone.render();
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(setPageType(""));
    const _this = this;
    if (_isCSR()) {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
        //clear section
        setSectionDetail();
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  render() {
    const { isFetching } = this.props.homerecommended;
    let pwa_meta =
      this.props.homerecommended && this.props.homerecommended.value ? this.props.homerecommended.value.pwa_meta : "";

    return !isFetching || this.config.isFetchingNext ? (
      <div className="body-content">
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        <div className="top-news-content box-content wdt-recommended">
          <h2>
            <span> खास आपके लिए </span>
          </h2>
          <ul className="nbt-list">
            {this.props.homerecommended &&
            this.props.homerecommended.value &&
            this.props.homerecommended.value &&
            this.props.homerecommended.value.length > 0 ? (
              this.props.homerecommended.value.map((item, index) => {
                if (item.tn == "ad" || item.mstype == "ad") {
                  return (
                    <ErrorBoundary key={index + "ad"}>
                      {
                        <AdCard
                          className={`${item.tn}_${item.index}`}
                          ctnstyle="inline"
                          mstype={item.mstype}
                          adtype={item.type}
                          elemtype="li"
                        />
                      }
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary key={"miniscorecard_" + index}>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard key={"miniscorecard_" + index} match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                          ""
                        )
                      ) : null}
                      {
                        // <ErrorBoundary key={"minischedule_" + index}><MiniSchedule key={"minischedule_" + index} schedule={this.props.homerecommended.mini_schedule} /></ErrorBoundary>
                      }
                    </ErrorBoundary>
                  );
                } else if (item.adSlot && item.items) {
                  return <li className={`${item.tn}_${item.index}`} key={`${item.tn}_${item.index}`} />;
                } else if (
                  item.tn == "news" ||
                  item.tn == "lb" ||
                  item.tn == "moviereview" ||
                  item.tn == "movieshow" ||
                  item.tn == "video" ||
                  item.tn == "poll" ||
                  item.tn == "photo" ||
                  item.tn == "html"
                ) {
                  return (
                    // Index is appended with id in Key to resolve issues if story is rearranged . Because sometimes it
                    // is noticed that if a story other than top comes at top it appears with small image which should not be
                    <ErrorBoundary key={item.id + index}>
                      <NewsListCard
                        className={`${item.tn}_${item.index}`}
                        item={item}
                        leadpost={index == 0 ? true : false}
                        key={item.id + index}
                        pagetype="home"
                      />
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary key={"miniscorecard_" + index}>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard key={"miniscorecard_" + index} match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                          ""
                        )
                      ) : null}
                    </ErrorBoundary>
                  );
                } else if (item.tn == "photolist" || item.tn == "videolist") {
                  return (
                    <ErrorBoundary key={(item.id ? item.id : item.tn) + index}>
                      <ListHorizontalCard
                        className={`${item.tn}_${item.index}`}
                        item={item}
                        key={(item.id ? item.id : item.tn) + index}
                      />
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                          ""
                        )
                      ) : null}
                    </ErrorBoundary>
                  );
                } else if (item.tn == "slideshow" || item.tn == "photostrip" || item.tn == "photopreview") {
                  return (
                    <ErrorBoundary key={item.id + index}>
                      <ListSlideshowCard
                        className={`${item.tn}_${item.index}`}
                        item={item}
                        type={item.tn}
                        key={item.id + index}
                      />
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                          ""
                        )
                      ) : null}
                    </ErrorBoundary>
                  );
                }
              })
            ) : (
              <FakeListing showImages={true} />
            )}
          </ul>
        </div>
      </div>
    ) : (
      <FakeListing />
    );
  }
}

// HomeRecommended.fetchData = function ({ dispatch, query, params }) {
//   //This pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
//   return dispatch(setPageType('home'));
//   //return dispatch(fetchRecommendedTopListDataIfNeeded(params, query));
// };

HomeRecommended.propTypes = {};

function mapStateToProps(state) {
  return {
    homerecommended: state.homerecommended,
    header: state.header,
    app: state.app,
  };
}

export default connect(mapStateToProps)(HomeRecommended);
