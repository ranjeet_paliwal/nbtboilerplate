import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchPointsTableDataIfNeeded } from "../../../actions/pointstable/pointstable";
import "../../../components/common/css/PointsTable.scss";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { scrollTo, _isCSR } from "../../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../../utils/cricket_util";

import styles from "../../../components/common/css/commonComponents.scss";

import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import AnchorLink from "../../../components/common/AnchorLink";
import ImageCard from "../../../components/common/ImageCard/ImageCard";
import AdCard from "../../../components/common/AdCard";
import Ads_module from "../../../components/lib/ads/index";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { setPageType } from "../../../actions/config/config";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

export class PointsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBind: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const { query } = this.props.location;
    dispatch(setPageType("pointstable"));
    //set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    if (
      this.props.value &&
      this.props.value[0] &&
      this.props.value[0].pwa_meta &&
      this.props.value[0].pwa_meta.cricketlb
    ) {
      params.seriesid = this.props.value[0].pwa_meta.cricketlb;
    }
    PointsTable.fetchData({ dispatch, query, params }).then(data => {
      //attach scroll only when sections is not there in feed
      if (typeof this.props.value[0] != "undefined" && !this.props.value[0].sections) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
      if (pwa_meta && typeof pwa_meta == "object") Ads_module.setSectionDetail(pwa_meta);
    });
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      // this.scrollHandler = throttle(this.handleScroll.bind(_this));
      //window.addEventListener('scroll', this.scrollHandler);
      // this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        PointsTable.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();

          if (!data.payload[0].sections) {
            this.scrollBind();
          } else {
            this.scrollUnbind();
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
    }
  }

  render() {
    const pagetype = "pointstable";
    let _this = this;
    const { isFetching, params } = this.props;
    let { sections, subsec1, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    let pointstable =
      this.props.value[1] &&
      this.props.value[1].standings &&
      this.props.value[1].standings.stage1 &&
      this.props.value[1].standings.stage1.team &&
      this.props.value[1].standings.stage1.team.length > 0
        ? this.props.value[1].standings.stage1.team
        : null;
    return this.props.value[0] && !isFetching ? (
      <div className="body-content" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(this.props.value[0].pwa_meta)}

        <div className="points_table_container">
            {/* TO DO Currently Heading is dependent on parentection items as in case when for a section child section exist but parent does not have ant stories */}
            {parentSection && parentSection.secname ? (
              <div className="sectionHeading">
                <h1>
                  <span>
                    {parentSection.pwa_meta && parentSection.pwa_meta.pageheading
                      ? parentSection.pwa_meta.pageheading
                      : parentSection.secname}
                    {parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug
                      ? " " + parentSection.pwa_meta.pageheadingslug
                      : null}
                  </span>
                </h1>
              </div>
            ) : null}

            {/*Cricket Points table Widget for event based page */
            pointstable && typeof pointstable != "null" ? (
              <ErrorBoundary key="PointsTable">
                <div className="wdt_pointsTable">
                  <div className="table_layout">
                    <table>
                      <tr>
                        <th>TEAM</th>
                        <th>P</th>
                        <th>W</th>
                        <th>L</th>
                        <th>PT</th>
                      </tr>

                      {pointstable.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td>
                              <ImageCard msid={_getTeamLogo(item.id, "square")} size="rectanglethumb" />
                              {item.name}
                            </td>
                            <td>{item.p}</td>
                            <td>{item.w}</td>
                            <td>{item.l}</td>
                            <td>{item.pts}</td>
                          </tr>
                        );
                      })}
                    </table>
                  </div>
                </div>
              </ErrorBoundary>
            ) : null}

            {isFetching ? <FakeListing showImages={true} /> : null}
        </div>
        

        {/* Breadcrumb  */}
        {parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}
        {/* For SEO Schema */ SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

PointsTable.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchPointsTableDataIfNeeded(params, query));
};

function mapStateToProps(state) {
  return {
    ...state.pointstable,
  };
}

export default connect(mapStateToProps)(PointsTable);
