import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router";
import { connect } from "react-redux";
import {
  fetchPhotoMazzaShowDataIfNeeded,
  fetchNextPhotoMazzaShowDataIfNeeded,
} from "../../../actions/photomazzashow/photomazzashow";
import FakePhotoMazzaCard from "../../../components/common/FakeCards/FakePhotoMazzaCard";
import PhotoMazzaShowCard from "../../../components/common/PhotoMazzaShowCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  elementInView,
  _isCSR,
  setHyp1Data,
  isMobilePlatform,
  handleReadMore,
  updateConfig,
  checkIsAmpPage,
  checkIsFbPage,
  _deferredDeeplink,
  filterAlaskaData,
} from "../../../utils/util";

import { fireActivity } from "../../../components/common/TimesPoints/timespoints.util";
import styles from "../../../components/common/css/PhotoMazzaShowCard.scss";

// import { fetchAmazonDataIfNeeded } from "../../../modules/amazonwidget/actions/amazonwidget";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module from "../../../components/lib/ads/index";

import { _getStaticConfig } from "../../../utils/util";
import Breadcrumb from "../../../components/common/Breadcrumb";
import PhotoStoryCard from "../../../components/common/PhotoStoryCard";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
import AdCard from "../../../components/common/AdCard";
import { setParentId, setPageType } from "../../../actions/config/config";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import AnchorLink from "../../../components/common/AnchorLink";
import WebTitleCard from "../../../components/common/WebTitleCard";
import KeyWordCard from "../../../components/common/KeyWordCard";
import { fetchRecommendedPhotoSection, nextGalLink } from "../../utils/photoshow_util";

import TPicon from "../../../components/common/TimesPoints/TPWidgets/TPicon";

import AmazonBtn from "../../../components/common/AmazonBtn/AmazonBtn";
import SvgIcon from "../../../components/common/SvgIcon";

const siteConfig = _getStaticConfig();
const locale = siteConfig.locale;

export class PhotoMazzaShow extends Component {
  constructor(props) {
    super(props);
    //Done to stop browser scroll restoration
    if (typeof window !== "undefined" && "scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    this.state = {
      curpg: 1,
      _scrollEventBind: false,
      //Default App Install Parameters
      photoshow_app_install: {
        active: false,
        show_slides: 6,
      },
      recommendedWidgetData: [],
      currentGalleryIndex: 0,
      isLoadingSlides: false,
    };
    this.config = {
      nextgalid: null,
      index: 0,
      //To refresh FBN ads on every 4th slide scroll up/down
      counter: 0,
      isFirstLoad: true,
    };
    this.scrollHandler = false;
    this.showAppInstallHandler = this.showAppInstall.bind(this);
  }

  appInstallValidate(referrer, pwa_meta) {
    const _this = this;
    const websiteUrl = process.env.WEBSITE_URL;
    if (referrer && !referrer.includes("google.com") && !referrer.includes(websiteUrl)) {
      //Fetching variables from feed to start or stop module from feed
      let app_install_active =
        pwa_meta &&
        pwa_meta.photoshow_app_install &&
        pwa_meta.photoshow_app_install.active &&
        pwa_meta.photoshow_app_install.active == "true"
          ? true
          : _this.state.photoshow_app_install.active;
      let app_install_show_slides =
        pwa_meta && pwa_meta.photoshow_app_install && pwa_meta.photoshow_app_install.show_slides
          ? parseInt(pwa_meta.photoshow_app_install.show_slides)
          : _this.state.photoshow_app_install.show_slides;
      _this.setState({
        photoshow_app_install: {
          show_slides: app_install_show_slides,
          active: app_install_active,
        },
      });
    }
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const { query } = this.props.location;
    const _this = this;

    //set section and subsection in window
    const { pwa_meta } = value[0] && value[0][0] ? value[0][0] : {};
    const slideShowId = value && value[0] && value[0][0] && value[0][0].id;

    if (params.msid) {
      // this.fetchRhsWidgetData(params.msid);
      fetchRecommendedPhotoSection(params.msid, this);
    }

    if (pwa_meta && typeof pwa_meta == "object") {
      //reset hyp1 variable
      setHyp1Data(pwa_meta);
      dispatch(setPageType("photoshow", pwa_meta.site));
      // updateConfig(pwa_meta, dispatch, setParentId);
    }

    //Readmore button handling
    handleReadMore();

    PhotoMazzaShow.fetchData({ dispatch, query, params }).then(data => {
      try {
        //set section and subsection in window

        const payloadData =
          _this.props.value && _this.props.value[0] && _this.props.value[0][0] ? _this.props.value[0][0] : {};
        const { pwa_meta, audetails } = payloadData ? payloadData : {};
        const parentId = pwa_meta.parentid;
        if (parentId) {
          updateConfig(pwa_meta, dispatch, setParentId);
        }

        // TimesPoints Activity
        const slideShowIdNew = payloadData && payloadData.id;
        if (typeof fireActivity == "function") {
          if (params.msid) {
            fireActivity("VIEW_PHOTO", params.msid);
          } else if (slideShowIdNew) {
            fireActivity("VIEW_PHOTO", slideShowIdNew);
          } else if (slideShowId) {
            fireActivity("VIEW_PHOTO", slideShowId);
          }
        }

        this.animate_btn_App();
        if (pwa_meta && typeof pwa_meta == "object") {
          Ads_module.setSectionDetail(pwa_meta);
          //reset hyp1 variable
          setHyp1Data(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";

          if (typeof data == "object" && data.type) {
            //Replace location pathname if not same with redirectUrl
            if (pwa_meta.redirectUrl.split(".com")[1] != window.location.pathname) {
              window.history.replaceState({}, pwa_meta.title, pwa_meta.redirectUrl.split(".com")[1]);
            }

            //If editor present in pwa_meta , set in window Object
            if (audetails) {
              window._editorname = audetails.cd;
            } else {
              window._editorname =
                typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
            }
            // Hit ga
            AnalyticsGA.pageview(pwa_meta.redirectUrl, {
              setEditorName: true,
            });
          }

          //activate app install module on client side
          _this.appInstallValidate(document.referrer, pwa_meta);
        }
      } catch (e) {}

      //After render function once data
      _this.afterRender();
      //To scroll to slide in case of single theme
      const msid = _this.props.params.msid;
      let u = "";
      let d = null;
      if (msid.includes("msid-") && msid.includes("picid-")) {
        var h = msid.match(/picid-[0-9]+/);
        h && h[0] && ((u = h[0].split("picid-")[1]), (d = document.getElementById(u)));
      }
      var position = 0;
      u && d && (position = d.offsetTop);
      // const y = d.getBoundingClientRect().top + window.pageYOffset - 100;

      // window.scrollTo({ top: y, behavior: "smooth" });
      //d can be null if normal photoshow opened
      d && d.scrollIntoView(), window.scrollBy(0, -100);
      // window.scrollTo({
      //   top: position,
      // });
    });

    scrollTo(document.documentElement, 0, 0);

    if (pwa_meta && typeof pwa_meta == "object") {
      //activate app install module on client side
      _this.appInstallValidate(document.referrer, pwa_meta);
    }
  }

  componentDidUpdate(prevProps) {
    //Readmore button handling
    handleReadMore();
    // resetting the config when page data is updated
    this.config = {
      nextgalid: null,
      index: 0,
      //To refresh FBN ads on every 4th slide scroll up/down
      counter: 0,
    };
    if (this.props.value[0] && this.props.value[0].length > 0) {
      this.config.index = this.props.value[0].length - 1;
    }
    if (
      prevProps.value &&
      prevProps.value[0] &&
      prevProps.value[0][0] &&
      prevProps.value[0][0].pwa_meta &&
      prevProps.value[0][0].pwa_meta.templatelayout
    ) {
      var elmnt = document.querySelector(".view-from-start");
      if (elmnt) elmnt.scrollIntoView();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.isFetching != nextProps.isFetching ||
      this.props.params.msid != nextProps.params.msid ||
      this.state.photoshow_app_install.active !== nextState.photoshow_app_install.active ||
      this.state.currentGalleryIndex !== nextState.currentGalleryIndex
    );
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      if (params.msid) {
        fetchRecommendedPhotoSection(params.msid, this);
        // this.fetchRhsWidgetData(params.msid);
        this.setState({
          recommendedWidgetData: [],
          currentGalleryIndex: 0,
        });
      }

      PhotoMazzaShow.fetchData({ dispatch, query, params });
      // Fire View_PHOTO activity after next call is successful
      if (typeof fireActivity == "function") {
        if (params.msid) {
          fireActivity("VIEW_PHOTO", params.msid);
        }
      }
      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (_isCSR()) {
      //clear section window data
      Ads_module.setSectionDetail();
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
        window.removeEventListener("scroll", this.animate_btn_App);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset hyp1 to ''
      setHyp1Data();
    }
  }

  afterRender() {
    const _this = this;
    if (_this.state._scrollEventBind == false) {
      _this.scrollHandler = throttle(_this.handleScroll.bind(_this));
      window.addEventListener("scroll", _this.scrollHandler);
      _this.state._scrollEventBind = true;

      // _this.config.observer = new IntersectionObserver(_this.intersectionCallback, { threshold: .5 });
      // _this.observerHandler(_this)
      // if(_this.props.value[0] && _this.props.value[0][0].it.id == "65337853")_this.sliderHandler()
    }
  }

  handleScroll(e) {
    let _this = this;
    if (this.state._scrollEventBind == false) return;

    let winHeight = window.innerHeight;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop == 0 ? html.scrollTop : body.scrollTop;

    let footerHeight = document.getElementById("footerContainer").offsetHeight;

    docHeight = docHeight - footerHeight;
    const hasMoreSlides =
      typeof _this.props.value[0][_this.config.index].nextset !== "undefined" &&
      _this.props.value[0][_this.config.index].nextset !== "";
    const offset = hasMoreSlides ? 5000 : 4000;

    if (scrollValue + offset > docHeight && this.isFirstLoad === false) {
      let nextgalid = (_this.config.nextgalid =
        _this.props.value[0][_this.config.index] &&
        _this.props.value[0][_this.config.index].pwa_meta &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid &&
        _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid != ""
          ? _this.props.value[0][_this.config.index].pwa_meta.nextGal.msid
          : null);

      const { dispatch, params, isFetching } = this.props;
      const { query } = this.props.location;
      const galIndex = _this.config.index;
      if (!isFetching && !this.state.isLoadingSlides && (nextgalid || hasMoreSlides)) {
        if (hasMoreSlides) {
          this.setState({ isLoadingSlides: true });
        }
        try {
          PhotoMazzaShow.fetchNextData({
            dispatch,
            query,
            params,
            nextgalid,
            galIndex,
          }).then(data => {
            this.setState({ isLoadingSlides: false });
            fetchRecommendedPhotoSection(nextgalid || params.msid, this);
            let _data = data ? data.payload : {};
            // _this.config.index++;
            /*window.setTimeout(()=>{
              var event = new CustomEvent("newsadded");
              // Dispatch/Trigger/Fire the event
              document.dispatchEvent(event);
            },500);*/

            //reset hyp1 variable
            setHyp1Data(_data.pwa_meta);

            //fire ibeat for next
            // _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
          });
        } catch (ex) {
          // Exception
          console.log("Error fetching next gal data", ex);
        }
      }
    }
    this.isFirstLoad = false;
    if (!_this.config.shouldTrackerOnScroll) {
      _this.config.shouldTrackerOnScroll = document.querySelector("[data-active]")
        ? elementInView(document.querySelector("[data-active]"), true)
          ? true
          : false
        : true;
    }

    _this.config.shouldTrackerOnScroll ? _this.trackersOnScroll() : null;
  }

  trackersOnScroll() {
    let _this = this;
    let items = document.querySelectorAll(`.photo_card`);
    let navHeight = document.getElementById("headerContainer")
      ? document.getElementById("headerContainer").clientHeight
      : 95;
    for (let i = 0; i < items.length; i++) {
      let elem = items[i];
      if (elem && elementInView(elem, true)) {
        let url = elem.getAttribute("data-url");
        let parentIndex =
          elem.parentElement && elem.parentElement.getAttribute("parent-index")
            ? elem.parentElement.getAttribute("parent-index")
            : 0;
        let pwa_meta = _this.props.value[0][parseInt(parentIndex)].pwa_meta;
        const audetails = _this.props.value[0][parseInt(parentIndex)].audetails;
        let _title = elem.querySelector("img") ? elem.querySelector("img").getAttribute("alt") : pwa_meta.title;

        // Get a reference to current gallery parent, to check if it
        // was the last one laoded ( or has been viewed once)
        let viewedOnceMarker =
          elem.parentElement && elem.parentElement.getAttribute("viewed")
            ? elem.parentElement.getAttribute("viewed")
            : false;
        if (parentIndex != this.state.currentGalleryIndex) {
          // this.refreshRhsAd();

          // Refresh FBN ad while moving down, parent index contains each gallery index
          if (parentIndex > this.state.currentGalleryIndex) {
            Ads_module.refreshAds(["fbn"]);
            // If this is false, the current gallery has come into view
            // for the first time in perpetual (has been viewed once)
            if (!viewedOnceMarker) {
              // Initialise attribute to true, signifying the gallery
              // has been viewed once, otherwise refresh ad.
              if (elem && elem.parentElement) {
                elem.parentElement.setAttribute("viewed", true);
              }
            } else {
              Ads_module.refreshAds(["atf"], null, { viewportCheck: true });
            }
          }
          this.setState({
            currentGalleryIndex: parseInt(parentIndex),
          });
        }

        if (url != location.pathname) {
          window.history.replaceState({}, _title, url);
          document.title = _title;
        }

        // trigger page view on page load
        if (!elem.getAttribute("gatracked")) {
          //If editor present in pwa_meta , set in window Object
          window._editorname = "";
          if (audetails) {
            window._editorname = audetails.cd;
          } else {
            window._editorname =
              typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
          }
          AnalyticsGA.pageview(url, {
            setEditorName: true,
            comscorepageview: elem.getAttribute("comscorepageview"),
          });
          //fire ibeat
          if (pwa_meta.ibeat) {
            if (elem.getAttribute("id") != "") {
              pwa_meta.ibeat.articleId = elem.getAttribute("id");
            }
            setIbeatConfigurations(pwa_meta.ibeat);
          }
          elem.setAttribute("gatracked", true);
          elem.setAttribute("comscorepageview", true);

          //To refresh FBN ads on every 4th slide up/down
          // #### Start
          _this.config.counter += 1;

          if (_this.config.counter == 3) {
            Ads_module.refreshAds(["fbn"]);
            _this.config.counter = 0;
          }
          // #### End
        }
      } else {
        if (elem && elem.getAttribute("gatracked")) {
          elem.removeAttribute("gatracked");
        }
      }
    }
  }

  showAppInstall(items, msid) {
    let _this = this;
    if (_this.state.photoshow_app_install && !_this.state.photoshow_app_install.active) return false;
    let app_install_show_slides = _this.state.photoshow_app_install.show_slides
      ? _this.state.photoshow_app_install.show_slides
      : 0;
    let imagepos = "";
    for (let i = 0; i < items.length; i++) {
      if (items[i].id == msid) {
        imagepos = i;
        break;
      }
    }
    return items.length > app_install_show_slides && imagepos <= app_install_show_slides ? true : false;
  }

  getRecommendedWidget(item) {
    const data = item && item.recommendedItems;
    // FIXME: For mobile, dont really need to use state
    return (
      <ErrorBoundary>
        {/* ALWAYS RETURN NULL FROM ERROR BOUNDARY TO AVOID SSR ERROR */}
        {/* New recommended widget */}
        {data && data.stry && Array.isArray(data.stry) ? (
          <div className="row wdt_list_vertical">
            {/* //   <div className="section">
          //     <div className="top_section">
          //       <h2>
          //         <span>Popular</span>
          //       </h2>
          //     </div>
          //   </div> */}
            <SectionHeader sectionhead={locale.photoshowrhsheading} />
            <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={data && data.stry} />
          </div>
        ) : null}

        {/* FIXME: Removed most viewed for mobile */}
        {/* {item.recommended &&
          item.recommended.most_viewed_photo &&
          item.recommended.most_viewed_photo[0] &&
          item.recommended.most_viewed_photo[0].items && (
            <div className="row wdt_list_vertical">
              <SectionHeader
                // sectionhead={item.recommended.most_viewed_photo[0].secname}
                sectionhead={item.recommended.most_viewed_photo[0].secname}
              />
              <GridSectionMaker
                type={defaultDesignConfigs.horizontalSlider}
                data={item.recommended.most_viewed_photo[0].items}
              />
            </div>
          )} */}
      </ErrorBoundary>
    );
  }

  //Scroll Page to next article
  scrollInView = (e, msid) => {
    e.preventDefault();
    var nextArtPos = document.getElementById(msid)
      ? document.getElementById(msid).offsetTop
      : document.querySelector(".fake-photoview")
      ? document.querySelector(".fake-photoview").offsetTop
      : null;
    if (nextArtPos != null) {
      window.scrollTo(0, nextArtPos);
    } else {
      // if no next article call is made, scroll to footer area, which will solve the purpose anyway
      const footer = document.getElementById("footerContainer");
      if (footer) {
        footer.scrollIntoView({ behavior: "smooth", block: "end" });
      }
    }
  };

  animate_btn_App() {
    // Check is button visible or not
    let ele = document.querySelector('[data-attr="btn_openinapp"]');

    if (ele) {
      ele.style.left = "-175px";
      ele.style.display = "none";

      // window.addEventListener("scroll_down", function() {
      //   ele.style.left = "0";
      // });

      window.addEventListener("scroll_up", function() {
        ele.style.display = "block";
        ele.style.left = "0";
      });
    }
  }

  render() {
    const { isFetching, value, params, isInlineContent, view, router, error, header } = this.props;
    const pagetype = "photoshow";
    let { query, pathname } = this.props.location;

    let isAmpPage = false;

    if (typeof pathname === "string" && pathname.includes("amp_")) {
      isAmpPage = true;
    }
    const newsArticleSchema = isAmpPage ? SeoSchema({ pagetype: "articleshow" }).attr().NewsArticle : null;
    // Decide layout photostory or old concept on the basis of templatelayout in pwa_meta
    // If templatelayout is undefined or blank ? New Version : Old version
    // const isphotostory = (value[0] && value[0][0] && value[0][0].pwa_meta && value[0][0].pwa_meta.templatelayout && value[0][0].pwa_meta.templatelayout != 'undefined' && value[0][0].pwa_meta.templatelayout != '') ? false : true;
    const appDefferredLink =
      typeof value[0] != "undefined" &&
      value[0].length > 0 &&
      this.state.currentGalleryIndex >= 0 &&
      value[0][this.state.currentGalleryIndex] &&
      value[0][this.state.currentGalleryIndex].it &&
      value[0][this.state.currentGalleryIndex].it.id != ""
        ? _deferredDeeplink("photo", siteConfig.appdeeplink, value[0][this.state.currentGalleryIndex].it.id)
        : "";

    const pwaConfigAlaska = filterAlaskaData(header.alaskaData, ["pwaconfig"], "label");

    let configFloatBtn = filterAlaskaData(header.alaskaData, ["pwaconfig", "floatingamazon"], "label");

    if (!isInlineContent) {
      return (
        <div className="body-content">
          {typeof value[0] != "undefined" && value[0].length > 0 && value[0][0] && value[0][0].it ? (
            value[0].map((item, index) => {
              // if(item.pwa_meta && item.pwa_meta.templatelayout && item.pwa_meta.templatelayout != 'undefined' && item.pwa_meta.templatelayout != '' && params.msid && !params.picid){// }
              return item.it ? (
                <ErrorBoundary key={item.it.id}>
                  {/* For SEO Meta */
                  index == 0 && item.pwa_meta ? PageMeta(item.pwa_meta) : null}

                  {index > 0 ? (
                    <React.Fragment>
                      <div className={"story_partition"}>
                        <span>{locale.next_gallery}</span>
                      </div>

                      <AdCard
                        mstype="atf"
                        adtype="dfp"
                        elemtype="div"
                        // esiad={true}
                        pagetype={pagetype}
                        query={query}
                        content={item}
                      />
                    </React.Fragment>
                  ) : null}
                  <div
                    className={styles["photomazzashow-content"] + " photomazzashow-content"}
                    itemType="https://schema.org/ImageGallery"
                    itemScope
                    id={item.it.id}
                    // {...newsArticleSchema}
                  >
                    {/* <span itemType="https://schema.org/ImageObject" itemScope="itemScope" itemProp="image	">
                      <meta
                        itemProp="url"
                        content={siteConfig.imageconfig.imagedomain + item.id + ",width-1200,height-900/pic.jpg"}
                      />
                      <meta content="1200" itemProp="width" />
                      <meta content="900" itemProp="height" />
                    </span>
                    {SeoSchema().publisherObj()}
                    {SeoSchema().metaTag({ name: "author", content: "websitename" })} */}
                    <meta itemProp="headline" content={item.it.hl || item.secname} />
                    <meta itemProp="mainEntityOfPage" content={item.pwa_meta.canonical} />
                    <meta itemProp="dateModified" content={item.pwa_meta.modified} />
                    <meta itemProp="datePublished" content={item.pwa_meta.publishtime} />
                    {typeof item.pwa_meta.first !== "undefined" && (
                      <div className="ps_view_from_start">
                        <Link to={item.it.wu}>
                          {locale.view_gallery_from_start}
                          <SvgIcon name="nextstory" className="nextstory" />
                        </Link>
                      </div>
                    )}
                    <div className="title_with_tpicon">
                      <h1>{item.it.hl || item.secname}</h1>
                      <ErrorBoundary>
                        <TPicon basePath={siteConfig.mweburl} pageType={"photoshow"} />
                      </ErrorBoundary>
                    </div>

                    {item.it.id != params.msid &&
                    index === 0 &&
                    item.pwa_meta &&
                    item.pwa_meta.templatelayout == "Layout1" ? (
                      <Link to={item.it.wu} className={styles["view-from-start"] + " view-from-start"}>
                        {locale.view_gallery_from_start}
                      </Link>
                    ) : null}
                    {/* if templatelayout = layout1 ? PhotoMazzaShowCard : PhotoStoryCard */}
                    {item.pwa_meta && item.pwa_meta.templatelayout && item.pwa_meta.templatelayout == "Layout1" ? (
                      <PhotoMazzaShowCard
                        parentid={item.it.id}
                        photoshow_app_install={this.state.photoshow_app_install}
                        showAppInstall={this.showAppInstallHandler}
                        item={item}
                        msid={params.msid}
                        parentIndex={index}
                        isLoadingSlides={this.state.isLoadingSlides}
                        isAmpPage={isAmpPage}
                      />
                    ) : (
                      <PhotoStoryCard
                        parentid={item.it.id}
                        photoshow_app_install={this.state.photoshow_app_install}
                        showAppInstall={this.showAppInstallHandler}
                        item={item}
                        msid={item.it.id}
                        parentIndex={index}
                        picid={params.picid}
                        isLoadingSlides={this.state.isLoadingSlides}
                        router={router}
                        pwaConfigAlaska={pwaConfigAlaska}
                      />
                    )}
                    {/* For amp */}
                    {item.pg.cnt > 10 &&
                      (checkIsAmpPage(this.props.location.pathname) || checkIsFbPage(this.props.location.pathname)) && (
                        <AnchorLink
                          href={item.pwa_meta && item.pwa_meta.nextGal && item.pwa_meta.nextGal.url}
                          className="more-btn"
                        >
                          {siteConfig.locale.next_topic.more_photos}
                        </AnchorLink>
                      )}
                    {/* Breadcrumb  */}
                    {typeof value[0] != "undefined" &&
                      value[0].length > 0 &&
                      value[0][0] &&
                      typeof value[0][0].breadcrumb != "undefined" &&
                      value[0][0].breadcrumb.div &&
                      value[0][0].breadcrumb.div.ul && (
                        <ErrorBoundary>
                          <Breadcrumb items={value[0][0].breadcrumb.div.ul} />
                        </ErrorBoundary>
                      )}
                    {item.pwa_meta && WebTitleCard(item.pwa_meta, "photoshow")}
                    {item.pwa_meta && item.pwa_meta.topicskey && (
                      <div className="keywords_wrap">{KeyWordCard(item.pwa_meta.topicskey, "photoshow")}</div>
                    )}
                    {/* Next gallery */}
                    {item.pwa_meta && item.pwa_meta.nextGal && item.pwa_meta.nextGal !== "" && (
                      <div className="next_article" data-attr="next_article">
                        <a
                          onClick={e => this.scrollInView(e, item.pwa_meta.nextGal.msid)}
                          data-nextart={item.pwa_meta.nextGal.msid}
                          href={nextGalLink(item, pagetype)}
                        >
                          <span className="nxttitle">
                            <span className="text_ellipsis">{item.pwa_meta.nextGal.title}</span>
                          </span>
                          <span className="nxtlink">{siteConfig.locale.next_gallery}</span>
                        </a>
                      </div>
                    )}
                  </div>

                  {this.getRecommendedWidget(item, index)}
                </ErrorBoundary>
              ) : null;
            })
          ) : (
            <FakePhotoMazzaCard showImages={true} />
          )}

          {/* Open in App Button Floating */
          appDefferredLink && configFloatBtn && configFloatBtn._type == "appinstall" && _isCSR() ? (
            ReactDOM.createPortal(
              <a
                className="btn_openinapp"
                data-attr="btn_openinapp"
                style={!isAmpPage ? { display: "none" } : null}
                href={appDefferredLink}
              >
                {siteConfig.locale.see_in_app}
              </a>,
              document.getElementById("share-container-wrapper"),
            )
          ) : appDefferredLink && configFloatBtn && configFloatBtn._type == "appinstall" ? (
            <a
              className="btn_openinapp"
              data-attr="btn_openinapp"
              style={!isAmpPage ? { display: "none" } : null}
              href={appDefferredLink}
            >
              {siteConfig.locale.see_in_app}
            </a>
          ) : null}

          {configFloatBtn && configFloatBtn._type == "amazon" && (
            <AmazonBtn data={configFloatBtn} isAmpPage={isAmpPage} />
          )}

          {isFetching ? <FakePhotoMazzaCard showImages={true} /> : null}
        </div>
      );
    } else {
      return (
        <div key={this.props.params.msid}>
          {typeof value[0] != "undefined" && value[0].length > 0 ? (
            value[0].map((item, index) => {
              return (
                <ErrorBoundary key={index}>
                  <PhotoMazzaShowCard
                    item={item}
                    msid={params.msid}
                    isInlineContent={isInlineContent}
                    view={view}
                    isAmpPage={isAmpPage}
                  />
                </ErrorBoundary>
              );
            })
          ) : (
            <FakePhotoMazzaCard showImages={true} />
          )}
        </div>
      );
    }
  }
}

PhotoMazzaShow.fetchData = function({ dispatch, query, params, router }) {
  //set pagetype
  // dispatch(setPageType("photoshow"));

  // dispatch(fetchAmazonDataIfNeeded(dispatch, params, query, router, "amazonMobile"));
  return dispatch(fetchPhotoMazzaShowDataIfNeeded(params, query)).then(data => {
    const pwaMeta = (data && data.payload && data.payload.pwa_meta) || "";
    if (pwaMeta) {
      Ads_module.setSectionDetail(pwaMeta);
      dispatch(setPageType("photoshow", pwaMeta.site));
      updateConfig(pwaMeta, dispatch, setParentId);
    }
    return data;
  });
};

PhotoMazzaShow.fetchNextData = function({ dispatch, query, params, nextgalid, galIndex }) {
  return dispatch(fetchNextPhotoMazzaShowDataIfNeeded(params, query, nextgalid, galIndex));
};

function mapStateToProps(state) {
  return {
    ...state.photomazzashow,
    header: state.header,
  };
}

export default connect(mapStateToProps)(PhotoMazzaShow);
