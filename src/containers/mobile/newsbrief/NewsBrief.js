import React, { Component } from "react";
import { connect } from "react-redux";

// import Breadcrumb from './../../components/common/Breadcrumb';
import { fetchListDataIfNeeded } from "../../../actions/newsbrief/newsbrief";
import Brief from "../../../components/common/Brief";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
// import ErrorBoundary from './../../components/lib/errorboundery/ErrorBoundary';
import { throttle, scrollTo, isMobilePlatform, updateConfig } from "../../../utils/util";

// import { AnalyticsGA } from './../../components/lib/analytics/index';
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
// import AnchorLink from '../../components/common/AnchorLink';
import Ads_module from "../../../components/lib/ads/index";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { setPageType, setParentId } from "../../../actions/config/config";

import { _getStaticConfig } from "../../../utils/util";
import { fetchRHSDataPromise } from "../../../actions/app/app";
const siteConfig = _getStaticConfig();

export class NewsBrief extends Component {
  constructor(props) {
    super(props);
    this.config = {
      isFetchingNext: false,
    };

    // this.listCount = 1;
    // this.listCountInc = () => {
    //   return this.listCount++;
    // };
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const { query } = this.props.location;
    if (!isMobilePlatform()) {
      fetchRHSDataPromise(dispatch);
    }

    let pwaMeta = "";
    if (value && value[0] && typeof value[0].pwa_meta == "object") {
      pwaMeta = value[0].pwa_meta;
    }

    NewsBrief.fetchData({ dispatch, query, params }).then(data => {
      //set section and subsection in window
      if (data && data.payload && Array.isArray(data.payload)) {
        const pwa_meta = data.payload[0] && data.payload[0].pwa_meta;
        if (pwa_meta && typeof pwa_meta == "object") {
          Ads_module.setSectionDetail(pwa_meta);
          pwaMeta = pwa_meta;
        }
      }
      updateConfig(pwaMeta, dispatch, setParentId);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        NewsBrief.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();

          //set section and subsection in window
          const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
          if (pwa_meta && typeof pwa_meta == "object") {
            Ads_module.setSectionDetail(pwa_meta);
            updateConfig(pwa_meta, dispatch, setParentId);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      //reset section window data
      Ads_module.setSectionDetail();
      // let navigations = document.querySelector("#paginationHomePage").children;
      // if(navigations.length>0){
      //   for(let nav of navigations){
      //     try{
      //       nav.removeEventListener('click',this.handleNavigationClick, true);
      //     }catch(ex){}
      //     nav.classList.remove("active");
      //   }
      // }
      // document.querySelector(".mymenu_slide") ? (document.querySelector(".mymenu_slide").scrollLeft = 0) : '';
    }
  }

  render() {
    const pagetype = "newsbrief";
    const { isFetching, params } = this.props;
    let items = [];
    if (this.props.value[0] && this.props.value[0].items) {
      items = this.props.value[0].items;
    }
    return this.props.value[0] && !isFetching ? (
      <div className="body-content" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(this.props.value[0].pwa_meta)}

        {items ? (
          <Brief
            briefsectioninfo={this.props.value[0].pg.briefsectioninfo}
            briefvisiblepwa={this.props.value[0].pg.briefvisiblepwa}
            briefcardno={this.props.value[0].pg.briefvisiblecount}
            item={this.props.value[0].items}
            pagetype={pagetype}
            sectionid={this.props.value[0].id}
            briefsction={this.props.value[0].pwa_meta}
          />
        ) : (
          <FakeListing />
        )}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

NewsBrief.fetchData = function({ dispatch, query, params }) {
  dispatch(setPageType("newsbrief"));
  return dispatch(fetchListDataIfNeeded(params, query));
};

function mapStateToProps(state) {
  return {
    ...state.newsbrief,
    header: state.header,
  };
}

export default connect(mapStateToProps)(NewsBrief);
