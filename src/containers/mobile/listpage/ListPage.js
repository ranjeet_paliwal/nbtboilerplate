import React, { Component } from "react";
import PropTypes, { array } from "prop-types";
import { connect } from "react-redux";
import Helmet from "react-helmet";
import Breadcrumb from "../../../components/common/Breadcrumb";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { fetchListDataIfNeeded, fetchNextListDataIfNeeded } from "../../../actions/listpage/listpage";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import {
  throttle,
  scrollTo,
  updateConfig,
  getPageType,
  extractMsidFromUrl,
  _isCSR,
  createAndCacheObserver,
  addObserverToWidgets,
  isMobilePlatform,
  checkIsAmpPage,
  _deferredLinkAtListPage,
  animate_btn_App,
} from "../../../utils/util";
import styles from "../../../components/common/css/commonComponents.scss";
import stylesSection from "../../../components/common/css/SectionWrapper.scss";
import globalconfig from "../../../globalconfig";
import { _getStaticConfig } from "../../../utils/util";

import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";

import { AnalyticsGA } from "../../../components/lib/analytics/index";
import Ads_module from "../../../components/lib/ads/index";
import AnchorLink from "../../../components/common/AnchorLink";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";

import "../../../components/common/css/ListPage.scss";
import KeyWordCard from "../../../components/common/KeyWordCard";
import YSubscribeCard from "../../../components/common/YSubscribeCard";
import AdCard from "../../../components/common/AdCard";
import AstroWidget from "../../../components/common/AstroWidget/AstroWidget";
import GridCardMaker from "../../../components/common/ListingCards/GridCardMaker";

import { setParentId, setPageType } from "../../../actions/config/config";
import fetch from "../../../utils/fetch/fetch";
// import Slider from "../../../components/desktop/Slider";
import AmazonWidget from "../../../components/common/AmazonWidget/AmazonWidget";
import OpenInAppAtALPage from "../../../components/common/OpenInAppALPage";
import ExperiencePage from "../../desktop/listpage/ExperiencePage";
import { fireGRXEvent } from "../../../components/lib/analytics/src/ga";
import SectionLayoutMemo from "../../desktop/home/SectionLayout";
const siteConfig = _getStaticConfig();

// import PageMeta from '../../components/common/PageMeta'; //For Page SEO/Head Part

export class ListPage extends Component {
  constructor(props) {
    super(props);
    if (typeof window !== "undefined" && "scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
    this.state = {
      curpg: 1,
      totalPages: 0,
      _scrollEventBind: false,
      relatedvideoData: null,
      relatedPhotoData: null,
      readMore: false,
    };
    this.config = {
      isFetchingNext: false,
      componentType: "",
      isExperiencePage: false,
    };
    this.scrollHandler = false;
    this.pagetype = "";
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({ readMore: !this.state.readMore });
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const { dispatch, value, router, route } = this.props;
    const { query } = this.props.location;
    let { params } = this.props;
    let pagetype = "";

    if (typeof params.msid === "undefined" && window.meta && window.meta.longurl) {
      params.msid = extractMsidFromUrl(window.meta.longurl);
      pagetype = this.pagetype = getPageType(window.meta.longurl);
    }
    if (params && params.msid == undefined) {
      params = this.setParams(params, router);
    }
    const parentId = (value[0] && value[0].pwa_meta && value[0].pwa_meta.parentidnew) || "";
    const photVidmsid = params.msid == "reviews" ? siteConfig.pages.techreview : params.msid;

    const observerConfig = {
      root: null,
      rootMargin: "0px",
      threshold: 0.2, // when 20% in view
    };
    // floating open in app button
    animate_btn_App();

    const observerCallback = (entries, self) => {
      entries.forEach(entry => {
        if (entry && entry.isIntersecting) {
          try {
            const widgetId = entry.target.getAttribute("data-scroll-id");
            fireGRXEvent("track", "scroll_depth", { widget_id: widgetId });

            self.unobserve(entry.target);
          } catch (e) {
            console.log("error while observing listpage widget", e);
          }
        }
      });
    };

    const { app } = this.props;

    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].listpage &&
        app.scrolldepth[process.env.SITE].listpage[process.env.PLATFORM] === "true") ||
      false;

    createAndCacheObserver(this, observerConfig, observerCallback, { shouldCreate: shouldCreateScrollDepth });
    // video data  Api call for AL page
    const relatedvideoApi = `${process.env.API_BASEPOINT}/api_relatedsecvideo.cms?msid=${photVidmsid}&tag=video&perpage=6&feedtype=sjson`;
    fetch(relatedvideoApi)
      .then(data => {
        this.setState(
          {
            relatedvideoData: data,
          },
          () => {
            // Adds observer to widgets
            addObserverToWidgets(
              this,
              { '[data-scroll-id="latest-video"]': true },
              { shouldCreate: shouldCreateScrollDepth },
            );
          },
        );
      })
      .catch({});

    const relatedPhotoApi = `${process.env.API_BASEPOINT}/sc_relatedphoto.cms?tag=latestphoto&feedtype=sjson&msid=${photVidmsid}&perpage=9`;
    fetch(relatedPhotoApi)
      .then(data => {
        this.setState(
          {
            relatedPhotoData: data,
          },
          () => {
            // Adds observer to widgets
            addObserverToWidgets(
              this,
              { '[data-scroll-id="latest-photo"]': true },
              { shouldCreate: shouldCreateScrollDepth },
            );
          },
        );
      })
      .catch({});

    // set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta === "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    ListPage.fetchData({ dispatch, query, params, history, router, pagetype }).then(data => {
      const pwaMeta = data && data.payload && data.payload[0] && data.payload[0].pwa_meta;
      // attach scroll only when sections is not there in feed
      // console.log("data111", data);
      if (typeof this.props.value[0] !== "undefined" && !this.props.value[0].sections) {
        this.scrollBind();
      }
      addObserverToWidgets(this, { '[data-scroll-id="most-read"]': true }, { shouldCreate: shouldCreateScrollDepth });
      const { pwa_meta, pg } = this.props.value[0] ? this.props.value[0] : {};
      if (pg && pg.tp) {
        this.state.totalPages = pg.tp;
      }
      if (pwa_meta && typeof pwa_meta === "object") {
        // set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        // fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      }

      if (pwaMeta) {
        updateConfig(pwaMeta, dispatch, setParentId);
      }
    });

    if (value && value[0] && value[0].pwa_meta) {
      updateConfig(value && value[0] && value[0].pwa_meta, dispatch, setParentId);
    }
  }
  // //TODO: Correct this
  // addObserverToWidgets(rowsSelector) {
  //   if (_isCSR()) {
  //     if (!this.memoizedObserver) {
  //       this.memoizedObserver = trackHomePageScrollDepth(this.IObserver, {});
  //     }

  //     this.memoizedObserver(rowsSelector);
  //   }
  // }

  setParams(params, router) {
    // console.log("params, router------", params, router);
    const pathname = router.location.pathname;
    if (pathname.indexOf("/tech/reviews") > -1) {
      params.msid = siteConfig.pages.techreview;
    }
    return params;
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window !== "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    const _this = this;
    if (this.state._scrollEventBind == false) return;

    const winHeight = window.innerHeight;

    const body = document.body;
    const html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = document.documentElement.scrollTop;
    }
    const footerCont = document.querySelector("#footerContainer");
    const moreBtnEle = document.querySelector(".more-btn");
    // const breadcrumbEle = document.querySelector(".breadcrumb");
    // if (footerCont && !this.props.value[0].sections) {
    //   footerCont.classList.add("hideFooter");
    //   moreBtnEle && moreBtnEle.classList.add("hideFooter");
    //   // breadcrumbEle && breadcrumbEle.classList.add("hideFooter");
    // }
    // docHeight -= footerHeight;
    const footerHeight = document.getElementById("footerContainer").offsetHeight;
    docHeight -= footerHeight;
    const curpg = parseInt(this.props.value[0].pg.cp) + 1;
    if (curpg <= this.state.totalPages && _this.isExperiencePage === false) {
      const { dispatch, params, isFetching, router } = this.props;
      const { query } = this.props.location;
      let pagetype = this.pagetype;
      if (typeof params.msid === "undefined" && window.meta && window.meta.longurl && window.meta.longurl != "") {
        params.msid = extractMsidFromUrl(window.meta.longurl);
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      }
      if (isFetching == false && !_this.config.isFetchingNext) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        ListPage.fetchNextListData({ dispatch, query, params, router, pagetype }).then(data => {
          _this.config.isFetchingNext = !_this.config.isFetchingNext;
          const _data = data ? data.payload : {};
          // silentRedirect(_this.generateListingLink());
          // console.log('gatrack' + (location.origin+ _this.generateListingLink()));
          // AnalyticsGA.pageview(location.origin + _this.generateListingLink());
          // _this.config.isFetchingNext = !_this.config.isFetchingNext;
          // fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";

          let url;
          url = location.origin + _this.generateListingLink();

          if (url != location.href || _data.pwa_meta.title != document.title) {
            // todo stop history push for webview
            window.history.replaceState({}, _data.pwa_meta.title, url);
            // Firing pageview of next pages loaded
            AnalyticsGA.pageview(window.location.pathname);
            document.title = _data.pwa_meta.title;
          }
        });
      }
      // if (footerCont && curpg === this.state.totalPages) {
      //   footerCont.classList.remove("hideFooter");
      //   moreBtnEle && moreBtnEle.classList.remove("hideFooter");
      //   breadcrumbEle && breadcrumbEle.classList.remove("hideFooter");
      // }
    }
  }
  generateListingLink(type) {
    const props = this.props;

    if (props.location && props.location.pathname && props.value[0] && props.value[0].pg && props.value[0].pg.cp) {
      const curpg = type == "moreButton" ? parseInt(props.value[0].pg.cp) + 1 : parseInt(props.value[0].pg.cp);
      if (props.location.pathname.indexOf("curpg") > -1) {
        const reExp = /curpg=\\d+/;
        return props.location.pathname.replace(reExp, `curpg=${curpg}`);
      }
      return props.location.pathname + ((props.location.pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.msid != nextProps.params.msid ||
      this.props.location.pathname != nextProps.location.pathname
    ) {
      const { dispatch, params, router, route, history } = nextProps;
      const { query } = nextProps.location;
      let pagetype = "";
      if (route && route.path && route.path == "/tech/reviews.cms") {
        params.msid = siteConfig.pages.techreview;
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      } else if (typeof params.msid === "undefined" && window.meta && window.meta.longurl) {
        params.msid = extractMsidFromUrl(window.meta.longurl);
        pagetype = this.pagetype = getPageType(window.meta.longurl);
      } else if (typeof params.msid !== "undefined" && nextProps.location && nextProps.location.pathname) {
        window.meta = {};
        window.meta.longurl = nextProps.location.pathname;
      }
      const { app } = this.props;

      const shouldCreateScrollDepth =
        (app &&
          app.scrolldepth &&
          app.scrolldepth[process.env.SITE] &&
          app.scrolldepth[process.env.SITE].listpage &&
          app.scrolldepth[process.env.SITE].listpage[process.env.PLATFORM] === "true") ||
        false;
      try {
        ListPage.fetchData({ dispatch, query, params, history, router, pagetype }).then(data => {
          const _data = this.props.value[0] ? this.props.value[0] : {};
          const { pg } = _data;
          if (pg && pg.tp) {
            this.state.totalPages = pg.tp;
          }
          if (_data && !_data.sections) {
            this.scrollBind();
          } else {
            this.scrollUnbind();
          }

          addObserverToWidgets(
            this,
            { '[data-scroll-id="most-read"]': true },
            { shouldCreate: shouldCreateScrollDepth },
          );
          // fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
          if (_data && _data.pwa_meta) {
            updateConfig(_data && _data.pwa_meta, dispatch, setParentId);
            if (!(_data.pwa_meta.AdServingRules && _data.pwa_meta.AdServingRules == "Turnoff"))
              Ads_module.setSectionDetail(_data.pwa_meta);
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }

    if (nextProps && nextProps.params && nextProps.params.msid) {
      animate_btn_App();
    }
  }

  /* Its a generic function to create Sectional Layout in Home Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({
    dataObj,
    datalabel,
    configlabel,
    gridclasses,
    override,
    sectionId,
    weblink,
    headingTag,
    compType,
    sectionHeading,
    isSectionHead = true, // this is false if section head is not required
    astroListingWidget,
    key,
    isTopSection,
  }) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    const sectionhead = sectionHeading || (dataObj && dataObj.secname) || (dataObj && dataObj.name) || undefined;
    override = override || {};
    const secId = sectionId || (dataObj[datalabel] && dataObj[datalabel].id);
    const parentSecId = (dataObj && dataObj.pwa_meta && dataObj.pwa_meta.parentidnew) || undefined;

    const { header } = this.props;
    const headerCopy = header ? JSON.parse(JSON.stringify(header)) : ""; // for copying Objects by Reference

    try {
      if (dataObj && dataObj.items && !Array.isArray(dataObj.items)) {
        const arrObj = [];
        arrObj.push(dataObj.items);
        dataObj.items = arrObj.slice();
      }
      let astroData = [];
      if (dataObj && dataObj.id == siteConfig.astrowidgetId && dataObj.subsec1 == siteConfig.astrowidgetId) {
        astroData = [
          astroListingWidget &&
            astroListingWidget.items &&
            Array.isArray(astroListingWidget.items) &&
            astroListingWidget.items[0],
        ];
        dataObj.items && dataObj.items.splice(0, 1);
      }

      return dataObj && dataObj.items ? (
        <div className={`${gridclasses} box-item row`}>
          {isSectionHead && sectionhead && (
            <ErrorBoundary>
              <SectionHeader
                data={secId && headerCopy ? headerCopy.alaskaData : undefined}
                sectionId={secId}
                sectionhead={sectionhead}
                weblink={weblink}
                headingTag={headingTag}
                compType={compType}
                parentSecId={parentSecId}
              />
              {/* <SectionHeader sectionhead={sectionhead} headingTag="h1" /> */}
              {/* petrol diesel widget for NBT */}

              <Petrolwidget data={dataObj} />
            </ErrorBoundary>
          )}

          {dataObj && dataObj.id == siteConfig.astrowidgetId && astroData && astroData.length > 0 ? (
            <div className="col12">
              <ul className="astro_widget in-AL">
                {astroData ? (
                  <React.Fragment>
                    <GridCardMaker card={astroData[0]} cardType="horizontal" imgsize="smallthumb" />
                    <li className="astro_text">
                      <span className="text_ellipsis">{astroData[0].syn}</span>
                    </li>
                  </React.Fragment>
                ) : null}

                <ErrorBoundary>
                  <AstroWidget item={astroData[0]} />
                </ErrorBoundary>
              </ul>
            </div>
          ) : null}
          <GridSectionMaker
            type={defaultDesignConfigs[configlabel]}
            data={dataObj.items}
            override={override}
            // imgsize={imgsize}
            compType={compType}
            key={key}
            isTopSection={isTopSection}
          />
        </div>
      ) : // <FakeListing />
      null;
    } catch (err) {
      console.log(err);
    }
  }

  componentWillUnmount() {
    delete window.meta;
    const _this = this;
    if (typeof window !== "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      // Reset section window object
      Ads_module.setSectionDetail();
    }
    // const footerCont = document.querySelector("#footerContainer");
    // if (footerCont) {
    //   footerCont.classList.remove("hideFooter");
    // }
  }

  getDataList(section, noOfEle, startIndex = 0) {
    const tmpSection = JSON.parse(JSON.stringify(section));
    if (tmpSection.items && tmpSection.items.length > 0) {
      if (noOfEle > 0) {
        tmpSection.items = tmpSection.items.splice(startIndex, noOfEle);
      } else {
        tmpSection.items = tmpSection.items.splice(startIndex);
      }
    }
    return tmpSection;
  }

  render() {
    const { isFetching, value, params, error, astroListingWidget, router } = this.props;
    const { sections, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";

    this.config.componentType = parentSection ? parentSection.tn : "";
    this.config.componentType = this.config.componentType === "movieshowlist" ? "movielist" : this.config.componentType;
    const compType = this.config.componentType;
    // For Cartoon Photolist sigle columnlayout required. Cartoon Section msid present in config (nbt.js)
    // If singleCol is true, render each slide as lead post
    const singleCol = !!(siteConfig.photoListView && siteConfig.photoListView.indexOf(params.msid) > -1);

    const trendingItems =
      parentSection.recommended && parentSection.recommended.trending && parentSection.recommended.trending.items
        ? // &&
          // parentSection.recommended.trending.items.length > 0
          parentSection.recommended.trending.items
        : undefined;
    const nextItems =
      parentSection.recommended &&
      parentSection.recommended.nextitems &&
      parentSection.recommended.nextitems.items &&
      parentSection.recommended.nextitems.items.length > 0
        ? parentSection.recommended.nextitems.items
        : undefined;

    const parentId = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.subsec1) || "";

    let pathname = router && router.location && router.location.pathname;
    let isAmpPage = checkIsAmpPage(pathname);

    let secName = parentSection && parentSection.secname;
    const msid = parentSection && parentSection.pwa_meta && parentSection.pwa_meta.msid;

    // const headerCopy = header ? JSON.parse(JSON.stringify(header)) : "";
    // video data  for last lavel
    const { relatedvideoData } = this.state;
    if (compType == "articlelist" && this.props.value[0] && this.props.value[0].ui_config) {
      this.isExperiencePage = true;
      return (
        <React.Fragment>
          {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
          <div
            className="section-wrapper body-content experiencepage"
            {...SeoSchema({ pagetype: compType }).attr().itemList}
          >
            {parentSection.items && parentSection.items.length > 0
              ? SeoSchema().metaTag({
                  name: "numberOfItems",
                  content: parentSection.items.filter(
                    item => !!(item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")),
                  ).length,
                })
              : null}
            {parentSection.pwa_meta
              ? SeoSchema().metaTag({
                  name: "url",
                  content: parentSection.pwa_meta.canonical,
                })
              : null}
            <OpenInAppAtALPage msid={msid} compType={compType} isAmpPage={isAmpPage} secName={secName} />
            <ExperiencePage uiConfig={this.props.value[0].ui_config} router={router} parentSecId={parentId} />
          </div>
        </React.Fragment>
      );
    }
    this.isExperiencePage = false;
    return this.props.value[0] && (!isFetching || this.config.isFetchingNext) ? (
      <div
        className={`body-content ${compType} ${sections != undefined ? "L1" : "L2"}`}
        {...SeoSchema({ pagetype: compType }).attr().itemList}
      >
        {/* For SEO Meta */ parentSection.pwa_meta ? PageMeta(parentSection.pwa_meta) : null}
        {parentSection.items && parentSection.items.length > 0
          ? SeoSchema().metaTag({
              name: "numberOfItems",
              content: parentSection.items.filter(
                item => !!(item && item.tn && (item.tn == "photo" || item.tn == "news" || item.tn == "video")),
              ).length,
            })
          : null}
        {parentSection.pwa_meta
          ? SeoSchema().metaTag({
              name: "url",
              content: parentSection.pwa_meta.canonical,
            })
          : null}

        {/* To show Youtube icon */}
        {compType == "videolist" ? (
          <div className="con_social">
            <YSubscribeCard />
          </div>
        ) : null}

        {/* ATF text read more toggle */}
        {parentSection && parentSection.pwa_meta && parentSection.pwa_meta.atf ? (
          <div className="col12">
            <div className="top_atf">
              <div className={`caption text_ellipsis ${!this.state.readMore ? "" : "more"}`}>
                {parentSection.pwa_meta.atf}
              </div>
              <span onClick={this.toggle} className="rdmore" data-exclude="amp">
                {!this.state.readMore ? "..." + siteConfig.locale.readmore : ""}
              </span>
            </div>
          </div>
        ) : null}

        {/* To show Top section of VL/PL/AL in L1 */}
        <TopSection
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          createSectionLayout={this.createSectionLayout.bind(this)}
          astroListingWidget={astroListingWidget}
          recommendedSec={this.state.relatedvideoData}
          recommendedPhoto={this.state.relatedPhotoData}
        />
        {/* For Display articlelist level L2 */}
        <ArticleListingPage
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          createSectionLayout={this.createSectionLayout.bind(this)}
        />
        {/* To show sectional data of photolist/videolist in L1 */}
        {/**
         * 0(or -ve value) noOfElements mean all items comes under that particular section
         * for videolist latest section shows all items comes under latest
         */}
        <SectionalData
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          createSectionLayout={this.createSectionLayout.bind(this)}
        />
        {/**
         * For rendering Articlelist sections in L1/L2.
         */}
        <ArticleListSections
          compType={compType}
          parentSection={parentSection}
          sections={sections}
          getDataList={this.getDataList.bind(this)}
          createSectionLayout={this.createSectionLayout.bind(this)}
        />
        {/* For Display photolist/Videolist level p2 */}
        <ListingData
          parentSection={parentSection}
          createSectionLayout={this.createSectionLayout.bind(this)}
          compType={compType}
          sections={sections}
        />
        {/* NBT Business section ET iframs */}
        {/* {process.env.SITE === "nbt" && parentSection && parentSection.subsec1 === siteConfig.pages.business ? (
          <ETiframs />
        ) : null} */}
        {/* supethit widget only for articlelist last lavel */}
        {sections == undefined ? (
          <SuperHitWidget
            compType={compType}
            parentSection={parentSection}
            recommendedSec={this.state.relatedvideoData}
            recommendedPhoto={this.state.relatedPhotoData}
          />
        ) : null}

        {/* To Display trending section and next Item section for VL/PL */}
        {/* Below code is used for next items and trending section in L1/L2 */}
        {trendingItems && compType !== "articlelist" ? (
          <RecommendedSec
            recommendedSec={parentSection.recommended.trending}
            createSectionLayout={this.createSectionLayout.bind(this)}
            compType={compType}
            istrendingSec="true"
          />
        ) : null}
        {compType == "videolist" && sections != undefined ? (
          <div className="ad-wrapper-250">
            <AdCard mstype="mrec2" adtype="dfp" />
          </div>
        ) : null}

        {compType != "articlelist" && nextItems && sections == undefined ? (
          <RecommendedSec
            recommendedSec={parentSection.recommended.nextitems}
            createSectionLayout={this.createSectionLayout.bind(this)}
            compType={compType}
          />
        ) : null}

        {/* <div className="video-slider">
          {compType == "articlelist" && sections == undefined && relatedvideoData && relatedvideoData.items ? (
            <RecommendedSec
              recommendedSec={relatedvideoData}
              compType={compType}
              createSectionLayout={this.createSectionLayout.bind(this)}
              istrending="true"
            />
          ) : null}
        </div> */}

        <OpenInAppAtALPage msid={msid} compType={compType} isAmpPage={isAmpPage} secName={secName} />
        <AmazonWidget item={value && value[0]} type="sponsored" router={router} techPageOnly />

        {(compType == "articlelist" || compType == "movielist") &&
        parentSection.recommended &&
        parentSection.recommended.trendingtopics &&
        parentSection.recommended.trendingnow &&
        ((parentSection.recommended.trendingnow[0] && parentSection.recommended.trendingnow[0].items) ||
          (parentSection.recommended.trendingtopics[0] && parentSection.recommended.trendingtopics[0].items)) ? (
          // &&
          // parentSection.recommended.trendingtopics[0].items.length > 0
          <div className="row box-item">
            <div className="col12 trending-bullet">
              <KeyWordCard
                items={[]
                  .concat(
                    parentSection.recommended.trendingnow[0] && parentSection.recommended.trendingnow[0].items
                      ? parentSection.recommended.trendingnow[0].items
                      : [],
                  )
                  .concat(
                    parentSection.recommended.trendingtopics[0] && parentSection.recommended.trendingtopics[0].items
                      ? parentSection.recommended.trendingtopics[0].items
                      : [],
                  )}
                secname={parentSection.recommended.trendingtopics[0].secname}
              />
            </div>
          </div>
        ) : null}
        {/* LAN-6019 merging trending-topics  and trending now for articlelist  */}
        {/* {(compType == "articlelist" ||compType == "movielist") &&
          (parentSection.recommended &&
          parentSection.recommended.trendingnow &&
          parentSection.recommended.trendingnow[0].items ? (
            // &&
            // parentSection.recommended.trendingnow[0].items.length > 0
            <div className="row box-item">
              <div className="col12 list-trending-now">
                <KeyWordCard
                  items={parentSection.recommended.trendingnow[0].items}
                  secname={parentSection.recommended.trendingnow[0].secname}
                />
              </div>
            </div>
          ) : null)} */}
        <PerpetualData value={value} createSectionLayout={this.createSectionLayout.bind(this)} compType={compType} />

        {/* BTF content */}
        {parentSection && parentSection.pwa_meta && parentSection.pwa_meta.syn ? (
          <ErrorBoundary>
            <div className="btf_con" dangerouslySetInnerHTML={{ __html: parentSection.pwa_meta.syn || "" }}></div>
          </ErrorBoundary>
        ) : null}

        {isFetching ? <FakeListing showImages /> : null}

        {parentSection && !sections && parseInt(parentSection.pg.cp) < parseInt(parentSection.pg.tp) ? (
          <AnchorLink
            hrefData={{ override: this.generateListingLink("moreButton") }}
            className={`${styles["more-btn"]} more-btn`}
          >
            {siteConfig.locale.read_more_listing}
          </AnchorLink>
        ) : (
          ""
        )}

        {/* Breadcrumb  */}
        {parentSection && typeof parentSection.breadcrumb !== "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

ListPage.fetchData = function({ dispatch, query, params, history, router, pagetype }) {
  // dispatch(setPageType("articlelist"));
  return dispatch(fetchListDataIfNeeded(params, query, router, pagetype)).then(data => {
    let pwaMeta;
    if (data) {
      if (Array.isArray(data.payload)) {
        pwaMeta = data.payload.length > 0 && data.payload[0].pwa_meta;
      } else {
        pwaMeta = data.payload && data.payload.pwa_meta;
      }
    }

    if (pwaMeta) {
      dispatch(setPageType("articlelist", pwaMeta.site));
      Ads_module.setSectionDetail(pwaMeta);
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

ListPage.fetchNextListData = function({ dispatch, query, params, router, pagetype }) {
  return dispatch(fetchNextListDataIfNeeded(params, query, router, pagetype));
};

function mapStateToProps(state) {
  return {
    ...state.articlelist,
    header: state.header,
    astroListingWidget: state && state.articlelist && state.articlelist.astroListingWidget,
    app: state.app,
  };
}

const TopSection = props => {
  const {
    compType,
    parentSection,
    sections,
    createSectionLayout,
    astroListingWidget,
    recommendedSec,
    recommendedPhoto,
  } = props;
  const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
  const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
  const pageH1Value = `${pageheading} ${pageheadingSlug}`;
  const webstories =
    parentSection && parentSection.seolocation && parentSection.seolocation.includes("web-stories") ? "true" : "false";
  return compType != "movielist" && parentSection && parentSection.items && sections != undefined ? (
    <React.Fragment>
      {createSectionLayout({
        dataObj: parentSection,
        configlabel: webstories == "true" ? "webstories" : compType == "articlelist" ? "gridHorizontalAL" : "gallery",
        sectionId: compType == "videolist" ? parentSection.id : undefined,
        gridclasses:
          compType == "videolist" ? "wdt_top_section video" : compType == "articlelist" ? "" : "wdt_top_section",
        headingTag: "h1",
        sectionHeading: pageH1Value,
        compType,
        astroListingWidget,
      })}

      {/* video widget added for AL page */}
      {compType == "articlelist" && parentSection && parentSection.vdata ? (
        <div className="col12 pd0">
          <div className="slidercomp">
            <SectionLayoutMemo
              key="1_slider"
              datalabel="slider"
              data={parentSection.vdata}
              sliderObj={{ size: "4", width: "221" }}
            />
          </div>
        </div>
      ) : null}

      {/* most viwed widget*/}
      {parentSection &&
      parentSection.recommended &&
      parentSection.recommended.trending &&
      parentSection.recommended.trending.items &&
      Array.isArray(parentSection.recommended.trending.items) ? (
        <div className="wdt_next_sections row box-item" data-row-id="most-read" data-scroll-id="most-read">
          <ErrorBoundary>
            <SectionHeader sectionhead={siteConfig.locale.most_read} />
            <GridSectionMaker
              type={defaultDesignConfigs.horizontalSlider}
              data={parentSection.recommended.trending.items.map(item => {
                item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=most read`;
                return item;
              })}
              compType={compType}
            />
          </ErrorBoundary>
        </div>
      ) : null}
    </React.Fragment>
  ) : null;
};

{
  /* For Display photolist/Videolist/movielist level p2 */
}
const ListingData = props => {
  const { parentSection, compType, createSectionLayout, sections, astroListingWidget } = props;
  const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
  const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
  const pageH1Value = `${pageheading} ${pageheadingSlug}`;
  const webstories =
    parentSection && parentSection.seolocation && parentSection.seolocation.includes("web-stories") ? "true" : "false";

  return compType != "articlelist" && parentSection && parentSection.items && sections == undefined ? (
    <React.Fragment>
      {createSectionLayout({
        dataObj: parentSection,
        compType,
        configlabel: compType == "videolist" ? "gridLead" : compType == "movielist" ? "movieListView" : "gridView",
        gridclasses:
          compType == "videolist" ? "wdt_sections video" : compType == "movielist" ? "movielisting" : "wdt_sections",
        override: {
          noOfColumns: compType == "photolist" ? 2 : compType == "movielist" ? undefined : 1,
          imgsize:
            compType == "videolist"
              ? "largewidethumb"
              : compType == "movielist"
              ? "posterthumb"
              : webstories == "true"
              ? "webstoriesthumb"
              : "midthumb",
          // offads: true,
          noOfElements: 10,
        },
        sectionHeading: pageH1Value,
        headingTag: "h1",
        isTopSection: true,
      })}
      {
        <div className="ad-wrapper-250">
          <AdCard mstype="mrec1" adtype="dfp" />
        </div>
      }
      {createSectionLayout({
        dataObj: parentSection,
        compType,
        configlabel: compType == "videolist" ? "gridLead" : compType == "movielist" ? "movieListView" : "gridView",
        gridclasses:
          compType == "videolist"
            ? "wdt_sections video"
            : compType == "movielist"
            ? "movielisting data-after-ads"
            : "wdt_sections data-after-ads",
        override: {
          noOfColumns: compType == "photolist" ? 2 : compType == "movielist" ? undefined : 1,
          imgsize:
            compType == "videolist"
              ? "largewidethumb"
              : compType == "movielist"
              ? "posterthumb"
              : webstories == "true"
              ? "webstoriesthumb"
              : "midthumb",
          // offads: true,
          startIndex: 10,
        },
        isSectionHead: false,
      })}
      {webstories !== "true" ? (
        <div className="ad-wrapper-250">
          <AdCard mstype="mrec2" adtype="dfp" />
        </div>
      ) : null}
    </React.Fragment>
  ) : null;
};

const ArticleListSections = props => {
  const { compType, parentSection, sections, getDataList, createSectionLayout } = props;

  return compType == "articlelist" && parentSection && sections != undefined
    ? sections.map((section, index) => {
        section = getDataList(section, 10);
        return (
          <React.Fragment>
            {// Conditions to show mrec1 on diff positions if PL not present
            (parentSection.items !== "" && index == 0) || (parentSection.items === "" && index == 1) ? (
              <div className="ad-wrapper-250">
                <AdCard mstype="mrec1" adtype="dfp" />
              </div>
            ) : null}

            {createSectionLayout({
              dataObj: section,
              configlabel:
                section.tn == "articlelist"
                  ? "listGallery"
                  : section.tn == "photolist"
                  ? "gridHorizontalLeadAL"
                  : section.tn == "videolist"
                  ? "gallery"
                  : "listGallery", // here photolist/videolist means photo/video listing which are marked as special under articlelisting
              weblink: section.override || section.wu,
              gridclasses:
                section.tn == "photolist"
                  ? "photo_video_section"
                  : section.tn == "videolist"
                  ? "photo_video_section video"
                  : undefined,
            })}
            {index == 1 ? (
              <div className="ad-wrapper-250">
                <AdCard mstype="mrec2" adtype="dfp" />
              </div>
            ) : null}
            {index > 2 && index % 2 != 0 ? (
              <div className="ad-wrapper-250">
                <AdCard mstype="ctnbiglist" adtype="ctn" />
              </div>
            ) : null}
            {/* {index > 1 && index % 2 == 0 ? <AdCard mstype="ctnbiglist" adtype="ctn" /> : null} */}
          </React.Fragment>
        );
      })
    : null;
};

const SectionalData = props => {
  const { compType, parentSection, sections, createSectionLayout } = props;

  return compType != "articlelist" &&
    compType != "movielist" &&
    parentSection &&
    parentSection.items &&
    sections != undefined
    ? sections.map((section, index) => (
        <React.Fragment>
          {index == 0 ? (
            <div className="ad-wrapper-250">
              <AdCard mstype="mrec1" adtype="dfp" />
            </div>
          ) : null}
          {createSectionLayout({
            dataObj: section,
            configlabel: "gridView",
            gridclasses: "wdt_sections",
            weblink: compType == "photolist" ? section.override || section.wu : undefined,
            override: {
              noOfColumns: 2,
              noOfElements: compType == "photolist" ? 4 : 0,
            },
            compType,
          })}
          {compType == "photolist" && index == 1 ? (
            <div className="ad-wrapper-250">
              <AdCard mstype="mrec2" adtype="dfp" />
            </div>
          ) : null}
          {compType == "photolist" && index > 2 && index % 2 != 0 ? (
            <div className="ad-wrapper-250">
              <AdCard mstype="ctnbiglist" adtype="ctn" />
            </div>
          ) : null}
        </React.Fragment>
      ))
    : null;
};

const ArticleListingPage = props => {
  const { compType, parentSection, sections, createSectionLayout } = props;
  const pageheading = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheading) || "";
  const pageheadingSlug = (parentSection && parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug) || "";
  const pageH1Value = `${pageheading} ${pageheadingSlug}`;
  return (
    <React.Fragment>
      {compType == "articlelist" && parentSection && parentSection.items && sections == undefined ? (
        <React.Fragment>
          {createSectionLayout({
            dataObj: parentSection,
            configlabel: "gridHorizontalAL",
            headingTag: "h1",
            sectionHeading: pageH1Value,
            override: {
              noOfElements: 10,
            },
          })}
          {
            <div className="ad-wrapper-250">
              <AdCard mstype="mrec1" adtype="dfp" />
            </div>
          }
        </React.Fragment>
      ) : null}

      {compType == "articlelist" && parentSection && parentSection.items && sections == undefined ? (
        <React.Fragment>
          {createSectionLayout({
            dataObj: parentSection,
            configlabel: "gridHorizontalAL",
            headingTag: "h1",
            sectionHeading: pageH1Value,
            isSectionHead: false,
            override: {
              startIndex: 10,
            },
            gridclasses: "data-after-ads",
          })}
          {
            <div className="ad-wrapper-250">
              <AdCard mstype="mrec2" adtype="dfp" />
            </div>
          }
        </React.Fragment>
      ) : null}
    </React.Fragment>
  );
};

const RecommendedSec = props => {
  const { recommendedSec, createSectionLayout, compType, istrendingSec } = props;
  // console.log("recommendedSec", recommendedSec);
  return compType != "movielist"
    ? createSectionLayout({
        dataObj: recommendedSec,
        configlabel: "gridView",
        compType,
        // imgsize: compType == "videolist" ? "smallwidethumb" : "midthumb",
        gridclasses: "wdt_next_sections",
        override: {
          type: "vertical",
          className: "col12 view-horizontal",
          imgsize: compType == "videolist" ? "smallwidethumb" : "midthumb",
          istrending: istrendingSec,
        },
      })
    : null;
};

const PerpetualData = props => {
  const { value, createSectionLayout, compType } = props;

  return value.length > 1
    ? value.map((section, index) => {
        if (index > 0) {
          return (
            <React.Fragment>
              <div className="prepetual-data" key={`perpSection${index}`}>
                {createSectionLayout({
                  dataObj: section,
                  compType,
                  configlabel:
                    compType == "articlelist"
                      ? "gridHorizontalAL"
                      : compType == "movielist"
                      ? "movieListView"
                      : compType == "videolist"
                      ? "gridLead"
                      : "gridView",
                  gridclasses:
                    compType == "videolist"
                      ? "wdt_sections video"
                      : compType == "movielist"
                      ? "movielisting"
                      : compType == "articlelist"
                      ? ""
                      : "wdt_sections",
                  override: {
                    noOfColumns:
                      compType == "photolist"
                        ? 2
                        : compType == "movielist" || compType == "articlelist"
                        ? undefined
                        : 1,
                    imgsize:
                      compType == "videolist"
                        ? "largewidethumb"
                        : compType == "movielist"
                        ? "posterthumb"
                        : section && section.seolocation && section.seolocation.includes("web-stories")
                        ? "webstoriesthumb"
                        : "midthumb",
                    // offads: true,
                  },
                  key: `perpSection${index}`,
                })}
              </div>
              {
                <div className="ad-wrapper-250">
                  <AdCard mstype="ctnbiglist" adtype="ctn" />
                </div>
              }
            </React.Fragment>
          );
        }
        return null;
      })
    : null;
};

const Petrolwidget = ({ data }) =>
  data && data.pdwidget && data.pdwidget.pprice ? (
    <div className="pdwidget">
      <a href={data.pdwidget.wu}>
        <div className="pp">
          <svg width="14" height="15" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M13.38 3.525l.013-.012-3.16-3.096-.9.883 1.79 1.758A2.083 2.083 0 009.759 5c0 1.15.95 2.083 2.12 2.083.302 0 .59-.062.85-.175v6.009a.843.843 0 01-.85.833.843.843 0 01-.848-.833v-3.75c0-.921-.759-1.667-1.697-1.667h-.848V1.667C8.485.746 7.725 0 6.788 0H1.697C.759 0 0 .746 0 1.667V15h8.485V8.75h1.273v4.167c0 1.15.95 2.083 2.12 2.083C13.05 15 14 14.067 14 12.917V5c0-.575-.238-1.096-.62-1.475zM6.789 5.833H1.697V1.667h5.09v4.166zm5.09 0A.843.843 0 0111.03 5c0-.458.382-.833.849-.833.466 0 .848.375.848.833a.843.843 0 01-.848.833z"
              fill="#000"
              fillRule="nonzero"
            />
          </svg>
          <h3>{data.pdwidget.prate}</h3>
          <span>{data.pdwidget.pprice}</span>
        </div>
        <div className="pd">
          <svg width="11" height="17" xmlns="http://www.w3.org/2000/svg">
            <g fillRule="nonzero" fill="none">
              <path
                d="M7.92 14.784H2.64a.793.793 0 01-.792-.792V7.128c0-.437.355-.792.792-.792h5.28c.437 0 .792.355.792.792v6.864a.793.793 0 01-.792.792z"
                fill="#000"
              />
              <path
                d="M5.28 8.422l-1.357 2.17a1.497 1.497 0 001.268 2.287l.178.001a1.497 1.497 0 001.268-2.287L5.28 8.422zm.089 3.93h-.178a.968.968 0 01-.82-1.48l.909-1.454.909 1.455a.968.968 0 01-.82 1.48z"
                fill="#FFF"
              />
              <path
                d="M9.53 5.49L6.336 3.292V2.112h-.528V.792A.793.793 0 005.016 0H1.32a.793.793 0 00-.792.792v1.32H0v13.2c0 .582.474 1.056 1.056 1.056h8.448c.582 0 1.056-.474 1.056-1.056V7.447c0-.783-.385-1.515-1.03-1.958zM1.056.791c0-.145.119-.264.264-.264h.528v1.056h.528V.528h.528v1.056h.528V.528h.528v1.056h.528V.528h.528c.145 0 .264.119.264.264v1.32H1.056V.792zm8.976 14.52a.529.529 0 01-.528.528H1.056a.529.529 0 01-.528-.528V3.696h4.224v-.528H.528V2.64h5.28v.528H5.28v.528h.71L9.23 5.924c.502.345.801.914.801 1.523v7.865z"
                fill="#000"
              />
            </g>
          </svg>
          <h3>{data.pdwidget.drate}</h3>
          <span>{data.pdwidget.dprice}</span>
        </div>
        <div className="checplist">
          <span className="iocl">{data.pdwidget.io}</span>
          <span className="tdate">{data.pdwidget.date}</span>
          <div className="cp">{data.pdwidget.cp}</div>
        </div>
      </a>
    </div>
  ) : null;

const SuperHitWidget = props => {
  const { compType, parentSection, recommendedSec, recommendedPhoto } = props;
  return compType == "articlelist" || compType == "movielist" ? (
    <React.Fragment>
      {recommendedSec && Array.isArray(recommendedSec.items) ? (
        <div
          className="wdt_next_sections row box-item"
          data-exclude="amp"
          data-row-id="latest-video"
          data-scroll-id="latest-video"
        >
          <ErrorBoundary>
            <SectionHeader sectionhead={siteConfig.locale.latest_video} weblink={recommendedSec && recommendedSec.wu} />
            <GridSectionMaker
              type={defaultDesignConfigs.horizontalSlider}
              data={recommendedSec.items.map(item => {
                item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=latest videos`;
                return item;
              })}
            />
          </ErrorBoundary>
        </div>
      ) : null}
      {recommendedPhoto && Array.isArray(recommendedPhoto.items) ? (
        <div
          className="wdt_next_sections row box-item"
          data-exclude="amp"
          data-row-id="latest-photo"
          data-scroll-id="latest-photo"
        >
          <ErrorBoundary>
            <SectionHeader
              sectionhead={siteConfig.locale.latest_photo}
              weblink={recommendedPhoto && recommendedPhoto.wu}
            />
            <GridSectionMaker
              type={defaultDesignConfigs.horizontalSlider}
              data={recommendedPhoto.items.map(item => {
                item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=latest photos`;
                return item;
              })}
            />
          </ErrorBoundary>
        </div>
      ) : null}
      {parentSection &&
      parentSection.recommended &&
      parentSection.recommended.trending &&
      parentSection.recommended.trending.items &&
      Array.isArray(parentSection.recommended.trending.items) ? (
        <div className="wdt_next_sections row box-item" data-row-id="most-read" data-scroll-id="most-read">
          <ErrorBoundary>
            <SectionHeader sectionhead={siteConfig.locale.most_read} />

            <GridSectionMaker
              type={defaultDesignConfigs.horizontalSlider}
              data={parentSection.recommended.trending.items.map(item => {
                item.wu = `${item.wu}?utm_source=AL Widgets&utm_medium=most read`;
                return item;
              })}
            />
          </ErrorBoundary>
        </div>
      ) : null}
    </React.Fragment>
  ) : null;
};

export default connect(mapStateToProps)(ListPage);
