import React, { Component } from "react";
import { connect } from "react-redux";
import Loadable from "react-loadable";
import {
  fetchTopListDataIfNeeded,
  fetchWidgetDataIfNeeded,
  fetchWidgetRelatedVideoDataIfNeeded,
  fetchPollDataIfNeeded,
} from "../../../actions/home/home";
import { fetchWeatherDataIfNeeded } from "../../../actions/app/app";

import FakeListing from "../../../components/common/FakeCards/FakeListing";
import NewsListCard from "../../../components/common/NewsListCard";
import ListHorizontalCard from "../../../components/common/ListHorizontalCard";
import ListSlideshowCard from "../../../components/common/ListSlideshowCard";
import Listing from "../../../components/common/Listing";
import AdCard from "../../../components/common/AdCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import LiveBlogStrip from "../../../modules/liveblogstrip/LiveBlogStrip";
import PosterCard from "../../../components/common/PosterCard";
import WeatherCard from "../../../components/common/WeatherCard";
import PollCard from "../../../components/common/PollCard";

import { PageMeta } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import { _isCSR, LoadingComponent, loadJS } from "../../../utils/util";
import ServiceDrawer from "../../../components/common/ServiceDrawer";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

import { setPageType } from "../../../actions/config/config";
import { setSectionDetail, recreateAds } from "../../../components/lib/ads";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";

const ScoreCard = Loadable({
  loader: () => import("../../../modules/scorecard/ScoreCard"),
  LoadingComponent,
});

export class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //miniScorecardInterval: "",
      loadServiceDrawer: true,
    };
  }

  componentDidMount() {
    let { dispatch, query, params, router } = this.props;
    Index.fetchData({ dispatch, query, params }).then(data => {
      let _data = data && data.payload ? data.payload : {};
      if (_data && _data.pwa_meta && _data.pwa_meta.ibeat) {
        //fire ibeat
        setIbeatConfigurations(_data.pwa_meta.ibeat);
      }
    });

    let _this = this;
    let homepagewidgets = _this.props.home.value && _this.props.home.value[1] ? _this.props.home.value[1] : [];
    let homepagepollwidget = homepagewidgets.filter(widget => widget._type == "poll");
    homepagewidgets = homepagewidgets.filter(
      widget =>
        !(
          widget._type == "banner" ||
          widget._type == "servicedrawer" ||
          widget.type == "schedule" ||
          widget._type == "weather" ||
          widget._type == "poll"
        ),
    );
    //set home page ads
    setSectionDetail({ subsectitle1: "home" });
    //setSectionDetail('home', '');

    for (let i = 0; i < homepagewidgets.length; i++) {
      if (
        (homepagewidgets[i]._sec_id && homepagewidgets[i]._type) ||
        homepagewidgets[i]._type == "viraladda" ||
        homepagewidgets[i]._type == "recipes" ||
        homepagewidgets[i]._type == "astro"
      ) {
        homepagewidgets[i]._rlvideoid = homepagewidgets[i]._rlvideoid ? homepagewidgets[i]._rlvideoid : "";
        homepagewidgets[i]._sec_id = homepagewidgets[i]._sec_id ? homepagewidgets[i]._sec_id : "";
        params = { ...params, ...homepagewidgets[i] };
        //console.log("Intersection" + i);
        dispatch(fetchWidgetDataIfNeeded(params)).then(data => {
          if (data && data.payload && data.payload._rlvideoid) {
            if ("IntersectionObserver" in window) {
              //console.log("IntersectionTest" + i);
              _this.rlvideoEleObserver = new IntersectionObserver(function (entries, observer) {
                entries.forEach(function (entry) {
                  if (entry.isIntersecting && !entry.target.querySelector(".nbt-horizontalView")) {
                    let tthis = entry.target;
                    //console.log("IntersectionTestBaad" + i);
                    _this.rlvideoEleObserver.unobserve(tthis);
                    dispatch(fetchWidgetRelatedVideoDataIfNeeded(data.payload._rlvideoid)).then(data => { });
                  }
                });
              });
              var elem = document.querySelectorAll('[data-rlvideoid="' + data.payload._rlvideoid + '"]');
              elem.forEach(function (elem) {
                _this.rlvideoEleObserver.observe(elem);
              });
            } else {
              // fall back
              //console.log('!IntersectionObserver');
            }
          }
        });
      }
    }

    //dispatch(fetchArticleListDataIfNeeded(params));

    // dispatch(fetchMiniScorecardDataIfNeeded(params));
    // _this.state.miniScorecardInterval = setInterval(() => {
    //   dispatch(fetchMiniScorecardDataIfNeeded(params));
    // }, 30000);

    //Load Weather widget when needed
    _this.loadWeatherWidget();
    _this.loadServiceDrawer();
    if (homepagepollwidget && homepagepollwidget.length > 0) {
      _this.loadPollWidget(homepagepollwidget[0]);
    }

    if (typeof document != undefined) {
      document.documentElement.scrollTop = 0;
    }

    dispatch(setPageType("home"));

    //Collect trk elm , and check if its already preRendered from Html.js
    // recreat in case of csr only | we are appending trkcsr class in case of csr only
    let trkElm = document.querySelector(".trkcsr");
    trkElm ? recreateAds(["trk"]) : null;
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    const _this = this;
    if (_isCSR()) {
      window.scrollTo(0, 0);
      try {
        //clearInterval(_this.state.miniScorecardInterval);
        setSectionDetail();
      } catch (ex) { }
    }
    dispatch(setPageType(""));
  }

  loadPollWidget(pollparams) {
    let _this = this;
    let { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
          if (entry.isIntersecting) {
            let tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); //Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    } else {
      // fall back
      dispatch(fetchPollDataIfNeeded(pollparams)); //Load Poll Widget
    }
  }

  loadWeatherWidget() {
    let _this = this;
    let { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.weatherElemObserver = new IntersectionObserver(
        function (entries, observer) {
          entries.forEach(function (entry) {
            if (entry.isIntersecting) {
              let tthis = entry.target;
              let params = {};
              params.cityName =
                window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
              dispatch(fetchWeatherDataIfNeeded(params)); //Load Weather Widget
              _this.weatherElemObserver.unobserve(tthis);
              tthis.remove();
              //_this.weatherElemObserver1.unobserve(document.querySelector('#weatherWidget'));
            }
          });
        },
        { rootMargin: "0px 0px 100px 0px" },
      );

      document.querySelector("#lazyLoadweatherWidget")
        ? _this.weatherElemObserver.observe(document.querySelector("#lazyLoadweatherWidget"))
        : null;
    } else {
      // fall back
      //Callback After Loading GEOLOCATION API in App.js
      const geoLocationApi = siteConfig.weather.geoLocationApi;
      if (!window.geoinfo) {
        window.getGeoLocation = function () {
          let params = {};
          params.cityName =
            window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
          dispatch(fetchWeatherDataIfNeeded(params)); //Load Weather Widget
        };
      } else {
        let params = {};
        params.cityName = window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
        dispatch(fetchWeatherDataIfNeeded(params)); //Load Weather Widget
      }
    }
  }

  loadServiceDrawer() {
    let _this = this;
    let { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.serviceDrawerElemObserver = new IntersectionObserver(
        function (entries, observer) {
          entries.forEach(function (entry) {
            if (entry.isIntersecting) {
              let tthis = entry.target;
              _this.state.loadServiceDrawer = true;
              _this.serviceDrawerElemObserver.unobserve(tthis);
              tthis.remove();
            }
          });
        },
        { rootMargin: "0px 0px 100px 0px" },
      );

      document.querySelector("#lazyLoadServiceDrawer")
        ? _this.serviceDrawerElemObserver.observe(document.querySelector("#lazyLoadServiceDrawer"))
        : null;
    } else {
      // fall back
      _this.state.loadServiceDrawer = true;
    }
  }

  render() {
    let pwa_meta = this.props.home && this.props.home.value[0] ? this.props.home.value[0].pwa_meta : "";
    let { weather, poll } = this.props.home;
    let { budget } = this.props.app;
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    // let { weather, poll, isFetching } = this.props.home;

    return (
      //!isFetching ?
      <div className="body-content">
        {/* TRK ad code for tracking purpose only requirment by Namita/Sandeep */}
        {process.env.SITE != "eisamay" ? (
          // <AdCard mstype="trk" adtype="trk" />
          <div>
            <div
              className={`prerender trk emptyAdBox ${_isCSR() ? "trkcsr" : ""}`}
              data-id="div-gpt-ad-1572861136504-trk"
              id="div-gpt-ad-1572861136504-trk"
              data-path={siteConfig.ads.dfpads.home.trk.name}
              data-name={siteConfig.ads.dfpads.home.trk.name}
              //data-mlb="[[1, 1]]"
              data-size="[[1, 1]]"
            />
          </div>
        ) : null}

        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}

        <div className="top-news-content">
          <ul className="nbt-list">
            {this.props.home.value[0] && this.props.home.value[0].items && this.props.home.value[0].items.length > 0 ? (
              this.props.home.value[0].items.map((item, index) => {
                if (item.tn == "ad" || item.mstype == "ad") {
                  return (
                    <ErrorBoundary key={index + "ad"}>
                      {<AdCard ctnstyle="inline" mstype={item.mstype} adtype={item.type} elemtype="li" />}
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary key={"miniscorecard_" + index}>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard key={"miniscorecard_" + index} match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                            ""
                          )
                      ) : null}
                      {
                        // <ErrorBoundary key={"minischedule_" + index}><MiniSchedule key={"minischedule_" + index} schedule={this.props.home.mini_schedule} /></ErrorBoundary>
                      }
                    </ErrorBoundary>
                  );
                } else if (
                  item.tn == "news" ||
                  item.tn == "lb" ||
                  item.tn == "moviereview" ||
                  item.tn == "movieshow" ||
                  item.tn == "video" ||
                  item.tn == "poll" ||
                  item.tn == "photo"
                ) {
                  return (
                    // Index is appended with id in Key to resolve issues if story is rearranged . Because sometimes it
                    // is noticed that if a story other than top comes at top it appears with small image which should not be
                    <ErrorBoundary key={item.id + index}>
                      {index == 2 && siteConfig.livebloghpfeed ? (
                        <li className="liveblog-content">
                          <LiveBlogStrip />
                        </li>
                      ) : null}
                      <NewsListCard
                        noLazyLoad={index == 0}
                        item={item}
                        leadpost={index == 0 ? true : false}
                        key={item.id + index}
                        pagetype="home"
                      />
                      {index == 2 ? (
                        // this.props.app.mini_scorecard &&
                        // this.props.app.mini_scorecard.Calendar ? (
                        true ? (
                          <ErrorBoundary key={"miniscorecard_" + index}>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard key={"miniscorecard_" + index} match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                            ""
                          )
                      ) : null}
                    </ErrorBoundary>
                  );
                } else if (item.tn == "photolist" || item.tn == "videolist") {
                  return (
                    <ErrorBoundary key={item.id + index}>
                      {index == 2 && siteConfig.livebloghpfeed ? (
                        <li className="liveblog-content">
                          <LiveBlogStrip />
                        </li>
                      ) : null}
                      <ListHorizontalCard item={item} key={item.id + index} />
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                            ""
                          )
                      ) : null}
                    </ErrorBoundary>
                  );
                } else if (item.tn == "slideshow" || item.tn == "photostrip") {
                  return (
                    <ErrorBoundary key={item.id + index}>
                      {index == 2 && siteConfig.livebloghpfeed ? (
                        <li className="liveblog-content">
                          <LiveBlogStrip />
                        </li>
                      ) : null}
                      {/*index == 6 && (process.env.SITE == "nbt" || process.env.SITE == "mt" || process.env.SITE == "tml") ?  
                            <ErrorBoundary key={"minischedulecricplay_" + index}>
                            <div className="cricplay_schedule scroll_content"><MiniSchedule_cricplay key={"minischedulecricplay_" + index} /></div>
                      </ErrorBoundary> : null*/}

                      <ListSlideshowCard item={item} type={item.tn} key={item.id + index} />
                      {index == 2 ? (
                        this.props.app.mini_scorecard && this.props.app.mini_scorecard.Calendar ? (
                          <ErrorBoundary>
                            <li>
                              <ScoreCard />
                            </li>
                            {/* <MiniScorecard match={this.props.app.mini_scorecard} /> */}
                          </ErrorBoundary>
                        ) : (
                            ""
                          )
                      ) : null}
                    </ErrorBoundary>
                  );
                }
              })
            ) : (
                <FakeListing showImages={true} />
              )}
          </ul>
        </div>

        {/* Subscription ad */ siteConfig.wapCode != "vk" ? <AdCard mstype="subs" adtype="dfp" /> : null}

        {this.props.home.value[1] ? (
          this.props.home.value[1].map((section, index) => {
            return (
              <ErrorBoundary key={`homelist_${index}`}>
                {index > 0 && index % 2 == 1 ? (
                  <AdCard key={`homelistad_${index}`} mstype="ctnbighome" adtype="ctn" pagetype="home" />
                ) : null}
                {section._type == "banner" ? (
                  <PosterCard section={section} key={section.id} />
                ) : section._type == "servicedrawer" ? (
                  <React.Fragment>
                    <div id="lazyLoadServiceDrawer" />
                    {this.state.loadServiceDrawer ? (
                      <ErrorBoundary key="weather">
                        <ServiceDrawer />
                      </ErrorBoundary>
                    ) : (
                        ""
                      )}
                  </React.Fragment>
                ) : section._type == "schedule" ? (
                  <div data-exclude="amp">
                    <div id="lazyLoadSchedule" />
                    {this.props.home.mini_schedule ? (
                      <ErrorBoundary key="schedule">
                        <div className="mini_schedule scroll_content">
                          <MiniSchedule dispatch={this.props.dispatch} schedule={this.props.home.mini_schedule} />
                        </div>
                      </ErrorBoundary>
                    ) : null}
                  </div>
                ) : section._type == "poll" ? (
                  <div data-exclude="amp">
                    <div id="lazyLoadpollWidget" />
                    {poll ? (
                      <ErrorBoundary key="poll">
                        <div className="box-content">
                          <h2>
                            <span>{section.secname}</span>
                          </h2>
                          <PollCard dispatch={this.props.dispatch} data={poll} widget="true" />
                        </div>
                      </ErrorBoundary>
                    ) : null}
                  </div>
                ) : section._type == "weather" ? (
                  <React.Fragment>
                    <div id="lazyLoadweatherWidget" data-exclude="amp" />
                    {weather && weather.weatherData ? (
                      <ErrorBoundary key="weather">
                        <WeatherCard dispatch={this.props.dispatch} weather={weather} />
                      </ErrorBoundary>
                    ) : null}
                  </React.Fragment>
                ) : section.msid || section.id ? (
                  <Listing
                    amp="no"
                    section={section}
                    widgetSubType={section.tn == "videolist" ? "homepageVideo" : ""}
                    widgetType={
                      section.tn == "photolist"
                        ? "photolist"
                        : section.tn == "videolist"
                          ? "videolist"
                          : section.tn == "articlelist"
                            ? "articlelist"
                            : ""
                    }
                    pagetype="home"
                  />
                ) : null}
                {/** Amazon Widget */}
                {/* {index == 0 ? <div className="box-content"><ul className="nbt-list"><li className="nbt-listview"><AmazonWidget pagename={'home'} /></li></ul></div> : null} */}
                {/* Paytm Widget */}
                {/* {index == 3 ? <div className="box-content"><ul className="nbt-list"><li className="nbt-listview"><PaytmWidget pagename={'home'} /></li></ul></div> : null} */}
              </ErrorBoundary>
            );
          })
        ) : (
            <FakeListing showImages={true} />
          )}
      </div>
    );
  }
}

// HP election widget
// Index.fetchData = function ({ dispatch, query, params }) {
//   //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
//   dispatch(setPageType('home'));
//   return dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(query, params, 'result')).then(() => dispatch(fetchTopListDataIfNeeded(params, query)));
// };

Index.fetchData = function ({ dispatch, query, params, router }) {
  //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
  dispatch(setPageType("home"));
  return dispatch(fetchTopListDataIfNeeded(params, query, router)).then(data => {
    // return fetchDesktopHeaderPromise(dispatch);
  });
};

Index.propTypes = {};

function mapStateToProps(state) {
  return {
    home: state.home,
    header: state.header,
    app: state.app,
  };
}

export default connect(mapStateToProps)(Index);
