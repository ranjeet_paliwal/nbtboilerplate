import React, { Component } from "react";
import { connect } from "react-redux";
import { setPageType } from "../../../actions/config/config";

class Webview extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.config = {};
  }
  componentDidMount() {
    const { dispatch, params, router } = this.props;
    let srcVal;

    if (router && router.params && router.params.url) {
      srcVal = router.params.url;
    } else if (router && router.location && router.location.query.url != "") {
      srcVal = router.location.query.url;
    }

    if (srcVal && srcVal.indexOf("https") == -1) {
      srcVal = "//" + srcVal.replace(/-/g, "/");
    }
    if (srcVal && srcVal.indexOf("frmapp=yes") < 0) {
      srcVal += srcVal.indexOf("?") > -1 ? "&" : "?";
      srcVal += "frmapp=yes";
    }

    document.getElementById("webviewiframe").src = srcVal;

    dispatch(setPageType("webviewpage"));
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(setPageType(""));
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      //let srcVal = nextProps.location.query && nextProps.location.query ? nextProps.location.query.URL : this.props.location.query.URL;
      let srcVal;
      document.getElementById("webviewiframe").src = "";
      if (router && router.params && router.params.url) {
        srcVal = router.params.url;
      } else if (router && router.location && router.location.query.url != "") {
        srcVal = router.location.query.url;
      }

      if (srcVal && srcVal.indexOf("https") == -1) {
        srcVal = "//" + srcVal.replace(/-/g, "/");
      }

      // if (srcVal.indexOf('frmapp=yes') < 0) {
      //     srcVal += (srcVal.indexOf('?') > -1 ? "&" : "?");
      //     srcVal += "frmapp=yes";
      // }

      document.getElementById("webviewiframe").src = srcVal;
    }
  }

  render() {
    const { dispatch, params, router } = this.props;

    return (
      <div className="naviagtionDiv">
        <iframe src="" id="webviewiframe" frameBorder="0" style={{ width: "100%", height: "calc(100vh - 100px)" }} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.articleshow,
    electionresult: state.electionresult,
  };
}

export default connect(mapStateToProps)(Webview);
