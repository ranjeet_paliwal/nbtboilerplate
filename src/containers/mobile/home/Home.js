import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

import {
  fetchTopListDataIfNeeded,
  fetchWidgetDataIfNeeded,
  fetchWidgetRelatedVideoDataIfNeeded,
  fetchPollDataIfNeeded,
  makeDataChangedFalse,
  fetchHomeCityWidgetDataIfNeeded,
} from "../../../actions/home/home";
import { fetchIplCapDataIfNeeded, fetchWeatherDataIfNeeded } from "../../../actions/app/app";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ListHorizontalCard from "../../../components/common/ListHorizontalCard";
import ListSlideshowCard from "../../../components/common/ListSlideshowCard";
import AdCard from "../../../components/common/AdCard";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import PosterCard from "../../../components/common/PosterCard";
import WeatherCard from "../../../components/common/WeatherCard";
import PollCard from "../../../components/common/PollCard";
import { PageMeta } from "../../../components/common/PageMeta"; // For Page SEO/Head Part
import ServiceDrawer from "../../../components/common/ServiceDrawer";
import YSubscribeCard from "../../../components/common/YSubscribeCard";

import {
  _isCSR,
  _getStaticConfig,
  _isFrmApp,
  generateUrl,
  LoadingComponent,
  shouldTPRender,
  createAndCacheObserver,
  addObserverToWidgets,
  fetchHomeCityWidgetData,
  getCountryCode,
  isInternationalUrl,
  checkIsAmpPage,
} from "../../../utils/util";

import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import { setPageType, setParentId, setRegion, resetRegion } from "../../../actions/config/config";
import { setSectionDetail, recreateAds, refreshAds } from "../../../components/lib/ads";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import ScoreCard from "../../../modules/scorecard/ScoreCard";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import SectionHeader from "../../../components/common/SectionHeader/SectionHeader";
import GridCardMaker from "../../../components/common/ListingCards/GridCardMaker";
import KeyWordCard from "../../../components/common/KeyWordCard";
import AstroWidget from "../../../components/common/AstroWidget/AstroWidget";
import MiniTVCard from "../../../components/common/MiniTVCard";
import Slider from "../../../components/desktop/Slider/index";
import "../../../components/common/css/desktop/SectionWrapper.scss";
import "../../../components/common/css/SectionWrapper.scss";
import FakeHorizontalListCard from "../../../components/common/FakeCards/FakeHorizontalListCard";
import Banner from "../../../components/common/Banner/Banner";
import { clearFeedInterval, setRefreshFeedInterval } from "../../utils/home_util";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import TPwidget from "../../../components/common/TimesPoints/TPWidgets/TPWidgets";
import GoldTopNewsWidget from "../../../components/common/Gold/GoldTopNewsWidget";
import GoldPodCastIframe from "../../../components/common/Gold/GoldPodCastIframe";
import Loadable from "react-loadable";
import BudgetWidget from "../../../components/common/BudgetWidget/BudgetWidget";
import { fireGRXEvent } from "../../../components/lib/analytics/src/ga";
import { retry } from "async";
import HomeList from "../../../components/common/HomeList";
import BriefWidget from "../../../components/common/BriefWidget";
import CapCard from "../../../campaign/cricket/components/CapCard";
import { MediaWireServiceRequest } from "../../../utils/serviceUtil";
import { internationalconfig } from "./../../../utils/internationalpageConfig";
const siteConfig = _getStaticConfig();
const siteName = process.env.SITE;
const intPages = internationalconfig && internationalconfig.routes;

const NewsLetter = Loadable({
  loader: () => import("../../../components/common/Newsletter"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("../../../components/common/Newsletter"),
});
const PointsTableCard = Loadable({
  loader: () => import("../../../campaign/cricket/components/PointsTableCard"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("../../../campaign/cricket/components/PointsTableCard"),
});

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // miniScorecardInterval: "",
      loadServiceDrawer: true,
      mediaWireData: "",
      hasMounted: false,
      fetchCitySection: true,
    };
  }

  componentDidMount() {
    let { dispatch, query, params, home, router } = this.props;
    this.setState({ hasMounted: true });
    if (typeof document && typeof window) {
      // redirection on the basis of geo location cookie
      // const countryCode = getCountryCode();
      // if (countryCode) {
      //   if (router && router.location && router.location.pathname === "/") {
      //     window.history.replaceState({}, "", internationalconfig[siteName].internationalPagesUrl[countryCode]);
      //   }
      // }
    }
    //moved setSectionDetail here to trigger it asap
    setSectionDetail({ subsectitle1: "home", adsec: "homepage" });
    Home.fetchData({ dispatch, query, params, router }).then(data => {
      let _data = data && data.payload ? data.payload : {};
      if (_data && _data.pwa_meta && _data.pwa_meta.ibeat) {
        //fire ibeat
        setIbeatConfigurations(_data.pwa_meta.ibeat);
      }
    });

    const _this = this;
    let homepagewidgets =
      // siteConfig.homeWidgetsList;
      _this.props.home.value && _this.props.home.value[1] ? _this.props.home.value[1] : [];
    const homepagepollwidget = homepagewidgets.filter(widget => widget._type == "poll");
    homepagewidgets =
      homepagewidgets &&
      homepagewidgets.filter(widget => {
        if (widget._platform && widget._platform !== process.env.PLATFORM) {
          return false;
        }
        return !(
          widget._type == "banner" ||
          widget._type == "servicedrawer" ||
          widget.type == "schedule" ||
          widget._type == "weather" ||
          widget._type == "poll"
        );
      });
    // set home page ads

    // setSectionDetail('home', '');

    for (let i = 0; i < homepagewidgets.length; i++) {
      //fetch city data based on geolocation and cityid if present in cookie
      if (homepagewidgets[i].label == "city" && process.env.SITE != "tlg") {
        continue;
      }
      if (
        (homepagewidgets[i]._sec_id && homepagewidgets[i]._type) ||
        homepagewidgets[i]._type == "viraladda" ||
        homepagewidgets[i]._type == "recipes" ||
        homepagewidgets[i]._type == "astro"
      ) {
        homepagewidgets[i]._rlvideoid = homepagewidgets[i]._rlvideoid ? homepagewidgets[i]._rlvideoid : "";
        homepagewidgets[i]._sec_id = homepagewidgets[i]._sec_id ? homepagewidgets[i]._sec_id : "";
        const _params = { ...params, ...homepagewidgets[i] };
        // console.log("Intersection" + i);
        dispatch(fetchWidgetDataIfNeeded(_params)).then(data => {
          if (data && data.payload) {
            if (data && data.payload && data.payload.params && data.payload.params.label) {
              if (data.payload.params.label) {
                const selector = `.row.${data.payload.label || data.payload.params.label}`;
              }
            }

            if ("IntersectionObserver" in window) {
              // console.log("IntersectionTest" + i);
              if (data.payload._rlvideoid) {
                _this.rlvideoEleObserver = new IntersectionObserver((entries, observer) => {
                  entries.forEach(entry => {
                    if (entry.isIntersecting && !entry.target.querySelector(".nbt-horizontalView")) {
                      const tthis = entry.target;
                      // console.log("IntersectionTestBaad" + i);
                      _this.rlvideoEleObserver.unobserve(tthis);
                      dispatch(fetchWidgetRelatedVideoDataIfNeeded(data.payload.params)).then(data => {});
                    }
                  });
                });
                const elem = document.querySelectorAll(`[data-rlvideoid="${data.payload._rlvideoid}"]`);
                elem.forEach(elem => {
                  _this.rlvideoEleObserver.observe(elem);
                });
              }

              if (homepagewidgets[homepagewidgets.length - 1]._sec_id == data.payload.id) {
                // Trackers for Widgets in Home
                // homeWidgetsTracker();
              }
            } else {
              // fall back
              // console.log('!IntersectionObserver');
            }
          }
        });
      }
    }

    const config = {
      root: null,
      rootMargin: "0px",
      threshold: 0.2, // when 20% in view
    };

    const callback = (entries, self) => {
      entries.forEach(entry => {
        if (entry && entry.isIntersecting) {
          try {
            const rowId = entry.target.getAttribute("data-row-number");
            const widgetId = entry.target.getAttribute("data-scroll-id");
            fireGRXEvent("track", "scroll_depth", { widget_id: widgetId || "NA", row_id: rowId || "NA" });

            self.unobserve(entry.target);
          } catch (e) {
            console.log("error while observing home widget", e);
          }
        }
      });
    };

    const { app } = this.props;

    const shouldCreateScrollDepth =
      (app &&
        app.scrolldepth &&
        app.scrolldepth[process.env.SITE] &&
        app.scrolldepth[process.env.SITE].home &&
        app.scrolldepth[process.env.SITE].home.mobile === "true") ||
      false;

    // Creates an observer and adds it to the current context
    createAndCacheObserver(this, config, callback, { shouldCreate: shouldCreateScrollDepth });
    //  Add selectors to observer
    this.addSelectorsToObserver();

    // dispatch(fetchArticleListDataIfNeeded(params));

    // dispatch(fetchMiniScorecardDataIfNeeded(params));
    // _this.state.miniScorecardInterval = setInterval(() => {
    //   dispatch(fetchMiniScorecardDataIfNeeded(params));
    // }, 30000);

    // Load Weather widget when needed
    _this.loadWeatherWidget();
    _this.loadServiceDrawer();
    if (homepagepollwidget && homepagepollwidget.length > 0) {
      _this.loadPollWidget(homepagepollwidget[0]);
    }

    let geoLocation = JSON.parse(_getCookie("geo_data"));
    if (_isCSR()) {
      let PLData =
        this.props.home && this.props.home.value && this.props.home.value[0] && this.props.home.value[0].items;
      let defaultData = PLData[PLData.length - 1];

      let options = {
        hostid: siteConfig.hostid,
        platform: 1,
        app_platform: "MWEB",
        secid: siteConfig.channelid,
        sltno: siteConfig.mediawireslot,
        geocl: geoLocation && geoLocation.city,
        geostate: geoLocation && geoLocation.region_code,
        geolocation: geoLocation && geoLocation.CountryCode,
        geocontinent: geoLocation && geoLocation.Continent,
        defaultData: defaultData,
      };

      MediaWireServiceRequest(options).then(value => {
        this.setState({ mediaWireData: value });
      });
    }

    if (typeof document !== undefined) {
      document.documentElement.scrollTop = 0;
    }

    dispatch(setPageType("home"));
    dispatch(setParentId("", ""));
    // if (intPages && intPages != "/") {
    dispatch(setRegion(router && router.location.pathname));
    // }
    // dispatch(setRegion(""));
    // Collect trk elm , and check if its already preRendered from Html.js
    // recreat in case of csr only | we are appending trkcsr class in case of csr only
    const trkElm = document.querySelector(".trkcsr");
    trkElm ? recreateAds(["trk"]) : null;
    setRefreshFeedInterval(120, _this.props.home.refreshFeedInterval, dispatch, "", _this.props.router);

    /* By default, invokes the handler whenever:
       1. Any part of the target enters the viewport
       2. The last part of the target leaves the viewport */
    setTimeout(() => {
      if ("IntersectionObserver" in window) {
        const observerData = new IntersectionObserver(_this.updateIframewithSrc);
        if (document.querySelector(".lang-frame")) {
          observerData.observe(document.querySelector(".lang-frame"));
        }
      }
    }, 3000);
    dispatch(fetchIplCapDataIfNeeded());
  }

  addSelectorsToObserver() {
    try {
      if (_isCSR()) {
        const toBeTrackedRows = Array.prototype.slice.call(document.querySelectorAll("#home_widget_container > div"));
        const rowsSelector = { '[data-row-id="photodhamaal"]': true, '[data-row-id="most-read"]': true };
        toBeTrackedRows.forEach(row => {
          rowsSelector[`[data-row-id=${row.getAttribute("data-row-id")}]`] = true;
        });
        const { app } = this.props;

        const shouldCreateScrollDepth =
          (app &&
            app.scrolldepth &&
            app.scrolldepth[process.env.SITE] &&
            app.scrolldepth[process.env.SITE].home &&
            app.scrolldepth[process.env.SITE].home.mobile === "true") ||
          false;
        addObserverToWidgets(this, rowsSelector, { shouldCreate: shouldCreateScrollDepth });
      }
    } catch (e) {
      console.log("Error creating selectors", e);
    }
  }

  updateIframewithSrc(entries, observer) {
    entries.forEach(entry => {
      if (entry.isIntersecting && !entry.target.getAttribute("src")) {
        const dataSrc = entry.target.getAttribute("data-src");
        entry.target.setAttribute("src", dataSrc);
      }
    });
  }

  fetchCityData(alaskaData) {
    const { dispatch } = this.props;
    let sectionId = siteConfig.pages.stateSection;
    sectionId = sectionId && sectionId.split(",")[0];
    // let pagetype = getPageType(location.pathname);
    let homepagewidgets = this.props.home.value && this.props.home.value[1] ? this.props.home.value[1] : [];

    const homePageCityParams = homepagewidgets && homepagewidgets.filter(widget => widget._sec_id == sectionId);

    let geoData = JSON.parse(_getCookie("geo_data"));
    let selectedCityId = _getCookie("selected_cityId");
    this.setState({ fetchCitySection: false });

    let params;
    //  check user previouslly clicked on city or not then geo location
    if (process.env.SITE != "tlg") {
      if (selectedCityId && selectedCityId != "" && selectedCityId != undefined) {
        params = fetchHomeCityWidgetData(alaskaData, selectedCityId, sectionId, homePageCityParams[0], dispatch);
        dispatch(fetchHomeCityWidgetDataIfNeeded(params, "home"));
      } else if (geoData && geoData.city != "" && geoData.city != undefined) {
        params = fetchHomeCityWidgetData(
          alaskaData,
          "/" + geoData.city + "/",
          sectionId,
          homePageCityParams[0],
          dispatch,
        );
        dispatch(fetchHomeCityWidgetDataIfNeeded(params, "home"));
      } else {
        let defaultparams = homePageCityParams[0];
        defaultparams.citySecId = undefined;
        dispatch(fetchHomeCityWidgetDataIfNeeded(defaultparams, "home"));
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { dispatch, location, params, router, home } = this.props;
    if (this.props.home.dataChanged) {
      this.props.dispatch(makeDataChangedFalse());
      refreshAds();
      AnalyticsGA.pageview("/", {
        forcefulGaPageview: true,
      });
    }
    if (prevProps.home && prevProps.home.value && prevProps.home.value[1] != this.props.home.value[1]) {
      this.addSelectorsToObserver();
    }
    // headline data changes on flag click
    if (
      location &&
      prevProps.location.pathname != location.pathname &&
      intPages &&
      intPages.includes(location.pathname)
    ) {
      dispatch(setRegion(location.pathname));
      dispatch(fetchTopListDataIfNeeded(params, "", router, "", location.pathname));
    }
  }

  shouldComponentUpdate(nextProps) {
    //fetch city data after alaska data is present at CSR
    if (
      _isCSR() &&
      this.state.fetchCitySection &&
      nextProps.alaskaData &&
      Object.keys(nextProps.alaskaData).length > 10
    ) {
      this.fetchCityData(nextProps.alaskaData);
    }
    return true;
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    clearFeedInterval(this.props.home.refreshFeedInterval, dispatch);
    const _this = this;
    if (_isCSR()) {
      window.scrollTo(0, 0);
      try {
        // clearInterval(_this.state.miniScorecardInterval);
        setSectionDetail();
      } catch (ex) {}
    }
    dispatch(setPageType(""));
  }

  loadPollWidget(pollparams) {
    const _this = this;
    const { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            const tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); // Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      document.querySelector("#lazyLoadpollWidget") &&
        _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    } else {
      // fall back
      dispatch(fetchPollDataIfNeeded(pollparams)); // Load Poll Widget
    }
  }

  loadWeatherWidget() {
    const _this = this;
    const { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.weatherElemObserver = new IntersectionObserver(
        (entries, observer) => {
          entries.forEach(entry => {
            if (entry.isIntersecting) {
              const tthis = entry.target;
              const params = {};
              params.cityName = siteConfig.weather.defaultCity;
              // window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
              // according to vasu defaultcity is to be filled
              dispatch(fetchWeatherDataIfNeeded(params)); // Load Weather Widget
              _this.weatherElemObserver.unobserve(tthis);
              tthis.remove();
              // _this.weatherElemObserver1.unobserve(document.querySelector('#weatherWidget'));
            }
          });
        },
        { rootMargin: "0px 0px 100px 0px" },
      );

      document.querySelector("#lazyLoadweatherWidget")
        ? _this.weatherElemObserver.observe(document.querySelector("#lazyLoadweatherWidget"))
        : null;
    } else {
      // fall back
      // Callback After Loading GEOLOCATION API in App.js
      const geoLocationApi = siteConfig.weather.geoLocationApi;
      if (!window.geoinfo) {
        window.getGeoLocation = function() {
          const params = {};
          params.cityName = siteConfig.weather.defaultCity;
          // window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
          dispatch(fetchWeatherDataIfNeeded(params)); // Load Weather Widget
        };
      } else {
        const params = {};
        params.cityName = siteConfig.weather.defaultCity;
        // params.cityName = window.geoinfo && window.geoinfo.city ? window.geoinfo.city : siteConfig.weather.defaultCity;
        dispatch(fetchWeatherDataIfNeeded(params)); // Load Weather Widget
      }
    }
  }

  loadServiceDrawer() {
    const _this = this;
    const { dispatch, params } = this.props;
    if ("IntersectionObserver" in window) {
      _this.serviceDrawerElemObserver = new IntersectionObserver(
        (entries, observer) => {
          entries.forEach(entry => {
            if (entry.isIntersecting) {
              const tthis = entry.target;
              _this.state.loadServiceDrawer = true;
              _this.serviceDrawerElemObserver.unobserve(tthis);
              tthis.remove();
            }
          });
        },
        { rootMargin: "0px 0px 100px 0px" },
      );

      document.querySelector("#lazyLoadServiceDrawer")
        ? _this.serviceDrawerElemObserver.observe(document.querySelector("#lazyLoadServiceDrawer"))
        : null;
    } else {
      // fall back
      _this.state.loadServiceDrawer = true;
    }
  }

  /* Its a generic function to create Sectional Layout in Home Page
  Provide designlabel to get required structure
  */
  createSectionLayout({ data, designlabel, sliderObj, headingObj, datalabel, className }) {
    try {
      // const _this = this;
      // let { value } = _this.props.home;
      // let dataObj = value[1];

      const { alaskaData } = this.props;
      const headerCopy = alaskaData ? JSON.parse(JSON.stringify(alaskaData)) : {}; // for copying Objects by Value

      const labelDataObj = data;
      const sectionhead = headingObj ? headingObj.secname : labelDataObj ? labelDataObj.secname : undefined;
      const weblink = data && data.override ? generateUrl(data) : "";

      // const videoClassName = () === "47344128" ? "photo_video_section video" : "";
      return labelDataObj ? (
        <div className={`row ${designlabel} ${datalabel} ${className || ""}`} key={data.msid}>
          {sectionhead && (
            <SectionHeader
              data={headerCopy}
              sectionId={labelDataObj && labelDataObj.id}
              key={labelDataObj && labelDataObj.id}
              sectionhead={sectionhead}
              weblink={weblink}
              cityStateId={labelDataObj && labelDataObj.citySecId}
            />
          )}

          {datalabel == "astro" ? (
            <div className="row mr0">
              <div className="col12">
                <ul className="astro_widget">
                  {labelDataObj.items[0] ? (
                    <ErrorBoundary>
                      <GridCardMaker card={labelDataObj.items[0]} cardType="horizontal" imgsize="smallthumb" />
                      <li className="astro_text">
                        <span className="text_ellipsis">{labelDataObj.items[0].syn}</span>
                      </li>
                    </ErrorBoundary>
                  ) : null}

                  <ErrorBoundary>
                    <AstroWidget item={labelDataObj && labelDataObj.items && labelDataObj.items[0]} />
                  </ErrorBoundary>
                </ul>
              </div>
              <div className="col12 pd0">
                <GridSectionMaker
                  key={designlabel}
                  type={defaultDesignConfigs[designlabel]}
                  data={labelDataObj ? labelDataObj.items : []}
                  override={{ startIndex: 1, offads: true, noOfElements: 5 }}
                />
              </div>
            </div>
          ) : datalabel == "webstories" ? (
            <GridSectionMaker
              type={defaultDesignConfigs[designlabel]}
              data={labelDataObj ? labelDataObj.items : []}
              override={{ offads: true }}
            />
          ) : (
            <GridSectionMaker
              // key={designlabel}
              type={defaultDesignConfigs[designlabel]}
              data={labelDataObj ? labelDataObj.items : []}
            />
          )}
          {datalabel == "ipl" ? (
            <div className="wdt_capcard">
              <CapCard type={"orange"} className="col12" />
            </div>
          ) : null}
          {datalabel == "ipl" ? (
            <div className="wdt_capcard">
              <CapCard type={"purple"} className="col12" />
            </div>
          ) : null}

          {sliderObj && sliderObj.showSlider && labelDataObj ? (
            <div className="col12">
              <div className="list-slider">
                {labelDataObj.rlvideo ? (
                  <Slider
                    type="grid"
                    // subtype="videosection"
                    size={sliderObj.size || "4"}
                    movesize="2"
                    sliderData={labelDataObj.rlvideo.items}
                    sliderClass="videosection"
                    margin="15"
                    width={sliderObj.width || "221"}
                    // height="243"
                    videoIntensive={false}
                  />
                ) : (
                  <span data-rlvideoid={labelDataObj._rlvideoid}>
                    <FakeHorizontalListCard elemSize={3} />
                  </span>
                )}
              </div>
            </div>
          ) : null}
          {datalabel == "video" ? (
            <div className="btn-subscribe">
              <YSubscribeCard />
            </div>
          ) : null}
          {datalabel == "ipl" ? (
            <div className="col12 hp_pointTable">
              <PointsTableCard ptMsid={siteConfig.iplpages.pointsTable} compType="pointtablecard" />
            </div>
          ) : null}
        </div>
      ) : (
        <FakeListing />
      );
    } catch (e) {
      console.log(e.message);
      return null;
    }
  }

  render() {
    const { pwa_meta, recommended } = this.props.home && this.props.home.value[0] ? this.props.home.value[0] : "";
    const { poll } = this.props.home;
    const { budget, minitv, adconfig } = this.props.app;
    const { config, router, error, dispatch, location, alaskaData } = this.props;
    const iframeStatus = this.props && this.props.app && this.props.app.electionframetop;

    if (error) {
      return null;
    }

    const budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    const isFrmApp = _isFrmApp(this.props.router);
    // let { weather, poll, isFetching } = this.props.home;
    // console.log("this.props.home.value[1]-------", this.props.home.value[0]);
    let totalWidgetRows = 0;
    // Removing last item from headline feed
    let homePageHeadline = JSON.parse(JSON.stringify(this.props.home.value[0] && this.props.home.value[0].items));
    // homePageHeadline.pop();
    if (homePageHeadline && Array.isArray(homePageHeadline)) {
      homePageHeadline.pop();
    }
    const pathname = router && router.location.pathname;
    const isAMPPage = checkIsAmpPage(pathname);

    return (
      //! isFetching ?
      <div className="body-content section-wrapper">
        {/* TRK ad code for tracking purpose only requirment by Namita/Sandeep */}
        <div>
          <div
            className={`prerender trk emptyAdBox ${_isCSR() ? "trkcsr" : ""}`}
            data-id="div-gpt-ad-1572861136504-trk"
            id="div-gpt-ad-1572861136504-trk"
            data-path={siteConfig.ads.dfpads.trk}
            data-name={siteConfig.ads.dfpads.trk}
            // data-mlb="[[1, 1]]"
            data-size="[[1, 1]]"
          />
        </div>

        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}

        {budgetLbConfig && budgetLbConfig.status === "true" ? <BudgetWidget /> : null}

        {/*  siteSync ad - toggled from pwa_campaign feed (MOBILE ONLY) */}
        {adconfig &&
        adconfig.sitesync &&
        adconfig.sitesync[process.env.SITE] &&
        adconfig.sitesync[process.env.SITE].active === "true" ? (
          <ErrorBoundary>
            {_isCSR() ? (
              ReactDOM.createPortal(
                <div
                  className={`prerender sitesync emptyAdBox`}
                  data-id="div-gpt-ad-1572861136504-sitesync"
                  id="div-gpt-ad-1572861136504-sitesync"
                  data-path={siteConfig.ads.dfpads.sitesync}
                  data-name={siteConfig.ads.dfpads.sitesync}
                  data-size="[[1, 1]]"
                />,
                document.getElementById("sitesync-wrapper"),
              )
            ) : (
              <div
                className={`prerender sitesync emptyAdBox`}
                data-id="div-gpt-ad-1572861136504-sitesync"
                id="div-gpt-ad-1572861136504-sitesync"
                data-path={siteConfig.ads.dfpads.sitesync}
                data-name={siteConfig.ads.dfpads.sitesync}
                data-size="[[1, 1]]"
              />
            )}
          </ErrorBoundary>
        ) : (
          ""
        )}

        {/** Adding result home page widget for mobile pwa and amp platforms */}

        <div className="top-news-content row">
          <ul className="nbt-list col12 pd0">
            {homePageHeadline && homePageHeadline.length > 0 ? (
              homePageHeadline.map((item, index) => {
                if (item.tn == "ad" || item.mstype == "ad") {
                  return (
                    <ErrorBoundary key={`${index}ad`}>
                      <AdCard
                        ctnstyle="inline"
                        mstype={item.mstype}
                        adtype={item.type}
                        elemtype="li"
                        className="news news-card horizontal"
                        rendertype={item.mstype && item.mstype === "ctnhometop" ? "prerender" : ""}
                      />
                    </ErrorBoundary>
                  );
                }
                if (item.tn == "banner") {
                  return <Banner data={item} />;
                }

                if (item.itemtype === "mediawire" && item.platform === process.env.PLATFORM) {
                  return (
                    <ErrorBoundary>
                      <div data-exclude="amp">
                        {this.state.mediaWireData && this.state.mediaWireData.tn === "ad" ? (
                          <AdCard
                            ctnstyle="inline"
                            mstype={this.state.mediaWireData.mstype}
                            adtype={this.state.mediaWireData.type}
                            elemtype="li"
                            className="news news-card horizontal"
                            rendertype={
                              this.state.mediaWireData.mstype && this.state.mediaWireData.mstype === "ctnhometop"
                                ? "prerender"
                                : ""
                            }
                          />
                        ) : (
                          <GridCardMaker
                            card={this.state.mediaWireData}
                            cardType={index < 2 ? "lead" : "horizontal"}
                            columns={index < 2 ? "6" : ""}
                            noLazyLoad={index < 2}
                            key={index}
                            keyName={index}
                            imgsize="midthumb"
                          />
                        )}
                      </div>
                    </ErrorBoundary>
                  );
                }

                if (
                  (item.tn == "news" ||
                    item.tn == "lb" ||
                    item.tn == "moviereview" ||
                    item.tn == "movieshow" ||
                    item.tn == "video" ||
                    item.tn == "poll" ||
                    item.tn == "photo") &&
                  item.platform !== "desktop" &&
                  item.platform !== "appinstall"
                ) {
                  return (
                    // Home is appended with id in Key to resolve issues if story is rearranged . Because sometimes it
                    // is noticed that if a story other than top comes at top it appears with small image which should not be
                    <ErrorBoundary key={item.id + index}>
                      {index === 3 && _isCSR() ? <ScoreCard dispatch={dispatch} pageName="home" /> : <div />}
                      <GridCardMaker
                        card={item}
                        cardType={index < 2 ? "lead" : "horizontal"}
                        columns={index < 2 ? "6" : ""}
                        noLazyLoad={index < 2}
                        key={index}
                        keyName={index}
                        imgsize="midthumb"
                      />
                      {/* MINITV */}
                      {index === 4 &&
                      minitv &&
                      // router.location.pathname.indexOf("/tech") < 0 &&
                      config.pagetype != "newsbrief" &&
                      minitv.hl &&
                      config.pagetype != "videolist" &&
                      config.pagetype != "videoshow" &&
                      !isFrmApp &&
                      config.pagetype != "webviewpage" ? (
                        <ErrorBoundary>
                          <MiniTVCard minitv={minitv} className="minitvhomepage" location={location} />
                        </ErrorBoundary>
                      ) : null}
                      {/* Trending Section */}
                      {/* index === 6 && this.props.home.value[1] && this.props.home.value[1][0] && (
                        <li className="trending_keywords" data-exclude="amp">
                          <KeyWordCard
                            secname={this.props.home.value[1][0].secname}
                            items={this.props.home.value[1][0].items}
                          />
                        </li>
                      ) */}

                      {/* <NewsListCard
                        noLazyLoad={index == 0}
                        item={item}
                        leadpost={index == 0 ? true : false}
                        key={item.id + index}
                        pagetype="home"
                      /> */}
                    </ErrorBoundary>
                  );
                }
                if (item.tn == "photolist" || item.tn == "videolist") {
                  return (
                    <ErrorBoundary key={item.id + index}>
                      <ListHorizontalCard item={item} key={item.id + index} />
                    </ErrorBoundary>
                  );
                }
                if ((item.tn == "slideshow" || item.tn == "photostrip") && siteName != "iag") {
                  return (
                    <ErrorBoundary key={item.id + index}>
                      <ListSlideshowCard item={item} type={item.tn} key={item.id + index} />
                    </ErrorBoundary>
                  );
                }
              })
            ) : (
              <FakeListing showImages />
            )}
            {typeof shouldTPRender === "function" && shouldTPRender() && (
              <ErrorBoundary>
                <TPwidget type="HOME" router={this.props.router} />
              </ErrorBoundary>
            )}

            <li className="list-slider">
              {(this.state.hasMounted || isAMPPage) &&
              recommended &&
              recommended.video &&
              recommended.video[0] &&
              recommended.video[0].items &&
              Array.isArray(recommended.video[0].items) &&
              recommended.video[0].items.length > 0 ? (
                <ErrorBoundary>
                  <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={recommended.video[0].items} />
                </ErrorBoundary>
              ) : null}
            </li>
            <li className="btn-subscribe">
              <YSubscribeCard />
            </li>
          </ul>
        </div>

        {
          /* Subscription ad */ <ErrorBoundary>
            <div key="AdvContainer">
              <AdCard mstype="subs" adtype="dfp" />
            </div>
            <div key="AdvContainerBlank"></div>
          </ErrorBoundary>
        }
        {/* US Election iframe driven from pwa campaign */}
        {(process.env.SITE === "nbt" ||
          process.env.SITE === "mt" ||
          process.env.SITE === "vk" ||
          process.env.SITE === "iag") &&
        iframeStatus &&
        iframeStatus.status === "true" ? (
          <div className="wdt_elec" data-exclude="amp">
            <iframe
              src={`${iframeStatus.url}&host=${process.env.SITE}&ismobile=1`}
              height={iframeStatus.mheight}
              width={iframeStatus.mwidth}
              border="0"
              scrolling="no"
              Frameborder="0"
              align="center"
              title="electionIframe"
            />
          </div>
        ) : null}
        {/* video iframe added for 5 days */}
        {/* <div className="vd_pl" data-exclude="amp">
          <iframe
            src={`${siteConfig.weburl}/pwafeeds/videoplayer_iframe.cms?wapCode=${siteName}`}
            height="250"
            width="300"
            border="0"
            marginWidth="0"
            marginHeight="0"
            hspace="0"
            vspace="0"
            frameBorder="0"
            scrolling="no"
            align="center"
            title=""
            allowTransparency="true"
            id="vodplayer"
          />
        </div> */}
        {/* condition for adding headlines section on homepage on international pages*/}
        {/* {isInternationalUrl(router) || getCountryCode() ? (
          <HomeList config={internationalconfig[siteName].headlineConfig} />
        ) : null} */}
        {/**
          putting a wrapper to control dynamically adding ads by ads-by-google vignette
          which breaks all UI of React hydrate
         */}
        <div id="home_widget_container" key="homeWidgetsContainer">
          <BriefWidget pagetype="home" alaskaData={alaskaData} />
          {this.props.home.value[1] ? (
            this.props.home.value[1].map((section, index) => {
              if (section.id === "72180247" || (section._platform && section._platform !== process.env.PLATFORM))
                return null; // For skipping trending section as it is already rendered
              totalWidgetRows += 1;
              if (
                ((isInternationalUrl(router) || getCountryCode()) && section.id === "2147478019") ||
                section.id === "2147477892" ||
                section.id === "2147477985" ||
                section.id === "696089404"
              ) {
                return null; // For skipping trending section as it is already rendered in case of international pages
              }
              return (
                <div
                  key={`homelist_${index}`}
                  data-row-number={`${index + 1}`}
                  // SCROLL ID IS THE IDENTIFIER USED TO FIRE GRX EVENT
                  data-scroll-id={`${(section.params && section.params["_actuallabel"]) ||
                    section.label ||
                    (section.params && section.params.label) ||
                    "NA"}`}
                  // ROW ID IS THE IDENTIFIER TO FIND THE ROW TO FIRE THE EVENT
                  // AS document.querySelector doesn't accept spaces, remove them
                  data-row-id={
                    (section.label && section.label.replace(" ", "")) ||
                    (section.params && section.params.label.replace(" ", "")) ||
                    `scroll-id-${index + 1}`
                  }
                >
                  <ErrorBoundary>
                    {index === 2 && this.state.hasMounted && <NewsLetter />}
                    {index === 10 && siteName != "iag" ? (
                      <div
                        className="row ext-photogallery"
                        data-row-number="11-photogallery"
                        data-scroll-id="photodhamaal"
                        data-row-id="photodhamaal"
                        data-exclude="amp"
                      >
                        <div className="section">
                          <div className="top_section">
                            <h2>
                              <span>
                                <a
                                  href={`https://${siteConfig.photogallerydomain}`}
                                  rel="noopener nofollow"
                                  target="_blank"
                                >
                                  {siteConfig.locale.photomasti}
                                </a>
                              </span>
                            </h2>
                          </div>
                        </div>

                        <div className="col12">
                          <iframe
                            scrolling="no"
                            frameBorder="no"
                            className="lang-frame"
                            data-src={`https://tamil.samayam.com/pwafeeds/topgalleies_tamil_pwa.cms?channel=${siteName}&platform=mobile`}
                          />
                        </div>
                      </div>
                    ) : null}
                    {index === 22 ? (
                      <div
                        className="row box-item"
                        data-row-id="most-read"
                        data-scroll-id="most-read"
                        data-row-number="23-most-read"
                      >
                        {recommended &&
                        recommended.trending &&
                        recommended.trending[0] &&
                        recommended.trending[0].items &&
                        Array.isArray(recommended.trending[0].items) &&
                        recommended.trending[0].items.length > 0 &&
                        (this.state.hasMounted || isAMPPage) ? (
                          <div>
                            <SectionHeader sectionhead={recommended.trending[0].secname} />
                            <GridSectionMaker
                              type={defaultDesignConfigs.listGallery}
                              data={recommended.trending[0].items}
                              override={{ istrending: "true" }}
                            />
                          </div>
                        ) : null}
                      </div>
                    ) : null}
                    {section._type == "banner" ? (
                      <PosterCard section={section} key={section.id} />
                    ) : section._type == "servicedrawer" ? (
                      <React.Fragment>
                        <div id="lazyLoadServiceDrawer" />
                        {this.state.loadServiceDrawer ? (
                          <ErrorBoundary key="serviceDrawer">
                            <ServiceDrawer />
                          </ErrorBoundary>
                        ) : (
                          ""
                        )}
                      </React.Fragment>
                    ) : section._type == "iframe" ? (
                      <div data-exclude="amp">
                        <ErrorBoundary key="iframe">
                          <GoldPodCastIframe />
                        </ErrorBoundary>
                      </div>
                    ) : section._type == "eisgold" || section._type == "nbtgold" ? (
                      <div data-exclude="amp">
                        <ErrorBoundary key={section._type}>
                          <GoldTopNewsWidget />
                        </ErrorBoundary>
                      </div>
                    ) : section._type == "poll" ? (
                      <div data-exclude="amp" className="wdt_poll">
                        <div id="lazyLoadpollWidget" />
                        {poll ? (
                          <ErrorBoundary key="poll">
                            <div className="box-content">
                              <SectionHeader
                                // weblink={siteConfig.poll.link}
                                sectionhead={section.label}
                              />
                              <PollCard dispatch={this.props.dispatch} data={poll} widget="true" />
                            </div>
                          </ErrorBoundary>
                        ) : null}
                      </div>
                    ) : section._type == "weather" ? (
                      <React.Fragment>
                        <div id="lazyLoadweatherWidget" data-exclude="amp" />
                        <div key="weather" className="weather-wrapper">
                          {this.props.weather ? (
                            <WeatherCard dispatch={this.props.dispatch} weather={this.props.weather} />
                          ) : null}
                        </div>
                      </React.Fragment>
                    ) : (section.msid || section.id) && section.items ? (
                      this.createSectionLayout({
                        data: section,
                        datalabel: section.params.label,
                        className:
                          section.params.label === "astro"
                            ? "box-item astro"
                            : section.params && section.params.label === "photo"
                            ? "photo_video_section box-item"
                            : (section.params.tn && section.params.tn == "video") ||
                              (section.params && section.params._type && section.params._type === "video")
                            ? "photo_video_section box-item video"
                            : "box-item",
                        designlabel:
                          section.params.label === "astro"
                            ? "listWithOnlyInfo"
                            : section.params.label === "webstories"
                            ? "webstories"
                            : section.tn == "articlelist"
                            ? "listGallery"
                            : section.params.label === "viral"
                            ? "listGallery"
                            : section.params.label === "photo"
                            ? "gridHorizontalLeadAL"
                            : "gallery",
                        sliderObj: {
                          showSlider: section._rlvideoid !== "",
                          size: "3",
                          width: "200",
                        },
                      })
                    ) : null}
                    {section._type != "banner" &&
                    section._type != "poll" &&
                    section._type != "weather" &&
                    index % 2 == 0 ? (
                      <div data-exclude="amp">
                        <AdCard adtype="ctn" mstype="ctnbighome" pagetype="home" />
                      </div>
                    ) : null}
                  </ErrorBoundary>
                </div>
              );
            })
          ) : (
            <FakeListing showImages />
          )}

          {/** ------------------------------------- Gold podcast section Start --------------------------------- */}
          {/* {process.env.SITE === "eisamay" ? <GoldPodCastIframe /> : null} */}
          {/** -------------------------------------Gold podcast section  END --------------------------------- */}

          {(this.state.hasMounted || isAMPPage) &&
          recommended &&
          recommended.seotrending &&
          recommended.seotrending[0] &&
          recommended.seotrending[0].items ? (
            <div
              className="row box-item"
              data-row-number={`${totalWidgetRows + 1 || "trending-row"}`}
              data-scroll-id="SEOTRENDING"
              data-row-id="SEOTRENDING"
            >
              <div className="col12 trending-bullet">
                <KeyWordCard items={recommended.seotrending[0].items} secname={recommended.seotrending[0].secname} />
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

// HP election widget
// Home.fetchData = function ({dispatch, query, params}) {
//   //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
//   dispatch(setPageType('home'));
//   return dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(query, params, 'result')).then(() => dispatch(fetchTopListDataIfNeeded(params, query)));
// };

Home.fetchData = function({ dispatch, query, params, router }) {
  //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
  dispatch(setPageType("home"));
  return dispatch(fetchTopListDataIfNeeded(params, query, router)).then(data => {
    // fetchDesktopHeaderPromise(dispatch);
    return data;
  });
};

Home.propTypes = {};

function mapStateToProps(state) {
  return {
    home: state.home,
    // header: state.header,
    alaskaData: state.header.alaskaData,
    app: state.app,
    config: state.config,
    routing: state.routing,
    weather: state.app.weather,
  };
}

export default connect(mapStateToProps)(Home);
