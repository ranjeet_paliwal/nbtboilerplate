import React, { Component } from "react";
import { connect } from "react-redux";
import fetch from "utils/fetch/fetch";

import styles from "./../../components/common/css/ExitPoll.scss";
import drawPieChart from "../../../modules/charts/piechart";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

class ExitPoll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    const { dispatch, params, query } = this.props;
    const exitpollurl = `https://s3-ap-southeast-1.amazonaws.com/til-toi-content/Election/mobile_elections_RJ_2013_exit-polls.json`;
    // const exitpollurl = '/staticpage/file?filename=mobile_elections_RJ_2013_exit-polls.json';
    let _data = fetch(exitpollurl)
      .then(
        data => {
          //   data = JSON.parse(data.slice(1 , data.length-1).replace('times.mobile.election.electionresults(' , '').slice(0, -1));
          //   if (! data) throw new NetworkError('Invalid JSON Response', response.status);

          this.setState({ data: JSON.parse(data) });
          return JSON.parse(data);
        },
        error => console.log(error),
      )
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    let { data } = this.state;

    return (
      <div className="elm_container box election_con">
        <div className="el_result el_widget">
          <div className="result_inner">
            <h2 className="tbl_txt">EXIT POLL RESULTS 2013</h2>
            <div className="el_oops hide" data-ele="cg-exit-error-loader">
              <div className="refresh_btn">
                <a href="javascript:void(0);" data-ele="refresh">
                  <img
                    style={{ fontSize: "10px" }}
                    src={`${siteConfig.weburl}/photo//45565565.cms`}
                    alt=""
                    className="ex_res"
                  />
                  <span>Refresh</span>
                </a>
              </div>
            </div>
            <div data-ele="cg-graph-container">
              <div className="clearfix el_resulttable">
                <i className="tbl_view hide" data-ele="table-view" />
              </div>
              {data.al_rslt && data.al_rslt.length > 0 ? null : (
                <div className="ctyLoaderCont" data-ele="cg-graph-loader">
                  <img
                    style={{ fontSize: "10px" }}
                    src={`${siteConfig.weburl}/photo/47444548.cms`}
                    className="ctyLoader"
                  />
                </div>
              )}
              {data.al_rslt && data.al_rslt.length > 0
                ? data.al_rslt.map((item, index) => {
                    return ChartItems(item, index, data.al_rslt.ttl_seat, "mp");
                  })
                : null}
            </div>
            <div className="show" data-ele="cg-table-container">
              <div className="clearfix el_resulttable">
                <i className="grid_view" data-ele="graph-view" />
                {data.al_rslt && data.al_rslt.length > 0
                  ? data.al_rslt.map((item, index) => {
                      return ListItems(item, index, data.al_rslt.ttl_seat);
                    })
                  : null}
              </div>
            </div>
            <div className="btn_group" />
          </div>
        </div>
      </div>
    );
  }
}

function ChartItems(items, _index, total, state) {
  let canvasid = "canvas" + Math.floor(Math.random() * (100 - 50)) + 50;
  let chartObj = {
    data: {},
    colors: [],
    radius: Math.min(110, window.innerWidth / 2),
    canvasid: canvasid,
  };
  items.map(item => {
    chartObj.data[item.an] = parseInt(item.ws);
    chartObj.colors.push(item.cc);
  });
  return (
    <div className="pie-chart" data-ele="piechart_mp_0" key={_index}>
      <div className="el_graphdim">
        <div className="chart" data-ele="mp_graph_0" data-highcharts-chart="0">
          <div dir="ltr" className="highcharts-container">
            <div id={chartObj.canvasid} />
            {setTimeout(() => {
              drawPieChart(chartObj);
            }, 200)}
            <div visibility="visible" className="chart-detail">
              <div className="charttxt">
                <div className="chartInnerText">
                  <div className="seatcounted">230</div>
                  <div className="totalseat">{total}</div>
                  <div className="seatstatus">Exit Polls</div>
                </div>
              </div>
              <div className="charttitle">
                {state && state != "" ? (
                  <div className="title">
                    <span>{state.toLocaleUpperCase()}</span>
                  </div>
                ) : null}
                <div className="subtitle">Majority Mark: {Math.round(total / 2) + 1}</div>
              </div>
              <div className="titlebackground" />
            </div>
          </div>
        </div>
      </div>
      <div className="legend">
        <ul>
          {items.map((item, index) => {
            return parseInt(item.ws) + parseInt(item.ls) > 0 ? (
              <li key={index}>
                <div className="trend-text">{item.an}</div>
                <div className="trend-color" style={{ background: `${item.cc}` }} />
              </li>
            ) : null;
          })}
        </ul>
      </div>
      <div className="clearfix powerbycont">
        <div className="el_powerdby"> Source: {items[0].src}</div>
      </div>
    </div>
  );
}

function ListItems(items, _index, total) {
  return (
    <div data-ele="table_mp_0" key={_index}>
      <div className="result-tbl">
        <div className="tbl_heading">
          <div className="tbl_col  first">Party</div>
          <div className="tbl_col  tac">Projected</div>
        </div>
        {items.map((item, index) => {
          return (
            <div className="tbl_row" key={index}>
              <div className="tbl_col col1 party_logo">
                {item.an}
                <img src={`${siteConfig.weburl}/photo/${item.lg}.cms`} />
              </div>
              <div className="tbl_col tac col1 ">{item.ws}</div>
            </div>
          );
        })}

        <div className="tbl_row">
          <div className="tbl_col col1 party_logo">TOTAL SEATS :</div>
          <div className="tbl_col tac col1 ">{total}</div>
        </div>
      </div>
      <div className="clearfix powerbycont mp ">
        <div className="el_powerdby">{items[0].src}</div>
      </div>
    </div>
  );
}

export default ExitPoll;
