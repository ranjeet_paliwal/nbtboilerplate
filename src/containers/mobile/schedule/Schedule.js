import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchScheduleDataIfNeeded } from "../../../actions/schedule/schedule";

import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { scrollTo, _isCSR } from "../../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../../utils/cricket_util";

import "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/MiniSchedule.scss";

// import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import AnchorLink from "../../../components/common/AnchorLink";
import ImageCard from "../../../components/common/ImageCard/ImageCard";
// import AdCard from "../../../components/common/AdCard";
import Ads_module from "../../../components/lib/ads/index";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { setPageType } from "../../../actions/config/config";
import MiniSchedule from "../../../modules/minischedule/MiniSchedule";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

export class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBind: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const { query, pathname } = this.props.location;
    dispatch(setPageType("schedule"));
    //set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);

    if (
      this.props.value &&
      this.props.value[0] &&
      this.props.value[0].pwa_meta &&
      this.props.value[0].pwa_meta.cricketlb
    ) {
      params.seriesid = this.props.value[0].pwa_meta.cricketlb;
      pathname.indexOf("/results/") > -1 ? (params.pastMatches = true) : false;
    }

    Schedule.fetchData({ dispatch, query, params }).then(data => {
      //attach scroll only when sections is not there in feed
      if (typeof this.props.value[0] != "undefined" && !this.props.value[0].sections) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
      if (pwa_meta && typeof pwa_meta == "object") Ads_module.setSectionDetail(pwa_meta);
    });
    //reset slect box
    if (document.getElementById("TeamsSelect")) {
      document.getElementById("TeamsSelect").selectedIndex = "0";
    }
    if (document.getElementById("VenuesSelect")) {
      document.getElementById("VenuesSelect").selectedIndex = "0";
    }
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      // this.scrollHandler = throttle(this.handleScroll.bind(_this));
      //window.addEventListener('scroll', this.scrollHandler);
      // this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        Schedule.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();

          if (!data.payload[0].sections) {
            this.scrollBind();
          } else {
            this.scrollUnbind();
          }
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
      //reset slect box
      if (document.getElementById("TeamsSelect")) {
        document.getElementById("TeamsSelect").selectedIndex = "0";
      }
      if (document.getElementById("VenuesSelect")) {
        document.getElementById("VenuesSelect").selectedIndex = "0";
      }
      // let navigations = document.querySelector("#paginationHomePage").children;
      // if(navigations.length>0){
      //   for(let nav of navigations){
      //     try{
      //       nav.removeEventListener('click',this.handleNavigationClick, true);
      //     }catch(ex){}
      //     nav.classList.remove("active");
      //   }
      // }
      // document.querySelector(".mymenu_slide") ? (document.querySelector(".mymenu_slide").scrollLeft = 0) : '';
    }
  }
  handlefilterteam = event => {
    document.querySelector(".schedule_container").setAttribute("team", event.target.value);
    this.filter(event.target.value);
  };
  handlefiltervenue = event => {
    document.querySelector(".schedule_container").setAttribute("venue", event.target.value);
    this.filter(event.target.value);
  };
  filter(value) {
    let filter_team = document.querySelector(".schedule_container").getAttribute("team");
    let filter_venue = document.querySelector(".schedule_container").getAttribute("venue");
    let allschedule = document.getElementsByClassName("wdt_schedule");
    if (filter_team == "" && filter_venue == "") {
      let i;
      for (i = 0; i < allschedule.length; i++) {
        allschedule[i].style.display = "block";
      }
    } else {
      //hide all object show filter content
      let i;

      for (i = 0; i < allschedule.length; i++) {
        allschedule[i].style.display = "none";
      }

      if (filter_team != "" && filter_venue != "") {
        let recordexist = false;
        for (i = 0; i < allschedule.length; i++) {
          if (
            (allschedule[i].getAttribute("teama") == filter_team ||
              allschedule[i].getAttribute("teamb") == filter_team) &&
            allschedule[i].getAttribute("venue") == filter_venue
          ) {
            allschedule[i].style.display = "block";
            recordexist = true;
          }
        }
        if (recordexist) document.querySelector(".no-record-found").style.display = "none";
        else document.querySelector(".no-record-found").style.display = "block";
      } else if (filter_team != "") {
        let filterteam = document.getElementsByClassName("team_" + filter_team);
        let ii;
        for (ii = 0; ii < filterteam.length; ii++) {
          filterteam[ii].style.display = "block";
        }
      } else if (filter_venue != "") {
        let filtervenue = document.getElementsByClassName("venue_" + filter_venue);
        let ii;
        for (ii = 0; ii < filtervenue.length; ii++) {
          filtervenue[ii].style.display = "block";
        }
      }
    }
  }

  teamScore(team, item) {
    let inning1 = item && item.inn_team_1 != null && item.inn_team_1 != undefined ? parseInt(item.inn_team_1) : null;
    let inning2 = item && item.inn_team_2 != null && item.inn_team_2 != undefined ? parseInt(item.inn_team_2) : null;
    if (inning1 && team == "A" && inning1 == parseInt(item.teama_Id)) {
      return this.teamScoreFill(1, item);
    } else if (inning2 && team == "A" && inning2 == parseInt(item.teama_Id)) {
      return this.teamScoreFill(2, item);
    } else if (inning1 && team == "B" && inning1 == parseInt(item.teamb_Id)) {
      return this.teamScoreFill(1, item);
    } else if (inning2 && team == "B" && inning2 == parseInt(item.teamb_Id)) {
      return this.teamScoreFill(2, item);
    }
  }
  teamScoreFill(inning, item) {
    if (inning == 1) {
      return (
        <React.Fragment>
          <span className="score">{item.inn_score_1.substring(0, item.inn_score_1.indexOf("("))}</span>
          <span className="over">
            {item.inn_score_1.substring(item.inn_score_1.indexOf("(") + 1, item.inn_score_1.length - 1)}
          </span>
        </React.Fragment>
      );
    } else if (inning == 2) {
      return (
        <React.Fragment>
          <span className="score">{item.inn_score_2.substring(0, item.inn_score_2.indexOf("("))}</span>
          <span className="over">
            {item.inn_score_2.substring(item.inn_score_2.indexOf("(") + 1, item.inn_score_2.length - 1)}
          </span>
        </React.Fragment>
      );
    }
  }
  scheduleWidget(item) {
    let _this = this;
    let date = new Date(item.matchdate_ist);
    return (
      <React.Fragment>
        <div className="match_date_venue">
          {item.matchnumber} - {_getShortMonthName(date.getMonth()) + " " + date.getDate()}
          <span />
        </div>
        <div className="liveScore table">
          <div className="table">
            <div className="teama table_col">
              <b className="country">{item.teama_short}</b>
              <span className="teaminfo">
                <ImageCard msid={_getTeamLogo(item.teama_Id, "square")} size="rectanglethumb" />
                <span className="con_wrap">
                  {item.matchtype.toLowerCase() != "test" ? _this.teamScore("A", item) : null}
                </span>
              </span>
            </div>
            <div className="versus table_col">
              <span>VS</span>
            </div>
            <div className="teamb table_col">
              <b className="country">{item.teamb_short}</b>
              <span className="teaminfo">
                <ImageCard msid={_getTeamLogo(item.teamb_Id, "square")} size="rectanglethumb" />
                <span className="con_wrap">
                  {item.matchtype.toLowerCase() != "test" ? _this.teamScore("B", item) : null}
                </span>
              </span>
            </div>
          </div>
          {/* match_venue */}
          <div className="match_final_status">
            {item.matchresult && item.matchresult != "" ? (
              item.matchresult
            ) : (
              <span className="match_venue">{item.venue}</span>
            )}
            {item.live == "1" && item.live == "2" && item.live == "3" && item.live == "4" ? (
              <span className="status_live">Live</span>
            ) : null}
          </div>
        </div>
      </React.Fragment>
    );
  }
  render() {
    const pagetype = "schedule";
    let _this = this;
    const { isFetching, params } = this.props;
    let { sections, subsec1, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    let schedule =
      this.props.value[1] && this.props.value[1].data && this.props.value[1].data.matches
        ? this.props.value[1].data.matches
        : null;
    if (schedule && this.props.location.pathname.indexOf("/results/") > -1) {
      let pastmatches = schedule.filter(function(data) {
        return data.matchstatus_Id == 114;
      });
      schedule = pastmatches;
    }

    let distinctteams = [];
    let distinctvenues = [];
    if (schedule && typeof schedule != "null" && schedule.length > 0) {
      //filter distinct team list name and id
      let schedulemap = new Map();
      schedule.map((item, index) => {
        if (!schedulemap.has(item.teama_Id) && item.teama_Id != "") {
          schedulemap.set(item.teama_Id, true); // set team A mapping existance
          distinctteams.push({
            id: item.teama_Id,
            name: item.teama,
          });
        }
        if (!schedulemap.has(item.teamb_Id) && item.teamb_Id != "") {
          schedulemap.set(item.teamb_Id, true); // set team B mapping existance
          distinctteams.push({
            id: item.teamb_Id,
            name: item.teamb,
          });
        }
      });

      //filter distinct venue list
      let venuesmap = new Map();
      schedule.map((item, index) => {
        if (item.venue_Id && !venuesmap.has(item.venue_Id) && item.venue_Id != "") {
          venuesmap.set(item.venue_Id, true); // set venue mapping
          distinctvenues.push({
            id: item.venue_Id,
            name: item.venue,
          });
        }
      });
    }
    return this.props.value[0] && !isFetching ? (
      <div className="body-content" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(this.props.value[0].pwa_meta)}

        {this.props.value[0].pwa_meta.cricketlb && this.props.value[0].pwa_meta.cricketlb != "" ? (
          <ErrorBoundary key="schedule">
            <div className="mini_schedule">
              <div className="view-horizontal">
                <MiniSchedule
                  key="minischedule"
                  seriesid={this.props.value[0].pwa_meta.cricketlb}
                  dispatch={this.props.dispatch}
                  schedule={this.props.value[1].data.matches}
                />
              </div>
            </div>
          </ErrorBoundary>
        ) : null}
        {/* TO DO Currently Heading is dependent on parentection items as in case when for a section child section exist but parent does not have ant stories */}
        {parentSection && parentSection.secname ? (
          <div className="schedule_intro">
            <div className="sectionHeading">
              <h1>
                <span>
                  {parentSection.pwa_meta && parentSection.pwa_meta.pageheading
                    ? parentSection.pwa_meta.pageheading
                    : parentSection.secname}
                  {parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug
                    ? " " + parentSection.pwa_meta.pageheadingslug
                    : null}
                </span>
              </h1>
            </div>
            <div className="matchNvenue">
              {distinctteams && typeof distinctteams != "null" && distinctteams.length > 0 ? (
                <select id="VenuesSelect" onChange={this.handlefilterteam.bind(this)}>
                  <option value="" selected="selected">
                    All Teams
                  </option>
                  {distinctteams.map((item, index) => {
                    return <option value={item.id}>{item.name}</option>;
                  })}
                </select>
              ) : null}
              {distinctvenues && typeof distinctvenues != "null" && distinctvenues.length > 0 ? (
                <select id="VenuesSelect" onChange={this.handlefiltervenue.bind(this)}>
                  <option value="" selected="selected">
                    All Venues
                  </option>
                  {distinctvenues.map((item, index) => {
                    return <option value={item.id}>{item.name}</option>;
                  })}
                </select>
              ) : null}
            </div>
          </div>
        ) : null}

        {/* Cricket Schedule Widget for event based page  */}
        {
          <React.Fragment>
            <ErrorBoundary key="schedule">
              <div className="schedule_container" team="" venue="">
                {schedule && typeof schedule != "null" && schedule.length > 0
                  ? schedule.map((item, index) => {
                      return (
                        <div
                          className={`wdt_schedule team_${item.teama_Id} team_${item.teamb_Id} venue_${item.venue_Id}`}
                          key={index}
                          teama={item.teama_Id}
                          teamb={item.teamb_Id}
                          venue={item.venue_Id}
                        >
                          {item.live == 1 || item.matchstatus_Id == "114" ? (
                            <AnchorLink
                              data-type="scorecard"
                              style={{ textDecoration: "none" }}
                              hrefData={{
                                override:
                                  siteConfig.mweburl +
                                  "/sports/cricket/live-score/" +
                                  item.teama_short.toLowerCase() +
                                  "-vs-" +
                                  item.teamb_short.toLowerCase() +
                                  "/" +
                                  item.matchdate_ist.replace(/\//g, "-") +
                                  "/scoreboard/matchid-" +
                                  item.matchfile +
                                  ".cms",
                              }}
                            >
                              {_this.scheduleWidget(item)}
                            </AnchorLink>
                          ) : (
                            _this.scheduleWidget(item)
                          )}
                        </div>
                      );
                    })
                  : null}
                <div className="no-record-found hide">No Record Found</div>
              </div>
            </ErrorBoundary>
            //{" "}
          </React.Fragment>
        }

        {isFetching ? <FakeListing showImages={true} /> : null}
        {this.props &&
        this.props.value &&
        this.props.value[0] &&
        this.props.value[0].pwa_meta &&
        this.props.value[0].pwa_meta.seodescription != undefined ? (
          <p className="btf-content">{this.props.value[0].pwa_meta.seodescription}</p>
        ) : null}

        {/* Breadcrumb  */}
        {parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}
        {/* For SEO Schema */ SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

Schedule.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchScheduleDataIfNeeded(params, query));
};

function mapStateToProps(state) {
  return {
    ...state.schedule,
  };
}

export default connect(mapStateToProps)(Schedule);
