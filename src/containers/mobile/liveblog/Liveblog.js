// import React, { Component } from "react";
// import { connect } from "react-redux";
// import { fetchLIVEBLOG_IfNeeded, fetchLiveBlogCount } from "../../../actions/liveblog/liveblog";
// import ImageCard from "../../../components/common/ImageCard/ImageCard";
// import FakeNewsListCard from "../../../components/common/FakeCards/FakeStoryCard";
// import LiveblogCard from "../../../components/common/LiveblogCard";
// import HighlightCard from "../../../components/common/HighlightCard";
// import Breadcrumb from "../../../components/common/Breadcrumb";

// import styles from "../../../components/common/css/liveblog.scss";
// import AnchorLink from "../../../components/common/AnchorLink";
// import { PageMeta, SeoSchema } from "../../../components/common/PageMeta";
// import { _isCSR, setHyp1Data } from "../../../utils/util";
// import SocialShare from "../../../components/common/SocialShare";
// import { AnalyticsGA } from "../../../components/lib/analytics";

// import { _getStaticConfig } from "../../../utils/util";
// import { setPageType } from "../../../actions/config/config";
// import { setSectionDetail } from "../../../components/lib/ads";
// import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
// import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
// const siteConfig = _getStaticConfig();

// class Liveblog extends Component {
//   constructor(props) {
//     super(props);
//     this.config = {
//       lbcount: 0,
//       lbupdatecount: 0,
//       poolInterval: 45, //unit in sec's
//       elemUpdateBtn: null,
//     };
//   }

//   componentDidMount() {
//     const { dispatch, params, query, lbcontent } = this.props;

//     //hack for reset ads section
//     if (this.props.meta && this.props.meta.pwa_meta) {
//       //setHyp1Data(this.props.meta.pwa_meta);
//       setSectionDetail(this.props.meta.pwa_meta);
//     }
//     Liveblog.fetchData({ dispatch, params, query }).then(data => {
//       let _data = data && data.payload && data.payload instanceof Array ? data.payload : [];
//       if (_data.length == 2 && _data[1] && _data[1].pwa_meta && _data[1].pwa_meta.ibeat) {
//         //fire ibeat
//         setIbeatConfigurations(_data[1].pwa_meta.ibeat);
//         setSectionDetail(_data[1].pwa_meta);
//       }
//     });

//     if (typeof window != undefined) {
//       this.shouldUpdateLiveblog();
//     }

//     //set hyp1 variable
//     this.props.meta && this.props.meta.pwa_meta ? setHyp1Data(this.props.meta.pwa_meta) : "";
//     // twitter js added
//     const twitter = document.createElement("script");
//     twitter.src = "https://platform.twitter.com/widgets.js";
//     twitter.defer = true;

//     document.head.appendChild(twitter);
//   }

//   componentWillUnmount() {
//     //reset hyp1 to ''
//     setHyp1Data();
//     //reset section to ''
//     setSectionDetail();
//   }

//   shouldUpdateLiveblog() {
//     const _this = this;
//     const { dispatch, params, query } = _this.props;
//     setInterval(function() {
//       AnalyticsGA.pageview(location.pathname, {
//         forcefulGaPageview: true,
//       });
//       if (_this.props.meta.pwa_meta.cricketlb) {
//         _this.config.elemUpdateBtn = document.querySelector(".btn_newupdates");
//         _this.config.elemUpdateBtn.innerHTML = " New Updates"; //set total new content count
//         _this.config.elemUpdateBtn.style.display = "inline-block"; //show update button
//       } else {
//         fetchLiveBlogCount(dispatch, params, query)
//           .then(data => {
//             _this.config.elemUpdateBtn = document.querySelector(".btn_newupdates");
//             //for first time update all config counts
//             if (data && _this.config.lbcount == 0) {
//               _this.config.lbcount = parseInt(data.count);
//               _this.config.lbupdatecount = parseInt(data.count);
//             } else if (_this.config.elemUpdateBtn && data && _this.config.lbcount < parseInt(data.count)) {
//               _this.config.elemUpdateBtn.innerHTML = parseInt(data.count) - _this.config.lbupdatecount + " New Updates"; //set total new content count
//               _this.config.elemUpdateBtn.style.display = "inline-block"; //show update button

//               _this.config.lbcount = parseInt(data.count); // update lbcount to new-count
//             }
//           })
//           .catch(err => {
//             console.log("Inside catch", err);
//           });
//       }
//     }, _this.config.poolInterval * 1000);
//   }

//   updateLiveblog() {
//     const _this = this;
//     const { dispatch, params, query } = this.props;
//     dispatch(fetchLIVEBLOG_IfNeeded(params, query));

//     //update count value on click
//     _this.config.lbupdatecount = _this.config.lbcount;
//     document.querySelector("ul.livepost li").scrollIntoView({ behavior: "smooth", block: "center" });
//     _this.config.elemUpdateBtn.style.display = "none"; //show update button
//   }

//   getShareDetail(isShareAll) {
//     if (!_isCSR()) return false;

//     let sharedata = {
//       title: document.title,
//       url: location.href,
//     };

//     if (!isShareAll) return sharedata;

//     //for watsapp sharing feature along with GA
//     AnalyticsGA.event({
//       category: "social",
//       action: "Whatsapp_Wap_AS",
//       label: sharedata.url,
//     });

//     var info = sharedata.url;
//     info += "?utm_source=whatsapp" + siteConfig.fullutm;
//     var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + info;
//     window.location.href = whatsappurl;

//     return false;
//   }

//   render() {
//     let { value, isFetching } = this.props;
//     let { lbcontent, tinyscore } = value != undefined ? value : {};
//     let maxItems = 20,
//       post = [],
//       highlights = [],
//       maxHighlights = 5,
//       count = 0;
//     let pagetype = "liveblog";
//     let meta = lbcontent != undefined ? this.props.meta.pwa_meta : {};
//     let breadcrumbData =
//       this.props.meta && this.props.meta.div && this.props.meta.div.ul ? this.props.meta.div.ul : null;

//     let { query } = this.props.location;

//     if (_isCSR()) {
//       maxItems = meta.cricketlb ? 1000 : 50;
//     }

//     if (lbcontent != undefined && meta != undefined) {
//       lbcontent.some(function(item, index) {
//         if (index < maxItems) post.push(item);
//         if (item.keyevent === 1 && count < maxHighlights && index < maxItems) {
//           highlights.push(item);
//           count++;
//         }
//       });
//     }

// return lbcontent != undefined && meta != undefined ? (
// <div
//   className={styles.lb_container + " lb_container"}
//   {...SeoSchema({ pagetype: pagetype, url: meta.canonical }).attr().liveBlog}
// >
//   {/* For SEO */ PageMeta(meta)}
//   {SeoSchema({ pagetype: pagetype }).metaUrl(meta.canonical)}
//   {SeoSchema().metaTag({
//     name: "datePublished",
//     content: meta.publishcontent,
//   })}
//   {SeoSchema().metaTag({
//     name: "dateModified",
//     content: meta.modifiedcontent,
//   })}
//   {SeoSchema().metaTag({
//     name: "coverageStartTime",
//     content: meta.createdate,
//   })}
//   {SeoSchema().metaTag({
//     name: "coverageEndTime",
//     content: meta.updateddate,
//   })}

//   {
//     <React.Fragment>
//       <div className={styles.lb_summaryCard + " lb_summaryCard"}>
//         <div className={meta.cricketlb && tinyscore ? "lb_top_summary width_scorecard" : "lb_top_summary"}>
//           <span className="source"><span className="liveblink"></span> {new Date(lbcontent[0].artDate).toString().split(" GMT")[0]}</span>

//           {meta.imgmsid != undefined && meta.imgmsid != "" ? (
//             <ImageCard msid={meta.imgmsid} size="smallthumb" imgsize={meta.imgsize || ""} />
//           ) : (
//             <ImageCard msid={meta.parentid} size="smallthumb" imgsize={meta.imgsize || ""} />
//           )}
//           <h1>{meta.heading}</h1>
//           {meta && meta.h2 && <h2 {...SeoSchema({ pagetype: pagetype }).attr().pageheadline}>{meta.h2}</h2>}
//           {meta.desc != "" ? (
//             <span className="caption" {...SeoSchema({ pagetype: pagetype }).attr().description}>
//               {meta.desc}
//             </span>
//           ) : (
//             ""
//           )}
//           {/* Scorecard Widget */}
//           {meta.cricketlb && tinyscore ? (
//             <div className="liveblog-scorecard">
//               <AnchorLink
//                 data-type="scorecard"
//                 target="_blank"
//                 style={{ textDecoration: "none" }}
//                 hrefData={{
//                   override:
//                     siteConfig.mweburl +
//                     "/sports/cricket/live-score/" +
//                     tinyscore.seoteamname +
//                     "/" +
//                     tinyscore.date +
//                     "/scoreboard/matchid-" +
//                     tinyscore.matchid +
//                     ".cms",
//                 }}
//               >
//                 <div className="liveScore table">
//                   <div className="teama table_col">
//                     <span className="country">{tinyscore.teama}</span>
//                     <span className="teaminfo">
//                       <img height="60" width="60" src={tinyscore.teamalogo} />
//                       <span className="score">{tinyscore.teamascore}</span>
//                       <span className="over">{tinyscore.teamaovers != "" ? tinyscore.teamaovers : ""}</span>
//                     </span>
//                   </div>
//                   <div className="versus table_col">
//                     <span>VS</span>
//                   </div>
//                   <div className="teamb table_col">
//                     <span className="country">{tinyscore.teamb}</span>
//                     <span className="teaminfo">
//                       <img height="60" width="60" src={tinyscore.teamblogo} />
//                       <span className="score">{tinyscore.teambscore}</span>
//                       <span className="over">{tinyscore.teambovers != "" ? tinyscore.teambovers : ""}</span>
//                     </span>
//                   </div>
//                 </div>
//               </AnchorLink>
//             </div>
//           ) : null}
//         </div>
//         {highlights.length > 0 ? <HighlightCard data={highlights} type="lbhighlight" /> : null}
//       </div>
//       {meta.cricketlb && tinyscore ? (
//         <div className="liveblog_tabs">
//           <ul>
//             <li className="current">
//               <b>{siteConfig.locale.commentary}</b>
//             </li>
//             <li>
//               <AnchorLink
//                 data-type="scorecard"
//                 target="_blank"
//                 style={{ textDecoration: "none" }}
//                 hrefData={{
//                   override:
//                     siteConfig.mweburl +
//                     "/sports/cricket/live-score/" +
//                     tinyscore.seoteamname +
//                     "/" +
//                     tinyscore.date +
//                     "/scoreboard/matchid-" +
//                     tinyscore.matchid +
//                     ".cms",
//                 }}
//               >
//                 {siteConfig.locale.scorecard}
//               </AnchorLink>
//             </li>
//           </ul>
//         </div>
//       ) : null}
//     </React.Fragment>
//   }
//   <LiveblogCard lbcontent={post} pwa_meta={meta} query={query} />
//   <span itemScope="" itemType="https://schema.org/NewsArticle">
//     {SeoSchema().metaTag({
//       name: "mainEntityOfPage",
//       content: meta ? meta.canonical : "",
//     })}
//     {SeoSchema().metaTag({
//       name: "headline",
//       content:
//         meta.hindiheading && meta.hindiheading.length > 100
//           ? meta.hindiheading.substr(0, 100)
//           : meta.hindiheading,
//     })}
//     {SeoSchema().metaTag({
//       name: "alternativeHeadline",
//       content: meta.alternatetitle,
//     })}
//     {SeoSchema().metaTag({
//       name: "description",
//       content: meta ? meta.desc : "",
//     })}
//     {SeoSchema().metaTag({
//       name: "articleBody",
//       content: meta ? meta.desc : "",
//     })}
//     {SeoSchema().language()}
//     {SeoSchema().metaTag({
//       name: "datePublished",
//       content: meta.publishcontent,
//     })}
//     {SeoSchema().metaTag({
//       name: "dateModified",
//       content: meta.modifiedcontent,
//     })}
//     <span itemType="https://schema.org/ImageObject" itemScope="itemScope" itemProp="image	">
//       <meta
//         itemProp="url"
//         content={
//           "https://navbharattimes.indiatimes.com/thumb/msid-" + meta.imgmsid + ",width-1200,height-900/pic.jpg"
//         }
//       />
//       <meta content="1200" itemProp="width" />
//       <meta content="900" itemProp="height" />
//     </span>
//     {SeoSchema().publisherObj()}
//     {SeoSchema().metaTag({ name: "author", content: "websitename" })}
//     {/* if post image not available */ SeoSchema().metaTag({
//       name: "thumbnailUrl",
//       content: meta.ogimg ? meta.ogimg : "",
//     })}

//     {SeoSchema({ pagetype: pagetype }).metaUrl(meta.canonical)}
//   </span>
//   <div className="pos_fixed">
//     {isFetching ? (
//       <span className="loader-anim" />
//     ) : (
//       <span className="btn_newupdates hide" onClick={this.updateLiveblog.bind(this)} />
//     )}
//   </div>
//   <div className="share_container">
//     <div className="share_whatsapp">
//       <a rel="nofollow" onClick={this.getShareDetail.bind(this, true)} href="javascript:void(0);">
//         <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24">
//           <path
//             d="M20.1,3.9C17.9,1.7,15,0.5,12,0.5C5.8,0.5,0.7,5.6,0.7,11.9c0,2,0.5,3.9,1.5,5.6l-1.6,5.9l6-1.6c1.6,0.9,3.5,1.3,5.4,1.3l0,0l0,0c6.3,0,11.4-5.1,11.4-11.4C23.3,8.9,22.2,6,20.1,3.9z M12,21.4L12,21.4c-1.7,0-3.3-0.5-4.8-1.3l-0.4-0.2l-3.5,1l1-3.4L4,17c-1-1.5-1.4-3.2-1.4-5.1c0-5.2,4.2-9.4,9.4-9.4c2.5,0,4.9,1,6.7,2.8c1.8,1.8,2.8,4.2,2.8,6.7C21.4,17.2,17.2,21.4,12,21.4z M17.1,14.3c-0.3-0.1-1.7-0.9-1.9-1c-0.3-0.1-0.5-0.1-0.7,0.1c-0.2,0.3-0.8,1-0.9,1.1c-0.2,0.2-0.3,0.2-0.6,0.1c-0.3-0.1-1.2-0.5-2.3-1.4c-0.9-0.8-1.4-1.7-1.6-2c-0.2-0.3,0-0.5,0.1-0.6s0.3-0.3,0.4-0.5c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0-0.4,0-0.5c0-0.1-0.7-1.5-1-2.1C8.9,6.6,8.6,6.7,8.5,6.7c-0.2,0-0.4,0-0.6,0S7.5,6.8,7.2,7c-0.3,0.3-1,1-1,2.4s1,2.8,1.1,3c0.1,0.2,2,3.1,4.9,4.3c0.7,0.3,1.2,0.5,1.6,0.6c0.7,0.2,1.3,0.2,1.8,0.1c0.6-0.1,1.7-0.7,1.9-1.3c0.2-0.7,0.2-1.2,0.2-1.3C17.6,14.5,17.4,14.4,17.1,14.3z"
//             stroke="none"
//             fill="#fff"
//           />
//         </svg>
//       </a>
//     </div>
//     <SocialShare sharedata={this.getShareDetail.bind(this, false)} />
//   </div>
//   {/* Breadcrumb  */}
//   {breadcrumbData ? (
//     <ErrorBoundary>
//       <Breadcrumb items={breadcrumbData} />
//     </ErrorBoundary>
//   ) : null}
// </div>
// ) : (
//   <FakeNewsListCard />
// );
//   }
// }

// function mapStateToProps(state) {
//   return {
//     ...state.liveblog,
//   };
// }

// function listItems(item, index) {
//   return (
//     <li key={index}>
//       {item.hoverridLink != "" && item.hoverridLink != "null" ? (
//         <a href={item.hoverridLink}>{item.title}</a>
//       ) : (
//         item.title
//       )}
//       {item.detailMsg != "" && item.keyevent === 0 ? <p>{item.detailMsg}</p> : null}
//       {(() => {
//         {
//           /*Create Image/Video on the basis of mstype/type ('m' for Image and)*/
//         }
//         switch (item.type) {
//           case "m":
//             {
//               /* Do not print media in case of keyevent===1 (highlights) */
//             }
//             if (item.keyevent === 0) {
//               return <ImageCard msid={item.msid} size="bigimage" />;
//             } else {
//               return true;
//             }
//         }
//       })()}
//     </li>
//   );
// }

// Liveblog.fetchData = function({ dispatch, query, params }) {
//   //set pagetype
//   dispatch(setPageType("liveblog"));
//   return dispatch(fetchLIVEBLOG_IfNeeded(params, query));
// };

// export default connect(mapStateToProps)(Liveblog);
