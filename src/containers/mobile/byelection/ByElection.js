import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchByElectionDataIfNeeded } from "../../../actions/byelection/byelection";

import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import { _isCSR } from "../../../utils/util";
import { _getShortMonthName, _getTeamLogo } from "../../utils/cricket_util";

import styles from "./../../components/common/css/commonComponents.scss";

// import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import Ads_module from "../../../components/lib/ads/index";
import ByElectionWidget from "../../../modules/byelection/ByElection";
// import PaytmWidget from '../../modules/paytmwidget/PaytmWidget';
import { setPageType } from "../../../actions/config/config";

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();

export class ByElection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _scrollEventBind: false,
    };
    this.scrollHandler = false;
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    const { query } = this.props.location;
    dispatch(setPageType("byelection"));
    //set section & subsection for first time
    if (value[0] && typeof value[0].pwa_meta == "object") Ads_module.setSectionDetail(value[0].pwa_meta);
    if (
      this.props.value &&
      this.props.value[0] &&
      this.props.value[0].pwa_meta &&
      this.props.value[0].pwa_meta.cricketlb
    ) {
      params.seriesid = this.props.value[0].pwa_meta.cricketlb;
    }
    ByElection.fetchData({ dispatch, query, params }).then(data => {
      //set section and subsection in window
      const { pwa_meta } = this.props.value[0] ? this.props.value[0] : {};
      if (pwa_meta && typeof pwa_meta == "object") Ads_module.setSectionDetail(pwa_meta);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        ByElection.fetchData({ dispatch, query, params }).then(data => {});
      } catch (ex) {}
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      //reset section window data
      Ads_module.setSectionDetail();
    }
  }

  render() {
    const pagetype = "byelection";
    let _this = this;
    const { isFetching, params } = this.props;
    let { sections, subsec1, ...parentSection } = this.props.value[0] ? this.props.value[0] : "";
    let pointstable =
      this.props.value[1] &&
      this.props.value[1].standings &&
      this.props.value[1].standings.stage1 &&
      this.props.value[1].standings.stage1.team &&
      this.props.value[1].standings.stage1.team.length > 0
        ? this.props.value[1].standings.stage1.team
        : null;
    return this.props.value[0] && !isFetching ? (
      <div className="body-content" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(this.props.value[0].pwa_meta)}

        {/* TO DO Currently Heading is dependent on parentection items as in case when for a section child section exist but parent does not have ant stories */}
        {parentSection && parentSection.secname ? (
          <div className={styles["sectionHeading"] + " sectionHeading"}>
            <h1>
              <span>
                {parentSection.pwa_meta && parentSection.pwa_meta.pageheading
                  ? parentSection.pwa_meta.pageheading
                  : parentSection.secname}
                {parentSection.pwa_meta && parentSection.pwa_meta.pageheadingslug
                  ? " " + parentSection.pwa_meta.pageheadingslug
                  : null}
              </span>
            </h1>
          </div>
        ) : null}
        {
          <ErrorBoundary key="ByElection">
            <ByElectionWidget
              pagetype={pagetype}
              apiUrl={siteConfig.byelection}
              ByElectioncontent={this.props.value[1]}
            />
          </ErrorBoundary>
        }
        {
          <ErrorBoundary key="ByElectionCandidate">
            <ByElectionWidget
              heading={false}
              pagetype={pagetype}
              apiUrl={siteConfig.byelectionalt}
              ByElectioncontent={this.props.value[2]}
            />
          </ErrorBoundary>
        }

        {isFetching ? <FakeListing showImages={true} /> : null}

        {/* Breadcrumb  */}
        {parentSection && typeof parentSection.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={parentSection.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}
        {/* For SEO Schema */ SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

ByElection.fetchData = function({ dispatch, query, params }) {
  return dispatch(fetchByElectionDataIfNeeded(params, query));
};

function mapStateToProps(state) {
  return {
    ...state.byelection,
  };
}

export default connect(mapStateToProps)(ByElection);
