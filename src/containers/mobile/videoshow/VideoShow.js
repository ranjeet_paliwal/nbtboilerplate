import React, { Component } from "react";
import { connect } from "react-redux";
/*import PropTypes from 'prop-types';
import Loader from 'components/lib/loader/Loader';
import Error from 'components/lib/error/Error';
import Cookies from 'universal-cookie';*/
import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchShowDataIfNeeded } from "../../../actions/videoshow/videoshow";
import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import VideoListCard from "../../../components/common/VideoListCard";
import { AnalyticsGA } from "../../../components/lib/analytics/index";

import { SeoSchema, PageMeta } from "../../../components/common/PageMeta"; //For SEO Meta and Schema

import { _getStaticConfig } from "../../../utils/util";
const siteConfig = _getStaticConfig();
//const pages = siteConfig.pages;

import { setPageType } from "../../../actions/config/config";
import { setSectionDetail } from "../../../components/lib/ads";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import WebTitleCard from "../../../components/common/WebTitleCard";
import VideoItem from "../../../components/common/VideoItem/VideoItem";
import { removeAllSlikePlayerInitializations } from "../../../modules/videoplayer/slike";

export class VideoShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      msid: "",
    };
    this.pageViewTriggered = false;
  }

  componentDidMount() {
    const { dispatch, params, value } = this.props;
    this.state.msid = params ? params.msid : this.props.msid;
    // dispatch(setPageType("videoshow"));
    //if (value && typeof value.pwa_meta == "object") setSectionDetail(value.pwa_meta);
    if (typeof params == "undefined") {
      VideoShow.fetchData({ dispatch, params: { msid: this.state.msid } }).then(function(data) {
        let value = typeof data != "undefined" && data.payload ? data.payload : value ? value : {};
        //set section and subsection in window
        const { pwa_meta, audetails } = value ? value : {};

        if (pwa_meta && typeof pwa_meta == "object") {
          setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          window._editorname = "";
          if (audetails) {
            window._editorname = audetails.cd;
          } else {
            window._editorname =
              typeof pwa_meta.editorname != "undefined" && pwa_meta.editorname != "" ? pwa_meta.editorname : "";
          }
          AnalyticsGA.pageview(window.location.pathname, {
            setEditorName: true,
          });
          this.pageViewTriggered = true;
        }
      });
    } else {
      VideoShow.fetchData({ dispatch, params }).then(function(data) {
        let value = typeof data != "undefined" && data.payload ? data.payload : value ? value : {};

        //set section and subsection in window
        const { pwa_meta } = value ? value : {};
        if (pwa_meta && typeof pwa_meta == "object") {
          setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        }
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const _this = this;
    if (typeof this.props.params != "undefined" && this.props.params.msid != nextProps.params.msid) {
      const { dispatch, params } = nextProps;

      VideoShow.fetchData({ dispatch, params });
    }
  }

  onVideoClickHandler(item, event) {
    const _this = this;
    const { dispatch } = _this.props;
    if (typeof _this.props.params != "undefined" && _this.props.params.msid != item.id) {
      _this.props.params.msid = item.id;
      let newParams = _this.props.params;
      VideoShow.fetchData({ dispatch, params: newParams }).then(data => {
        let _data = data && data.payload ? data.payload : {};
        //fire ibeat for next
        _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
      });
    } else {
      _this.state.msid = item.id;
      VideoShow.fetchData({ dispatch, params: { msid: item.id } }).then(data => {
        let _data = data && data.payload ? data.payload : {};
        //fire ibeat for next
        _data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : "";
      });
    }
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(setPageType(""));

    // When video is in paused state, and we exit page, it doesn't get docked
    // Remove all slike initializations in this case ( also fires grx video_played event as a result)
    if (!window.adPlaying && window.currentPlayerInst) {
      if (window.currentPlayerInst.getVideoState) {
        const videoState = window.currentPlayerInst.getVideoState();
        if (videoState.paused) {
          removeAllSlikePlayerInitializations();
        }
      }
    }

    //reset section to ''
    setSectionDetail();
  }

  render() {
    const { isFetching, params, value, error } = this.props;
    const firstItem = (value && value.items && value.items[0]) || "";
    const NextItems = (value && value.items && value.items[0] && value.items[0].nextvideos) || "";

    if (error) {
      return null;
    }

    return firstItem ? (
      <div className="body-content">
        {value && value.pwa_meta ? PageMeta(value.pwa_meta) : null}
        {this.state.showPopup ? <VideoPopup /> : ""}
        <div className="box-content videoshow-container">
          <React.Fragment>
            <div className="top_wdt_video">
              <div className="row">
                <div className="col col-8">
                  <ul className="nbt-list">
                    <li className="table nbt-listview videoshowview lead-post">
                      <span className="table_col img_wrap">
                        <VideoItem item={firstItem} lead={true} />
                      </span>
                      <div>
                        Follow us on: <span id="youtube-btn-subscibe" />
                      </div>

                      <span
                        className="table_col con_wrap"
                        dangerouslySetInnerHTML={{ __html: (firstItem && firstItem.Story) || "" }}
                      />
                    </li>
                  </ul>

                  {WebTitleCard((value && value.pwa_meta) || "")}
                </div>
                <div className="col col-4 mr0">
                  <div className="col col-4 mr0">
                    <div
                      data-adtype="desktopMrec"
                      className="ad1 mrec"
                      data-id="div-gpt-ad-1543293292770-vd-mrec"
                      data-name="/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_ATF_300"
                      data-size="[[300, 250]]"
                    />
                  </div>
                </div>
              </div>
            </div>

            <h2>
              <span>More Videos from this Section</span>
            </h2>
            {NextItems && (
              <ul className="nbt-list">
                <SectionVideos data={(NextItems && NextItems.items) || ""} />
              </ul>
            )}
            {value && value.items[1] && value.items[1].ul && (
              <ErrorBoundary>
                <Breadcrumb items={value.items[1].ul} />
              </ErrorBoundary>
            )}
          </React.Fragment>
        </div>
      </div>
    ) : (
      <FakeListing showImages={true} pagetype="videoshow" />
    );
  }
}

VideoShow.fetchData = function({ dispatch, params }) {
  //set pagetype
  // dispatch(setPageType("videoshow"));
  return dispatch(fetchShowDataIfNeeded(params)).then(data => {
    if (data && data.payload && data.payload.pwa_meta) {
      setSectionDetail(data.payload.pwa_meta);
      dispatch(setPageType("videoshow", data.payload.pwa_meta.site));
    }
  });
};

const SectionVideos = ({ data }) => {
  const noLazyLoad = false;
  return data && Array.isArray(data) && data.length > 0
    ? data.map((item, index) => {
        return <VideoItem key={index.toString()} item={item} />;
      })
    : "";
};

function mapStateToProps(state) {
  return {
    ...state.videoshow,
  };
}

export default connect(mapStateToProps)(VideoShow);
