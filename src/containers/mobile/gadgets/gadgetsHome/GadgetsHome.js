import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded, fetchBrandListDataIfNeeded } from "../../../../actions/listpage/listpage";
import { setPageType, setParentId } from "../../../../actions/config/config";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { defaultDesignConfigs } from "../../../defaultDesignConfigs";
import KeyWordCard from "../../../../components/common/KeyWordCard";
import GadgetSlider from "../../../../components/common/GadgetSlider";
import BrandSlider from "../../../../components/common/BrandSlider";
import GridCardMaker from "../../../../components/common/ListingCards/GridCardMaker";
import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import { updateConfig, _getStaticConfig, _isFrmApp, checkIsAmpPage, animate_btn_App } from "../../../../utils/util";
import Ads_module from "./../../../../components/lib/ads/index";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import "./../../../../components/common/css/SectionWrapper.scss";
import "./../../../../components/common/css/GadgetNow.scss";
import AdCard from "../../../../components/common/AdCard";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import { PageMeta } from "../../../../components/common/PageMeta";
import MiniTVCard from "../../../../components/common/MiniTVCard";
import AmazonWidget from "../../../../components/common/AmazonWidget/AmazonWidget";
import OpenInAppAtALPage from "../../../../components/common/OpenInAppALPage";

const siteConfig = _getStaticConfig();
export class GadgetsNow extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { dispatch, query, params, router } = this.props;

    if (params && params.msid == undefined) {
      params.msid = siteConfig.pages.tech;
    }
    GadgetsNow.fetchData({ dispatch, query, params, router }).then(data => {
      // Do not remove below line articlelist will change after fetching data for particular MSID.
      const { articlelist } = this.props;
      if (articlelist && articlelist.value && articlelist.value[0]) {
        const { pwa_meta, pg } = articlelist.value[0] ? articlelist.value[0] : {};
        // if (pg && pg.tp) {
        //   this.state.totalPages = pg.tp;
        // }
        if (pwa_meta && typeof pwa_meta == "object") {
          //set section and subsection in window
          Ads_module.setSectionDetail(pwa_meta);
          //fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      }
    });
    const category = "camera";
    GadgetsNow.fetchBranchListData({ dispatch, query, params, router, category });
  }

  componentDidUpdate() {
    const { articlelist, dispatch } = this.props;
    if (articlelist.value && articlelist.value[0] && articlelist.value[0].pwa_meta) {
      const pwaMeta = { ...articlelist.value[0].pwa_meta };
      updateConfig(pwaMeta, dispatch, setParentId);
      animate_btn_App();
    }
  }

  createSectionLayout = ({ data, index }) => {
    return (
      <div
        className={`${
          data.tn === "videolist"
            ? "photo_video_section row box-item video"
            : data.tn === "photolist"
            ? "photo_video_section row box-item"
            : "row box-item"
        }`}
      >
        <ErrorBoundary>
          <SectionHeader sectionhead={data.secname} weblink={data.override || data.wu || ""} />
          <GridSectionMaker
            type={
              //data.secnameseo && data.secnameseo == "review" ? defaultDesignConfigs.reviewListingGn :
              data.tn == "videolist"
                ? defaultDesignConfigs.gallery
                : data.tn == "photolist"
                ? defaultDesignConfigs.gridHorizontalLeadAL
                : defaultDesignConfigs.reviewListingGn
            }
            data={data.items}
            key={index}
            keyName={index}
          />
        </ErrorBoundary>
        {/* </ErrorBoundary> */}
      </div>
    );
  };

  fetchBrandSlider = () => {
    return (
      <React.Fragment>
        <div className="row con_brands" data-exclude="amp">
          <SectionHeader
            sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.brands}
            weblink={""}
          />
          <BrandSlider />
        </div>
        <AdCard adtype={"ctn"} mstype={"ctnbighome"} />
      </React.Fragment>
    );
  };

  render() {
    const { articlelist, router, location } = this.props;
    let { value, isFetching, gadgetsData } = articlelist;
    const widgetData = value[0] || null;
    let { minitv } = this.props.app;
    let isFrmApp = _isFrmApp(this.props.router);
    let { pwa_meta, recommended } =
      articlelist && articlelist.value instanceof Array && articlelist.value[0] ? articlelist.value[0] : {};

    let msid = widgetData && widgetData.id;
    let secName = widgetData && widgetData.secname;
    let pathname = location && location.pathname;
    let isAmpPage = checkIsAmpPage(pathname); 

    console.log("Props", this.props) ; 
    return !isFetching ? (
      <div className="body-content section-wrapper">
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        <div className="top-news-content row">
          <ul className="col12 pd0">
            {this.props.articlelist &&
            this.props.articlelist.value &&
            this.props.articlelist.value.length > 0 &&
            this.props.articlelist.value[0].items.length > 0 ? (
              <React.Fragment>
                <TopSection
                  items={this.props.articlelist.value[0].items}
                  minitv={minitv}
                  location={location}
                  isFrmApp={isFrmApp}
                />
                <OpenInAppAtALPage msid={msid} compType="articlelist" isAmpPage={isAmpPage} secName={secName} />  
                <li class="news news-card horizontal col">
                  <AmazonWidget item={{ id: "gnhome" }} type="sponsored" router={router} />
                </li>
              </React.Fragment>
            ) : null}
          </ul>
        </div>

        <div className="row con_gadgets">
          <SectionHeader
            sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.gadgets}
            weblink={siteConfig.mweburl + `/tech/mobile-phones`}
          />
          <GadgetSlider gadgetsData={gadgetsData} />
        </div>

        {<AdCard mstype="mrec2" adtype="dfp" classtype="mrbottom" />}
        <div>
          {widgetData && widgetData.sections && widgetData.sections.length > 0
            ? widgetData.sections.map((section, index) => {
                return (
                  <React.Fragment>
                    <div>
                      {index > 1 && index % 2 == 0 ? <AdCard adtype={"ctn"} mstype={"ctnbighome"} /> : null}
                      {this.createSectionLayout({
                        data: section,
                        index: index,
                      })}
                    </div>
                  </React.Fragment>
                  //
                );
              })
            : null}
        </div>
        <div>
          {recommended &&
            recommended.trending &&
            recommended.trending.items &&
            this.createSectionLayout({
              data: recommended.trending,
            })}
        </div>
        <div>
          {recommended &&
          recommended.trendingtopics &&
          recommended.trendingtopics.length > 0 &&
          recommended.trendingtopics[0] ? (
            <div className="row box-item">
              <div className="col12 trending-bullet">
                <KeyWordCard
                  items={recommended.trendingtopics[0].items}
                  secname={recommended.trendingtopics[0].secname}
                />
              </div>
            </div>
          ) : null}
        </div>
        <div>{this.fetchBrandSlider()}</div>
        {/* Breadcrumb  */}
        {widgetData && typeof widgetData.breadcrumb != "undefined" ? (
          <ErrorBoundary>
            <Breadcrumb items={widgetData.breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : (
          ""
        )}
      </div>
    ) : (
      /* fake card for home page */
      <FakeListing />
    );
  }
}

GadgetsNow.fetchData = function({ dispatch, query, params, router }) {
  //This is pagetype is set in server side because we want to show top drawer initially to stop the fluctuating on homepage
  dispatch(setPageType("home"));

  if (params && typeof params === "object") {
    params.pageType = "gnhome";
  } else {
    params = {
      pageType: "gnhome",
    };
  }

  return dispatch(fetchListDataIfNeeded(params, query, router)).then(data => {
    // fetchDesktopHeaderPromise(dispatch);
    const pwaMeta = data && data.payload && data.payload[0] && data.payload[0].pwa_meta;
    if (pwaMeta) {
      updateConfig(pwaMeta, dispatch, setParentId);
    }

    return data;
  });
};

GadgetsNow.fetchBranchListData = function({ dispatch, query, params, router, category }) {
  return dispatch(fetchBrandListDataIfNeeded(params, query, router, category));
};

GadgetsNow.propTypes = {};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    home: state.home,
    articlelist: state.articlelist,
    app: state.app,
  };
}

const TopSection = props => {
  const { items, section, minitv, isFrmApp, location } = props;
  return items.map((item, index) => {
    if (
      item.tn == "news" ||
      item.tn == "lb" ||
      item.tn == "moviereview" ||
      item.tn == "movieshow" ||
      item.tn == "video" ||
      item.tn == "poll" ||
      item.tn == "photo"
    ) {
      return (
        <ErrorBoundary key={item.id + index}>
          <GridCardMaker
            card={item}
            cardType={index < 2 ? "lead" : "horizontal"}
            columns={index < 2 ? "6" : ""}
            noLazyLoad={index < 2 ? true : false}
            key={index}
            keyName={index}
            imgsize="midthumb"
          />
          {/* MINITV */}
          {index === 6 &&
          minitv &&
          // router.location.pathname.indexOf("/tech") < 0 &&
          minitv.hl &&
          !isFrmApp ? (
            <ErrorBoundary>
              <MiniTVCard minitv={minitv} className="minitvhomepage" location={location} />
            </ErrorBoundary>
          ) : null}
        </ErrorBoundary>
      );
    } else if (item.tn == "ad" && !item.offads) {
      // for handling platforms i.e. if we want to show ad only in mobile or desktop
      return typeof item.platform == "string" && item.platform != process.env.PLATFORM ? null : (
        <li key={index} className="news news-card horizontal col con_ads">
          <AdCard mstype={item.mstype} adtype={item.type} ctnstyle={item.mstype} />
        </li>
      );
    }
  });
};

export default connect(mapStateToProps)(GadgetsNow);
