/* eslint-disable indent */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { connect } from "react-redux";
import Parser from "html-react-parser";
import {
  fetchGadgetListDataIfNeeded,
  fetchNextGadgetListDataIfNeeded,
} from "../../../../actions/gn/gadgetlist/gadgetlist";
import { PageMeta, SeoSchema } from "../../../../components/common/PageMeta";
import { setParentId, setPageType } from "../../../../actions/config/config";
import Breadcrumb from "../../../../components/common/Breadcrumb";

import "../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../components/common/css/GadgetNow.scss";
import "../../../../components/common/css/desktop/gn_gadgetList.scss";
import "../../../../components/common/css/gn_gadgetList.scss";
import "../../../../components/common/css/commonComponents.scss";

import GadgetWidget from "../../../../components/common/Gadgets/GadgetWidget/GadgetWidget";
import GadgetFilterToolMobile from "../../../../components/common/Gadgets/GadgetFilterToolMobile";

import { fetchFilterListDataIfNeeded } from "../../../../actions/gn/gadgetlist/gadgetfiltertool";
import { AnalyticsGA } from "../../../../components/lib/analytics/index";

import PriceTable from "../../../../components/common/Gadgets/PriceTable/PriceTable";
import CategoryList from "../../../../components/common/Gadgets/CategoryList/CategoryList";
import TechKeywords from "../../../../components/common/Gadgets/TechKeywords";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
// eslint-disable-next-line import/named
//import { PageMeta } from "../../components/PageMeta/PageMeta";
import { _getStaticConfig, getKeyByValue, updateConfig, _isCSR } from "../../../../utils/util";
import { getDeviceObject } from "../../../utils/gadgets_util";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";

import SearchModule from "../../../../components/common/Gadgets/SearchModule/SearchModule";
import SvgIcon from "../../../../components/common/SvgIcon";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import Ads_module from "../../../../components/lib/ads/index";

const siteConfig = _getStaticConfig();
let deviceAddedforCompare = [];
let applyButtonClicked = false;

class Gadgetlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      category: "",
      deviceInfo: "",
      isCompareClicked: false,
      suggestedDevices: "",
      updatedDeviceInfo: "",
      sortCriteria: {
        recency: "popular",
        rating: "asc",
        price: "asc",
        active: "popular",
      },
    };
    this.config = {
      isFetchingNext: false,
    };
  }

  componentDidMount() {
    const { dispatch, params, router, gadgetsData } = this.props;
    const { query } = this.props.location;
    // this.setState({
    //   category: params.category,
    // });
    // const _this = this;

    Gadgetlist.fetchData({ dispatch, query, params, router }).then(data => {
      let pwa_meta = null;
      if (gadgetsData !== null) {
        pwa_meta = gadgetsData.pwa_meta;
      } else if (data) {
        pwa_meta = data && data.payload && data.payload.gadgetListData && data.payload.gadgetListData.pwa_meta;
      }
      if (pwa_meta && typeof pwa_meta === "object") {
        // set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        // fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    // Gadgetlist.fetchData({ dispatch, query, params, router }).then(data => {
    //   // prefill fiter
    //   this.prefillfilter();
    // });

    setTimeout(() => {
      this.prefillfilter();
      const activeLI = document.querySelector("#wdt-inline-gadgets li.active");
      if (activeLI) {
        document.querySelector("#wdt-inline-gadgets .scroll").scrollLeft = activeLI.offsetLeft;
      }
    }, 3000);

    dispatch(setPageType("gadgetlist"));

    updateConfig(gadgetsData && gadgetsData.pwa_meta, dispatch, setParentId);
  }

  capitalize = s => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  techKeywordHandler = () => {
    //  alert("ss");
    this.restForm();
    setTimeout(() => {
      this.prefillfilter();
    }, 3000);
  };

  setDataType = (tabType, tabVal) => {
    // debugger
    const { dispatch, params, query, router } = this.props;
    let {
      sortCriteria: { recency, rating, price },
    } = this.state;

    // const dataType = obj.currentTarget.id;

    //    this.dataType = dataType;
    // let category=this.state.category;
    const category = params.category;
    let filters = params && params.filters;
    //  params ? (params.category = category) : null;

    const sortData = {
      recency: {
        popular: "popular",
        latest: "latest",
      },
      rating: {
        asc: "desc",
        desc: "asc",
      },
      price: {
        asc: "desc",
        desc: "asc",
      },
    };

    let ratingVal = rating;
    let priceVal = price;
    let sortStr = "";
    let filterPath = "";
    const filterText = "/filters/";

    if (tabType == "rating") {
      ratingVal = sortData.rating[rating];
      sortStr = `${tabType}-${ratingVal}`;
    } else if (tabType == "price") {
      priceVal = sortData.price[price];
      sortStr = `${tabType}-${priceVal}`;
    } else {
      recency = tabType;
    }

    if (recency == "latest") {
      sortStr = "latest";
    }

    const activeVal = tabType;

    // this.setState({
    //   sortbystate: `sort=${dataType}`,
    // });

    /*     if (filters && sortStr) {
      // sortStr = `${filters}`;
      let filtersArr = "";
      if (filters.includes("&sort=")) {
        filtersArr = filters.split("&sort=");
      } else {
        filtersArr = filters.split("sort=");
      }

      if (filtersArr.length > 1) {
        filtersArr.pop();
      }

      filters = filtersArr[0];
      if (filters) {
        filters += "&";
      }

      filterPath = `${filterText}${filters}sort=${sortStr}`;
    } else {
      if (sortStr) {
        filterPath = `${filterText}sort=${sortStr}`;
      } else {
        filterPath = `${filterText}`;
      }
    } */

    if (filters) {
      // sortStr = `${filters}`;
      let filtersArr = "";
      if (filters.includes("&sort=")) {
        filtersArr = filters.split("&sort=");
      } else {
        filtersArr = filters.split("sort=");
      }

      if (filtersArr.length > 1) {
        filtersArr.pop();
      }

      filters = filtersArr[0];
      // if (filters == `brand=${params.brand}`) {
      //   //if only single brand is there in filter
      //   filterText = "";
      //   filters = params.brand;
      // }

      if (filters && sortStr) {
        filters += "&";
      }

      if (sortStr) {
        filterPath = `${filterText}${filters}sort=${sortStr}`;
      } else {
        filterPath = `${filterText}${filters}`;
      }
    } else if (sortStr) {
      if (params.brand) {
        filterPath = `${filterText}brand=${params.brand}&sort=${sortStr}`;
      } else {
        filterPath = `${filterText}sort=${sortStr}`;
      }
    } else {
      filterPath = `${filterText}`;
    }

    if (tabType == "popular") {
      sortStr = "";
      if (
        router &&
        router.params &&
        router.params.brand &&
        !router.params.brand.includes("|") &&
        (!filters || (filters && !filters.includes("brand=")))
      ) {
        // if there is only a single brand in fliter

        filterPath = `/${router.params.brand.toLowerCase()}`;
      } else if (!filters) {
        filterPath = "";
      }
    }

    if (router && router.location.pathname.indexOf("articlelist") < 0) {
      this.props.router.push(`/tech/${category}${filterPath}`);
    } else {
      return dispatch(fetchGadgetListDataIfNeeded(params, query, router, tabType));
    }

    this.setState({
      sortCriteria: {
        recency,
        rating: ratingVal,
        price: priceVal,
        active: activeVal,
        activeSort: sortStr,
      },
    });
  };

  componentWillReceiveProps(newProps, newState) {
    // debugger;
    const _this = this;
    //   const previousCategory = _this.props.params.category;
    //  const newCategory = newProps.params.category;
    const { dispatch, query, params, router, gadgetsData } = newProps;
    const dataType = document.querySelector(".tabs-square li.active")
      ? document.querySelector(".tabs-square li.active").id
      : null;
    if (
      newProps.params.filters !== this.props.params.filters ||
      newProps.params.brand !== this.props.params.brand ||
      newProps.params.category !== this.props.params.category ||
      router.location.pathname != this.props.location.pathname
      // newProps.location.pathname.indexOf('upcoming') > -1
    ) {
      const brandSelector = document.querySelectorAll(".filtercontent-brand input:checked");
      const brands = [];
      if (brandSelector) {
        brandSelector.forEach(item => brands.push(_this.capitalize(item.value)));
      }

      if (!params.brand && brands.length > 0) {
        params.brand = brands.join("|");
      }

      // if (!applyButtonClicked) {
      //   setTimeout(() => {
      //     this.restForm();
      //     this.prefillfilter();
      //     applyButtonClicked = false;
      //   }, 1000);
      // } else {
      //   applyButtonClicked = false;
      // }

      Gadgetlist.fetchData({ dispatch, query, params, router, dataType }).then(data => {
        let pwa_meta = null;
        if (gadgetsData !== null) {
          pwa_meta = gadgetsData.pwa_meta;
        } else if (data) {
          pwa_meta = data && data.payload && data.payload.gadgetListData && data.payload.gadgetListData.pwa_meta;
        }
        if (pwa_meta && typeof pwa_meta === "object") {
          Ads_module.setSectionDetail(pwa_meta);
          // fire ibeat
          pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
          updateConfig(pwa_meta, dispatch, setParentId);
        }
      });
    }
  }

  prefillfilter() {
    // debugger;
    const { params, router } = this.props;
    // let checkBox = document.getElementsByTagName('input');
    const checkBox = document.querySelectorAll("input");
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == "radio" && params.filters && params.filters.indexOf("sort=") > -1) {
        // set radio button value
        const filterlist = params.filters.split("&");
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf("sort=") + 5;
          const getvalue = filterlist[i].substr(getIndex);
          if (getvalue == checkBox[b].value) checkBox[b].click();
        }
      } else if (
        checkBox[b].type == "radio" &&
        (!params.filters || params.filters.indexOf("sort=") < 0) &&
        checkBox[b].value == "popular"
      ) {
        checkBox[b].checked = "checked";
      }
      if (
        checkBox[b].type == "checkbox" &&
        checkBox[b].getAttribute("datavalues") == "upcoming" &&
        router &&
        router.location &&
        router.location.pathname.indexOf("upcoming") > -1
      ) {
        // set Upcoming checkbox button value
        checkBox[b].checked = "checked";
      } else if (
        checkBox[b].type == "checkbox" &&
        params.brand &&
        checkBox[b].getAttribute("datavalues") == "brand" &&
        checkBox[b].value == params.brand
      ) {
        // set brand checkbox button value
        checkBox[b].checked = "checked";
      } else if (checkBox[b].type == "checkbox" && params.filters) {
        // set all filters checkbox button value
        // set radio button value
        const filterlist = params.filters.split("&");
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf("=") + 1;
          const getfiltecriteria = filterlist[i].substr(0, getIndex - 1);
          const getvalue = filterlist[i].substr(getIndex);
          if (getfiltecriteria == checkBox[b].getAttribute("datavalues") && getvalue.indexOf(checkBox[b].value) > -1)
            checkBox[b].checked = "checked";
        }
      }
    }
  }

  setCategoryfromParent = obj => {
    // debugger;
    obj.preventDefault();

    const { dispatch, params, query, router } = this.props;
    const deviceType = obj.currentTarget.id;
    const seoName = getKeyByValue(gadgetsConfig.gadgetMapping, deviceType);

    this.props.router.push(`/tech/${seoName}`);
    this.restForm();
    const categoryoverride = seoName;
    this.setState({
      category: seoName,
      deviceInfo: "",
      sortCriteria: {
        recency: "popular",
        rating: "asc",
        price: "asc",
        active: "popular",
      },
    });
    deviceAddedforCompare = []; // make global variable blank for each new devices
    const tabparams = { seoName };
    dispatch(fetchFilterListDataIfNeeded(tabparams, query, router, categoryoverride));
    dispatch(fetchGadgetListDataIfNeeded(tabparams, query, router, categoryoverride));
  };

  readMore(event) {
    const { dispatch, query, params, router, value, location } = this.props;
    event.preventDefault();

    const curpg = this.state.curpg + 1;
    this.setState({ curpg });

    const _this = this;
    if (_this.config.isFetchingNext == false) {
      _this.config.isFetchingNext = !_this.config.isFetchingNext;
      Gadgetlist.fetchNextListData({ dispatch, query, params, router, curpg }).then(data => {
        const _data = data ? data.payload : {};
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        AnalyticsGA.event({ category: "Web GS", action: "GadgetsList", label: "morepage" });

        if (typeof AnalyticsGA !== "undefined" && typeof AnalyticsGA.pageview !== "undefined") {
          AnalyticsGA.pageview(window.location.origin + location.pathname);
        }
        // AnalyticsGA.event({ category: "Wap_GS", action: "GadgetsList", label: "morepage" });
        // AnalyticsGA.pageview(location.origin + _this.generateListingLink());
        // Ads_module.refreshAds(['fbn']);//refresh fbn ads when next list added
        // fire ibeat for next
        // data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { params, location, routeParams } = this.props;

    if (prevProps.params.category !== params.category) {
      this.restForm();
      this.prefillfilter();
    } else if (location.pathname != prevProps.location.pathname && !applyButtonClicked) {
      setTimeout(() => {
        this.prefillfilter();
        applyButtonClicked = false;
      }, 1000);
    }
  }

  OverLayScreen(obj) {
    document.getElementById(obj.target.parentElement.children[1].id).style.height = "100%";
    applyButtonClicked = false;
    // if (document.location.pathname.includes("sort=latest")) {
    //   document.querySelector("#filterby-sortcontent #radio-latest").checked = true;
    // }

    this.setState({ isCompareClicked: true });
  }

  CloseOverLayScreen(obj) {
    document.getElementById(obj.target.parentElement.parentElement.parentElement.children[1].id).style.height = "0";
  }

  AddToCompare(e) {
    const { deviceInfo } = this.state;

    const seoName = e.currentTarget.getAttribute("seo");
    const deviceName = e.currentTarget.getAttribute("itemname");
    const className = e.currentTarget.classList;
    const item = {
      seoName,
      deviceName,
    };

    if (deviceAddedforCompare.length == 3 && !className.contains("highlighted")) {
      alert("Can't added more than 3 devices!!");
      return false;
    }

    if (className && className.contains("highlighted")) {
      className.remove("highlighted");
      deviceAddedforCompare = deviceAddedforCompare.filter(obj => obj.seoName !== seoName);
    } else {
      className.add("highlighted");
      deviceAddedforCompare.push(item);
    }

    // console.log("deviceAddedforCompare", deviceAddedforCompare);

    // const item = {
    //   seoName,
    //   deviceName,
    // };

    // let deviceInfoCopy = [];
    // if (deviceInfo) {
    //   deviceInfoCopy = [...deviceInfo];
    // }

    // deviceInfoCopy.push(item);

    this.setState({ deviceInfo: deviceAddedforCompare });
  }

  suggestedDevicesCallBack = (item, deviceListInfo) => {
    let { suggestedDevices, updatedDeviceInfo } = this.state;
    suggestedDevices = item;
    updatedDeviceInfo = deviceListInfo;
    this.setState({
      suggestedDevices,
      updatedDeviceInfo,
    });
  };

  restForm(obj) {
    // clear check box

    const checkBox = document.getElementsByTagName("input");
    if (checkBox && checkBox.length > 0) {
      for (let b = 0; b < checkBox.length; b++) {
        if (checkBox[b].type == "radio") {
          checkBox[b].checked = false;
        }
        if (checkBox[b].type == "checkbox") {
          checkBox[b].checked = false;
        }
      }
    }
    if (document.getElementById("filterapplied")) document.getElementById("filterapplied").innerHTML = "";
  }

  filterApply(obj) {
    const { router } = this.props;
    const filterkeyword = [];
    let filtercriteria = "";
    let sortby = "";
    let filterby = "";
    let upcoming = false;
    const checkBox = document.getElementsByTagName("input");
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == "radio") {
        checkBox[b].checked && checkBox[b].value != "popular" ? (sortby += `sort=${checkBox[b].value}`) : null;
      }
      // if(checkBox[b].type=='checkbox' && checkBox[b].checked==true && checkBox[b].getAttribute("datavalues")=='upcoming'){
      //     prepend=checkBox[b].getAttribute("datavalues");
      // }
      // else
      if (checkBox[b].type == "checkbox" && checkBox[b].checked == true) {
        if (checkBox[b].getAttribute("datavalues") == "upcoming") {
          upcoming = true;
        } else if (filterkeyword.includes(checkBox[b].getAttribute("datavalues"))) {
          filterby += `%7C${checkBox[b].value}`;
        } else {
          filterkeyword.push(checkBox[b].getAttribute("datavalues"));
          if (filterby) {
            filterby += "&";
          }
          // filterby += filterby != "" || sortby != "" ? "&" : "";
          filterby += `${checkBox[b].getAttribute("datavalues")}=${checkBox[b].value}`;
        }
      }
    }
    // handle if single brand is selected no filters parms added
    if (
      sortby == "" &&
      filterkeyword.length > 0 &&
      filterkeyword.length < 2 &&
      filterkeyword[0].toLowerCase() == "brand" &&
      filterby != "" &&
      filterby.indexOf("%7C") < 0
    ) {
      const temparray = filterby.split("=");
      filtercriteria += `/${temparray[1] && temparray[1].toLowerCase()}`;
    } else {
      /* if (sortby != "") filtercriteria += sortby; // append sorting if required
      if (filterby != "") filtercriteria += filterby; // append filter if required
      if (sortby != "" || filterby != "") filtercriteria = "/filters/" + filtercriteria; */
      const filtertext = "/filters/";
      // if (filterby) {
      //   filtercriteria = filterby;
      // }
      // if (sortby) {
      //   filtercriteria += "&" + sortby;
      // }
      if (filterby && sortby) {
        sortby = `&${sortby}`;
      }

      if (filterby || sortby) {
        filtercriteria = filtertext + filterby + sortby;
      }
    }

    let routepath = router.location.pathname;
    if (upcoming) routepath = `/tech/upcoming-${this.props.params.category}`;
    else routepath = `/tech/${this.props.params.category}`;
    const array = routepath.split("/");
    const routearray = [];
    if (array.length > 2) {
      // Re Manipulate routepath if route have already filters

      for (let i = 0; i < array.length; i++) {
        if (i < 3) routearray.push(array[i]);
      }
      const updateroute = routearray.join("/");
      routepath = updateroute;
    }

    applyButtonClicked = true;

    if (filtercriteria != "") {
      this.props.router.push(routepath + filtercriteria);
    } else {
      this.props.router.push(routepath);
    }
    // close overlay
    document.getElementById("FilterCloseOverLayScreen").click();
    // document.getElementById("resetform").click();
    //  AnalyticsGA.event({ category: "Wap_GS", action: "GDS_category_filter", label: filtercriteria });
  }

  render() {
    const { gadgetsData, isFetching, params, router, urlParams } = this.props;
    const { deviceInfo, isCompareClicked } = this.state;
    // console.log("this.props", this.props);
    const pagetype = "gadgetlist";
    const sortCriteria = { ...this.state.sortCriteria };

    const breadcrumb = gadgetsData && gadgetsData.breadcrumb;

    const heading = (gadgetsData && gadgetsData.brandheader) || "";

    const deviceCount = deviceInfo && deviceInfo.length;

    const pagination = (gadgetsData && gadgetsData.pg) || "";
    const navigationfilter = gadgetsData && gadgetsData.navigationfilter;
    const pwaMeta = (gadgetsData && gadgetsData.pwa_meta) || "";
    const gadgetsArray = (gadgetsData && gadgetsData.gadgets) || "";
    const gadgetsCount = gadgetsData && gadgetsData.gadgets && gadgetsData.gadgets.length;
    const atfContent = gadgetsData && gadgetsData.seo && gadgetsData.seo.atfcontent;
    const btfContent = gadgetsData && gadgetsData.seo && gadgetsData.seo.btfcontent;
    let activeGadget = params && params.category;
    activeGadget = activeGadget ? gadgetsConfig.gadgetMapping[activeGadget] : "";

    let deviceInfoParam = {};
    if (deviceInfo && isCompareClicked) {
      deviceInfo.length > 0 &&
        deviceInfo.forEach((item, index) => {
          deviceInfoParam[`device${index + 1}`] = {
            isVisible: true,
            searchResult: "",
            userInput: "",
            userInputHtml: item.deviceName,
            seoName: item.seoName,
          };
        });

      for (let i = deviceCount; i < 3; i++) {
        deviceInfoParam[`device${i + 1}`] = {
          isVisible: true,
          searchResult: "",
          userInput: "",
          userInputHtml: "",
          seoName: "",
        };
      }
    } else {
      deviceInfoParam = getDeviceObject(3);
    }

    return (
      <React.Fragment>
        <div className="row gadgetlist_body pwa" {...SeoSchema({ pagetype }).attr().itemList}>
          <div className="col12">
            <div className="gl_top_bar">
              <div className="section">
                <div className="top_section">
                  <h1>
                    <span> {heading}</span>
                  </h1>
                </div>
              </div>
              {pwaMeta ? PageMeta(pwaMeta) : null}
              <div className="select-inline-gadgets">
                <CategoryList clicked={this.setCategoryfromParent.bind(this)} activeGadget={activeGadget} />
              </div>
              {atfContent ? Parser(atfContent) : ""}
              <TechKeywords data={navigationfilter} params={params} clicked={this.techKeywordHandler} />
            </div>
          </div>
          {pwaMeta ? PageMeta(pwaMeta) : null}

          {/* <GadgetWidget
          _category={this.state.category}
          params={this.props.params}
          isFetching={isFetching}
          router={router}
          pagetype={pagetype}
          gadgetList={gadgetList}
          dispatch={this.props.dispatch}
          navigationfilter={navigationfilter}
        /> */}
          <div className="gl_list_mobiles col12">
            <GadgetWidget
              _category={this.state.category}
              params={this.props.params}
              isFetching={isFetching}
              router={router}
              pagetype={pagetype}
              gadgetList={gadgetsArray}
              dispatch={this.props.dispatch}
              navigationfilter={navigationfilter}
              ClickAddToCompare={this.AddToCompare.bind(this)}
              setDataType={this.setDataType}
              sortCriteria={sortCriteria}
              heading={heading}
            />
          </div>

          {gadgetsArray &&
          pagination &&
          pagination.cp &&
          pagination.tp &&
          parseInt(pagination.cp) < parseInt(pagination.tp) ? (
            <div className="more-list">
              <a onClick={this.readMore.bind(this)} className="more-btn" href={`/tech/${params.category}`}>
                {siteConfig.locale.tech.loadmore}
              </a>
            </div>
          ) : (
            ""
          )}
        </div>
        {btfContent && (
          <div className="row seo-btf-content pwa">
            <div
              className="col12 pd0"
              dangerouslySetInnerHTML={{
                __html: btfContent,
              }}
            />
          </div>
        )}

        <PriceTable data={gadgetsData} params={params} />

        {breadcrumb ? (
          <ErrorBoundary>
            <Breadcrumb items={breadcrumb.div.ul} />
          </ErrorBoundary>
        ) : null}

        {_isCSR()
          ? ReactDOM.createPortal(
              <div className="overlay_compare_filter" data-exclude="amp">
                <div id="Tab1">
                  <div
                    id="btn-compare"
                    className={`btn-compare btn${deviceCount ? " active" : ""}`}
                    data-attr="overlay_compare_btn1"
                    onClick={this.OverLayScreen.bind(this)}
                  >
                    <span>
                      <SvgIcon name="gn-compare-icon" />
                      <b className="compare_count" id="compare_count">
                        {deviceCount}
                      </b>
                    </span>
                  </div>
                  <div id="CompareTool" className="overlay b">
                    <div className="backToScreen">
                      <a href="javascript:void(0)" className="bck" onClick={this.CloseOverLayScreen.bind(this)}>
                        <SvgIcon name="back" />
                      </a>
                      {siteConfig.locale.tech.comparetxt}
                    </div>
                    <div className="overlay-content">
                      <div className="overlay_scroll">
                        {isCompareClicked && (
                          <SearchModule
                            activeGadget={activeGadget}
                            deviceInfo={deviceInfoParam}
                            suggestedDevicesCallBack={this.suggestedDevicesCallBack}
                            //countCallBack={() => {}}
                            //addedSuggestedDevice={addedDevicefromSuggestion}
                            // searchBoxStatus={() => {
                            //   this.setState({ showSearchBox: false });
                            // }}
                          />
                        )}

                        {/* <CompareTool _category={this.props.category} pagetype={pagetype} {...this.props} /> */}
                        {/* <div className="box-content">
                      {item && item[0] && item[0].popularGadgetPair && item[0].popularGadgetPair.compare ? (
                        <span>{GadgetCompare(item[0].popularGadgetPair.compare)}</span>
                      ) : null}
                    </div> */}
                      </div>
                    </div>
                  </div>
                </div>
                <div id="Tab2">
                  <div
                    className="btn-filter btn"
                    data-attr="overlay_compare_btn2"
                    onClick={this.OverLayScreen.bind(this)}
                  >
                    <span>
                      <SvgIcon name="gn-filter" />
                    </span>
                  </div>
                  <div id="filtertool" className="overlay">
                    <div className="backToScreen">
                      <a
                        id="FilterCloseOverLayScreen"
                        href="javascript:void(0)"
                        className="bck"
                        onClick={this.CloseOverLayScreen.bind(this)}
                      >
                        <SvgIcon name="back" />
                      </a>
                      FILTER
                      <span onClick={this.filterApply.bind(this)} className="btn_filter apply">
                        APPLY
                      </span>
                      <span id="resetform" onClick={this.restForm.bind(this)} className="btn_filter clear">
                        CLEAR
                      </span>
                    </div>
                    <div className="overlay-content">
                      {/* <GadgetFilterTool pagetype={pagetype} techgadgetfilter={techgadgetfilter}/> */}
                      <GadgetFilterToolMobile
                        category={this.state.category}
                        params={this.props.params}
                        isFetching={isFetching}
                        router={router}
                        pagetype={pagetype}
                        dispatch={this.props.dispatch}
                      />
                    </div>
                  </div>
                </div>
              </div>,
              document.getElementById("share-container-wrapper"),
            )
          : null}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.gadgetlist,
  };
}

Gadgetlist.fetchData = ({ dispatch, params, query, router }) => {
  dispatch(setPageType("gadgetlist"));
  return dispatch(fetchGadgetListDataIfNeeded(params, query, router)).then(data => {
    const { payload } = data;
    if (payload) {
      const pwaMeta = payload && payload.gadgetListData && payload.gadgetListData.pwa_meta;
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

Gadgetlist.fetchNextListData = ({ dispatch, params, query, router, curpg }) =>
  dispatch(fetchNextGadgetListDataIfNeeded(params, query, router, curpg));

export default connect(mapStateToProps)(Gadgetlist);
// export default Gadgetlist;
