/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */
import React, { PureComponent } from "react";

import { connect } from "react-redux";
import PropTypes, { array } from "prop-types";
import { browserHistory } from "react-router";
import Helmet from "react-helmet";
import AnchorLink from "../../../../../components/common/AnchorLink";
import {
  fetchDataIfNeeded,
  fetchPhotoDataIfNeeded,
  fetchVideoDataIfNeeded,
  updateGadgetData,
} from "../../../../../actions/gn/comparision/List/index";
import Slider from "../../../../../components/desktop/Slider/Gadget";
import "../../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../../components/common/css/GadgetNow.scss";
import "../../../../../components/common/css/desktop/gn_compare_list.scss";
import Ads_module from "./../../../../../components/lib/ads/index";
import "../../../../../components/common/css/gn_compare_list.scss";
import gadgetsConfig from "../../../../../utils/gadgetsConfig";
const gadgetMapping = (gadgetsConfig && gadgetsConfig.gadgetMapping) || "";

import {
  _getStaticConfig,
  getKeyByValue,
  isMobilePlatform,
  getAffiliateUrl,
  getBuyLink,
  getAffiliateTags,
  updateConfig,
} from "../../../../../utils/util";
import SearchModule from "../../../../../components/common/Gadgets/SearchModule/SearchModule";
import SectionLayoutMemo from "../../../../desktop/home/SectionLayout";
import ErrorBoundary from "../../../../../components/lib/errorboundery/ErrorBoundary";
import AmazonAffiliate from "../../../../../components/common/Gadgets/AmazonAffiliate/AmazonAffiliate";
import Breadcrumb from "../../../../../components/common/Breadcrumb";
import SectionHeader from "../../../../../components/common/SectionHeader/SectionHeader";
import GadgetsCompare from "../../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";
import { PageMeta } from "../../../../../components/common/PageMeta";
import AdCard from "../../../../../components/common/AdCard";
import SvgIcon from "../../../../../components/common/SvgIcon";
import { setPageType, setParentId } from "../../../../../actions/config/config";
import { setIbeatConfigurations } from "../../../../../components/lib/analytics/src/iBeat";
import CategoryList from "../../../../../components/common/Gadgets/CategoryList/CategoryList";

const siteConfig = _getStaticConfig();
const deviceText = (siteConfig && siteConfig.locale && siteConfig.locale.tech.gadgetSliderConfig) || {};
class Comparison extends PureComponent {
  constructor(props) {
    super(props);
    const activeDevice =
      props.routeParams && props.routeParams.device ? props.routeParams.device : props.location.query.category;
    const deviceToSearch = activeDevice ? gadgetMapping && gadgetMapping[activeDevice] : "mobile";
    this.state = {
      suggestedDevices: "",
      activeGadget: deviceToSearch,
      addedDevicefromSuggestion: "",
      updatedDeviceInfo: "",
      // itemAddedToCompare: 0,
    };
  }

  componentDidMount() {
    const { dispatch, query, params, comparisionList } = this.props;
    Comparison.fetchData({ dispatch, query, params }).then(response => {
      const { data } = comparisionList;
      const pwa_meta = data && data.listData && data.listData.compareData && data.listData.compareData.pwa_meta;
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
      dispatch(setPageType("comparelist"));
    });

    Comparison.fetchVideoData({ dispatch, query, params }).then(data => { });
    Comparison.fetchPhotoData({ dispatch, query, params }).then(data => { });
  }

  componentDidUpdate(prevProps) {
    const { dispatch, query, params, comparisionList, location } = this.props;
    const { data } = comparisionList;
    const pwa_meta = data && data.listData && data.listData.compareData && data.listData.compareData.pwa_meta;
    if (pwa_meta && typeof pwa_meta == "object") {
      //set section and subsection in window
      Ads_module.setSectionDetail(pwa_meta);
      //fire ibeat
      pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      updateConfig(pwa_meta, dispatch, setParentId);
    }
    if (location && prevProps && location.pathname != prevProps.location.pathname) {
      setTimeout(() => {
        Ads_module.refreshAds(["mrec2"]);
      }, 2000);
    }
  }

  preventDefaultAndPropagation = e => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    if (e.preventDefault) {
      e.preventDefault();
    }
  };

  addFromSuggestions = deviceInfo => e => {
    this.preventDefaultAndPropagation(e);
    let { addedDevicefromSuggestion } = this.state;

    addedDevicefromSuggestion = {
      Product_name: deviceInfo.name,
      seoname: deviceInfo.uname,
    };
    this.setState({
      addedDevicefromSuggestion,
    });
  };

  switchDeviceHandler = obj => {
    obj.preventDefault();
    const deviceType = obj.currentTarget.id;
    const { dispatch } = this.props;

    const seoName = this.getKeyByValue(deviceType);
    this.props.router.push(`/tech/compare-${seoName}`);

    this.setState({
      activeGadget: deviceType,
      suggestedDevices: "",
    });

    dispatch(updateGadgetData(deviceType));
  };

  getKeyByValue = value => {
    const gadgets = gadgetMapping;
    for (const key in gadgets) {
      if (Object.prototype.hasOwnProperty.call(gadgets, key)) {
        if (gadgets[key] === value) return key;
      }
    }
    return "";
  };

  onGadgetItemClick = item => e => {
    this.preventDefaultAndPropagation(e);
    const { activeGadget } = this.state;
    // const { tab } = this.state;
    const seoName = this.getKeyByValue(activeGadget);
    window.location.href = `/tech/${seoName}/${item.lcuname}`;
  };

  onAmazonBuyButtonClick = card => e => {
    this.preventDefaultAndPropagation(e);

    const amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.CL);
    // const affiliateUrl = getAffiliateUrl(amazonCard, amazonTag);
    const affiliateUrl = getBuyLink({ data: card, tag: amazonTag });

    const win = window.open(affiliateUrl, "_blank");
    win.focus();
  };

  suggestedDevicesCallBack = (item, deviceListInfo) => {
    let { suggestedDevices, updatedDeviceInfo } = this.state;
    suggestedDevices = item;
    updatedDeviceInfo = deviceListInfo;
    this.setState({
      suggestedDevices,
      updatedDeviceInfo,
    });
  };

  render() {
    const { comparisionList } = this.props;

    const { activeGadget, suggestedDevices, addedDevicefromSuggestion } = this.state;

    const seoName = this.getKeyByValue(activeGadget);
    let amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.CD);
    amazonTag += "-21";
    const prepareDeviceURL = `${process.env.API_ENDPOINT}/tech/compare-${seoName}`;
    const { data } = comparisionList;

    const deviceDataCopy = data && data.listData && data.listData.compareData ? { ...data.listData.compareData } : "";
    const pwa_meta = deviceDataCopy && deviceDataCopy.pwa_meta;

    const breadcrumb = data && data.listData && data.listData.compareData && data.listData.compareData.breadcrumb;

    let arrPopularTwoDevices = [];
    let arrLatestTwoDevices = [];
    let categoryInFeed = "";

    if (deviceDataCopy && deviceDataCopy.popularGadgetPair && deviceDataCopy.popularGadgetPair.compare) {
      if (Array.isArray(deviceDataCopy.popularGadgetPair.compare)) {
        arrPopularTwoDevices = deviceDataCopy.popularGadgetPair.compare.filter(item => item.product_name.length < 4);
        categoryInFeed = arrPopularTwoDevices && arrPopularTwoDevices[0] && arrPopularTwoDevices[0].category;
      }
    }

    if (deviceDataCopy && deviceDataCopy.recentcomp && deviceDataCopy.recentcomp.compare) {
      if (Array.isArray(deviceDataCopy.recentcomp.compare)) {
        arrLatestTwoDevices = deviceDataCopy.recentcomp.compare.filter(item => item.product_name.length < 4);
      }
    }

    const regionalDeviceName = categoryInFeed ? deviceText[categoryInFeed] : "";

    return (
      <React.Fragment>
        {/* For SEO */ pwa_meta ? PageMeta(pwa_meta) : null}
        <div className="container-comparewidget pwa">
          <div className="wdt_compare-gadgets">
            <CategoryList
              clicked={this.switchDeviceHandler.bind(this)}
              activeGadget={activeGadget}
              pagetype="compare"
            />
            <h1>
              {`${regionalDeviceName} ${siteConfig.locale.tech.compare}`}
              <span className="search-suggest"> ({siteConfig.locale.tech.englishtype})</span>
            </h1>

            <div className="row-fields">
              <SearchModule
                activeGadget={activeGadget}
                suggestedDevicesCallBack={this.suggestedDevicesCallBack}
                addedSuggestedDevice={addedDevicefromSuggestion}
              />
            </div>
          </div>
          {suggestedDevices && suggestedDevices.gadget && (
            <div className="row box-item wdt_suggested_slider ui_slider">
              <SectionHeader
                sectionhead={`${siteConfig.locale.tech.suggestedDevice} ${deviceText[activeGadget]} ${siteConfig.locale.tech.ki} ${siteConfig.locale.tech.comparetxt}`}
              />
              <div className="col12">
                <div className="list-slider">
                  <Slider
                    type="comparisonGadgets"
                    size="5"
                    sliderData={suggestedDevices.gadget}
                    width="165"
                    sliderClass="trendingslider"
                    videoIntensive={false}
                    gadgetItemClick={this.onGadgetItemClick}
                    amazonBuyClick={this.onAmazonBuyButtonClick}
                    margin="10"
                    addFromSuggestions={this.addFromSuggestions}
                    isComparison={true}
                  />
                </div>
              </div>
            </div>
          )}

          <AdCard adtype="ctn" mstype="ctnTechCL" />

          <div className="wdt_popular_slider ui_slider">
            <GadgetsCompare
              type="popular"
              data={arrPopularTwoDevices}
              category={activeGadget}
              seoName={seoName}
              regionalDeviceName={regionalDeviceName}
            />
          </div>

          <div className="wdt_lstcomparsion_slider ui_slider">
            <GadgetsCompare
              type="latest"
              data={arrLatestTwoDevices}
              category={activeGadget}
              seoName={seoName}
              regionalDeviceName={regionalDeviceName}
            />
          </div>

          <AdCard mstype="mrec2" adtype="dfp" ctnstyle="mrec2" />

          <AmazonAffiliate category={activeGadget} tag={amazonTag} isslider="1" noimg="0" noh2="0" />
          {breadcrumb && (
            <ErrorBoundary>
              <Breadcrumb items={breadcrumb.div.ul} />
            </ErrorBoundary>
          )}

          {/* <div className="AL_Innov1" /> */}
        </div>
      </React.Fragment>
    );
  }
}

Comparison.fetchData = function fetchData({ dispatch, query, params }) {
  dispatch(setPageType("comparelist"));

  return dispatch(fetchDataIfNeeded({ query, params }));
};
Comparison.fetchVideoData = function fetchVideoData({ dispatch, query, params }) {
  return dispatch(fetchVideoDataIfNeeded({ query, params }));
};
Comparison.fetchPhotoData = function fetchVideoData({ dispatch, query, params }) {
  return dispatch(fetchPhotoDataIfNeeded({ query, params }));
};

Comparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    comparisionList: state.comparisionList,
    alaskaData: state.header.alaskaData,
    home: state.home,
  };
}

export default connect(mapStateToProps)(Comparison);
