import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchListDataIfNeeded } from "../../../../actions/gn/home/home";
import { setParentId, setPageType } from "../../../../actions/config/config";

import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import { defaultDesignConfigs } from "../../../defaultDesignConfigs";
import KeyWordCard from "../../../../components/common/KeyWordCard";
import GadgetSlider from "../../../../components/common/GadgetSlider";
import BrandSlider from "../../../../components/common/BrandSlider";
import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import { updateConfig, _getStaticConfig, _isFrmApp } from "../../../../utils/util";
import { getPWAMeta } from "../../../utils/gadgets_util";
import Ads_module from "./../../../../components/lib/ads/index";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import "./../../../../components/common/css/SectionWrapper.scss";
import "./../../../../components/common/css/GadgetNow.scss";
import "./../../../../components/common/css/gn_compare_list.scss";
import AdCard from "../../../../components/common/AdCard";
import { PageMeta } from "../../../../components/common/PageMeta";
import GadgetsCompareModule from "../../../../components/common/Gadgets/GadgetsCompareModule/GadgetsCompareModule";

const siteConfig = _getStaticConfig();

const widgetSequence = ["news", "reviews", "ts", "video", "photos"];

export class GadgetsNowHome extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let { dispatch, query, params, router, gadgetsNowHome } = this.props;
    let pwaMeta = getPWAMeta(gadgetsNowHome && gadgetsNowHome.data);
    GadgetsNowHome.fetchData({ dispatch, query, params, router }).then(data => {
      if (data && data.payload && data.payload.newsData) {
        pwaMeta = getPWAMeta(data.payload);
      }

      if (pwaMeta && typeof pwaMeta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwaMeta);
        //fire ibeat
        pwaMeta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwaMeta, dispatch, setParentId);
      }
    });
  }

  render() {
    const { alaskaData, home, articlelist, gadgetsNowHome, router } = this.props;
    let widgetData;
    let gadgetsData;
    let pwaMeta;

    if (gadgetsNowHome && gadgetsNowHome.data && gadgetsNowHome.data.newsData) {
      widgetData = gadgetsNowHome.data.newsData;
      gadgetsData = gadgetsNowHome.data.gadgetsData;
      pwaMeta = getPWAMeta(gadgetsNowHome.data);
    }

    const recommended = widgetData && widgetData.recommended;
    const sectionConfig = widgetData && widgetData.sectionconfig;
    const widgetItems = [];

    if (widgetData && widgetData.sections && widgetData.sections.length > 0) {
      widgetSequence.forEach((sequence, index) => {
        const section = widgetData.sections.filter(
          item => sectionConfig[sequence] && item.id == sectionConfig[sequence].msid,
        );
        if (section.length > 0) {
          widgetItems.push(<SectionLayout data={section[0]} index={index} />);
        }
        if (index > 1 && index % 2 == 0) {
          widgetItems.push(<AdCard adtype="ctn" mstype="ctnbighome" />);
        }
      });
    }

    return widgetData ? (
      <div className="body-content section-wrapper gn">
        {pwaMeta ? PageMeta(pwaMeta) : null}

        <div className="row con_gadgets">
          <SectionHeader
            sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.gadgets}
            weblink={siteConfig.weburl + `/tech/mobile-phones`}
            headingTag="h1"
          />
          <GadgetSlider gadgetsData={gadgetsData} />
        </div>
        <AdCard mstype="mrec2" adtype="dfp" classtype="mrbottom" />
        <div className="container-comparewidget pwa" data-exclude="amp">
          <SectionHeader
            sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.comparedotxt}
            weblink=""
          />
          <GadgetsCompareModule heading="h2" source="gadgetsnow" />
        </div>
        <div>
          <div className="row con_brands" data-exclude="amp">
            <SectionHeader
              sectionhead={siteConfig.locale && siteConfig.locale.tech && siteConfig.locale.tech.brands}
              weblink={""}
            />
            <BrandSlider />
          </div>
          <AdCard adtype={"ctn"} mstype={"ctnbighome"} />
        </div>

        <div>{widgetItems}</div>

        <div>
          {recommended && recommended.trending && recommended.trending.items && (
            <SectionLayout data={recommended.trending} />
          )}
        </div>
        <div>
          {recommended &&
          recommended.trendingtopics &&
          recommended.trendingtopics.length > 0 &&
          recommended.trendingtopics[0] ? (
            <div className="row box-item">
              <div className="col12 trending-bullet">
                <KeyWordCard
                  items={recommended.trendingtopics[0].items}
                  secname={recommended.trendingtopics[0].secname}
                />
              </div>
            </div>
          ) : null}
        </div>
      </div>
    ) : (
      <FakeListing />
    );
  }
}

GadgetsNowHome.fetchData = function({ dispatch, query, params, router }) {
  dispatch(setPageType("gadgethome"));
  return dispatch(fetchListDataIfNeeded(params, query, router)).then(data => {
    const pwaMeta = getPWAMeta(data && data.payload);
    if (pwaMeta) {
      updateConfig(pwaMeta, dispatch, setParentId);
    }
  });
};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    app: state.app,
    gadgetsNowHome: state.gadgetsNowHome,
  };
}

const SectionLayout = ({ data, index }) => {
  let cssClass = "row box-item";
  let widgetType = defaultDesignConfigs.reviewListingGn;
  if (data.tn === "videolist" || data.tn === "photolist") {
    cssClass += " photo_video_section row box-item";
    if (data.tn === "videolist") {
      cssClass += " video";
    }
  }

  if (data.tn == "videolist") {
    widgetType = defaultDesignConfigs.gallery;
  } else if (data.tn == "photolist") {
    widgetType = defaultDesignConfigs.gridHorizontalLeadAL;
  }

  return (
    <div className={cssClass}>
      <ErrorBoundary>
        <SectionHeader sectionhead={data.secname} weblink={data.override || data.wu || ""} />
        <GridSectionMaker type={widgetType} data={data.items} key={index} keyName={index} />
      </ErrorBoundary>
    </div>
  );
};

export default connect(mapStateToProps)(GadgetsNowHome);
