/* eslint-disable no-underscore-dangle */
import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import Slider from "../../../../components/desktop/Slider/Gadget";
import GadgetVariants from "../../../../components/common/Gadgets/VariantWidgets";
import VideoItem from "../../../../components/common/VideoItem/VideoItem";
import SpecificationAccordion from "./../../../desktop/Gadgets/Gadgetshow/SpecificationAccordion";
import "../../../../components/common/css/desktop/GadgetNow.scss";
import "../../../../components/common/css/GadgetNow.scss";
import "../../../../components/common/css/desktop/gn_productDetails.scss";
import "../../../../components/common/css/gn_productDetails.scss";

import { Link } from "react-router";

import {
  getBuyLink,
  objToQueryStr,
  _getCookie,
  _getStaticConfig,
  formatMoney,
  adsPlaceholder,
  getKeyByValue,
  getAffiliateTags,
  isLoggedIn,
  _isCSR,
  updateConfig,
} from "../../../../utils/util";
import { getGadgetPrice } from "../../../../containers/utils/gadgets_util";
import ImageCard from "../../../../components/common/ImageCard/ImageCard";
import ErrorBoundary from "../../../../components/lib/errorboundery/ErrorBoundary";
import AnchorLink from "../../../../components/common/AnchorLink";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import { AnalyticsGA } from "../../../../components/lib/analytics";
import GadgetsRating from "../../../../components/common/Gadgets/GadgetRatingWidget";
import SectionLayoutMemo from "../../../desktop/home/SectionLayout";
import AmazonAffiliate from "../../../../components/common/Gadgets/AmazonAffiliate/AmazonAffiliate";
import AdCard from "../../../../components/common/AdCard";
import Wdt2GudAffiliatesList from "../../../../components/common/Gadgets/Wdt2GudAffiliatesList/Wdt2GudAffiliatesList";
import WdtAmazonDetail from "../../../../components/common/Gadgets/WdtAmazonDetail/WdtAmazonDetail";
import SocialShare from "../../../../components/common/SocialShare";
import { defaultDesignConfigs } from "../../../defaultDesignConfigs";
import GridSectionMaker from "../../../../components/common/ListingCards/GridSectionMaker";
import SectionHeader from "../../../../components/common/SectionHeader/SectionHeader";
import Breadcrumb from "../../../../components/common/Breadcrumb";
import SvgIcon from "../../../../components/common/SvgIcon";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import { setParentId } from "../../../../actions/config/config";
import Ads_module from "./../../../../components/lib/ads/index";
import { connect } from "react-redux";
import GadgetsCompare from "../../../../components/common/Gadgets/GadgetsCompare/GadgetsCompare";

const gadgetCategories = (gadgetsConfig && gadgetsConfig.gadgetCategories) || "";

const Config = _getStaticConfig();

class GadgetShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: "OVERVIEW",
      commentsData: null,
      currentImageIndex: 0,
      isCommentClicked: false,
      readMoreExpanded: false,
      showReviewCard: false,
    };
  }

  componentDidMount() {
    const { data, dispatch } = this.props;
    const pwa_meta = data && data && data.pwa_meta;
    //for scroll top from as page
    const hashValue = window.location.hash;
    if (hashValue) {
      const spsId = hashValue.replace("#", "");
      const scrollToElement = document.getElementById(spsId);
      if (scrollToElement) {
        window.scrollTo({ top: scrollToElement.offsetTop });
      }
      //console.log('hashvalue', hashValue);
    }
    if (data && data.article) {
      const commentsParams = Object.assign({}, Config.Comments.allComments, {
        msid: data.article.msid,
      });

      fetch(`${Config.mweburl}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
        .then(res => res.json())
        .then(commentsData => {
          if (pwa_meta && typeof pwa_meta == "object") {
            //set section and subsection in window
            Ads_module.setSectionDetail(pwa_meta);
            //fire ibeat
            pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
            updateConfig(pwa_meta, dispatch, setParentId);
          }
          this.setState({
            commentsData,
          });
        })
        .catch(e => {
          this.setState({
            commentsData: null,
          });
        });
    }
  }

  componentDidUpdate(prevProps) {
    const { data, dispatch } = this.props;
    const pwa_meta = data && data && data.pwa_meta;
    if (!prevProps.data && data) {
      if (data && data.article) {
        const commentsParams = Object.assign({}, Config.Comments.allComments, {
          msid: data.article.msid,
        });

        fetch(`${Config.mweburl}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
          .then(res => res.json())
          .then(commentsData => {
            if (pwa_meta && typeof pwa_meta == "object") {
              //set section and subsection in window
              Ads_module.setSectionDetail(pwa_meta);
              //fire ibeat
              pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
              updateConfig(pwa_meta, dispatch, setParentId);
            }
            this.setState({
              commentsData,
            });
          })
          .catch(e => {
            this.setState({
              commentsData: null,
            });
          });
      }
    }
  }

  // Default values mobile and recently reviewed data

  closeIfOnOverlay = e => {
    if (e.target.id === "comments__body") {
      return false;
    }
    this.closeCommentSlider();
    return false;
  };

  closeCommentSlider = () => {
    this.setState({
      isCommentClicked: false,
    });

    document.body.classList.remove("disable-scroll");
  };

  showComments = () => {
    this.setState({
      isCommentClicked: true,
    });
    document.body.classList.add("disable-scroll");
  };

  openCommentsPopup = () => {
    // this.preventDefaultAndPropagation(e);
    this.setState({
      isCommentClicked: true,
    });
  };

  // eslint-disable-next-line react/sort-comp
  getReviewCard = () => {
    this.setState({
      showReviewCard: true,
    });
  };

  toggleReadMore = () => {
    const { readMoreExpanded } = this.state;
    console.log("toggleReadMore is called");
    this.setState({
      readMoreExpanded: !readMoreExpanded,
    });
  };

  scrollToSection = e => {
    const { data } = this.props;
    const gadgetData = data.gadget && data.gadget[0] && data.gadget[0];
    const targetId = e.target.id || e.target.parentNode.id || "";
    if (targetId) {
      const scrollToElement = document.getElementById(`${targetId}_${gadgetData ? gadgetData.uname : ""}`);
      if (scrollToElement) {
        window.scrollTo({ top: scrollToElement.offsetTop });
      }
    }
  };

  closeReviewCard = () => {
    this.setState({
      showReviewCard: false,
    });
  };

  openHoveredImage = currentImageIndex => {
    if (currentImageIndex !== this.state.currentImageIndex) {
      this.setState({
        currentImageIndex,
      });
    }
  };

  scrollToElementTop = (elemId, changeHash = false, setTimeout = false) => {
    if (changeHash) {
      window.history.replaceState({}, "", changeHash);
    }
    const elem = document.getElementById(elemId);
    if (elem) {
      if (!setTimeout) {
        elem.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" });
      } else {
        setTimeout(() => elem.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest" }), 1000);
      }
    }
  };

  preventDefaultAndPropagation = e => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    if (e.preventDefault) {
      e.preventDefault();
    }
  };

  getShareDetail(isShareAll) {
    if (!_isCSR()) return false;

    let sharedata = {
      title: document.title,
      url: location.href,
    };

    if (!isShareAll) return sharedata;

    //for watsapp sharing feature along with GA
    AnalyticsGA.event({
      category: "social",
      action: "Whatsapp_Wap_AS",
      label: sharedata.url,
    });

    var info = sharedata.url;
    info += "?utm_source=whatsapp" + Config.fullutm;
    var whatsappurl = "whatsapp://send?text=" + encodeURIComponent(sharedata.title) + " - " + info;
    console.log("whatsappurl", whatsappurl);
    window.location.href = whatsappurl;

    return false;
  }

  onGadgetItemClick = item => e => {
    this.preventDefaultAndPropagation(e);
    const { data } = this.props;
    const gadgetData = (data.gadget && data.gadget[0] && data.gadget[0]) || null;
    window.location.href = `${Config.mweburl}/tech/${gadgetCategories[gadgetData && gadgetData.category]}/${
      item.lcuname
    }`;
  };

  render() {
    const {
      data,
      category,
      authentication,
      fixedNav,
      trendingRightData,
      currentTab,
      showLoginRegister,
      isLastGadget,
      router,
    } = this.props;

    const { showReviewCard, currentImageIndex, readMoreExpanded, commentsData, isCommentClicked } = this.state;
    if (!data) {
      return null;
    }

    let mainImageId;

    if (data && Array.isArray(data.gadget)) {
      if (data.gadget[0] && Array.isArray(data.gadget[0].imageMsid)) {
        mainImageId = data.gadget[0].imageMsid[currentImageIndex];
      } else if (data.gadget[0] && typeof data.gadget[0].imageMsid === "string" && data.gadget[0].imageMsid) {
        mainImageId = data.gadget[0].imageMsid;
      }
    }

    const gadgetData = (data.gadget && data.gadget[0] && data.gadget[0]) || null;
    const getRegionaldata = [].concat(gadgetData && gadgetData.regional);

    const secData =
      getRegionaldata &&
      Array.isArray(getRegionaldata) &&
      getRegionaldata.filter(data => {
        return data && data.host === Config.hostid;
      });
    const getregionalreview =
      secData && secData[0] && secData[0].reviewid && secData[0].reviewid.msid ? secData[0].reviewid.msid : "";
    let isReviewDisabled = false;

    if (typeof window !== "undefined" && window.localStorage && gadgetData) {
      const reviewAndLoginIds = localStorage.getItem("reviewAndLoginIds");
      const ssoid = _getCookie("ssoid");
      if (reviewAndLoginIds && Array.isArray(JSON.parse(reviewAndLoginIds)) && ssoid) {
        if (JSON.parse(reviewAndLoginIds).includes(`${getregionalreview}-${ssoid}`)) {
          isReviewDisabled = true;
        }
      }
    }

    let sliderClass = "";
    const productTitle = secData && secData[0] && secData[0].name ? secData[0].name : "";
    const h1Value = data && data.h2 ? data.h2 : productTitle;
    let textval = "";

    const seoName = getKeyByValue(gadgetCategories[gadgetData && gadgetData.category]);
    const amazonTag = getAffiliateTags(gadgetsConfig.affiliateTags.GS_RHS_SPONS);

    if (gadgetData && (parseInt(gadgetData.upcoming) || parseInt(gadgetData.rumoured))) {
      if (parseInt(gadgetData.upcoming) && parseInt(gadgetData.rumoured)) {
        sliderClass = "rumoured";
      } else {
        sliderClass = parseInt(gadgetData.upcoming) ? "upcoming" : "rumoured";
      }
    }

    const sortPrice = getGadgetPrice(gadgetData);
    const sortPriceSchema = sortPrice.replace(/[₹, ]/g, ""); // replaces space, ₹ and comma

    // let sortPrice = "";
    let priceTableInfo = "";
    if (gadgetData && gadgetData.affiliate) {
      const affiliateData = gadgetData.affiliate;
      /*  if (affiliateData && affiliateData.exact && affiliateData.exact.items) {
        sortPrice = affiliateData.exact.items.sort_price;
      } else if (affiliateData && affiliateData.related && affiliateData.related[0]) {
        sortPrice = affiliateData.related[0].sort_price;
      } */

      if (affiliateData.exact) {
        if (affiliateData.exact.items) {
          priceTableInfo = affiliateData.exact.items;
        } else {
          priceTableInfo = affiliateData.exact[0];
        }
      }
    }

    let updatedDate = "";
    if (gadgetData && gadgetData.affiliate && gadgetData.affiliate.related) {
      const relatedNode = gadgetData.affiliate.related;
      if (relatedNode && relatedNode.items) {
        updatedDate = relatedNode.items.updatedAt;
      } else if (relatedNode && relatedNode[0]) {
        updatedDate = relatedNode[0].updatedAt;
      }
    }

    const tagSeeAlso = getAffiliateTags(gadgetsConfig.affiliateTags.GS_SEEALSO);

    let popularGadgetPair = "";
    if (
      data &&
      data.popularGadgetPair &&
      data.popularGadgetPair.compare &&
      Array.isArray(data.popularGadgetPair.compare)
    ) {
      popularGadgetPair = data.popularGadgetPair.compare.filter(item => item.product_name.length < 4);
    }

    return (
      <div style={{ overflow: "hidden" }}>
        <div className="productdetails_body pwa" id={`pdp-${(gadgetData && gadgetData.lcuname) || ""}`}>
          <h1>{h1Value}</h1>
          <div className="row">
            <div className="col12" data-exclude="amp">
              <div className={`pd_top_nav`}>
                <div className="nav_items">
                  <ul className="tabs" onClick={this.scrollToSection}>
                    <li id="overview" className={currentTab === "overview" ? "active" : ""}>
                      <span>{Config.locale.tech.overview}</span>
                    </li>
                    {data && data.article && parseInt(data.article.isMarkedReview) ? (
                      <li id="critic_review" className={currentTab === "critic_review" ? "active" : ""}>
                        <span>{Config.locale.tech.criticReview}</span>
                      </li>
                    ) : (
                      ""
                    )}

                    <li id="user_review" className={currentTab === "user_review" ? "active" : ""}>
                      <span>{Config.locale.tech.userReview}</span>
                    </li>
                    <li id="specification" className={currentTab === "specification" ? "active" : ""}>
                      <span>{Config.locale.tech.specifications}</span>
                    </li>
                  </ul>
                </div>
              </div>
              {_isCSR() ? (
                ReactDOM.createPortal(
                  <div className="share_container" data-exclude="amp">
                    <div className="share_whatsapp">
                      <a
                        data-attr="share_whatsapp"
                        rel="nofollow"
                        href="javascript:void(0);"
                        onClick={this.getShareDetail.bind(this, true)}
                      >
                        <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                      </a>
                    </div>
                    <SocialShare
                      sharedata={this.getShareDetail.bind(this, false)}
                      openCommentsPopup={this.openCommentsPopup}
                    />
                  </div>,
                  document.getElementById("share-container-wrapper"),
                )
              ) : (
                <div className="share_container" data-exclude="amp">
                  <div className="share_whatsapp">
                    <a
                      data-attr="share_whatsapp"
                      rel="nofollow"
                      href="javascript:void(0);"
                      onClick={this.getShareDetail.bind(this, true)}
                    >
                      <SvgIcon name="whatsapp" color="#fff" width="20px" height="20px" />
                    </a>
                  </div>
                  <SocialShare
                    sharedata={this.getShareDetail.bind(this, false)}
                    openCommentsPopup={this.openCommentsPopup}
                  />
                </div>
              )}
            </div>
            <div className="col12">
              <div className="product_summary" id={`overview_${gadgetData ? gadgetData.uname : ""}`}>
                {/* <Slider /> */}
                <div className="product_slider">
                  <ul className="slider-tabs">
                    {gadgetData && gadgetData.imageMsid && Array.isArray(gadgetData.imageMsid) ? (
                      gadgetData.imageMsid.slice(0, 4).map((imageData, index) => (
                        <li
                          className={index === currentImageIndex ? "active" : ""}
                          key={imageData}
                          onMouseOver={() => this.openHoveredImage(index)}
                        >
                          <ImageCard msid={imageData} size="gnthumb" noLazyLoad={index < 2 ? true : false} />
                        </li>
                      ))
                    ) : gadgetData && gadgetData.imageMsid ? (
                      <li>
                        <ImageCard msid={gadgetData.imageMsid} size="gnthumb" noLazyLoad />
                      </li>
                    ) : (
                      ""
                    )}
                  </ul>
                </div>

                <div className="tabs_circle" onClick={this.scrollToSection}>
                  <ul className="tabs_circle_list">
                    <li>
                      <AnchorLink
                        href={`${Config.mweburl}/tech/compare-${gadgetCategories[gadgetData && gadgetData.category]}`}
                      >
                        {Config.locale.tech.comparedotxt}
                      </AnchorLink>
                    </li>
                    <li id="similar" data-exclude="amp">
                      {Config.locale.tech.similar_gadget}
                    </li>
                  </ul>
                </div>
                <div className="enable-read-more" onClick={this.toggleReadMore}>
                  <div className="first_col">
                    <span
                      className={`caption text_ellipsis ${readMoreExpanded ? "more" : ""}`}
                      dangerouslySetInnerHTML={{ __html: data && data.story }}
                    />
                  </div>
                  <div className="second_col" data-exclude="amp">
                    <span className={`readmore ${readMoreExpanded ? "collapse" : "expand"}`}>&nbsp;</span>
                  </div>
                </div>
              </div>

              {/* <AdCard adtype="ctn" mstype={"ctncomparevideo"} /> */}

              {data &&
              Array.isArray(data.gadget) &&
              data.gadget[0] &&
              (Array.isArray(data.gadget[0].variants) || data.gadget[0].variants) ? (
                <GadgetVariants
                  currentlyActive={data.gadget[0].name}
                  gadgetCategory={category}
                  data={{ variants: [].concat(data.gadget[0].variants) }}
                />
              ) : null}

              {data && gadgetData && data.article && parseInt(data.article.isMarkedReview) ? (
                <CriticReview
                  data={data.article}
                  gadgetId={gadgetData.uname}
                  averageRating={gadgetData.averageRating}
                  criticRating={data.criticRating}
                />
              ) : (
                ""
              )}

              {data && data.productid ? (
                <ErrorBoundary>
                  <WdtAmazonDetail
                    affview="3"
                    type="gadgetshow"
                    productid={data.productid}
                    noimg="0"
                    msid={data.article.msid}
                    title={Config.locale.tech.seemore}
                    tag={tagSeeAlso}
                  />
                </ErrorBoundary>
              ) : (
                ""
              )}

              {data && data.twogud ? (
                <ErrorBoundary>
                  <Wdt2GudAffiliatesList
                    twogudData={data.twogud}
                    brandName={data.twogud.brandname}
                    price={data.twogud.price}
                    productName={data.twogud.productname}
                    tag={gadgetsConfig.affiliateTags.PDP}
                    msid={data.id}
                  />
                </ErrorBoundary>
              ) : (
                ""
              )}

              <div>
                <AdCard mstype="ctnTechPDP" adtype="ctn" />
              </div>

              {data && data.relatedgadgets && Array.isArray(data.relatedgadgets.gadget) ? (
                <div
                  className="pd_suggested_slider ui_slider box-item"
                  id={`similar_${gadgetData ? gadgetData.uname : ""}`}
                >
                  <div className="section">
                    <div className="top_section">
                      <h2>
                        <span>{Config.locale.tech.similarGadgets}</span>
                      </h2>
                    </div>
                  </div>
                  <Slider
                    type="gadgetShow"
                    sliderData={data.relatedgadgets.gadget}
                    size="4"
                    width=""
                    margin="15"
                    sliderClass="trendingslider"
                    isGadgetShow={true}
                    videoIntensive={false}
                    gadgetItemClick={this.onGadgetItemClick}
                    // amazonBuyClick={this.onAmazonBuyButtonClick}
                    // addFromSuggestions={this.addFromSuggestions}
                  />
                </div>
              ) : (
                ""
              )}
              {gadgetData ? (
                <div id={`user_review_${gadgetData.uname}`} className="box-item usrreviewswidget">
                  <GadgetsRating
                    data={{
                      ur: gadgetData.averageRating
                        ? Number.parseFloat(gadgetData.averageRating / 2).toFixed(1)
                        : Config.locale.tech.befirsttoreview,
                      rating: gadgetData.ratings,
                      id: getregionalreview,
                      productCategory: gadgetData.category,
                      productName: gadgetData.name,
                      brandName: gadgetData.brand_name,
                      imageLink: `${process.env.IMG_URL}/photo/${mainImageId || "66951362"}/${gadgetData.uname}.jpg`,
                      itemDescription: data && data.story,
                      isProductAvailable: sliderClass === "",
                      itemPrice: gadgetData.price,
                      sortPrice: sortPriceSchema,
                      pid: gadgetData.pid,
                      updatedDate: updatedDate,
                      announced: gadgetData.announced,
                      canonical: data && data.pwa_meta && data.pwa_meta.canonical,
                      router: router,
                    }}
                    isReviewDisabled={isReviewDisabled}
                    showLoginRegister={showLoginRegister}
                    loggedIn={authentication.loggedIn}
                    getreview={this.getReviewCard}
                    closereview={this.closeReviewCard}
                    showReviewCard={showReviewCard}
                  />
                </div>
              ) : (
                ""
              )}
              <div className="box-item">
                <AdCard mstype="mrec1" adtype="dfp" />
              </div>
              {data && data.gadget[0] && data.gadget[0].specs ? (
                <div id={`specification_${gadgetData && gadgetData.uname}`} className="box-item">
                  <SpecificationAccordion
                    heading={h1Value}
                    data={data && data.gadget[0] && data.gadget[0].specs}
                    price={sortPrice}
                  />
                </div>
              ) : null}
              <AmazonAffiliate
                category={gadgetData && gadgetData.category}
                tag={amazonTag}
                noimg="0"
                noh2="0"
                isslider="1"
              />
              <div className="box-item">
                <AdCard mstype="mrec2" adtype="dfp" />
              </div>
              <ErrorBoundary>
                {gadgetData ? (
                  <MoreFromBrand
                    gadgetCategory={gadgetCategories[(gadgetData && gadgetData.category) || ""]}
                    title={`${data.reginalname} ${data.regionalcat}`}
                    titleLink={`${process.env.API_BASEPOINT}/${gadgetCategories[gadgetData && gadgetData.category]}/${
                      gadgetData.brand_name
                    }`}
                    scrollToElementTop={this.scrollToElementTop}
                    elemID={`user_review_${gadgetData.uname}`}
                    data={data && Array.isArray(data.similar) && data.similar[0] && data.similar[0].gadget}
                  />
                ) : (
                  ""
                )}
              </ErrorBoundary>
              {/* {Config.gadgetShow.popularComparison} */}
              {popularGadgetPair && (
                <GadgetsCompare
                  type="popular"
                  data={popularGadgetPair}
                  // linksData={arrOtherThanTwo}
                  category={gadgetData && gadgetData.category}
                  seoName={category}
                  cssClass="wdt_popular_slider ui_slider"
                />
              )}
              {priceTableInfo && data && (
                <div className="price-widget gn_table_layout box-item">
                  <div className="section">
                    <div className="top_section">
                      <h2>
                        <span>{`${data.gadget && data.gadget[0] && data.gadget[0].name}`}</span>
                      </h2>
                    </div>
                  </div>
                  <table>
                    <tr>
                      <td>STORE</td>
                      <td>PRODUCT NAME</td>
                      <td>OFFER PRICE</td>
                      {priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice && <td>ACTUAL PRICE</td>}
                    </tr>
                    <tr>
                      <td>{priceTableInfo.Identifier}</td>
                      <td>{priceTableInfo.name}</td>
                      <td>{priceTableInfo.LowestNewPrice && priceTableInfo.LowestNewPrice.FormattedPrice}</td>
                      {priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice && (
                        <td>{priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice}</td>
                      )}
                    </tr>
                  </table>
                </div>
              )}
              {data && data.relarticle && data.relarticle.items && (
                <div className="box-item news">
                  <SectionHeader sectionhead={data.relarticle.secname} weblink={data.relarticle.wu} />
                  <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={data.relarticle.items} />
                </div>
              )}
              {data && data.relphoto && data.relphoto.items && (
                <div className="box-item photos">
                  <SectionHeader sectionhead={data.relphoto.secname} weblink={data.relphoto.wu} />
                  <GridSectionMaker type={defaultDesignConfigs.horizontalSlider} data={data.relphoto.items} />
                </div>
              )}
              {data && data.relvideo && data.relvideo.items && (
                <div className="box-item videos">
                  <SectionHeader sectionhead={data.relvideo.secname} weblink={data.relvideo.wu} />
                  <ul class="col12 view-horizontal">
                    {data.relvideo.items.map(card => {
                      return (
                        <li class="news-card vertical col video">
                          <VideoItem
                            item={card}
                            src={card.imageid}
                            alt={card.seotitle ? card.seotitle : ""}
                            title={card.seotitle || ""}
                          />
                        </li>
                      );
                    })}
                  </ul>
                </div>
              )}
            </div>
          </div>
          {isCommentClicked ? (
            <div className="popup_comments">
              <div className="body_overlay" onClick={this.closeIfOnOverlay} />
              <div style={{ right: 0 }} id="comments__body" className="comments-body">
                <span className="close_icon" onClick={this.closeCommentSlider} />
                <iframe
                  src={`${process.env.WEBSITE_URL}comments_slider_react_v1.cms?msid=${
                    data.article.msid
                  }&type=reviews&isloggedin=${isLoggedIn()}`}
                  height="100%"
                  width="550"
                  border="0"
                  MARGINWIDTH="0"
                  MARGINHEIGHT="0"
                  HSPACE="0"
                  VSPACE="0"
                  FRAMEBORDER="0"
                  SCROLLING="no"
                  align="center"
                  title="iplwidget"
                  ALLOWTRANSPARENCY="true"
                  id="comment-frame"
                />
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
        {data && data.breadcrumb ? <Breadcrumb items={data.breadcrumb.div.ul} /> : null}
        {!isLastGadget ? (
          <div className="pdp-story-seperator pwa" id="">
            <div className="end-ad">{<AdCard mstype="mrec2" adtype="dfp" />}</div>
            <div className="story_partition" data-exclude="amp">
              <span>{Config.locale.tech.nextGadget}</span>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}

const CommentsSection = ({ data, showComments, type }) => {
  const removeSpcChar = str => {
    const removed = str.replace(/<\/?[^>]+(>|$)/g, "");
    return removed;
  };
  if (type === "reviews") {
    return (
      <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
        <button className="btn-blue">{Config.locale.tech.text_write_reviews}</button>
        {data && Array.isArray(data) && data.length > 0 ? (
          <div className="pd-bottom-comments">
            {data.slice(1).length > 0 &&
              data
                .slice(1)
                .slice(0, 2)
                .map(c => (
                  <div className="pd-comment-box" key={c.C_T}>
                    <div className="user-thumbnail">
                      <ImageCard
                        className="userimg flL"
                        src={
                          (c.user_detail && c.user_detail.thumb) ||
                          `${process.env.API_BASEPOINT}${Config.Thumb.userImage}`
                        }
                        type="absoluteImgSrc"
                        size="gnthumb"
                      />
                    </div>
                    {"user_detail" in c && c.user_detail.FL_N ? (
                      <AnchorLink
                        href={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ""}
                        className="name"
                        target="_blank"
                      >
                        {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                      </AnchorLink>
                    ) : null}
                    <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                    <div className="footbar clearfix">
                      <span
                        data-action="comment-reply"
                        className="cpointer"
                        onClick={() => showComments()}
                        role="button"
                        tabIndex={0}
                      >
                        Reply
                      </span>
                    </div>
                  </div>
                ))}
          </div>
        ) : null}
        {data.length > 2 ? (
          <span className="btn-blue btm" onClick={() => showComments()}>
            {Config.locale.tech.readMoreReview}
          </span>
        ) : (
          ""
        )}
      </div>
    );
  }
  return (
    <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
      <div className="pd-bottom-comments-btns">
        <div className="cmtbtn-wrapper">
          {data && Array.isArray(data) && data.length > 0 ? (
            <span className="cmtbtn view" onClick={showComments} role="button" tabIndex={0}>
              {`${Config.locale.tech.text_view_comment} (${data[0].cmt_c})`}
            </span>
          ) : null}
          <span
            className="cmtbtn add"
            onClick={showComments}
            style={{ width: data.length === 0 ? "100%" : "50%" }}
            role="button"
            tabIndex={0}
          >
            {type === "reviews" ? (
              <span>{Config.locale.tech.text_write_reviews}</span>
            ) : (
              <span>
                <span className="icon" />
                <span>{Config.locale.tech.text_write_comment}</span>
              </span>
            )}
          </span>
        </div>
      </div>
      {data && Array.isArray(data) && data.length > 0 ? (
        <div className="pd-bottom-comments">
          {data.slice(1).length > 0 &&
            data
              .slice(1)
              .slice(0, 2)
              .map(c => (
                <div className={`pd-comment-box${c && c.C_A_ID ? " authorComment" : ""}`} key={c.C_T}>
                  <div className="user-thumbnail">
                    <Thumb
                      className="userimg flL"
                      src={
                        (c.user_detail && c.user_detail.thumb) ||
                        `${process.env.API_BASEPOINT}${Config.Thumb.userImage}`
                      }
                    />
                  </div>
                  {"user_detail" in c && c.user_detail.FL_N ? (
                    <Link to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ""} className="name" target="_blank">
                      {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                    </Link>
                  ) : null}
                  <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                  <div className="footbar clearfix">
                    <span
                      data-action="comment-reply"
                      className="cpointer"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      Reply
                    </span>
                    <span
                      className="up cpointer"
                      data-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      data-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span>
                  </div>
                </div>
              ))}
        </div>
      ) : null}
      {data.length > 2 ? (
        <span className="btn-blue btm" onClick={() => showComments()}>
          {Config.locale.tech.readMoreReview}
        </span>
      ) : (
        ""
      )}
    </div>
  );
};

CommentsSection.propTypes = {
  data: PropTypes.array,
  loggedIn: PropTypes.bool,
  showComments: PropTypes.func,
  type: PropTypes.string,
};

const MoreFromBrand = ({ data, title, titleLink, gadgetCategory, scrollToElementTop, elemID }) => {
  if (!Array.isArray(data)) {
    return null;
  }

  return (
    <div className="box-item gadgetslider">
      <div className="section">
        <div className="top_section">
          <h2>
            <span>
              <AnchorLink href={titleLink}>{title}</AnchorLink>
            </span>
          </h2>
        </div>
      </div>
      <ul className="col12 view-horizontal">
        {data.map(brandData => {
          const amztitle = brandData.c_model_lcuname;
          const price = brandData.price;
          const amzga = `${amztitle}_${price}`;
          const affiliateTag = getAffiliateTags(gadgetsConfig.affiliateTags.GS_BRAND);
          // const affiliateTag = (Config && Config.affiliateTags && Config.affiliateTags.GS_BRAND) || "";

          const brandTitle = brandData.regional && brandData.regional.name ? brandData.regional.name : brandData.name;
          const url =
            brandData.affiliate && Array.isArray(brandData.affiliate.related) && brandData.affiliate.related[0]
              ? brandData.affiliate.related[0].url
              : `/tech/${gadgetCategory}/${brandData.uname}`;
          const buyURL = getBuyLink({
            url,
            price,
            title: brandTitle,
            amzga,
            tag: affiliateTag,
          });
          // console.log('brandData in more from branc', brandData);
          return (
            <li className="news-card vertical" key={brandData.uname}>
              <div className="proditem">
                <span className="prod_img">
                  <AnchorLink href={`/tech/${gadgetCategory}/${brandData.uname}`}>
                    <ImageCard
                      title={brandTitle}
                      alt={brandTitle}
                      msid={[].concat(brandData.imageMsid)[0]}
                      size="gnthumb"
                    />
                  </AnchorLink>
                </span>
                <span className="con_wrap prod_dec">
                  <h4 title={brandTitle}>
                    <AnchorLink className="text_ellipsis" href={`/tech/${gadgetCategory}/${brandData.uname}`}>
                      {brandTitle}
                    </AnchorLink>
                  </h4>
                  <span className="price_tag symbol_rupees">{formatMoney(brandData.price, 0)}</span>
                </span>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

MoreFromBrand.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string,
  titleLink: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

// below PopularComparison compoent needs to delete as it seems it is of no use here

const PopularComparison = ({ dataForPopularMobile, restdataForPopularMobile, title }) => {
  return (
    <div className="wdt_popular_slider ui_slider">
      <h2>
        <span>{title}</span>
      </h2>

      {dataForPopularMobile && (
        <Slider
          type="compareListPopular"
          size="1"
          sliderData={dataForPopularMobile}
          width="165"
          height="185"
          SliderClass="pd_slider"
          islinkable="true"
          sliderWidth="180"
          headingRequired="yes"
        />
      )}

      <div className="items-in-list">
        <ul>
          {restdataForPopularMobile
            ? restdataForPopularMobile.map(item => {
                return (
                  <li key={item.product_name.join(" vs ")}>
                    <span className="text_ellipsis">
                      {`${Config.locale.tech.comparedotxt} ${item.product_name.join(" vs ")}`}
                    </span>
                  </li>
                );
              })
            : ""}
        </ul>
      </div>
    </div>
  );
};

PopularComparison.propTypes = {
  dataForPopularMobile: PropTypes.array,
  restdataForPopularMobile: PropTypes.array,
  title: PropTypes.string,
};

const CriticReview = ({ data, criticRating, averageRating, gadgetId }) => {
  return (
    <div className="thumb_img_reviews" id={`critic_review_${gadgetId}`} data-exclude="amp">
      <div className="section">
        <div className="top-section">
          <h2>
            <span>{Config.locale.tech.criticReview}</span>
          </h2>
        </div>
      </div>
      <h3>{data.title}</h3>
      <div className="img_wrap">
        <ImageCard msid={data.msid} size="gnthumb" />
        <span className="image_caption">
          <span className="lft">
            <span className="head">
              <b className="icon_star one" />
              {Config.locale.tech.criticRating}
            </span>
            {Number.parseInt(criticRating) !== 0 ? (
              <span className="rate_txt">
                {Number.parseFloat(criticRating).toFixed(1)}
                <small>/5</small>
              </span>
            ) : (
              "NA"
            )}
          </span>
          <span className="cen">
            <span className="head">
              <b className="icon_star one" />
              {Config.locale.tech.userRating}
            </span>
            <span className="rate_txt">
              {Number.parseFloat(averageRating / 2).toFixed(1)}
              <small>/5</small>
            </span>
          </span>
          {data.topfeature && Array.isArray(data.topfeature["#text"]) ? (
            <span className="rgt">
              <span className="head">{Config.locale.tech.topfeature}</span>
              <ul className="features_txt">
                {data.topfeature["#text"].map(topFeatData => (
                  <li key={topFeatData}>{topFeatData}</li>
                ))}
              </ul>
            </span>
          ) : (
            ""
          )}
        </span>
      </div>
      <span className="review_text">{data.artsyn}</span>
      <AnchorLink href={data.url} className="more-btn">
        {Config.locale.tech.readCompleteReview}
      </AnchorLink>
    </div>
  );
};

CriticReview.propTypes = {
  data: PropTypes.object,
  criticRating: PropTypes.string,
  averageRating: PropTypes.string,
};

GadgetShow.propTypes = {
  data: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    alaskaData: state.header.alaskaData,
    gadgetshow: state.gadgetshow,
  };
}

export default connect(mapStateToProps)(GadgetShow);
