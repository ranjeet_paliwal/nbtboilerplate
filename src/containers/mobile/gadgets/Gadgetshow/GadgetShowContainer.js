import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { PageMeta } from "../../../../components/common/PageMeta";
import fetchDataIfNeeded, { updateNavData, fetchRelatedGadgetData } from "../../../../actions/gn/gadgetshow/gadgetshow";
import fetch from "../../../../utils/fetch/fetch";
import { updateConfig, _getStaticConfig } from "../../../../utils/util";
import { setParentId, setPageType } from "../../../../actions/config/config";
import gadgetsConfig from "../../../../utils/gadgetsConfig";
import GadgetShow from "./GadgetShow";
import Ads_module from "./../../../../components/lib/ads/index";
import { setIbeatConfigurations } from "../../../../components/lib/analytics/src/iBeat";
import FakeListing from "../../../../components/common/FakeCards/FakeListing";
import { invokeGaComscore } from "../../../../containers/utils/gadgets_util";

const gadgetCategories = (gadgetsConfig && gadgetsConfig.gadgetCategories) || "";

class GadgetShowContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentArticle: 0,
      currentTop: 0,
      trendingRightData: null,
      currentTab: "overview",
      currentBottom: 0,
      fixedNav: false,
    };

    this.article1Ref = React.createRef();

    this.handleScrollChange = this.handleScrollChange.bind(this);
    this.changePageUrl = this.changePageUrl.bind(this);
  }

  componentDidMount() {
    const { dispatch, params, query, history, data, relatedGadgetsToFetch, location } = this.props;
    const sectionName = data && data.articleData && data.articleData.secname ? data.articleData.secname : "";

    if ((location.hash === "#specifications" || location.hash === "#rate") && data) {
      const gadgetData =
        (data && data.gadgetData && data.gadgetData.gadget && data.gadgetData.gadget[0] && data.gadgetData.gadget[0]) ||
        null;

      const hashKey = {
        "#specifications": "specification",
        "#rate": "user_review",
      };

      if (window && gadgetData) {
        window.scrollTo(0, 0);
        // window.onload = () => {
        //   this.scrollToElementTop(`${hashKey[location.hash]}_${gadgetData.uname}`);
        // };
      }
    }

    fetch(`${process.env.API_BASEPOINT}/webgn_common.cms?feedtype=sjson&count=10&platform=web&tag=ibeatmostread`).then(
      data => {
        this.setState({
          trendingRightData: data,
        });
      },
    );

    GadgetShowContainer.fetchData({ dispatch, query, params, history }).then(() => {
      const { data } = this.props;
      const pwa_meta = data && data.gadgetData && data.gadgetData.pwa_meta;
      if (pwa_meta && typeof pwa_meta == "object") {
        //set section and subsection in window
        Ads_module.setSectionDetail(pwa_meta);
        //fire ibeat
        pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : "";
        updateConfig(pwa_meta, dispatch, setParentId);
      }
    });

    if (data && data.gadgetData) {
      window.addEventListener("scroll", this.handleScrollChange);
    }

    // Create refs for all related gadgets before hand.
    if (relatedGadgetsToFetch && Array.isArray(relatedGadgetsToFetch)) {
      for (let i = 0; i < relatedGadgetsToFetch.length; i++) {
        this[`article${i + 2}Ref`] = React.createRef();
      }
    }

    dispatch(setPageType("gadgetshow"));
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, location, history, data, relatedGadgetsToFetch } = this.props;

    if (!prevProps.data && data) {
      window.addEventListener("scroll", this.handleScrollChange);
      const articleOneDiv = this.article1Ref.current;
      if (articleOneDiv) {
        const articleOneDim = articleOneDiv.getBoundingClientRect();
        const currentTop = articleOneDiv.offsetTop;
        const currentBottom = articleOneDiv.offsetTop + articleOneDim.height;
        this.setState({
          currentTop,
          currentBottom,
        });
      }

      if ((location.hash === "#specifications" || location.hash === "#rate") && data) {
        const gadgetData =
          (data &&
            data.gadgetData &&
            data.gadgetData.gadget &&
            data.gadgetData.gadget[0] &&
            data.gadgetData.gadget[0]) ||
          null;

        const hashKey = {
          "#specifications": "specification",
          "#rate": "user_review",
        };

        this.scrollToElementTop(`${hashKey[location.hash]}_${gadgetData.uname}`);
      }

      if (relatedGadgetsToFetch && Array.isArray(relatedGadgetsToFetch)) {
        for (let i = 0; i < relatedGadgetsToFetch.length; i++) {
          this[`article${i + 2}Ref`] = React.createRef();
        }
      }
    } else if (prevProps.location.pathname !== location.pathname) {
      window.scrollTo({ top: 0 });
      GadgetShowContainer.fetchData({ dispatch, query, params, history });
    }
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScrollChange);
    document.querySelector("body").classList.remove("AS");
  }

  scrollToElementTop = elemId => {
    const elem = document.getElementById(elemId);
    if (elem) {
      // window.scrollTo({ top: elem.offsetTop });
      setTimeout(
        () =>
          elem.scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "nearest",
          }),
        1000,
      );
    }
  };

  handleScrollChange(e) {
    const { currentArticle, currentTop, fixedNav, currentTab } = this.state;
    const { data, relatedGadgetsToFetch, isFetchingRelatedArticles, dispatch, params, relatedGadgetsData } = this.props;

    let currentId = "";
    if (currentArticle === 0) {
      currentId =
        (data &&
          data.gadgetData &&
          data.gadgetData.gadget &&
          data.gadgetData.gadget[0] &&
          data.gadgetData.gadget[0].uname) ||
        "";
    } else if (
      relatedGadgetsData[currentArticle - 1] &&
      relatedGadgetsData[currentArticle - 1].gadget &&
      relatedGadgetsData[currentArticle - 1].gadget[0] &&
      relatedGadgetsData[currentArticle - 1].gadget[0].uname
    ) {
      currentId = relatedGadgetsData[currentArticle - 1].gadget[0].uname;
    }
    const overviewElement = document.getElementById(`overview_${currentId}`);
    let newTab = currentTab;
    if (overviewElement) {
      const inBetween = elem =>
        window.scrollY > elem.offsetTop - 150 &&
        window.scrollY <= elem.offsetTop + elem.getBoundingClientRect().height + 150;

      // Check which element is in view
      const specificationElement = document.getElementById(`specification_${currentId}`);
      const userReviewElement = document.getElementById(`user_review_${currentId}`);
      const criticReviewElement = document.getElementById(`critic_review_${currentId}`);
      if (overviewElement && currentTab !== "overview") {
        if (inBetween(overviewElement)) {
          newTab = "overview";
        }
      }
      if (specificationElement && currentTab !== "specification") {
        if (inBetween(specificationElement)) {
          newTab = "specification";
        }
      }

      if (userReviewElement && currentTab !== "user_review") {
        if (inBetween(userReviewElement)) {
          newTab = "user_review";
        }
      }

      if (criticReviewElement && currentTab !== "critic_review") {
        if (inBetween(criticReviewElement)) {
          newTab = "critic_review";
        }
      }
    }

    const currentArticleDiv = this[`article${currentArticle + 1}Ref`].current;
    const currentArticleDim = currentArticleDiv && currentArticleDiv.getBoundingClientRect();
    const articleTop = currentArticleDim && currentArticleDiv.offsetTop;
    const articleBottom = currentArticleDiv && currentArticleDiv.offsetTop + currentArticleDim.height;

    // Sticky Navigation on scroll
    if (window.scrollY > articleTop + 150 && !fixedNav) {
      this.setState({
        fixedNav: true,
      });
    } else if (window.scrollY < articleTop - 150 && fixedNav) {
      this.setState({
        fixedNav: false,
      });
    }

    const nextArticlesLength =
      (data &&
        data.gadgetData &&
        Array.isArray(data.gadgetData.similar) &&
        data.gadgetData.similar[0] &&
        Array.isArray(data.gadgetData.similar[0].gadget) &&
        data.gadgetData.similar[0].gadget.length) ||
      0;

    /* To fetch data when in the middle of the last article */
    if (
      window.scrollY > currentTop / 2 &&
      relatedGadgetsData.length === currentArticle &&
      !isFetchingRelatedArticles &&
      Array.isArray(relatedGadgetsToFetch) &&
      relatedGadgetsToFetch.length > 0
    ) {
      dispatch(fetchRelatedGadgetData({ id: relatedGadgetsToFetch[0], category: params.category }));
    }

    let newArticle = currentArticle;

    /*  If user has scrolled past current article downwards or upwards */
    if (window.scrollY > articleBottom || window.scrollY < articleTop) {
      /*  User moves upwards, and is not at first article */
      if (window.scrollY < articleTop && currentArticle !== 0) {
        newArticle = currentArticle - 1;
      } else if (window.scrollY > articleBottom && currentArticle !== nextArticlesLength) {
        /* User moves downwards, and is not at the last article */
        newArticle = currentArticle + 1;
      }
    }

    if (newArticle !== currentArticle) {
      const newArticleDiv = this[`article${newArticle + 1}Ref`].current;
      const newArticleDim = newArticleDiv && newArticleDiv.getBoundingClientRect();
      const newTop = newArticleDiv && newArticleDiv.offsetTop;
      const newBottom = currentArticleDiv && currentArticleDiv.offsetTop + newArticleDim.height;

      this.changePageUrl(newArticle);

      this.setState({
        currentArticle: newArticle,
        currentTop: newTop,
        currentBottom: newBottom,
      });
    }
    // console.log('CURRENTTAB', currentTab);
    // console.log('NEWTAB', newTab);
    if (currentTab !== newTab) {
      this.setState({
        currentTab: newTab,
      });
    }
  }

  changePageUrl(newArticle) {
    let url;
    let gadgetData;
    const { data, relatedGadgetsData, relatedAdsData } = this.props;
    const { currentArticle } = this.state;

    const currentStateArticle = currentArticle;
    // To change history only when article is being changed.
    if (newArticle !== currentStateArticle) {
      // if (TPLib && TPLib.logPoints) {
      //   TPLib.logPoints(TPLib.ConfigData.tpconfig.ARTICLE_READ, '1234');
      // }

      if (newArticle === 0) {
        gadgetData = (data.gadgetData && data.gadgetData.gadget && data.gadgetData.gadget[0]) || null;

        document.title = data.gadgetData && data.gadgetData.pwa_meta ? data.gadgetData.pwa_meta.title : "";

        url =
          gadgetData && gadgetData.lcuname && gadgetData.category
            ? `/tech/${gadgetCategories[gadgetData.category]}/${gadgetData.lcuname}`
            : data.gadgetData && data.gadgetData.pwa_meta && data.gadgetData.pwa_meta.redirectUrl;

        window.history.pushState({ title: "" }, "", url);
      } else {
        const articleData = relatedGadgetsData[newArticle - 1];
        const newTitle = articleData.pwa_meta && articleData.pwa_meta.title ? articleData.pwa_meta.title : "";
        document.title = newTitle;
        gadgetData = (articleData.gadget && articleData.gadget[0]) || null;

        url =
          gadgetData && gadgetData.lcuname && gadgetData.category
            ? `/tech/${gadgetCategories[gadgetData.category]}/${gadgetData.lcuname}`
            : gadgetData && gadgetData.pwa_meta && gadgetData.pwa_meta.redirectUrl;

        window.history.pushState({ title: "" }, "", url);

        invokeGaComscore(gadgetData.lcuname);

        let adsDataUpdated = "";
        // Array.isArray(relatedAdsData) && relatedAdsData[newArticle - 1] && relatedAdsData[newArticle - 1].wapads
        //   ? relatedAdsData[newArticle - 1].wapads
        //   : null;

        const rGadgetsData =
          Array.isArray(relatedGadgetsData) && relatedGadgetsData[newArticle - 1]
            ? relatedGadgetsData[newArticle - 1]
            : null;

        if (adsDataUpdated && rGadgetsData) {
          let currArticle = (articleData && articleData.id && articleData.id) || "";
        }
      }
    }
    return true;
  }

  render() {
    const {
      isFetching,
      data,
      params,
      showLoginRegister,
      authentication,
      location,
      relatedGadgetsData,
      isFetchingRelatedArticles,
      router,
    } = this.props;

    const { fixedNav, currentArticle, trendingRightData, currentTab } = this.state;

    let relatedGadgetsToFetch = "";
    let latestTrendingData = null;

    const pwaMeta = data && data.gadgetData && data.gadgetData.pwa_meta;

    if (data) {
      relatedGadgetsToFetch = data.relatedGadgetsToFetch;
      latestTrendingData = data.latestTrendingData;
      const gadgetData =
        data && data.gadgetData && Array.isArray(data.gadgetData.gadget) && data.gadgetData.gadget[0]
          ? data.gadgetData.gadget[0]
          : "";
    }

    return !isFetching ? (
      <div className="articles_container AS">
        {pwaMeta ? PageMeta(pwaMeta) : null}

        <div ref={this.article1Ref} style={{ overflow: "hidden" }}>
          <GadgetShow
            fixedNav={currentArticle === 0 ? fixedNav : false}
            data={data && data.gadgetData}
            hash={location.hash}
            currentTab={currentTab}
            trendingRightData={trendingRightData}
            showLoginRegister={showLoginRegister}
            category={params.category}
            authentication={authentication}
            latestTrendingData={latestTrendingData}
            isLastGadget={relatedGadgetsToFetch && relatedGadgetsToFetch.length === 0}
            router={router}
          />
        </div>
        {Array.isArray(relatedGadgetsData)
          ? relatedGadgetsData.map((relatedData, index) => (
              <div
                key={relatedGadgetsData.productid}
                style={{ overflow: "hidden" }}
                ref={this[`article${index + 2}Ref`]}
              >
                <GadgetShow
                  data={relatedData}
                  fixedNav={currentArticle > 0 && currentArticle === index + 1 ? fixedNav : false}
                  category={params.category}
                  currentTab={currentTab}
                  latestTrendingData={latestTrendingData}
                  showLoginRegister={showLoginRegister}
                  trendingRightData={trendingRightData}
                  authentication={authentication}
                  isLastGadget={index === relatedGadgetsData.length - 1}
                  router={router}
                />
              </div>
            ))
          : ""}
        {/* {isFetchingRelatedArticles ? <ArticleSHowFakeLoader /> : ''} */}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

GadgetShowContainer.fetchData = function({ dispatch, query, history, params }) {
  // dispatch(setPageType("gadgetShow"));
  return dispatch(fetchDataIfNeeded(params)).then(data => {
    const { payload } = data;
    const pwaMeta = payload && payload.gadgetData && payload.gadgetData.pwa_meta;
    updateConfig(pwaMeta, dispatch, setParentId);
    dispatch(setPageType("gadgetshow"));
  });
};

GadgetShowContainer.propTypes = {
  dispatch: PropTypes.func,
  showLoginRegister: PropTypes.func,
  closeLoginRegister: PropTypes.func,
  params: PropTypes.object,
  location: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  dockedVideo: PropTypes.object,
  data: PropTypes.object,
  isFetchingRelatedArticles: PropTypes.bool,
  relatedArticlesDataFetched: PropTypes.bool,
  isFetching: PropTypes.bool,
  relatedArticlesData: PropTypes.object,
  loggedIn: PropTypes.bool,
  isFetchingNewsData: PropTypes.bool,
  relatedGadgetsToFetch: PropTypes.array,
  newsDataFetched: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.gadgetshow,
    authentication: state.authentication,
  };
}

export default connect(mapStateToProps)(GadgetShowContainer);
