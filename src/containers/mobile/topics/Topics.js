import React, { Component } from "react";
import { connect } from "react-redux";

import Breadcrumb from "../../../components/common/Breadcrumb";
import { fetchListDataIfNeeded, fetchNextListDataIfNeeded, fetchProfileData } from "../../../actions/topics/topics";
import TopicsCard, { ProfileDesc, ProfileInfo, ShortProfile } from "../../../components/common/TopicsCard";
import WebTitleCard from "../../../components/common/WebTitleCard";

import FakeListing from "../../../components/common/FakeCards/FakeListing";
import ErrorBoundary from "../../../components/lib/errorboundery/ErrorBoundary";
import {
  throttle,
  scrollTo,
  _isCSR,
  setHyp1Data,
  _getStaticConfig,
  handleReadMore,
  loadInstagramJS,
  loadTwitterJS,
  handleFBembed,
  isMobilePlatform,
} from "../../../utils/util";
import { designConfigs } from "./designConfigs";
import { defaultDesignConfigs } from "../../defaultDesignConfigs";
import GridSectionMaker from "../../../components/common/ListingCards/GridSectionMaker";
import { AnalyticsGA } from "../../../components/lib/analytics/index";
import { PageMeta, SeoSchema } from "../../../components/common/PageMeta"; //For Page SEO/Head Part
import AdCard from "../../../components/common/AdCard";
import Ads_module from "../../../components/lib/ads/index";
import AnchorLink from "../../../components/common/AnchorLink";
import styles from "../../../components/common/css/commonComponents.scss";
import "../../../components/common/css/TopicsCard.scss";
import { setIbeatConfigurations } from "../../../components/lib/analytics/src/iBeat";
import { setPageType } from "../../../actions/config/config";
import PlayerStats from "../../../campaign/cricket/components/PlayerStats";
const siteConfig = _getStaticConfig();
export class Topics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      totalPages: 0,
      _scrollEventBind: false,
    };
    this.config = {
      isFetchingNext: false,
      listCount: 1,
      designConfigs: Object.assign(defaultDesignConfigs, designConfigs),
      listCountInc: () => {
        return this.config.listCount++;
      },
    };
    this.scrollHandler = false;
    // this.listCount = 1;
    // this.listCountInc = () => {
    //   return this.listCount++;
    // };
  }

  componentDidMount() {
    const { dispatch, params, value, router } = this.props;
    const { query } = this.props.location;

    //set section & subsection for first time

    if (value && typeof value.pwa_meta == "object") {
      Ads_module.setSectionDetail(value.pwa_meta);
    }

    Topics.fetchData({ dispatch, query, params, router }).then(data => {
      //attach scroll only when sections is not there in feed
      if (typeof this.props.value != "undefined" && this.props.value.items) {
        this.scrollBind();
      }
      //set section and subsection in window
      const { pwa_meta, pg } = this.props.value ? this.props.value : {};
      if (pg && pg.tp) {
        this.state.totalPages = pg.tp;
      }
      if (pwa_meta && typeof pwa_meta == "object") {
        Ads_module.setSectionDetail(pwa_meta);
        //set hyp1 variable
        pwa_meta ? setHyp1Data(pwa_meta) : "";
        //fire ibeat
        pwa_meta.ibeat && pwa_meta.ibeat.articleId ? setIbeatConfigurations(pwa_meta.ibeat) : "";
      }

      handleReadMore();
    });

    if (typeof window != "undefined") {
      window.setTimeout(() => {
        try {
          loadInstagramJS();
          loadTwitterJS();
          handleFBembed();
        } catch (ex) {
          console.log("ERROR PROCESSING EMBEDS", ex);
        }
      }, 2000);
    }

    Ads_module.render({});

    dispatch(setPageType("topics"));
  }

  scrollBind() {
    const _this = this;
    if (this.state._scrollEventBind == false) {
      this.scrollHandler = throttle(this.handleScroll.bind(_this));
      window.addEventListener("scroll", this.scrollHandler);
      this.state._scrollEventBind = true;
    }
  }

  scrollUnbind() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
    }
  }

  handleScroll(e) {
    let _this = this;
    if (this.state._scrollEventBind == false) return;

    let body = document.body;
    let html = document.documentElement;
    let docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    let scrollValue = document.body.scrollTop;
    if (scrollValue == 0) {
      scrollValue = (document.scrollingElement || document.documentElement).scrollTop;
    }
    let footerHeight = document.querySelector("#footerContainer,footer").offsetHeight;
    docHeight = docHeight - footerHeight;
    let curpg = parseInt(this.props.value.pg.cp) + 1;
    if (scrollValue + 1000 > docHeight && curpg <= this.state.totalPages) {
      const { dispatch, params, isFetching, router } = this.props;
      const { query } = this.props.location;
      if (isFetching == false) {
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        Topics.fetchNextListData({ dispatch, query, params, router }).then(data => {
          let _data = data ? data.payload : {};
          //silentRedirect(_this.generateListingLink());
          _this.config.isFetchingNext = !_this.config.isFetchingNext;

          AnalyticsGA.pageview(location.origin + _this.generateListingLink());
          Ads_module.refreshAds(["fbn"]); //refresh fbn ads when next list added

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";

          // history push
          let queryString, url;
          queryString = window.location.search;
          url = location.origin + _this.generateListingLink();

          if (url != location.href || _data.pwa_meta.title != document.title) {
            //todo stop history push for webview
            window.history.replaceState({}, _data.pwa_meta.title, url);
            document.title = _data.pwa_meta.title;
          }
        });
      }
    }
  }

  generateListingLink(type) {
    let props = this.props;
    //console.log("generateListingLink : " + type);
    let pathname = props.location && props.location.pathname;

    if (pathname && props.value && props.value.pg && props.value.pg.cp) {
      let curpg = type == "moreButton" ? parseInt(props.value.pg.cp) + 1 : parseInt(props.value.pg.cp);

      if (pathname.indexOf("/profile.cms") > -1) {
        if (pathname.indexOf("curpg") > -1) {
          let reExp = /curpg=\\d+/;
          return pathname.replace(reExp, "curpg=" + curpg);
        } else return pathname + ((pathname.indexOf("?") > -1 ? "&curpg=" : "?curpg=") + curpg);
      }

      var pathnameArr = props.location.pathname.split("/");
      var cp = pathnameArr[pathnameArr.length - 1];
      //console.log(cp);
      if (isNaN(cp)) {
        return props.location.pathname + "/" + curpg;
      } else {
        return props.location.pathname.replace(cp, curpg);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.params.searchkey != nextProps.params.searchkey ||
      this.props.params.searchtype != nextProps.params.searchtype ||
      this.props.params.curpg != nextProps.params.curpg
    ) {
      const { dispatch, params } = nextProps;
      const { query } = nextProps.location;
      try {
        Topics.fetchData({ dispatch, query, params }).then(data => {
          // CTN_module
          //CTN_module.renderCtn();
          let _data = data && data.payload ? data.payload[0] : {};
          const { pg } = _data;
          if (pg && pg.tp) {
            this.state.totalPages = pg.tp;
          }
          // if (!data.payload[0].items) {
          //   this.scrollBind();
          // } else {
          //   this.scrollUnbind();
          // }

          //fire ibeat for next
          _data && _data.pwa_meta && _data.pwa_meta.ibeat && _data.pwa_meta.ibeat.articleId
            ? setIbeatConfigurations(_data.pwa_meta.ibeat)
            : "";
        });
      } catch (ex) {}

      scrollTo(document.documentElement, 0, 100);
    }
  }

  componentWillUnmount() {
    const _this = this;
    if (typeof window != "undefined") {
      try {
        window.removeEventListener("scroll", this.scrollHandler, false);
      } catch (ex) {}
      this.state._scrollEventBind = false;
      //reset section window data
      Ads_module.setSectionDetail();
      //reset hyp1 to ''
      setHyp1Data();
    }
  }

  /*Its a generic function to create Sectional Layout in Topics Page
  Provide datalabel and config label to get required structure
  */
  createSectionLayout({ dataObj, datalabel, configlabel, override, pagetype }) {
    configlabel = configlabel && configlabel != "" ? configlabel : datalabel;
    return dataObj ? (
      <div className={`row box-item ${pagetype}`}>
        <GridSectionMaker type={this.config.designConfigs[configlabel]} data={dataObj.items} override={override} />
      </div>
    ) : (
      <FakeListing />
    );
  }

  render() {
    const { isFetching, dispatch, router, params, value, error, profile } = this.props;
    const { query } = this.props.location;
    const topicsData = value;
    let canonical = value && value && value.pwa_meta && value.pwa_meta.canonical;
    let updatedCanonical = "";
    let pagetype = "";

    if (
      typeof params != "undefined" &&
      typeof params.searchtype != "undefined" &&
      params.searchtype &&
      params.searchtype == "ampdefault"
    ) {
      pagetype = "all";
      updatedCanonical = canonical.replace("/ampdefault", "");
    } else if (
      typeof params != "undefined" &&
      typeof params.searchtype != "undefined" &&
      params.searchtype &&
      params.searchtype == "ampnews"
    ) {
      pagetype = "news";
      updatedCanonical = canonical.replace("ampnews", "news");
    } else if (
      typeof params != "undefined" &&
      typeof params.searchtype != "undefined" &&
      params.searchtype &&
      params.searchtype == "ampphotos"
    ) {
      pagetype = "photos";
      updatedCanonical = canonical.replace("ampphotos", "photos");
    } else if (
      typeof params != "undefined" &&
      typeof params.searchtype != "undefined" &&
      params.searchtype &&
      params.searchtype == "ampvideos"
    ) {
      pagetype = "videos";
      updatedCanonical = canonical.replace("ampvideos", "videos");
    } else if (typeof params != "undefined" && typeof params.searchtype != "undefined") {
      pagetype = params.searchtype;
    } else {
      pagetype = "all";
    }

    let dtype = topicsData && topicsData.dtype && topicsData.dtype.toLowerCase();

    //For handling of default case of topics
    if (dtype === "default") {
      dtype = "all";
    }

    let moreTxt = siteConfig.locale.next_topic.more_all;
    if (pagetype == "all") moreTxt = siteConfig.locale.next_topic.more_all;
    else if (pagetype == "news") moreTxt = siteConfig.locale.next_topic.more_news;
    else if (pagetype == "photos") moreTxt = siteConfig.locale.next_topic.more_photos;
    else if (pagetype == "videos") moreTxt = siteConfig.locale.next_topic.more_videos;

    if (updatedCanonical) {
      this.props.value.pwa_meta.canonical = updatedCanonical;
    }

    // const businessType = (topicsData.pwa_meta && topicsData.pwa_meta.profile) || "Politician";
    // console.log("topicsData-----------", topicsData);
    const businessType = profile && profile.businessType ? profile.businessType.toLowerCase() : "";

    return topicsData ? (
      <div className="body-content section-topics" {...SeoSchema({ pagetype: pagetype }).attr().itemList}>
        {/* For SEO */ PageMeta(topicsData.pwa_meta)}

        {/* seodescription */}
        {/* {(topicsData.pwa_meta && topicsData.pwa_meta.seodescription) !=
        "undefined"
          ? WebTitleCard(topicsData.pwa_meta)
          : null} */}

        {/* For SEO Schema */}
        {SeoSchema().metaTag({
          name: "numberofitems",
          content: "20",
        })}

        <div className="row player-stats">
          <div className={`col12 ${profile == undefined ? "txt-wrap" : ""}`}>
            <div className="con_stats">
              {/* Topics Details */}
              {topicsData && topicsData.topicsdata ? (
                <ShortProfile
                  item={topicsData}
                  data={topicsData.topicsdata}
                  profile={profile}
                  businessType={businessType}
                  heading={topicsData.hl}
                />
              ) : null}
              <div className="profileDesc">
                {topicsData && topicsData.topicsdata && profile != undefined ? (
                  <ProfileDesc data={topicsData.topicsdata} item={topicsData} />
                ) : null}
              </div>
              {profile && Object.keys(profile).length > 0 && businessType === "cricketer" ? (
                <PlayerStats data={profile} />
              ) : null}
            </div>
          </div>
        </div>

        {/* Tablisting */}
        {topicsData && topicsData.tabs && Array.isArray(topicsData.tabs) ? (
          <ErrorBoundary>
            <TopicsCard
              data={topicsData.tabs}
              type="topicsTabs"
              params={params}
              dispatch={dispatch}
              router={router}
              query={query}
              dtype={dtype}
            />
          </ErrorBoundary>
        ) : null}

        {!isFetching || this.config.isFetchingNext ? (
          topicsData && topicsData.items && topicsData.items instanceof Array ? (
            this.createSectionLayout({
              dataObj: topicsData,
              configlabel:
                pagetype == "videos" || pagetype == "ampvideos"
                  ? "gridLead"
                  : pagetype == "photos" || pagetype == "ampphotos"
                  ? "gridView"
                  : "topicslisting",
              override: {
                noOfColumns: pagetype == "photos" || pagetype == "ampphotos" ? 2 : 1,
                adcols: pagetype == "photos" || pagetype == "ampphotos" ? 12 : null,
              },
              pagetype: pagetype,
            })
          ) : (
            <div key={topicsData.hl} className="no-data-found">
              <div className="nodata_txt_icon">
                <b />
                <span>No record found</span>
              </div>
            </div>
          )
        ) : (
          <FakeListing />
        )}

        {/* more-btn */}
        {topicsData && parseInt(topicsData.pg && topicsData.pg.cp) < parseInt(topicsData.pg && topicsData.pg.tp) ? (
          <AnchorLink
            hrefData={{ override: this.generateListingLink("moreButton") }}
            className={styles["more-btn"] + " more-btn"}
          >
            {siteConfig.locale.read_more_listing}
          </AnchorLink>
        ) : (
          ""
        )}

        {/* Breadcrumb widget for mobile starts after Web title */}
        {isMobilePlatform() && topicsData.breadcrumb && topicsData.breadcrumb.div ? (
          <Breadcrumb items={topicsData.breadcrumb.div.ul} />
        ) : null}
        {/* Breadcrumb widget for mobile ends after Web title */}
      </div>
    ) : (
      <FakeListing />
    );
  }
}

// fetchData
Topics.fetchData = function({ dispatch, query, params, router }) {
  dispatch(setPageType("topics"));
  return dispatch(fetchListDataIfNeeded(params, query, router));
};

// fetchNextListData
Topics.fetchNextListData = function({ dispatch, query, params, router }) {
  return dispatch(fetchNextListDataIfNeeded(params, query, router));
};

function mapStateToProps(state) {
  return {
    ...state.topics,
  };
}

export default connect(mapStateToProps)(Topics);
