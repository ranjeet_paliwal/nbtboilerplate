/* eslint-disable no-underscore-dangle */
import {
  getSplatsVars,
  isFeatureURL,
  renderDfpAds,
  getFilteredKeyword,
  wapadsGenerator,
} from "../../components/lib/ads/lib/utils";
import { _getStaticConfig } from "../../utils/util";

const siteconfig = _getStaticConfig();

/**
 * browser _sessionStorage implementation
 * @param {String} key session object key
 * @param {String} value session object value
 * */
export const _getMainJS = (js, ASSET_PATH, SITE_PATH, version, reqURL) => {
  const jsStr = `
    // A temporary fix for webpack error on new build
    window.onerror = function (msg, source, lineno, colno, error) {
      if (error && error.name === 'ReferenceError' && msg.indexOf('webpackJsonp is not defined') > -1) {
        window.location.reload(true);
      }
    };
    if(browser != ''){
      document.querySelector('body').setAttribute('user-agent', browser)
    }
    var jsArray = "${js}".split(',').map(function (js) {
      return "${ASSET_PATH}" + js;
    });
    var loadServiceworker = true;
    if ('deviceMemory' in navigator) {
      if (navigator.deviceMemory < 1) {
        loadServiceworker = false;
      }
    }

    function loadcommonjs() {
      // bot cache fix
      if(window && window.location
          && window.location.href && window.location.href.includes('webcache.googleusercontent.com')
            || typeof window == "undefined" && "${reqURL}".includes('webcache.googleusercontent.com'))
      {
        return true;
      }

      var appendScriptWithOnload = function (jspath ,callback ,syncJS) { 
        var _script = document.createElement("script");
        _script.defer = true;
        _script.src = jspath;
        if(callback){
          // Attach handlers for all browsers
          _script.onload = function(){callback(syncJS)};
          _script.onreadystatechange = function(){callback(syncJS)};
        }
        document.body.appendChild(_script);
      }

      var bootstrapOnLoad = function (){
        // analytics and gpt ads services are added in headwith defer
        // jsArray.push('https://www.google-analytics.com/analytics.js'/*,'https://www.googletagservices.com/tag/js/gpt.js'*/)
        var clientJs = '';

        for (var i = 0; i < jsArray.length; i++) {
          if(jsArray[i].indexOf('client') > -1 && process.env.NODE_ENV=='production'){
            clientJs = jsArray[i];
            continue;
          }
          if(jsArray[i].indexOf('vendor') > -1 && process.env.NODE_ENV=='production'){
            appendScriptWithOnload(jsArray[i], appendScriptWithOnload, clientJs);
          }else{
            appendScriptWithOnload(jsArray[i]);
          }
        }
      }
      
      bootstrapOnLoad();

      // if(process.env.NODE_ENV=='production'){
      //   for (var i = 0; i < jsArray.length; i++) {
      //     if(jsArray[i].indexOf('bootstrap') > -1){
      //       // appendScriptWithOnload(jsArray[i],bootstrapOnLoad);
      //       break;
      //     }
      //   }
      // }else{
      //   // bootstrapOnLoad();
      // }
    }

    //Method to handle lazy load in opera mini and Jio
    function load_img_opera(){
      var imgs = document.querySelectorAll('img');
      for (var i = 0; i < imgs.length; i++) {
        if(imgs[i].getAttribute('data-src')){
          imgs[i].setAttribute("src", imgs[i].getAttribute('data-src'))
        }
      }
    }

    function send_message_to_sw(msg) {
      return new Promise(function (resolve, reject) {
        // Create a Message Channel
        var msg_chan = new MessageChannel();
        var jsloaderFlag = true;
        // Handler for recieving message reply from service worker
        msg_chan.port1.onmessage = function (event) {
          if (event.data.error) {
            reject(event.data.error);
          } else if(jsloaderFlag){
            loadcommonjs();
            jsloaderFlag = !jsloaderFlag;
          }
        };

        // Send message to service worker along with port for reply
        // navigator.serviceWorker.controller.postMessage("Client 1 says '"+msg+"'", [msg_chan.port2]);
        navigator.serviceWorker.controller.postMessage(jsArray, [msg_chan.port2]);
      });
    }

    function swRegistration() {
      navigator.serviceWorker.register('/${SITE_PATH}service-worker.js?v=${version}').then(function (registration) {
        console.log('Service Worker registered! Scope: ' + registration.scope);
      }).catch(function (err) {
        console.log('Service Worker registration failed: ' + err);
        loadcommonjs();
      });

      window.setTimeout(function () {
        navigator.serviceWorker.ready.then(function (reg) {

          if (navigator.serviceWorker && navigator.serviceWorker.controller) {
            send_message_to_sw('Demo');
          } else {
            console.log("Controller not exist");
            loadcommonjs();
          }

          return reg.sync.register('syncHomePage');
        }).catch(function (err) {
          console.log('Service Worker on ready failed: ', err);
        });
      }, 500);
    }

    if(browser == 'operamini' || browser == 'jio'){
      load_img_opera();
    }
    else {
      if ('serviceWorker' in navigator && loadServiceworker) {
          if (process.env.SITE === 'mt') {
            // A POC to test impact of sw load on pagespeed  
            window.addEventListener('load', () => swRegistration());
          } else {
            if (document.readyState === "loading") {
              document.addEventListener("DOMContentLoaded", swRegistration);
            } else {
              // DOMContentLoaded already fired
              swRegistration();
            }
          }
          
          var deferredPrompt;
          //var btnAdd = document.getElementById('add_to_home');
          window.addEventListener('beforeinstallprompt', function (e) {
            // beforeinstallprompt Event fired
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            e.preventDefault();
            // Stash the event so it can be triggered later.
            deferredPrompt = e;
      
            // e.userChoice will return a Promise.
            e.userChoice.then(function (choiceResult) {
        
              //console.log(choiceResult.outcome);
              if (typeof ga == 'function') {
                if (choiceResult.outcome == 'dismissed') {
                  if(typeof window.manualAddToHomescreenPrompt != "undefined" && window.manualAddToHomescreenPrompt) {
                      if(window && window.dataLayer) 
                      { 
                            window.dataLayer.push({
                            'event':'tvc_pwainstall',
                            'eventCategory':'pwainstall',
                            'eventAction': document.location.pathname,
                            'eventLabel': 'dismiss-m'
                          })  
                      }
                      else { ga('send', 'event', 'pwainstall', document.location.pathname, 'dismiss-m') }
                      if (window && window.grx) {
                        window.grx("track","add_to_home_screen",{'status':"no"})
                      }
                   }
                   else
                   {
                      if( window && window.dataLayer) 
                      { 
                            window.dataLayer.push({
                            'event':'tvc_pwainstall',
                            'eventCategory':'pwainstall',
                            'eventAction': document.location.pathname,
                            'eventLabel': 'dismiss'
                          })  
                      }
                      else { ga('send', 'event', 'pwainstall', document.location.pathname, 'dismiss')}
                      if (window && window.grx) {
                        window.grx("track","add_to_home_screen",{'status':"no"})
                      }
                   }
                } else {
                  if(typeof window.manualAddToHomescreenPrompt != "undefined" && window.manualAddToHomescreenPrompt) {
                    if (window && window.grx) {
                      window.grx("track","add_to_home_screen",{'status':"yes"})
                    }
                      if(window && window.dataLayer) 
                      { 
                            window.dataLayer.push({
                            'event':'tvc_pwainstall',
                            'eventCategory':'pwainstall',
                            'eventAction': document.location.pathname,
                            'eventLabel': 'add-m'
                          })  
                      }
                      else { 
                         ga('send', 'event', 'pwainstall', document.location.pathname, 'add-m')  
                      } 
                    }
                  else
                  {
                    if (window && window.grx) {
                      window.grx("track","add_to_home_screen",{'status':"yes"})
                    }
                    if( window && window.dataLayer) 
                    { 
                          window.dataLayer.push({
                          'event':'tvc_pwainstall',
                          'eventCategory':'pwainstall',
                          'eventAction': document.location.pathname,
                          'eventLabel': 'add'
                        })  
                    }
                   else{
                      ga('send', 'event', 'pwainstall', document.location.pathname, 'add')
                    }
                  }
                
                }
              }
            });
          });

          // btnAdd.addEventListener('click', function(e){
          //     // hide our user interface that shows our A2HS button
          //     btnAdd.style.display = 'none';
          //     deferredPrompt.prompt();
          //     // Wait for the user to respond to the prompt
          //     deferredPrompt.userChoice
          //         .then((choiceResult) => {
          //             if (typeof ga == 'function') {
          //                 if (choiceResult.outcome === 'accepted') {
          //                     ga('send', 'event', 'pwainstall', document.location.pathname, 'dismiss');
          //                     console.log('User accepted the A2HS prompt');
          //                 } else {
          //                     ga('send', 'event', 'pwainstall', document.location.pathname, 'add');
          //                     console.log('User dismissed the A2HS prompt');
          //                 }
          //             }
          //             deferredPrompt = null;
          //         });
          // })

          //Add to homescreen is successful
          window.addEventListener('appinstalled', function (evt) {
              if (typeof ga == 'function') {
                if(typeof window.manualAddToHomescreenPrompt != "undefined" && window.manualAddToHomescreenPrompt) {
                  if( window && window.dataLayer) 
                  { 
                        window.dataLayer.push({
                        'event':'tvc_pwainstall',
                        'eventCategory':'pwainstall',
                        'eventAction': document.location.pathname,
                        'eventLabel': 'appinstalled-m'
                      })  
                  }
                  else{   ga('send', 'event', 'pwainstall', document.location.pathname, 'appinstalled-m') }
                }
                else
                {
                  if( window && window.dataLayer) 
                  { 
                        window.dataLayer.push({
                        'event':'tvc_pwainstall',
                        'eventCategory':'pwainstall',
                        'eventAction': document.location.pathname,
                        'eventLabel': 'appinstalled'
                      })  
                  }
                  else {  ga('send', 'event', 'pwainstall', document.location.pathname, 'appinstalled') }
                }
              }
          });
      } else {
          loadcommonjs();
      }
    }
  `;
  return jsStr;
};

export const _getPreRenderAdsJS = (pagetype, siteConfig, adconfig, pwaMeta) => {
  // console.log("=====_getPreRenderAdsJS====");
  // console.log("------------------------- :", adconfig);
  // console.log("siteConfig:",siteConfig);
  let waitEventStatus = false;
  if (
    adconfig &&
    adconfig.waitEvent &&
    adconfig.waitEvent[siteConfig.channelCode] &&
    adconfig.waitEvent[siteConfig.channelCode].active
  ) {
    waitEventStatus = adconfig.waitEvent[siteConfig.channelCode].active;
  }
  const keyword = pwaMeta && pwaMeta.key ? pwaMeta.key : "";
  const blacklist = pwaMeta && pwaMeta.blacklist && pwaMeta.blacklist === "true" ? "1" : "0";
  const pwaPagetype = pwaMeta && pwaMeta.pagetype ? pwaMeta.pagetype : "";
  const SCN = pwaMeta && pwaMeta.subsectitle1 ? pwaMeta.subsectitle1 : "";
  const SubSCN = pwaMeta && pwaMeta.subsectitle2 ? pwaMeta.subsectitle2 : "";
  const LastSubSCN = pwaMeta && pwaMeta.subsectitle3 ? pwaMeta.subsectitle3 : "";
  const msid = pwaMeta && pwaMeta.msid ? pwaMeta.msid : "";
  const pvukey = pwaMeta && pwaMeta.issex && pwaMeta.issex === "true" ? "1" : "0";
  let longurl = "";
  if (pagetype === "articlelist") {
    longurl = pwaMeta && pwaMeta.canonical ? pwaMeta.canonical.replace(siteConfig.weburl, "") : "";
  }
  const jsStr = ` 
    window.current_pagetype = '${pagetype}';
    window.channelCode = '${siteConfig.channelCode}';
    window.keyword = "${keyword}";
    window.blacklist = '${blacklist}';
    window.puvkey = '${pvukey}';
    window.pwaPagetype = '${pwaPagetype}';
    window.waitForAdsEvent = '${waitEventStatus}';

    window.tgtkeys = {};
    window.tgtkeys.keyword = "${keyword}";
    window.tgtkeys.BL = '${blacklist}';
    window.tgtkeys.puvkey = '${pvukey}';
    window.tgtkeys.templatetype = '${pwaPagetype}';
    window.tgtkeys.ctnkeyword = "${keyword}";
    window.tgtkeys.SCN = '${SCN}';
    window.tgtkeys.SubSCN = '${SubSCN}';
    window.tgtkeys.LastSubSCN = '${LastSubSCN}';
    window.tgtkeys.msid = '${msid}';

    window.meta = {};
    window.meta.longurl = '${longurl}';
    window.meta.adsec = '${pwaMeta && pwaMeta.key ? pwaMeta.adsec : "others"}';
    window.wapads = JSON.parse('${JSON.stringify(siteConfig.ads.dfpads.others)}');

    var preRenderElemArray = [];
    window.preRenderAds = function(){
      var wapadsGenerator = ${wapadsGenerator.toString()};
      window.wapads = wapadsGenerator({
        secname : window.meta.adsec,
        defaultWapAds : window.wapads,
        pagetype : window.current_pagetype
      });

      var _isFeatureURL = ${isFeatureURL.toString()}      
      // Switch off ads for Featured URLs.
      if(_isFeatureURL()) return false;

      var getSplatsVars = ${getSplatsVars.toString()}
      var splatsVars = getSplatsVars();

      var getFilteredKeyword = ${getFilteredKeyword.toString()}
      keyword = getFilteredKeyword(keyword);

      var renderDfpAds = ${renderDfpAds.toString()}  
      preRenderElemArray = [];
      var elemPrender = Array.prototype.slice.call(document.querySelectorAll('.prerender'));
      elemPrender.forEach(function(elem , i){
        if(elem && elem.innerHTML == ''){
            if (elem.getAttribute("data-adtype") == "atf") {
              var newElement = document.getElementById("general-atf-wrapper")
              if (newElement) {
                window.observeAtfSizeChange = true;
                observeResizeChange(newElement);
              }
            }
            preRenderElemArray.push(elem);                
            renderDfpAds(preRenderElemArray, elem, channelCode, i, keyword, blacklist, tgtkeys.templatetype , puvkey);      
        }else{
          console.log('.atf not found')
        } 
      })
    }
    preRenderAds();

    window.current_pagetype = '';
    window.channelCode = '';
    function observeResizeChange(elem) {
      var resizeObserver = new ResizeObserver(resizedEntry => {
        try {
          let resizedHeight = Math.max(resizedEntry[0].contentRect.height - 50, 0)
          resizedHeight = Math.ceil(resizedHeight);
          if (resizedHeight % 2 !== 0) {
            resizedHeight += 1;
          }
          transformElementsByPixel(resizedHeight);
        } catch (e) {
          console.log(e);
        }
      });
      resizeObserver.observe(elem);
      // The following code is to just ensure that if the div is loaded with height already greater 
      // than 50px before resize observer is initialized, it will take care of it
      // It will run only once while the observer is initialized
      if (elem.offsetHeight > 50) {
        let resizedHeight = Math.max(elem.offsetHeight - 50, 0)
          resizedHeight = Math.ceil(resizedHeight);
          if (resizedHeight % 2 !== 0) {
            resizedHeight += 1;
          }
          transformElementsByPixel(resizedHeight);
      }
    }

    function transformElementsByPixel(height) {
      if (document.getElementById("childrenContainer")) {
        document.getElementById("childrenContainer").style.transform = 'translateY('+ height +'px)';
      }
      if (document.getElementById("electionwidget")) {
        document.getElementById("electionwidget").style.transform = 'translateY('+ height +'px)';
      }
      if (document.getElementById("footer-wrapper")) {
        document.getElementById("footer-wrapper").style.transform = 'translateY('+ height +'px)';
      }
      let footerPadding = 80 + height;
      if (document.getElementById("mobileFooter")) {
        document.getElementById("mobileFooter").style.paddingBottom = footerPadding + 'px';
      }
    }
    
    `;
  return jsStr;
};

export const _getDeviceRelatedJS = siteConfig => {
  const jsStr = `
    //Check User Agent
    let ua=navigator.userAgent.toLowerCase();
    ua = ua.toLowerCase();
    let browser = '';

    // check if opera mini or JIO or UC
    if(ua.indexOf('presto') > -1){
      //Opera Browser
      browser = 'operamini';
    }else if(ua.indexOf('kaios') > -1){
      //Jio Phone
      browser = 'jio';
    }else if(ua.indexOf('ucbrowser') > -1){
      //UC Browser
      if(ua.indexOf('ucmini') > -1){
        browser = 'ucmini';
      }
      else{
        browser = 'uc';
      }
    }
  `;
  return jsStr;
};

export const _getibeatValue = pagetype => {
  const ibeatValue = pagetype == "articleshow" ? 1 : pagetype == "videoshow" ? 2 : 3;
  return ibeatValue;
};

export const _getMainCss = (css, ASSET_PATH, SITE_PATH) => {
  const cssStr = `
    if(typeof window != 'undefined'){
      var fileref=document.createElement("style");
      //check if browser "fetch" compatible
      if (typeof fetch == 'function') {
        fetch("${ASSET_PATH}${SITE_PATH}${css}").then(function(resp){
          if(resp.status == 200){
            resp.text().then(function(data){
              fileref.innerHTML = data;
              var head = document.querySelector('head');
              head.appendChild(fileref);
            });
          }
        })
      }	else{
        fileref=document.createElement("link");
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("media", "all")
        fileref.setAttribute("href", "${ASSET_PATH}${SITE_PATH}${css}") ;
        var head = document.querySelector('head');
        head.appendChild(fileref);
      }
    }
  `;
  return cssStr;
};

export const loadInterStitialJS = interStialdata => {
  const intScript = `
  function openInterStial(country, geoInfo) {
    if ((country && geoInfo.CountryCode == country) || !country) {
      window.location.replace("${process.env.WEBSITE_URL}defaultinterstitial.cms");
    }
  }

  function loadJS(url, callback) {
    const script = document.createElement("script");
    script.src = url;
    script.onload = script.onreadystatechange = function() {
        // Attach handlers for all browsers
        if (!script.loaded && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
          script.loaded = true;
          const endTime = new Date().getTime();
          const timeSpent = endTime - script.startTime;
          if (callback) {
            try {
              callback();
            } catch (e) {
              // to handle
              throw new Error("logger.error", e.stack);
            }
          }
          script.onload = script.onreadystatechange = null; // Handle memory leak in IE
        }
      };
    document.getElementsByTagName("head")[0].appendChild(script);
  }



  function getCookie(cookieName) {
    var cookieArray = document.cookie.split('; ').find(row => row.startsWith(cookieName));
    return cookieArray && cookieArray.split('=')[1];
  }

  function setCookie(name, value, expireTo) {
    var d = new Date();
    d.setTime(d.getTime() + expireTo * 24 * 60 * 60 * 1000);  
    var expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + "; " + expires ;
  }
 
  if (typeof document !== "undefined" &&
  typeof window !== "undefined"&&
  typeof navigator !== "undefined" &&
  document.referrer != "${process.env.WEBSITE_URL}defaultinterstitial.cms"  
  ) {
  try {
    var googleBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent);
    var fCapvalue = 0;
    var cookieValue = '';
    var cookieName = "instl_count";
     
    var status = "${interStialdata.status}";
    var frequency = "${interStialdata.frequency}";
    var country = "${interStialdata.country}";
   // console.log("loadInterStitialJS11", status,frequency, country);
    if (!googleBot) {
      const cookieValue = getCookie(cookieName); 
      
      if (cookieValue) {
        fCapvalue = cookieValue
      }
      if (parseInt(frequency) > fCapvalue) {
        var currentTime = 0;
        var diff = 0;
        if (typeof localStorage !== 'undefined' && !localStorage.getItem("intlastinvoked")) {
          localStorage.setItem("intlastinvoked", new Date().getTime());
        } else {
          var lastTime = window.localStorage.getItem("intlastinvoked");
          currentTime = new Date().getTime();
          diff = (currentTime - lastTime) / 1000;
        }

        // re-load interstial only when the diff B/W last invoke and current is more than 60 Sec/1 Min
        // this was done to restrict autoload of interstial when redirect from interstial to home page
      
        // console.log('diffff', diff, fCapvalue);
        if (diff >= 60 || fCapvalue == 0) {
          fCapvalue++;
          // set data in cookie
          setCookie(cookieName, fCapvalue, 1);  // 1 Day     

          // set data in session storage
          localStorage.setItem("intlastinvoked", currentTime);
         // console.log("ssssssssssssssss");


          if (getCookie("geo_data") && JSON.parse(getCookie("geo_data"))) {
            const geoInfo = JSON.parse(getCookie("geo_data"));
           // console.log("geoInfo11", geoInfo);
            openInterStial(country, geoInfo);
            
          } else {             
              loadJS("https://geoapi.indiatimes.com/?cb=1", () => {               
                const geoInfo = window.geoinfo;
                //console.log("Yeahh", geoInfo);
                openInterStial(country, geoInfo);
              });             
            }         
        }
      }
    }     
  } catch (ex) {
    console.log("Error:Interstial", ex);
  }
} `;

  return intScript;
};
