import { AnalyticsGA } from "../../components/lib/analytics/index";
import { _getCookie, _setCookie, _getStaticConfig } from "../../utils/util";
import { fireGRXEvent } from "../../components/lib/analytics/src/ga";
import globalconfig from "../../globalconfig";
const siteConfig = _getStaticConfig();
const ctnjs = siteConfig.ads.ctnAddress;
const ctntestjs = "https://static.clmbtech.com/ad/commons/js/colombia_test.js";

const geourl = "https://geoapi.indiatimes.com/?cb=1";
export const handleScroll = () => {
  const str = `
    window.addEventListener("scroll", handleScroll);
    function handleScroll() {
        const ctnjs = document.createElement("script");
        ctnjs.src = '${ctnjs}';
        ctnjs.defer = true;
        document.head.appendChild(ctnjs);
        
        window.removeEventListener("scroll", handleScroll);
    }`;
  return str;
};

export const loadCCaud = reqURL => {
  const str = `
  (function(){
    let dmpparam = "";
    if (getCookie("_col_uuid")) {
      dmpparam = '&fpc=' + getCookie("_col_uuid");
    } 
    if (getCookie("optout") === "1" || getCookie("optout") === "0") {
      dmpparam += '&optout=' + getCookie("optout");
    } 
    const ccaudUrl = 'https://ade.clmbtech.com/cde/aef/var=colaud?cid=${siteConfig.ccaudId}&_u=${siteConfig.weburl +
    reqURL}'+ dmpparam;
    const ccaudjs = document.createElement("script");
    ccaudjs.src = ccaudUrl;
    ccaudjs.async = true;
    document.head.appendChild(ccaudjs);
  })()
  `;
  return str;
};

export const getCookie = () => {
  const str = `
  window.getCookie = (cookieName) => {
    var cookieArray = document.cookie.split('; ').find(row => row.startsWith(cookieName));
    return cookieArray && cookieArray.split('=')[1];
  }
  `;
  return str;
};

export const setCookie = () => {
  const str = `
   window.setCookie = (name, value, expireTo, path) => {
    var d = new Date();
    d.setTime(d.getTime() + expireTo * 24 * 60 * 60 * 1000);  
    var expires = "expires=" + d.toUTCString();
    var cpath = "";
    if(path){
      cpath = "path=" + path;
    }else{
      cpath = "";
    }
    document.cookie = name + "=" + value + "; " + expires + "; " + cpath;
  }
  `;
  return str;
};

export const eraseCookie = () => {
  const str = `
   window.eraseCookie = (name) => {
      document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
    `;
  return str;
};

export const loadComscore = reqURL => {
  const str = `
  var csucfr = "";
  var isGDPRRegion = window.geoinfo && window.geoinfo.isGDPRRegion || false;
  // console.log("geoinfo-------------",window.geoinfo,isGDPRRegion);
  if (getCookie("ckns_policyV2") && isGDPRRegion) {
    csucfr = "&cs_ucfr=1";
  }
  document.getElementById("comscoreContainer").innerHTML = '<img src=https://sb.scorecardresearch.com/p?c1=2&c2=6036484&c4=${siteConfig.weburl +
    reqURL}&c9=${siteConfig.weburl}'+ csucfr +' />'`;
  return str;
};

export const loadGeoAPI = reqURL => {
  const geostr = `

  function checkGDPRRegion(){
    const arrGDPRContinents = ["EU"];
    let bool = false;
    if (arrGDPRContinents.indexOf(window.geoinfo.Continent) > -1) {
      bool = true;
    } else if (window.geoinfo.CountryCode === "US" && window.geoinfo.region_code === "CA") {
      bool = true;
    }
    window.geoinfo.isGDPRRegion = bool;
  }
      if(!getCookie("geo_data")){
        const geoapi = document.createElement("script");
        geoapi.src = "${geourl}";
        geoapi.defer = true;
        document.head.appendChild(geoapi);

        geoapi.onload = () =>{
          checkGDPRRegion();
          ${loadComscore(reqURL)};
          setCookie("geo_data",JSON.stringify(window.geoinfo),1,"/");
        }
      }else{
        window.geoinfo = getCookie("geo_data") && JSON.parse(getCookie("geo_data"));
        ${loadComscore(reqURL)};
      }
    `;
  return geostr;
};

export const checkForRelatedApps = () => {
  // Check to see if the API is supported.
  if (navigator && "getInstalledRelatedApps" in navigator) {
    console.log("installedApps");
    getRelatedApps();
  } else {
    console.log("installedApps not supported");
  }
};

const getRelatedApps = () => {
  navigator.getInstalledRelatedApps().then(relatedApps => {
    checkCookieValues(relatedApps);
    relatedApps.forEach(app => {
      // console.log("app--", app.id, app.platform, app.url);

      // app = { platform: "play" };

      let eventCategory;
      let eventAction;

      if (app && app.platform !== undefined) {
        if (app.platform === "play") {
          eventCategory = "apppresent";
          eventAction = "pwa";
          globalconfig.isAppInstalled = true;
        } else if (app.platform === "webapp") {
          eventCategory = "pwapresent";
          eventAction = "pwa";
        }
      }
      fireGTMInstallApp({ eventCategory, eventAction }, true);
    });
  });
};

const checkCookieValues = relatedApps => {
  const arrRelatedApps = [];
  const site = process.env.SITE;

  if (relatedApps.length === 0) {
    if (_getCookie(`${site}_apppresent`) !== undefined) {
      uninstallEvent("appuninstalled");
    }
    if (_getCookie(`${site}_pwapresent`) !== undefined) {
      uninstallEvent("pwauninstalled");
    }
  } else {
    relatedApps.forEach(app => {
      if (app.platform === "play") {
        arrRelatedApps.push("apppresent");
      } else if (app.platform === "webapp") {
        arrRelatedApps.push("pwapresent");
      }
    });
    if (_getCookie(`${site}_apppresent`) !== undefined && arrRelatedApps.indexOf("apppresent") === -1) {
      uninstallEvent("appuninstalled");
    } else if (_getCookie(`${site}_pwapresent`) !== undefined && arrRelatedApps.indexOf("pwapresent") === -1) {
      uninstallEvent("pwauninstalled");
    }
  }
};

const uninstallEvent = category => {
  let eventCategory;
  let eventAction;
  const site = process.env.SITE;
  let installCategory = "";

  if (category === "appuninstalled") {
    eventCategory = "appuninstalled";
    eventAction = "pwa";
    installCategory = "apppresent";
  } else if (category === "pwauninstalled") {
    eventCategory = "pwauninstalled";
    eventAction = "pwa";
    installCategory = "pwapresent";
  }
  // Deleting app install cookie
  document.cookie = `${site}_${installCategory}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;

  // Deleting uninstall cookie incase user have it in cookie data
  if (_getCookie(`${site}_${eventCategory}`) !== undefined) {
    document.cookie = `${site}_${eventCategory}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
  }

  fireGTMInstallApp({ eventCategory, eventAction });
};

const fireGTMInstallApp = (params, boolSetcookie) => {
  const { eventCategory, eventAction } = params;
  const site = process.env.SITE;
  if (_getCookie(`${site}_${eventCategory}`) === undefined) {
    // Fire Grx event for app_status
    fireGRXEvent("track", "app_status", { status: eventCategory });
    AnalyticsGA.event({
      category: eventCategory,
      action: eventAction,
      label: "NA",
    });
  }
  if (boolSetcookie) {
    _setCookie(`${site}_${eventCategory}`, 1, 365);
  }
};

export const dfpBackupForCTN = () => {
  const str = `
    window.dfp_over_ctn = true;
    var updateDFPBidValue  = function(bidvalue, dfpslot, colombiadcontainerid, key){
      var publishToCtn = function(flag,containerid){
        document.getElementById(containerid).parentElement.style.display = "";
        colombia && colombia.setdfpstatus(flag, containerid);
      }
      
      var dfpSlotSize = JSON.stringify([300, 250]);
      if(document.getElementById(colombiadcontainerid) && document.getElementById(colombiadcontainerid).getAttribute("data-dfpsize")){
        dfpSlotSize = document.getElementById(colombiadcontainerid).getAttribute("data-dfpsize");
      }
      googletag.cmd.push(function() {
        var target_slot = googletag.defineSlot(dfpslot, JSON.parse(dfpSlotSize), colombiadcontainerid).addService(googletag.pubads());
        
        // this ecpm value would be dynamically set by CTN depending on the highest bid
        target_slot.setTargeting(key, bidvalue);
        googletag.pubads().enableLazyLoad({fetchMarginPercent: -1, renderMarginPercent: -1,});
        googletag.enableServices();
    
        googletag.pubads().addEventListener('slotRenderEnded', function(event) {
          if (event.slot == target_slot) {
            // console.log('dfp event rendered for element - ', colombiadcontainerid);
            if (event.isEmpty) {
              // console.log("Unfilled impression, call function to load backup ads");
              googletag.destroySlots([target_slot]);
              publishToCtn('fail',colombiadcontainerid);
            }else{
              publishToCtn('success',colombiadcontainerid);
            }
          }
        });
      });
      googletag.cmd.push(function() { googletag.display(colombiadcontainerid); });
    }
  `;

  return str;
};
