/* eslint-disable eqeqeq */
/* eslint-disable indent */
/* eslint-disable camelcase */
const globalconfig = require("../../globalconfig");
const criticalCss = (portalName, themeStyle, pagetype, reqURL) => {
  // const sprite = `https://static.${portalName}.indiatimes.com/img/pwa_sprite.svg?v=${getCssVersion()}`; // sprite path
  // const sprite = spritepath && spritepath !== "" ? spritepath : getCssPath(portalName, "pwa_sprite");
  // const spritesize = "77px 1050px"; // sprite size
  const isTimesPointsPage = reqURL && reqURL.indexOf("timespoints.cms") > -1;
  const isFromApp = reqURL && reqURL.indexOf("frmapp=yes") > -1;

  let bodyPadding = 0;
  if (globalconfig.topatfEnabled && globalconfig.alaskaMastHeadEnabled) {
    bodyPadding = 196;
  } else if (globalconfig.topatfEnabled || globalconfig.alaskaMastHeadEnabled) {
    bodyPadding = 146;
  } else {
    bodyPadding = 96;
  }
  if (pagetype === "videoshow") {
    bodyPadding -= 46;
  }
  if (isTimesPointsPage) {
    bodyPadding -= 100;
  }
  if (isFromApp) {
    bodyPadding = 0;
  }
  // let atfadstyle = "";
  //   if (globalconfig.isExpando) {
  //     atfadstyle = `
  //     .ad1.esi{min-height:${parseInt(globalconfig.atfheight) + 5}px;padding:0;line-height:${parseInt(
  //   globalconfig.atfheight,
  // ) + 5}px; box-sizing: border-box; margin:2%;}
  //     `;
  //   } else {
  const atfadstyle = `
    .ad1.esi{padding:0; box-sizing: border-box; margin:2%;}
    `;
  // }
  const atfAd = `
                  ${atfadstyle}
                  .ad1.esi .andbeyond_adunit {overflow:visible !important;}
                  .ad1.esi iframe{vertical-align: middle !important;}
                  .ad1.esi.expando{overflow:visible; z-index:1}
                  .atf-wrapper {margin: 10px 0;padding: 0;height: 50px;}
                  .transform {transition: all 0.5s ease-in; will-change: transform}
                  .off-transform {transform: none !important; will-change: unset;}
                `;

  const homeCss = `
          .nbt-list [data-plugin=ctn]{padding-bottom: 2%!important; border-bottom: 1px solid #dfdfdf !important;}
  `;

  const commonCss = `
            html {-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%}

            @media (max-width: 319px) {
            html {font-size: 56.25%;}
            }

            @media (min-width: 320px) and (max-width: 359px) {
            html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ), (min--moz-device-pixel-ratio : 3 ), (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
            html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ), (max--moz-device-pixel-ratio : 2.9 ), (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
            html {font-size: 68.75%;}
            } 

            #__SVG_SPRITE_NODE__ {display:none;}
            .svgiconcount {display:inline-flex; width:100%; height:100%;}
            .svgiconcount svg {margin:auto;}
            #izmiddle-box, #_aff_inc__rnd {position:relative; z-index: 999;}

            body {margin: 0; padding: 0; font-size: 1.6rem; background: #e4e4e4; color: #000; font-family: Noto Sans, Arial,Helvetica,sans-serif;padding-top:${bodyPadding}px;}
            body.webview .navbar, body.webview .footer, body.webview .breadcrumb , body.webview .landscape-mode , body.webview [data-plugin=ctn] , body.webview .btn_openinapp, body.webview .share_container .share_whatsapp, body.webview .share_container .share_icon, body.webview .comment_wrap, body.webview .con_fbapplinks{display: none !important;} 
            body.webview .master-player-container.enable-video-show .player-options{left:0; width:auto; margin:auto; text-align:right; min-height:24px}
            body.webview .master-player-container.enable-video-show .player-options .dock_icon{margin:0 45px 0 0; display:none}
            
            body.election-widget-result .el_rest_page, body.election-widget-result .fbnad, body.election-widget-result .ad1, body.election-widget-result .alaskamasthead, body.election-widget-result .timespoint-popup{display:none !important;}
            body.election-widget-result .share_container .el_resulttable{bottom:165px; border-radius: 15px 0 0 15px;}
            .fbnad{position:fixed;bottom:0;width:100%;}
            .clear{clear:both}

            body.webview .Cube{left:-2000px}
            body.webview .wdt_superhit, body.webview .keywords_wrap, body.webview .webtitle, body.webview .recipeArticle .con_review_rate{display:none !important}
            img {border: 0; vertical-align: bottom;}
            a{color:#000; text-decoration: none;}
            a:focus, a:hover, a:active {outline:none; color:#000; cursor: pointer; text-decoration: none;}

            .row{display:flex;flex-wrap:wrap;align-content:flex-start;margin-right:-15px;margin-left:-15px;}
            .row .col{margin:0;}
            .row .col1, .row .col2, .row .col3, .row .col4, .row .col5, .row .col6, .row .col7, .row .col8, .row .col9, .row .col10, .row .col11, .row .col12{display:flex;flex-wrap:wrap;align-content:flex-start;padding-right:15px;padding-left:15px;box-sizing:border-box;}
            .row .col12{flex:0 0 100%;max-width:100%;}
            .row .col11{flex:0 0 91.66%;max-width:91.66%;}
            .row .col10{flex:0 0 83.33%;max-width:83.33%;}
            .row .col9{flex:0 0 75%;max-width:75%;}
            .row .col8{flex:0 0 66.66%;max-width:66.66%;}
            .row .col7{flex:0 0 58.33%;max-width:58.33%;}
            .row .col6{flex:0 0 50%;max-width:50%;}
            .row .col5{flex:0 0 41.66%;max-width:41.66%;}
            .row .col4{flex:0 0 33.33%;max-width:33.33%;}
            .row .col3{flex:0 0 25%;max-width:25%;}
            .row .col2{flex:0 0 16.66%;max-width:16.66%;}
            .row .col1{flex:0 0 8.33%;max-width:8.33%;}
            .row .mr0, .row.mr0{margin:0;}
            .row .pd0, .row.pd0{padding:0;}


            .news-card.lead{width:100%;margin-bottom:4%;}
            .news-card.lead .img_wrap{display:block;}
            .news-card.lead .img_wrap img{width:100%;}
            .news-card.lead .con_wrap{display:block;margin:10px 3% 0;}
            .news-card.lead .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
            .news-card.lead .con_wrap .text_ellipsis{font-size:1.5rem;line-height:2.5rem;max-height:7.5rem;-webkit-line-clamp:3; word-break: break-word;}
            
            .news-card.horizontal{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;width:100%;}
            .news-card.horizontal .img_wrap{margin:0 10px 0 0;position:relative;}
            .news-card.horizontal .img_wrap img{width:80px;}
            .news-card.horizontal .con_wrap .section_name{color:#1f81db;font-size:1.3rem;display:block;margin-bottom:5px;}
            .news-card.horizontal .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;-webkit-line-clamp:2; word-break: break-word;}
            
            .news-card.horizontal-lead{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;flex-wrap:nowrap;width:100%;}
            .news-card.horizontal-lead .img_wrap{margin:0 10px 10px 0;position:relative;}
            .news-card.horizontal-lead .img_wrap img{width:140px;}
            .news-card.horizontal-lead .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
            .news-card.horizontal-lead .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3; word-break: break-word;}
            
            .news-card.vertical{margin-bottom:20px;}
            .news-card.vertical .img_wrap{margin:0 0 10px 0;position:relative;display:block;}
            .news-card.vertical .img_wrap img{width:154px;}
            .news-card.vertical .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
            .news-card.vertical .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3; word-break: break-word;}

            .news-card.only-info{display:flex;border-bottom:1px solid #dfdfdf;margin:0 3% 4%;padding-bottom:4%;width:100%;}
            .news-card.only-info img{display:none;}
            .news-card.only-info .con_wrap .section_name{font-size:1.3rem;color:#1f81db;display:block;margin-bottom:5px;}
            .news-card.only-info .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:6.6rem;-webkit-line-clamp:3; word-break: break-word;}
            .news-card .con_wrap a, .news-card .img_wrap a {display:block;}

            #comscoreContainer{height:0px}
            #childrenContainer{overflow-x: hidden;}
            .wdt_social_share svg{display:none; width:48px; height:48px}

            ._hide{text-indent: -1000px;position: absolute;}
            .mobile_body .hideSecName li.news-card .con_wrap .section_name{display:none}
            .mobile_body .mobilehide{display:none}
            .mobile_body .selectCity_wrapper{min-height: 59px;}
            .wdt_select_city, .sel-city-content{display:none}
            .vd_pl{width: 300px;margin: auto; height:255px}
            .ad1.fbn{position: fixed; bottom: 0;left: 0;right: 0;background:#e4e4e4; padding: 1% 2%; z-index: 99; transition: all 350ms ease-in-out; min-height: 50px;}
            .ad1, [data-plugin=ctn]{padding: 2%; text-align: center; position: relative; z-index: 0;overflow: hidden;min-height:50px;}
            .mobile_body [data-slotname=ctnbighome]{min-height:250px;box-sizing: border-box;margin: 2%;padding: 0;}
            .fb-ads {padding: 2%; text-align: center; position: relative; z-index: 0;overflow: hidden;min-height:50px;}
            .ad1.emptyAdBox {padding: 0 ; margin: 0 ; min-height: 0px; line-height:0; overflow:hidden;}
            .ad1.emptyAdBox:after{display: none;}
            .colombiaFail:after{display: none;}
            .fixedatf{top: 40px; position: fixed; z-index: 100;background: #f5f5f5; width:100%}
            .ad1:empty{padding:0; min-height:0}
            .ad1:empty:after{display: none;}
            [data-plugin=ctn]:empty{min-height:1px}
            .ad1.fbn:empty{min-height:50px;padding: 1% 2%;}
            .ad1.sitesync{z-index:1000;}
            .ad1.fbn:after, .ad1.esi:after{display:none}  
            .ad1.topatf{padding:0; min-height:${globalconfig.topatfEnabled ? "50" : "0"}px;height:${
    globalconfig.topatfEnabled ? "50" : "0"
  }px;background: #e4e4e4;}
            #headerContainer{position: fixed; top:0;z-index:99;left:0; right:0;}
            .top_fixed50{transform:translateY(-50px)}
            .top_fixed100{transform:translateY(-100px)}
            .top_fixed150{transform:translateY(-150px)}
            ${atfAd != "" ? atfAd : null}
            .liveblog-content{min-height:132px;background-color:#ebebeb;margin-bottom:4%;width:100%}

            .ad-wrapper-250{min-height:250px;display:block!important;}
            .ad-wrapper-275{height:275px; overflow-y:scroll;}
            .section-wrapper .top-news-content {padding-top: 4%;}
            .section-wrapper .top-news-content .news-card.lead.col6 .img_wrap{min-height:112px}
            .section-wrapper .top-news-content .news-card.lead.col6 .con_wrap .text_ellipsis{overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical;}
            .section-wrapper .top-news-content .nbt-list [data-plugin=ctn]{min-height:90px; width:100%;padding: 0 0 4%;margin: 0 3% 4%;}
            [data-slotname=ctnhomevideo]{height:250px;}
            [data-slotname=ctnbccl]{height:114px;}
            [data-slotname=ctnhometop]{height:90px;}
            .top-news-content, .box-content {margin: 0 0 4%; background: #fff; border-bottom: 2px solid #c6c6c6; padding: 0 0 3%;}
            .top-news-content .nbt-list.col12.pd0{display:flex !important}
            .box-item{margin-bottom: 4%; background: #fff; padding-bottom:3%; overflow:hidden}
            ul {margin: 0; padding: 0; list-style-type: none;}
            li.nbt-listview {margin: 0 3% 4%; padding-bottom: 4%; width:auto; position: relative; border-bottom: 1px solid #efefef;}
            li.nbt-listview .img_wrap{position:relative; width:32%; vertical-align:top;display: table-cell;}
            li.nbt-listview .img_wrap img{vertical-align:bottom; width: 100%; height: auto;}
            li.nbt-listview .con_wrap {text-align: left; word-break: break-word; padding: 0 0 20px 10px; vertical-align: top; position: relative; color: #333;}
            li.nbt-listview .text_ellipsis{font-size: 1.6rem; line-height: 2.4rem;}
            li.nbt-listview .time-caption {font-size: 1.1rem; line-height: 1.2rem; display: block; position: absolute; bottom: 0; font-weight: normal;color:#bababa;}

            li.nbt-listview.lead-post {border-bottom: 0; padding-bottom: 0; position: relative; margin: 0 0 4%;}
            li.nbt-listview.lead-post, li.nbt-listview.lead-post .table_row, li.nbt-listview.lead-post .table_col {display: block; width: auto; float: none;}
            li.nbt-listview.lead-post .img_wrap{min-height:178px}
            li.nbt-listview.lead-post .con_wrap{margin:0 3% 4%;}
            li.nbt-listview.lead-post .text_ellipsis{font-size:2.0rem;line-height:3.0rem;}
            li.nbt-listview.lead-post .time-caption{position:static}

            .nbt-list li.nbt-listview.section-name .news_section{padding:0 0 0 10px; left:32%; position: absolute; top:0; z-index:10;}
            .nbt-list li.nbt-listview.section-name .news_section a{font-size: 1.4rem; line-height: 2.2rem;}
            .nbt-list li.nbt-listview.lead-post.section-name .con_wrap{padding: 15px 0 0 0}
            .nbt-list li.nbt-listview.section-name .con_wrap{padding-top: 25px}
            .nbt-list li.nbt-listview.lead-post.section-name .news_section{padding:0 0 5px 3%; left:0; position: absolute; bottom:0; z-index:10; top:auto;}
            .nbt-list li.nbt-listview.lead-post.section-name .news_section a{font-size: 1.4rem; line-height: 2.2rem;}
            .nbt-list li.nbt-listview.lead-post.section-name .con_wrap{padding-bottom: 35px}
            
            .section {width: 100%; padding: 0 15px;}
            .section .top_section {position:relative; margin: 0 3% 4%;}
            .section .top_section .read_more{position: absolute;right: 0;bottom: 10px;font-size: 13px;}
            
            .section h1, .section h2 {font-size: 1.8rem; border-bottom: 2px solid #dfdfdf; margin:0}
            .section h1 span, .section h2 span {height: 40px; line-height: 40px; display: inline-block}
            .section ul.sub-list li a {font-size: 1.2rem; line-height: 2.1rem; display:block}
            .sectionHeading {background: #fff;}
            .sectionHeading h1{font-size: 1.8rem; margin:0 3% 4%; padding-top: 3%; font-weight: bold; display: block; border-bottom: 2px solid #dfdfdf; height: 40px; position: relative;}

            li.nbt-listview.videoshowview h3{font-size: 1.6rem; font-weight: 600;}
            li.nbt-listview.videoshowview.lead-post{margin: 0 0 4%; border-bottom: 5px solid #e4e4e4;}
            li.nbt-listview.videoshowview.lead-post .img_wrap{min-height:auto}
            li.nbt-listview.videoshowview.lead-post .con_wrap{position:static; background: none; color:#333333}
            li.nbt-listview.lead-post .text_ellipsis{display: -webkit-box;}
            li.nbt-listview.videoshowview .description{font-size: 1.6rem; line-height: 2.4rem; margin: 7px 0 10px; font-weight: normal;}

            .tophighlight, .lbhighlight{background:#ebebeb;}
            .top-article{padding:3%;margin:0 0 4%;}
            .top-article h2{font-size:16px;margin:0 0 5px;}
            .top-article li{position:relative;padding:0 0 0px 15px;font-size:1.6rem;line-height:2.4rem;margin:0 0 10px;}
            .story-article .tophighlight .btn_open_in_app{display: block; height: 36px; margin: 10px 0 0; visibility: hidden;}

            .table{display:table;width:100%;}
            .table .table_row{display:table-row;vertical-align:middle;}
            .table .table_col{display:table-cell;vertical-align:middle;}
            .election_cube, .in_focus_widget, .live_blog_widget, .highlights_widget{display:none}
            .alaskamasthead {text-align:center;background: #e4e4e4;}
            .alaskamasthead img {max-width:100%; height: 50px;} 
            .mobile_body .articlelist .pdwidget{display:none}

        
            .pie-chart{width:315px; height: 360px; margin: 0 auto;}
            .chartcountnew {width:315px; height: 330px;}
            .pie-chart .legend {margin-top:-40px;}
            .pie-chart .powerbycont {margin:8px auto;}
            .networkContainer {position: fixed;bottom: 0;left: 0;right: 0;padding: 10px 3%;visibility:hidden;}
            .mobile_body .section-wrapper .top-news-content .news-card.lead.col6 .con_wrap{margin-left: 0;margin-right: 0;}
            .box-item:empty {display:none;}
            .box-item .col12:empty {display:none;}
            
           
            .toastmessage{background: #3b4045; color:#fff; position: fixed; bottom: 0; left: 0; right: 0; z-index: 1005; opacity: .95; padding: 10px 3%; text-align:center; font-size:15px; line-height:25px; font-family:arial;}
            .toastmessage a, .toastmessage a:hover, .toastmessage a:focus{text-decoration:underline; color: #fff;}
            .toastmessage button.okBtn{border: 0; outline: none; height: 25px; width: 50px; text-align: center; font-size: 12px; font-family: arial; font-weight: bold; cursor: pointer; line-height: 25px; margin-left: 15px; color:#000; background:#fff}
            .box-item:empty {display:none;}
            .card_breaking_news {position: relative;margin: 0 0 2%; height:45px}
            .breaking_news_card_placeholder{height: 45px; margin: 0 0 2%;background:#000}
            .breaking_news_card_placeholder .head_wrap {width: 4%;display: inline-block;height: 100%;}
            .breaking_news_card_placeholder .con_wrap {width: 88%;display: inline-block;height: 100%;}
            .breaking_news_card_placeholder .btn_wrap {display: inline-block;height: 100%;background: #454545;width: 8%;}
            .news-card.horizontal.con_ads {min-height:86px;padding: 0 0 3%;}
            .section ul.sub-list {position: relative;white-space: nowrap;}
            .section ul.sub-list li {display: inline-block;vertical-align: top;margin: 0 25px 5px 0;}
            .hideFooter{overflow: hidden; height: 0px; box-sizing: border-box; padding: 0;}
            li.nbt-horizontalView.app-exclusive{background: #121212; margin: 0 0 4%; padding: 5px 3% 22px; width: 100%; box-sizing: border-box;position:relative;}
            li.nbt-horizontalView.app-exclusive ul.nbt-list{white-space: nowrap;}
            li.nbt-horizontalView.app-exclusive .app-txt{font-size: 1.2rem; display: inline-block; width: 25%; background: #fed20a; color: #000; margin: auto; right: 0; left: 0; position: absolute; bottom: 9px; height: 27px; line-height: 31px; border-radius: 20px; text-align: center; padding: 0 10px; white-space: nowrap; font-weight: bold;}
            li.nbt-horizontalView.app-exclusive span.article-text {font-size: 1.4rem; line-height: 2.2rem; margin: 3px 0 5px; color:#fff;display:inline-block;font-weight:600;}
            li.nbt-horizontalView.app-exclusive ul.nbt-list li.nbt-listview .img_wrap{height:90px;width:120px}
            li.nbt-horizontalView .nbt-list li.nbt-listview {margin: 0 2px 0 0;padding-bottom: 0;display: inline-block;}
            .briefbox .slide_list .article-section.slide div.top_content a span.img_wrap{height: 56vw;display: block;}
            .mobile_body .wdt_poll{min-height:345px;}
            .movielist .movielisting .news-card.horizontal .img_wrap {height: 160px;width: 120px;}
            .mobile_body .movielisting .news-card.horizontal .con_wrap span {display: block;margin-bottom: 5px;word-break: break-word;}
            .mobile_body .movielisting .news-card.horizontal .con_wrap .title {font-size: 1.6rem;line-height: 2.4rem;margin-bottom: 10px;}
            .text_ellipsis {overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2;}
            .mobile_body .card_breaking_news .trending_keywords h3 {display: none;}
            .mobile_body .card_breaking_news .trending_keywords {display: inline-block; width:auto}
            .card_breaking_news .head_wrap {font-size: 1.2rem;text-align: center;}
            .card_breaking_news .head_wrap {width: 22%;display: table-cell;height: 100%;background: #d0021b;}
            .card_breaking_news .con_wrap {width: 70%;display: inline-block;height: 100%;}
            .card_breaking_news .btn_wrap {display: inline-block;height: 100%;background: #454545;width: 8%;}
            .card_breaking_news .con_wrap .bnew_text {line-height: 3.5rem;}
            .card_breaking_news .con_wrap .bnew_text {white-space: nowrap;}
            .breadcrumb span {text-transform: capitalize;}
            .mobile_body .articlelist .top_atf {padding: 10px 3% 4%; position: relative;}
            .mobile_body .articlelist .top_atf .caption {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem;}
            .mobile_body .articlelist .top_atf .rdmore {font-size: 1.4rem; padding: 5px; position: absolute; right: 3px; bottom: 10px;}
            .youtube-iframe{position:relative;width:180px;height:25px;display:flex;align-items:center;z-index:10;}
            .youtube-iframe .txt{font-family:arial;font-size:12px;color:#000;text-transform:capitalize;}
            .youtube-iframe iframe{position:absolute;width:118px;height:25px;top:0;left:65px;z-index:100;}
            .youtube-iframe iframe:hover{height:150px;width:250px;}
            .fixed_bar_with_app{display:none}
            .tp_icondiv{vertical-align:middle; margin: 0; display: inline-block; text-align: right; width: 10%;}
            .default-outer-player{height:200px; width:100%; position:relative;margin:auto; background-size: contain; background-repeat: no-repeat; background-position: center center;}
            .liveblink_newscard {display: inline-block; position: relative; font-size: 12px; text-transform: uppercase; padding-left: 13px; color: red; font-family:sans-serif;}
            .news-card.lead .liveblink_newscard {display:block;}
    `;

  const headerCss = `
            .header {background: #fff; height:50px; position: relative;}
            .header .logo {float: left; margin: 0 0 0 20px; position: relative; height: 50px;}
            .header .logo h1, .header .logo h2{margin:0; padding:0}
            .header .logo a {display: block; font-size: initial; height:50px; line-height:50px}
            .header .logo img {vertical-align:middle;}
            .header .logo.logo-gadget img{width:116px}
            .header .logo.logo-tech img{width:74px}
            .header .logo .txt-seo{font-size: 1px; display: block; text-indent: -100000px; height: 1px; color:#fff;}
            .header .hamburger {cursor: pointer; width: 18px; height: 32px; float: left;margin: 16px 0 0 15px;}
            .hamburger i {background-color: #000; border-radius: 2px; content: ''; display: block; width: 18px; height: 2px; margin-top: 3px; background-color: #000;}
            .header #search-chk, .footer .footlinks-wrapper{display:none;}
            .header #srchFrm{z-index:-1; transition: all .25s ease-in-out; -moz-transition: all .25s ease-in-out; -webkit-transition: all .25s ease-in-out; opacity:0}

            .masthead-wrapper{position:relative; height:75px; overflow:hidden; background:#1c1c1e}  
            .masthead-wrapper a{display:block}
            .masthead-wrapper .corona-numbers.mobile{position:absolute; top:0; bottom:0; left:0; right:0; margin:auto; color:#fff; font-family:arial}
            .masthead-wrapper .corona-numbers.mobile .name{font-size:10px; text-transform: uppercase; display:block; margin-bottom:5px}
            .masthead-wrapper .corona-numbers.mobile .odometer{min-height:20px; color:#6c6c6c; font-weight:bold; box-shadow: none; font-weight:normal}
            .masthead-wrapper .corona-numbers.mobile .heading{text-align:left; text-transform: uppercase; margin: 0 0 6px 0; display: flex; justify-content: space-between; border-bottom:1px solid #3b3b3b}
            .masthead-wrapper .corona-numbers.mobile .heading h4{font-size:14px; margin:5px 0 5px 10px}
            .masthead-wrapper .corona-numbers.mobile .heading h4 b{font-size:14px; font-weight:normal; color:#b4b4b4}
            .masthead-wrapper .corona-numbers.mobile .heading span{font-size: 12px; padding: 7px 10px 0 10px; background:#000}
            .masthead-wrapper .corona-numbers.mobile .confirmed{width:33%; text-align:center; border-right:1px solid #3b3b3b; display:inline-block; vertical-align:top;}
            .masthead-wrapper .corona-numbers.mobile .confirmed .name{color:#66a6f3}
            .masthead-wrapper .corona-numbers.mobile .recovered{width:33%; text-align:center; border-right:1px solid #3b3b3b; display:inline-block; vertical-align:top;}
            .masthead-wrapper .corona-numbers.mobile .recovered .name{color:#42ef82}
            .masthead-wrapper .corona-numbers.mobile .deaths{width:33%; text-align:center; display:inline-block; vertical-align:top;}
            .masthead-wrapper .corona-numbers.mobile .deaths .name{color:#fb2b2b}

            .header .icons {float: right; margin: 13px 15px 0 0; min-width:20px}
            .header .apna_bazaar_header{display:none}
            .header .icons.app_download{min-width:23px}
            .header .app_download svg{width: 42px; height: 23px;}
            .header .app_download svg.ios, .header .app_download svg.android {width: 23px; height: 23px;}
            .header .dropDown_city svg{width: 20px; height: 23px; color:red;}
            .appicotext {font-size: 14px; transform: translate(10px, 9px); font-family: auto; font-weight:bold;}
            .appicotext.ta, .appicotext.bn, .appicotext.kn, .appicotext.te {font-size:12px; transform: translate(6px, 7px);}
            .appicotext.ml {font-size:10px; transform: translate(7px, 6px);}
            .header .searchbox{position:absolute; margin:auto; top:0; left:0; right:0; display: block;}
            .header .searchbox .txtbox{width:100%; background:#fff;}
            .header .searchbox input[type=text]{display:block;padding: 16px 0px 15px 4%; width: 96%; border: 0; color: #a9a9a9; font-size: 1.4rem; outline:none; font-family: arial;}
            .header .search input[type=checkbox]#search-chk:checked + #srchFrm{opacity: 1;  z-index:100;}
            .header .search input[type=submit]{background: transparent; border: 0; height: 24px; width: 24px;color:transparent;}

            .topdrawer_content{max-height: 0; transition: max-height 0.4s ease-out; overflow: hidden;}
            .topdrawer_content.visible {max-height: 250px; transition: max-height .5s ease-in;}
            .topdrawer_content .inner{padding: 10px 4% 5px; background: #F2F2F2; border-top:1px solid #e2e2e2;}
            .topdrawer_content ul{display:flex; justify-content: space-between;}
            .topdrawer_content ul li{display: inline-block; width: 20%;  text-align: center; vertical-align: top}
            .topdrawer_content ul li a{display: block;padding:0;}
            .topdrawer_content ul li span.txt{margin: 2px 0 0; display: block; font-size: 1.1rem; line-height: 1.5rem; color: #666666;}
            .topdrawer_content .td-icons svg {width:28px; height:28px;}

            .search_icon svg {width: 24px; height: 24px; display: inline-block;}
            .home_icon {width: 24px; height: 24px; display: inline-block; vertical-align: top;}

            .hrnavigation {background: #fff; border-top: 1px solid #d5d5d5;}
            .hrnavigation h3{display: block; margin:0; padding:0}
            .hrnavigation h3 a{position: absolute; width: 45px; height: 35px; text-align: center; padding-top: 10px; opacity: 0.5;}
            .hrnavigation .nav_scroll {display: block; white-space: nowrap; margin: 0; padding: 0; width: calc(100% - 40px); overflow-y: scroll; height: 45px;margin-left: 40px;}
            .hrnavigation ul {margin: 0; padding: 0; list-style: none; position: relative;}
            .hrnavigation ul li {display: inline-block; margin: 0 5px; padding: 0; text-align: center; vertical-align: middle; width: auto; min-height: 43px; border-bottom: solid 2px transparent;}
            .hrnavigation ul li a.nav_link {display: flex; align-items: center; height:40px; font-size: 1.6rem; line-height: 2.6rem; padding: 2px 8px 0; box-sizing: border-box;}
            .hrnavigation ul li.active a.nav_link{font-weight: bold; font-size: 1.6rem; padding: 7px 10px;}
            .sidenav, .tp_home_widget {display:none;}
            .sidenav {display:none;}
  `;

  const articleshowCss = `
            .story-article {color: #000;}
            .article-section {background: #fff;}
            .news_card{margin:0 0 4%;}
            .news_card h1{font-size:1.8rem;line-height:2.8rem;padding:10px 3% 5px;color:#000; margin:0}
            .news_card .news_card_source{margin:0 3% 10px;display:inline-block;font-size:1.2rem;color:#a0a0a0;}
            .backToBrandwire{padding:10px 3% 0;}
            .backToBrandwire a{color: #999; font: normal 1.1rem/1.4rem arial,sans-serif; text-transform: uppercase; position:relative; display:block; margin-left:12px;}
            .story-article img {max-width: 100%; height: auto;}
            .story-article .story-content{padding:0 4% 20px 4%; font-size: 1.5rem; line-height: 2.8rem; word-wrap:break-word;}
            .story-article .story-content p.inlinetext{margin:0}
            .story-article .story-content section{margin: 5px 0px 20px; box-shadow: 0px 5px 3px -2px rgba(143, 143, 143, 0.29); padding-bottom: 15px;}
            .story-article .story-content section:last-child{margin-bottom: 0;}
            .story-article .story-content section h2{margin: 15px 0 5px;}
            .story-article .story-content section p{display: block; margin:0 0 15px;}
            .story-article .story-content section p:last-child{margin:0}

            .share_icon{display:none}
            .news_card .enable-read-more {padding: 0 3% 10px;}
            .news_card .txtdisclaim{padding: 0 3% 5px; font-style: italic; font-size: 1.25rem; color: #c5c5c5;}
            .enable-read-more .first_col {width: 90%; display: inline-block; vertical-align: top;}
            .enable-read-more .second_col{width: 10%; vertical-align:bottom; margin: 0 0 5px; display: inline-block;}
            .enable-read-more .caption, .clone-caption {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; transition: max-height .4s ease-out; font-weight: normal; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2; margin:0}
            .adsForThirdparty {margin:5px 0 0 0}
            .adsForThirdparty img{width:auto; height:auto}
            .story-article .story-content br{content: ''; display: block; height: 1.0rem;}
            .story-article .embedarticle {border-left: 5px solid #000; padding: 0 0 0 10px; position: relative; clear: both; display: block; margin: 10px 0;}  
            .story-article .embedarticle a {display: flex;}
            .story-article .embedarticle a img {width: 68px; height: 51px; margin-right: 15px;}
            .story-article .embedarticle a strong {font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2;}
            .ads-between-story{height:250px; overflow-y:auto;}

            .story-article .img_wrap {margin: 0 0 2%; position: relative;}
            .story-article .img_wrap .img_caption {margin:0; padding: 10px 0 5px; color: #8d8d8d; font-size: 1.2rem; line-height: 1.6rem;}
            .story-article .news_card .img_wrap{min-height:240px;}

            .mobile_body .story-article .news-card.banner-image{width: auto; margin-top: 4%; border: 0; padding: 0;}

            .mobile_body .briefwidget {min-height: 400px;}


            .yContainer{text-align: right; margin: 0 3% 3px 0; display:block; overflow:hidden;} 
            .yContainer span{display: inline-block; vertical-align:top; text-transform: uppercase; font-size: 12px; line-height: 28px; color: #716f6f; margin: 0 10px 0 0; height: 24px;}
            .yContainer div{width: 120px !important;}
            .story-article .news_card .yContainer, .videolist-container .top-news-content .yContainer, .videoshow-container.box-content .yContainer{padding:8px 3% 2px 0; background: #eaeaea; border-bottom: 1px solid #c6c6c6; margin:0}
            .videoshow-container.box-content .yContainer{margin:10px 0 0 0}
            .box-content .yContainer{background:transparent}
            .master-player-container .yContainer{display:none}


            .recipeArticle .con_review_rate{display:none}
            .recipeArticle .time_calor{font-size: 12px; background-color: #f2f2f2; padding: 10px 0; overflow:hidden; margin:0 0 20px}
            .recipeArticle .time_calor ul{margin:0; padding:0; white-space:nowrap;}
            .recipeArticle .time_calor ul li{display:inline-block; vertical-align:top; width:33%; text-align:center;}
            .recipeArticle .time_calor .txt_innner {display: inline-block; vertical-align: middle; text-align: left; color:#333;}
            .recipeArticle .time_calor ul li i{display:inline-block; vertical-align:top;}
            .recipeArticle .time_calor ul li span {font-size:14px; line-height:12px; font-weight:bold; font-family:arial; display:block; margin-top: 3px;}
            .recipeArticle .time_calor ul li svg{ width:28px; height:28px; display: inline-block; vertical-align: middle; margin-right: 8px; color:#333;}
            .recipe_ingriedient{border-bottom:1px solid #d8d8d8; padding: 0 4% 20px 4%; margin-bottom: 15px}
            .recipe_ingriedient h4{color: #525252; font-size: 2.0rem; margin: 5px 0 20px; text-transform: uppercase;}


            .default-outer-player .default-player{top: 0; width: 100%;}
            .default-outer-player .inlinevideo{position: absolute; margin: auto; bottom: 0; left: 0; right: 0; padding: 10px 3% 5px; color: #fff;}
            .default-outer-player .inlinevideo .text_ellipsis{font-size: 1.2rem; line-height: 1.6rem;}
            
            .tp_icondiv .tp_points {display: inline-block; width: 25px; height: 25px; border-radius: 50%; background-image: linear-gradient(135deg, #3023ae, #cf10ef 102%);}
            .news_card_source {width: 80%; display: inline-block;}
            .news_card .enable-read-more .caption{color: #7d7d7d;}
            .mobile_body .top-article {padding: 3%;}
            .mobile_body .tp-widget-wrapper{min-height:60px;}
            .top-article li {padding: 0 0 0px 15px;font-size: 1.4rem;line-height: 2.2rem;}
            .top-article h3 {font-size: 16px;margin: 0 0 5px;}
            .story-article iframe.amzproduct{height:185px}
            .story-article .story-content [data-plugin=ctn] {width: auto !important; margin: 2% -4% !important; min-height: 100px !important;}
            .article-section .story-article .story-content .bccl-ad-wrapper{height: 120px; overflow-y: auto;}
            .article-section .story-article .story-content [ctn-style=ctnbccl] {margin: 0 !important; padding:0}
            .article-section .story-article .story-content .top-img-ad{min-height:280px}
            .article-section .story-atf-wrapper {min-height: 250px;}
            .article-section .story-article .adCANContainer {height: 900px; overflow-y:auto;}
            .article-section .story-article  .ad1 {min-height: 250px;}
            .article-section .story-article  .ad1:empty {min-height: 0px;}
  `;

  const videoshowCss = `
            .breadcrumb {padding: 10px 4% 5px;background: #fff;}
            .breadcrumb ul {line-height: 1.8rem;}
            .breadcrumb ul li{list-style-type: none;display: inline;}
            .breadcrumb ul li a, .breadcrumb ul li a span {color: #333;text-decoration: none;font-size: 1.1rem;vertical-align: top;line-height: 1.6rem;font-weight: bold;}
            .mobile_body .videoshow-container.hypervideo .top_wdt_video {padding: 0;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 {-ms-flex: 0 0 100%;flex: 0 0 100%;max-width: 100%;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_social{width: 100%;height: 45px;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_wrap {margin: 10px 3%;}
            .top_wdt_video .row .col8 .con_wrap .more_items {margin: 0;}
            .mobile_body .videoshow-container .top_wdt_video .section {margin: 10px 0 10px;box-sizing: border-box;}
            #parent_video_show_container{min-height: 200px;width:100%; background-size: contain; background-position: 50%; background-repeat: no-repeat;}
            .breadcrumb ul li span {font-size: 1.1rem;line-height: 1.6rem;vertical-align: top;}
            .videoshow-container .top_wdt_video .section h1 span {line-height: 2.6rem; height: auto;}
            .tpstory_title h1 {display: inline-block;width: 90%;}
            .videoshow-container .enable-read-more .first_col {width: 90%; display: inline-block; vertical-align: top;}
            .videoshow-container .enable-read-more .second_col{width: 10%; vertical-align:bottom; margin: 0 0 5px; display: inline-block;}
            .videoshow-container .enable-read-more .caption {font-size: 1.2rem; line-height: 2.2rem; max-height: 6.6rem; transition: max-height .4s ease-out; font-weight: normal; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; margin:0}
            .videoshow-container .enable-read-more .more {-webkit-line-clamp: unset; max-height: none; transition: max-height 1s ease-in; display: block;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_wrap {margin: 10px 3%;}
            .mobile_body .videoshow-container.hypervideo .top_wdt_video .row .col8 .con_wrap .more_items {margin: 0;font-family: arial;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_wrap .more_items span {vertical-align: top;font-size: 1.0rem;}
            #video_show_container{height: 200px;width:100%;}
            #video_show_container .image-container{z-index: 9999;position: absolute;}
            .mobile_body .videoshow-container .nbt-list li:nth-child(2n+2) {margin-left: 4%;}
            .mobile_body .videoshow-container .nbt-list li {background: transparent;width: 48%;min-height: 100px;margin: 0 0 4% 0;padding: 0;display: inline-block;position: relative;box-sizing: border-box;}
            .mobile_body .videoshow-container {background: #fff;overflow: hidden;}
            .mobile_body .videoshow-container .section_videos {padding: 4% 4% 0 !important; margin-bottom: 50px;}
            .mobile_body .videoshow-container .section_videos {margin-bottom: 50px;}
            .mobile_body .videoshow-container .nbt-list li {width: 48% ;margin: 0 0 4% 0;}
            .mobile_body .videoshow-container .nbt-list li .con_wrap {display:block;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_social .yContainer {display: -ms-flexbox;display: flex;float: right;}
            .mobile_body .videoshow-container .tpstory_title .tp_icondiv {vertical-align: top;}
            .mobile_body .videoshow-container .top_wdt_video .section h1 {border-bottom: 0;margin: 0;font-size: 1.6rem;height: auto;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_social {margin: 0 3%;display: block;}
            .card_breaking_news .table {table-layout: fixed;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_social .share_icon {position: initial;display: inline-block;vertical-align: top;width: 35px;height: 35px;margin: 10px 0 0;border-radius: 50%;}
            .mobile_body .videoshow-container .top_wdt_video .row .col12 .section .top_section h3 {margin: 0; padding:0;font-size: 1.8rem; border-bottom: 2px solid #dfdfdf; height: 40px;}
            .mobile_body .videoshow-container .section .top_section {margin: 0 0 4%;}
            .mobile_body .tpstory_title h1 {display: inline-block;width: 90%;}
            .mobile_body .videoshow-container .nbt-list {display: -ms-flexbox; display: flex; -ms-flex-wrap: wrap; flex-wrap: wrap;}
            .mobile_body .videoshow-container .top_wdt_video .row .col8 .con_social .youtube-iframe {display: -ms-flexbox; display: flex; float: right;}
  `;

  const moviereviewCss = `
            li.moviereview-listview {margin: 0 3% 4%; padding-bottom: 4%; width:auto;}
            li.moviereview-listview .img_wrap{position:relative; width:35%; vertical-align:top;}
            li.moviereview-listview .img_wrap img{vertical-align:bottom; width: 100%; height: auto;}
            li.moviereview-listview .con_wrap {text-align: left; word-break: break-word; padding: 0 0 0 10px; vertical-align: top; position: relative;}
            li.moviereview-listview .rating{display:none}
            li.moviereview-listview .con_wrap .title{font-size: 1.6rem; line-height: 2.4rem; font-weight: 600; margin: 0 0 5px; display: block;}
            li.moviereview-listview .con_wrap .cast{margin: 0 0 15px;}
            li.moviereview-listview .text_ellipsis{font-size: 1.4rem; line-height: 2.2rem;}
            li.moviereview-listview .des{font-size: 1.4rem; line-height: 2.2rem; display: block;}

            li.moviereview-listview.lead-post{margin:0 0 4%; width: 100%;}
            li.moviereview-listview.lead-post .table_row .poster-bg{position: absolute; object-fit: cover; width: 100%; height:174px;}
            li.moviereview-listview.lead-post .img_wrap{width: auto; text-align: center;}
            li.moviereview-listview.lead-post .img_wrap img{width:auto; max-width: 140px; max-height: 187px; margin: 15px 15px 0; box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.1); border: 5px solid #fff;}
            li.moviereview-listview.lead-post .con_wrap{padding:15px 10px 0 0; box-sizing: border-box; width: 100%}

            .moviereview-summaryCard .poster_card{position:relative; height:300px; background:#333;}
            .moviereview-summaryCard h1 {font-size: 1.8rem; line-height: 2.8rem; padding: 15px 0 10px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; max-height: 5.6rem;}
            .moviereview-summaryCard .img_wrap{width: 120px; vertical-align: top; text-align: left; position: relative;}
            .moviereview-summaryCard .img_wrap img{border:4px solid #fff; max-width: 120px; max-height: 160px; width:auto;}
            .moviereview-summaryCard .img_wrap .video_icon {border: 0;width: 40px; height: 40px;top: 0; left: 0; margin: auto;}
            .moviereview-summaryCard .con_wrap {text-align: left; word-break: break-word; padding: 0 0 0 10px; vertical-align: top; position: relative; width:100%}
            .moviereview-summaryCard .con_wrap .cast{font-size: 1.8rem; line-height: 2.6rem; max-height: 7.8rem; -webkit-line-clamp: 3;}
            .moviereview-summaryCard .con_wrap .des{font-size: 1.4rem; line-height: 2.0rem; display: block; position: absolute; bottom: 0}
            .moviereview-summaryCard .con_wrap .des span{display: block;}
            .moviereview-summaryCard .con_poster{position: absolute; z-index: 2; background: transparent; padding: 0 3%; left: 0; right: 0; top: 0; bottom: 0; color:#fff}
            .moviereview-summaryCard .rating {margin: 10px 0 0 0;}
            .moviereview-summaryCard .con_review_rate, .listen-on{display:none}
            .moviereview-summaryCard .con_review_rate{min-height:37px}
  `;

  const photoshowCss = `
            .photomazzashow-content{background:#fff;}
            .photomazzashow-content h1{padding:15px 3%;text-align:left;font-size:2.0rem;line-height:3.0rem;color:#000;}
            .photomazzashow-content .pwa-deals{min-height:310px; margin-top: 4%; margin-left: 3%; margin-right: 3%; padding-top: 4%;}
            .photomazzashow-content .inline_app_download{padding: 15px 3%; min-height: 60px; box-sizing: border-box;}
            .photomazzashow-content .tp_icondiv{vertical-align:top;}
            .photo_story li.photo_card{background:#282e34;}
            .photo_story li.photo_card .readmore{display:none}
            .photo_story li.photo_card .photo_des{padding:20px 3%;color:#9e9e9e;}
            .photo_story li.photo_card h2{font-size:2.0rem;line-height:30px;font-weight:normal;margin:0 0 15px;color:#ffffff;}
            .photo_story li.photo_card .caption{font-size:1.6rem;line-height:2.4rem; word-break: break-word;}
            .title_with_tpicon {padding: 15px 3%;}
            .title_with_tpicon h1 {display: inline-block;width: 90%;padding: 0;margin: 0;}
            .photo_story li.photo_card .pagination {position: absolute;top: 3%;}
            .photo_story li.photo_card h2 {font-size: 1.8rem;line-height: 2.8rem;margin: 0 0 10px;}
            .photo_des .photo_con{margin:0}
            .photo_story li.photo_card {position: relative;}
            .photo_story li.photo_card .pagination span {padding: 0 10px;font-size: 1.4rem;line-height: 30px;font-family: arial;display: block;}
            .photo_story li.photo_card .share_icon {position: absolute;bottom: 10px;right: 10px;background: rgba(0, 0, 0, 0.5);box-shadow: none;width: 40px;height: 40px;}
            .share_icon span {padding: 12px;display: block;}
            .photo_story li.photo_card .place_holder {position: relative;background: #e8e8e8;}
            .photo_story li.photo_card .place_holder .adgsplash_float {bottom:0;}
            .photo_story li.photo_card .photo_des .table_col:last-child {width: 10%;vertical-align: bottom;margin: 0 0 20px;}
            .photo_story li.photo_card .photo_des .photo_con .table{margin: 0;}
            .photo_story li.photo_card .readmore {position: relative;}
            .photo_story li.photo_card .caption{font-size:1.6rem;line-height:2.4rem; word-break: break-word;}
            .photo_story li.photo_card .photo_des .caption p {margin: 0}
  `;

  const liveblogCss = `
            .lb_container .lb_summaryCard{background:#fff;}
            .mobile_body .lb_container .lb_summaryCard h1 { margin-top: -7px; }
            .mobile_body .lb_container .lb_summaryCard .lb_top_summary {min-height: 120px;}
            .lb_container .lb_summaryCard h1{font-size: 1.6rem; line-height: 2.4rem; padding: 0;}
            .lb_container .lb_summaryCard .source{margin: 0 0 10px; display: block; font-size: 1.1rem;}
            .lb_container .lb_summaryCard .caption{font-size: 1.1rem; display: block; padding: 5px 0 0; line-height: 1.7rem; margin-bottom: 10px;}
            .lb_container .livepost li.livelist{margin:0 0 4%;padding:3%;font-size:1.6rem;line-height:2.8rem;background:#fff;}
            .lb_container .lb_summaryCard .lbimg { float: left; width: 100px; border: 2px solid #fff; margin: 0 15px 10px 0; height: calc(100px * 405 / 540)}
            .lb_container .lb_summaryCard .lb_top_summary{padding: 15px}
  `;

  const videolistCss = `
            #childrenContainer .wdt_top_section ul li span.table_col.img_wrap{min-height:180px;width:100%;}
            #childrenContainer .wdt_sections ul li span.table_col.img_wrap{min-height:86px;}
            #childrenContainer .wdt_top_section .view-horizontal li span.table_col.img_wrap{min-height:112px;width:100%;}
            .mobile_body .videolist .wdt_sections.video ul {padding: 0;}
            .mobile_body .wdt_sections {padding: 0 3%;}
            .mobile_body .wdt_sections .section .top_section h1 span {height: auto;line-height: 2.8rem;padding: 10px 10px 5px 0;display: inline-block;}
            .mobile_body .wdt_sections.video ul li {padding: 0 0 4%;}
            .mobile_body .wdt_sections .section .top_section{margin-left: 5px;margin-right: 5px;}
            .news.news-card.lead.col12.con_ads{min-height:250px;}
            .mobile_body .wdt_top_section, .mobile_body .wdt_next_sections {padding: 2% 0 4%;margin: 0 0 4%;}
            .mobile_body .wdt_top_section .section .top_section, .mobile_body .wdt_next_sections .section .top_section {margin-left: 0;margin-right: 0;margin-bottom: 0;}
            .mobile_body .wdt_top_section .section {height:76px;}
            .videolist .con_social{margin-right: 3%;}
            .videolist .con_social .youtube-iframe{margin-left: auto; padding: 0 0 3% 0;}
            .wdt_top_section .section .top_section .read_more {display:none}
            .mobile_body .L1.videolist .wdt_top_section .lead .img_wrap img{height: calc((100vw - 30px) * 300 / 540);}
            .mobile_body .L2.videolist .wdt_sections .lead .img_wrap img{min-height: calc(100vw * 300 / 540);}
            .mobile_body .wdt_top_section.video .view-horizontal li.vertical .img_wrap img, .mobile_body .wdt_next_sections.video .view-horizontal li.vertical .img_wrap img {width: 200px; height: calc(200px * 300 / 540)}
            .mobile_body .wdt_top_section ul.view-horizontal li.vertical .img_wrap img, .mobile_body .wdt_next_sections ul.view-horizontal li.vertical .img_wrap img{width: 127px; height: calc(127px * 300 / 540)}
  `;

  let topicsCss = `
            .mobile_body .section-topics {background: #fff; padding:3%}
            .mobile_body .section-topics .tabs-circle ul {display: -ms-flexbox;display: flex;}
            .mobile_body .section-topics .tabs-circle {margin: 4% 3%;}
            .mobile_body .section-topics .tabs-circle ul li {margin-right: 1%;width: 24%;height: 30px;line-height: 33px;}
            .mobile_body .section-topics .rest-topics .news-card.horizontal {padding-left: 0;padding-right: 0;-ms-flex-wrap: nowrap;flex-wrap: nowrap;flex: auto;}
            .mobile_body .section-topics .row.box-item.photos ul li span.img_wrap{min-height:86px;}
            .mobile_body .section-topics .row.box-item.videos ul li span.img_wrap{min-height:183px;width: 100%;}
            .mobile_body .section-topics .box-item.videos .news-card.lead .con_wrap {margin-left: 0;}
            .row.box-item.photos .news-card.vertical .con_wrap .text_ellipsis {font-size: 1.4rem;line-height: 2.2rem;-webkit-line-clamp: 3;word-break: break-word;}
            .mobile_body .section-topics .row.box-item.all ul li.con_ads div{min-height:85px;}
  `;

  const photolistCss = `
            .mobile_body .wdt_top_section .section .top_section h2, .mobile_body .wdt_top_section .section .top_section h1, .mobile_body .wdt_next_sections .section .top_section h2, .mobile_body .wdt_next_sections .section .top_section h1 {border-bottom: 0;}
            .mobile_body .wdt_top_section .section .top_section, .mobile_body .wdt_next_sections .section .top_section {margin-left: 0; margin-right: 0; margin-bottom: 0;}
            .mobile_body .wdt_top_section, .mobile_body .wdt_next_sections {padding: 2% 0 4%; margin: 0 0 4%;}
            .mobile_body .wdt_top_section .news-card.lead .con_wrap {margin-left: 0; margin-right: 0;}
            .news-card.lead .con_wrap .section_name {font-size: 1.3rem; margin-bottom: 5px;}
            .mobile_body .view-horizontal {-ms-flex-wrap: nowrap; flex-wrap: nowrap; overflow-x: auto;}
            .mobile_body .wdt_top_section ul.view-horizontal li.vertical, .mobile_body .wdt_next_sections ul.view-horizontal li.vertical {margin: 0; padding: 0 15px 0 0;}
            .mobile_body .wdt_top_section ul.view-horizontal li.vertical .img_wrap img {width: 140px;}
            .ad1.mrec, .ad1.mrec1, .ad1.mrec2 {min-height: 250px; z-index: 1; margin: auto; padding: 0;}
            .photolist .news-card.lead .img_wrap img{width:100%; height: calc((100vw - 30px) * 0.75)}
            .mobile_body .L2.photolist .wdt_sections .vertical .img_wrap img{width: 154px; height: calc(154px * 405 / 540);}
  `;

  /* Campaign based css start */

  const electionsCss = `
            body.webview .wdt_btn .btn_result.live_updates, body.webview .state_wise_widget, body.webview .top_notification, body.webview .wdt_election_result .share_icon, body.webview .right-open-in-app{display:none}
            .star_candidates_widget .scroll_content{white-space: nowrap; margin: 0 0 1%}
            .star_candidates_widget h2.election_h2{margin-bottom: 5px;}
            .right-open-in-app, .right-open-in-app:hover, .right-open-in-app:visited{color: #fff; font-size: 10px; text-transform: uppercase; display: inline-block; width: 42px; text-align: center; background: #E55E40; border-radius: 2px; padding: 4px 0 3px; font-family: arial; position: absolute; right: 3%; top: 57px; outline:none}
            .right-open-in-app span{display: block; margin-bottom: 1px;}    
            .highcharts-contextbutton{display:none}
            .star-candidates {width:100%; box-sizing:border-box;}
            .star_candidates_widget {background:#e6ecee; padding: 0 10px 20px; box-sizing:border-box; width:100%;}
            .star_candidates_widget .h2WithViewMore {margin: 0 0 10px 10px;}
            .mobile_body .star_candidates_widget .section {padding: 0;}
            .mobile_body .star_candidates_widget .section .top_section {margin:0 0 10px;}
            .mobile_body .star_candidates_widget .section .top_section h2 {font-size:22px; color: #000; background: transparent; padding: 0;}
            .mobile_body .star_candidates_widget .section .top_section h2 span, .star_candidates_widget .section .top_section h2 a {color: #000;}
            .candidate_box {height:100%; box-sizing:border-box; font-weight:bold; border-radius: 5px; box-shadow: 0 0 4px 0 #c8d5d9; background-color: #ffffff; padding: 10px; text-align:center; margin: 2px;}
            .candidate_box .candidate_imgwrap {display:inline-block; position:relative; margin-bottom:10px}
            .candidate_box .candidate_imgwrap img {width:96px; height:96px; border-radius:50%; border:1px solid #e6ecee; padding:8px; box-sizing:border-box; object-fit: cover;}
            .candidate_box .candidate_name {font-size: 0.9rem; display:block; text-align: center; max-width: 100%; overflow:hidden; text-overflow:ellipsis; margin: 0 auto; white-space: nowrap;}
            .candidate_box .constituency {font-size: 0.9rem; color:#8c8c8c; display:block; marging: 5px 0 10px;}
            .candidate_box .party-name {font-size: 0.7rem; color:#8c8c8c; display:inline-block; color:#fff; border-radius:5px; padding: 5px 15px; text-transform:uppercase;}
            .candidatelist {padding: 0 15px;}
            .candidatelist .slide {margin-bottom: 15px; margin-right: 2%; width: 49%; box-sizing: border-box;}
            .candidatelist .slide:nth-child(2n) {margin-right: 0;}
            .mobile_body .result_inner .combinedchartbox {width:100%;box-sizing:border-box;margin-bottom: 0;}
            .mobile_body .result_inner .combinedchartbox .pie-chart {border: 1px solid #eee;}
            .mobile_body .results_container .combinedchartbox .pie-chart {border: none;}
            .tablecount, .graphcount {width:100%;}
            .exitpoll_table_container.col12 {padding:0;}
            .mobile_body .chart-horizontal{white-space: nowrap; overflow: auto;}
            .mobile_body .chart-horizontal .table-chart {display:inline-block; width:90%; margin-right:10px;}
            .mobile_body .result_inner .tbl_txt, .results_container .tbl_txt {background:#e6ecee; padding:10px 0; text-align:center;font-size:1.2rem; margin:0;}
            .result-tbl {display: table; width: 100%; border-collapse: collapse; margin: 20px auto 10px; font-size: 1rem; background: #fff;}
            .result-tbl .tbl_row {display: table-row; text-align: left; color: #333;}
            .result-tbl .tbl_row:nth-child(even) {background-color: #e6ecee;}
            .result-tbl .tbl_row:last-child {background-color: transparent;}
            .result-tbl .tbl_row:last-child .tbl_col {border: none;font-weight: bold;}
            .result-tbl .tbl_row:last-child .tbl_col span {color: #7b7b7b;}
            .result-tbl .tbl_col {display: table-cell;border-bottom: 1px solid #e5e5e5;padding: 5px 2%;font-size: 11px;vertical-align: middle;line-height: 18px;text-transform: uppercase;text-align: center;}
            .result-tbl .tbl_col:first-child {border-left: 1px solid #e5e5e5;}
            .result-tbl .tbl_col:last-child {border-right: 1px solid #e5e5e5;}
            .result-tbl .tbl_col.party_logo {text-align: left;width: 45%;text-transform: uppercase;line-height: 25px;font-weight: bold;}
            .result-tbl .tbl_col.party_logo img {max-width: 32px !important; float: left; margin-right: 6px;}
            .result-tbl .tbl_col.party_logo img[src*="69376388"] {margin-top: 7px;}
            .result-tbl .tbl_heading {font-weight: normal;display: table-row;background-color: #000;text-align: left;line-height: 1.4rem;font-size: 1.3rem;color: #fff;text-transform: uppercase;}
            .result-tbl .tbl_heading .tbl_col {line-height: 24px;font-size: 12px;text-align: center;display: table-cell;vertical-align: middle;text-transform: uppercase;}
            .result-tbl .tbl_heading .tbl_col.first {text-align: left;padding-left: 4%;}
            .nodatawidget { padding: 20px; text-align: center; font-size: 18px; font-weight: bold; color: #666666; box-sizing:border-box; width:100%;}
            .nodatawidget .refresh_btn {position: static; margin-top: 30px;}
            .nodatawidget .refresh_btn a {display: inline-block; background-color: #F44336; padding: 4px 20px 4px 15px; border-radius: 5px; color: #fff;}
            .exitpoll-widget {margin: 0 0 20px;}
            .exitpoll-widget .section {padding: 0; width: 100%; box-sizing: border-box;}
            .exitpoll-widget .section .top_section { margin: 0;}
            .exitpoll-widget .section .top_section h2, .exitpoll-widget .section .top_section h1 {background: #1a1a1a; color: #fff; padding: 0px 10px; font-size: 20px; border: none; box-sizing: border-box;}
            .exitpoll-widget .section .top_section h2 a, .exitpoll-widget .section .top_section h1 a {color:#fff;}
            .exitpoll-widget .section .top_section .read_more {display:none;}
            .el_powerdby {text-align:center;}
            .el_resulttable {display:none;}
            .table-chart {width: 100%;}
            .el_exitpoll {padding: 0 15px;}
            .results_container .switcher {padding-top:10px;}
            .election-hpwidget {background:#fff;}
            .election-hpwidget .election-header-wrapper .assembly-election-header{display:inline-block;background-color:#352f2f;border-radius:4px 0px 0px 4px;position:relative;font-size:1.2rem;line-height:2.2rem;color:#fff;padding-left:10px;margin:10px 10px 0;}
            .election-hpwidget .election-header-wrapper .assembly-election-header span{display:inline-block;background-color:#fc363b;margin-left:7px;padding:0 7px;}
            .statestoggle{display:block; margin:5px 0 0; white-space:nowrap; overflow-x:auto; overflow-y:visible; padding:10px 0;}
            .statestoggle span{border-top:1px solid #c0c2c4;border-bottom:1px solid #c0c2c4;font-size:1rem;height:24px;line-height:22px;color:#333;position:relative;padding:0 15px;display:inline-block; vertical-align:top;}
            .statestoggle span b{font-weight:normal;position:relative;z-index:2;}
            .statestoggle span:last-child::after{display:none;}
            .statestoggle span.active{color:#fff;}
            .election-hpwidget .election-header-wrapper .read_more{display:none;}
            .election-hpwidget .result_inner .election-data-header .section .top_section{margin:0; text-align:center;}
            .election-hpwidget .result_inner .election-data-header .section .top_section h2{background:transparent;font-size:14px;padding:5px 0;}
            .election-hpwidget .result_inner .election-data-header .section .top_section h2 a{color:#000;}
            .election-hpwidget .result_inner .election-data-header .electionsnav{background:#ebeef1;}
            .election-hpwidget .result_inner .election-data-header .electionsnav a{font-size:12px;color:#000;padding:0 10px; display:inline-block; vertical-align:top; height:22px; line-height:22px;}
            .election-hpwidget .result_inner .star-candidates-wrapper{margin-top:10px;}
            .election-hpwidget .result_inner .result-tbl .tbl_heading{background:transparent;color:#000; font-weight:bold;}
            .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col{border-left:none !important;border-right:none !important;}
            .election-hpwidget .result_inner .result-tbl .tbl_heading .tbl_col:first-child {padding-left:25px;}
            .election-hpwidget .result_inner .result-tbl .tbl_col:first-child{border-right:1px solid #e5e5e5;}
            .election-hpwidget .result_inner .result-tbl .tbl_row:last-child .tbl_col{border:none;font-size:11px;}
            .election-hpwidget .result_inner .result-tbl .tbl_row:nth-child(even){background:transparent;}
            .result-tbl .tbl_col .partyclr {display: inline-block;border-radius: 2px;padding: 1px 7px;line-height: normal;color: #fff;font-size: 11px;margin-left: 20px;font-weight: normal;}
            .election-as-widget .statestoggle {margin:5px -5px 10px}
            .election-as-widget .statestoggle .states {padding:0 10px;}
            .election-as-widget .statestoggle .states span {border:none;}
            .election-hpwidget .row .col12.ad-count {margin: 10px auto 15px;}
    `;

  const budgetCss = `
          .wdt_budget {position:relative; margin:0 0 4%; background:#fff}
          .wdt_budget .budget_head{text-align:center; display:block; overflow:hidden; background:#000; height:40px; width:100%}
          .wdt_budget .budget_head h3 {font-weight:normal; margin:0}
          .wdt_budget .budget_head h3 a{font-size: 1.6rem; line-height: 40px; color:#fff; display:block}
          .wdt_budget .bdt_top{margin-top:4%}
          .wdt_budget .cheaper_dearer{padding-bottom: 4%; margin: 0;}
          .wdt_budget .wdt_lb_home{margin:0 3%; padding-top:4%; overflow:hidden}
          .wdt_budget .wdt_lb_home h3{height:40px; line-height:40px; background:#ff6161; display:block; text-align:center; margin:0;}
          .wdt_budget .wdt_lb_home h3 a{font-size: 1.6rem; color:#fff; display:block;}
          .wdt_budget .lb-strip .lb-card .con_wrap{border-color:#ffc8c8}
          .wdt_budget .wdt_lb_home .lb-strip .lb-card .con_wrap:after{border-color:#ff0000;}
          .mobile_body .wdt_budget .wdt_lb_home .lb-strip{max-height:200px; overflow: hidden; overflow-y: auto; margin-bottom: 5px;}
          .mobile_body .wdt_budget .view-horizontal .news-card .img_wrap{margin:0}
          .mobile_body .wdt_budget .view-horizontal li.news-card.vertical .img_wrap img {max-height: 100px; width: 306px;}
          .mobile_body .wdt_budget .view-horizontal li.news-card.vertical{margin:0; max-height:100px}
          .mobile_body .wdt_budget .view-horizontal .news-card .con_wrap{display:none}
    `;

  const newsbriefCss = `
            .briefbox { white-space:normal;}
            .slide_list li{margin:15px auto; width: 100%; overflow: hidden; border: 1px solid #d5d5d5; box-shadow: 0 2px 5px #b8b8b8;white-space:normal;}
            .slide_list li .top_content{position: relative; overflow: hidden;}
            .slide_list li .top_content .share_icon {position: absolute; top: 0px; right: 0px; background: transparent; box-shadow: none; width: 40px; height: 40px;}
            .slide_list li .top_content .counter {padding: 0 6px;left: 5px;position: absolute;background: #000;top: 5px;box-shadow: none;font-size: 1.3rem;color: #ffffff; height:28px; line-height:28px;}
            .slide_list li .top_content .share_icon svg {width: 15px;}
            .slide_list li .top_content .shareModalContainer .social-table svg{width:48px;}
            .slide_list li .top_content .shareModalContainer .socialText{margin-top:5px}
            .slide_list li  .more{font-size: 1.3rem;color: #000; display:block; text-align:right; padding:0 3%;}
            .slide_list li .img_wrap{vertical-align:top}
            .slide_list li .img_wrap img {vertical-align: bottom; width: 100%; height: auto;}
            .slide_list li .con_wrap {color:#000;  margin: auto; bottom: 0; padding: 15px 3% 10px; }
            .slide_list li .text_ellipsis {font-size: 1.6rem; line-height: 2.6rem; margin-bottom:10px; font-weight:bold;}
            .slide_list li .time-caption {font-size: 1.1rem; line-height: 1.2rem; display: inline-block; font-weight: normal;}
            .slide_list li .bottom_content{margin:3%; font-size: 1.4rem; line-height: 2.2rem; min-height:130px}
            .slide_list li .bottom_content ul li{font-size: 1.4rem; line-height: 2.2rem; color:#333333; border: 0;  box-shadow: none; margin: 0 0 0 15px; position: relative; overflow: initial; width: auto; display: block;}
            .slide_list li .bottom_content .wdt_download_app{display:none}
            .slide_list li .bottom_content.app_in_brief{position:relative;}
            .slide_list li .bottom_content.app_in_brief ul li, .slide_list li .bottom_content.app_in_brief ul{color:#cecece}
            .slide_list li .bottom_content.app_in_brief .wdt_download_app{display:block; position:absolute; top:0; bottom:0; left:0; right:0; margin:auto; background:#fff; color:#000; font-size:1.8rem; line-height:2.8rem; opacity:.9; text-align:center;}
            .slide_list li .wdt_download_apptooltip{display:block;text-align:center; position:absolute; bottom:40px; left:0; right:0; width:80%; margin:auto; background:#fff; color:#000; font-size:1.8rem; line-height:2.8rem; opacity:.9;}
            .slide_list li .bottom_content.app_in_brief .wdt_download_app b{display:block; margin:20px 0 10px}
            .slide_list li .bottom_content.app_in_brief .btn_app_download{background:#ff0000; font-size:1.6rem; height:30px; line-height:34px; padding:0 20px; border-radius:15px; display: inline-block; color: #fff; cursor:pointer;}
            .slide_list li .btn_app_downloadtooltip{background:#000000; font-size:1.4rem;line-height:20px; padding:8px 20px 5px; border-radius:15px; display: inline-block; color: #fff; cursor:pointer;}
            .briefbox .swipperParentDiv .article-section{ overflow:initial} 
            .landscape-mode{display:none}    
    `;

  const worldcupCss = `
            .table_layout{margin:0 3% 4%;}
            .table_layout table{width:100%;border-collapse:collapse;margin:0 auto;border-spacing:0;border:0;}
            .table_layout table tr td{font-size:1.2rem; font-weight:bold;text-align:center;}
            .table_layout table tr td:first-child{text-align:left;text-transform:uppercase;font-weight:bold;width:45%;}
            .table_layout table tr:first-child th:first-child{text-align:left;text-transform:uppercase;font-weight:bold;width:45%;}
            .table_layout table tr td:first-child img{width:18px; height:18px; margin-right:10px; border-radius: 50%;}
            .table_layout table tr:first-child th{font-size:1.2rem;}
            .table_layout table td, .table_layout table th{padding:10px 5px;}

            .mini_schedule{white-space: nowrap; margin: 0 0 4%; background: #fff; padding: 3% 0 1%;}
            .mini_schedule .wdt_schedule .match_final_status .match_venue{height: 1.5rem;}
            .mini_schedule .wdt_schedule .match_final_status .status_live{position: absolute; left: 0; right: 0; bottom: -1px; width: 30px; margin: auto;}
            .mini_schedule .wdt_schedule{position:relative; overflow:hidden; min-height:112px}
            .mini_schedule .wdt_schedule .match_final_status{max-height:2.8rem; overflow:hidden}
            .mini_schedule .wdt_schedule .teama .teaminfo img, .mini_schedule .wdt_schedule .teamb .teaminfo img{width:35px}
            .wdt_schedule{border-radius:8px; font-family:arial;padding:5px 10px 5px;margin:5px 0 5px 4%;display:inline-block;width:70%;vertical-align:top;min-height:100px;}
            .wdt_schedule:last-child{margin-right:4%;}
            .wdt_schedule .liveScore{margin-top:5px;}
            .wdt_schedule .match_date_venue{font-size:1.0rem;line-height:1.5rem; text-align:center;white-space:normal;text-transform:uppercase;}
            .wdt_schedule .match_date_venue span{display:block; font-weight:bold;line-height:2rem;}
            .wdt_schedule .teama, .wdt_schedule .teamb{width:40%;}
            .wdt_schedule .teama .teaminfo, .wdt_schedule .teamb .teaminfo{overflow:hidden;clear:both;display:block; margin-top:2px}
            .wdt_schedule .teama img{float:left;margin:2px 5px 0 0;width:50px;}
            .wdt_schedule .teama .con_wrap{display:inline-block;vertical-align:top;}
            .wdt_schedule .teamb{text-align:right;}
            .wdt_schedule .teamb .con_wrap{display:inline-block;vertical-align:top;}
            .wdt_schedule .teamb .teaminfo img{float:right;margin:2px 0 0 5px;width:50px; border-radius: initial;}
            .wdt_schedule .teamb .country{text-align: right; display: block; width: auto;}
            .wdt_schedule .country{display:block;font-size:1.2rem;font-weight:bold;line-height:normal;text-transform:uppercase;text-align:left;width:50px;}
            .wdt_schedule .country.btm{width: 50px; clear: both; text-align: center; padding-top: 10px;}
            .wdt_schedule .teama .country.btm{float:left}
            .wdt_schedule .teamb .country.btm{float:right}
            .wdt_schedule .score{font-size:1.4rem;font-weight:bold;display:block;margin:0 0 1px;}
            .wdt_schedule .over{font-size:1.1rem; display:block;}
            .wdt_schedule .match_final_status{font-size:1rem;line-height:1.4rem; text-align:center;white-space:normal;margin-top:10px;text-transform:uppercase;}
            .wdt_schedule .match_final_status .time{margin-right:5px; color:#000;}
            .wdt_schedule .match_final_status .status_live{font-size:10px;display:inline-block;padding:0 7px 0 15px;height:17px;line-height:17px;text-transform:uppercase;border-radius:3px;position:relative;}
            .wdt_schedule .match_final_status .match_venue{font-size:1rem;line-height:1.5rem; overflow: hidden; display: block;}
            .wdt_schedule .versus{width:10%; text-align:center}
            .wdt_schedule .versus span{height: 16px; width: 23px; border-radius: 50%; display: inline-block; font-size: 1rem; text-align: center; padding: 6px 0 0 0; font-weight: bold; margin-top:5px;}
            .wdt_schedule .calendar{display:none}
            .schedule_intro{background: #fff;}  
            .schedule_container{margin: 0 0 4%; background: #fff; padding: 4% 0;}
            .schedule_container .wdt_schedule{display:block;width:auto;margin:0 3% 4%;}
            .schedule_container .wdt_schedule a{display:block}
            .schedule_container .wdt_schedule:last-child{margin-bottom:0;}
            .schedule_container .wdt_schedule .match_final_status{width:97%;}
            .matchNvenue{background: #fff; padding: 0 3% 0; text-align: right;}
            .matchNvenue select {width: 35%; padding: 5px 5px 3px; font-size: 12px; margin-right: 5%; outline: none;}
            .matchNvenue select:last-child{margin:0;}
            .wdt_teams.box-item{padding-bottom:0} 
            .wdt_teams .section .top_section{margin:0 0 4%}
            .wdt_teams ul{display:flex; flex-wrap: wrap;}
            .wdt_teams ul li{text-align:center; width:25%; margin:2% 0}
            .wdt_teams ul li a{display:block}
            .wdt_teams ul li img{width:70%}    
            .wdt_teams ul li .name{display:block; font-size:1.4rem; font-weight:bold; margin:10px 0 0 0;}
            .wdt_teams.hp ul{flex-wrap: nowrap; overflow-x: auto; padding-right: 15px; padding-left: 15px;}
            .wdt_teams.hp ul li{margin-right:15px}  
            .wdt_teams.hp ul li img{width:75px}
            .wdt_venues {background:#fff}
            .wdt_venues ul li.news-card:nth-child(2n+1){padding-right: 5px; padding-left:10px; margin-bottom: 4%;}
            .wdt_venues ul li.news-card:nth-child(2n+2){padding-left: 5px; padding-right:10px; margin-bottom: 4%;}
            .wdt_venues ul li.news-card a{display:block; border:2px solid #282e34; width:100%}
            .wdt_venues ul li.news-card .con_wrap {padding: 7px 10px 6px; background: #282e34; display: block; box-sizing: border-box;}
            .wdt_venues ul li.news-card .con_wrap .text_ellipsis {color: #fff; height: 4.4rem; font-size: 1.2rem; line-height: 2rem; max-height: 4rem; -webkit-line-clamp: 2;}
            .wdt_venues.hp{width:100%;}
            .wdt_venues.hp ul li.news-card.vertical{margin-bottom:0; padding:0}
            .wdt_venues.hp .news-card.vertical .img_wrap{margin:0}
            .wdt_venues.hp .list-slider{width:97%; margin-left:3%}
            .wdt_players .list-slider{width:100%}
            .wdt_players .matchNvenue{width:100%; margin-bottom:4%}
            .wdt_players ul li{border-right: 1px solid #dfdfdf; text-align: center; padding: 10px 10px 0; margin: 5px 3% 5px 0; border-radius: 5px; box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.22);}
            .wdt_players ul li .img_wrap img{border-radius: 50%; overflow: hidden; width: 100px; height:100px; border:1px solid #dfdfdf; padding:5px}
            .wdt_players ul li .con_wrap .text_ellipsis{color:#000; font-size:1.2rem; line-height:2.0rem; max-height:4.0rem; margin-top:10px}
              
            .floating_cube.wdt_schedule{border-radius: 0; box-shadow: none; width: auto; margin: 5px 10px 0; padding: 0; display: block; background: transparent; min-height: 80px;}
            .floating_cube.wdt_schedule .versus span{font-size: 9px; padding-top: 5px; height: 18px; width: 18px; box-sizing: border-box;} 
            .floating_cube.wdt_schedule .wdt_schedule .score{font-size:1.2rem}
            .floating_cube.wdt_schedule .score, .floating_cube.wdt_schedule .over{font-size:12px}
            .floating_cube.wdt_schedule .score.active{color:#00b758}
            .floating_cube.wdt_schedule .teama img, .floating_cube.wdt_schedule .teamb img{width:20px}
            .floating_cube.wdt_schedule .match_date_venue{text-transform: initial; text-align: left; font-weight: bold; font-size: 1.1rem; background: #000; color: #fff;height: 30px; line-height: 32px; margin: -5px -10px 5px; padding: 0 10px;}
            .floating_cube.wdt_schedule .country{font-size:12px;}
            .floating_cube.wdt_schedule .match_final_status{line-height:15px; max-height: 30px; color:#828282; margin-top:2px; font-size:11px}
            .floating_cube.wdt_schedule .match_final_status .status_live{bottom:0}
            .flipbox-wrapper .flipbox-box .flipbox-side{min-height:115px}
            .flipbox-wrapper .flipbox-box .flipbox-side h4.news-txt{padding:8px 10px 0px 9px;}
            .flipbox-wrapper .flipbox-box .flipbox-side h4.news-txt a{font-size: 1.2rem; line-height: 2.0rem; height: 6.0rem; -webkit-line-clamp: 3;}
            .Cube{bottom:100px;left:0}
            #content{height:125px;}
            .cube_player_details .match_final_status{color:#000; font-size: 1rem; line-height: 1.4rem; text-align: center; white-space: normal; margin-top: 10px; text-transform: uppercase;}
            .cube_player_details{margin:5px 10px;}
            .cube_player_details .series-title{text-transform: initial; text-align: left; font-weight: bold; font-size: 1.1rem; background: #000; color: #fff;height: 30px; line-height: 32px; margin: -5px -10px 5px; padding: 0 10px;}
            .cube_player_details .row_player{overflow:hidden; font-size:1.1rem; color:#000; margin-top:10px; font-weight:bold; font-family:arial}
            .cube_player_details .row_player.active{color:#00b758}
            .cube_player_details .row_player .runs{float:right; font-family:arial;}
            
    `;
    const gadgetnowCss = `
    .btn-blue {background: #135394; height: 40px; padding: 0 15px; color: #fff; font-size: 18px; cursor: pointer; line-height: 40px; outline: none; border: 0; display: block; width: 60%; margin: 20px auto; border-radius: 5px; text-align: center;}
    .info-camparsion{display:block;text-align:center;color:#888;font-size: 1.2rem; line-height: 1.8rem;}
    .wdt-inline-gadgets{position:relative; width:100%; margin-bottom: 15px;}
    .wdt-inline-gadgets ul.scroll{display:flex; padding: 0 5px; width:100%; overflow: visible; white-space: nowrap;}
    .wdt-inline-gadgets ul.scroll li {display: inline-block; vertical-align: top; position:relative}
    .wdt-inline-gadgets ul.scroll li a{min-width:75px; width:75px;text-align:center;padding:0 5px 5px;border-bottom:2px solid #e5e5e5;flex-basis:100%;cursor:pointer;height:70px;position:relative;padding-top:10px; display: flex; flex-wrap: wrap; justify-content: center; align-items: center;}      
    .wdt-inline-gadgets ul.scroll li label{font-size:1.2rem; line-height:1.6rem; display:block;margin-top:5px;cursor:pointer; white-space: normal; max-height:3.2rem; overflow:hidden; width:100%}
    .container-comparewidget.pwa .wdt_compare-gadgets {padding: 4% 3%; margin-bottom: 3%; overflow: initial; background: #fff;}
    .container-comparewidget.pwa .wdt_compare-gadgets h1, .container-comparewidget.pwa .wdt_compare-gadgets h2{font-size:22px;margin:0 0 10px;font-weight:bold;}
    .container-comparewidget.pwa .wdt_compare-gadgets .search-suggest{font-size: 1.1rem; font-weight: normal; color: #888585;}
    .container-comparewidget.pwa .wdt_compare-gadgets .info-camparsion{color:#888;}
    .container-comparewidget.pwa .wdt_compare-gadgets .info-camparsion.red{color:#ff0000;}
    .container-comparewidget.pwa .wdt_compare-gadgets .input_field{margin-bottom:12px;width:100%;position:relative;}
    .container-comparewidget.pwa .wdt_compare-gadgets .input_field input[type=text]{padding:8px 25px 6px 10px;font-size:14px;box-sizing:border-box;width:100%;color:#000;border-radius:5px;outline:none;border:1px solid #d5d5d5;}
    .wdt_compare-gadgets .input_field:nth-child(4){display:none}

    .container-compare-show h1 {font-size: 1.8rem; line-height: 2.6rem; padding: 4% 3% 2%; margin: 0; background: #fff; word-break: break-word;}
    .wdt_compare-gadgets.details {background: #fff; padding: 5px; width: auto;}
    .wdt_compare-gadgets.details ul{display: flex; justify-content: space-between;}
    .wdt_compare-gadgets.details ul li{flex: 1 1 0px; border-radius: 0; border: 1px solid #e3e3e3; margin: 0 5px 0 0; padding: 15px 5px 5px; box-sizing: border-box; position:relative;min-height:220px;white-space:normal;}
    .wdt_compare-gadgets.details ul li:last-child{margin-right:0;}
    .wdt_compare-gadgets.details ul li span {display: block; text-align: center; margin: 0 0 5px;}
    .wdt_compare-gadgets.details ul li .img_wrap{height: 90px;margin-top:10px;}
    .wdt_compare-gadgets.details ul li .img_wrap img{max-height:80px;width:auto;height:auto;max-width:100%;}
    .wdt_compare-gadgets.details ul li .con_wrap h4{height:44px;font-weight:bold; font-size: 14px; line-height: 22px; color: #000; margin:0}
    .wdt_compare-gadgets.details ul li .con_wrap .price_tag{font-size:1.2rem; color:#d70000; line-height: 20px; font-weight: bold; font-family: arial;}
    .wdt_compare-gadgets.details ul li .btn_wrap{margin:5px 0 0 0;}
    .wdt_compare-gadgets.details ul li .btn_wrap .btn-buy{width: 100%; font-size: 12px; height: 25px; line-height: 28px; overflow:hidden}
    .wdt_compare-gadgets .input_field{width: 100%; position: relative;}
    .wdt_compare-gadgets.details .compare_criteria, .container-compare-show .wdt_compare{display:none}

    ul.gn-tabs{display:flex;-ms-flex-wrap:nowrap;flex-wrap:nowrap;overflow-x:auto;overflow-y:hidden;width:80%;margin:22px auto 20px;}
    ul.gn-tabs li{display:inline-block;text-align:center;white-space:nowrap;flex-basis:100%;cursor:pointer;}
    ul.gn-tabs li a{display: block; height: 3rem; line-height: 3rem;}
    ul.gn-tabs li:first-child{border-radius:5px 0 0 5px;}
    ul.gn-tabs li:last-child{margin-right:0;border-radius:0 5px 5px 0;}
    ul.gn-tabs li{display:block;color:#5a5a5a;font-size:1.4rem;line-height:3.8rem;height:3.0rem;border:solid 1px #acacac;}
    ul.gn-tabs li.active{color:#fff;background:#000;border-color:#000;}
    ul.gn-tabs li.active a{color:#fff;}
    ul.tabs {display: flex; flex-wrap: nowrap; overflow: auto; margin: 0 0 10px;}

    .con_gadgets{margin-bottom: 4%; background: #fff;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content{width:100%}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li{display:block; border: 1px solid #e3e3e3; color: #000; margin: 0 3% 4%; position: relative;}
    .mobile_body .wdt-inline-gadgets .slider .slider_content li .slide{box-shadow: none; min-height: auto; margin: 0; padding: 15px 3% 10px; position: static; display: flex; flex-wrap: wrap; font-family: Arial, Helvetica, sans-serif;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .prod_img{width:25%; text-align: left; height:auto; padding: 0;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .prod_img img{max-width:60px; height: auto;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .gadget_detail{width:75%; text-align: left; white-space: normal;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .gadget_detail .title{padding:0 25px 2px 0}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .gadget_detail .title a{font-size: 1.4rem; line-height: 2.2rem; max-height: 4.4rem; font-weight: bold;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul li .slide .gadget_detail .price{font-size: 1.8rem; font-weight: bold; margin-bottom: 5px; display: block; margin: 0 0 5px; font-family: arial;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage{display: flex; flex-wrap: wrap; justify-content: space-between;}
    .mobile_body .wdt-inline-gadgets .inline-gadgets-content ul.ram-storage li{font-size:1.2rem;line-height:1.8rem;color:#666666;padding:0;margin:0 0 5px 10px;position:relative; border: 0;}
    .mobile_body .wdt-inline-gadgets .slider .btnPrev, .mobile_body .wdt-inline-gadgets .slider .btnNext {display: none;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .prod_img{width:auto;height:120px;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .slide span{display:block; cursor: pointer;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider img{max-width:100%; max-height:100px; width:auto}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .title{display:inline-block;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .title .text_ellipsis{font-size:1.4rem;line-height:2.2rem; height:auto; max-height:4.4rem;font-weight:bold; display: -webkit-box;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating span{display:inline-block;vertical-align:top;position:relative;font-size:1.1rem; margin-right: 10px; color:#777}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating span:last-child{margin-right: 0;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating span a{color:#1a75ff}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating b{font-family:arial;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating b small{color:#f5a623;font-size:1rem;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .rating span:first-child{margin:0 10px 0 0;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .price{margin-top:5px;min-height:21px;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .price b{font-size:1.6rem; font-family:arial; font-weight:bold;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .btn_wrap{margin-top:5px;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .btn_wrap button{text-align: center; color: #000; padding: 0 8px; font-family: $font-family-name, arial, sans-serif; font-size: 1.2rem; border: 0; height: 28px; line-height: 32px; cursor: pointer; background: #fed20a; border-radius: 3px; outline: none; font-weight: bold; margin: 0;}
    .mobile_body .wdt-inline-gadgets .slider.trendingslider .gd_launch{font-size: 1.1rem; line-height: 1.6rem;}

    .gadgetlist_body.pwa .gl_top_bar {background: #fff; margin: 0; padding: 0 3%; overflow: hidden;}
    .gadgetlist_body.pwa .gl_top_bar .section {padding: 0;}
    .gadgetlist_body.pwa .gl_top_bar .section .top_section {margin: 0 0 4%;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs {width: 100%;padding: 0;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs span {display: none;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs ul {display:flex; margin: 5px 3% 15px 0;justify-content: flex-end;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs ul li {padding: 0 10px;border: 1px solid #2f6ba8;font-size: 12px;display: inline-block;vertical-align: top;font-weight: 400;height: 25px;line-height: 25px;margin: 0;min-width: 60px;box-sizing: border-box;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs ul li:first-child {border-radius: 15px 0 0 15px;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs ul li:last-child {border-radius: 0 15px 15px 0;}
    .mobile_body .gadgetlist_body.pwa .gl_tabs ul li.active {background: #135394;color: #fff;}
    .mobile_body .gadgetlist_body.pwa .gl_list_mobiles{background: #fff;padding-bottom: 4%;margin-bottom: 4%;}
    .gl_top_bar ul.gl_suggested_keywords li{display:inline-block;vertical-align:top;background:#e6e6e6;border:1px solid #dadada;white-space:nowrap;border-radius:18px;margin:0 15px 15px 0;}
    .gl_top_bar ul.gl_suggested_keywords li:last-child{margin-right:0;}
    .gl_top_bar ul.gl_suggested_keywords li a{display:block;font-size:1.1rem;color:#000;height:34px;line-height:34px;padding:0 20px;}
    .gl_top_bar ul.gl_suggested_keywords li.active a{color:#fff;font-weight:bold;}
    .gl_top_bar ul.gl_suggested_keywords li.active{background:#135394;border-color:#135394;}

    .productdetails_body.pwa h1{font-size: 1.8rem; line-height: 2.6rem; padding: 4% 3% 2%; margin: 0; background: #fff;}
    .productdetails_body.pwa .pd_top_nav {margin: 0; padding: 0 3% 4%; background: #fff; width: 100%; overflow-x: auto;}
    .productdetails_body.pwa .pd_top_nav .nav_items {width: 100%; border: 0;}
    .productdetails_body.pwa .pd_top_nav .nav_items ul li {margin: 0 20px 0 0;}
    .productdetails_body.pwa .pd_top_nav .nav_items ul li:last-child {margin: 0;}
    .productdetails_body.pwa .product_summary{background: #fff; margin-bottom: 4%; padding: 0 3% 4%; overflow:hidden}
    .productdetails_body.pwa .product_slider {width: 100%; margin: 0 0 4%; float: none;}
    .productdetails_body.pwa .product_slider ul.slider-tabs {display: block;margin: 0; overflow-x: auto; white-space: nowrap; width: auto;}
    .productdetails_body.pwa .product_slider ul.slider-tabs li {width: 210px; min-height: 200px; display: inline-block; vertical-align: top; margin: 0 10px 0 0; border: 1px solid #d1d1d1; position: relative;}
    .productdetails_body.pwa .product_slider ul.slider-tabs li img {max-height: 150px; margin: auto; width: auto; height: auto; max-width: 91%; position: absolute; top: 0; bottom: 0; left: 0px; right: 0;}
    .productdetails_body.pwa .share_container{display:none}
    .productdetails_body.pwa .tabs_circle {padding: 0 0 15px; text-align: center; background: #fff;}
    .productdetails_body.pwa .tabs_circle .tabs_circle_list {white-space: nowrap;}
    .productdetails_body.pwa .tabs_circle .tabs_circle_list li {display: inline-block; border-radius: 20px; height: 35px; line-height: 36px; margin: 0 2% 0 0; white-space: nowrap; cursor: pointer; width: 48%; vertical-align: top; text-align: center; border: 1px solid #333; background: #fff; color: #333; font-size: 1.2rem; font-weight: 700;}
    .productdetails_body.pwa .tabs_circle .tabs_circle_list li:last-child {margin-right: 0;}
`;
  const profieCss = `
            .section-topics h1 {font-size: 1.6rem; line-height: 2.4rem; margin:0 0 10px}  
            .section-topics .player-stats .con_stats{width:100%; background:#fff}
            .section-topics .player-stats .profile-date {font-size: 12px; line-height:18px; color: #a0a0a0; margin: 5px 0 0; font-family: Arial, Helvetica, sans-serif;}
            .section-topics .player-stats .lead-topics {margin: 0 0 20px; width: 100%;}
            .section-topics .player-stats .lead-topics .news-card.lead {margin: 0; box-sizing: border-box; padding: 10px; box-shadow: 0 1px 2px 1px rgba(0,0,0,0.1); overflow:hidden; min-height:180px;}
            .section-topics .player-stats .lead-topics .news-card.lead .img_wrap {float: left; margin: 0 10px 0 0;}
            .section-topics .player-stats .lead-topics .news-card.lead .img_wrap img {width: 120px;}
            .section-topics .player-stats .shortInfo li {font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: #333;}
            .section-topics .player-stats .shortInfo li span {display: inline-block; vertical-align:top; position:relative}
            .section-topics .player-stats .lead-topics .news-card.lead .con_wrap {margin: 0;}
            .section-topics .player-stats .profileInfo{margin:10px 0 0;}
            .section-topics .player-stats .profileInfo li {font-size: 13px; line-height: 19px; font-family: Arial, Helvetica, sans-serif; margin-bottom: 10px;}
            .section-topics .player-stats .profileInfo li .head {font-weight: bold; display:block}
            .section-topics .player-stats .profileInfo li .content {display:block}
            .section-topics .player-stats .profileInfo li:last-child{margin-bottom:0}
            .section-topics .player-stats .txt-wrap .only-txt, .section-topics .player-stats .profileDesc {font-size: 1.45rem; line-height: 2.7rem; display: block;}
            .section-topics .player-stats .enable-read-more .first_col .text_ellipsis {font-size: 1.4rem; line-height: 2.2rem; max-height: 13.2rem; -webkit-line-clamp: 6; overflow: hidden;}
    `;
  const movieshowCss = `
            .movieshow .movieshowlft {width:100%;}
            .movieshow {background:#fff;}
            .movieshow .moviewshowcard {background:#282e34;}
            .movieshow .moviewshowcard .mb10 {margin-bottom:10px;}
            .movieshow .moviewshowcard .movieinfo {padding:15px; color:#fff;}
            .movieshow .moviewshowcard .moviemeta {font-size:1.2rem; line-height:1.4rem;}
            .movieshow .moviewshowcard h1 {font-size:1.8rem; line-height:2.4rem; font-weight:bold; margin-bottom:5px;}
            .movieshow .moviewshowcard h1 b {border: 1px solid #fff;border-radius: 50%; width:24px; height:24px; font-size: 12px;vertical-align: top;color: #fff; line-height:24px; text-align:center; display:inline-block; margin-left:5px; font-weight:normal;}
            .movieshow .moviewshowcard .ms_button {border:1px solid #fff; border-radius:15px; height:30px; padding:0 15px; width:auto; margin:5px 0 15px; text-align:center; font-size:1.2rem; background:transparent; color:#fff;}
            .movieshow .moviewshowcard svg {margin-right:5px; vertical-align:middle; width:16px; height:16px;}
            .movieshow .moviewshowcard .ms_poster {margin-top:10px; overflow:hidden;}
            .movieshow .moviewshowcard .ms_poster .ms_posterimg {width: 135px; float:left;}
            .movieshow .moviewshowcard .ms_poster .ms_posterinfo { padding-left:150px; box-sizing:border-box;}
            .movieshow .moviewshowcard .ms_synopsis h3 {font-size:1.4rem; font-weight:bold; margin-bottom:10px;}
            .movieshow .moviewshowcard .ms_trailers {padding-bottom: 15px;}
            .movieshow .moviewshowcard .ms_trailers .section {padding: 0 15px; box-sizing:border-box;}
            .movieshow .moviewshowcard .ms_trailers .section h2 span {font-size: 1.4rem; color:#fff; font-weight:normal;}
            .movieshow .moviewshowcard .ms_trailers .slider ul li .con_wrap .text_ellipsis {color:#fff;}
            .movieshow .adcards {display: flex; justify-content:space-between; margin:10px 0;}
            .movieshow .adcards .box-item {width: 49%;}
            .movieshow .star-candidates {width:100%; box-sizing:border-box;}
            .movieshow .star_candidates_widget {background:#e6ecee; padding:0 10px 20px; box-sizing:border-box; width:100%;}
            .movieshow .star_candidates_widget .h2WithViewMore {margin: 0 0 10px 10px;}
            .movieshow .star_candidates_widget h2 {font-size:22px;}
            .movieshow .slider.candidates-newlist .btnPrev, .movieshow .slider.candidates-newlist .btnNext {top:50%; transform: translateY(-50%);}
            .movieshow .candidate_box {height:100%; box-sizing:border-box; font-weight:bold; border-radius: 5px; box-shadow: 0 0 4px 0 #c8d5d9; background-color: #ffffff; padding: 10px; text-align:center; margin: 2px;}
            .movieshow .candidate_box .candidate_imgwrap {display:inline-block; position:relative; margin-bottom:10px}
            .movieshow .candidate_box .candidate_imgwrap img {width:96px; height:96px; border-radius:50%; border:1px solid #e6ecee; padding:8px; box-sizing:border-box; object-fit: cover;}
            .movieshow .candidate_box .candidate_name {font-size: 1.4rem; display:block; text-align: center; max-width: 100%; overflow:hidden; text-overflow:ellipsis; margin: 0 auto; white-space: nowrap;}
            .movieshow .candidate_box .constituency {font-size: 1.2rem; color:#8c8c8c; display:block; marging: 5px 0 10px;}
            .movieshow .metaitems {display:inline-block; margin-right: 10px;}
            .movieshow .metafirst {display:inline-block; border-right: solid 1px #575757; padding-right:10px; margin-right:10px;}
            .movieshow .dotlist {display:inline-block; padding-left:13px; margin-left:7px;}

            .movieshow .rating {width: 100% !important; padding: 0; text-align: left; margin:15px 0;}
            .movieshow .rating .table_col {width: 50%;}
            .movieshow .rating .txt {font-size: 1.1rem;display:block; color:#fff;margin-top:5px;}
            .movieshow .rating .count {font-size: 1.4rem; display: inline-block; font-family: arial; color:#fff;position: relative; padding-left:25px;}
            .movieshow .rating .count b {font-size: 2.0rem; font-weight: 400;}
            .movieshow .empty-stars {color:#d8d8d8;}
            .movieshow .rating .critic:before {color:#ff001f;}
            .movieshow .rating .ratestar:before {color:#fc0;}
            .movieshow .rating .users:before {color: #00b3ff;}
            .movieshow .short-text {font-size:1.4rem; line-height:2rem;}
            .movieshow .short-text .readMoreText {position:relative; color:#00b3ff; cursor:pointer; margin-left:5px; font-size:1.3rem; display:inline-block;white-space:nowrap;}
            .movieshow .moreitems {position:relative; margin-left:7px;}
            .movieshow .moreitems ul {position:absolute; top:30px; left:0; background:#fff; border-radius:6px;z-index:999;}
            .movieshow .moreitems ul li {border-bottom:solid 1px #dbdbdb; padding: 5px 15px; color:#000; font-size:0.9rem;}
            .movieshow .moreitems ul li:last-child {border:none;}
  `;

  const glossaryCss = `
            .section-topics.glossary-list .section .top_section {position: relative; margin: 0 3% 4%;}
            .section-topics.glossary-list .section h2, .section-topics.glossary-list .section h1{font-size:1.8rem; margin:0}
            .mobile_body .section-topics.glossary-list{padding:0; background:#fff}
            .mobile_body .section-topics.glossary-list .only-heading .section{padding:0;}
            .mobile_body .section-topics.glossary-list .only-heading .section h1, .mobile_body .section-topics.glossary-list .only-heading .section h1 span:before{border:0;}
            .mobile_body .section-topics.glossary-list .tabs-circle{margin-bottom:0;}
            .mobile_body .section-topics.glossary-list .tabs-circle ul{flex-wrap:nowrap;overflow-x:auto; display: flex;}
            .mobile_body .section-topics.glossary-list .tabs-circle ul li{margin:0 20px 10px 0;border-radius:50%;border:solid 1px #575960;background:#fff;}
            .mobile_body .section-topics.glossary-list .tabs-circle ul li, .mobile_body .section-topics.glossary-list .tabs-circle ul li a{height:42px;width:42px;align-items:center;justify-content:center;text-transform:uppercase;color:#1f81db;font-size:16px;padding:0;display:flex;line-height:normal;flex-shrink:0;overflow:hidden;}
            .mobile_body .section-topics.glossary-list .tabs-circle ul li.active{background:#000;color:#fff;}
            .mobile_body .section-topics.glossary-list .tabs-circle:after{display:none;}
            .mobile_body .section-topics.glossary-list .wdt_glossary{overflow:hidden;clear:both;}
            .mobile_body .section-topics.glossary-list .wdt_glossary .section{padding:0;}
            .mobile_body .section-topics.glossary-list .wdt_glossary .box-item{padding-bottom:0;}
            .mobile_body .section-topics.glossary-list .wdt_glossary .box-item .news-card{margin-bottom:5px;border:0;padding-bottom:5px;}
            .mobile_body .section-topics.glossary-list .wdt_glossary .box-item .news-card .con_wrap .text_ellipsis{font-size:1.4rem;line-height:2.2rem;max-height:4.4rem;-webkit-line-clamp:2;word-break:break-word;}
    `;
  let commonStyle = commonCss + headerCss + gadgetnowCss;

  topicsCss += glossaryCss + profieCss;
  let othersCss = budgetCss + worldcupCss + topicsCss;

  const csrCritical =
    homeCss +
    articleshowCss +
    moviereviewCss +
    videolistCss +
    videoshowCss +
    liveblogCss +
    photoshowCss +
    newsbriefCss +
    glossaryCss +
    photoshowCss +
    photolistCss +
    electionsCss +
    movieshowCss +
    worldcupCss +
    budgetCss +
    profieCss;

  if (portalName == "nbt") {
    commonStyle += `
            ${/* common css start */ ""}
            @media (max-width: 319px) {
              html {font-size: 62.5%;}
            }

            @media (min-width: 320px) and (max-width: 359px) {
              html {font-size: 68.75%;}
            }

            @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ), (min--moz-device-pixel-ratio : 3 ), (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
              html {font-size: 68.75%;}
            }

            @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ), (max--moz-device-pixel-ratio : 2.9 ), (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
              html {font-size: 75.00%;}
            }
            .header .logo img {width: 81px;}
            .header .logo .etLogo img{width:57px;}
            .header .goldnav a {width: 23px;height: 23px;display: block;}
            .header .goldnav a svg {width: 23px;height: 23px;}
            .hrnavigation ul li a.nav_link[href*=nbtgold] {color: #f5ae2d;}
            .hrnavigation ul li a.nav_link{font-size:1.5rem}
            .mobile_body .wdt_poll .pollContainer .btnsubmit{line-height:33px}
            `;
  }

  if (portalName == "eis") {
    commonStyle += `
            .header .logo img {width: 75px; background:#fff}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:75px; height:34px; background: url(https://eisamay.indiatimes.com/photo/67160861.cms) 0 0 no-repeat; background-size: 75px 34px;}
            .header .goldnav a {width: 23px;height: 23px;display: block;}
            .header .goldnav a svg {width: 23px;height: 23px;}
            .hrnavigation ul li a.nav_link[href*=eisamaygold] {color: #f5ae2d;}
    
        `;
  }
  if (portalName == "mt") {
    commonStyle += `
            .header .logo img {width: 120px;}
            .header .logo.logo-tech img{width:89px}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:120px; height:19px; background: url(https://maharashtratimes.com/photo/67345383.cms) 0 0 no-repeat; background-size: 120px 19px;}
        `;
  }
  if (portalName == "vk") {
    commonStyle += `
            .header .logo img {width: 109px;}
            .header .logo a{line-height:55px}
            .header .logo.logo-tech img{width:88px}
            .header .logo.logo-tech a{line-height:50px}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:120px; height:26px; background: url(https://vijaykarnataka.com/photo/67175293.cms) 0 0 no-repeat; background-size: 120px 26px;} 
            .vk-mini .header .logo img, body.enable-video-show .vk-mini .header .logo img{display:none;}
            .vk-mini .header .logo a{width:86px; height:38px; background: url(https://vijaykarnataka.com/photo/77152342.cms) 0 0 no-repeat; background-size: 86px 38px; margin-top:3px} 
            body.enable-video-show .vk-mini .header .logo a{width:86px; height:38px; background: url(https://vijaykarnataka.com/photo/68096869.cms) 0 0 no-repeat; background-size: 86px 38px;} 
            .vk-lavalavk .header .logo img, body.enable-video-show .vk-lavalavk .header .logo img{display:none;}
            .vk-lavalavk .header .logo a{width:70px; height:14px; background: url(https://vijaykarnataka.com/photo/69338768.cms) 0 0 no-repeat; background-size: 70px 14px;} 
            body.enable-video-show .vk-lavalavk .header .logo a{width:70px; height:14px; background: url(https://vijaykarnataka.com/photo/69338758.cms) 0 0 no-repeat; background-size: 70px 14px;} 
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "tlg") {
    commonStyle += `
            .header .logo img {width: 93px;}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:93px; height:31px; background: url(https://telugu.samayam.com/photo/67455141.cms) 0 0 no-repeat; background-size: 93px 31px;}
            .viral-adda .header .logo img{display:none;}
            .viral-adda .header .logo a{width:91px; height:41px; background: url(https://telugu.samayam.com/photo/67453191.cms) 0 0 no-repeat; background-size: 91px 41px;}
            body.enable-video-show .viral-adda .header .logo a{width:91px; height:41px; background: url(https://telugu.samayam.com/photo/67522691.cms) 0 0 no-repeat; background-size: 91px 41px;}
            .viral-adda .box-content h2, .viral-adda .sectionHeading h1{padding: 0; margin:5%; width: 90%;}
            .viral-adda .box-content h2:before, .viral-adda .sectionHeading h2:before, .viral-adda .box-content.videoview h2:before{height: 2px; top: 32%; width: 100%; background:#0c2acf;}
            .viral-adda .box-content h2:after, .viral-adda .sectionHeading h2:after{background:#0c2acf; top:58%}
            .viral-adda .box-content h2 span, .viral-adda .sectionHeading h1 span{padding: 0 10px 0 0; color:#0c2acf}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{height:auto; font-size:1.6rem}
            .m-scene.tech .section h3 span, .m-scene.tech .section h2 span, .m-scene.tech .section h1 span{height:auto; line-height:30px; max-width:65%; padding:10px 10px 5px 0px}
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "tml") {
    commonStyle += `
            @media (max-width: 319px) {
                html {font-size: 50.00%;}
            }

            @media (min-width: 320px) and (max-width: 359px) {
                html {font-size: 56.25%;}
            }

            @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ), (min--moz-device-pixel-ratio : 3 ), (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
                html {font-size: 56.25%;}
            }

            @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ), (max--moz-device-pixel-ratio : 2.9 ), (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
                html {font-size: 62.5%;}
            }
            .header .logo img {width: 88px;}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:88px; height:30px; background: url(https://tamil.samayam.com/photo/67455143.cms) 0 0 no-repeat; background-size: 88px 30px;}
            .viral-adda .header .logo img{display:none;}
            .viral-adda .header .logo a{width:89px; height:39px; background: url(https://tamil.samayam.com/photo/67453192.cms) 0 0 no-repeat; background-size: 89px 39px;}
            body.enable-video-show .viral-adda .header .logo a{width:89px; height:39px; background: url(https://tamil.samayam.com/photo/67522697.cms) 0 0 no-repeat; background-size: 89px 39px;}
            .viral-adda .box-content h2, .viral-adda .sectionHeading h1{padding: 0; margin:5%; width: 90%;}
            .viral-adda .box-content h2:before, .viral-adda .sectionHeading h2:before, .viral-adda .box-content.videoview h2:before{height: 2px; top: 32%; width: 100%; background:#0c2acf;}
            .viral-adda .box-content h2:after, .viral-adda .sectionHeading h2:after{background:#0c2acf; top:58%}
            .viral-adda .box-content h2 span, .viral-adda .sectionHeading h1 span{padding: 0 10px 0 0; color:#0c2acf}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{height:auto;}
            .m-scene.tech .section h3 span, .m-scene.tech .section h2 span, .m-scene.tech .section h1 span{height:auto; line-height:25px; max-width:65%; padding:10px 10px 5px 0px}
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "mly") {
    commonStyle += `
            @media (max-width: 319px) {
              html {font-size: 56.25%;}
            }

            @media (min-width: 320px) and (max-width: 359px) {
              html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-min-device-pixel-ratio : 3 ), (min--moz-device-pixel-ratio : 3 ), (-o-min-device-pixel-ratio : 3/1 ), (min-device-pixel-ratio : 3 ) {
              html {font-size: 62.5%;}
            }

            @media (min-width: 360px) and (-webkit-max-device-pixel-ratio : 2.9 ), (max--moz-device-pixel-ratio : 2.9 ), (-o-max-device-pixel-ratio : 2.9/1 ), (max-device-pixel-ratio : 2.9 ) {
              html {font-size: 68.75%;}
            }
            .header .logo img {width: 100px;}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            .mobile_body .hrnavigation ul li a.nav_link{font-size: 1.4rem; font-family:arial}
            body.enable-video-show .header .logo img{display:none;}
            body.enable-video-show .header .logo a{width:100px; height:29px; background: url(https://malayalam.samayam.com/photo/67455139.cms) 0 0 no-repeat; background-size: 100px 29px;}
            .m-scene.tech .section h3, .m-scene.tech .section h2, .m-scene.tech .section h1{height:auto; font-size:1.6rem}
            .m-scene.tech .section h3 span, .m-scene.tech .section h2 span, .m-scene.tech .section h1 span{height:auto; line-height:25px; max-width:65%; padding:10px 10px 5px 0px}
            .statestoggle span {line-height: 26px;}
        `;
  }
  if (portalName == "iag") {
    commonStyle += `
            .header .logo img {width: 106px;}
            .navbar .header .app_download .appdownload_icon{width:50px;}
            .container.iag .comment_wrap .btn_comment, .container.iag .moviereview-summaryCard .con_review_rate{display:none !important}
        `;
  }

  if (pagetype == "home") {
    commonStyle += homeCss;
  } else if (pagetype == "articlelist") {
    commonStyle += worldcupCss;
  } else if (pagetype == "videolist") {
    commonStyle += videolistCss;
  } else if (pagetype == "articleshow") {
    commonStyle += articleshowCss;
  } else if (pagetype == "videoshow") {
    commonStyle += videoshowCss;
  } else if (pagetype == "moviereview") {
    commonStyle += moviereviewCss;
  } else if (pagetype == "liveblog") {
    commonStyle += liveblogCss;
  } else if (pagetype == "topics") {
    commonStyle += topicsCss;
  } else if (pagetype == "photoshow") {
    commonStyle += photoshowCss;
  } else if (pagetype == "photolist") {
    commonStyle += photolistCss;
  } else if (pagetype == "newsbrief") {
    commonStyle += newsbriefCss;
  } else if (pagetype == "elections") {
    commonStyle += electionsCss;
  } else if (pagetype == "movieshow") {
    commonStyle += movieshowCss;
  } else if (pagetype == "others") {
    commonStyle += othersCss;
  }
  commonStyle += `${/* Theme style css passed through feed will show here, if available */ ""}
                   ${themeStyle}
                  `;

  return {
    commonStyle,
    csrCritical,
  };
};

module.exports = {
  common: criticalCss,
  csrCritical: criticalCss(process.env.SITE, null, null, null).csrCritical,
};
