/* eslint-disable camelcase */
/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-danger */
/* eslint-disable indent */
import React from "react";
import PropTypes from "prop-types";
import serialize from "serialize-javascript";
import { SeoSchema } from "../../components/common/PageMeta"; // for desktop critical css
import tpsdk from "../../components/lib/analytics/lib/tpsdk";

// import tpConfig from "../../components/common/TimesPoints/TimesPointConfig";
import { _getDeviceRelatedJS, _getMainCss, _getMainJS, _getPreRenderAdsJS, loadInterStitialJS } from "./utils";

import {
  _getStaticConfig,
  isMobilePlatform,
  getPWAmetaByPagetype,
  filterAlaskaData,
  deleteNode,
  filterInitialState,
  getPageType,
  isProdEnv,
} from "../../utils/util";

import { getCookie, setCookie, eraseCookie, handleScroll, loadGeoAPI, loadCCaud, dfpBackupForCTN } from "./services"; // For Page SEO/Head Part
const criticalCss = require("./criticalCss"); // for mobile critical css
const criticalCssD = require("./criticalCssD");
const ASSET_PATH = process.env.ASSET_PATH || "/";
const SITE_PATH = process.env.SITE_PATH || "";
const siteConfig = _getStaticConfig();
const tpConfig = siteConfig.TimesPoint;
const fs = require("fs");

function Html({ js, css, html, head, initialState, version, pagetype, reqURL }) {
  // For events theming. to add CSS rules in critical
  // Will pass css string to criticalCSS component
  const highChartsPagesArray = ["elections"];
  const siteName = process.env.SITE;
  const platform = process.env.PLATFORM;
  const themeStyle =
    initialState.articlelist.value &&
    initialState.articlelist.value[0] &&
    initialState.articlelist.value[0].theme &&
    initialState.articlelist.value[0].theme.style
      ? initialState.articlelist.value[0].theme.style
      : "";

  const criticalStyle =
    process.env.PLATFORM == "desktop"
      ? criticalCssD.common(process.env.SITE === "eisamay" ? "eis" : process.env.SITE, themeStyle, pagetype, reqURL)
      : criticalCss.common(process.env.SITE === "eisamay" ? "eis" : process.env.SITE, themeStyle, pagetype, reqURL);
  const pwa_meta = getPWAmetaByPagetype(initialState, pagetype);

  function csrCriticalDev() {
    if (["development"].includes(process.env.DEV_ENV)) {
      const fileContent = process.env.PLATFORM == "desktop" ? criticalCssD.csrCritical : criticalCss.csrCritical;
      // Path of the new file with its name
      const filepath = "src/public/csrcritical.css";

      fs.writeFile(filepath, fileContent, err => {
        if (err) throw err;

        console.log("CSR Critical Css file was succesfully saved!");
      });
    }
  }
  csrCriticalDev();

  let editorName;
  if (pwa_meta) {
    if (pagetype === "moviereview") {
      editorName = pwa_meta.cd ? pwa_meta.cd : "no-author";
    } else {
      editorName = pwa_meta.cd ? pwa_meta.cd : pwa_meta.editorname ? pwa_meta.editorname : "no-author";
    }
  }
  editorName =
    editorName &&
    editorName
      .toString()
      .toLowerCase()
      .replace(/ /g, "-");

  // just to make sure author name is present
  // if (!editorName) {
  //   editorName = "no-author";
  // }
  // Setting banner data for CSR
  let bannerObj = "";
  try {
    if (initialState.header && initialState.header.alaskaData) {
      const HomePageWidgets = filterAlaskaData(
        initialState.header.alaskaData,
        ["pwaconfig", "HomePageWidgets"],
        "label",
      );
      const keys = Object.keys(HomePageWidgets);
      if (keys) {
        for (let i = 0; i < 20; i++) {
          if (
            keys[i] &&
            HomePageWidgets[keys[i]] &&
            HomePageWidgets[keys[i]]._type &&
            HomePageWidgets[keys[i]]._type == "banner"
          ) {
            bannerObj = HomePageWidgets[keys[i]];
            break;
          }
        }
      }
    }
  } catch (ex) {
    console.log("Error", ex);
  }
  if (pagetype === "home") {
    deleteNode(initialState.header.alaskaData, "pwaconfig", "");
    if (
      initialState.home &&
      Array.isArray(initialState.home.value) &&
      initialState.home.value[0] &&
      initialState.home.value[0].recommended
    ) {
      delete initialState.home.value[0].recommended;
    }
  } else {
    deleteNode(initialState.header.alaskaData, "pwaconfig", "HomePageWidgets");
  }
  // Deleting Footer state to reduce inital state
  deleteNode(initialState.header.alaskaData, "pwaconfig", "Footer");
  // if (pagetype === "articleshow") {
  filterInitialState(initialState);
  // }

  const interStialConfig = initialState.app && initialState.app.interstial;
  const interStialdata = (interStialConfig && interStialConfig[siteName]) || "";

  const vignetteStatus = initialState.app && initialState.app.vignette;
  const vignettedata = (vignetteStatus && vignetteStatus[siteName]) || "";

  const mobileinterstialStatus = initialState.app && initialState.app.AdxMobileInterstial;
  const AdxMobileInterstial = (mobileinterstialStatus && mobileinterstialStatus[siteName]) || "";

  // const ccaudUrl = `https://ade.clmbtech.com/cde/ae/${siteConfig.ccaudId}/var=_ccaud?_u=${siteConfig.weburl + reqURL}`;

  // let atfAdSize = [];

  // if (globalconfig.newAtfAdSize) {
  //   atfAdSize = globalconfig.newAtfAdSize;
  // }

  // console.log("dataaaa", initialState.gadgetlist.gadgetsData.pwa_meta);
  const isComparePages = reqURL && reqURL.includes("/tech/compare-");
  const isGadgetList =
    initialState &&
    initialState.gadgetlist &&
    initialState.gadgetlist.gadgetsData &&
    initialState.gadgetlist.gadgetsData.pwa_meta;

  const isGadgetShowPage =
    initialState && initialState.gadgetshow && initialState.gadgetshow.data && initialState.gadgetshow.data.gadgetData;

  // console.log("isGadgetList", initialState);

  return (
    <html lang={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"}>
      <head>
        {/* process.env.NODE_ENV=='production'?
          <link href={"/bundle."+version+".css"} rel="preload"  as="style" />
          :
          null
        */}
        {/* css.map(css => <link key={css} rel="preload"  as="style" href={`${ASSET_PATH}${css}`} />) */}
        {/* analytics and gpt ads services are added in headwith defer */}
        {/* <script defer={true} src="https://www.google-analytics.com/analytics.js" type="text/javascript"></script> 

        {/* Common schema organization and website */}
        <noscript>
          <style>{`.nojsicon {display:none;}`}</style>
          Please enable javascript.
        </noscript>
        {SeoSchema().commonSchema({ requrl: reqURL })}
        <meta value="summary_large_image" name="twitter:card" />
        <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
        {/* <meta content="NOINDEX, NOFOLLOW" name="ROBOTS" /> */}
        <link rel="shortcut icon" href={siteConfig.icon} />
        <meta property="fb:admins" content="556964827" />
        <meta property="fb:app_id" content="972612469457383" />
        <meta content="SSM4580016100404216113TIL" name="tpngage:name" />
        <meta content={siteConfig.google_site_verification} name="google-site-verification" />
        <meta content="text/html; charset=UTF-8" httpEquiv="Content-Type" />
        <meta httpEquiv="content-language" content={siteConfig.languagemeta ? `${siteConfig.languagemeta}` : "en"} />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge,chrome=1" />
        {head.title.toComponent()}
        {head.meta.toComponent()}
        <meta
          name="viewport"
          content="width=device-width, height=device-height,initial-scale=1.0,user-scalable=yes,maximum-scale=5"
        />
        {/* {process.env.SITE == "nbt" && pagetype && (pagetype == "photolist" || pagetype == "recommended") ? (
          <meta content="NOINDEX,NOFOLLOW" name="robots" />
        ) : null} */}
        {(isComparePages || isGadgetList || isGadgetShowPage) && <meta content="noodp,noydir" name="robots" />}
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta content="yes" name="apple-touch-fullscreen" />
        <meta name="msapplication-tap-highlight" content="no" />
        {/* Instant article activation meta */}
        <meta property="fb:pages" content={`${siteConfig.fbIAcontentId}`} />
        {/* Need to preload or DNS-prefetch a manifest.json file for performance */}
        {/* preconnect a font file to eliminate render-blocking resources */}
        <link rel="preload" href={`${siteConfig.fontUrl}`} as="font" crossOrigin="anonymous" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="manifest" href={`/${SITE_PATH}manifest.json`} />
        {/* prefetching */}
        <link rel="dns-prefetch" href={`https://${siteConfig.staticSiteName}`} />
        <link rel="dns-prefetch" href={`https://${siteConfig.siteName}`} />
        <link rel="dns-prefetch" href="https://static.langimg.com" />
        <link rel="dns-prefetch" href="https://ade.clmbtech.com" />
        <link rel="dns-prefetch" href="https://static.clmbtech.com" />
        <link rel="dns-prefetch" href="https://image2.pubmatic.com" />
        <link rel="dns-prefetch" href="https://image6.pubmatic.com" />
        <link rel="dns-prefetch" href="https://www.google-analytics.com" />
        {/* <link rel="dns-prefetch" href="https://www.googletagservices.com" /> */}
        <link rel="dns-prefetch" href="https://securepubads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="https://adservice.google.co.in" />
        <link rel="dns-prefetch" href="https://connect.facebook.net" />

        {/* prefetching */}
        {/* <link rel="apple-touch-icon" sizes="72x72" href={`${ASSET_PATH}${SITE_PATH}icons/icon-72x72.png`} />
        <link rel="apple-touch-icon" sizes="96x96" href={`${ASSET_PATH}${SITE_PATH}icons/icon-96x96.png`} />
        <link rel="apple-touch-icon" sizes="128x128" href={`${ASSET_PATH}${SITE_PATH}icons/icon-128x128.png`} />
        <link rel="apple-touch-icon" sizes="144x144" href={`${ASSET_PATH}${SITE_PATH}icons/icon-144x144.png`} />
        <link rel="apple-touch-icon" sizes="152x152" href={`${ASSET_PATH}${SITE_PATH}icons/icon-152x152.png`} />
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="384x384" href={`${ASSET_PATH}${SITE_PATH}icons/icon-384x384.png`} /> */}
        <link rel="apple-touch-icon" sizes="192x192" href={`${ASSET_PATH}${SITE_PATH}icons/icon-192x192.png`} />
        <link rel="apple-touch-icon" sizes="512x512" href={`${ASSET_PATH}${SITE_PATH}icons/icon-512x512.png`} />
        {/* <link href="https://fonts.googleapis.com/css?family=Mukta" rel="stylesheet" /> */}
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content={`${ASSET_PATH}${SITE_PATH}icons/icon-144x144.png`} />
        <meta name="theme-color" content="#ffffff" />
        <meta name="apple-mobile-web-app-title" content={siteConfig.siteName} />
        <meta name="application-name" content={siteConfig.siteName} />
        <script defer src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" type="text/javascript" />
        {/* <script defer src={ccaudUrl} type="text/javascript" /> */}
        {head.link.toComponent()}
        {/* print critical css */}
        {/* themeStyle contains css rules string */}
        {process.env.SITE == "nbt" || process.env.SITE == "mt" ? (
          <link href={`${siteConfig.fontUrl}`} rel="stylesheet" />
        ) : (
          <style
            dangerouslySetInnerHTML={{
              __html: `${siteConfig.fontContent}`,
            }}
          ></style>
        )}

        <style id="criticalCss">{criticalStyle.commonStyle}</style>
        <script
          dangerouslySetInnerHTML={{
            __html: `${getCookie()} ${setCookie()} ${eraseCookie()}`,
          }}
        />

        {/* InterStial */}
        {platform == "desktop" &&
        pagetype &&
        pagetype == "home" &&
        interStialdata &&
        interStialdata.status == "true" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `${loadInterStitialJS(interStialdata)}`,
            }}
          />
        ) : null}
        {/* Vignette Ads for Desktop Only: Interstitial in Between Page */}
        {typeof isMobilePlatform === "function" && !isMobilePlatform() && process.env.DEV_ENV == "production" ? (
          <script
            data-ad-client="ca-pub-1902173858658913"
            async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          />
        ) : null}
        {process.env.DEV_ENV !== "production" || (reqURL && reqURL.indexOf("enableCWV") > -1) ? (
          <React.Fragment>
            <script defer src="https://unpkg.com/web-vitals"></script>
            <script
              dangerouslySetInnerHTML={{
                __html: `
            addEventListener('DOMContentLoaded', function() {
              webVitals.getCLS(console.log, true);
              webVitals.getFID(console.log);
              webVitals.getLCP(console.log);
            })`,
              }}
            />
          </React.Fragment>
        ) : null}
        <script
          dangerouslySetInnerHTML={{
            __html: `if(/Trident/.test(navigator.userAgent))
              document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.10.4/polyfill.min.js"/>')`,
          }}
        />
        <div id="comscoreContainer"></div>
        <script
          dangerouslySetInnerHTML={{
            __html: `window.landing_page = "${siteConfig.weburl + reqURL}";`,
          }}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `${loadGeoAPI(reqURL)}`,
          }}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `${loadCCaud(reqURL)}`,
          }}
        />
        {/* Add GMT Script  Google Tag Manager */}
        {
          <script
            dangerouslySetInnerHTML={{
              __html: `(function(w,d,s,l,i){w[l]=w[l]||[];${
                pagetype === "articleshow" ||
                pagetype === "photoshow" ||
                // pagetype === "videoshow" ||    // will not be firing authornamepushed here as it will be difficult to handle
                pagetype === "moviereview"
                  ? `w[l].push({'event': 'authorNamePushed', 'authorName': '${editorName}'})`
                  : null
              };w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
              var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.defer=true;j.src=
           'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
           })(window,document,'script','dataLayer','${siteConfig.ga.gtm}');`,
            }}
          />
        }
        {/* Grx script + initial pageview call */}
        {
          <script
            dangerouslySetInnerHTML={{
              __html: `(function(g, r, o, w, t, h, rx) {
                        (g[t] =
                          g[t] ||
                          function() {
                            (g[t].q = g[t].q || []).push(arguments);
                          }),
                          (g[t].l = 1 * new Date());
                        (g[t] = g[t] || {}), (h = r.createElement(o)), (rx = r.getElementsByTagName(o)[0]);
                        h.async = 1;
                        h.src = w;
                        rx.parentNode.insertBefore(h, rx);
                      })(window, document, "script", "https://static.growthrx.in/js/v2/web-sdk.js", "grx");
                      grx('config','applicationServerKey','BDp8pYlqIqwIZw0JLBj91Phfr4w0tNxcmtYtcpVxYinzQIWcTkxHAoDj_GYhxgG_zEluRkkcQrC7sOpyZWAoJ3k'); //We need to use the same key as izooto currently uses
                      grx('config','service_worker','/service-worker.js?v=${version}')
                      grx('config','notification_params', {utm_source:"GrowthRx", utm_medium:"push_notifications"});
                      grx("init", "${isProdEnv() ? siteConfig.growthrx.prod.id : siteConfig.growthrx.stg.id}");
                      if(window.location.pathname && window.location.pathname.indexOf('videoshow') == -1) {
                        var _grxLandingPageEventDetails =  {
                          url: window.location.pathname,
                          screen_type: "${pagetype}",
                        }
                        if (window.navigator && window.navigator.connection) {
                            if (window.navigator.connection.downlink) {
                              _grxLandingPageEventDetails.network_browser_speed =  window.navigator.connection.downlink;
                            }
                            if (window.navigator.connection.effectiveType) {
                              _grxLandingPageEventDetails.network_browser_effective_type = window.navigator.connection.effectiveType
                            }
                        }
                        if("${pagetype}" == "videoshow" || "${pagetype}" == "articleshow" || "${pagetype}" == "moviereview" || "${pagetype}" == "photoshow") {
                          // Split pathname to get location and msid of article
                          // Eg ["/metro/mumbai/other-news/bmc-has-started-preparati…izens-extra-bed-in-hospitals-during-corona-crisis", "81757096.cms"]
                          const [pageLocation, _] = window.location.pathname.split("/${pagetype}/");
                  
                          // Remove seolocation by splitting against last "/"
                          // Eg "/metro/mumbai/other-news"
                          const pageSections = pageLocation.slice(0, pageLocation.lastIndexOf("/"));
                  
                          // Split levels again and only keep truthy values (non empty strings)
                          //  Eg ["", "metro", "mumbai", "other-news"] -> ["metro", "mumbai", "other-news"]
                          const pageSectionLevels = pageSections.split("/").filter(level => level);
                  
                          // Set all these section levels dynamically for grx calls
                          pageSectionLevels.forEach((level, index) => {
                            _grxLandingPageEventDetails[\`section_l\${index + 1}\`] = level;
                          });
                        }
                        grx("track", "page_view",_grxLandingPageEventDetails);
                      }                   
              `,
            }}
          />
        }
        {process.env.SITE == "nbt" || process.env.SITE == "mt" || process.env.SITE == "iag" ? (
          <React.Fragment>
            <script defer src={siteConfig.indexExchangeUrl} type="text/javascript" />
            <script defer src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" type="text/javascript" />
          </React.Fragment>
        ) : (
          <script
            dangerouslySetInnerHTML={{
              __html: `var PWT={}; 
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        var gptRan = false;
        PWT.jsLoaded = function(){ 
            loadGPT();
        };
        var loadGPT = function() {
            if (!gptRan) {
              gptRan = true;
              var gads = document.createElement('script');
              gads.src = 'https://securepubads.g.doubleclick.net/tag/js/gpt.js';
              var node = document.getElementsByTagName('script')[0];
              node.parentNode.insertBefore(gads, node);
            }
        };
        // 500 ms timeout can be updated as per publisher preference.
        setTimeout(loadGPT, 500);

        (function() {
            var purl = window.location.href;
            var url = '${siteConfig.pwtUrl}';
            var profileVersionId = '';
            if(purl.indexOf('pwtv=')>0){
                var regexp = /pwtv=(.*?)(&|$)/g;
                var matches = regexp.exec(purl);
                if(matches.length >= 2 && matches[1].length > 0){
                    profileVersionId = '/'+matches[1];
                }
            }
            var wtads = document.createElement('script');
            wtads.async = true;
            wtads.type = 'text/javascript';
            wtads.src = url+profileVersionId+'/pwt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(wtads, node);
        })();`,
            }}
          />
        )}
        {/* {reqURL.indexOf("/articleshow_esi_test/") > -1 ? null : (
            <script defer src={ccaudUrl} type="text/javascript" />
          )} */}
        {isMobilePlatform() && process.env.SITE != "mt" && pagetype && pagetype === "articleshow" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `${handleScroll()}`,
            }}
          />
        ) : reqURL.indexOf("/articleshow_esi_test/") > -1 ? (
          <script defer src="https://static.clmbtech.com/ad/commons/js/colombia_test.js" type="text/javascript" />
        ) : (
          <script src={siteConfig.ads.ctnAddress} defer></script>
          // <script src="https://static.clmbtech.com/ctn/commons/js/colombia_v2.js" defer></script>
        )}
        {tpConfig && siteName && tpConfig.channels.includes(siteName) && <script dangerouslySetInnerHTML={tpsdk} />}
        {/* Start Google Analytics */}
        {/* Replacing google analytics with google tag manager */}
        {/* <script
          dangerouslySetInnerHTML={{
            __html: `window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
                      ga('create', '${siteConfig.ga.gatrackid}', 'auto');
                      ${
              pwa_meta && pwa_meta.editorname && pwa_meta.editorname != ""
                ? `ga('set', 
                                '${process.env.SITE == "nbt" ? "dimension4" : "dimension2"}' , 
                                '${pwa_meta.editorname}'
                            )`
                : ""
              }
                      ga('send', 'pageview');
                      ga('set','${process.env.SITE == "nbt" ? "dimension4" : "dimension2"}' , '')
                      window.sessionPageView ? (window.sessionPageView += 1) : (window.sessionPageView = 1);
                    `,
          }}
        /> */}
        <script
          dangerouslySetInnerHTML={{
            __html: `window.sessionPageView ? (window.sessionPageView += 1) : (window.sessionPageView = 1);`,
          }}
        />
        {pagetype === "articleshow" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `window && (window.dataLayer = window.dataLayer || []);`,
            }}
          />
        ) : null}
        {/* <script defer src="https://www.google-analytics.com/analytics.js" />  */}
        {/* End Google Analytics */}
        {
          /* add IBeat js which will handle all calls of Ibeat , so descarding internal iBeat calls */
          // moving to footer
          <script defer src="https://agi-static.indiatimes.com/cms-common/ibeat.min.js" />
        }
        <script
          dangerouslySetInnerHTML={{
            __html: `${_getDeviceRelatedJS(siteConfig)}`,
          }}
        />
        {/* Mobile Adx interstitial DFP ad  */}
        {typeof isMobilePlatform === "function" &&
        isMobilePlatform() &&
        AdxMobileInterstial &&
        AdxMobileInterstial.status === "true" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.googletag = window.googletag || { cmd: [] };
                  googletag.cmd.push(function() {
                    var slot = googletag.defineOutOfPageSlot(
                      '${siteConfig.ads.dfpads.interstitial}',
                      googletag.enums.OutOfPageFormat.INTERSTITIAL,
                    );
                    if (slot) slot.addService(googletag.pubads());
                    if (typeof colaud !== 'undefined') {
                      googletag
                      .pubads()
                      .setTargeting("sg", colaud.aud)
                    }
                    googletag.enableServices();
                    googletag.display(slot);
                  });`,
            }}
          />
        ) : null}
        {/* Izooto script will call on page interaction */}
        {/* Changes for Izooto and target property : VK */}
        {/* {reqURL && typeof reqURL === "string" ? (
          <React.Fragment>
            <script
              dangerouslySetInnerHTML={{
                __html: `                
                    window._izq = window._izq || []; window._izq.push(["init" ]);
                    window._izq.push(["registerSubscriptionCallback",function(obj) {
                      var event = new CustomEvent('browserNotification', {detail : obj});
                      document.dispatchEvent(event);                        
                    }]);                    
                `,
              }}
            />

            <script async type="text/javascript" src={siteConfig.izooto} />
          </React.Fragment>
        ) : null} */}
        {/* Load hightcharts conditionally , only for highChartsPagesArray pages  */}
        {reqURL && typeof reqURL === "string" && highChartsPagesArray.indexOf(getPageType(reqURL)) > -1 ? (
          <React.Fragment>
            <script defer src="https://code.highcharts.com/highcharts.js" />
            <script defer src="https://code.highcharts.com/modules/exporting.js" />
            <script id="highchart-script" defer src="https://code.highcharts.com/modules/export-data.js" />
          </React.Fragment>
        ) : null}
        {/* slike v3 loader.js file */}
        <script
          dangerouslySetInnerHTML={{
            __html: ` function SlikeLoader(){this.BASE_SDK_URL="//tvid.in/sdk",this.BEACON_BASE_URL="//slike.indiatimes.com/pixel?",this.PLAYBACK_CAPABILITIES={MSE:window.MediaSource&&window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"'),HLS:document.createElement("video").canPlayType("application/vnd.apple.mpegURL")},this.es6Support=this.isES6()}SlikeLoader.prototype.isES6=function(){try{return Function("() => {};"),!0}catch(e){return!1}},SlikeLoader.prototype.loadJs=function(e,o){var i=document.createElement("script"),n=document.getElementsByTagName("head")[0];i.src=e,i.onload=function(){o(!0)},i.onerror=function(){o(!1)},n.appendChild(i)},SlikeLoader.prototype.polyFillNeeded=function(){var e="function"==typeof window.Promise||!1,o="function"==typeof Object.assign||!1,i="function"==typeof window.URL||!1;return!(e&&o&&i)},SlikeLoader.prototype.load=function(e,o){var i=this;i.loadTimeStamp=Date.now(),i.config=e,i.config.noBeacon=!(i.config.video.id||!i.config.video.url),i.callback=o,i.env=e.env?e.env:"prod",i.version=e.version?e.version:"",i.isES5=!!e.isES5&&e.isES5,i.debug=!!e.debug&&e.debug,i.sendBeacon("vj=100&apikey="+i.config.apiKey+"&k="+i.config.video.id);var n=i.getSDKPath();if(i.polyFillNeeded()){var t=i.BASE_SDK_URL+"/lib/polyfills.js";i.loadJs(t,function(e){e?i.loadSDK(n):i.callback(!1,i.config)})}else i.loadSDK(n)},SlikeLoader.prototype.loadSDK=function(e){var i=this;i.loadJs(e,function(e){if(e){var o=Date.now()-i.loadTimeStamp;i.sendBeacon("vj=200&pfs="+o+"&apikey="+i.config.apiKey+"&k="+i.config.video.id),i.callback(!0,i.config)}else i.callback(!1,i.config),i.sendBeacon("vj=151&apikey="+i.config.apiKey+"&k="+i.config.video.id)})},SlikeLoader.prototype.sendBeacon=function(e){var o=this;if(!(o.config.noBeacon||o.config.GDPR_MODE||o.config.player.skipAnalytics)){var i=o.BEACON_BASE_URL+"lts="+o.loadTimeStamp+"&"+e;setTimeout(function(){var e=document.createElement("img");e.src=i,e.onload=function(){e=null}},500)}},SlikeLoader.prototype.getSDKPath=function(){var e=this,o=!1,i=!1,n="",t=e.BASE_SDK_URL;return"stg"==e.env&&(t+="/stg"),e.version&&(t=t+"/"+e.version),parseInt(e.config.player.slikeAdPercent)&&(o=!0),e.PLAYBACK_CAPABILITIES.MSE&&(i=!0),n+=o?"slikeima.":"",n+=i?"hls.":"",e.es6Support&&!e.isES5&&!e.isUnsupportedBrowser()||(n+="es5."),(e.debug||e.getDebugFlagFromLocalStorage())&&(n+="debug."),n+="spl.js",e.config.sdkBase="local"==e.env?"":t,"local"==e.env?"/"+n:t+"/"+(e.version?"":e.config.apiKey+".")+n},SlikeLoader.prototype.getDebugFlagFromLocalStorage=function(){try{return!!localStorage.getItem("__SPL__DEBUG__")}catch(e){return!1}},SlikeLoader.prototype.isUnsupportedBrowser=function(){var e=window.navigator.userAgent,o=e.indexOf("MSIE ");if(0<o)return parseInt(e.substring(o+5,e.indexOf(".",o)),10);if(0<e.indexOf("Trident/")){var i=e.indexOf("rv:");return parseInt(e.substring(i+3,e.indexOf(".",i)),10)}var n=e.indexOf("Edge/");return 0<n?parseInt(e.substring(n+5,e.indexOf(".",n)),10):-1!==e.indexOf("UCBrowser")||-1!==e.indexOf("UBrowser")},window.spl=new SlikeLoader;`,
          }}
        />
        {pagetype &&
        (pagetype == "moviereview" || pagetype == "movieshow" || pagetype == "liveblog" || pagetype == "photoshow") ? (
          <React.Fragment>
            {/* <script type="text/javascript" src="https://platform.twitter.com/widgets.js" rel="dns-prefetch" defer /> */}
            <script
              type="text/javascript"
              src="https://platform.instagram.com/en_US/embeds.js"
              rel="dns-prefetch"
              defer
            />
          </React.Fragment>
        ) : null}
        {/* {atfAdSize && atfAdSize.length > 0 && (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.newAtfAdSize = "${JSON.stringify(atfAdSize)}";`,
          />
        )} */}
        {bannerObj && (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.bannerData = "${encodeURIComponent(JSON.stringify(bannerObj))}";`,
            }}
          />
        )}
      </head>
      <body
        data-platform={isMobilePlatform() ? "mobile" : "desktop"}
        /* Condition for handling election widget */
        className={reqURL.indexOf("isElectionWidget=result") > -1 ? "election-widget-result" : ""}
      >
        {/* GTM Body Script */}
        {
          <noscript
            dangerouslySetInnerHTML={{
              __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=${siteConfig.ga.gtm}"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
            }}
          />
        }
        <div
          id="root"
          className={`container ${process.env.SITE}`}
          dangerouslySetInnerHTML={{
            __html: html,
          }}
        />
        {
          <div
            id="modal"
            className={typeof isMobilePlatform === "function" && isMobilePlatform() ? "mobile" : "desktop"}
          ></div>
        }
        {<div id="dock-root-container" className={`hide ${isMobilePlatform() ? `mobile` : `desktop`}`} />}
        {
          <div
            id="outer_minitv_container"
            draggable="true"
            className={`hide ${isMobilePlatform() ? `mobile` : `desktop`}`}
          />
        }
        {isMobilePlatform() && <div id="socialshare-parent-container" />}
        {!isMobilePlatform() && <div id="comments-parent-container" className="desktop_body" />}
        {process.env.NODE_ENV == "production" ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: "true",
                  API_ENDPOINT: process.env.API_ENDPOINT,
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  NODE_API_BASEPOINT: process.env.NODE_API_BASEPOINT,
                  API_TIMEOUT:
                    process.env.PLATFORM === "desktop" ? process.env.API_TIMEOUT : process.env.API_TIMEOUT_MOBILE,
                  IMG_URL: process.env.IMG_URL,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  SITE: process.env.SITE,
                  NODE_ENV: process.env.NODE_ENV,
                  DEV_ENV: process.env.DEV_ENV,
                  PLATFORM: process.env.PLATFORM == "desktop" ? "desktop" : "mobile",
                },
              })}`,
            }}
          />
        ) : (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: "true",
                  REDUX_LOGGER: process.env.REDUX_LOGGER,
                  API_ENDPOINT: process.env.API_ENDPOINT,
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  NODE_API_BASEPOINT: process.env.NODE_API_BASEPOINT,
                  API_TIMEOUT:
                    process.env.PLATFORM === "desktop" ? process.env.API_TIMEOUT : process.env.API_TIMEOUT_MOBILE,
                  IMG_URL: process.env.IMG_URL,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  SITE: process.env.SITE,
                  NODE_ENV: process.env.NODE_ENV,
                  DEV_ENV: process.env.DEV_ENV,
                  PLATFORM: process.env.PLATFORM == "desktop" ? "desktop" : "mobile",
                },
              })}`,
            }}
          />
        )}
        <script
          dangerouslySetInnerHTML={{
            __html: `window.__INITIAL_STATE__ = ${serialize(initialState)}`,
          }}
        />
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
              var googletag = googletag || {};
              googletag.cmd = googletag.cmd || [];
            `,
          }}
        />
        {/* {js.map(js => <script data-val={js} key={js} defer={true} src={`${ASSET_PATH}${js}`} />)} */}

        <script
          dangerouslySetInnerHTML={{
            __html: `${_getMainJS(js, ASSET_PATH, SITE_PATH, version, reqURL)}`,
          }}
        />
        {/* DMP JS
        <script src="https://static.clmbtech.com/ase/16153/790/aa.js" type="text/javascript"></script>
        <script defer={true} type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
        */}
        {css.map(css => (
          <script
            dangerouslySetInnerHTML={{
              __html: `${_getMainCss(css, ASSET_PATH, SITE_PATH)}`,
            }}
          />
        ))}
        {/* css.map(css => <link key={css} rel="stylesheet" href={`${ASSET_PATH}${css}`} />) */}
        {/* Prerender ads as atf and fbn  TODO : support for refresh and scn addition */}
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `${_getPreRenderAdsJS(pagetype, siteConfig, initialState.app.adconfig, pwa_meta || [])}`,
          }}
        />

        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `${dfpBackupForCTN()}`,
          }}
        />

        {/* Start ComScore Analytics */}
        {/* <div id="comscoreContainer"></div> */}
        {/* <script
          dangerouslySetInnerHTML={{
            __html: `${loadComscore(reqURL)}`,
          }}
        /> */}
        {/* End ComScore Analytics */}

        {/* kai ios Ads */}
        {/* <script
          dangerouslySetInnerHTML={{
            __html: `
            if ( navigator.userAgent.toLowerCase().indexOf('kaios') > -1  ) {
              
              var metaView = document.createElement("meta");
              metaView.setAttribute("content", "width=device-width, height=device-height,initial-scale=.8,user-scalable=yes,maximum-scale=5");
              metaView.setAttribute("name","viewport")
              document.head.appendChild(metaView);
              
              var _script = document.createElement("script");
              _script.defer = true;
              _script.src = "https://static.kaiads.com/ads-sdk/ads-sdk.v3.min.js" ;
              _script.defer = "defer";
              
              if(_script.readyState) { 
                _script.onreadystatechange = function() {
                  if ( _script.readyState === "loaded" || _script.readyState === "complete" ) {
                    _script.onreadystatechange = null;
                    loadKaiAds();
                  }
                };
              }
              else {
                _script.onload = function() {
                  loadKaiAds();
                }
              }

              document.getElementsByTagName("head")[0].appendChild(_script);
              function loadKaiAds(){ 
                if(document.readyState === "complete" || document.readyState === "loaded" || document.readyState === "interactive" ){
                  var getallAdscontainer=document.querySelectorAll('div[data-adtype]');                                        
                  //getallAdscontainer.forEach(KaiAds);
                  var i=0;
                  for(i==0;i<getallAdscontainer.length;i++){
                    KaiAds(getallAdscontainer[i]);
                  }                
                }
              }
              
              function AdsDimension(type,orientation){
                if(orientation=='h'){
                  return 100;
                }
                if(orientation=='w'){
                  return 320;
                }
              }

              function KaiAds(item){
                var getAdsType=item.getAttribute("data-adtype");
                //for production please remove test or set value test="0"
                if(getAdsType=='atf' || getAdsType=='fbn' || getAdsType=='mrec' || getAdsType=='mrec1' || getAdsType=='mrec2' || getAdsType=='btf'){
                  item.style.display = "block";
                  getKaiAd({
                    publisher: 'ef3cf10b-6bee-443e-a310-c1be4462f34a',
                    app: '${process.env.SITE}',
                    slot: ''+ getAdsType,
                    container: item,
                    test:1, 

                    h: AdsDimension(getAdsType,'h'),
                    w: AdsDimension(getAdsType,'w'),
                    timeout:10000,
                    onerror: err => console.log('Kai Ads error code:', err),
                    onready: ad => {
                        // Ad is ready to be displayed
                        // calling 'display' will display the ad
                        ad.call('display', {tabindex: 0,navClass: 'items',display: 'block'})
                    }
                  })
                }
              }
            }
            `,
          }}
        /> */}
      </body>
    </html>
  );
}

Html.propTypes = {
  js: PropTypes.array.isRequired,
  css: PropTypes.array.isRequired,
  html: PropTypes.string,
  head: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
};

export default Html;
