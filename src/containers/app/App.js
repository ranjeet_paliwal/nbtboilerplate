import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Loadable from "react-loadable";
import { browserHistory } from "react-router";

import { fireVideoPlayedGRXEvent } from "../../modules/videoplayer/slike";
import { fetchExitPollIfNeeded } from "../../campaign/election/actions/exitpoll/exitpoll";
import { fetchResultsIfNeeded } from "../../campaign/election/actions/results/results";
import { fetchElectionConfigSuccess } from "../../campaign/election/actions/electoralmap/electoralmap";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import { fetchTopListDataIfNeeded, fetchWidgetSequenceDataIfNeeded } from "./../../actions/home/home";
import { updateUserData } from "../../actions/authentication/authentication";
import { getSplatsVars } from "./../../components/lib/ads/lib/utils";
import {
  _getStaticConfig,
  _sessionStorage,
  getPageType,
  _isFrmApp,
  _setStaticConfig,
  isTechSite,
  filterAlaskaData,
  LoadingComponent,
  isMobilePlatform,
  _getCookie,
  shouldTPRender,
  getElectionConfigFromData,
  _loadsvgsprite,
  checkTopAtf,
  addCriticalCss,
  isGadgetPage,
} from "../../utils/util";

import mainCss from "./../../public/main.css"; //DON'T REMOVE THIS - this is merging this into final css loader module in webpack
import { loadJS, get_Scroll_dir, _isCSR, _filterRecursiveALJSON } from "../../utils/util";
import { fetchMiniScorecardDataIfNeeded } from "../../modules/scorecard/actions/scorecard";
// import PushNotificationCard from "../../components/common/PushNotificationCard";

import {
  showVideoPopup,
  addSliderData,
  addVideoConfig,
  removeVideoConfig,
  setVideoHeader,
  setVideoMsid,
  setVideoType,
  removeSliderData,
  setMinitvVisibility,
  setMinitvPlaying,
  updateVideoConfig,
  updateAdState,
} from "../../actions/videoplayer/videoplayer";

import Ads_Module from "./../../components/lib/ads/index";
import { fetchHeaderDataIfNeeded } from "../../actions/header/header";
import globalconfig from "../../globalconfig.js";
import AnchorLink from "../../components/common/AnchorLink";
import {
  addListenerForIzootoDismissal,
  fireUTMSourceGRX,
  setGRXNetworkSpeed,
  setGRXSectionDetails,
} from "../utils/app_util";
// import { fireGRXEvent } from "../../components/lib/analytics/src/ga";
// import TimesPointNudge from "../../components/common/TimesPoints/TimesPointModal/TimesPointNudge";
import { fetchMiniScheduleDataIfNeeded } from "../../modules/minischedule/actions/minischedule";
import { getMonth } from "../../components/common/TimesPoints/timespoints.util";
import { checkForRelatedApps } from "../html/services";
import { setGRXParameter } from "../../components/lib/analytics/src/ga";
const siteName = process.env.SITE;
const siteConfig = _getStaticConfig();
// const _electionConfig = electionConfig[process.env.SITE]

const TimesPoints = Loadable({
  loader: () => import("../../components/common/TimesPoints/TimesPoints"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("../../components/common/TimesPoints/TimesPoints"),
});
const DockPortal = Loadable({
  loader: () => import("../../modules/dockportal/dockportal"),
  LoadingComponent,
  webpackRequireWeakId: () => require.resolveWeak("../../modules/dockportal/dockportal"),
});
const Minitv = Loadable({
  loader: () => import("../../modules/videoplayer/minitv"),
  LoadingComponent,
});
const VideoPopup = Loadable({
  loader: () => import("../../modules/videoplayer/videoPopUp"),
  LoadingComponent,
});
const Cube = Loadable({
  loader: () => import("../../components/common/Cube"),
  LoadingComponent,
});
const TimesPointNudge = Loadable({
  loader: () => import("../../components/common/TimesPoints/TimesPointModal/TimesPointNudge"),
  LoadingComponent,
});
const PushNotificationCard = Loadable({
  loader: () => import("../../components/common/PushNotificationCard"),
  LoadingComponent,
});
//TODO: Change env file api endpoint to langdev
class App extends Component {
  constructor(props) {
    super(props);

    (this.state = {
      makePlayerVisible: false,
      makeVideoHeaderVisible: false,
      dockHeader: false,
      dockVideo: false,
      showPopup: false,
      scroll: false,
      loadWidgetsAfterScroll: false,
    }),
      (this.config = {
        isFrmapp: false,
        pollingInterval: 30000,
        isCubeVisible: false,
        thumbid: siteConfig.imageconfig.thumbid,
      });

    this.scrollRef = this.handleScroll.bind(this);
    this.loadWidgetScrollRef = this.loadWidgetAfterScroll.bind(this);

    if (_isCSR()) {
      document.addEventListener("playVideo", this.addVideoConfigData.bind(this), false);
      document.addEventListener("playVideoInPopup", this.playVideoInPopup.bind(this), false);
      document.addEventListener("addSliderData", this.addSliderData.bind(this), false);
      document.addEventListener("setVideoHeader", this.setVideoHeader.bind(this), false);
      document.addEventListener("showVideoPopup", this.openVideoPopup.bind(this), false);
      document.addEventListener("setPlayingVideoMsid", this.setVideoMsid.bind(this), false);
      document.addEventListener("setVideoPlayerType", this.setVideoType.bind(this), false);
      document.addEventListener("setMinitvVisibility", this.setMinitvVisibility.bind(this), false);
      document.addEventListener("setMinitvPlaying", this.setMinitvPlaying.bind(this), false);
      document.addEventListener("updateVideoConfig", this.updateVideoConfig.bind(this), false);
      window.addEventListener("scroll", this.scrollRef);
      window.addEventListener("scroll", this.loadWidgetScrollRef);
    }
  }

  userLoginCallback = data => {
    const { dispatch, authentication } = this.props;
    const userInfo = data.detail;
    const userAction = typeof window && window.loginButtonClicked ? "login" : "auto-login";
    if (userInfo && authentication && !authentication.userData) {
      // setGRXParameter("userid", userInfo.uid || "");
      if (_isCSR() && window.grx) {
        window.grx("userId", userInfo.uid || "");
      }
      dispatch(updateUserData(userInfo, userAction));
      delete window.loginButtonClicked;
    }
  };

  userLogoutCallback = data => {
    const { dispatch } = this.props;
    dispatch(updateUserData(null, "logout"));
    // setGRXParameter("userid", "");
    if (_isCSR() && window.grx) {
      window.grx("userId", null);
    }
  };

  componentDidMount() {
    const { dispatch, params, query, header, prebid, election, alaskaData, AdxMobileInterstial, location } = this.props;
    const geoLocationApi = siteConfig.weather.geoLocationApi;
    let _this = this;
    setGRXNetworkSpeed();
    fireUTMSourceGRX(location && location.query);
    addListenerForIzootoDismissal();

    if (_isCSR()) {
      window.addEventListener("beforeunload", function(event) {
        delete event["returnValue"];
        if (window && window.currentPlayerInst) {
          fireVideoPlayedGRXEvent();
        }
        return undefined;
      });
    }
    /**
     * CSR Critical CSS
     */
    addCriticalCss();
    // const mobileinterstialStatus = (AdxMobileInterstial && AdxMobileInterstial[siteName]) || "";
    // if (
    //   typeof isMobilePlatform === "function" &&
    //   isMobilePlatform() &&
    //   mobileinterstialStatus &&
    //   mobileinterstialStatus.status === "true"
    // ) {
    //   window.googletag = window.googletag || { cmd: [] };
    //   googletag.cmd.push(function() {
    //     var slot = googletag.defineOutOfPageSlot(
    //       "${siteConfig.ads.dfpads.interstitial}",
    //       googletag.enums.OutOfPageFormat.INTERSTITIAL,
    //     );
    //     if (slot) slot.addService(googletag.pubads());
    //     googletag.enableServices();
    //     googletag.display(slot);
    //   });
    // }

    // If geoinfo exists dont place extra call
    // issue- getGeoLocation overrides the geoinfo global variable
    if (!window.geoinfo) {
      loadJS(geoLocationApi, window.getGeoLocation); //Load Location Api for weather widget
    }
    checkForRelatedApps();
    _loadsvgsprite(header);
    //App.fetchData({dispatch, params, query});

    // this.cricketCubeDataPooling.bind(this);

    //need take  care ,it  is on for scorecard & Need to review
    dispatch(fetchMiniScorecardDataIfNeeded(params)).then(function(data) {
      // dispatch(fetchSCOREBOARD_IfNeeded());
      //off becasue no require
    });
    // this.cricketCubeDataPooling();
    //dispatch(fetchBUDGETRESULT_IfNeeded())
    // set homepage if user comes directly on inner pages
    if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      const data = {};
      data.docurl = docurl;
      data.doctitle = doctitle;
      data.newdoctitle = "Homepage";
      data.newdocurl = "/?back=1";
      // if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && window.geoinfo.notEU) {
      if (window.geoinfo && window.geoinfo.hasOwnProperty("isGDPRRegion") && !window.geoinfo.isGDPRRegion) {
        // history.replaceState(null, "Recommended Homepage", "/recommended.cms");
        // history.replaceState(null, "Homepage", "/?back=1");
        // history.pushState(null, doctitle, docurl);
        safelyReplaceHistory(data);
        // } else if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && !window.geoinfo.notEU) {
      } else if (window.geoinfo && window.geoinfo.hasOwnProperty("isGDPRRegion") && window.geoinfo.isGDPRRegion) {
        // history.replaceState(null, "Homepage", "/?back=1");
        // history.pushState(null, doctitle, docurl);
        safelyReplaceHistory(data);
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.isGDPRRegion) {
            // history.replaceState(null, "Homepage", "/?back=1");
            // history.pushState(null, doctitle, docurl);
            safelyReplaceHistory(data);
          } else {
            // history.replaceState(null, "Homepage", "/?back=1");
            // history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            // history.pushState(null, doctitle, docurl);
            safelyReplaceHistory(data);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      history.replaceState(null, "Homepage", "/");
      history.pushState(null, doctitle, docurl);
      if (window) {
        window.onbeforeunload = function(e) {
          window.history.back();
        };
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.isGDPRRegion) {
            // history.replaceState(null, "Homepage", "/?back=1");
            history.replaceState(null, "Homepage", "/");
            history.pushState(null, doctitle, docurl);
          } else {
            history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            history.pushState(null, doctitle, docurl);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      // history.replaceState(null, "Homepage", "/?back=1");
      history.replaceState(null, "Homepage", "/");
      history.pushState(null, doctitle, docurl);
    }

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      document.body.classList.add("webview");
    }

    let masterPlayer = document.querySelector(".master-player-container");

    if (masterPlayer) {
      masterPlayer.addEventListener("webkitfullscreenchange", function() {
        if (
          document.webkitIsFullScreen == false &&
          document.getElementsByClassName("spl_overlay spl_A")[0].classList.contains("spl_H")
        ) {
          _this.dockMasterPlayer({ detail: "dockVideo" });
        }
      });
    }

    get_Scroll_dir(); // Trigeer Custom ('scroll_up and scroll_down') event on scroll

    //Set FBN bottom 0 on scrolling down
    //   navigator.serviceWorker.ready.then((reg) => {
    //   const updatesChannel = new BroadcastChannel('topnews_api_updates');
    //   updatesChannel.addEventListener('message', async (event) => {
    //     console.log('Inside Update');
    //     const {cacheName, updatedUrl} = event.data.payload;

    //     // Do something with cacheName and updatedUrl.
    //     // For example, get the cached content and update
    //     // the content on the page.
    //     const cache = await caches.open(cacheName);
    //     const updatedResponse = await cache.match(updatedUrl);
    //     const updatedText = await updatedResponse.text();

    //   });
    // });

    // let objSwipe = pipSwipable(document.querySelector('.master-player-container'));
    // objSwipe.initialize();

    //Delay font files
    //Moved font loading to HTML.js for experimentation
    // this.loadfontFiles();

    window.addEventListener("scroll", this.showCube.bind(this));

    // TODO- Need to remove fetchtoplist call from appshell.
    let pageType = getPageType(this.props.router.location.pathname);
    /*  For cube and top story widget */
    if (pageType != "home") {
      dispatch(fetchTopListDataIfNeeded(params, query, this.props.router));
    }

    //-------- Lok sabha Election Cube
    // RotateCube();
    // if (_sessionStorage().get('CricketCube') == null) {
    //   _sessionStorage().set('CricketCube', true);
    // }

    // subscribe('ShowCubeEvent', function () {
    //   _this.showCube(_this);
    // });

    // ExitPollRotateCube();
    //data hit for CSR
    // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());

    //------- Data pulling of loksabha result
    // dataPooling("result", this.props);

    //------------End Loksabha Cube-----------------
    //set dfpAdConfig
    window.dfpAdConfig = _filterRecursiveALJSON(header, ["pwaconfig", "dfpAdConfig"], true);

    //For Prebid, setting app state(prebidconfig) in window object
    // _isCSR() && prebid
    //   ? prebidModule.setPreBidObj(prebid[process.env.SITE])
    //   : null;

    //Initiate the event to refresh ads slots on view
    //  process.env.SITE == "tlg" ? modifyAdsOnScrollView() : "";

    // Handling for Common ads, topatf and andbeyond
    //Ads part of app shel (not template wise)
    renderAppShellAds();

    // setting window object for ads config
    if (window && this.props.adconfig) {
      window.adconfig = this.props.adconfig;
    }

    if (typeof document) {
      document.addEventListener("user.status", this.userLoginCallback, false);
      document.addEventListener("userLogout", this.userLogoutCallback, false);
    }

    window.izootocallback = () => {
      window._izq = window._izq || [];
      window._izq.push(["init"]);
      window._izq.push([
        "registerSubscriptionCallback",
        function(obj) {
          var event = new CustomEvent("browserNotification", { detail: obj });
          document.dispatchEvent(event);
        },
      ]);
    };

    // To enable visibility in strip, if minitv was closed in last session
    if (_isCSR() && _sessionStorage().get("MINITV_CLOSED")) {
      const playerTypeEvent = new CustomEvent("setMinitvVisibility", {
        detail: { showMinitv: false },
      });
      document.dispatchEvent(playerTypeEvent);
    }
    //Below function will be called if any route change happens
    this.unlisten = browserHistory.listen(location => {
      let navEle = document.querySelector(".navbar");
      if (navEle) {
        navEle.className = "navbar";
      }
      globalconfig.isLandingPage = false;
    });
  }

  componentDidUpdate(prevProps, prevState) {
    this.showCube();
    const { router } = this.props;
    const url = router.location.pathname;
    if (checkTopAtf(dfpAdConfig, url)) {
      globalconfig.topatfEnabled = true;
    } else {
      globalconfig.topatfEnabled = false;
    }

    // Following code is for loading the highcharts library on the elections pages if it is not already loaded
    const highChartsPagesArray = ["elections"];
    if (
      url &&
      typeof url === "string" &&
      highChartsPagesArray.indexOf(getPageType(url)) > -1 &&
      !document.getElementById("highchart-script")
    ) {
      loadJS("https://code.highcharts.com/highcharts.js");
      loadJS("https://code.highcharts.com/modules/exporting.js");
      loadJS("https://code.highcharts.com/modules/export-data.js", undefined, "highchart-script");
    }
  }

  componentWillReceiveProps(nextProps) {
    //Reset thumb to tech on conditional basis
    //take default msid of thumb from config, which we set when our app.js initialised.
    isTechSite() && this.props.router.location.pathname.indexOf("/tech") > -1
      ? _setStaticConfig({
          imageconfig: { thumbid: siteConfig.imageconfig.thumbid },
        })
      : _setStaticConfig({ imageconfig: { thumbid: this.config.thumbid } });
  }

  loadWidgetAfterScroll() {
    if (window.scrollY > 1500 && !this.state.loadWidgetsAfterScroll) {
      this.setState(
        {
          loadWidgetsAfterScroll: true,
        },
        () => {
          this.unbindLoadingWidgetScroll();
        },
      );
    }
  }

  unbindLoadingWidgetScroll() {
    window.removeEventListener("scroll", this.loadWidgetScrollRef);
  }

  // cricketCubeDataPooling() {
  //   let _this = this;
  //   const { dispatch, params, query, appdata } = _this.props;

  //   _this.config.timer = setInterval(() => {
  //     dispatch(fetchSCOREBOARD_IfNeeded());
  //     dispatch(fetchMiniScorecardDataIfNeeded(params));
  //     //   if (_this.config.pollingClear) clearInterval(timer);
  //   }, window.pollingInterval || _this.config.pollingInterval);
  // }

  // _this.props.router.location.pathname

  setMinitvVisibility(event) {
    event && event.detail ? this.props.dispatch(setMinitvVisibility(event.detail.showMinitv)) : null;
  }

  setMinitvPlaying(event) {
    event && event.detail ? this.props.dispatch(setMinitvPlaying(event.detail.isMinitvPlaying)) : null;
  }

  setVideoHeader(event) {
    this.props.dispatch(setVideoHeader(event.detail));
  }

  setVideoMsid(event) {
    this.props.dispatch(setVideoMsid(event.detail));
  }

  setVideoType(event) {
    if (event.detail.videoType === "adStarted") {
      this.props.dispatch(updateAdState(true));
    } else if (event.detail.videoType === "adEnded") {
      this.props.dispatch(updateAdState(false));
    } else {
      this.props.dispatch(setVideoType(event.detail.videoType));
      this.props.dispatch(updateAdState(false));
    }
  }

  playVideoInPopup(event) {
    this.addVideoConfigData(event);
    this.openVideoPopup();
    this.props.dispatch(removeSliderData());
  }

  openVideoPopup() {
    if (document) {
      document.body.style.overflow = "hidden";
    }
    this.props.dispatch(showVideoPopup());
  }

  addVideoConfigData(event) {
    this.props.dispatch(removeVideoConfig());
    this.props.dispatch(addVideoConfig(event.detail.config));
  }

  addSliderData(event) {
    const videoList = event.detail.nextvideos;
    if (videoList && videoList.items) {
      this.props.dispatch(addSliderData(videoList.items));
    }
  }

  updateVideoConfig(event) {
    this.props.dispatch(updateVideoConfig(event.detail.config));
  }

  handleScroll() {
    const mobileinterstial = this.props && this.props.mobileinterstial;
    const vignette = this.props && this.props.vignette;
    // const izootoTxt = document.createElement("script");
    // izootoTxt.text = `window._izq = window._izq || []; window._izq.push(["init" ]);`;
    // const izooto = document.createElement("script");
    // izooto.src = siteConfig.izooto;
    // izooto.async = true;

    // loadJS(siteConfig.izooto, window.izootocallback, "izooto-jssdk", true);

    const gSignin = document.createElement("script");
    gSignin.src = "https://apis.google.com/js/platform.js";
    gSignin.defer = true;

    // const onetapSignin = document.createElement("script");
    // onetapSignin.src = "https://accounts.google.com/gsi/client";
    // onetapSignin.defer = true;

    // document.head.appendChild(onetapSignin);

    this.setState({ scroll: true });

    this.unbindScroll();

    // vignette and interstitial AD
    // interstitialDFP(mobileinterstial);
    // adsbygoogle(vignette);
  }
  unbindScroll() {
    window.removeEventListener("scroll", this.scrollRef);
  }

  loadfontFiles() {
    var newStyle = document.createElement("style");
    newStyle.appendChild(document.createTextNode(siteConfig.fontContent));
    document.head.appendChild(newStyle);
  }

  componentWillMount() {
    const { router, header } = this.props;
    globalconfig.router = router;
    globalconfig.pagetype = router ? getPageType(router.location.pathname) : "others";
    globalconfig.ad_pagetype = router ? getPageType(router.location.pathname, true) : "others";
    const dfpAdConfig = _filterRecursiveALJSON(header, ["pwaconfig", "dfpAdConfig"], true);
    let alaskaMastHead = filterAlaskaData(header.alaskaData, ["pwaconfig", "masthead", "mobile"], "label");
    const isTimesPointsPage = router && router.location.pathname.indexOf("timespoints.cms") > -1;

    if (checkTopAtf(dfpAdConfig, router.location.pathname)) {
      globalconfig.topatfEnabled = true;
    } else {
      globalconfig.topatfEnabled = false;
    }

    if (!isTimesPointsPage && alaskaMastHead && alaskaMastHead._status == "active" && alaskaMastHead._banner != "") {
      globalconfig.alaskaMastHeadEnabled = true;
    } else {
      globalconfig.alaskaMastHeadEnabled = false;
    }
    //Setting bannerdata for SSR/Amp
    try {
      if (header.alaskaData) {
        let HomePageWidgets = filterAlaskaData(header.alaskaData, ["pwaconfig", "HomePageWidgets"], "label");
        let keys = Object.keys(HomePageWidgets);
        if (keys) {
          for (var i = 0; i < 20; i++) {
            if (
              keys[i] &&
              HomePageWidgets[keys[i]] &&
              HomePageWidgets[keys[i]]._type &&
              HomePageWidgets[keys[i]]._type == "banner"
            ) {
              globalconfig.bannerData = HomePageWidgets[keys[i]];
              break;
            }
          }
        }
      }
    } catch (ex) {
      console.log("Error", ex);
    }
    // if (
    //   !globalconfig.atfheight ||
    //   !(
    //     adSizeConfig &&
    //     adSizeConfig[process.env.SITE] &&
    //     adSizeConfig[process.env.SITE].atfSizePercentage &&
    //     adSizeConfig[process.env.SITE].atfSizePercentage !== "false"
    //   )
    // ) {
    //   globalconfig.atfheight = adsize && adsize.atfheight;
    //   globalconfig.isExpando = true;
    //   delete globalconfig.newAtfAdSize;
    // }
  }

  componentWillUnmount() {
    //removing all event listeners
    document.removeEventListener("playVideoInPopup", this.playVideoInPopup.bind(this), false);
    document.removeEventListener("showVideoPopup", this.openVideoPopup.bind(this), false);
    document.removeEventListener("setVideoHeader", this.setVideoHeader.bind(this), false);
    document.removeEventListener("playVideo", this.addVideoConfigData.bind(this), false);
    document.removeEventListener("addSliderData", this.addSliderData.bind(this), false);
    document.removeEventListener("setPlayingVideoMsid", this.setVideoMsid.bind(this), false);
    document.removeEventListener("setVideoPlayerType", this.setVideoType.bind(this), false);
    document.removeEventListener("setMinitvVisibility", this.setMinitvVisibility(event), false);
    document.removeEventListener("setMinitvPlaying", this.setMinitvPlaying(event), false);
    document.removeEventListener("updateVideoConfig", this.updateVideoConfig.bind(this), false);
    this.unlisten && this.unlisten();
  }

  lazyLoad() {
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); //Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    }
  }

  checkGeoInfo(cubeProp) {
    let bool = true;
    if (_isCSR()) {
      let geoinfo = window.geoinfo,
        arrCodes = [],
        arrStates = [],
        arrCity = [];
      let countrycodeStr = cubeProp && cubeProp.countrycode && cubeProp.countrycode != "" ? cubeProp.countrycode : null;
      let stateStr = cubeProp && cubeProp.state && cubeProp.state != "" ? cubeProp.state : null;
      let cityStr = cubeProp && cubeProp.city && cubeProp.city != "" ? cubeProp.city : null;
      if (geoinfo && countrycodeStr) {
        arrCodes = countrycodeStr.split(",");
        if (arrCodes.indexOf(geoinfo.CountryCode) == -1) {
          bool = false;
        }
      } else if (geoinfo && stateStr) {
        arrStates = stateStr.split(",");
        if (arrStates.indexOf(geoinfo.region_code) == -1) {
          bool = false;
        }
      } else if (geoinfo && cityStr) {
        arrCity = cityStr.split(",");
        if (arrCity.indexOf(geoinfo.city) == -1) {
          bool = false;
        }
      }
    }
    return bool;
  }

  /**
   * This is used to show/hide cube on the basis of Section name included/excluded And
   * depending on FCap remain.
   * Cube on home page is hidden on load would be visible on scroll.
   * Fcap - if time lapsed after closing the cube is greater than the fcap time cube, then cube is shown.
   */
  showCube() {
    let pageType = getPageType(this.props.router.location.pathname);
    let { election } = this.props;
    let cubeProp = this.getCubeDetails(election);

    const hideCube = new CustomEvent("hideCube", {
      detail: {
        routeChange: true,
      },
    });
    const rotateCube = new CustomEvent("rotateCube", {
      detail: {
        cubeProp: cubeProp,
      },
    });

    if (_isCSR()) {
      let cube = document && document.querySelector(".Cube");

      if (cube) {
        // First check if section name included or not || is it home page
        if (
          !this.isValidSection(cubeProp) ||
          !this.isValidPlatform(cubeProp) ||
          this.isFcapRemain(cubeProp) ||
          !this.checkGeoInfo(cubeProp) ||
          (pageType == "home" && document.documentElement.scrollTop < 250 && !this.config.isCubeVisible)
        ) {
          // if (this.cube) {
          //   this.cube.hideCube(true);
          //   return;
          // }
          window.dispatchEvent(hideCube);
          return;
        }
        // elese check for fcap value
        else {
          // if (this.cube) {
          //   this.cube.RotateCube(cubeProp);
          // }
          window.dispatchEvent(rotateCube);
          this.config.isCubeVisible = true;
          cube.className = "Cube";
        }
      }
    }
  }

  /**
   * To manipulate the value given in config part for including and excluding section for Cube.
   * @param {*} arrSection
   */
  getSectionURL(arrSection) {
    let retrnSectnArr;
    retrnSectnArr = arrSection.map((item, key) => {
      if (item == "/") {
        return "/";
      } else {
        return "/" + item + "/";
      }
    });
    return retrnSectnArr;
  }

  /**
   * this is used to match values from included/excluded from the current route path
   * @param {*} pathname
   * @param {*} arrSection
   */
  checkIfExist(pathname, arrSection) {
    let bool = false;
    for (let item of arrSection) {
      if (item == "/") {
        bool = pathname == item ? true : false;
      } else if (pathname.indexOf(item) > -1 || item == "/all/") {
        bool = true;
      }
    }
    return bool;
  }

  /**
   * This method is solely responsible to return true if cube to be shown for that section.
   * @param {*} cubeProp
   */
  isValidSection(cubeProp) {
    let pathname = this.props.router.location.pathname;
    let arrInclSection, arrExclSection, bool;
    if (cubeProp.inclSection) {
      arrInclSection = cubeProp.inclSection.split(",");
      arrInclSection = this.getSectionURL(arrInclSection);
      if (this.checkIfExist(pathname, arrInclSection)) {
        bool = true;
      }
    }
    if (cubeProp.exclSection) {
      arrExclSection = cubeProp.exclSection.split(",");
      arrExclSection = this.getSectionURL(arrExclSection);
      if (this.checkIfExist(pathname, arrExclSection)) {
        bool = false;
      }
    }

    return bool;
  }

  isValidPlatform(cubeProp) {
    let bool = false;
    let allowedplatform = cubeProp.platform.toLowerCase();
    let platform = isMobilePlatform() ? "wap" : "web";

    if (allowedplatform.indexOf("all") > -1 || allowedplatform.indexOf(platform) > -1) {
      bool = true;
    }
    return bool;
  }

  /**
   * The below method is used to specifiy property on which cube is shown
   * @param {*} cubeProp
   */
  isValidProp(cubeProp) {
    // process.env.SITE
    let bool = false;

    if (cubeProp.sites.indexOf("all") > -1 || cubeProp.sites.indexOf(process.env.SITE) > -1) {
      bool = true;
    }
    return bool;
  }

  // checkForIND(miniScoreCard) {
  //   let bool = false;
  //   if (miniScoreCard.Calendar) {
  //     miniScoreCard.Calendar.forEach(function (item) {
  //       if (item.live == 1 && (item.teama == "India" || item.teamb == "India")) {
  //         bool = true;
  //       }
  //     })
  //   }
  //   return bool;
  // }

  // publishGA(data) {
  //   if (this.config && this.config.cubeGA) {
  //     publish('CriketCubeGA', data);
  //     console.log("this.config.cubeGA", this.config.cubeGA);
  //     this.config.cubeGA = false;
  //   }
  //   return true;
  // }

  checkIOS() {
    let isIOS = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) ? true : false;
    return isIOS;
  }

  /**
   * This is used to get the config values from canvas maker
   * @param {*} cubeDetail
   */
  getCubeDetails(cubeDetail = {}) {
    let cubeData,
      cubeProp = {},
      msInHr = 3600000;
    if (cubeDetail && cubeDetail.cube) {
      cubeData = cubeDetail.cube;
      cubeProp.autoClose = cubeData.autoclose && cubeData.autoclose != "" ? cubeData.autoclose : null;
      cubeProp.turntime = cubeData.turntime && cubeData.turntime != "" ? cubeData.turntime : 5000;
      cubeProp.inclSection = cubeData.inclSection && cubeData.inclSection != "" ? cubeData.inclSection : null;
      cubeProp.exclSection = cubeData.exclSection && cubeData.exclSection != "" ? cubeData.exclSection : null;
      cubeProp.countrycode = cubeData.countrycode && cubeData.countrycode != "" ? cubeData.countrycode : null;
      cubeProp.state = cubeData.state && cubeData.state != "" ? cubeData.state : null;
      cubeProp.city = cubeData.city && cubeData.city != "" ? cubeData.city : null;
      cubeProp.sites = cubeData.sites && cubeData.sites != "" ? cubeData.sites : null;
      cubeProp.platform = cubeData.platform && cubeData.platform != "" ? cubeData.platform : null;

      // default value for fcap is half-an-hr
      cubeProp.fcap = cubeData.fcap && cubeData.fcap != "" ? cubeData.fcap * msInHr : 0.5 * msInHr;
      // cubeProp.fcap = 60000;

      cubeProp.show = cubeData.show && cubeData.show != "" && cubeData.show == "true" ? true : false;
      cubeProp.testshow = cubeData.testshow && cubeData.testshow != "" && cubeData.testshow == "true" ? true : false;
    }

    return cubeProp;
  }

  /**
   * If time lapse after cube closed (auto/manually) is greater than fcap time, then cube is shown
   * @param {*} cubeProp
   */
  isFcapRemain(cubeProp) {
    let isfcapLeft = true;
    if (_isCSR()) {
      const prevTime = _sessionStorage().get("closeTime");
      let date = new Date();
      let curTime = date.getTime();
      if (Number(cubeProp.fcap) < curTime - Number(prevTime)) {
        isfcapLeft = false;
      }
    }
    return isfcapLeft;
  }

  render() {
    //election is used for widgetswiching
    //theme used for theming (articlelist state maped to app)
    const {
      header,
      bnews,
      minitv,
      config,
      scoreboard,
      mini_scorecard,
      electionresult,
      election,
      app,
      budget,
      theme,
      dispatch,
      params,
      router,
      location,
      videoplayer,
      pushNotificationcooki,
    } = this.props;
    let { query } = location;
    let isFrmApp = _isFrmApp(this.props.router);
    let { pathname } = router.location;
    if (this.props.routes && this.props.routes[2] && this.props.routes[2].longurl) {
      pathname = this.props.routes[2].longurl;
    }
    let pagetype = getPageType(pathname);

    const showDCWidget =
      //  Minitv was closed, then show the widget
      Boolean(_isCSR() && _sessionStorage().get("MINITV_CLOSED")) ||
      // Minitv is not fetching / not available or election is available, show widget
      Boolean(
        _isCSR() &&
          _sessionStorage().get("minitv") &&
          app &&
          !app.isFetching &&
          !(minitv && minitv.hl) &&
          !(election && election.cube && election.cube.show && election.cube.show == "true"),
      );
    // console.log("pwacaompaing", config);
    //Liveblog Widget config
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    let showLiveblogWidget =
      budgetLbConfig &&
      budgetLbConfig.status == "true" &&
      budgetLbConfig.msid != "" &&
      (config.pagetype == "home" || this.props.router.location.pathname.indexOf("/budget/articlelist/") > -1)
        ? true
        : false;

    //To set theme for events
    //Theme name will be used as class in parent wrapper, so that css rules can apply
    // Added check to make sure it only works on csr, and null for others.
    let locpath = this.props.router.location.pathname;
    let themename =
      theme &&
      theme.name &&
      (locpath.includes("articlelist") || locpath.includes("photolist") || locpath.includes("videolist"))
        ? theme.name
        : null;

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      this.config.isFrmapp = true;
    }

    const isTimesPointsPage = this.props.router.location.pathname.indexOf("timespoints.cms") > -1;

    //viral adda logo header
    let viralBranding =
      this.props.router.location.pathname.indexOf("/viral-adda/") > -1 ||
      this.props.router.location.pathname.indexOf("/viral-corner/") > -1
        ? true
        : false;

    //Education logo header
    let educationLogo =
      this.props.router.location.pathname.indexOf("/education/") > -1 && process.env.SITE == "vk" ? true : false;
    //lavalavk logo header
    let lavalavkLogo =
      this.props.router.location.pathname.indexOf("/lavalavk/") > -1 && process.env.SITE == "vk" ? true : false;
    //tech section layer
    let techLayer = "false";
    if (isGadgetPage(this.props.config)) {
      techLayer = true;
    } else if (
      (this.props.router.location.pathname.indexOf("/tech") > -1 || pathname.includes("/gadget-news/")) &&
      isTechSite()
    ) {
      techLayer = true;
      //Reset thumb to tech
      _setStaticConfig({
        imageconfig: { thumbid: siteConfig.imageconfig.thumbid },
      });
    } else {
      techLayer = false;
      //Reset thumb to default
      // FIX FOR NBT-12248 (Only occurs in SSR)
      // Once thumbid is reset above, need to replace it with defaultthumb to reset it.
      //FIXME: Add default thumb to non tech sites too for certainity
      _setStaticConfig({
        imageconfig: { thumbid: siteConfig.imageconfig.defaultthumb },
      });
    }

    let ad_pagetype = getPageType(pathname, true);
    if (ad_pagetype == "videolist" || ad_pagetype == "photolist") {
      ad_pagetype = "articlelist";
    }
    // let pagetype = getPageType(this.props.router.location.pathname);
    // let homeData =
    //   this.props &&
    //   this.props.home &&
    //   this.props.home.value &&
    //   this.props.home.value[0] &&
    //   this.props.home.value[0].items &&
    //   this.props.home.value[0].items.length > 0
    //     ? this.props.home.value[0].items
    //     : null;

    let cubeProp = {};
    if (election && election.cube && election.cube.show && election.cube.show == "true") {
      cubeProp = this.getCubeDetails(election);
      // config.resultLbl = election.result.counttext;
    }
    // let isFreqLeft = (cubeProp.fcap == "infinite")? true :
    // this.showCube();

    // let alaskaMastHead = filterAlaskaData(header.alaskaData, ["pwaconfig", "masthead", "mobile"], "label");
    let alaskaTPConfig = filterAlaskaData(header.alaskaData, ["pwaconfig", "TimesPoints"], "label");
    return (
      <div>
        {/* {this.state.showPopup ? <VideoPopup /> : ""} */}
        {/* Add themename as class in wrapper (Required to apply CSS rules for events) */}
        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" id="__SVG_DEFS_NODE__" style={{position: 'absolute', width: 0, height: 0}}>
          <linearGradient x1="49.8%" y1=".3%" x2="50.2%" y2="99.7%" id="gold_a">
              <stop stop-color="#F0B320" offset="0%"/>
              <stop stop-color="#DB9027" offset="99.9%"/>
          </linearGradient>
      
          <mask id="apna-bazar_a" fill="#fff">
              <path d="M.947 15.572C1.053 23.787 8 30.363 16.463 30.264c8.463.1 15.409-6.477 15.515-14.692C31.873 7.419 25.03.879 16.653.879l-.19.001-.191-.002C7.895.878 1.052 7.418.947 15.572z"/>
          </mask>
          <mask id="apna-bazar_b" fill="#fff">
              <path d="M10.97.908a3.402 3.402 0 01-1.028.275.726.726 0 01-.24-.038 8.202 8.202 0 00-2.52-.533 3.59 3.59 0 00-2.534 1.067.42.42 0 00-.06.564l2.316 3.14a16.007 16.007 0 00-2.759 2.369 12.974 12.974 0 00-2.188 3.152A9.773 9.773 0 00.955 15.18c.085 2.02 1.837 3.592 3.919 3.517h9.556c2.082.075 3.834-1.497 3.92-3.517a9.788 9.788 0 00-1.003-4.276 12.835 12.835 0 00-2.19-3.152 15.736 15.736 0 00-2.802-2.391L14.5 1.784a.415.415 0 00-.097-.536 2.659 2.659 0 00-1.802-.726 4.6 4.6 0 00-1.633.386zm.383.834a3.742 3.742 0 011.246-.318c.291 0 .575.088.813.25l-2.021 3.37H7.875L5.673 2.056a2.35 2.35 0 011.515-.545c.744.048 1.476.21 2.169.481.188.062.384.094.581.094a4.056 4.056 0 001.415-.344zM1.965 15.177A9.979 9.979 0 014.899 8.35a14.982 14.982 0 012.854-2.402h3.782a14.678 14.678 0 012.858 2.405 9.942 9.942 0 012.935 6.827c-.059 1.497-1.355 2.665-2.898 2.61v-.002H4.873l-.102.001c-1.5 0-2.742-1.146-2.807-2.612z"/>
          </mask>
          <mask id="apna-bazar_c" fill="#fff">
              <path d="M.347.339a.161.161 0 00-.054.124v.717c0 .047.02.092.056.122a.204.204 0 00.137.053h.873c.845 0 1.381.204 1.61.61H.486a.202.202 0 00-.139.05.16.16 0 00-.054.122v.551a.158.158 0 00.054.125.198.198 0 00.139.05h2.566a1.09 1.09 0 01-.616.673c-.403.17-.84.25-1.277.232H.486a.2.2 0 00-.136.05.158.158 0 00-.056.122v.686a.156.156 0 00.054.118c.77.734 1.767 1.76 2.993 3.08a.181.181 0 00.15.064h1.171a.176.176 0 00.175-.094.136.136 0 00-.024-.184c-1.17-1.287-2.09-2.25-2.76-2.89a3.247 3.247 0 001.66-.594c.415-.309.693-.76.776-1.261h1.008a.21.21 0 00.139-.05.161.161 0 00.054-.124v-.55a.162.162 0 00-.05-.128.2.2 0 00-.138-.049H4.474a1.786 1.786 0 00-.388-.776H5.49a.198.198 0 00.14-.048.162.162 0 00.052-.124v-.55a.157.157 0 00-.056-.127.2.2 0 00-.132-.05H.48a.201.201 0 00-.132.05z"/>
          </mask>
        </svg>
        <div id="svgsprite"></div>
        <div
          className={
            "m-scene" +
            (techLayer ? " tech" : "") +
            (viralBranding ? " viral-adda" : "") +
            (educationLogo ? ` ${process.env.SITE}-mini` : "") +
            (lavalavkLogo ? ` ${process.env.SITE}-lavalavk` : "") +
            ` ${themename || ""}`
          }
        >
          {/* By Election result widget */}
          {/* {this.props.router.location.pathname.indexOf("/byelectionresult/") < 0 &&
          typeof election != "undefined" &&
          election.result &&
          election.result.showwidget &&
          election.result.showwidget == "true" &&
          process.env.SITE == "mly" ? (
            <ByElection />
          ) : null} */}
          {/* Election loksabha result widget */}
          {/* {(typeof election != "undefined" &&
            election.result &&
            election.result.showwidget &&
            election.result.showwidget == "true" &&
            ((election.result.pages && election.result.pages.indexOf(pagetype) > -1) ||
              pathname.indexOf("/elections/") > -1)) ||
          pathname.indexOf("/articleshow_esi_test/") > -1 ? (
            <ErrorBoundary>
              <ElectionWidget data={electionresult} isFrmapp={this.config.isFrmapp} />
            </ErrorBoundary>
          ) : null} */}
          {/* <LokSabhaElectionResult isFrmapp={this.config.isFrmapp} pagetype={pagetype} />  */}
          {/* Election loksabha and assembly exitpoll widget */}
          {/* {typeof election != "undefined" &&
          election.exitpoll &&
          election.exitpoll.showwidget &&
          election.exitpoll.showwidget == "true" &&
          election.exitpoll.pages &&
          election.exitpoll.pages.indexOf(pagetype) > -1 ? (
            <ErrorBoundary>
              <ExitPollWidget data={this.props.exitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} />
            </ErrorBoundary>
          ) : null} */}
          {/* <LokSabhaExitPoll data={this.props.loksabhaexitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} /> */}
          {/* {
            <ErrorBoundary>
              <ConstituenciesWidget />
            </ErrorBoundary>
          } */}
          {/* {!this.config.isFrmapp ? (
            <ErrorBoundary>
              {!this.config.isFrmapp ? (
                <div
                  data-adtype="topatf"
                  className="ad1 topatf"
                  data-id="div-gpt-ad-1540206148700-007"
                  data-name={siteConfig.ads.dfpads.topatf}
                  //data-mlb="[[320, 100], [320, 50]]"
                  data-size="[[320, 100], [320, 50]]"
                  style={{height:"50px", background:"yellow"}}
                />
              ) : null}
            </ErrorBoundary>
          ) : null} */}

          {/* AlaskaMastHead banner - doesn't come for TP page */}
          {/* {!isTimesPointsPage && alaskaMastHead && alaskaMastHead._status == "active" && alaskaMastHead._banner != "" ? (
            <div className="alaskamasthead" data-exclude="amp">
              <AnchorLink href={alaskaMastHead._destinationurl}>
                <img src={alaskaMastHead._banner} />
              </AnchorLink>
            </div>
          ) : null} */}
          {/* AlaskaMastHead banner End */}

          {/* <div className="masthead-wrapper" data-exclude="amp">
            <a
              href={`${siteConfig.weburl}/coronavirus/trending/${
                siteConfig.coronaId
              }.cms?utm_source=masthead&utm_medium=referral&utm_campaign=coronavirus${isFrmApp ? "&frmapp=yes" : ""}`}
              target="_blank"
            >
              <img
                src="https://static.langimg.com/thumb/msid-74808425,width-320,height-80,resizemode-4/samayam-telugu.jpg"
                className="mast-head"
                width="100%"
              />
              <ErrorBoundary>
                <CoronaBanner />
              </ErrorBoundary>
              
              
            </a>
          </div> */}

          {typeof shouldTPRender == "function" && shouldTPRender() && (
            <ErrorBoundary>
              <TimesPoints
                loc={this.props.location}
                pageType={getPageType(this.props.location.pathname)}
                router={this.props.router}
              />
            </ErrorBoundary>
          )}

          <ErrorBoundary>
            <div id="parentContainer" className="animated">
              {this.props.children}
            </div>
          </ErrorBoundary>

          {_isCSR() ? (
            <ErrorBoundary>
              <PushNotificationCard key="pushnotify" cookieVal={pushNotificationcooki} />
            </ErrorBoundary>
          ) : null}
          {/* Configuration Cube for sports and Election --Oct-19 */}
          {(_isCSR() &&
            cubeProp.show &&
            // homeData &&
            this.isValidSection(cubeProp) &&
            this.isValidPlatform(cubeProp) &&
            this.isValidProp(cubeProp)) ||
          (_isCSR() &&
            cubeProp.testshow &&
            // homeData &&
            this.isValidProp(cubeProp) &&
            this.isValidSection(cubeProp) &&
            location.pathname.indexOf("/articleshow_esi_test/") > -1) ? (
            <ErrorBoundary>
              <div className="CubeParent">
                <Cube
                  dispatch={dispatch}
                  params={params}
                  query={query}
                  // homeData={homeData}
                  cubeProp={cubeProp}
                  electionresult={electionresult}
                  router={this.props.router}
                  // ref={inst => (this.cube = inst)}
                />
              </div>
            </ErrorBoundary>
          ) : null}
          {/*
              ((_isCSR() && cubeProp.show && this.isValidSection(cubeProp) && this.isValidProp(cubeProp)) || (_isCSR() && cubeProp.testshow && this.isValidSection(cubeProp) && this.isValidProp(cubeProp) && (location.pathname.indexOf('/articleshow_esi_test/') > -1))) ?
                <ErrorBoundary>
                  <Cube dispatch={dispatch} params={params} query={query}
                    cubeProp={cubeProp} election={election} electionresult={electionresult} ref={inst => this.cube = inst} ></Cube>
                </ErrorBoundary>
                : null
            */}

          {/* Cricket cube */}
          {/* {
            (_isCSR() && scoreboard && scoreboard.data && mini_scorecard && this.checkForIND(mini_scorecard) && _sessionStorage().get('CricketCube') != 'false') ?
              <ErrorBoundary>
                <CricketCube scorecard={mini_scorecard} scoredetail={scoreboard} />
              </ErrorBoundary>
              : null
          } */}

          {/* Election cube */}
          {/*
            // (_isCSR() && _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
            (_isCSR() && !this.config.isFrmapp && election.result && election.result.cube && election.result.cube.show == "true" &&
              _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
              <ErrorBoundary>
                <ElectionCube data={this.props.loksabhaelectionresult} election={this.props.election} />
              </ErrorBoundary>
              : null
          */}

          {/* Election EXITPOLL cube */}
          {/* Check for showcube condition , and check for on which pages to show pages , * is for all */}
          {/*
            (_isCSR() && _sessionStorage().get('ExitPollCube') && _sessionStorage().get('ExitPollCube') != 'false' && election.exitpoll && election.exitpoll.cube && election.exitpoll.cube.show && election.exitpoll.cube.show == "true"
              && election.exitpoll.cube.pages && (election.exitpoll.cube.pages.indexOf('-*-') > -1 || election.exitpoll.cube.pages.indexOf('-' + pagetype + '-') > -1)) ?
              <ErrorBoundary>
                <ExitPollElectionCube data={this.props.loksabhaexitpoll} />
              </ErrorBoundary>
              : null

          */}
          {/*  
          {!this.config.isFrmapp /*&& _isCSR() ? ? (
            <ErrorBoundary>
              {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
                <div
                  data-adtype="andbeyond"
                  className="ad1 andbeyond emptyAdBox"
                  data-id="div-gpt-ad-1558437895757-0"
                  data-name={siteConfig.ads.dfpads.andbeyond}
                  //data-mlb="[[1, 1]]"
                  data-size="[[1, 1]]"
                />
              ) : null}
              {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
                <div
                  data-adtype="canvasAd"
                  className="ad1 canvasAd emptyAdBox"
                  data-id="div-gpt-ad-1558437895757-0-canvasAd"
                  data-name={siteConfig.ads.dfpads.canvasAd}
                  //data-mlb="[[1, 1]]"
                  data-size="[[1, 1]]"
                />
              ) : null}
              {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
                <div
                  data-adtype="networkPGT"
                  className="ad1 oop emptyAdBox"
                  data-id="div-gpt-ad-1558437895757-0-networkPGT"
                  data-name={siteConfig.ads.dfpads.networkPGT}
                  data-mlb="[[1, 1]]"
                  data-size="[[1, 1]]"
                />
              ) : null}
            </ErrorBoundary>
          ) : null}
          */}

          {typeof shouldTPRender == "function" &&
          shouldTPRender() &&
          _isCSR() &&
          this.state.loadWidgetsAfterScroll &&
          showDCWidget &&
          (!_getCookie("show_nudge") || _getCookie("show_nudge") === "1") ? (
            <ErrorBoundary>
              <TimesPointNudge
                getMonth={getMonth}
                frequency={alaskaTPConfig && alaskaTPConfig._dcfrequency}
                router={this.props.router}
              />
            </ErrorBoundary>
          ) : null}
        </div>
        {_isCSR() && (
          <ErrorBoundary>
            <DockPortal />
          </ErrorBoundary>
        )}
        {_isCSR() && (
          <ErrorBoundary>
            <VideoPopup />
          </ErrorBoundary>
        )}

        {_isCSR() &&
        minitv &&
        this.state.scroll &&
        // router.location.pathname.indexOf("/tech") < 0 &&
        config.pagetype != "newsbrief" &&
        minitv.hl &&
        config.pagetype != "videolist" &&
        config.pagetype != "videoshow" &&
        !isFrmApp &&
        config.pagetype != "webviewpage" &&
        videoplayer.showMinitv &&
        !_sessionStorage().get("MINITV_CLOSED")
          ? ReactDOM.createPortal(<Minitv item={minitv} />, document.getElementById("outer_minitv_container"))
          : null}
      </div>
    );
  }
}

App.fetchData = function({ dispatch, params, query, history, router, state }) {
  let { pathname } = router.location;
  let pagetype = getPageType(pathname);

  const electionConfig =
    state && state.app && state.app.election
      ? getElectionConfigFromData({ electionConfig: state.app.election, pathname, pagetype, params })
      : {};
  if (electionConfig.showWidget) {
    if (electionConfig.shouldSetConfig) {
      const config = {};
      if (electionConfig.widgetType === "exitpoll") {
        config.exitpollurl = electionConfig.widgetUrl;
      }
      if (electionConfig.widgetType === "result") {
        config.resulturl = electionConfig.widgetUrl;
      }
      config.state = electionConfig.stateName;
      const electionConfigArr = [];
      electionConfigArr.push(config);
      dispatch(fetchElectionConfigSuccess(electionConfigArr));
    }
    if (electionConfig.widgetType === "exitpoll") {
      return dispatch(
        fetchExitPollIfNeeded(params, query, { url: electionConfig.widgetUrl, state: electionConfig.stateName }),
      )
        .then(() => {
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        })
        .catch(err => {
          console.log(`error--${err}`);
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        });
    } else if (electionConfig.widgetType === "result") {
      return dispatch(
        fetchResultsIfNeeded(params, query, { url: electionConfig.widgetUrl, state: electionConfig.stateName }),
      )
        .then(() => {
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        })
        .catch(err => {
          console.log(`error--${err}`);
          return dispatch(fetchHeaderDataIfNeeded()).then(() => {
            dispatch(fetchWidgetSequenceDataIfNeeded());
          });
        });
    }
  }

  return dispatch(fetchHeaderDataIfNeeded()).then(() => {
    dispatch(fetchWidgetSequenceDataIfNeeded());
  });
};

App.propTypes = {
  children: PropTypes.node,
};

function mapStateToProps(state) {
  return {
    ...state.app,
    config: state.config,
    app: state.app,
    routing: state.routing,
    scoreboard: state.scoreboard,
    home: state.home,
    videoplayer: state.videoplayer,
    //Mapping articlelist state object as theme to app (For events)
    theme:
      state.articlelist.value && state.articlelist.value[0] && state.articlelist.value[0].theme
        ? state.articlelist.value[0].theme
        : {},

    exitpoll: state.exitpoll,
    electionresult: state.electionresult,
    header: state.header,
    loksabhaelectionresult: state.loksabhaelectionresult,
    loksabhaexitpoll: state.loksabhaexitpoll,
    authentication: state.authentication,
  };
}

const renderAppShellAds = () => {
  //handle topatf and andbeyond
  if (document.querySelector(".topatf, .andbeyond, .innove")) {
    let listenerFun = function() {
      document.removeEventListener("gdpr.status", listenerFun, true);
      Ads_Module.render({});
    };
    document.addEventListener("gdpr.status", listenerFun, true);
  }
};

const safelyReplaceHistory = data => {
  // handling of gtm
  window.dataLayer.push({ genuineback: false });
  history.replaceState(null, data.newdoctitle, data.newdocurl);
  history.pushState(null, data.doctitle, data.docurl);
};
// DFP interstitial ad code
const interstitialDFP = mobileinterstial => {
  if (mobileinterstial && mobileinterstial[siteName] && mobileinterstial[siteName].status === "true") {
    window.googletag = window.googletag || { cmd: [] };
    googletag.cmd.push(function() {
      var slot = googletag.defineOutOfPageSlot(
        siteConfig.ads.dfpads.interstitial,
        googletag.enums.OutOfPageFormat.INTERSTITIAL,
      );
      if (slot) slot.addService(googletag.pubads());
      googletag.enableServices();
      googletag.display(slot);
    });
  }
};

// vignette ad dynamically added
const adsbygoogle = vignette => {
  if (vignette && vignette[siteName] && vignette[siteName].status === "true") {
    loadJS("https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", "", "", "", [
      {
        name: "data-ad-client",
        value: "ca-pub-1902173858658913",
      },
    ]);
  }
};

export default connect(mapStateToProps)(App);
