import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import Helmet from 'react-helmet';
import Header from "./../../components/common/Header";
import { publish } from "./../../utils/pubsub";
import SearchHeader from "./../../components/common/SearchHeader";
import Footer from "./../../components/common/Footer";
import LiveBlogStrip from "./../../modules/liveblogstrip/LiveBlogStrip";
// import ElectionExitPollWidget from './../../components/common/ElectionExitPollWidget';
//import NetworkMessage from './../../components/common/NetworkMessage';

// import ConstituenciesWidget from '../../campaign/election/common/component/ConstituenciesWidget';
// import ExitPollWidget from './../../campaign/election/electionwidget/container/indexExitPoll';
// import ElectionWidget from './../../campaign/election/electionwidget/container/index';
// import LokSabhaElectionResult, { dataPooling } from './../../campaign/election/loksabhaelectionresult/container/index';
// import LokSabhaExitPoll from './../../campaign/election/loksabhaexitpoll/container/index';
// import BudgetWidget from './../../campaign/budget/budgetwidget/container/index';
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import { fetchWidgetSequenceDataIfNeeded } from "../../actions/home/home";
import { fetchHeaderDataIfNeeded } from "./../../actions/header/header";
import { fetchBnewsDataIfNeeded } from "./../../actions/app/app";
import { fetchMiniTvDataIfNeeded } from "./../../actions/app/app";

import { fetchMiniScorecardDataIfNeeded } from "../../actions/app/app";

import BreakingNewsCard from "../../components/common/BreakingNewsCard";
import MiniTVCard from "../../components/common/MiniTVCard";
// import { ElectionCube, RotateCube } from '../../components/common/ElectionCube';
// import { CricketCube, RotateCube } from '../../components/common/CricketCube';
import { AnalyticsGA } from "./../../components/lib/analytics/index";
//import {checkIsAmpPage,removeClass, addClass, isOperaMini,checkIsATPListing} from './../../utils/util';

import { subscribe } from "../../utils/pubsub";

import { _getStaticConfig, _sessionStorage, getPageType, _isFrmApp, modifyAdsOnScrollView } from "../../utils/util";

import mainCss from "./../../public/main.css"; //DON'T REMOVE THIS - this is merging this into final css loader module in webpack
import { loadJS, get_Scroll_dir, _isCSR, _filterRecursiveALJSON } from "../../utils/util";
// import { pipSwipable } from '../../modules/videoplayer/utils';
// import { fetchSCOREBOARD_IfNeeded } from '../../campaign/cricket/scoreboard/action';
// import { fetchELECTIONRESULT_IfNeeded } from '../../campaign/election/electionresult/action';
// import { fetchLOKSABHAELECTIONRESULT_IfNeeded } from '../../campaign/election/loksabhaelectionresult/action';
// import { fetchExitPollIfNeeded } from '../../campaign/election/exitpoll/action';
// import { fetchLOKSABHAEXITPOLL_IfNeeded } from '../../campaign/election/loksabhaexitpoll/action';

// import { ExitPollElectionCube, ExitPollRotateCube } from '../../campaign/election/common/component/ExitPollElectionCube'
// import { electionConfig } from '../../campaign/election/utils/config';
// import Ads_Module from './../../components/lib/ads/index.js';
import Ads_Module, { prebidModule } from "./../../components/lib/ads/index";
// const _electionConfig = electionConfig[process.env.SITE]
const siteConfig = _getStaticConfig();

class App extends Component {
  constructor(props) {
    super(props);

    (this.state = {
      makePlayerVisible: false,
      makeVideoHeaderVisible: false,
      dockHeader: false,
      dockVideo: false,
    }),
      (this.config = {
        isFrmapp: false,
        pollingInterval: 30000,
      });
  }
  componentDidMount() {
    const { dispatch, params, query, header, prebid } = this.props;
    const geoLocationApi = siteConfig.weather.geoLocationApi;
    let _this = this;
    loadJS(geoLocationApi, window.getGeoLocation); //Load Location Api for weather widget
    //App.fetchData({dispatch, params, query});
    //check session storage for minitv & bnews
    if (!_sessionStorage().get("bnews")) dispatch(fetchBnewsDataIfNeeded());
    if (!_sessionStorage().get("minitv")) dispatch(fetchMiniTvDataIfNeeded());
    dispatch(fetchHeaderDataIfNeeded());

    // this.cricketCubeDataPooling.bind(this);

    // dispatch(fetchMiniScorecardDataIfNeeded(params)).then(function (data) {
    //   dispatch(fetchSCOREBOARD_IfNeeded());
    // });
    // this.cricketCubeDataPooling();

    //dispatch(fetchBUDGETRESULT_IfNeeded())
    // set homepage if user comes directly on inner pages
    if (
      process.env.SITE == "nbt" &&
      getPageType(window.location.href) != "home" &&
      !window.location.href.includes("frmapp=yes")
    ) {
      let docurl = window.location.href;
      let doctitle = document.title;
      if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && window.geoinfo.notEU) {
        history.replaceState(null, "Recommended Homepage", "/recommended.cms");
        history.pushState(null, doctitle, docurl);
      } else if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && !window.geoinfo.notEU) {
        history.replaceState(null, "Homepage", "/?back=1");
        history.pushState(null, doctitle, docurl);
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.notEU) {
            history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            history.pushState(null, doctitle, docurl);
          } else {
            history.replaceState(null, "Homepage", "/?back=1");
            history.pushState(null, doctitle, docurl);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      history.replaceState(null, "Homepage", "/?back=1");
      history.pushState(null, doctitle, docurl);
      if (window) {
        window.onbeforeunload = function(e) {
          window.history.back();
        };
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.notEU) {
            history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            history.pushState(null, doctitle, docurl);
          } else {
            history.replaceState(null, "Homepage", "/?back=1");
            history.pushState(null, doctitle, docurl);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      history.replaceState(null, "Homepage", "/?back=1");
      history.pushState(null, doctitle, docurl);
    }

    document.addEventListener("showVideoWithOverlay", this.showVideoWithOverlay.bind(this), false);
    document.addEventListener("showVideoWithoutOverlay", this.showVideoWithoutOverlay.bind(this), false);
    document.addEventListener("hideMasterPlayer", this.hideMasterPlayer.bind(this), false);
    document.addEventListener("dockMasterPlayer", this.dockMasterPlayer.bind(this), false);
    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      document.body.classList.add("webview");
    }

    let masterPlayer = document.querySelector(".master-player-container");

    if (masterPlayer) {
      masterPlayer.addEventListener("webkitfullscreenchange", function() {
        if (
          document.webkitIsFullScreen == false &&
          document.getElementsByClassName("spl_overlay spl_A")[0].classList.contains("spl_H")
        ) {
          _this.dockMasterPlayer({ detail: "dockVideo" });
        }
      });
    }

    get_Scroll_dir(); // Trigeer Custom ('scroll_up and scroll_down') event on scroll

    //Set FBN bottom 0 on scrolling down
    this.handle_Fbn_add();
    //   navigator.serviceWorker.ready.then((reg) => {
    //   const updatesChannel = new BroadcastChannel('topnews_api_updates');
    //   updatesChannel.addEventListener('message', async (event) => {
    //     console.log('Inside Update');
    //     const {cacheName, updatedUrl} = event.data.payload;

    //     // Do something with cacheName and updatedUrl.
    //     // For example, get the cached content and update
    //     // the content on the page.
    //     const cache = await caches.open(cacheName);
    //     const updatedResponse = await cache.match(updatedUrl);
    //     const updatedText = await updatedResponse.text();

    //   });
    // });

    // let objSwipe = pipSwipable(document.querySelector('.master-player-container'));
    // objSwipe.initialize();

    //Delay font files
    this.loadfontFiles();

    //-------- Lok sabha Election Cube
    // RotateCube();
    // if (_sessionStorage().get('CricketCube') == null) {
    //   _sessionStorage().set('CricketCube', true);
    // }

    // this.showCube();
    //window.addEventListener('scroll', this.showCube.bind(this));

    // subscribe('ShowCubeEvent', function () {
    //   _this.showCube(_this);
    // });

    // ExitPollRotateCube();
    //data hit for CSR
    // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());

    //------- Data pulling of loksabha result
    // dataPooling("result", this.props);

    //------------End Loksabha Cube-----------------
    //set dfpAdConfig
    window.dfpAdConfig = _filterRecursiveALJSON(header, ["pwaconfig", "dfpAdConfig"], true);

    //For Prebid, setting app state(prebidconfig) in window object
    // _isCSR() && prebid
    //   ? prebidModule.setPreBidObj(prebid[process.env.SITE])
    //   : null;

    //Initiate the event to refresh ads slots on view
    process.env.SITE == "tlg" ? modifyAdsOnScrollView() : "";

    // Handling for Common ads, topatf and andbeyond
    //Ads part of app shel (not template wise)
    renderAppShellAds();
  }

  componentWillReceiveProps(nextProps) {
    // RotateCube();
    // ExitPollRotateCube();
    //------ Rotate cricket cube
    // RotateCube(nextProps.mini_scorecard);
  }

  // cricketCubeDataPooling() {
  //   let _this = this;
  //   const { dispatch, params, query, appdata } = _this.props;

  //   _this.config.timer = setInterval(() => {
  //     dispatch(fetchSCOREBOARD_IfNeeded());
  //     dispatch(fetchMiniScorecardDataIfNeeded(params));
  //     //   if (_this.config.pollingClear) clearInterval(timer);
  //   }, window.pollingInterval || _this.config.pollingInterval);
  // }

  loadfontFiles() {
    var newStyle = document.createElement("style");
    newStyle.appendChild(document.createTextNode(siteConfig.fontContent));
    document.head.appendChild(newStyle);
  }

  showVideoWithOverlay() {
    this.setState(() => {
      return { makePlayerVisible: true, makeVideoHeaderVisible: true };
    });
  }

  showVideoWithoutOverlay() {
    this.setState(() => {
      return { makePlayerVisible: true, dockHeader: true, dockVideo: false };
    });
  }

  hideMasterPlayer(event) {
    let masterPlayerContainer = document.querySelector(".master-player-container");
    //if (event && event.detail == "vidComplete" && !this.state.dockVideo) return;

    // if(event == "closeVideo") {
    //   window.history.go(-1);
    // }

    //  if (event == "closeMiniTV" || ) {
    if (typeof S != "undefined" && typeof S.pauseAllPlayers == "function" && typeof S.destroy == "function") {
      try {
        if (
          typeof window.SPL != "undefined" &&
          typeof window.SPL.pauseAllPlayers == "function" &&
          typeof window.SPL.destroy == "function"
        ) {
          S.pauseAllPlayers();
          if (window.currentPlayerInst) {
            S.destroy(window.currentPlayerInst, () => {
              console.log("Player destroyed!!!");
            });
          }
        }
      } catch (e) {
        console.log("Logging exception ", e);
      }
    }
    let videoUrl = document.querySelector(".master-player-container")
      ? masterPlayerContainer.getAttribute("data-videourl")
      : "";
    if (event == "closeDock") {
      AnalyticsGA.event({
        category: "PIP Close",
        action: "Button",
        label: videoUrl,
      });
    } else if (typeof event != "undefined" && event.detail && event.detail == "vidComplete") {
      AnalyticsGA.event({
        category: "PIP Close",
        action: event.detail,
        label: videoUrl,
      });
    }

    this.setState(() => {
      return {
        makePlayerVisible: false,
        makeVideoHeaderVisible: false,
        dockVideo: false,
      };
    });

    document.body.classList.remove("videoplaying");
    masterPlayerContainer.classList.remove("enable_dock");
    masterPlayerContainer.classList.remove("enable-video-show");
    masterPlayerContainer.style = "";
    document.body.classList.remove("enable_dock");
    document.body.classList.remove("enable-video-show");
  }

  openvideoshow() {
    let masterPlayerContainer = document.querySelector(".master-player-container");

    masterPlayerContainer.classList.remove("enable_dock");
    masterPlayerContainer.classList.add("enable-video-show");
    document.body.classList.remove("enable_dock");
    document.body.classList.add("enable-video-show");
    document.querySelector("#masterVideoPlayer").style.height = masterPlayerContainer.getAttribute("data-height");
    masterPlayerContainer.style.height = masterPlayerContainer.getAttribute("data-height");
    if (
      masterPlayerContainer.getAttribute("data-videourl") &&
      masterPlayerContainer.getAttribute("data-videourl") != "#"
    ) {
      this.props.router.push(masterPlayerContainer.getAttribute("data-videourl"));
    }
    this.setState(() => {
      return { makePlayerVisible: true, dockHeader: true, dockVideo: false };
    });
    let videoUrl = document.querySelector(".master-player-container")
      ? masterPlayerContainer.getAttribute("data-videourl")
      : "";
    AnalyticsGA.event({
      category: "PIP UnDock",
      action: "Click",
      label: videoUrl,
    });
  }

  dockMasterPlayer(event) {
    // if(event.detail == "dockVideo"){
    //   window.history.go(-1);
    // }

    let masterPlayerContainer = document.querySelector(".master-player-container");
    if (!this.state.dockVideo && masterPlayerContainer && document.querySelector("#masterVideoPlayer")) {
      masterPlayerContainer.classList.remove("enable-video-show");
      masterPlayerContainer.classList.add("enable_dock");
      document.body.classList.remove("enable-video-show");
      document.body.classList.add("enable_dock");
      document.querySelector("#masterVideoPlayer").style.height = "84px";
      masterPlayerContainer.style.height = "84px";
      this.setState(() => {
        return { makePlayerVisible: true, dockHeader: false, dockVideo: true };
      });
      event;
      {
        AnalyticsGA.event({
          category: "PIP Dock",
          action: event.detail,
          label: masterPlayerContainer.getAttribute("data-videourl"),
        });
        // AnalyticsGA.GTM({ category: 'PIP Dock', action: event.detail, label: masterPlayerContainer.getAttribute('data-videourl') , event : 'tvc_pipdock'})
      }
    }
    // else{
    //   this.setState(() => {
    //     return { makePlayerVisible: true, dockHeader: true, dockVideo: false};
    //   });
    // }
  }

  videoplayeroptions(option) {
    if (option == "dock") {
      this.dockMasterPlayer({ detail: "dockVideo" });
    } else if (option == "close") {
      this.hideMasterPlayer("closeVideo");
    }
    if (this.props.config && this.props.config.pagetype != "videoshow") {
      window.history.go(-1);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("showVideoWithOverlay", this.showVideoWithOverlay.bind(this), false);
    document.removeEventListener("showVideoWithoutOverlay", this.showVideoWithoutOverlay.bind(this), false);
    document.removeEventListener("hideMasterPlayer", this.hideMasterPlayer.bind(this), false);
    document.removeEventListener("dockMasterPlayer", this.dockMasterPlayer(event), false);
  }

  handle_Fbn_add() {
    let ele = document.querySelector(".fbn");
    ele
      ? window.addEventListener("scroll_up", function() {
          ele.style.bottom == "-60px" ? (ele.style.bottom = "0") : "";
        })
      : "";
  }

  lazyLoad() {
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); //Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    }
  }

  // showCube() {
  //   let pageType = getPageType(this.props.router.location.pathname);
  //   let cube = document.querySelector('.election_cube');
  //   if (_sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false') {
  //     if (cube) {
  //       if (!(this.checkIOS()) && (pageType == 'home') && (document.documentElement.scrollTop < 750)) {
  //         //cube.className = 'election_cube hide';
  //         return true;
  //       }
  //       cube.className = 'election_cube';

  //     }
  //   }

  // }

  checkForIND(miniScoreCard) {
    let bool = false;
    if (miniScoreCard.Calendar) {
      miniScoreCard.Calendar.forEach(function(item) {
        if (item.live == 1 && (item.teama == "India" || item.teamb == "India")) {
          bool = true;
        }
      });
    }
    return bool;
  }

  // publishGA(data) {
  //   if (this.config && this.config.cubeGA) {
  //     publish('CriketCubeGA', data);
  //     console.log("this.config.cubeGA", this.config.cubeGA);
  //     this.config.cubeGA = false;
  //   }
  //   return true;
  // }

  checkIOS() {
    let isIOS = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) ? true : false;
    return isIOS;
  }

  render() {
    //election is used for widgetswiching
    const {
      header,
      bnews,
      minitv,
      config,
      router,
      scoreboard,
      mini_scorecard,
      electionresult,
      election,
      app,
      budget,
    } = this.props;
    let { query } = this.props.location;
    let isFrmApp = _isFrmApp(this.props.router);

    //Liveblog Widget config
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    let showLiveblogWidget =
      budgetLbConfig &&
      budgetLbConfig.status == "true" &&
      budgetLbConfig.msid != "" &&
      (config.pagetype == "home" || this.props.router.location.pathname.indexOf("/budget/articlelist/") > -1)
        ? true
        : false;

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      this.config.isFrmapp = true;
    }
    //viral adda logo header
    let viralBranding =
      this.props.router.location.pathname.indexOf("/viral-adda/") > -1 ||
      this.props.router.location.pathname.indexOf("/viral-corner/") > -1
        ? true
        : false;
    //Education logo header
    let educationLogo =
      this.props.router.location.pathname.indexOf("/education/") > -1 && process.env.SITE == "vk" ? true : false;
    //lavalavk logo header
    let lavalavkLogo =
      this.props.router.location.pathname.indexOf("/lavalavk/") > -1 && process.env.SITE == "vk" ? true : false;
    //tech section layer
    let techLayer =
      this.props.router.location.pathname.indexOf("/tech/") > -1 && process.env.SITE == "nbt" ? true : false;

    let pagetype = getPageType(this.props.router.location.pathname);
    return (
      <div>
        <div
          className={
            "m-scene" +
            (techLayer ? " tech" : "") +
            (viralBranding ? " viral-adda" : "") +
            (educationLogo ? ` ${process.env.SITE}-mini` : "") +
            (lavalavkLogo ? ` ${process.env.SITE}-lavalavk` : "")
          }
        >
          {!this.config.isFrmapp ? (
            <ErrorBoundary>
              {/*handle topatf above header*/}
              <div
                data-adtype="topatf"
                className="ad1 topatf emptyAdBox"
                data-id="div-gpt-ad-1540206148700-007"
                data-name={siteConfig.ads.dfpads.topatf}
                //data-mlb="[[320, 100], [320, 50]]"
                data-size="[[320, 100], [320, 50]]"
              />
              {config.searchHeader ? (
                <SearchHeader />
              ) : (
                <Header
                  navigations={header.alaskaData}
                  viralBranding={viralBranding}
                  {...this.state}
                  {...this.props}
                  query={query}
                />
              )}
            </ErrorBoundary>
          ) : null}
          {/* Budget result widget */}
          {/*
            (config.pagetype == 'home') ?
              <ErrorBoundary>
                <BudgetWidget navigations={header} />
              </ErrorBoundary>
              :
              null

          */}

          {/* Election loksabha result widget */}
          {/*
            (election.result && election.result.showwidget && election.result.showwidget == "true" && election.result.pages && election.result.pages.indexOf(pagetype) > -1) ?
              <ErrorBoundary>
                <LokSabhaElectionResult isFrmapp={this.config.isFrmapp} pagetype={pagetype} />
                <ElectionWidget data={electionresult} isFrmapp={this.config.isFrmapp} />
              </ErrorBoundary>
              : null
          */}
          {/* Election loksabha and assembly exitpoll widget */}
          {/*
            (election.exitpoll && election.exitpoll.showwidget && election.exitpoll.showwidget == "true" && election.exitpoll.pages && election.exitpoll.pages.indexOf(pagetype) > -1) ?
              <ErrorBoundary>
                <LokSabhaExitPoll data={this.props.loksabhaexitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} />
                <ExitPollWidget data={this.props.exitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} />
              </ErrorBoundary>
              : null
          */}
          {/* {
            <ErrorBoundary>
              <ConstituenciesWidget />
            </ErrorBoundary>
          } */}
          {/* Master Video player wrapper */}
          <div
            className={
              this.state.makeVideoHeaderVisible ? "master-player-container overlay" : "master-player-container"
            }
          >
            <div className="mp-modal" hidden={this.state.makeVideoHeaderVisible ? undefined : true} />
            <div className="mp-header" hidden={this.state.makeVideoHeaderVisible ? undefined : true}>
              <span className="close-btn close_icon" onClick={this.hideMasterPlayer.bind(this, "closeMiniTV")} />
            </div>
            <div
              id="masterVideoPlayer"
              onClick={this.state.dockVideo ? this.openvideoshow.bind(this) : null}
              className="player-api player-size"
              hidden={this.state.makePlayerVisible ? undefined : true}
            />
            <div className="player-options">
              <span className="close_icon" onClick={this.videoplayeroptions.bind(this, "close")} />
              <span className="dock_icon" onClick={this.videoplayeroptions.bind(this, "dock")}>
                <span />
              </span>
            </div>
            {this.state.dockVideo ? (
              <span className="close_icon" onClick={this.hideMasterPlayer.bind(this, "closeDock")} />
            ) : null}
          </div>

          {/* Breaking News */}
          {bnews &&
          router.location.pathname.indexOf("/tech") < 0 &&
          config.pagetype != "newsbrief" &&
          config.pagetype != "videolist" &&
          config.pagetype != "videoshow" &&
          !isFrmApp &&
          config.pagetype != "webviewpage" ? (
            <ErrorBoundary>
              <BreakingNewsCard bnews={bnews} />
            </ErrorBoundary>
          ) : null}

          {/* MINITV */}
          {minitv &&
          router.location.pathname.indexOf("/tech") < 0 &&
          config.pagetype != "newsbrief" &&
          minitv.hl &&
          config.pagetype != "videolist" &&
          config.pagetype != "videoshow" &&
          !isFrmApp &&
          config.pagetype != "webviewpage" ? (
            <ErrorBoundary>
              <MiniTVCard minitv={minitv} />
            </ErrorBoundary>
          ) : null}

          {showLiveblogWidget ? (
            <LiveBlogStrip lbconfig={budgetLbConfig} msid={budgetLbConfig.msid} widgetType={"multipleposts"} />
          ) : null}

          <div id="parentContainer" className="animated">
            {this.props.children}
          </div>

          {/* Cricket cube */}
          {/* {
            (_isCSR() && scoreboard && scoreboard.data && mini_scorecard && this.checkForIND(mini_scorecard) && _sessionStorage().get('CricketCube') != 'false') ?
              <ErrorBoundary>
                <CricketCube scorecard={mini_scorecard} scoredetail={scoreboard} />
              </ErrorBoundary>
              : null
          } */}

          {/* Election cube */}
          {/*
            // (_isCSR() && _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
            (_isCSR() && !this.config.isFrmapp && election.result && election.result.cube && election.result.cube.show == "true" &&
              _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
              <ErrorBoundary>
                <ElectionCube data={this.props.loksabhaelectionresult} election={this.props.election} />
              </ErrorBoundary>
              : null
          */}

          {/* Election EXITPOLL cube */}
          {/* Check for showcube condition , and check for on which pages to show pages , * is for all */}
          {/*
            (_isCSR() && _sessionStorage().get('ExitPollCube') && _sessionStorage().get('ExitPollCube') != 'false' && election.exitpoll && election.exitpoll.cube && election.exitpoll.cube.show && election.exitpoll.cube.show == "true"
              && election.exitpoll.cube.pages && (election.exitpoll.cube.pages.indexOf('-*-') > -1 || election.exitpoll.cube.pages.indexOf('-' + pagetype + '-') > -1)) ?
              <ErrorBoundary>
                <ExitPollElectionCube data={this.props.loksabhaexitpoll} />
              </ErrorBoundary>
              : null

          */}

          {!this.config.isFrmapp /*&& _isCSR() ?*/ ? (
            <ErrorBoundary>
              <Footer
                navigations={header} /*As we are using _FilterAlJson inside Footer to get alskaData */
                offfoot={router.location.pathname.indexOf("/tech") ? 1 : 0}
              />
              {process.env.SITE != "tlg" && queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
                <div
                  data-adtype="andbeyond"
                  className="ad1 andbeyond emptyAdBox"
                  data-id="div-gpt-ad-1558437895757-0"
                  data-name={siteConfig.ads.dfpads.andbeyond}
                  //data-mlb="[[1, 1]]"
                  data-size="[[1, 1]]"
                />
              ) : null}
            </ErrorBoundary>
          ) : null}
        </div>
        {!this.config.isFrmapp ? (
          <div className="landscape-mode" data-exclude="amp">
            <div className="center-align">
              <span className="img_landscape" />
              <div className="txt">
                <h3>{siteConfig.locale.orientation_heading}</h3>
                {siteConfig.locale.orientation_text}
              </div>
            </div>
          </div>
        ) : null}

        {/* {
          process.env.SITE != 'tlg' && (!(query.utm_medium) || query.utm_medium && query.utm_medium.toLowerCase() != 'affiliate') ?
            <div className="camapaignads"
              dangerouslySetInnerHTML={{ __html: `<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script><script>if(googletag==null || googletag==undefined){var googletag = googletag || {};}googletag.cmd = googletag.cmd || [];</script><script>googletag.cmd.push(function() {googletag.defineSlot('${siteConfig.ads.dfpads.andbeyond}', [1, 1], 'div-gpt-ad-1558437895757-0').addService(googletag.pubads());googletag.pubads().setTargeting('Hyp1', '').setTargeting('_ref', '').setTargeting('SCN', '').setTargeting('SubSCN', '').setTargeting('LastSubSCN', '');googletag.pubads().enableSingleRequest();googletag.enableServices();});</script><div id='div-gpt-ad-1558437895757-0'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1558437895757-0'); });</script></div>` }}
            />
            : null
        } */}
      </div>
    );
  }
}

App.fetchData = function({ dispatch, params, query, history, router }) {
  let { pathname } = router.location;
  let pagetype = getPageType(pathname);

  // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
  /*
  if (pagetype == 'home' || pagetype == 'liveblog') {
    //dispatch(fetchExitPollIfNeeded());
    //dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
    params && (params.value = _electionConfig.feedlanguage);
    dispatch(fetchELECTIONRESULT_IfNeeded());
    // dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "result"));
  }
  */
  return dispatch(fetchHeaderDataIfNeeded()).then(() => {
    dispatch(fetchWidgetSequenceDataIfNeeded());
  });
};

App.propTypes = {
  children: PropTypes.node,
};

function mapStateToProps(state) {
  return {
    ...state.app,
    header: state.header,
    config: state.config,
    routing: state.routing,
    scoreboard: state.scoreboard,
    home: state.home,

    // exitpoll: state.exitpoll,
    electionresult: state.electionresult,

    // loksabhaelectionresult: state.loksabhaelectionresult,
    // loksabhaexitpoll: state.loksabhaexitpoll,
  };
}

const renderAppShellAds = () => {
  //handle topatf and andbeyond
  if (document.querySelector(".topatf, .andbeyond")) {
    let listenerFun = function() {
      document.removeEventListener("gdpr.status", listenerFun, true);
      Ads_Module.render({});
    };
    document.addEventListener("gdpr.status", listenerFun, true);
  }
};

export default connect(mapStateToProps)(App);
