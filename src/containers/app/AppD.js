import React, { Component, Suspense, lazy } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { browserHistory } from "react-router";
import Loadable from "react-loadable";
require("es6-promise").polyfill();
import ErrorBoundary from "../../components/lib/errorboundery/ErrorBoundary";
import { fetchTopListDataIfNeeded } from "../../actions/home/home";
import { fetchWeatherDataIfNeeded } from "../../actions/app/app";
import { fireVideoPlayedGRXEvent } from "../../modules/videoplayer/slike";
// import { ElectionCube, RotateCube } from '../../components/common/ElectionCube';
// import { CricketCube, RotateCube } from '../../components/common/CricketCube';
// import Cube from "../../components/common/Cube";
import DockPortal from "../../modules/dockportal/dockportal";
import VideoPopup from "../../modules/videoplayer/videoPopUp";

import AnchorLink from "../../components/common/AnchorLink";
import TopStoryPopup from "../../components/desktop/TopStoryPopup";

import {
  _getStaticConfig,
  _sessionStorage,
  getPageType,
  _isFrmApp,
  _setStaticConfig,
  _setCookie,
  _getCookie,
  isTechSite,
  filterAlaskaData,
  shouldTPRender,
  isMobilePlatform,
  _loadsvgsprite,
  checkTopAtf,
  addCriticalCss,
} from "../../utils/util";

import mainCss from "./../../public/main.css"; //DON'T REMOVE THIS - this is merging this into final css loader module in webpack

import { loadJS, _isCSR, _filterRecursiveALJSON, LoadingComponent } from "../../utils/util";
import { fetchMiniScorecardDataIfNeeded } from "../../modules/scorecard/actions/scorecard";
import {
  showVideoPopup,
  addSliderData,
  addVideoConfig,
  removeVideoConfig,
  setVideoHeader,
  setVideoMsid,
  setVideoType,
  setMinitvVisibility,
  setMinitvPlaying,
  updateVideoConfig,
  updateAdState,
} from "../../actions/videoplayer/videoplayer";

import Ads_Module from "../../components/lib/ads/index";
import ByElection from "../../modules/byelection/ByElection";

import globalconfig from "../../globalconfig.js";
import TimesPoints from "../../components/common/TimesPoints/TimesPoints";
import TimesPointModal from "../../components/common/TimesPoints/TimesPointModal/TimesPointModal";
import { updateUserData } from "../../actions/authentication/authentication";
import { getMonth } from "../../components/common/TimesPoints/timespoints.util";
import { fireGRXEvent, setGRXParameter } from "../../components/lib/analytics/src/ga";
import {
  fireUTMSourceGRX,
  addListenerForIzootoDismissal,
  setGRXNetworkSpeed,
  setGRXSectionDetails,
} from "../utils/app_util";
import PushNotificationCard from "../../components/common/PushNotificationCard";
// import { fetchMiniScheduleDataIfNeeded } from "../../modules/minischedule/actions/minischedule";

const siteConfig = _getStaticConfig();

const siteName = process.env.SITE || "";

const Cube = Loadable({
  loader: () => import("../../components/common/Cube"),
  LoadingComponent,
});

const TimesPointNudge = Loadable({
  loader: () => import("../../components/common/TimesPoints/TimesPointModal/TimesPointNudge"),
  LoadingComponent,
});

// const PushNotificationCard = Loadable({
//   loader: () => import("../../components/common/PushNotificationCard"),
//   LoadingComponent,
// });

class App extends Component {
  constructor(props) {
    super(props);

    (this.state = {
      makePlayerVisible: false,
      makeVideoHeaderVisible: false,
      scroll: false,
      dockHeader: false,
      dockVideo: false,
    }),
      (this.config = {
        isFrmapp: false,
        pollingInterval: 30000,
        isCubeVisible: false,
        thumbid: siteConfig.imageconfig.thumbid,
        // thumbid need to be there for default placeholder
      });

    this.minitvDragConfig = {
      active: false,
      xOffset: 0,
      yOffset: 0,
      currentX: null,
      currentY: null,
      initialX: null,
      initialY: null,
    };
    this.scrollRef = this.handleScroll.bind(this);
    if (_isCSR()) {
      document.addEventListener("playVideo", this.addVideoConfigData.bind(this), false);
      document.addEventListener("playVideoInPopup", this.playVideoInPopup.bind(this), false);
      document.addEventListener("addSliderData", this.addSliderData.bind(this), false);
      document.addEventListener("setVideoHeader", this.setVideoHeader.bind(this), false);
      document.addEventListener("showVideoPopup", this.openVideoPopup.bind(this), false);
      document.addEventListener("setPlayingVideoMsid", this.setVideoMsid.bind(this), false);
      document.addEventListener("setVideoPlayerType", this.setVideoType.bind(this), false);
      document.addEventListener("setMinitvVisibility", this.setMinitvVisibility.bind(this), false);
      document.addEventListener("setMinitvPlaying", this.setMinitvPlaying.bind(this), false);
      document.addEventListener("updateVideoConfig", this.updateVideoConfig.bind(this), false);
      window.addEventListener("scroll", this.scrollRef);
    }
  }

  componentDidMount() {
    const { dispatch, params, query, header, prebid, election, interstial, location } = this.props;
    const geoLocationApi = siteConfig.weather.geoLocationApi;
    let _this = this;
    setGRXNetworkSpeed();
    fireUTMSourceGRX(location && location.query);
    addListenerForIzootoDismissal();

    if (_isCSR()) {
      window.addEventListener("beforeunload", function(event) {
        delete event["returnValue"];
        event.preventDefault();
        if (window && window.currentPlayerInst) {
          fireVideoPlayedGRXEvent();
        }
        return undefined;
      });
    }

    /**
     * CSR Critical CSS
     */
    addCriticalCss();

    // alaska banner save in window
    /*try {
      let HomePageWidgets = filterAlaskaData(header.alaskaData, ["pwaconfig", "HomePageWidgets"], "label");
      let bannerObj = "";
      let keys = Object.keys(HomePageWidgets);
      for (var i = 0; i < 20; i++) {
        if (HomePageWidgets[keys[i]]._type == "banner") {
          bannerObj = HomePageWidgets[keys[i]];
          break;
        }
      }
      window.bannerData = [];
      if (typeof window == "object") {
        window.bannerData = bannerObj;
      }
    } catch (ex) {}*/
    // this.cricketCubeDataPooling.bind(this);

    //need take  care ,it  is on for scorecard & Need to review
    dispatch(fetchMiniScorecardDataIfNeeded(params)).then(function(data) {
      // dispatch(fetchSCOREBOARD_IfNeeded());
      //off becasue no require
    });
    const newParams = {};
    newParams.cityName = siteConfig.weather.defaultCity;
    dispatch(fetchWeatherDataIfNeeded(newParams));

    // this.cricketCubeDataPooling();

    //dispatch(fetchBUDGETRESULT_IfNeeded())
    // set homepage if user comes directly on inner pages
    if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      const data = {};
      data.docurl = docurl;
      data.doctitle = doctitle;
      data.newdoctitle = "Homepage";
      data.newdocurl = "/?back=1";
      // if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && window.geoinfo.notEU) {
      if (window.geoinfo && window.geoinfo.hasOwnProperty("isGDPRRegion") && !window.geoinfo.isGDPRRegion) {
        // history.replaceState(null, "Recommended Homepage", "/recommended.cms");
        // history.replaceState(null, "Homepage", "/?back=1");
        // history.pushState(null, doctitle, docurl);
        safelyReplaceHistory(data);
        // } else if (window.geoinfo && window.geoinfo.hasOwnProperty("notEU") && !window.geoinfo.notEU) {
      } else if (window.geoinfo && window.geoinfo.hasOwnProperty("isGDPRRegion") && window.geoinfo.isGDPRRegion) {
        // history.replaceState(null, "Homepage", "/?back=1");
        // history.pushState(null, doctitle, docurl);
        safelyReplaceHistory(data);
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.isGDPRRegion) {
            // history.replaceState(null, "Homepage", "/?back=1");
            // history.pushState(null, doctitle, docurl);
            safelyReplaceHistory(data);
          } else {
            // history.replaceState(null, "Homepage", "/?back=1");
            // history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            // history.pushState(null, doctitle, docurl);
            safelyReplaceHistory(data);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      // history.replaceState(null, "Homepage", "/");
      // history.pushState(null, doctitle, docurl);
      if (window) {
        window.onbeforeunload = function(e) {
          //window.history.back();
        };
      } else {
        let listenerFun = function(event) {
          document.removeEventListener("gdpr.status", listenerFun, true);
          if (event && event.detail && event.detail.isGDPRRegion) {
            history.replaceState(null, "Homepage", "/");
            history.pushState(null, doctitle, docurl);
          } else {
            history.replaceState(null, "Recommended Homepage", "/recommended.cms");
            history.pushState(null, doctitle, docurl);
          }
        };
        document.addEventListener("gdpr.status", listenerFun, true);
      }
      //_getCookie("ckns_policy") ?  history.replaceState(null, "Homepage", "/?back=1") : history.replaceState(null, "Recommended Homepage", "/recommended.cms");
      //history.replaceState(null, "Homepage", "/recommended_default.cms");
      //history.replaceState(null, "Homepage", "/?back=1");
    } else if (getPageType(window.location.href) != "home" && !window.location.href.includes("frmapp=yes")) {
      let docurl = window.location.href;
      let doctitle = document.title;
      history.replaceState(null, "Homepage", "/");
      history.pushState(null, doctitle, docurl);
    }

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      document.body.classList.add("webview");
    }

    let masterPlayer = document.querySelector(".master-player-container");

    if (masterPlayer) {
      masterPlayer.addEventListener("webkitfullscreenchange", function() {
        if (
          document.webkitIsFullScreen == false &&
          document.getElementsByClassName("spl_overlay spl_A")[0].classList.contains("spl_H")
        ) {
          _this.dockMasterPlayer({ detail: "dockVideo" });
        }
      });
    }

    //Delay font files
    //Moved font loading to HTML.js for experimentation
    //this.loadfontFiles();

    window.addEventListener("scroll", this.showCube.bind(this));

    let pageType = getPageType(this.props.router.location.pathname);
    if (pageType != "home") {
      dispatch(fetchTopListDataIfNeeded(params, query, this.props.router));
    }

    //-------- Lok sabha Election Cube
    // RotateCube();
    // if (_sessionStorage().get('CricketCube') == null) {
    //   _sessionStorage().set('CricketCube', true);
    // }

    // ExitPollRotateCube();
    //data hit for CSR
    // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());

    //------- Data pulling of loksabha result
    // dataPooling("result", this.props);

    //------------End Loksabha Cube-----------------
    //set dfpAdConfig

    window.dfpAdConfig = _filterRecursiveALJSON(header, ["pwaconfig", "dfpAdConfig"], true);

    //For Prebid, setting app state(prebidconfig) in window object
    // _isCSR() && prebid
    //   ? prebidModule.setPreBidObj(prebid[process.env.SITE])
    //   : null;

    //Initiate the event to refresh ads slots on view
    //  process.env.SITE == "tlg" ? modifyAdsOnScrollView() : "";

    // Handling for Common ads, topatf and andbeyond
    //Ads part of app shel (not template wise)
    renderAppShellAds();

    // setting window object for ads config
    if (window && this.props.adconfig) {
      window.adconfig = this.props.adconfig;
    }
    // if (Config && Config.TimesPoint && Config.TimesPoint.jsProd) {
    //   setTimeout(() => {
    //     loadJS(Config.TimesPoint.jsProd, () => {
    //       if (typeof initTimesPoint === "function") {
    //         initTimesPoint();
    //         // if (document.getElementById('tpwidget-prelogin')) {
    //         //   const loginDiv = document.getElementById('tpwidget-prelogin');
    //         //   loginDiv.onclick = this.showLoginRegister;
    //         // }
    //       }
    //     });
    //   }, 2000);
    // }

    if (typeof document) {
      document.addEventListener("user.status", this.userLoginCallback, false);
      document.addEventListener("userLogout", this.userLogoutCallback, false);
    }

    window.izootocallback = () => {
      window._izq = window._izq || [];
      window._izq.push(["init"]);
      window._izq.push([
        "registerSubscriptionCallback",
        function(obj) {
          var event = new CustomEvent("browserNotification", { detail: obj });
          document.dispatchEvent(event);
        },
      ]);
    };

    // setTimeout(this.makeMiniTvDraggable, 5000);
    this.makeMiniTvDraggable();

    // To enable visibility in strip, if minitv was closed in last session
    if (_isCSR() && _sessionStorage().get("MINITV_CLOSED")) {
      const playerTypeEvent = new CustomEvent("setMinitvVisibility", {
        detail: { showMinitv: false },
      });
      document.dispatchEvent(playerTypeEvent);
    }
    _loadsvgsprite(header);
    //Below function will be called if any route change happens
    this.unlisten = browserHistory.listen(location => {
      globalconfig.isLandingPage = false;
    });
  }

  componentDidUpdate(prevProps, prevState) {
    this.showCube();
    // Following code is for loading the highcharts library on the elections pages if it is not already loaded
    const highChartsPagesArray = ["elections"];
    const { router } = this.props;
    const url = router.location.pathname;
    if (
      url &&
      typeof url === "string" &&
      highChartsPagesArray.indexOf(getPageType(url)) > -1 &&
      !document.getElementById("highchart-script")
    ) {
      loadJS("https://code.highcharts.com/highcharts.js");
      loadJS("https://code.highcharts.com/modules/exporting.js");
      loadJS("https://code.highcharts.com/modules/export-data.js", undefined, "highchart-script");
    }
  }

  componentWillReceiveProps(nextProps) {
    //Reset thumb to tech on conditional basis
    //take default msid of thumb from config, which we set when our app.js initialised.
    isTechSite() &&
    this.props.router &&
    this.props.router.location &&
    this.props.router.location.pathname &&
    this.props.router.location.pathname.indexOf("/tech") > -1
      ? _setStaticConfig({
          imageconfig: { thumbid: siteConfig.imageconfig.thumbid },
        })
      : _setStaticConfig({ imageconfig: { thumbid: this.config.thumbid } });
  }

  // _this.props.router.location.pathname

  userLoginCallback = data => {
    const { dispatch, authentication } = this.props;
    const userInfo = data.detail;
    const userAction = typeof window && window.loginButtonClicked ? "login" : "auto-login";
    if (userInfo && authentication && !authentication.userData) {
      // setGRXParameter("userid", userInfo.uid || "");
      if (window.grx) {
        window.grx("userId", userInfo.uid);
      }
      dispatch(updateUserData(userInfo, userAction));
      delete window.loginButtonClicked;
    }
  };

  userLogoutCallback = data => {
    const { dispatch } = this.props;
    dispatch(updateUserData(null, "logout"));
    if (window.grx) {
      window.grx("userId", null);
    }
    // setGRXParameter("userid", "");
  };

  setMinitvVisibility(event) {
    event && event.detail ? this.props.dispatch(setMinitvVisibility(event.detail.showMinitv)) : null;
  }

  setMinitvPlaying(event) {
    event && event.detail ? this.props.dispatch(setMinitvPlaying(event.detail.isMinitvPlaying)) : null;
  }

  setVideoHeader(event) {
    this.props.dispatch(setVideoHeader(event.detail));
  }

  setVideoMsid(event) {
    this.props.dispatch(setVideoMsid(event.detail));
  }

  makeMiniTvDraggable = () => {
    //FIXME: Add condition to call this only when minitv exists

    // Config with all relevant positions of minitv container
    const minitvDragConfig = this.minitvDragConfig;
    // Container to drag the minitv around in ( encompasses whole page)
    const rootContainer = document.querySelector("body");
    // Minitv wrapper which will be dragged around
    const miniTvContainer = document.getElementById("outer_minitv_container");

    // Function to change position of dragged element
    const setTranslatePosition = (xPos, yPos, element) => {
      element.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
    };

    //Drag start fired on mouse down
    const dragStart = e => {
      // Calculating initial X and Y
      minitvDragConfig.initialX = e.clientX - minitvDragConfig.xOffset;
      minitvDragConfig.initialY = e.clientY - minitvDragConfig.yOffset;

      if (e.target === miniTvContainer || e.target.parentElement.parentElement === miniTvContainer) {
        // When target is minitv div, make dragging true in config
        minitvDragConfig.active = true;
      }
    };

    // Drag in progress when mouse is being moved around
    const dragInProgress = e => {
      // Check if drag was started by clicking currently
      if (minitvDragConfig.active) {
        e.preventDefault();

        // Update config according to current values
        minitvDragConfig.currentX = e.clientX - minitvDragConfig.initialX;
        minitvDragConfig.currentY = e.clientY - minitvDragConfig.initialY;

        // Change offsets for next drag / When drag stops
        minitvDragConfig.xOffset = minitvDragConfig.currentX;
        minitvDragConfig.yOffset = minitvDragConfig.currentY;

        setTranslatePosition(minitvDragConfig.currentX, minitvDragConfig.currentY, miniTvContainer);
      }
    };

    // Drag end fired when mouse is released
    const dragEnd = e => {
      // Set next initial X and Y as current on drag end
      minitvDragConfig.initialX = minitvDragConfig.currentX;
      minitvDragConfig.initialY = minitvDragConfig.currentY;

      // Set currently dragging as false.
      minitvDragConfig.active = false;
    };

    if (rootContainer && miniTvContainer) {
      rootContainer.addEventListener("mousedown", dragStart, false);
      rootContainer.addEventListener("mouseup", dragEnd, false);
      rootContainer.addEventListener("mousemove", dragInProgress, false);
    }
  };

  setVideoType(event) {
    if (event.detail.videoType === "adStarted") {
      this.props.dispatch(updateAdState(true));
    } else if (event.detail.videoType === "adEnded") {
      this.props.dispatch(updateAdState(false));
    } else {
      this.props.dispatch(setVideoType(event.detail.videoType));
      this.props.dispatch(updateAdState(false));
    }
  }

  playVideoInPopup(event) {
    this.addVideoConfigData(event);
    this.openVideoPopup();
    this.props.dispatch(removeSliderData());
  }

  openVideoPopup() {
    if (document) {
      document.body.style.overflow = "hidden";
    }
    this.props.dispatch(showVideoPopup());
  }

  addVideoConfigData(event) {
    this.props.dispatch(removeVideoConfig());
    this.props.dispatch(addVideoConfig(event.detail.config));
  }

  addSliderData(event) {
    const videoList = event.detail.nextvideos;
    if (videoList && videoList.items) {
      this.props.dispatch(addSliderData(videoList.items));
    }
  }

  updateVideoConfig(event) {
    this.props.dispatch(updateVideoConfig(event.detail.config));
  }

  handleScroll() {
    // loadJS(siteConfig.izooto, window.izootocallback, "izooto-jssdk", true);

    const gSignin = document.createElement("script");
    gSignin.src = "https://apis.google.com/js/platform.js";
    gSignin.defer = true;

    // const onetapSignin = document.createElement("script");
    // onetapSignin.src = "https://accounts.google.com/gsi/client";
    // onetapSignin.defer = true;

    // // document.head.appendChild(gSignin);
    // document.head.appendChild(onetapSignin);

    this.setState({ scroll: true });

    this.unbindScroll();
  }
  unbindScroll() {
    window.removeEventListener("scroll", this.scrollRef);
  }

  loadfontFiles() {
    var newStyle = document.createElement("style");
    newStyle.appendChild(document.createTextNode(siteConfig.fontContent));
    document.head.appendChild(newStyle);
  }

  componentWillMount() {
    const { router } = this.props;
    const pathname = router && router.location && router.location.pathname ? router.location.pathname : undefined;
    const dfpAdConfig = _filterRecursiveALJSON(this.props.header, ["pwaconfig", "dfpAdConfig"], true);
    // TODO - Check for SCN, SubSCN values here to avoid page jump
    if (checkTopAtf(dfpAdConfig, pathname)) {
      globalconfig.topatfEnabled = true;
    } else {
      globalconfig.topatfEnabled = false;
    }
    globalconfig.router = router;
    globalconfig.pagetype = pathname ? getPageType(pathname) : "others";
    globalconfig.ad_pagetype = pathname ? getPageType(pathname, true) : "others";
  }

  componentWillUnmount() {
    //removing all event listeners
    document.removeEventListener("playVideoInPopup", this.playVideoInPopup.bind(this), false);
    document.removeEventListener("showVideoPopup", this.openVideoPopup.bind(this), false);
    document.removeEventListener("setVideoHeader", this.setVideoHeader.bind(this), false);
    document.removeEventListener("playVideo", this.addVideoConfigData.bind(this), false);
    document.removeEventListener("addSliderData", this.addSliderData.bind(this), false);
    document.removeEventListener("setPlayingVideoMsid", this.setVideoMsid.bind(this), false);
    document.removeEventListener("setVideoPlayerType", this.setVideoType.bind(this), false);
    document.removeEventListener("setMinitvPlaying", this.setMinitvPlaying(event), false);
    document.removeEventListener("setMinitvVisibility", this.setMinitvVisibility(event), false);
    document.removeEventListener("updateVideoConfig", this.updateVideoConfig.bind(this), false);
    this.unlisten();
  }

  lazyLoad() {
    if ("IntersectionObserver" in window) {
      _this.pollElemObserver = new IntersectionObserver(function(entries, observer) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            let tthis = entry.target;
            dispatch(fetchPollDataIfNeeded(pollparams)); //Load Poll Widget
            _this.pollElemObserver.unobserve(tthis);
            tthis.remove();
          }
        });
      });

      _this.pollElemObserver.observe(document.querySelector("#lazyLoadpollWidget"));
    }
  }

  checkGeoInfo(cubeProp) {
    let bool = true;
    if (_isCSR()) {
      let geoinfo = window.geoinfo,
        arrCodes = [],
        arrStates = [],
        arrCity = [];
      let countrycodeStr = cubeProp && cubeProp.countrycode && cubeProp.countrycode != "" ? cubeProp.countrycode : null;
      let stateStr = cubeProp && cubeProp.state && cubeProp.state != "" ? cubeProp.state : null;
      let cityStr = cubeProp && cubeProp.city && cubeProp.city != "" ? cubeProp.city : null;
      if (geoinfo && countrycodeStr) {
        arrCodes = countrycodeStr.split(",");
        if (arrCodes.indexOf(geoinfo.CountryCode) == -1) {
          bool = false;
        }
      } else if (geoinfo && stateStr) {
        arrStates = stateStr.split(",");
        if (arrStates.indexOf(geoinfo.region_code) == -1) {
          bool = false;
        }
      } else if (geoinfo && cityStr) {
        arrCity = cityStr.split(",");
        if (arrCity.indexOf(geoinfo.city) == -1) {
          bool = false;
        }
      }
    }
    return bool;
  }

  /**
   * This is used to show/hide cube on the basis of Section name included/excluded And
   * depending on FCap remain.
   * Cube on home page is hidden on load would be visible on scroll.
   * Fcap - if time lapsed after closing the cube is greater than the fcap time cube, then cube is shown.
   */
  showCube() {
    let pageType = getPageType(this.props.router.location.pathname);
    let { election } = this.props;
    let cubeProp = this.getCubeDetails(election);

    const hideCube = new CustomEvent("hideCube", {
      detail: {
        routeChange: true,
      },
    });
    const rotateCube = new CustomEvent("rotateCube", {
      detail: {
        cubeProp: cubeProp,
      },
    });

    if (_isCSR()) {
      let cube = document && document.querySelector(".Cube");

      if (cube) {
        // First check if section name included or not || is it home page
        if (
          !this.isValidSection(cubeProp) ||
          !this.isValidPlatform(cubeProp) ||
          this.isFcapRemain(cubeProp) ||
          !this.checkGeoInfo(cubeProp) ||
          (pageType == "home" && document.documentElement.scrollTop < 250 && !this.config.isCubeVisible)
        ) {
          // if (this.cube) {
          //   this.cube.hideCube(true);
          //   return;
          // }
          window.dispatchEvent(hideCube);
          return;
        }
        // elese check for fcap value
        else {
          // if (this.cube) {
          //   this.cube.RotateCube(cubeProp);
          // }
          window.dispatchEvent(rotateCube);
          this.config.isCubeVisible = true;
          cube.className = "Cube";
        }
      }
    }
  }
  /**
   * To manipulate the value given in config part for including and excluding section for Cube.
   * @param {*} arrSection
   */
  getSectionURL(arrSection) {
    let retrnSectnArr;
    retrnSectnArr = arrSection.map((item, key) => {
      if (item == "/") {
        return "/";
      } else {
        return "/" + item + "/";
      }
    });
    return retrnSectnArr;
  }

  /**
   * this is used to match values from included/excluded from the current route path
   * @param {*} pathname
   * @param {*} arrSection
   */
  checkIfExist(pathname, arrSection) {
    let bool = false;
    for (let item of arrSection) {
      if (item == "/") {
        bool = pathname == item ? true : false;
      } else if (pathname.indexOf(item) > -1 || item == "/all/") {
        bool = true;
      }
    }
    return bool;
  }

  /**
   * This method is solely responsible to return true if cube to be shown for that section.
   * @param {*} cubeProp
   */
  isValidSection(cubeProp) {
    let pathname = this.props.router.location.pathname;
    let arrInclSection, arrExclSection, bool;
    if (cubeProp.inclSection) {
      arrInclSection = cubeProp.inclSection.split(",");
      arrInclSection = this.getSectionURL(arrInclSection);
      if (this.checkIfExist(pathname, arrInclSection)) {
        bool = true;
      }
    }
    if (cubeProp.exclSection) {
      arrExclSection = cubeProp.exclSection.split(",");
      arrExclSection = this.getSectionURL(arrExclSection);
      if (this.checkIfExist(pathname, arrExclSection)) {
        bool = false;
      }
    }

    return bool;
  }

  isValidPlatform(cubeProp) {
    let bool = false;
    let allowedplatform = cubeProp.platform.toLowerCase();
    let platform = isMobilePlatform() ? "wap" : "web";

    if (allowedplatform.indexOf("all") > -1 || allowedplatform.indexOf(platform) > -1) {
      bool = true;
    }
    return bool;
  }

  /**
   * The below method is used to specifiy property on which cube is shown
   * @param {*} cubeProp
   */
  isValidProp(cubeProp) {
    // process.env.SITE
    let bool = false;

    if (cubeProp.sites.indexOf("all") > -1 || cubeProp.sites.indexOf(process.env.SITE) > -1) {
      bool = true;
    }
    return bool;
  }

  // checkForIND(miniScoreCard) {
  //   let bool = false;
  //   if (miniScoreCard.Calendar) {
  //     miniScoreCard.Calendar.forEach(function (item) {
  //       if (item.live == 1 && (item.teama == "India" || item.teamb == "India")) {
  //         bool = true;
  //       }
  //     })
  //   }
  //   return bool;
  // }

  // publishGA(data) {
  //   if (this.config && this.config.cubeGA) {
  //     publish('CriketCubeGA', data);
  //     console.log("this.config.cubeGA", this.config.cubeGA);
  //     this.config.cubeGA = false;
  //   }
  //   return true;
  // }

  checkIOS() {
    let isIOS = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) ? true : false;
    return isIOS;
  }

  /**
   * This is used to get the config values from canvas maker
   * @param {*} cubeDetail
   */
  getCubeDetails(cubeDetail) {
    let cubeData,
      cubeProp = {},
      msInHr = 3600000;

    if (cubeDetail && cubeDetail.cube) {
      cubeData = cubeDetail.cube;
      cubeProp.autoClose = cubeData.autoclose && cubeData.autoclose != "" ? cubeData.autoclose : null;
      cubeProp.turntime = cubeData.turntime && cubeData.turntime != "" ? cubeData.turntime : 5000;
      cubeProp.inclSection = cubeData.inclSection && cubeData.inclSection != "" ? cubeData.inclSection : null;
      cubeProp.exclSection = cubeData.exclSection && cubeData.exclSection != "" ? cubeData.exclSection : null;
      cubeProp.countrycode = cubeData.countrycode && cubeData.countrycode != "" ? cubeData.countrycode : null;
      cubeProp.state = cubeData.state && cubeData.state != "" ? cubeData.state : null;
      cubeProp.city = cubeData.city && cubeData.city != "" ? cubeData.city : null;
      cubeProp.sites = cubeData.sites && cubeData.sites != "" ? cubeData.sites : null;
      cubeProp.platform = cubeData.platform && cubeData.platform != "" ? cubeData.platform : null;

      // default value for fcap is half-an-hr
      cubeProp.fcap = cubeData.fcap && cubeData.fcap != "" ? cubeData.fcap * msInHr : 0.5 * msInHr;
      // cubeProp.fcap = 60000;

      cubeProp.show = cubeData.show && cubeData.show != "" && cubeData.show == "true" ? true : false;
      cubeProp.testshow = cubeData.testshow && cubeData.testshow != "" && cubeData.testshow == "true" ? true : false;
    }
    return cubeProp;
  }

  /**
   * If time lapse after cube closed (auto/manually) is greater than fcap time, then cube is shown
   * @param {*} cubeProp
   */
  isFcapRemain(cubeProp) {
    let isfcapLeft = true;
    if (_isCSR()) {
      const prevTime = _sessionStorage().get("closeTime");
      let date = new Date();
      let curTime = date.getTime();
      if (Number(cubeProp.fcap) < curTime - Number(prevTime)) {
        isfcapLeft = false;
      }
    }
    return isfcapLeft;
  }

  render() {
    //election is used for widgetswiching
    //theme used for theming (articlelist state maped to app)
    const {
      header,
      bnews,
      minitv,
      config,
      scoreboard,
      mini_scorecard,
      electionresult,
      election,
      app,
      budget,
      theme,
      location,
      pushNotificationcooki,
    } = this.props;
    let { query } = this.props.location;
    let { dispatch, params, router } = this.props;
    let isFrmApp = _isFrmApp(this.props.router);
    let { pathname } = router.location;
    if (this.props.routes && this.props.routes[2] && this.props.routes[2].longurl) {
      pathname = this.props.routes[2].longurl;
    }
    let pagetype = getPageType(pathname);

    //Liveblog Widget config
    let budgetLbConfig = budget && budget[process.env.SITE] ? budget[process.env.SITE].lb : undefined;
    let showLiveblogWidget =
      budgetLbConfig &&
      budgetLbConfig.status == "true" &&
      budgetLbConfig.msid != "" &&
      (config.pagetype == "home" ||
        (this.props.router.location.pathname &&
          this.props.router.location.pathname.indexOf("/budget/articlelist/") > -1))
        ? true
        : false;

    //To set theme for events
    //Theme name will be used as class in parent wrapper, so that css rules can apply
    let locpath = this.props.router.location.pathname;
    let themename =
      theme &&
      theme.name &&
      (locpath.includes("articlelist") || locpath.includes("photolist") || locpath.includes("videolist"))
        ? theme.name
        : null;

    //frmapp handling
    let queryparameter =
      this.props.routing && this.props.routing.locationBeforeTransitions
        ? this.props.routing.locationBeforeTransitions.search
        : "";
    if (queryparameter && queryparameter.toLowerCase().indexOf("frmapp=yes") > -1) {
      this.config.isFrmapp = true;
    }

    //viral adda logo header
    let viralBranding =
      (this.props.router.location.pathname && this.props.router.location.pathname.indexOf("/viral-adda/") > -1) ||
      (this.props.router.location.pathname && this.props.router.location.pathname.indexOf("/viral-corner/") > -1)
        ? true
        : false;
    //Education logo header
    let educationLogo =
      this.props.router.location.pathname &&
      this.props.router.location.pathname.indexOf("/education/") > -1 &&
      process.env.SITE == "vk"
        ? true
        : false;
    //lavalavk logo header
    let lavalavkLogo =
      this.props.router.location.pathname &&
      this.props.router.location.pathname.indexOf("/lavalavk/") > -1 &&
      process.env.SITE == "vk"
        ? true
        : false;
    //tech section layer
    let techLayer = "false";
    if (
      this.props.router.location.pathname &&
      this.props.router.location.pathname.indexOf("/tech") > -1 &&
      isTechSite()
    ) {
      techLayer = true;
      //Reset thumb to tech
      _setStaticConfig({
        imageconfig: { thumbid: siteConfig.imageconfig.thumbid },
      });
    } else {
      techLayer = false;
      //Reset thumb to default
      // FIX FOR NBT-12248 (Only occurs in SSR)
      // Once thumbid is reset above, need to replace it with defaultthumb to reset it.
      //FIXME: Add default thumb to non tech sites too for certainity
      _setStaticConfig({
        imageconfig: { thumbid: siteConfig.imageconfig.defaultthumb },
      });
    }

    let ad_pagetype = getPageType(pathname, true);
    if (ad_pagetype == "videolist" || ad_pagetype == "photolist") {
      ad_pagetype = "articlelist";
    }
    // let pagetype = getPageType(this.props.router.location.pathname);
    // let homeData =
    //   this.props &&
    //   this.props.home &&
    //   this.props.home.value &&
    //   this.props.home.value[0] &&
    //   this.props.home.value[0].items &&
    //   this.props.home.value[0].items.length > 0
    //     ? this.props.home.value[0].items
    //     : null;

    let cubeProp = {};
    if (election && election.cube && election.cube.show && election.cube.show == "true") {
      cubeProp = this.getCubeDetails(election);
      // config.resultLbl = election.result.counttext;
    }
    // let isFreqLeft = (cubeProp.fcap == "infinite")? true :
    // this.showCube();

    let showTPCheckinWidget = true;
    //  Dont show tp checkin widget when cube shows up
    if (election && election.cube && election.cube.show && election.cube.show == "true") {
      showTPCheckinWidget = false;
    }

    let alaskaMastHead = filterAlaskaData(header.alaskaData, ["pwaconfig", "masthead", "desktop"], "label");
    let alaskaTPConfig = filterAlaskaData(header.alaskaData, ["pwaconfig", "TimesPoints"], "label");
    return (
      <div>
        {/* Add themename as class in wrapper (Required to apply CSS rules for events) */}
        <div id="svgsprite"></div>
        <div
          className={
            "m-scene" +
            (techLayer ? " tech" : "") +
            (viralBranding ? " viral-adda" : "") +
            (educationLogo ? ` ${process.env.SITE}-mini` : "") +
            (lavalavkLogo ? ` ${process.env.SITE}-lavalavk` : "") +
            ` ${themename || ""}`
          }
        >
          {/* By Election result widget */}
          {this.props.router.location.pathname &&
          this.props.router.location.pathname.indexOf("/byelectionresult/") < 0 &&
          typeof election != "undefined" &&
          election.result &&
          election.result.showwidget &&
          election.result.showwidget == "true" &&
          process.env.SITE == "mly" ? (
            <ByElection />
          ) : null}
          {/* Election loksabha result widget */}
          {/* {(typeof election != "undefined" &&
            election.result &&
            election.result.showwidget &&
            election.result.showwidget == "true" &&
            ((election.result.pages && election.result.pages.indexOf(pagetype) > -1) ||
              pathname.indexOf("/elections/") > -1)) ||
          pathname.indexOf("/articleshow_esi_test/") > -1 ? (
            <ErrorBoundary>
              <ElectionWidget data={electionresult} isFrmapp={this.config.isFrmapp} />
            </ErrorBoundary>
          ) : null} */}
          {/* <LokSabhaElectionResult isFrmapp={this.config.isFrmapp} pagetype={pagetype} />  */}
          {/* Election loksabha and assembly exitpoll widget */}
          {/* {typeof election != "undefined" &&
          election.exitpoll &&
          election.exitpoll.showwidget &&
          election.exitpoll.showwidget == "true" &&
          election.exitpoll.pages &&
          election.exitpoll.pages.indexOf(pagetype) > -1 ? (
            <ErrorBoundary>
              <ExitPollWidget data={this.props.exitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} />
            </ErrorBoundary>
          ) : null} */}
          {/* <LokSabhaExitPoll data={this.props.loksabhaexitpoll} isFrmapp={this.config.isFrmapp} pagetype={pagetype} /> */}
          {/* {
            <ErrorBoundary>
              <ConstituenciesWidget />
            </ErrorBoundary>
          } */}
          {typeof shouldTPRender == "function" && shouldTPRender() && (
            <React.Fragment>
              <ErrorBoundary>
                <TimesPoints
                  loc={this.props.location}
                  pageType={getPageType(this.props.location.pathname)}
                  router={this.props.router}
                />
              </ErrorBoundary>

              <ErrorBoundary>
                <TimesPointModal />
              </ErrorBoundary>
            </React.Fragment>
          )}

          {typeof shouldTPRender == "function" &&
          shouldTPRender() &&
          _isCSR() &&
          showTPCheckinWidget &&
          this.state.scroll &&
          (!_getCookie("show_nudge") || _getCookie("show_nudge") === "1") ? (
            <ErrorBoundary>
              <TimesPointNudge getMonth={getMonth} frequency={alaskaTPConfig && alaskaTPConfig._dcfrequency} />
            </ErrorBoundary>
          ) : null}

          {/* <ErrorBoundary>
              <TimesPointNudge getMonth={getMonth} />
            </ErrorBoundary> */}

          <ErrorBoundary>
            {!this.config.isFrmapp && globalconfig.topatfEnabled ? (
              <div
                data-adtype="topatf"
                className="ad1 topatf emptyAdBox"
                data-id="div-gpt-ad-1540206148700-007"
                data-name={siteConfig.ads.dfpads.topatf}
                data-size="[[1000, 60], [1000, 70]]"
              />
            ) : null}
            {/* {queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="andbeyond"
                className="ad1 andbeyond emptyAdBox"
                data-id="div-gpt-ad-1558437895757-0"
                data-name={siteConfig.ads.dfpads.andbeyond}
                //data-mlb="[[1, 1]]"
                data-size="[[1, 1]]"
              />
            ) : null} */}
            {/* {process.env.SITE == "nbt" && queryparameter.indexOf("utm_medium=affiliate") == -1 ? (
              <div
                data-adtype="networkPGT"
                className="ad1 networkPGT emptyAdBox"
                data-id="div-gpt-ad-1558437895757-0-networkPGT"
                data-name={siteConfig.ads.dfpads.networkPGT}
                //data-mlb="[[1, 1]]"
                data-size="[[1, 1]]"
              />
            ) : null} */}
          </ErrorBoundary>

          {/* AlaskaMastHead banner */}
          {alaskaMastHead && alaskaMastHead._status == "active" && alaskaMastHead._banner != "" ? (
            <div className="alaskamasthead" data-exclude="amp">
              <AnchorLink href={alaskaMastHead._destinationurl}>
                <img src={alaskaMastHead._banner} />
              </AnchorLink>
            </div>
          ) : null}
          {/* AlaskaMastHead banner End */}

          <ErrorBoundary>
            <div id="parentContainer" className="animated desktop_body">
              {this.props.children}
            </div>
          </ErrorBoundary>

          {_isCSR() ? (
            <ErrorBoundary>
              <PushNotificationCard key="pushnotify" cookieVal={pushNotificationcooki} />
            </ErrorBoundary>
          ) : null}

          <div className="CubeParent">
            {/* Configuration Cube for sports and Election --Oct-19 */}
            {(_isCSR() && cubeProp && cubeProp.show && this.isValidSection(cubeProp)) ||
            (_isCSR() &&
              cubeProp.testshow &&
              // homeData &&
              this.isValidSection(cubeProp) &&
              this.isValidPlatform(cubeProp) &&
              location.pathname.indexOf("/articleshow_esi_test/") > -1) ? (
              <ErrorBoundary>
                <Cube
                  dispatch={dispatch}
                  params={params}
                  query={query}
                  // homeData={homeData}
                  cubeProp={cubeProp}
                  router={this.props.router}
                  ref={inst => (this.cube = inst)}
                />
              </ErrorBoundary>
            ) : null}
            {/*
              ((_isCSR() && cubeProp.show && this.isValidSection(cubeProp) && this.isValidProp(cubeProp)) || (_isCSR() && cubeProp.testshow && this.isValidSection(cubeProp) && this.isValidProp(cubeProp) && (location.pathname.indexOf('/articleshow_esi_test/') > -1))) ?
                <ErrorBoundary>
                  <Cube dispatch={dispatch} params={params} query={query}
                    cubeProp={cubeProp} election={election} electionresult={electionresult} ref={inst => this.cube = inst} ></Cube>
                </ErrorBoundary>
                : null
            */}
          </div>

          {/* Election cube */}
          {/*
            // (_isCSR() && _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
            (_isCSR() && !this.config.isFrmapp && election.result && election.result.cube && election.result.cube.show == "true" &&
              _sessionStorage().get('ResultPollCube') && _sessionStorage().get('ResultPollCube') != 'false' && this.props.loksabhaelectionresult && this.props.loksabhaelectionresult.data) ?
              <ErrorBoundary>
                <ElectionCube data={this.props.loksabhaelectionresult} election={this.props.election} />
              </ErrorBoundary>
              : null
          */}

          {/* Election EXITPOLL cube */}
          {/* Check for showcube condition , and check for on which pages to show pages , * is for all */}
          {/*
            (_isCSR() && _sessionStorage().get('ExitPollCube') && _sessionStorage().get('ExitPollCube') != 'false' && election.exitpoll && election.exitpoll.cube && election.exitpoll.cube.show && election.exitpoll.cube.show == "true"
              && election.exitpoll.cube.pages && (election.exitpoll.cube.pages.indexOf('-*-') > -1 || election.exitpoll.cube.pages.indexOf('-' + pagetype + '-') > -1)) ?
              <ErrorBoundary>
                <ExitPollElectionCube data={this.props.loksabhaexitpoll} />
              </ErrorBoundary>
              : null

          */}
        </div>
        {/**hack for innove,off for now FIXME: 
        _isCSR() ? (
          <div suppressHydrationWarning dangerouslySetInnerHTML={{ __html: `` }} />
        ) : ad_pagetype && siteConfig.ads.dfpads[ad_pagetype] && siteConfig.ads.dfpads[ad_pagetype].innove ? (
          <div
            dangerouslySetInnerHTML={{
              __html: `<script>
                        window.googletag = window.googletag || {cmd: []};
                        googletag.cmd.push(function() {
                          googletag.defineOutOfPageSlot('${siteConfig.ads.dfpads[ad_pagetype].innove.name}', 'div-gpt-ad-1584421340704-0-innove').addService(googletag.pubads());
                          googletag.enableServices();
                        });
                      </script>
                      <div id='div-gpt-ad-1584421340704-0-innove'>
                        <script>
                          window.googletag = window.googletag || {cmd: []};
                          googletag.cmd.push(function() { googletag.display('div-gpt-ad-1584421340704-0-innove'); });
                        </script>
                      </div>`,
            }}
          />
          ) : null*/}
        {_isCSR() && <DockPortal />}
        {_isCSR() && <VideoPopup />}
        {_isCSR() && <TopStoryPopup location={location} />}
      </div>
    );
  }
}

App.fetchData = function({ dispatch, params, query, history, router }) {
  let { pathname } = router.location;
  let pagetype = getPageType(pathname);
  // dispatch(fetchMiniScheduleDataIfNeeded(params, query));
  return Promise.resolve([]);

  // dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
  /*
  if (pagetype == 'home' || pagetype == 'liveblog') {
    //dispatch(fetchExitPollIfNeeded());
    //dispatch(fetchLOKSABHAEXITPOLL_IfNeeded());
    params && (params.value = _electionConfig.feedlanguage);
    dispatch(fetchELECTIONRESULT_IfNeeded());
    // dispatch(fetchLOKSABHAELECTIONRESULT_IfNeeded(params, query, "result"));
  }
  */
  // return dispatch(fetchHeaderDataIfNeeded()).then(() => {
  //   dispatch(fetchWidgetSequenceDataIfNeeded());
  // });
};

App.propTypes = {
  children: PropTypes.node,
};

function mapStateToProps(state) {
  return {
    ...state.app,
    config: state.config,
    routing: state.routing,
    scoreboard: state.scoreboard,
    home: state.home,
    //Mapping articlelist state object as theme to app (For events)
    theme:
      state.articlelist.value && state.articlelist.value[0] && state.articlelist.value[0].theme
        ? state.articlelist.value[0].theme
        : {},

    exitpoll: state.exitpoll,
    electionresult: state.electionresult,
    header: state.header,
    authentication: state.authentication,
    // loksabhaelectionresult: state.loksabhaelectionresult,
    // loksabhaexitpoll: state.loksabhaexitpoll,
  };
}

const renderAppShellAds = () => {
  //handle topatf and andbeyond
  if (document.querySelector(".topatf, .andbeyond, .innove")) {
    let listenerFun = function() {
      document.removeEventListener("gdpr.status", listenerFun, true);
      Ads_Module.render({});
    };
    document.addEventListener("gdpr.status", listenerFun, true);
  }
};

const safelyReplaceHistory = data => {
  // handling of gtm
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({ genuineback: false });
  history.replaceState(null, data.newdoctitle, data.newdocurl);
  history.pushState(null, data.doctitle, data.docurl);
};

export default connect(mapStateToProps)(App);
