import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router";

import { fetchStaticPageData } from "./../../actions/staticpage/staticpage";
import ErrorBoundary from "./../../components/lib/errorboundery/ErrorBoundary";
import FakeNewsListCard from "./../../components/common/FakeCards/FakeStoryCard";
import NotFound from "../notfound/NotFound";

class StaticPage extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let _this = this;
    const { dispatch, params, query } = _this.props;

    // fetchStaticPageData({dispatch,params,query});
    StaticPage.fetchData({ dispatch, params, query });
  }

  render() {
    //console.log("Inside staticpage", this.props);
    let { htmlstr, error } = this.props;
    return (
      <div>
        {(() => {
          if (htmlstr) {
            return (
              <div
                dangerouslySetInnerHTML={{
                  __html: htmlstr
                }}
              />
            );
          } else if (error) {
            return (
              <ErrorBoundary>
                <NotFound />
              </ErrorBoundary>
            );
          } else {
            return (
              <ErrorBoundary>
                <FakeNewsListCard />
              </ErrorBoundary>
            );
          }
        })()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.staticpage
  };
}

StaticPage.fetchData = ({ dispatch, query, params }) => {
  return dispatch(fetchStaticPageData({ dispatch, params, query }));
};

export default connect(mapStateToProps)(StaticPage);
