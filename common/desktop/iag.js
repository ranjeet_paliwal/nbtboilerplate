module.exports = {
  hostid: "338",
  channelid: "696089404",
  mediawireslot: "17",
  wapsitename: "Iam Gujarat",
  wapsiteregionalname: "I am Gujarat",
  siteName: "www.iamgujarat.com",
  staticSiteName: "static.iag.indiatimes.com",
  fbIAcontentId: "1377529588952481",
  languagemeta: "gu",
  language: "Gujarati",
  channelCode: "iag",
  wapCode: "gujarati",
  gdprID: "IAG",
  appPlayStoreLink: "https://play.google.com/store/apps/details?id=com.iag.reader&hl=en_IN",
  langnetstorageBasePath: "https://langnetstorage.indiatimes.com/langappfeeds",
  newsApp: "Gujarati News - I am Gujarat",
  searchIframeLink: "/topsearchbox.cms",
  languagefullName: "Gujarati News",
  video: {
    rootId: "71923219",
    newsId: "71924303",
  },
  editTrendingMSID: "75796278",
  applist: "/appslist.cms",
  rss: "/rss.cms",
  coronaId: "75828139",
  androidpackageid: "1432506254",
  androidpackage: "com.iag.reader",
  deeplinkappid: "iagreaderactivities",
  deeplinkpubid: "pub=6:I Am Gujarat",
  iosapp_name: "IAmGujarat App",
  weburl: "https://www.iamgujarat.com",
  mweburl: "https://www.iamgujarat.com",
  gadgetdomain: "https://gujarati.gadgetsnow.com",
  ccaudId: "7269:1210",
  footerDesktopLink: "https://www.iamgujarat.com/?mobile=no",
  cookieLessDomain: "https://static.iag.indiatimes.com",
  domain: "https://www.iamgujarat.com/",
  webdomain: "iamgujarat.com",
  mwebdomain: "iamgujarat.com",
  // next phase
  photogallerydomain: "www.iamgujarat.com/photo-galleries",
  google_site_verification: "cJxHCrs_WYG8a6CfC-UMVKGQJFS32HoRMQgoWUADAuA",
  twitterHdandler: "@imgujarat",
  logo: "iag-logo.jpg",
  logo_id: "https://static.langimg.com/thumb/msid-76342013,width-166,resizemode-4/imgujarat.jpg",
  fb: "https://www.facebook.com/imgujarat/",
  twitter: "https://twitter.com/imgujarat",
  gplus: "https://plus.google.com/u/0/111247391086283244184/about",
  youtubeUrl: "https://www.youtube.com/channel/UCEAFz4sEY8Ny0ScUvMJr9Tg",
  izooto: "",
  fullutm: "&utm_campaign=iamgujarat&utm_medium=referral",
  slikeshareutm: "utm_source=slikevideo&utm_medium=referral",
  telegramFollowLink: "https://telegram.me/iamgujaratofficial",
  shortutm: "ma4", // Using for micron
  icon: "https://www.iamgujarat.com/icon/favicon-iag.ico",
  breakingNewsFeedUrl: "https://langnetstorage.indiatimes.com/LANGBNews/iagbreakingnews.htm",
  // breakingNewsFeedUrl: "https://langdev9350.indiatimes.com/pwafeeds/breakingnews.cms", local path if necessary
  fbsdkurl: "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=1989934677908101",
  fbpage: "https://www.facebook.com/imgujarat/",
  // next phase
  livebloghpfeed: "",
  livebloghostname: "iag", // Using to getch liveblog callback feed
  liveblogbnbewsurl: "",
  glossaryLanding: "https://langnetstorage.indiatimes.com/langappfeeds/iag_glossarylist_business.htm",
  navfeedurl: "https://langnetstorage.indiatimes.com/langappfeeds/iag_navjson.htm",
  // navfeedurl: "https://langdev9350.indiatimes.com/off-url-forward/wdt_navjson_v2.cms",
  // logo_schema dimension should be 600*60
  logo_schema: "https://static.langimg.com/thumb/msid-76937725,width-600,resizemode-4/iam-gujarat.jpg", // correction needed
  siteid: "2147478070",
  viralBrandingLink: "/viral/articlelist/71923146.cms",
  fbpixelid: "",
  fbappid: "1989934677908101",
  astrowidgetId: "71920097",
  bennett: "https://timesofindia.indiatimes.com/bennettjul2020_form_nbt.cms",
  css:
    "$body-bgcolor:#fff;  $navbar-top-bgcolor:#fff; $navbar-bg-color:#F5CC10; $navbar-border-top:#c8cccb; $navbar-border-shadow:#D8B40D;  $navbar-active-border-bottom-color:#000;  $navbar-link-color:#000;  $hamburger-bg-color:#747474;  $sidenav-bg-color:#fff; $sidenav-color:#000;  $sidenav-border-bottom:#F5F5F5; $sidenav-arrow-color:#D1D1D1; $layer-bg:rgba(0,0,0,0.5);  $breadcrumb-home-color:#DA4848; $card-border-bottom:#F5F5F5;  $card-bg-color:#8e8e93; $card-text-color:#191919; $card-date-color:#bababa; $home-section-border-bottom:#f5f5f5;  $home-section-heading-border-bottom:#F5CC10;  $home-section-arrow-color:#000000;  $home-section-more-color:#858585; $home-section-more-border:#CCCCCC;  $footer-background:#585858; $footer-border-top:#333;  $footer-border-bottom:#7b7b7b;  $footer-heading-color:#e6e6e6;  $footer-text-color:#fff;  $footer-link-color:#d0d0d0; $footer-trending-color:#95989a; $footer-copyright-color:#d0d0d0;",
  fontContent:
    "@font-face {font-family: Noto Sans;src: url('https://static.iag.indiatimes.com/fonts/NotoSansGujarati-Regular.eot');src: url('https://static.iag.indiatimes.com/fonts/NotoSansGujarati-Regular.eot?#iefix') format('embedded-opentype'),url('https://static.iag.indiatimes.com/fonts/NotoSansGujarati-Regular.woff2') format('woff2'),url('https://static.iag.indiatimes.com/fonts/NotoSansGujarati-Regular.ttf') format('truetype');font-weight: normal;font-style: normal;font-display: optional;}",
  fontUrl: "https://static.iag.indiatimes.com/fonts/NotoSansGujarati-Regular.woff2",
  cricket_SIAPI: {
    minischedule: "https://toicri.timesofindia.indiatimes.com/jsons/calendar_new_liupre.json",
    schedule: "https://toicri.timesofindia.indiatimes.com/jsons/calendar_new.json",
    points_table: "https://toicri.timesofindia.indiatimes.com/jsons/stats/Standings/standing_%seriesid%.json",
    topPlayer: "https://navbharattimesfeeds.indiatimes.com/pwafeeds/feed_topplayer.cms?feedtype=sjson",
  },
  Thumb: {
    userImage: "/photo/11834507.cms",
  },
  locale: {
    missedNews: "આ સમાચાર તમે વાંચ્યા?",
    live_blog: "લાઈવ બ્લોગ",
    recommendednews: "રિકમેન્ડેડ ન્યૂઝ",
    latest: "લેટેસ્ટ",
    most_read: "સૌથી વધુ વંચાયેલું",
    latest_video: "લેટેસ્ટ વિડીયો",
    latest_photo: "લેટેસ્ટ ફોટોઝ",
    whatsapp_share: "Hey, you might find this interesting: ",
    see_in_app: "એપમાં જુઓ",
    appLinkText:
      "ગુજરાત, દેશ, વિદેશ, શિક્ષણ, બિઝનેસ, મૂવી, જ્યોતિષ, ધર્મ, સ્પોર્ટ્સના લેટેસ્ટ સમાચાર ઉપરાંત વાયરલ ન્યૂઝ મેળવવા માટે ડાઉનલોડ કરો I am Gujaratની એપ",
    actors: "કલાકારો",
    releaseDate: "રિલીઝ ડેટ",
    director: "ડિરેક્ટર",
    genre: "પ્રકાર",
    duration: "અવધિ",
    readmore: "વધુ વાંચો",
    commentary: "કોમેન્ટ્રી",
    scorecard: "સ્કોરકાર્ડ",
    photo_maza: "ફોટો મઝા",
    key_words: "કીવર્ડ્સ",
    more_from_topics: "આ ટોપિક પર વધુ",
    brief: {
      tooltip: "આ ફોર્મેટમાં સમાચાર પસંદ આવ્ય હોય તો વધુ વાંચવા માટે ડાઉનલોડ કરો એપ",
      limitation_txt: "બ્રીફના બાકીના સમાચાર વાંચવા માટે ક્લિક કરો",
    },
    fblikeTxt: {
      txt1: "તમામ તાજી ખબરો માટે ",
      siteName: "I am Gujarat ",
      txt2: "ફેસબૂક પેજને લાઈક કરો ",
    },
    photoshowrhsheading: "સંબંધિત ફોટા",
    newsLetter: {
      LS: "5230",
      thankyou: "આભાર",
      headerText: "શું આપ મેળવવા ઈચ્છો છો રોજેરોજના મહત્વના સમાચાર?",
      joinIn: "જોડાઓ",
      footerText: "ઉપરના બોક્સમાં એન્ટર કરો આપનું ઈમેલ આઈડી, અને મેળવો લેટેસ્ટ ન્યૂઝ સીધા આપના ઈનબોક્સમાં",
      verification: "વેરિફિકેશન મેલ આપના ઈમેલ એડ્રેસ પર મોકલી દેવાયો છે",
      already: "You have already subscribed to the newsletter",
      Subscribed: "આપનું સબસ્ક્રિપ્શન શરુ થઈ ચૂક્યું છે",
      errorNetwork: "આપના નેટવર્કમાં સમસ્યા છે, થોડીવાર પછી ફરી પ્રયાસ કરો!",
      notverified: "વેરિફિકેશન મેલ આપના ઈમેલ આઈડી પર મોકલી દેવાયો છે. કૃપા કરી તેને વેરિફાઈ કરો",
      verified: "આપનું ઈમેલ આઈડી પહેલાથી જ વેરિફાઈ થઈ ચૂક્યું છે",
      unchecked: "કૃપા કરી ચેક બોક્સ સિલેક્ટ કરો",
      validemail: "કૃપા કરી સાચું ઈમેલ આઈડી એન્ટર કરો (example@domain.com)",
      blankemail: "ઈમેલ એડ્રેસને ખાલી નહીં છોડી શકાય",
      login: "લોગ ઈન થવા સબસ્ક્રાઈબ કરો",
      error: "કોઈ સમસ્યા આવી રહી છે? સુનિશ્ચિત કરો કે આપે જૂના મેલરમાંથી અનસબસ્ક્રાઈબ નથી કર્યું",
    },
    social_download: {
      title: "જોતા રહો આઇ એમ ગુજરાત અને રહો લેટેસ્ટ ન્યૂઝ સાથે અપડેટ",
      fblike: "Like our Facebook page",
      appdownload: "Download our app",
      extrasharetxt: "To download and read in app, please click here.",
      fblikeURL:
        "https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fimgujarat%2F&width=90&layout=button_count&action=like&size=small&share=false&height=21&appId=1989934677908101",
    },
    share_popup_header: "Please share",
    read_more: "વધુ વાંચો",
    read_less: "ઓછું વાંચો",
    view_gallery_from_start: "ગેલેરી શરુથી નિહાળો",
    next_gallery: "નેક્સ્ટ ગેલેરી",
    next_article: {
      moviereview: "મૂવી રિવ્યૂ",
      topicslist: "ટોપિક લિસ્ટ",
      articleshow: "Read Next Story",
      default: "ડિફોલ્ટ",
      photoshow: "ફોટો શો",
      movieshow: "નેક્સ્ટ મૂવી",
    },
    next_topic: {
      more_all: "વધુ જુઓ",
      more_news: "વધુ સમાચાર",
      more_photos: "વધુ ફોટોગ્રાફ્સ જુઓ",
      more_videos: "વધુ વિડીયો જુઓ",
    },
    search_placeholder: "Search",
    highlight: "હાઈલાઈટ્સ:",
    popularVideos: "પોપ્યુલર વિડીયો",
    orientation_heading: "Please rotate the device",
    orientation_text:
      "Our site is not supported in landscape mode. Please rotate the device and continue reading in portrait mode.",
    videos: "વિડીયો",
    photo: "ફોટો",
    related_videos: "રિલેટેડ વિડીયો",
    most_viewed_videos: "સૌથી વધુ જોવાયેલા વિડીયો",
    default_horizontal_tile_width: 133, // These widths are used for slider
    default_slideshow_tile_width: 120,
    related_articles_horizontal_tile_width: 230,
    relatedarticle: "રિલેટેડ આર્ટિકલ",
    cast: "કલાકારો",
    read_more_listing: "more",
    critics_review: "ક્રિટિકનો રિવ્યૂ",
    photomasti: "ફોટોમસ્તી",
    twitte_reaction: "ટ્વીટર પર રિએક્શન",
    critics_rating: "ક્રિટિકનું રેટિંગ",
    user_rating: "વાંચકોનું રેટિંગ",
    movie_review: "મૂવી રિવ્યૂ",
    film_review: "ફિલ્મ રિવ્યૂ",
    review_movie: "મૂવીનો રિવ્યૂ લખો",
    rate_movie: "મૂવીને રેટ કરો",
    rate: "રેટ",
    rate_recipe: "રેસિપીને રેટ કરો",
    slide_to_rate: "રેટ કરવા સ્લાઈડ કરો",
    slide_to_rate_movie: "મૂવીને રેટ કરવા સ્લાઈડ કરો",
    reader_rating: "વાંચકોનું રેટિંગ",
    movie_review_tile_bg: "https://static.langimg.com/moviereview_list_bg/photo/65022261.cms",
    movie_review_detail_bg: "https://static.langimg.com/moviereview_list_bg/photo/65594589.cms",
    breaking_news: "બ્રેકિંગ ન્યૂઝ",
    trending: "ટ્રેન્ડિંગ",
    highlightText: "હાઈલાઈટ ટેક્સ્ટ",
    live_video: "લાઈવ વિડીયો", // Using for minitv widget
    view_text: "ટેક્સ્ટ જુઓ",
    aur_jaane: "વધુ જાણો",
    oops: "ઓહ...",
    thanks: "આભાર.. ",
    offlineText: "આપ ઓફલાઈન છો",
    onlineText: "ઓનલાઈન ટેક્સ્ટ",
    openinapp: "એપમાં જુઓ",
    superhit: "  સુપરહિટ",
    live_updates: "લાઈવ અપડેટ્સ",
    fblikecardtext: "IAGના ફેસબુક પેજને લાઈક કરો", // Using in Service Drawer
    appdownloadcardtext: "હંમેશા રહો કનેક્ટેડ IamGujaratની એપ સાથે", // Using in Service Drawer
    installapp: "એપ ઈન્સ્ટોલ કરો",
    login: "લોગઈન",
    logout: "લોગઆઉટ",
    amazonDeals: "એમેઝોન ડીલ્સ",
    seemoreimages: "વધુ ઈમેજ જુઓ", // Using in Photomazzashow
    read_more_liveblog: "વધુ વાંચો",
    footerheading: "Search",
    comment_text: "કૉમેન્ટ લખો",
    download_text: "ટેક્સ્ટ ડાઉનલોડ કરો",
    write_comment: "કૉમેન્ટ લખો",
    view_comment: "કૉમેન્ટ જુઓ",
    text_topcomment: "ટોપ કૉમેન્ટ",
    text_latestcomment: "લેટેસ્ટ કૉમેન્ટ",
    text_author_comment: "ઓથર કૉમેન્ટ",

    wp_broadcast: {
      description: "Get  the  most important and latest news in your Whatsapp.",
      btnText: "Subscribe",
      url: "https://widget.whatsbroadcast.com/widget_more/8531f3a382edfe5c49a332a5fb08a2aa/?show=walink&text=",
    },
    crossbrand: "This article was originally published in {site name}",
    appexclusive: "એપ એક્સ્લુઝિવ",
    votesuccessfully: "વોટ સફળતાપૂર્વક અપાઈ ગયો છે",
    votealready: "આપ વોટ કરી ચૂક્યા છો",
    thanks: "આભાર",
    submit_vote: "વોટ",
    photoshow_app_install: {
      read_more: "વધુ વાંચો",
      download: "ડાઉનલોડ",
    },
    disclaimer: "ડિસ્ક્લેમર",
    live_tv: "લાઈવ ટીવી",
    data_hub: "ડેટ હબ",
    fantasy_game: "ફેન્ટસી ગેમ",
    topics_no_result: "આ ટોપિક પર કોઈ રિઝલ્ટ ઉપલબ્ધ નથી",
    utility: "ઉપયોગિતા",
    tech: {
      category: [],
      gadgetSliderConfig: {},
      affiliateTags: {},
    },
    user_review: "User Review",
    listenAlbum: "Listen to this album",
    castncrew: "Cast & Crew",
    filmSongs: "Film Songs",
    news: "News",
    photos: "Photos",
    choseStateCity: { dropDownText: "શહેર પસંદ કરો", extraCityContent: "વાંચો આપના શહેરના સમાચાર" },
  },

  imageconfig: {
    thumbid: "76342009",
    liveblogthumb: "56295200",
    defaultthumb: "76342009",
    imagedomain: "https://static.langimg.com/thumb/msid-",
    bigimage: "width-680,resizemode-3", // Article Inline Images, will maintain aspect ratio of original image
    gallerythumb: "width-700,height-525,resizemode-75", // photp Listing large gallery thumbs and articleshow lead image, Aspect Ratio 4:3
    smallthumb: "width-400,height-300,resizemode-75", // Listing small thumbs, Aspect Ratio 4:3
    smallslideshowthumb: "width-135,height-102,resizemode-1", // Used for external photogallery strip on top news, Aspect Ratio 4:3
    midthumb: "width-540,height-405,resizemode-75", // Photo Listing widget thumb, etc Aspect Ratio 4:3
    largethumb: "width-540,height-405,resizemode-75", // Lead Images Aspect Ratio 4:3
    amplargethumb: "width-1600,height-900,resizemode-75", // Lead Images Aspect Ratio 16:9 for AMP only videoshow
    amplargethumbarticle: "width-1200,height-900,resizemode-75", // Lead Images Aspect Ratio 16:9 for AMP only for article
    smallwidethumb: "width-320,height-180,resizemode-75", // For video listing widgets Aspect Ratio 16:9
    largewidethumb: "width-680,height-380,resizemode-75", // For videowidget lead image and videoshow Aspect Ratio 16:9
    posterthumb: "width-392,height-514,resizemode-4", // Moview Posters
    squarethumb: "width-400,height-400,resizemode-75", // for two colum layout
    rectanglethumb: "width-160,height-103,resizemode-75", // for rectangle flags
    smallsquarethumb: "width-150,height-150,resizemode-3", // for resize mode three
    // gnthumb: "width-200,resizemode-75", //for Gadget now
    // gnthumbWithHeight: "height-150,resizemode-75" // for GN PDP, need height
  },
  sso: {
    BASE_URL: "https://www.iamgujarat.com",
    SSO_CHANNEL: "iamguj",
    stg: {
      BASE_URL: "https://langdev9350.indiatimes.com",
      MYTIMES_URL_USER: "https://mytest.indiatimes.com/mytimes/profile/info/v1/?ssoid=",
      MYTIMES_URL_SETTINGS: "https://jssostg.indiatimes.com/sso/identity/profile/edit?channel=tm",
      SSO_URL_LOGIN: "https://jssostg.indiatimes.com/sso/identity/login",
      SSO_URL_LOGOUT: "https://jssostg.indiatimes.com/sso/identity/profile/logout/external",
      SSO_URL_GETTICKET: "https://jssostg.indiatimes.com/sso/crossdomain/getTicket?version=v1&platform=wap&channel=",
      SOCIAL_API_INTEGRATOR: "https://testsocialappsintegrator.indiatimes.com/socialsite",
      SSO_BASE_URL: "https://jssostg.indiatimes.com/sso/crossdomain",
      SSO_CROSSWALK: "https://jssocdnstg.indiatimes.com/crosswalk/jsso_crosswalk_0.6.0.min.js",
    },
    SSO_BASE_URL: "https://jsso.indiatimes.com/sso/crossdomain",
    SSO_CROSSWALK: "https://jssocdn.indiatimes.com/crosswalk/jsso_crosswalk_0.5.3.min.js",
    SSO_URL_LOGIN: "https://jsso.indiatimes.com/sso/identity/login",
    SSO_URL_LOGOUT: "https://jsso.indiatimes.com/sso/identity/profile/logout/external",
    SSO_URL_GETTICKET: "https://jsso.indiatimes.com/sso/crossdomain/getTicket?version=v1&platform=wap&channel=",
    SSO_TRUECALLER_visibility: 4,
    SSO_URL_NewsTicket: "https://jsso.indiatimes.com/sso/crossdomain/getTicket?version=v1",
    SSO_URL_SubStatus: "https://webapi.sendpal.in/api/v1/get/user/ngs/status?dataformat=json",
    SSO_URL_unsubscribe: "https://webapi.sendpal.in/api/v1/multiple/unsubscribe/ngs?dataformat=json",
    SSO_URL_subscribe:
      "https://webapi.sendpal.in/api/v1/add/subscription/ngs?frequency=1&scheduletime=1&status=1&dataformat=json&nlid=39",
    SSO_URL_confirm: "https://webapi.sendpal.in/api/v1/confirm/subscription/ngs?dataformat=json",
    PROFILE_URL: "https://mytimes.indiatimes.com/profile",
    SOCIAL_API_INTEGRATOR: "https://socialappsintegrator.indiatimes.com/socialsite",
    // next phase
    MYTIMES_URL_USER: "https://myt.indiatimes.com/mytimes/profile/info/v1/?ssoid=",
    MYTIMES_URL_SETTINGS: "https://jsso.indiatimes.com/sso/identity/profile/edit?channel=iamguj",
    MYTIMES_ALREADY_RATED: "https://myt.indiatimes.com/mytimes/alreadyRated?appKey=TELUGU&",
    MYTIMES_ADD_ACTIVITY: "https://myt.indiatimes.com/mytimes/addActivity?appKey=TELUGU&",
  },
  TimesPoint: {
    jsDev: "https://test.timespoint.com/tpwidgets-test/static/dist/js/main.js",
    jsProd: "https://image.timespoints.iimg.in/tpwidgets/static/dist/js/main.js",
    channels: ["tml", "nbt", "eisamay", "tlg"],
    // status: true,
    pcode: "channel97218",
    scode: "channel97218",
    activities: {
      DAILY_CHECKINS: "act6012496",
      DAILY_CHECKINS_MOBILE: "act6030506",
      READ_ARTICLE: "act8316645",
      WATCH_VIDEOS: "watch_video",
      SH_FB: "sh_fb",
      SH_TW: "sh_tw",
      LOGIN: "login",
      MOVIE_RATE: "act3375455",
      // Fired by MyProfile team
      MOVIE_REVIEW: "act3297358",
      VIEW_PHOTO: "view_photo",
      LIVE_BLOG: "act6238982",
      SUB_NL: "sub_nltr",
      REGISTER: "act8838785",
      // Fired by MyProfile team
      COMMENT: "cmnt",
      SUB_NOTIFICATION: "act264224",
      // Not found for staging
      CHECKIN_BONUS_DESKTOP: "act6332795",
      // Not found for staging
      CHECKIN_BONUS_MOBILE: "act6352539",
      WHATSAPP_SHARE: "act6313063",
    },
    campaingHistoryName: {
      desktop: "campaign7551210",
      mobile: "campaign7662887",
    },
    campaignDays: 5,

    stg: {
      pcode: "channel3148168",
      scode: "channel3148168",
      campaingHistoryName: {
        desktop: "campaign1396315",
        mobile: "campaign1522056",
      },
      activities: {
        DAILY_CHECKINS: "act2110205",
        DAILY_CHECKINS_MOBILE: "act2135027",
        READ_ARTICLE: "read",
        WATCH_VIDEOS: "watch_video",
        SH_FB: "sh_fb",
        SH_TW: "sh_tw",
        LOGIN: "act2313032",
        MOVIE_RATE: "act2346857",
        // Fired by MyProfile team
        MOVIE_REVIEW: "rv",
        VIEW_PHOTO: "view_photo",
        LIVE_BLOG: "act2666870",
        SUB_NL: "sub_nltr",
        REGISTER: "register",
        // Fired by MyProfile team
        COMMENT: "cmnt",
        SUB_NOTIFICATION: "act2807986",
        CHECKIN_BONUS_DESKTOP: "act2890495",
        CHECKIN_BONUS_MOBILE: "act2920867",
        WHATSAPP_SHARE: "act2839582",
      },
    },

    TP_FAQ_MSID: "79549078",
    NP_DOMAIN: {
      production: "https://npcoins.indiatimes.com",

      development: "https://nprelease.indiatimes.com",
      stg1: "https://nprelease.indiatimes.com",
      stg2: "https://nprelease.indiatimes.com",
    },

    TP_DOMAIN: {
      production: "https://tpapi.timespoints.com",

      development: "https://test.timespoints.com/tpapi",
      stg1: "https://test.timespoints.com/tpapi",
      stg2: "https://test.timespoints.com/tpapi",
    },

    SDK_DOMAIN: {
      production: "https://image.timespoints.iimg.in",

      development: "https://test-img.timespoints.com",
      stg1: "https://test-img.timespoints.com",
      stg2: "https://test-img.timespoints.com",
    },

    tpwidgetJS: {
      production: "/tpwidgets/dist/js/tpwidget.js",

      development: "/static/tpwidgets/static/dist/js/tpwidget.js",
      stg1: "/static/tpwidgets/static/dist/js/tpwidget.js",
      stg2: "/static/tpwidgets/static/dist/js/tpwidget.js",
    },

    //* * Moved to common config */
    tpPage: "/timespoints.cms",
    tpRedeemLink: "https://www.timespoints.com/products",
  },
  // Next phase
  Comments: {
    topComments: {
      appkey: "GUJRAT",
      sortcriteria: "AgreeCount",
      order: "desc",
      size: "1",
      lastdeenid: "0",
      after: "true",
      withReward: "false",
      pagenum: "1",
    },
    allComments: {
      appkey: "GUJRAT",
      sortcriteria: "CreationDate",
      order: "asc",
      size: "6",
      lastdeenid: 0,
      after: true,
      withReward: false,
      pagenum: 1,
    },
  },
  pages: {
    elections: "72143382",
    business: "71919639",
    iplHomePage: "77652179",
    stateSection: "71919524",
  },
  iplpages: {
    venues: "77652208",
    teams: "77652278",
    pointsTable: "77652194",
    miniSchedule: "77652306",
  },
  redis: {
    host: "192.168.24.51",
    port: "9350",
    db: "1",
  },
  // To print label in listing node at bottom right corner [templatename, label]
  listNodeLabels: {
    articlelist: ["articlelist"],
    news: ["articleshow"],
    photo: ["photoshow"],
    slideshow: ["photoshow"],
    video: ["videoshow"],
    videolist: ["videolist"],
    photolist: ["photolist"],
    lb: ["liveblog", "Live Update"],
    moviereview: ["moviereview", "Movie Review"],
    movieshow: ["movieshow", "Movie"],
    poll: ["pollsurvey", "Poll"],
  },
  photoListView: "47882310",
  // Used In AnchorLinkCard
  routes: [
    "/exitpolls/",
    "/results/",
    "/electionlist/",
    "/articlelist/",
    "/photoarticlelist/",
    "/photomazza/",
    "/photolist/",
    "/photoshow/",
    "/photogallery/",
    "/articleshow/",
    "/moviereview/",
    "/movieshow/",
    "/liveblog/",
    "/videolist/",
    "/videoshow/",
    "/topics/",
    "/teams/",
    "/venues/",
    "/pointstable/",
    "/iplt20.cms",
    "/schedule/",
    "/sportsresult/",
    "elections.cms",
  ],

  topdrawer: [
    {
      secname: "Video",
      link: "/videos/videolist/71923219.cms",
      classname: "one",
    },
    {
      secname: "Sports",
      link: "/sports/articlelist/71919645.cms",
      classname: "two",
    },
    {
      secname: "Cinema",
      link: "/entertainment/articlelist/71919683.cms",
      classname: "three",
    },
    {
      secname: "Jokes",
      link: "/gujarati-jokes/articlelist/71934243.cms",
      classname: "four",
    },
    {
      secname: "Astrology",
      link: "/astrology/articlelist/71920097.cms",
      classname: "five",
    },
  ],

  staticPagesDir: "/var/www/html/testpages/",
  slike: {
    apikey: "iamgujaratweba5ec933e061",
    snapchatapikey: "",
    ampapikey: "teluguampweb210lang", // not used anywhere
    default_slikeid_minitv: "1x13qpaggu", // Times-Now
    nextvideo_domain: "https://navbharattimes.indiatimes.com",
    default_pl_videolist: "/feeds/videpostroll_v5_slike/71924303.cms?feedtype=json&callback=cached",
  },

  slikeAdCodes: {
    astro: "1564454",
    others: "ros",
  },
  gRecaptchaKey: "6LcxSGUUAAAAAPsGPKVpEkLKxnuluuqDKa1AivEN",
  ga: {
    gtm: "GTM-K2H9XKD",
    gverifyCode: "zU5qA2HPdN2PFPyZd70q25_5NHXI8AE8AbqWNwXsoFA",
    gatrackid: "UA-29031733-14",
    gaprofile: "[]",
  },

  growthrx: {
    stg: {
      id: "gf1381096",
    },
    prod: {
      id: "g33c357b0",
    },
  },

  iBeat: {
    host: "https://www.iamgujarat.com/",
    wapsitename: "Iam Gujarat",
    // FIXME : telugu value this is
    ibeatkey: "38ad319407d7d909cb63d5bfe2f7c34", // not used in the project
  },

  appexclusive_ga: {
    category: "Wap to App install",
    action: "click",
    label: "articlelist_topsection",
  },
  poll: {
    link: "pollslist.cms",
  },
  poll_ga: {
    poll_submit: {
      category: "pollSurvey",
      action: "submit",
      label: "pollsubmit",
    },
    poll_result_loggedIn: {
      category: "pollSurvey",
      action: "viewresult",
      label: "loggedin",
    },
    poll_result_nonloggedIn: {
      category: "pollSurvey",
      action: "viewresult",
      label: "non-loggedin",
    },
  },

  photoshow_app_install: {
    ga_object: {
      category: "Wap to App install",
      action: "click",
      label: "photo show",
    },
  },
  sso_ga: {
    truecaller_requestscreen: {
      category: "True Caller",
      action: "Click",
      label: "Screen Request",
    },
    truecaller_screen: {
      category: "True Caller",
      action: "Click",
      label: "Screen View",
    },
    truecaller_screencontinue: {
      category: "True Caller",
      action: "Click",
      label: "Continue",
    },
    truecaller_skip: {
      category: "True Caller",
      action: "Click",
      label: "Skip",
    },
    truecaller_loginsuccess: {
      category: "True Caller",
      action: "Click",
      label: "Login Successful",
    },
  },
  comscore: { c1: "2", c2: "6036484" },
  applinks: {
    android: {
      hrnav: "https://iamgujarat.onelink.me/yS1l/63579910",
      asshow: "https://iamgujarat.onelink.me/yS1l/2b5aa8fa",
      photoshow: "https://iamgujarat.onelink.me/yS1l/a8989653",
      serviceDrawer: "https://iamgujarat.onelink.me/yS1l/be72b6ed",
      social: "https://iamgujarat.onelink.me/yS1l/6a7ff93f",
      // https://go.onelink.me/yS1l?pid=WAP_MWEB_New&c=Internal_Campaign&af_dp=iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dnews-%24%7C%24-domain%3Dt-%24%7C%24-id%3D569276
      oip_appexclusive: "https://navbharattimes.indiatimes.com/off-url-forward/tl_openinapp.cms?msid=", // not used
      // https://go.onelink.me/cMxT/b256ac1
    },
  },
  appdeeplink: {
    domain: "https://iamgujarat.onelink.me",
    shortcode: "yS1l",
    c: "Internal_Campaign",
    pid: {
      hrnav: "WAP_HRNAV_New",
      serviceDrawer: "WAP_BOTTOM_New",
      news: "WAP_MWEB_New",
      article_end: "WAP_AS_New",
      photo: "WAP_PG_New",
      toparticle: "WAP_Top%20Articles_New",
      extphoto: "WAP_Home_Photostrip_New",
      photoshow: "WAP_Photoshow_New",
      campaign: "WAP_Election",
      campaignHome: "WAP_Election_Home",
      openinapp_as: "IAG_AS_Brandwire",
      openinapp_al: "IAG_AL_Brandwire",
      photofeature: "WAP_MWEB_New",
      photoFeature: "WAP_MWEB_New",
      recipe: "WAP_MWEB_New",
    },
    openapp: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    opennews: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    openphoto: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    openextphoto: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    openhtmlview: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    opensection: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    openphotofeature: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    openrecipe: "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat",
    // opennews:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dnews-%24%7C%24-domain%3Dt-%24%7C%24-id%3D%7BMSID%7D",
    // openphoto:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dphoto-%24%7C%24-domain%3Dt-%24%7C%24-id%3D%7BMSID%7D",
    // openextphoto:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dphoto-%24%7C%24-domain%3DP-%24%7C%24-id%3D%7BMSID%7D",
    // openhtmlview:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dhtml-%24%7C%24-domain%3Dt-%24%7C%24-id%3D%3Cinput_web_url%3E",
    // opensection:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Dsection-%24%7C%24-sectionID%3DBriefs-01-%24%7C%24-sectionName%3D%E0%A4%AC%E0%A5%8D%E0%A4%B0%E0%A5%80%E0%A4%AB-%24%7C%24-id%3D%7BMSID%7D",
    // openphotofeature:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3DphotoFeature-%24%7C%24-domain%3Dt-%24%7C%24-id%3D%7BMSID%7D",
    // openrecipe:
    //   "iagreaderactivities%3A%2F%2Fopen-%24%7C%24-pub%3D6%3AI%2520Am%2520Gujarat-%24%7C%24-type%3Drecipe-%24%7C%24-domain%3Dt-%24%7C%24-id%3D%7BMSID%7D",
  },
  pushNotification: {
    cookieName: "notifypopup_iag",
    defaultThumb: "https://static.langimg.com/thumb/msid-84435655,width-100,height-100/pic.jpg",
    heading: "મેળવો લેટેસ્ટ તેમજ બ્રેકિંગ ન્યૂઝના નોટિફિકેશન્સ",
  },
  weather: {
    geoLocationApi: "https://geoapi.indiatimes.com/?cb=1",
    defaultCity: "Ahmedabad",
    citiesJson: "https://navbharattimes.indiatimes.com/pwafeeds/pwa_geocities.cms",
    weatherFeed: "https://navbharattimes.indiatimes.com/pwafeeds/pwa_weathertoday.cms?lat=",
    heading: "આજનું તાપમાન",
  },
  pollution: {
    heading: "प्रदूषण",
  },
  fuel: {
    heading: "फ्यूल",
    feedUrl: "https://toibnews.timesofindia.indiatimes.com/fuelprice/latestPrice",
  },

  oneTapSignIn: {
    smartlockurl: "https://smartlock.google.com/client",
    isViewGaTracked: false,
    eventGAObj: {
      hitType: "event",
      category: "Login",
      action: "One_Tap",
      label: "",
    },
    text: {
      oneTapSuccessMsg: "આપે સફળતાપૂર્વક લોગઈન કરી લીધું છે",
      oneTapErrorMsg: "ઓહ! કંઈક ગડબડ છે. થોડીવાર પછી પ્રયાસ કરો",
      oneTapBtnText: "ફરી પ્રયાસ કરો",
    },
    constants: {
      onetapIgnoreDays: 15,
      onetapCookie: "oneTapNextShow",
      onetapIframeCheckTimer: 2000,
      smartlockJsLoadTimer: 10000,
      clientId: "1099234790050-15hq39ek160djij0kld2hjkj4vlrq8nq.apps.googleusercontent.com",
      channel: "iamguj",
      jssoAPIGetURL: "https://jsso.indiatimes.com/sso/services/gponetaplogin/verify",
    },
  },

  ZodiacSigns: {
    url: "#",
    data: [
      {
        name: "aries",
        regName: "મેષ",
        pos: 1,
      },
      {
        name: "taurus",
        regName: "વૃષભ",
        pos: 2,
      },
      {
        name: "gemini",
        regName: "મિથુન",
        pos: 3,
      },
      {
        name: "cancer",
        regName: "કર્ક",
        pos: 4,
      },
      {
        name: "leo",
        regName: "સિંહ",
        pos: 5,
      },
      {
        name: "virgo",
        regName: "ધન",
        pos: 6,
      },
      {
        name: "libra",
        regName: "તુલા",
        pos: 7,
      },
      {
        name: "scorpio",
        regName: "વૃશ્ચિક",
        pos: 8,
      },
      {
        name: "sagittarius",
        regName: "ધન",
        pos: 9,
      },
      {
        name: "capricorn",
        regName: "મકર",
        pos: 10,
      },
      {
        name: "aquarius",
        regName: "કુંભ",
        pos: 11,
      },
      {
        name: "pisces",
        regName: "મીન",
        pos: 12,
      },
    ],
  },
  ads: {
    gptUrl: "https://www.googletagservices.com/tag/js/gpt.js",
    ctnAddress: "https://static.clmbtech.com/ctn/commons/js/colombia_v2.js",
    dmpUrl: "https://static.clmbtech.com/ase/7269/1210/aa.js",
    ADS_PRIORITY: "FAN",
    ctnads: {
      inline: "341277",
      ampinline: "328062",
      ampinlinehome: "328061",
      ampctnhometop: "330646", // For AMP Home Page

      ctnlist1: "341277",
      ctnlistTop: "341277",
      ctnlistshow: "207483",
      ctnvs: "340820", // Video Show
      ctnlist: "340824", // for SCB - single code base
      ctnpgl1: "340818",
      ctnpgl2: "340819",
      ctnvll1: "340825",
      ctnvll2: "340820",
      ctntrendvl: "340822",
      ctnsluglist: "355618",
      listdefault: "355778",
      listhorizontalsmall: "355786",

      ctnshow: "324112",
      ctnshow1: "324112",
      ctnmidart: {
        ctnId: "324111",
        dfpSlot: "/7176/iamgujarat/iamgujarat_Bid_Experiment/iamgujarat_ROS_AS_MID_Bid_Experiment_640",
        dfpSize: [
          [640, 360],
          [300, 250],
        ],
      },
      ctnrhsend: "264096",

      ctnrhsvideo: "340817",
      ctnrecommended: "351354", // "335038",
      ctnmidart_test: "", // for test page only requested by sales
      ctnbiglist: "341278",
      ctninlineshow: "208370",
      // ctnppdad: "346794", // this adcode is used for testing.
      ctnppdad: "319688",
      ctnshowvideo: "340826", // video ad on articleshow

      ctnhmvideoslider: "340827",
      ctnhmphotoslider: "340828",
      ctnhome: "340812",
      ctnhometop: "340812",
      ctnhomevideo: "340812",
      homedefault: "340811",
      homedefaultsmall: "340813",
      homehorizontalsmall: "340814",
      homevertical: "340815",
      homehorizontal: "340816",
      homeibeat: "341269",
      ctnbighome: "341273",

      ctnslider: "210640",
      ctnVideoURL: "https://telugu.samayam.com/ads_native_video_dev.cms",

      ctnphotoshowrhs: "324691",
      ctnbigphoto: "340834",
      ctnshowphoto: "340835",
      adspositionsPhoto: "-3-,-5-,-9-,-14-,-20-,-27-,-36-,-45-,-55-",

      ctnint: "324743",
      ctnint_test: "",

      adspositionsLiveblog: "-5-,-10-,-15-,-20-,-25-,-30-,-35-,-40-",
      ctncookievalue: "ce_tsmap",

      ctntopicsall: "340824",
      ctntopicsnews: "340824",
      ctntopicsphotos: "340824",
      ctntopicsvideos: "340823",
      ctnbccl: "340379",
    },
    ctnslots: {
      atf: "327958",
      ctnshow: "323124",
      ctnbiglist: "",
      ctnbigphoto: "",
      midart: "327957",
      articleshow: "ctnshow,atf,midart",
      liveblog: "",
      photoshow: "",
      videoshow: "",
      videolist: "",
    },

    dfpads: {
      adSiteName: "iamgujarat",
      adSiteShortName: "IamGujarat",
      topatf: "/7176/iamgujarat/HP/Homepage/IMG_HP_ATF_Band_1000",
      cubead1: "/7176/iamgujarat/ROS/IMG_ROS_CUBE_195x85",
      cubead2: "/7176/iamgujarat/ROS/IMG_ROS_CUBE2_195x85",
      cubeheadad1: "/7176/iamgujarat/ROS/IMG_ROS_CUBE_195x30",
      cubeheadad2: "/7176/iamgujarat/ROS/IMG_ROS_CUBE_195x30",
      cubeheadad3: "/7176/iamgujarat/ROS/IMG_ROS_CUBE2_195x30",
      cubeheadad4: "/7176/iamgujarat/ROS/IMG_ROS_CUBE2_195x30",
      andbeyond: "/7176/iamgujarat/ROS/IMG_ROS_Andbeyond_1x1",
      multiplex: "/7176/iamgujarat/ROS/IMG_ROS_Multiplex",
      canvasAd: "/7176/iamgujarat_Mweb/iamgujarat_Mweb_ROS/IamGujarat_Mweb_ROS_Mcanvas_1x1",
      // inuxu: "/7176/iamgujarat/ROS/IMG_ROS_Inuxu_1x1",
      // @vasu asked to add mobile code for the desktop as well
      inuxu: "/7176/iamgujarat_Mweb/iamgujarat_Mweb_ROS/IMG_MWEB_ROS_Inuxu_1x1",
      skinrhs: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_SkinRHS",
      others: {
        atf: {
          id: "div-gpt-ad-21897448184-atf",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_ATF",
          size: [
            [728, 90],
            [980, 200],
            [980, 120],
            [950, 90],
            [930, 180],
            [750, 300],
            [750, 200],
            [750, 100],
            [970, 90],
          ],
        },
        mrec1: {
          id: "div-gpt-ad-21897448184-mrec1",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_Mrec1",
          size: [
            [300, 250],
            [250, 250],
            [200, 200],
          ],
        },
        mrec2: {
          id: "div-gpt-ad-21897448184-mrec2",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_Mrec2",
          size: [
            [300, 250],
            [250, 250],
            [200, 200],
          ],
        },
        mrecinf: {
          id: "div-gpt-ad-21897448184-mrec2",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_MrecINF",
          size: [
            [300, 250],
            [250, 250],
            [200, 200],
          ],
        },
        btf: {
          id: "div-gpt-ad-21897448184-btf",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_BTF",
          size: [
            [728, 90],
            [980, 200],
            [980, 120],
            [950, 90],
            [930, 180],
            [750, 300],
            [750, 200],
            [750, 100],
            [970, 90],
          ],
        },
        innove: {
          id: "div-gpt-ad-21897448184-innove",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_Innov1",
          size: [[1, 1]],
        },
        skinrhs: {
          id: "div-gpt-ad-21897448184-skinRhs",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_SkinRHS",
          size: [
            [125, 600],
            [120, 600],
            [160, 600],
          ],
        },
        slug: {
          id: "div-gpt-ad-1572861422851-slug",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_Slug1",
          size: [
            [300, 100],
            [320, 100],
          ],
        },
        subs: {
          id: "div-gpt-ad-1565953864757-subs",
          name: "/7176/iamgujarat/iamgujarat_Others/iamgujarat_Others_Subs",
          size: [
            [1, 1],
            [320, 100],
          ],
        },
      },
    },
  },
};
