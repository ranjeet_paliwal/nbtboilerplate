const testFolder = "./dist/";
const fs = require("fs");
const path = require("path");
const dotenv = require("dotenv");
const DOT_ENV_PATH = path.join(
  __dirname,
  "server/env/" + process.env.SITE + "." + process.env.NODE_ENV + ".env"
);
dotenv.load({
  path: DOT_ENV_PATH
});

var arr = [];
//var site_url = "http://localhost:3003";
var site_url = "";

fs.readdirSync(testFolder).forEach(file => {
  if (
    file.indexOf(".") > 0 &&
    file != "sw.js" &&
    file != "service-worker.js" &&
    file != "client-stats.json" &&
    file.indexOf(".js") < 0 &&
    file.indexOf(".png") < 0 &&
    file != "server.js" &&
    file != "robots.txt" &&
    file != "manifest.json" &&
    file != "favicon.ico" &&
    file != "404.html" &&
    file != "500.html" &&
    file != "browserconfig.xml" &&
    file != "serviceworker-cache-polyfill.js" &&
    file != "offlinestuff.js"
  ) {
    arr.push(site_url + "/" + file);
    //console.log(file);
  }
});

let versionFile = fs.readFileSync(testFolder + "client-stats.json");
versionFile = JSON.parse(versionFile);
let versionFileHash = versionFile.hash;
let assets = versionFile.assets;
for (let as in assets) {
  if (assets[as].name != "client-stats.json") {
    arr.push(site_url + "/" + assets[as].name);
  }
}

//console.log(arr);

fs.readdirSync(testFolder + "css/").forEach(file => {
  arr.push(site_url + "/css/" + file + "?v=" + versionFileHash);
});

/*fs.readdirSync(testFolder+"img/").forEach(file => {
	//arr.push(site_url+"/img/"+file);
});

fs.readdirSync(testFolder+"photo/").forEach(file => {
	//arr.push(site_url+"/photo/"+file);
});

fs.readdirSync(testFolder+"fonts/").forEach(file => {
	//arr.push(site_url+"/fonts/"+file);
});
*/
var filelist = '"' + arr.join('","') + '"';

var staticResource = ['"/android-icon-36x36.png"', '"' + site_url + '/"'];

var version = Date.now();
var versionString =
  'const version = "v' +
  version +
  '::"; const domainName="' +
  process.env.WEBSITE_URL +
  '";';
filelist =
  " const offlineStuff=[" + staticResource.join(",") + "," + filelist + "];";

/*fs.writeFile("./dist/offlinestuff.js", filelist, function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("offlinestuff file created!",(staticResource.length+filelist.length));
});*/

var filecontents = fs.readFileSync("./dist/service-worker.js");
filecontents = versionString + filelist + filecontents;
fs.writeFileSync("./dist/service-worker.js", filecontents);

fs.writeFileSync("./dist/version.js", version);
